INCLUDEPATH             +=  $${BASEDIR}/LM/LMLibrary/include
DEPENDPATH              +=  $${BASEDIR}/LM/LMLibrary
LIBS                    +=  -L"$${MAINBINDIR}"
LIBS                    +=  -lLMLibrary
DEFINES                 +=  LMLibCppStyle
QT                      +=  network

#win32:PRE_TARGETDEPS    +=  $${MAINBINDIR}/LMLibrary.lib
#unix: PRE_TARGETDEPS    +=  $${MAINBINDIR}/libLMLibrary.a
