INCLUDEPATH             +=  $${BASEDIR}/RE/RELibrary/include
DEPENDPATH              +=  $${BASEDIR}/RE/RELibrary
LIBS                    +=  -L"$${MAINBINDIR}"
unix|linux:LIBS         +=  -lRELibrary
win32:LIBS              +=  -lRELibrary$${RELibraryMajVersion}
DEFINES                 +=  RELibCppStyle

win32:PRE_TARGETDEPS    +=  $${MAINBINDIR}/RELibrary$${RELibraryMajVersion}.dll
linux:PRE_TARGETDEPS    +=  $${MAINBINDIR}/libRELibrary.so
macx:PRE_TARGETDEPS     +=  $${MAINBINDIR}/libRELibrary.dylib
