#!/bin/sh

function doThingy {
    CONF=$1
    BITS=$2
    ARCH=$3
    DFLAG=
    if [ $1 = "Debug" ]; then
        DFLAG=d
    fi
    DEST="$CONF/$ARCH"
    QTBASE="/c/QtSDK/Qt/4.8.5/win-msvc2012-$BITS"
    echo "Handling $BITS bits $CONF builds withing $ARCH folders..."
    mkdir -p    $DEST $DEST/codecs $DEST/iconengines $DEST/imageformats $DEST/sqldrivers
    echo "  Copying Tesseract tessdata folder..."
    cp -R Dependencies/tesseract-3.02/tessdata                                      $DEST/
    echo "  Copying DMTX binaries..."
    cp -R Dependencies/libdmtx-0.7.4/$ARCH/vc11/bin/*.dll                           $DEST/
    echo "  Copying OpenCV binaries..."
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_calib3d248${DFLAG}.dll    $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_contrib248${DFLAG}.dll    $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_core248${DFLAG}.dll       $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_features2d248${DFLAG}.dll $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_ffmpeg248_64.dll          $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_flann248${DFLAG}.dll      $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_gpu248${DFLAG}.dll        $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_highgui248${DFLAG}.dll    $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_imgproc248${DFLAG}.dll    $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_legacy248${DFLAG}.dll     $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_ml248${DFLAG}.dll         $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_nonfree248${DFLAG}.dll    $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_objdetect248${DFLAG}.dll  $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_ocl248${DFLAG}.dll        $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_photo248${DFLAG}.dll      $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_stitching248${DFLAG}.dll  $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_superres248${DFLAG}.dll   $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_video248${DFLAG}.dll      $DEST/
    cp -R Dependencies/opencv-2.4.8/$ARCH/vc11/bin/opencv_videostab248${DFLAG}.dll  $DEST/

    echo "  Copying Tesseract binaries..."
    cp -R Dependencies/tesseract-3.02/$ARCH/vc11/bin/*.dll                          $DEST/
    echo "  Copying A2iA binaries..."
    cp -R Dependencies/A2iACheckReader.dll                                          $DEST/
    echo "  Copying Qt basic libraries..."
    cp -R $QTBASE/bin/QtCore${DFLAG}4.dll                           $DEST/
    cp -R $QTBASE/bin/QtGui${DFLAG}4.dll                            $DEST/
    cp -R $QTBASE/bin/QtNetwork${DFLAG}4.dll                        $DEST/
    cp -R $QTBASE/bin/QtScript${DFLAG}4.dll                         $DEST/
    cp -R $QTBASE/bin/QtSql${DFLAG}4.dll                            $DEST/
    cp -R $QTBASE/bin/QtXml${DFLAG}4.dll                            $DEST/
    echo "  Copying Qt plugin libraries..."
    cp -R $QTBASE/plugins/codecs/qcncodecs${DFLAG}4.dll             $DEST/codecs/
    cp -R $QTBASE/plugins/codecs/qjpcodecs${DFLAG}4.dll             $DEST/codecs/
    cp -R $QTBASE/plugins/codecs/qkrcodecs${DFLAG}4.dll             $DEST/codecs/
    cp -R $QTBASE/plugins/codecs/qtwcodecs${DFLAG}4.dll             $DEST/codecs/
    cp -R $QTBASE/plugins/iconengines/qsvgicon${DFLAG}4.dll         $DEST/iconengines/
    cp -R $QTBASE/plugins/imageformats/qgif${DFLAG}4.dll            $DEST/imageformats/
    cp -R $QTBASE/plugins/imageformats/qico${DFLAG}4.dll            $DEST/imageformats/
    cp -R $QTBASE/plugins/imageformats/qjpeg${DFLAG}4.dll           $DEST/imageformats/
    cp -R $QTBASE/plugins/imageformats/qmng${DFLAG}4.dll            $DEST/imageformats/
    cp -R $QTBASE/plugins/imageformats/qsvg${DFLAG}4.dll            $DEST/imageformats/
    cp -R $QTBASE/plugins/imageformats/qtga${DFLAG}4.dll            $DEST/imageformats/
    cp -R $QTBASE/plugins/imageformats/qtiff${DFLAG}4.dll           $DEST/imageformats/
    cp -R $QTBASE/plugins/sqldrivers/qsqlite${DFLAG}4.dll           $DEST/sqldrivers/
}

#doThingy Debug      32 x86
#doThingy Debug      64 x64
#doThingy Release    32 x86
#doThingy Release    64 x64

doThingy  $1 $2 $3
