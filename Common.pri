CONFIG(debug, debug|release) {
    CONFNAME        =   "Debug"
    DEFINES         +=  QT_NO_DEBUG_OUTPUT QT_NO_DEBUG_STREAM
} else {
    CONFNAME        =   "Release"
    CONFIG          +=  warn_off
    DEFINES         +=  QT_NO_DEBUG_OUTPUT QT_NO_DEBUG_STREAM
}
CONFIG              +=  qt thread
QT                  +=  core
QT                  -=  gui

ARCH                =   x86_64 #$${QMAKE_TARGET.arch}
contains(ARCH, "x86_64") {
    message         ("Will compile for 64 bit...")
    ARCH            =   "x64"
    DEFINES         +=  x86_64
} else {
    message         ("Will compile for 32 bit...")
    ARCH            =   "x86"
    DEFINES         +=  x86
}
win32:DEFINES       +=  _CRT_SECURE_NO_WARNINGS

macx {
    # WARNING: For some OpenCV functions to properly link:
    # In /opt/QtSDK/Qt/4.8.5/macx-llvm/mkspecs/common/g++-macx.conf:
    # QMAKE_CFLAGS_X86_64 += -Xarch_x86_64 -mmacosx-version-min=10.7
    # QMAKE_CFLAGS_PPC_64 += -Xarch_ppc64 -mmacosx-version-min=10.7
    # Also, the following 3 variables QMAKE_MACOSX_DEPLOYMENT_TARGET, QMAKE_CXXFLAGS and QMAKE_LFLAGS need those changes:
    QMAKE_MACOSX_DEPLOYMENT_TARGET  =   10.7
    QMAKE_CXXFLAGS                  +=  -std=gnu++11 -stdlib=libc++ -mmacosx-version-min=10.7 -Wno-deprecated-register
    QMAKE_LFLAGS                    +=  -std=gnu++11 -stdlib=libc++ -mmacosx-version-min=10.7 -Wno-deprecated-register

    QMAKE_INFO_PLIST                =   src/Resources/Info.plist
}

BASEDIR             =   $$PWD
MAINBINDIR          =   "$${PWD}/$${CONFNAME}/$${ARCH}"
INSTALLDIR          =   /opt/PQRRS


# Libraries versioning system.
LMLibraryVersion            =   0.1.3
RELibraryVersion            =   0.5.27
REClientBinaryVersion       =   0.0.9
SCLibraryVersion            =   0.2.4
RELibraryAdminVersion       =   0.2.20
RELibraryApiTesterVersion   =   0.2.4
RSUniversalUIVersion        =   0.2.3
ShowcaseVersion             =   0.1.3
POCShowcaseVersion          =   0.1.3
TessUtilsVersion            =   0.1.1
SCLibraryTesterVersion      =   0.3.2
PlusTekVersion              =   0.1.1
VersionUpdaterVersion       =   0.1.3

REPlugA2iAVersion           =   0.1.4
REPlugFeaturesFinderVersion =   0.0.7
REPlugFujiToolsVersion      =   0.1.5
REPlugOpenCVVersion         =   0.0.13

# Extracts major version code from version code.
defineReplace(extractMajCode) {
    spl         = $$split($$1, ".")
    return      ($$first(spl))
}
defineTest(checkTouchVersion) {
    targetName              = $$1
    currentVersion          = $$2
    flagFile                = "$${BASEDIR}/Versioning/$${targetName}.lastBuildVersion"
    headerFile              = "$${BASEDIR}/Versioning/$${targetName}Version.h"
    varName                 = "$${targetName}Version"
    !infile($$flagFile,$$varName,$$currentVersion) {
        # Prepare some usefull constants.
        H                   =   $$LITERAL_HASH
        unix|linux {
            message         ("Unix-touching $$headerFile ...")
            system          ( echo "$$varName=$${currentVersion}"                               >   $$flagFile )
            system          ( echo \"$${LITERAL_HASH}define $$varName $$currentVersion\"        >   $${headerFile} )
            system          ( echo \"$${LITERAL_HASH}define STRINGIZE_(x) $${LITERAL_HASH}x\"   >>  $${headerFile} )
            system          ( echo \"$${LITERAL_HASH}define STRINGIZE(x) STRINGIZE_(x)\"        >>  $${headerFile} )
        } else {
            message         ("Win-touching $$headerFile ...")
            system          ( echo $$varName=$${currentVersion}                                 >   $$flagFile )
            system          ( echo $${LITERAL_HASH}define $$varName $$currentVersion            >   $${headerFile} )
            system          ( echo $${LITERAL_HASH}define STRINGIZE_(x) $${LITERAL_HASH}x       >>  $${headerFile} )
            system          ( echo $${LITERAL_HASH}define STRINGIZE(x) STRINGIZE_(x)            >>  $${headerFile} )
        }
    }
    INCLUDEPATH         +=  "$${BASEDIR}/Versioning"
    HEADERS             +=  "$${BASEDIR}/Versioning/$${targetName}Version.h"
}

# Major version number helpers for libs.
LMLibraryMajVersion         =   $$extractMajCode(LMLibraryVersion)
RELibraryMajVersion         =   $$extractMajCode(RELibraryVersion)
SCLibraryMajVersion         =   $$extractMajCode(SCLibraryVersion)
