# Base settings.
include             (../../Common.pri)
DESTDIR             =   "$$MAINBINDIR"
TARGET              =   LMLibrary
TEMPLATE            =   lib
CONFIG              +=  staticlib
QT                  +=  network

VERSION             =   $${LMLibraryVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

TRANSLATIONS        +=  src/Translations/LMLibrary_fr.ts

HEADERS             +=  src/Core.h \
                        src/LMTypes.h


SOURCES             +=  src/Core.cpp \
                        src/LMTypes.cpp
