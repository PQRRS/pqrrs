#include "LMTypes.h"
#include "../../Versioning/LMLibraryVersion.h"

void LM::StaticInitializer () {
    static bool	initDone			= false;
    if (initDone)					return;
    initDone						= true;
}

char const* LM::Version () {
    return const_cast<char*>(STRINGIZE(LMLibraryVersion));
}
