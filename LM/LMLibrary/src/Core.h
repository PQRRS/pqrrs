#ifndef LM_Core_h
#define LM_Core_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include <QtCore/QDate>
#include <QtCore/QStringList>
#include <QtCore/QCryptographicHash>
#include "LMTypes.h"

#define SECURITYLESS_BUILD
//#ifdef SECURITYLESS_BUILD
//#   pragma message("!!!   SECURITYLESS BUILD   !!!")
//#endif

namespace LM {

class Core : public QObject {
public:
    //! License Raw Data Setter.
    Q_PROPERTY  (QString        license                 WRITE setLicense)
    //! License Verification Flag Getter.
    Q_PROPERTY  (bool           verified                READ isVerified)
    //! Registration Name Getter.
    Q_PROPERTY  (QString        name                    READ name)
    //! Registration Company Getter.
    Q_PROPERTY  (QString        registeredCompanyName   READ registeredCompanyName)
    //! Registration Email Getter.
    Q_PROPERTY  (QString        registeredEmail         READ registeredEmail)
    //! Expiration Timestamp Getter.
    Q_PROPERTY  (quint32        expiresAfter            READ expiresAfter)
    //! Registered Software Tags Getter.
    Q_PROPERTY  (QStringList    softwareTags            READ softwareTags)
    //! Registered Hardware Tags Getter.
    Q_PROPERTY  (QStringList    hardwareTags            READ hardwareTags)
    //! License Loading Status Message Getter.
    Q_PROPERTY  (QString        lastMessage             READ lastMessage)

    /*! @brief Licensing Management Core Constructor.
     * @param p is the parent object to use when constructing the Core.
     */
                                        Core							(QObject *p=0);

public:
    // Some helpers.
    static QString                      hostUuid                        ();
    static QString                      reverse                         (QString const &s) {
        QByteArray          ba              = s.toUtf8();
        char                *d              = ba.data();
        std::reverse                        (d, d+s.length());
        return                              QString(d);
    }
    // Main methods.
    void                                reset                           ();
    bool                                loadFromFile                    (QString const &fn);
    bool                                setLicense                      (QString const &v);
    // Direct accessors.
    bool                                isVerified                      () const    { return _verified; }
    QString const&                      name                            () const    { return _name; }
    QString const&                      registeredCompanyName           () const    { return _registeredCompanyName; }
    QString const&                      registeredEmail                 () const    { return _registeredEmail; }
    quint32 const&                      expiresAfter                    () const    { return _expiresAfter; }
    QStringList const&                  softwareTags                    () const    { return _softwareTags; }
    QStringList const&                  hardwareTags                    () const    { return _hardwareTags; }
    QString const&                      lastMessage                     () const    { return _lastMessage; }

    static bool                         LMObfuscatedCheesecakeForData   (LM::Core const *lm, QString const &softId, QString const &hwSerial, quint8 *out);
    static void                         LMCheesecakeForData             (QString const &softId, QString const &hwSerial, quint8 const *obfCk, quint8 *out);

private:
    // Static instance.
    static LM::Core						*_Instance;
    bool                                _verified;
    QString                             _name;
    QString                             _registeredCompanyName;
    QString                             _registeredEmail;
    quint32                             _expiresAfter;
    QByteArray                          _sunKey;
    QByteArray                          _moonKey;
    QByteArray                          _equinoxKey;
    QStringList                         _softwareTags;
    QStringList                         _hardwareTags;
    mutable QString                     _lastMessage;
};

}

// Usefull definitions.
#ifndef DeclareCheesecake
#   define  DeclareCheesecake(a, b, c) quint8 ck[16]; LM::Core::LMCheesecakeForData(a, b, c, ck)
#   define  CKZ01   ((CK00|(CK04&CK07&0xd0))%0x0d)
#   define  CKZ02   ((CK03|(CK05&CK10&0xeb))%0xcb)
#   define  CKZ03   ((CK03|(CK05&CK10&0xeb))%0x07)
#   define  CKZ04   ((CK03|(CK05&CK10&0xeb))%0x1d)
#   define  CKZ05  (((CK02|(CK03&CK09&0x25))%0x32)-0x20)
#   define  CKZ06   ((CK06|(CK11&CK15&0xe0))%0xce)
#   define  CKZ07   ((CK01|(CK08&CK13&0xd1))%0x9b)
#   define  CKZ08   ((CK07|(CK12&CK11&0xdd))%0x6a)
#   define  CKT01  (((CK02|(CK03&CK09&0x25))%0x32)==0x20)
#   define  CK00    ck[0]
#   define  CK01    ck[1]
#   define  CK02    ck[2]
#   define  CK03    ck[3]
#   define  CK04    ck[4]
#   define  CK05    ck[5]
#   define  CK06    ck[6]
#   define  CK07    ck[7]
#   define  CK08    ck[8]
#   define  CK09    ck[9]
#   define  CK10    ck[10]
#   define  CK11    ck[11]
#   define  CK12    ck[12]
#   define  CK13    ck[13]
#   define  CK14    ck[14]
#   define  CK15    ck[15]
#endif

#endif
