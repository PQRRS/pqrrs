#ifndef LM_LMTypes_h
#define LM_LMTypes_h

// Make sure basic Qt preprocessor variables are ready.
#include <QtCore/QtGlobal>
#include <QtCore/QDebug>
#ifdef Q_OS_WIN
#   include <windows.h>
#endif

// Import / Export.
#ifdef LMLib
#	define LMLibOpt Q_DECL_EXPORT
#else
#	define LMLibOpt Q_DECL_IMPORT
#endif

// Define LMCConv based on the platform.
#ifndef LMCConv
    #ifdef Q_OS_WIN
        #define LMCConv WINAPI
    #endif
#endif

#ifndef Dbg
#   define Dbg      qDebug()    << Q_FUNC_INFO << "#"
#   define Wrn      qWarning()  << Q_FUNC_INFO << "#"
#   define Crt      qCritical() << Q_FUNC_INFO << "#"
#endif

/*! @brief Licensing Management Namespace.
 * The Licensing Management system can be seen as a static framework: it offers helper methods that can be compiled into the main binary
 * so there is no need to rely on it as an external library, thus making harder the task of patching the binary so it runs licenseless.
 * The principles of the Licensing Management system are as follow:
 * - An application compiles against it, using a software identifier (Eg. com.Company.ProductName).
 * - Then, users of the software go to Licensor.PierreQR.fr and request licenses for every software identifiers and machines they use.
 *
 * A license has the following information embeded into it:
 * - Name (Eg. DOE John),
 * - Company name  (Eg. Cyberceuf Inc.),
 * - Email (Eg. doe.j@coronners.net),
 * - Expiration date,
 * - A certain amount of software tags (So the license only applies to those),
 * - A certain amount of hardware tags (So the license only applies to those),
 * Once an application has succesfully loaded a license file, a certain number of variables (Cheesecakes) are defined.
 * Those are computed based on all the factors of the license (Hardware and software identifiers). Once available, cheesecake has to be used within
 * the application algorythms.
 * In the edventuallity of a fake license being loaded, there are great chances that the cheesecakes would not be as expected. Even if the attaquer
 * patched the licensing system, chances that cheesecakes have been computed an unexpected way are very high, thus making the application randomly crash. */
namespace LM {
    /*! @brief Static Initializer.
     * Initializes the Licensing  Management system so it is ready to factorize License Management Cores. */
    void                            StaticInitializer				();
    /*! @brief Licensing Manager Version.
     * Returns the current Licensing Management binary version. */
    char const*                     Version							();
}

#endif
