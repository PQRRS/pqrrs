#include "Core.h"
#include "LMTypes.h"
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QDate>
#include <QtCore/QByteArray>
#include <QtCore/QStringList>
#include <QtCore/QCryptographicHash>
#include <QtNetwork/QNetworkInterface>

typedef QHash<QString,QString> SSDictionary;

LM::Core*	LM::Core::_Instance	= 0;

// Constructor / Destructor.
LM::Core::Core (QObject *p)
: QObject(p), _verified(false) {
    // Initialize everything.
    LM::StaticInitializer   ();
}

QString LM::Core::hostUuid() {
    QStringList uuids;
    foreach (QNetworkInterface const &i, QNetworkInterface::allInterfaces()) {
        QNetworkInterface::InterfaceFlags   f   = i.flags();
        if (!(f & QNetworkInterface::IsLoopBack) &&
                (f & (QNetworkInterface::CanBroadcast | QNetworkInterface::CanMulticast)) &&
                !i.hardwareAddress().isEmpty()) {
            uuids   << i.hardwareAddress().remove(':').toLower();
        }
    }
    uuids.sort      ();
    return uuids.first();
}


void LM::Core::reset () {
    // Clean up current values.
    #ifdef SECURITYLESS_BUILD
        _verified                       = true;
        _name                           = "DOE John";
        _registeredCompanyName          = "Almighty Inc.";
        _registeredEmail                = "j.doe@almighty.inc";
        _expiresAfter                   = QDate().year()+10;
    #else
        _verified                       = false;
        _name.clear                     ();
        _registeredCompanyName.clear    ();
        _registeredEmail.clear          ();
        _expiresAfter                   = 0;
    #endif
    _sunKey.clear                       ();
    _moonKey.clear                      ();
    _equinoxKey.clear                   ();
    _softwareTags.clear                 ();
    _hardwareTags.clear                 ();
    _lastMessage.clear                  ();
}
bool LM::Core::loadFromFile (QString const &fn) {
    #ifdef SECURITYLESS_BUILD
        Q_UNUSED(fn);
       return setLicense("");
    #else
        QFile               f       (fn);
        if (!f.exists()) {
            _lastMessage            = tr("%1 doesn't exist.").arg(fn);
            Wrn                     << _lastMessage;
            return                  false;
        }
        f.open                      (QIODevice::ReadOnly);
        bool                ok      = setLicense(f.readAll());
        f.close                     ();
        return                      ok;
    #endif
}
bool LM::Core::setLicense (const QString &v) {
    reset                               ();
    #ifdef SECURITYLESS_BUILD
    Q_UNUSED(v);
    #else
    SSDictionary        dict;
    foreach (QString const &s, v.split(QRegExp("\r|\n"))) {
        if (!s.contains(':'))           continue;
        QStringList     parts           = s.split(':');
        QString         k               = parts[0].remove('\n').remove(' ');
        dict[k]                         = parts[1].trimmed();
    }

    // Retrieve license from preferences.
    _name                               = dict["LicenseName"];
    _registeredCompanyName              = dict["RegisteredName"];
    _registeredEmail                    = dict["RegisteredEmail"];
    _expiresAfter                       = dict["ExpiresAfter"].toUInt();
    _equinoxKey                         = QByteArray::fromHex(dict["EquinoxKey"].remove('-').toUtf8());
    QStringList         licSoftIds      = dict["UseableSoftware"].remove(' ').trimmed().split(',');
    QStringList         licHardIds      = dict["UseableHardware"].remove(' ').trimmed().split(',');
    // Make lowercase sorted software list.
    foreach (QString const &softId, licSoftIds)
        _softwareTags                   << softId.toLower();
    _softwareTags.sort                  ();
    // Make lowercase sorted hardware list.
    foreach (QString const &hardId, licHardIds)
        _hardwareTags                   << hardId.toLower();
    _hardwareTags.sort                  ();

    // Compute sun key.
    QString             sth             = _softwareTags.join(":float:");
    QString             hth             = _hardwareTags.join(":uint32:");
    QString             hash            = QString("bof:%1:string:%2:sqlite3:%3:db:%4:fmt:%5:spec:%6:eof").arg(hth).arg(sth).arg(_expiresAfter-2000).arg(_name.toLower()).arg(_registeredEmail.toLower()).arg(_registeredCompanyName.toLower());
    _sunKey                             = QCryptographicHash::hash(hash.toUtf8(), QCryptographicHash::Md5);
    QString             sunKeyStr       = _sunKey.toHex();
    { // Check moon key.
        hash                            = QString("%1__vtable__%2").arg(sunKeyStr).arg(reverse(sunKeyStr));
        _moonKey                        = QCryptographicHash::hash(hash.toUtf8(), QCryptographicHash::Md5);
        if (_moonKey!=QByteArray::fromHex(dict["MoonKey"].remove('-').toUtf8())) {
            _lastMessage                = tr("The license data is invalid.");
            Wrn                         << _lastMessage;
            return                      false;
        }
    }
    _verified                           = true;
    #endif
    return                              _verified;
}

// Checks a license for validity against software / hardware ID, and outs an obfuscated cheesecake.
bool LM::Core::LMObfuscatedCheesecakeForData (LM::Core const *lm, QString const &softId, QString const &hwSerial, quint8 out[]) {
    #ifdef SECURITYLESS_BUILD
    Q_UNUSED(lm); Q_UNUSED(softId); Q_UNUSED(hwSerial); Q_UNUSED(out);
    #else
    // Basic checks.
    if (!lm->_verified || !lm->_expiresAfter || lm->_softwareTags.isEmpty() || lm->_hardwareTags.isEmpty()) {
        lm->_lastMessage    = QObject::tr("The license data was not properly verified.");
        return              false;
    }
    // Check year.
    else if (lm->_expiresAfter<(quint32)QDate::currentDate().year()) {
        lm->_lastMessage    = QObject::tr("This license expired after %1.").arg(lm->_expiresAfter);
        return              false;
    }
    // Allowed hardware?
    else if (!lm->_softwareTags.contains(softId.toLower())) {
        lm->_lastMessage    = QObject::tr("The license does not allow you to run this specific software package.");
        return              false;
    }
    // Allowed hardware?
    else if (!lm->_hardwareTags.contains(hwSerial.toLower())) {
        lm->_lastMessage    = QObject::tr("The license does not allow you to use this specific hardware device (%2).").arg(hwSerial);
        return              false;
    }
   else if (!out)
        return              false;
    // Make CK array.
    QByteArray      hh  = QCryptographicHash::hash(QString("%1__staticMetaObject__%2").arg(softId).arg(hwSerial).toUtf8(), QCryptographicHash::Md5);
    quint8 const    *h  = (quint8 const*)hh.constData();
    quint8 const    *s  = (quint8 const*)lm->_sunKey.constData();
    quint8 const    *m  = (quint8 const*)lm->_moonKey.constData();
    quint8 const    *e  = (quint8 const*)lm->_equinoxKey.constData();
    for (quint8 i=0; i<16; ++i)
        out[i]          = s[i] ^ m[i] ^ e[i] ^ h[i];
    #endif
    return                  true;
}
// Get cheesecake from a given software ID, hardware ID and obfuscated cheesecake.
void LM::Core:: LMCheesecakeForData (QString const &softId, QString const &hwSerial, quint8 const obfCk[], quint8 out[]) {
    #ifdef SECURITYLESS_BUILD
        Q_UNUSED(softId); Q_UNUSED(hwSerial); Q_UNUSED(obfCk);
        const quint8 tmp[] = { 0x82,0x1b,0xb6,0xc8,0x2d,0x0b,0xce,0xd0,0x9a,0x25,0xeb,0xdd,0x06,0xd1,0x1f,0xe0 };
        memcpy(out, tmp, 16);
    #else
        QByteArray      hh      = QCryptographicHash::hash(QString("%1__staticMetaObject__%2").arg(softId).arg(hwSerial).toUtf8(), QCryptographicHash::Md5);
        quint8 const    *h      = (quint8 const*)hh.constData();
        for (quint8 i=0; i<16; ++i)
            out[i]              = obfCk[i] ^ h[i];
    #endif
}
