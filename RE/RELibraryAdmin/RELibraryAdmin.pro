# Base settings.
include             (../../Common.pri)
DESTDIR             =   "$$MAINBINDIR"
TARGET              =   RELibraryAdmin
TEMPLATE            =   app
QT                  +=  sql script gui

include             (../../LMLibrary.pri)
include             (../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                        install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/../../../libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}" && \
                        install_name_tool \
                            -change \
                                "libSCLibrary.$${SCLibraryMajVersion}.dylib" \
                                "@loader_path/../../../libSCLibrary.$${SCLibraryMajVersion}.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}"
}

target.path         =   $${INSTALLDIR}/bin
INSTALLS            +=  target

VERSION             =   $${RELibraryAdminVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

DEFINES         +=  FANN_NO_DLL _CRT_SECURE_NO_WARNINGS

TRANSLATIONS    +=  src/Translations/RELibraryAdmin_fr.ts
RESOURCES       +=  src/RELibraryAdmin.qrc

FORMS           +=  Forms/DlgAbout.ui \
                    Forms/WgtAnnsManager.ui \
                    Forms/WgtColorEditor.ui \
                    Forms/WgtCVMListEditor.ui \
                    Forms/WgtDocumentTypesManager.ui \
                    Forms/WgtFillColorEditor.ui \
                    Forms/WgtFilterStackEditor.ui \
                    Forms/WgtGlobalProperties.ui \
                    Forms/WgtGrayValueEditor.ui \
                    Forms/WgtResultDisplay.ui \
                    Forms/WgtZoneRecognizerEditor.ui \
                    Forms/WgtZoneRecognizersManager.ui \
                    Forms/WndMain.ui

HEADERS         +=  src/Core.h \
                    src/DBMigrator.h \
                    src/JSMinifier.h \
                    src/Models/AnnTestingResultsModel.h \
                    src/Models/DocumentTypesModel.h \
                    src/Models/GlobalPropertiesModel.h \
                    src/Models/SampleImagesModel.h \
                    src/Models/TrainedAnnsModel.h \
                    src/Models/ZoneFiltersModel.h \
                    src/Models/ZoneRecognizersModel.h \
                    src/REATypes.h \
                    src/UI/DlgAbout.h \
                    src/UI/EditorsAndDelegates/CustomEditorFactory.h \
                    src/UI/EditorsAndDelegates/ItemDelegates.h \
                    src/UI/EditorsAndDelegates/WgtColorEditor.h \
                    src/UI/EditorsAndDelegates/WgtCVMListEditor.h \
                    src/UI/EditorsAndDelegates/WgtEnumItemEditor.h \
                    src/UI/EditorsAndDelegates/WgtFakeByteArrayEditor.h \
                    src/UI/EditorsAndDelegates/WgtFillColorEditor.h \
                    src/UI/EditorsAndDelegates/WgtFilterStackEditor.h \
                    src/UI/EditorsAndDelegates/WgtGrayValueEditor.h \
                    src/UI/EditorsAndDelegates/WgtParametersListEditor.h \
                    src/UI/EditorsAndDelegates/WgtPathEditor.h \
                    src/UI/EditorsAndDelegates/WgtRatioEditor.h \
                    src/UI/EditorsAndDelegates/WgtZoneRecognizerEditor.h \
                    src/UI/WgtAnnsManager.h \
                    src/UI/WgtAnnTrainingHistory.h \
                    src/UI/WgtDocumentTypesManager.h \
                    src/UI/WgtGlobalProperties.h \
                    src/UI/WgtResultDisplay.h \
                    src/UI/WgtZoneRecognizersManager.h \
                    src/UI/Wizards/ClientBinaryGenerator.h \
                    src/UI/WndMain.h \
                    src/UI/ZoneRecognizersScene/ZoneRecognizerItem.h \
                    src/UI/ZoneRecognizersScene/ZoneRecognizersScene.h \
                    src/Workers/AnnRunner.h \
                    src/Workers/SampleImagesSigner.h \
                    ThirdParty/Fann/floatfann.h \
                    ThirdParty/JSEdit/JSEdit.h

SOURCES         +=  src/Core.cpp \
                    src/main.cpp \
                    src/Models/AnnTestingResultsModel.cpp \
                    src/Models/DocumentTypesModel.cpp \
                    src/Models/GlobalPropertiesModel.cpp \
                    src/Models/SampleImagesModel.cpp \
                    src/Models/TrainedAnnsModel.cpp \
                    src/Models/ZoneFiltersModel.cpp \
                    src/Models/ZoneRecognizersModel.cpp \
                    src/REATypes.cpp \
                    src/UI/EditorsAndDelegates/CustomEditorFactory.cpp \
                    src/UI/EditorsAndDelegates/ItemDelegates.cpp \
                    src/UI/EditorsAndDelegates/WgtCVMListEditor.cpp \
                    src/UI/EditorsAndDelegates/WgtEnumItemEditor.cpp \
                    src/UI/EditorsAndDelegates/WgtFilterStackEditor.cpp \
                    src/UI/EditorsAndDelegates/WgtParametersListEditor.cpp \
                    src/UI/EditorsAndDelegates/WgtZoneRecognizerEditor.cpp \
                    src/UI/WgtAnnsManager.cpp \
                    src/UI/WgtAnnTrainingHistory.cpp \
                    src/UI/WgtDocumentTypesManager.cpp \
                    src/UI/WgtGlobalProperties.cpp \
                    src/UI/WgtResultDisplay.cpp \
                    src/UI/WgtZoneRecognizersManager.cpp \
                    src/UI/Wizards/ClientBinaryGenerator.cpp \
                    src/UI/WndMain.cpp \
                    src/UI/ZoneRecognizersScene/ZoneRecognizerItem.cpp \
                    src/UI/ZoneRecognizersScene/ZoneRecognizersScene.cpp \
                    src/Workers/AnnRunner.cpp \
                    src/Workers/SampleImagesSigner.cpp \
                    ThirdParty/Fann/floatfann.c \
                    ThirdParty/JSEdit/JSEdit.cpp

