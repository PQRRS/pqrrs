#ifndef REA_Core_h
#define REA_Core_h

// Base class.
#include <QtCore/QObject>
// Base types.
#include "REATypes.h"
#include <RELibrary/RETypes>
// Various required stuff.
#include <QtCore/QDir>
#include <QtCore/QMutex>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlDatabase>
// Workers.
#include "Workers/SampleImagesSigner.h"

// Forward declarations.
class QProxyModel;
namespace LM { class Core; }
namespace RE { class Engine; }
namespace REA {
	class GlobalPropertiesModel;
	class DocumentTypesModel;
	class SampleImagesModel;
	class TrainedAnnsModel;
	class AnnTestingResultsModel;
	class AnnRunner;
}

namespace REA {

/*!
 * @brief The Recognition Engine Adminstrator Core Class.
 * This class is in charge of synchronizing every other. It handles GUI interaction to the underlying data objects.
 * It also holds informations about the project being currently opened. It also broadcasts events to the whole application (E.g.
 * Project Opening / Closing Events, ANN Training Events, Image Signature Computation Events, etc).
 * As any RELibraryAdmin project is in fact a wrapped SQLite3 database, it also holds a constant connexion to the SQLite3
 * database Qt engine that makes the transactions, while keeping an abstraction level via the various objects models.
 * When adding functionnality to the RELibraryAdmin project, the developer has to centralize most of the logic within this class,
 * paying high attention to what should be const, public, etc so encapsulation stays in good shape.
 */
class Core : public QObject {
Q_OBJECT

public:
    /*! @brief Default Constructor.
     * The Core's constructor will in turn:
     * - Initialize its members to null when possible,
     * - Call the REA::StaticInitializer to ensure static data is ready,
     * - Initialize the Licensing Manager based on the current Host's UUID, Software Identifier, and will then retrieve the Licensing Cheesecake,
     * - Create a main Recognition Engine Instance, passing it the initialized Licensing Manager,
     * - Initialize any required Model Proxy (Document Types, Sample Images, ANNs),
     * - Create a ANN main Runner Thread and connect it's signal to the Core's internal slots,
     * - Finally store the initialized instance of the core in the Core::_Instance variable for later Singleton retrieval.
     * @param[in] lm is the Licensing Manager to use accross the entire application workflow.
     * @param[in] p is the parent QObject to be used for owning the Core. A null QObject can be passed.
     * @return A fully initialized, ready-to-use RELibraryAdmin Core.
     */
    Core							(LM::Core *lm, QObject *p=0);
    /*! @brief Destructor.
     * In turn, the destructor will perform the following:
     * - Close the current project if any, and broadcast the close event to any attached listened,
     * - Destroy the Recognition Engine,
     * - Destroy any previously instanciated Model Proxy (Document Types, Sample Images, ANNs),
     * - Delete the HEAP allocation for the Licensing Manager Cheesecake.
     */
    virtual								~Core							();
    //! @brief Core Singleton Instance Getter.
    static Core&                        Instance                        ();
    //! @brief License Status Flag Getter.
    bool                                licensed                        () const;
    //! @brief Main Recognition Engine Instance Getter.
	RE::Engine&							recognitionEngine				();
    //! @brief Main Database Connexion (Project) Getter.
	QSqlDatabase&						database						();
    //! @brief Global Properties Model Getter.
	QAbstractItemModel&					globalPropertiesModel			();
    //! @brief Document Types Model (Proxy) Getter.
	QAbstractItemModel&					documentTypesModel				();
    //! @brief Underlying Document Types Model Getter.
	DocumentTypesModel*					realDocumentTypesModel			();
    //! @brief Sample Images Model (Proxy) Getter.
	QAbstractItemModel&					sampleImagesModel				();
    //! @brief Underlying Sample Images Model Getter.
	SampleImagesModel*					realSampleImagesModel			();
    //! @brief Trained ANNs Model (Proxy Getter).
	QAbstractItemModel&					trainedAnnsModel				();
    //! @brief Underlying Trained ANNs Model Getter.
	TrainedAnnsModel*					realTrainedAnnsModel			();
    //! @brief Image Signer "Idle" Flag Getter.
	bool								signerIddle						() const;
    //! @brief ANNs Trainer Thread "Idle" Flag Getter.
	bool								annTrainerIdle					() const;
    //! @brief ANNs Tester Thread "Idle" Flag Getter.
	bool								annTesterIdle					() const;
    //! @brief Global "Idle" Flag Getter.
	bool								allProcessesIdle				() const;

    /*! @brief Project Creation Method.
     * This method is in charge of creating a fresh RELibraryAdmin project database. It will, in turn:
     * - Create the requested project at the given path,
     * - Call the REA::Core::openProject() method.
     * @see REA::Core::openProject(QString const&, QStringList const&).
     * @param[in] path is the full path to use for the project's filename.
     */
	void								createProject					(QString const &path);
    /*! @brief Project Opening Method.
     * This method handles project database opening. It will in turn:
     * - Close the currently opened project, if any,
     * - Store the project filename for later retrieval, and initialize the database system,
     * - For each SQL parameterized file in the list, will try to execute it,
     * - Run any pending migration on top of the project, based on the current database version and available SQL migration files,
     * - Initialize database models (Global Properties, Document Types, Sample Images, Trained ANNs),
     * - Will iterate through required plugins and ensure none of them is missing or incompatible,
     * - Broadcast the "project opened" event to the attached listeners.
     * Once the project has successfully been opened, many of the UI behavior and state will change.
     * \param[in] path is the full path from which to read the RELibraryAdmin project database file.
     * \param[in] sqlFiles is a list of SQL files to use prior to migrating the project.
     */
	void								openProject						(QString const &path, QStringList const &sqlFiles=QStringList());
    /*! @brief Client Binary Generation Method.
     * This method is the creation logic for Client Binary files, based on a Client Binary Parameters object.
     * It will in turn:
     * - Call the Licensing Manager System and declare a Cheesecake object,
     * - Create an empty Client Binary shell (RE::T::ClientBinaryData).
     * - Set the binary with its Versions (Binary Version, Generator Version and Library Version),
     * - Set the binary with its Human-readable Name, Copyright and ANN Failure Threshold value,
     * - Fill-up the ANN training data,
     * - Prepare a global Script object containing the Global Script Definitions,
     * - Then, for each exportable Document Type, it will:
     *   - Export the Document Type's global Filter Stack definition,
     *   - Export the marked Recognizers data and filters stack,
     *   - Export the Document Type's informations (Name, Code, MICR and Regexps, Scripts, etc).
     *   - Check unicity of the MICR regexp for each Document Type.
     * - Generate a minified version of the Script,
     * - Attach the ANN data to the binary.
     * @see REA::ClientBinaryParameters, RE::T::ClientBinaryData
     * @param[in] cbp is a Client Binary Parameters object to follow while generating the binary.
     */
    void								generateClientBinary			(ClientBinaryParameters const &cbp) const;
    /*! @brief Project Closing Method.
     * This method will close any opened project, and will in turn:
     * - Clear the stored Project Filename,
     * - Broadcast a "Project Closed" event to any attached subscriber,
     * - Nullify Model Proxies (Document Types, Sample Images, Trained ANNs),
     * - If any, delete the database models (Global Properties, Document Types, Sample Images, Trained ANNs),
     * - Finally close the database since destroying models commited any pending transaction.
     */
	void								closeProject					();
    //! @brief Current Project Filename Getter.
	QString const&						projectFilename					() const;
    //! @brief Current Project Directory Getter.
    QDir                                projectDirectory                () const;
    //! @brief Current Project Required Plugins Getter.
	QStringList							projectRequiredPlugins			() const;

    //! @brief New Sample Image Adder Method.
    void								addSampleImages					(quint32 dtId, QStringList const &filenames);
    //! @brief Sample Image Signature Computation Method.
    void								signSampleImages				(QModelIndexList const &rows);
    //! @brief Next Pending Sample Image Index for Signing Getter.
    QModelIndex							nextPendingSampleImageIndex		();

    //! @brief ANN Training Start Method.
	void								annStartTraining				(RE::T::UIntList const &dtIds, double targetMse, double learningRate, double activationStepness, quint32 hiddenNeuronsCount, quint32 maxEpochs, quint32 reportsFrequency);
    //! @brief ANN Testing Start Method.
    void								annStartTesting					(RE::T::UIntList const &dtIds, QByteArray const &annData);
    //! @brief ANN Training / Testing Killer Method.
    void								annStopRunning					();

protected slots:
    //! @brief Triggered when a Signer Thread has Processed an Image.
	void								sampleImagesSigner_processed	(QModelIndex idx, QByteArray sig);
    //! @brief Triggered when a Signer Thread has Exhausted its Queue.
    void								sampleImageSigner_finished		();
    //! @brief Triggered when an ANN Training Thread has Started.
    void								annRunner_trainingStarted		();
    //! @brief Triggered when an ANN Training Thread has Updated its Training Report.
    void								annRunner_trainingReport		(quint32 epoch, double mse);
    //! @brief Triggered when an ANN Training Thread has Finished Training.
    void								annRunner_trainingFinished		(bool success, QByteArray annData);
    //! @brief Triggered when an ANN Testing Thread has Started.
    void								annRunner_testingStarted		(RE::T::UIntToStringHash documentTypesList);
    //! @brief Triggered when an ANN Testing Thread has Updated its Testing Report.
    void								annRunner_testingReport			(quint32 expectedType, RE::T::UIntToDoubleHash results);
    //! @brief Triggered when an ANN Testing Thread has Finished Testing.
    void								annRunner_testingFinished		();

private:
    //! @brief Application-Wide REA::Core (Singleton) Instance.
    static REA::Core					*_Instance;
    //! @brief Application-Wide RE::Engine Instance.
    RE::Engine							*_re;
    //! @brief Currently Opened Project Filename.
    QString								_projectFilename;

    //! @brief Application-Wide LM::Core Instance.
    LM::Core                            *_lm;
    //! @brief Licensed Status Flag Cache.
    bool                                _licensed;
    //! @brief Current Machine's Host UUID Cache.
    QString                             _hostUuid;
    //! @brief Licensing Manager Obfuscated Cheesecake Cache.
    quint8                              *_obfCheesecake;

    //! @brief Project's Database Object.
    QSqlDatabase						_database;

    //! @brief Project's Global Properties Model.
    GlobalPropertiesModel				*_globalPropertiesModel;
    //! @brief Project's Document Types Model.
    DocumentTypesModel					*_documentTypesModel;
    //! @brief Project's Sample Images Model.
    SampleImagesModel					*_sampleImagesModel;
    //! @brief Project's Trained ANNs Model.
    TrainedAnnsModel					*_trainedAnnsModel;
    //! @brief Model Proxies for Document Types, Sample Images and Trained ANNs.
    QProxyModel							*_documentTypesModelProxy,
										*_sampleImagesModelProxy,
										*_trainedAnnsModelProxy;

    //! @brief To-Be-Added Sample Image Queue.
    SampleImageInfo::List				_sampleImagesToAdd;
    //! @brief To-Be-Signed Sample Images Queue.
    QModelIndexList						_sampleImagesToSign;
    //! @brief Database Lock and Threads Lock.
	mutable QMutex						_databaseMutex, _workerMutex;
    //! @brief Number of Instanciated Image Signer Threads.
    quint32								_signerCount;
    //! @brief Number of ANNs Trainer Threads Count.
    quint32								_trainerCount;
    //! @brief Number of ANNs Tester Threads Count.
    quint32								_testerCount;
    //! @brief Main ANN Runner Thread.
    AnnRunner							*_annRunner;

signals:
    //! @brief Emited upon Project Opening.
    void								projectOpened					();
    //! @brief Emited upon Project Closing.
    void								projectClosed					();

    //! @brief Emited once a Sample Image Signer Thread has Started.
    void								sampleImagesProcessingStarted	(int);
    //! @brief Emited every time a Sample Image Signed has Processed a Single Image.
    void								sampleImageProcessed			();
    //! @brief Emited once a Sample Image Signer Thread has Exhausted its Queue.
    void								sampleImagesProcessingFinished	();

    //! @brief Emited once an ANN Training Thread has Started.
    void								annTrainingStarted				();
    //! @brief Emited every time an ANN Training Thread Updates its Report.
    void								annTrainingReport				(qint32 epoch, double error);
    //! @brief Emited once an ANN Training Thread has Finished Running.
    void								annTrainingFinished				(bool success, QByteArray data);

    //! @brief Emited once an ANN Testing Thread has Started.
    void								annTestingStarted				(RE::T::UIntToStringHash codesList);
    //! @brief Emited every time an ANN Testing Thread Updates its Report.
    void								annTestingReport				(quint32 expectedType, RE::T::UIntToDoubleHash results);
    //! @brief Emited once an ANN Testing Thread has Finished Running.
    void								annTestingFinished				();
};

}

#endif
