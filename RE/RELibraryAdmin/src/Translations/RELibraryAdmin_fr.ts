<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>DlgAbout</name>
    <message>
        <location filename="../../Forms/DlgAbout.ui" line="20"/>
        <source>About...</source>
        <translation>A Propos...</translation>
    </message>
    <message>
        <location filename="../../Forms/DlgAbout.ui" line="35"/>
        <source>RELibraryAdmin Version:</source>
        <translation>Version de RELibraryAdmin:</translation>
    </message>
    <message>
        <location filename="../../Forms/DlgAbout.ui" line="52"/>
        <source>RELibrary Version</source>
        <translation>Version de RELibrary</translation>
    </message>
    <message>
        <location filename="../../Forms/DlgAbout.ui" line="75"/>
        <source>Plugins Versions</source>
        <translation>Version des Plugins</translation>
    </message>
</context>
<context>
    <name>ItemDelegates::Signature</name>
    <message>
        <source>Not yet computed.</source>
        <translation type="obsolete">Pas encore calculé.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="44"/>
        <source>Licensing Issue</source>
        <translation>Problème de License</translation>
    </message>
</context>
<context>
    <name>REA::AnnTestingResultsModel</name>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="37"/>
        <source>Successes</source>
        <translation>Reussites</translation>
    </message>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="38"/>
        <source>Failures</source>
        <translation>Echecs</translation>
    </message>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="39"/>
        <source>Ratio</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="40"/>
        <source>Highest</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="41"/>
        <source>Lowest</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="42"/>
        <source>Average</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="116"/>
        <source>%1 x %2 (%3 to %4)</source>
        <translation>%1 x %2 (%3 à %4)</translation>
    </message>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="43"/>
        <source>Misstaken with</source>
        <translation>Confondu avec</translation>
    </message>
    <message>
        <location filename="../Models/AnnTestingResultsModel.cpp" line="66"/>
        <source>No Data</source>
        <translation>Pas de Données</translation>
    </message>
</context>
<context>
    <name>REA::ClientBinaryGenerator</name>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="565"/>
        <source>Client Binary Generation Wizard</source>
        <translation>Assistant de Génération du Binaire Client</translation>
    </message>
</context>
<context>
    <name>REA::ClientBinaryGenerator::AnnSelectionPage</name>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="115"/>
        <source>ANN Selection</source>
        <translation>Selection du RNA</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="116"/>
        <source>Please choose from the list which ANN you would like to use in the Client Binary.</source>
        <translation>Veuillez choisir dans la liste quel RNA vous souhaitez utiliser dans le Binaire Client.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="132"/>
        <source>ANN Threshold</source>
        <translation>Seuil du RNA</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="143"/>
        <source>Only export one single Document Type.</source>
        <translation>N&apos;exporter qu&apos;un Type de Document.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="147"/>
        <source>No ANN selected.</source>
        <translation>Aucun RNA selectionné.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="199"/>
        <source>There are no recognizable document types for this ANN.</source>
        <translation>Aucun Type de Document n&apos;est associé a ce RNA.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="201"/>
        <source>The selected ANN is not trained.</source>
        <translation>Le RNA selectionné n&apos;a pas été entrainé.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="220"/>
        <source>%1: %2 doesn&apos;t have a name.</source>
        <translation>%1: %2 n&apos;a pas de nom.</translation>
    </message>
    <message numerus="yes">
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="236"/>
        <source>%1: %n object(s)</source>
        <translation>
            <numerusform>%1: %n objet</numerusform>
            <numerusform>%1: %n objets</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="237"/>
        <source>Total: %n object(s)</source>
        <translation>
            <numerusform>Total: %n objets</numerusform>
            <numerusform>Total: %n objets</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>REA::ClientBinaryGenerator::FinalPage</name>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="461"/>
        <source>Tweaking and Validation</source>
        <translation>Réglages et Validation</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="462"/>
        <source>Please review the options you have choosed below, and make sure everything is correct. The next step will finalize the Client Binary generation.</source>
        <translation>Merci de bien vouloir vérifier les options choisies ci-dessous. L&apos;étape suivante finalisera la création du Binaire Client.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="500"/>
        <source>The Client Binary will be generated with the following settings:

Path: %0

Name: %1
Copyright: %2

Base ANN: %3
Total Document Types: %4
Total Recognizers: %5</source>
        <translation>Le Binaire Client sera généré avec les paramètres suivants:

Chemin: %0

Nom: %1
Droits: %2

RNA de Base: %3
Total Types de Documents: %4
Total Reconnaisseurs: %5</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="527"/>
        <source>The Client Binary will be generated with the following settings:

Path: %0

Name: %1
Copyright: %2

Document Type: %3
Total Recognizers: %4</source>
        <translation>Le Binaire Client sera généré avec les paramètres suivants:

Chemin: %0

Nom: %1
Droits: %2

Type de Document: %3
Total Reconnaisseurs: %4</translation>
    </message>
</context>
<context>
    <name>REA::ClientBinaryGenerator::IntroductionPage</name>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="34"/>
        <source>Introduction</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="35"/>
        <source>This wizard will help you to fullfill the informations required for generating a Client Binary.</source>
        <translation>Cet assistant vous guidera dans le renseignement des informations nécessaires a la géneration d&apos;un Binaire Client.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="42"/>
        <source>You will first need to select what ANN to use for this Client Binary. Then you will then have to pick from the Zone Recognizers you want to make available in the Client Binary. Finally, the Client Binary will be generated and dependencies will be packed into one single file you will then be able to easily deploy. You may start by entering a name and a copyright for below.</source>
        <translation>Vous devrez tout d&apos;abord séléctionner quel RNA utiliser pour ce Binaire Client. Puis, vous devrez choisir quels Reconnaisseurs vous voulez rendre disponibles dans le Binaire Client. Pour finir, le Binaire Client sera généré dans un fichier facilement déployable. Veuillez commencer par entrer un nom et les droits d&apos;auteur ci-dessous.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="50"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="64"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="71"/>
        <source>Copyright</source>
        <translation>Droits d&apos;Auteur</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="95"/>
        <source>Save Client Binary...</source>
        <translation>Enregistrer le Binaire Client...</translation>
    </message>
</context>
<context>
    <name>REA::ClientBinaryGenerator::RecognizersSelectionPage</name>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="268"/>
        <source>Recognizers Selection</source>
        <translation>Selection des Reconnaisseurs</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="269"/>
        <source>Please choose from the list which Recognizers you would like to use in the Client Binary.</source>
        <translation>Veuillez choisir quels Reconnaisseurs exporter dans le Binaire Client.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="287"/>
        <source>No ANN selected.</source>
        <translation>Aucun RNA séléctionné.</translation>
    </message>
    <message>
        <location filename="../UI/Wizards/ClientBinaryGenerator.cpp" line="426"/>
        <source>You must select at least one recognizer to be exported in the Client Binary to be able to proceed to the next step.</source>
        <translation>Vous de vez choisir au moins un Reconnaisseur à exporter dans le Binaire Client pour pouvoir poursuivre.</translation>
    </message>
</context>
<context>
    <name>REA::DocumentTypesModel</name>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="29"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="30"/>
        <source>Parent Key</source>
        <translation>Clé Parente</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="31"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="32"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="34"/>
        <source>MICR RegExp</source>
        <translation>Expr. Reg. MICR</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="40"/>
        <source>Preprocessing Filters Data</source>
        <translation>Données de Pré-Traitement</translation>
    </message>
    <message>
        <source>Item #%1 - Status:
%2</source>
        <translation type="obsolete">Objet #%1 - Status:
%2</translation>
    </message>
    <message>
        <source>// Subtype selection script.
// This script should return a code matching a document subtype within this document type.
</source>
        <translation type="obsolete">// Script de Sélection du Sous-Type
// Ce script doit retourner un code correspondant a un code de sous-type de document.</translation>
    </message>
    <message>
        <source>// Rejection tests script.
// Access to strings, acceptationReasons and rejectionReasons is permited.
</source>
        <translation type="obsolete">// Script de Tests de Rejets.
// L’accès aux objets strings, acceptationReasons et rejectionReasons est permis.</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="33"/>
        <source>No ANN Training</source>
        <translation>Ne Pas Entrainer</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="36"/>
        <source>Sub-Type Selection Script</source>
        <translation>Script de Séléction de Sous-Type</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="37"/>
        <source>Sub-Type Selection Simulation Strings</source>
        <translation>Chaînes de Simulation de Sous-Type</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="38"/>
        <source>Rejection Tests Scripts</source>
        <translation>Scripts de Tests de Rejets</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="39"/>
        <source>Rejection Tests Simulation Strings</source>
        <translation>Chaînes de Simulation de Tests de Rejets</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="202"/>
        <source>New Document Type</source>
        <translation>Nouveau Type de Document</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="35"/>
        <source>Bad MICR Rejection</source>
        <translation>Rejet si MICR Non-Conforme</translation>
    </message>
    <message>
        <source>Rejection Tests Script</source>
        <translation type="obsolete">Script de Tests de Rejet</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="41"/>
        <source>Recognition Data</source>
        <translation>Données de Reconnaissance</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="42"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="143"/>
        <source>You may want to add a few sample images to this type to optimally train the ANN on it.</source>
        <translation>Vous devriez ajouter des échantillons d&apos;images à ce type pour entrainer le RNA de façon optimalle sur celui-ci.</translation>
    </message>
    <message>
        <location filename="../Models/DocumentTypesModel.cpp" line="143"/>
        <source>Sane.</source>
        <translation>Sain.</translation>
    </message>
</context>
<context>
    <name>REA::GlobalPropertiesModel</name>
    <message>
        <location filename="../Models/GlobalPropertiesModel.cpp" line="16"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="../Models/GlobalPropertiesModel.cpp" line="17"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>REA::SampleImagesModel</name>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="17"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="18"/>
        <source>Document Type</source>
        <translation>Type de Document</translation>
    </message>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="19"/>
        <source>Filename</source>
        <translation>Nom de Fichier</translation>
    </message>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="21"/>
        <source>Signature</source>
        <translation>Signature</translation>
    </message>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="60"/>
        <source>Item #%1 - Status:
%2</source>
        <translation>Objet #%1 - Status:
%2</translation>
    </message>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="20"/>
        <source>Train.</source>
        <translation>Entrain.</translation>
    </message>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="57"/>
        <source>The specified file doesn&apos;t exist.</source>
        <translation>Le fichier spéficifié n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="59"/>
        <source>Signature hasn&apos;t been computed yet.</source>
        <translation>La signature n&apos;a pas encore été calculée.</translation>
    </message>
    <message>
        <location filename="../Models/SampleImagesModel.cpp" line="60"/>
        <source>Sane.</source>
        <translation>Sain.</translation>
    </message>
</context>
<context>
    <name>REA::Signature</name>
    <message>
        <location filename="../UI/EditorsAndDelegates/ItemDelegates.cpp" line="88"/>
        <source>Not yet computed.</source>
        <translation>Pas encore calculé.</translation>
    </message>
</context>
<context>
    <name>REA::TrainedAnnsModel</name>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="15"/>
        <source>Key</source>
        <translation>Cle</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="16"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="17"/>
        <source>Target MSE</source>
        <translation>MSE desire</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="18"/>
        <source>Act. Step.</source>
        <translation>Seuil Act.</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="19"/>
        <source>Learn. Rate</source>
        <translation>Taux d&apos;Apprentissage</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="20"/>
        <source>Hid. Neur.</source>
        <translation>Neur. Cach.</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="21"/>
        <source>Max. Epoch</source>
        <translation>Max. Epochs</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="22"/>
        <source>Reports Freq.</source>
        <translation>Freq des Rapts.</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="23"/>
        <source>Selection</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="24"/>
        <source>Data</source>
        <translation>Donnees</translation>
    </message>
    <message>
        <location filename="../Models/TrainedAnnsModel.cpp" line="25"/>
        <source>S</source>
        <translation>S</translation>
    </message>
</context>
<context>
    <name>REA::WgtAnnsManager</name>
    <message>
        <location filename="../UI/WgtAnnsManager.cpp" line="106"/>
        <location filename="../UI/WgtAnnsManager.cpp" line="111"/>
        <source>Start</source>
        <translation>Demarrer</translation>
    </message>
    <message>
        <location filename="../UI/WgtAnnsManager.cpp" line="106"/>
        <location filename="../UI/WgtAnnsManager.cpp" line="111"/>
        <source>Stop</source>
        <translation>Arretter</translation>
    </message>
    <message>
        <location filename="../UI/WgtAnnsManager.cpp" line="220"/>
        <source>New ANN</source>
        <translation>Nouveau RNA</translation>
    </message>
</context>
<context>
    <name>REA::WgtDocumentTypesManager</name>
    <message>
        <location filename="../UI/WgtDocumentTypesManager.cpp" line="96"/>
        <source>Total: %1 - Trainable: %2 - Runnable: %3</source>
        <translation>Total: %1 - Entrainable: %2 - Executable: %3</translation>
    </message>
    <message>
        <source>New Document Type</source>
        <translation type="obsolete">Nouveau Type de Document</translation>
    </message>
    <message>
        <location filename="../UI/WgtDocumentTypesManager.cpp" line="275"/>
        <source>Choose file(s) to open...</source>
        <translation>Choisissez le(s) fichier(s) à ouvrir...</translation>
    </message>
</context>
<context>
    <name>REA::WgtZoneRecognizersManager</name>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Attention</translation>
    </message>
    <message>
        <source>This can be slow... Continue?</source>
        <translation type="obsolete">Ceci peut-être lent. Continuer?</translation>
    </message>
    <message>
        <location filename="../UI/WgtZoneRecognizersManager.cpp" line="262"/>
        <source>Directory for Batch Saving...</source>
        <translation>Répertoire de Sauvegarde Batch...</translation>
    </message>
    <message>
        <location filename="../UI/WgtZoneRecognizersManager.cpp" line="295"/>
        <source>No results returned from recognizer</source>
        <translation>Pas de resultat retourne par le reconnaisseur</translation>
    </message>
    <message>
        <location filename="../UI/WgtZoneRecognizersManager.cpp" line="331"/>
        <source>Viewing Image %1 of %2 (%3).</source>
        <translation>Visionage de l&apos;Image %1 sur %2 (%3).</translation>
    </message>
    <message>
        <location filename="../UI/WgtZoneRecognizersManager.cpp" line="333"/>
        <source>Invalid Document Type selection, make sure only one is selected.</source>
        <translation>Selection de Type de Document invalide, veuillez seléctionner un Type de Document.</translation>
    </message>
</context>
<context>
    <name>REA::WndMain</name>
    <message>
        <location filename="../UI/WndMain.cpp" line="192"/>
        <source>New Project...</source>
        <translation>Nouveau Projet...</translation>
    </message>
    <message>
        <location filename="../UI/WndMain.cpp" line="205"/>
        <source>Open Project...</source>
        <translation>Ouvrir le Projet...</translation>
    </message>
    <message>
        <source>#</source>
        <translation type="obsolete">#</translation>
    </message>
    <message>
        <location filename="../UI/WndMain.cpp" line="226"/>
        <source>Sender</source>
        <translation>Eméteur</translation>
    </message>
    <message>
        <source>Method</source>
        <translation type="obsolete">Méthode</translation>
    </message>
    <message>
        <location filename="../UI/WndMain.cpp" line="226"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
</context>
<context>
    <name>REA::ZoneRecognizerItem</name>
    <message>
        <location filename="../UI/ZoneRecognizersScene/ZoneRecognizerItem.cpp" line="14"/>
        <source>Unknown Recognizer</source>
        <translation>Reconaisseur Inconnu</translation>
    </message>
</context>
<context>
    <name>REA::ZoneRecognizersScene</name>
    <message numerus="yes">
        <source>%n filter(s)</source>
        <translation type="obsolete">
            <numerusform>%n filtre</numerusform>
            <numerusform>%n filtres</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../UI/ZoneRecognizersScene/ZoneRecognizersScene.cpp" line="87"/>
        <source>%n filters</source>
        <comment>filter</comment>
        <translation>
            <numerusform>%n filtre</numerusform>
            <numerusform>%n filtres</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>WgtAnnsManager</name>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="191"/>
        <source>Training</source>
        <translation>Entrainement</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="463"/>
        <source>Max. Epochs</source>
        <translation>Max. Cycles</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="523"/>
        <source>A number of Epochs on which to report the progression of the ANN training.</source>
        <translation>Le nombre de cycle avant d&apos;effectuer chaque rapport.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="507"/>
        <source>Report Frequency</source>
        <translation>Frequence des Rapports</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="409"/>
        <source>Hidden Neurons</source>
        <translation>Neurones Cachés</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="309"/>
        <source>Learning Rate</source>
        <translation>Taux d&apos;Apprentissage</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="21"/>
        <source>ANNs Manager</source>
        <translation>Gestionaire de RNA</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="24"/>
        <source>Offers options to train and test type recognition ANNs.</source>
        <translation>Offre les options d&apos;entrainement et de test pour les RNA.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="82"/>
        <source>List of previously saved ANNs.</source>
        <translation>Liste de RNA précédemment sauvegardés.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="143"/>
        <source>Create a new ANN with default values.</source>
        <translation>Créée un nouveau RNA avec les valeurs par défaut.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="154"/>
        <source>Delete the selected ANN.</source>
        <translation>Supprime le RNA séléctionné.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="168"/>
        <source>Allows choice between training and testing the selected ANN.</source>
        <translation>Permet de choisir entre entrainement et test du RNA.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="184"/>
        <source>The training tab of this panel allows you to finelly train new or previously saved ANNs.</source>
        <translation>L&apos;onglet d&apos;entrainement permet d&apos;entrainer les RNAs.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="223"/>
        <source>Allows a certain amount of parameters to be set for training the selected ANN.</source>
        <translation>Permet le reglage d&apos;un certain nombre de parametres pour entrainer le RNA.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="256"/>
        <location filename="../../Forms/WgtAnnsManager.ui" line="275"/>
        <source>Define the target error to reach before stopping the training.</source>
        <translation>Definit le taux d&apos;erreur a atteindre avant d&apos;arréter l&apos;entrainement.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="259"/>
        <source>Target MSE</source>
        <translation>Taux d&apos;Erreur Cible</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="306"/>
        <location filename="../../Forms/WgtAnnsManager.ui" line="325"/>
        <source>Define the learning rate for the ANN. A higher value will make it learn faster, while a lower value will make it more efficient.</source>
        <translation>Definit le taux d&apos;apprentissage du RNA. Un taux plus élevé rendra l&apos;entrainement plus rapide au détriment des resultats.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="356"/>
        <location filename="../../Forms/WgtAnnsManager.ui" line="375"/>
        <source>Change the weight multiplier applied to each connexion. Dont alter this field unless you know what you&apos;re doing.</source>
        <translation>Change le multiplicateur de poids pour chaque connexion de neurone. Ne pas modifier sauf si vous savez exactement pourquoi.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="359"/>
        <source>Activation Stepness</source>
        <translation>Seuil d&apos;Activation</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="406"/>
        <location filename="../../Forms/WgtAnnsManager.ui" line="425"/>
        <source>The number of hidden neurons to place on the intermediate layer.</source>
        <translation>Le nombre de neurones cachés dans le calque intermédiaire.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="460"/>
        <location filename="../../Forms/WgtAnnsManager.ui" line="479"/>
        <source>Maximum epoch cycle to train the ANN before automatically cancelling.</source>
        <translation>Nombre maximum de cycles d&apos;entrainement avant arret.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="504"/>
        <source>This parameter defines the frequency on which to place a point on the graph.</source>
        <translation>Ce parametre définit la frequence des rapports a placer sur le graphique.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="589"/>
        <source>Displays real-time informations about the ANN being trained.</source>
        <translation>Affiche les informations d&apos;entrainement en temps réel.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="616"/>
        <source>Revert the training parameters to their previously saved values.</source>
        <translation>Rétablir les paramètres d&apos;entrainement à leurs valeurs enregistrées.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="619"/>
        <source>Revert</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="630"/>
        <source>Save the parameters and the ANN if it has been trained.</source>
        <translation>Enregistrer les paramètres du RNA.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="633"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="651"/>
        <source>Start training the ANN with the current parameters.</source>
        <translation>Démarrer l&apos;entrainement du RNA avec les paramètres courrants.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="654"/>
        <location filename="../../Forms/WgtAnnsManager.ui" line="754"/>
        <source>Start</source>
        <translation>Demarrer</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="668"/>
        <source>The testing tab allows you to make sure the saved ANNs match your criteria.</source>
        <translation>L&apos;onglet permet de vérifier que le RNA est entrainé correctement sur vos critères.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="696"/>
        <source>This table displays the results of the ANN being run against the unchecked sample images.</source>
        <translation>Ce tableau affiche les résultats des tests du RNA sur les images seléctionnées.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="751"/>
        <source>Start running the ANN against the unchecked sample images.</source>
        <translation>Demarer le test du RNA sur les images seléctionnées.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtAnnsManager.ui" line="675"/>
        <source>Testing</source>
        <translation>Test</translation>
    </message>
</context>
<context>
    <name>WgtCVMListEditor</name>
    <message>
        <location filename="../../Forms/WgtCVMListEditor.ui" line="14"/>
        <source>Convolution Matrices List Editor</source>
        <translation>Editeur de Matrices de Convolution</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtCVMListEditor.ui" line="54"/>
        <source>Revert the training parameters to their previously saved values.</source>
        <translation>Rétablir les paramètres sauvegardés.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtCVMListEditor.ui" line="57"/>
        <source>Revert</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtCVMListEditor.ui" line="68"/>
        <source>Save the parameters and the ANN if it has been trained.</source>
        <translation>Enregistrer les paramètres et le RNA si il a été entrainé.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtCVMListEditor.ui" line="71"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
</context>
<context>
    <name>WgtColorEditor</name>
    <message>
        <location filename="../../Forms/WgtColorEditor.ui" line="14"/>
        <source>Gray Value Editor</source>
        <translation>Editeur de Niveau de Gris</translation>
    </message>
</context>
<context>
    <name>WgtDocumentTypesManager</name>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="20"/>
        <source>Document Types Manager</source>
        <translation>Gestionnaire de Types de Documents</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="66"/>
        <source>Configurable Types.</source>
        <translation>Types Configurables.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="109"/>
        <source>Create a Type.</source>
        <translation>Créer un Type.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="120"/>
        <source>Delete selected Type.</source>
        <translation>Supprimer le Type séléctionné.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="190"/>
        <source>MICR RegExp</source>
        <translation>Exp. Reg. MICR</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="180"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <source>Data and Scripts</source>
        <translation type="obsolete">Données et Scripts</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="200"/>
        <source>No ANN Training</source>
        <translation>Ne Pas Entrainer</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="214"/>
        <source>MICR Mismatch Rejection</source>
        <translation>Rejet si MICR Non-Conforme</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="309"/>
        <source>Rejection Tests Script</source>
        <translation>Script de Tests de Rejet</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="336"/>
        <source>Rejection Tests Script Editor</source>
        <translation>Editeur du Script de Tests de Rejet</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="292"/>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="367"/>
        <source>Validate Script</source>
        <translation>Valider Script</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="151"/>
        <source>Root Properties</source>
        <translation>Propriétés de Base</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="234"/>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="261"/>
        <source>Subtype Selection Script</source>
        <translation>Script de Séléction de Sous-Type</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="392"/>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="413"/>
        <source>Sample Images</source>
        <translation>Echantillons d&apos;Images</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="471"/>
        <source>Check</source>
        <translation>Cocher</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="482"/>
        <source>Uncheck</source>
        <translation>Décocher</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="500"/>
        <source>Attach sample image(s).</source>
        <translation>Attacher échantillon(s) d&apos;image(s).</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="511"/>
        <source>Computes signature(s) for the selected sample image(s).</source>
        <translation>Calculer signatures pour les échantillons d&apos;images séléctionnés.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="522"/>
        <source>Delete selected sample image(s).</source>
        <translation>Supprimer les échantillons d&apos;images séléctionnés.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="540"/>
        <source>Recognition</source>
        <translation>Reconnaissance</translation>
    </message>
    <message>
        <source>Recognition Zones</source>
        <translation type="obsolete">Zones d&apos;Extractions</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtDocumentTypesManager.ui" line="569"/>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
</context>
<context>
    <name>WgtFillColorEditor</name>
    <message>
        <location filename="../../Forms/WgtFillColorEditor.ui" line="14"/>
        <source>Fill Color Editor</source>
        <translation>Editeur de Couleur de Remplissage</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtFillColorEditor.ui" line="26"/>
        <source>Use Source Image</source>
        <translation>Utiliser l&apos;Image Source</translation>
    </message>
</context>
<context>
    <name>WgtFilterStackEditor</name>
    <message>
        <location filename="../../Forms/WgtFilterStackEditor.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtFilterStackEditor.ui" line="71"/>
        <source>Revert the training parameters to their previously saved values.</source>
        <translation>Revenir aux valeurs précédemment enregistrées.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtFilterStackEditor.ui" line="74"/>
        <source>Revert</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtFilterStackEditor.ui" line="85"/>
        <source>Save the parameters and the ANN if it has been trained.</source>
        <translation>Enregistrer les paramètres et le RNA s’il a été entrainé.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtFilterStackEditor.ui" line="88"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
</context>
<context>
    <name>WgtGlobalProperties</name>
    <message>
        <location filename="../../Forms/WgtGlobalProperties.ui" line="32"/>
        <source>Project Properties</source>
        <translation>Propriétés du Projet</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtGlobalProperties.ui" line="56"/>
        <source>Default Copyright</source>
        <translation>Droits d&apos;Auteur par Défaut</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtGlobalProperties.ui" line="66"/>
        <source>Database Version</source>
        <translation>Version BDD</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtGlobalProperties.ui" line="85"/>
        <source>Global Script</source>
        <translation>Script Global</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtGlobalProperties.ui" line="120"/>
        <source>Rejection Tests Script Editor</source>
        <translation>Editeur du Script de Tests de Rejet</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtGlobalProperties.ui" line="151"/>
        <source>Validate Script</source>
        <translation>Valider Script</translation>
    </message>
</context>
<context>
    <name>WgtGrayValueEditor</name>
    <message>
        <location filename="../../Forms/WgtGrayValueEditor.ui" line="14"/>
        <source>Gray Value Editor</source>
        <translation>Editeur de Niveau de Gris</translation>
    </message>
</context>
<context>
    <name>WgtZoneRecognizersManager</name>
    <message>
        <source>Recognition Zones Manager</source>
        <translation type="obsolete">Gestionnaire de Zones de Reconnaissance</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="73"/>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="392"/>
        <source>Recognizer</source>
        <translation>Reconnaisseur</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="93"/>
        <source>VPD</source>
        <translation>DPV</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="134"/>
        <source>Check</source>
        <translation>Cocher</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="152"/>
        <source>Fit to View</source>
        <translation>Ajuster a la Vue</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="166"/>
        <source>Zoom Out</source>
        <translation>Zoom Arrière</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="177"/>
        <source>Zoon In</source>
        <translation>Zoom Avant</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="278"/>
        <source>Preprocessing</source>
        <translation>Pré-Traitement</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="434"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="549"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="591"/>
        <source>Raw</source>
        <translation>Brut</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="323"/>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="486"/>
        <source>Filters</source>
        <translation>Filtres</translation>
    </message>
    <message>
        <source>Revert the training parameters to their previously saved values.</source>
        <translation type="obsolete">Rétablir les paramètres d&apos;entrainement aux valeurs enregistrées.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="240"/>
        <source>Revert</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <source>Save the parameters and the ANN if it has been trained.</source>
        <translation type="obsolete">Enregistrer les paramètres du RNA et son entrainement.</translation>
    </message>
    <message>
        <location filename="../../Forms/WgtZoneRecognizersManager.ui" line="251"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Rafraichir</translation>
    </message>
</context>
<context>
    <name>WndMain</name>
    <message>
        <location filename="../../Forms/WndMain.ui" line="14"/>
        <source>RELibraryAdmin</source>
        <translation>RELibraryAdmin</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="81"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="85"/>
        <source>Recent</source>
        <translation>Recent</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="88"/>
        <location filename="../../Forms/WndMain.ui" line="91"/>
        <source>Browse for recently used Projects.</source>
        <translation>Parcourir les projets recemment utilises.</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="94"/>
        <source>&amp;Recent</source>
        <translation>&amp;Récent</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="111"/>
        <location filename="../../Forms/WndMain.ui" line="120"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="114"/>
        <location filename="../../Forms/WndMain.ui" line="117"/>
        <source>Allows to change the view.</source>
        <translation>Permet le changement de vue.</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="129"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="143"/>
        <location filename="../../Forms/WndMain.ui" line="405"/>
        <source>Console</source>
        <translation>Console</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="296"/>
        <source>Clear</source>
        <translation>Nettoyer</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="366"/>
        <source>Artificial Neural Networks</source>
        <translation>Réseaux de Nerones Artificiels</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="390"/>
        <source>Generate...</source>
        <translation>Generer...</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="414"/>
        <source>About...</source>
        <translation>A Propos...</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="423"/>
        <source>Global Properties</source>
        <translation>Propriétés Globales</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="381"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="348"/>
        <source>Document Types</source>
        <translation>Types de Documents</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="333"/>
        <location filename="../../Forms/WndMain.ui" line="336"/>
        <source>Open an existing Project.</source>
        <translation>Ouvrir un projet existant.</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="351"/>
        <location filename="../../Forms/WndMain.ui" line="354"/>
        <source>Changes the current view to the Document Types management.</source>
        <translation>Change de vue entre les types de documents et les RNA.</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="369"/>
        <location filename="../../Forms/WndMain.ui" line="372"/>
        <source>Changes the current view to the ANN management.</source>
        <translation>Changer de vue pour le gestionnaire de RNA.</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="315"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="318"/>
        <location filename="../../Forms/WndMain.ui" line="321"/>
        <source>Create a new Project.</source>
        <translation>Creer un nouveau projet.</translation>
    </message>
    <message>
        <location filename="../../Forms/WndMain.ui" line="330"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
</context>
</TS>
