#ifndef REA_JSMinifier_h
#define REA_JSMinifier_h

// Required stuff.
#include <QtCore/QString>

namespace REA {

class JSMinifier {
public:
    /**/					JSMinifier					() {}
	QString					minify						(QString const &in) const {
		return doMinify(doMinify(in));
	};
protected:
	QString					doMinify					(QString const &inOrig) const {
		QString			out;
		QString const	in					= inOrig.trimmed();
		quint32			len					= in.length();
		QString const	spaceLessChars		= "[]{}()+-/*=%$<>!|&;,\r\n";
		QString const	spaceChars			= "\r\n\t ";
		QString const	endlChars			= "\r\n";
		bool			inSlashComment		= false;
		bool			inStarComment		= false;
		bool			inSingleQuotes		= false;
		bool			inDoubleQuotes		= false;
		QChar			current,next,prev	= 0;
		for (quint32 i=0; i<len; ++i) {
			current							= in.at(i);
			next							= i==len-1 ? ' ' : in.at(i+1);
			
			// Start of comment: //
			if (!inSingleQuotes && !inDoubleQuotes && !inSlashComment && current=='/' && next=='/') {
				inSlashComment				= true;
				continue;
			}
			// End of comment (ENDL).
			else if (inSlashComment) {
				if (endlChars.contains(current))
					inSlashComment			= false;
				continue;
			}
			
			// Start of comment: /*
			if (!inSingleQuotes && !inDoubleQuotes && !inStarComment && current=='/' && next=='*') {
				inStarComment				= true;
				continue;
			}
			// End of comment: */
			else if (inStarComment) {
				if (prev=='*' && current=='/')
					inStarComment			= false;
				continue;
			}
			
			// Start of a single-quote string litteral.
			if (!inDoubleQuotes && current=='\'')
				inSingleQuotes				= true;
			// End of a single-quote string litteral.
			else if (inSingleQuotes && current=='\'' && prev!='\\')
				inSingleQuotes				= false;
			
			// Start of a double-quote string litteral.
			else if (!inSingleQuotes && current=='"')
				inDoubleQuotes				= true;
			// End of a double-quote string litteral.
			else if (inDoubleQuotes && current=='"' && prev!='\\')
				inDoubleQuotes				= false;
			
			// If current is a space and if not in a string litteral.
			if (!inSingleQuotes && !inDoubleQuotes && spaceChars.contains(next) && i<len-1) {
				// Loop until next character isn't a space.
				while (i<len && spaceChars.contains(next)) {
					i						++;
					next					= in.at(i+1);
				}
				// Chatacter doesn't require a space?
				if (spaceLessChars.contains(current) || spaceLessChars.contains(next)) {
					out						+= current;
					prev					= current;
				}
				else {
					out						+= current+' ';
					prev					= ' ';
				}
			}
			else
				out							+= (prev=current);
		}
		return out;
	};
};

}

#endif
