// Required stuff.
#include "Core.h"
#include "UI/WndMain.h"
#include <LMLibrary/Core>
#include <QtCore/QDir>
#include <QtCore/QTranslator>
#include <QtGui/QStyleFactory>
#include <QtGui/QApplication>
#include <QtGui/QMessageBox>

int main (int argc, char *argv[]) {
    QApplication    a           (argc, argv);
    a.setQuitOnLastWindowClosed (true);

    // Set global application's style.
    a.setStyle                  (QStyleFactory::create("Plastique"));
    QFont           font        ("Helvetica", 10);
    QApplication::setFont       (font);

    // Setup licensing.
    LM::Core        *lm         = new LM::Core(&a);
    lm->loadFromFile(QString("%1/.PQRRSLicense.lic").arg(QDir::homePath()));
    // Instanciate stuff.
    REA::StaticInitializer      ();

    // Install translator.
    QTranslator     trans;
    QString         locale      = QLocale::system().name().left(2).toUpper();
    if (trans.load(QString(":/Translations/RELibraryAdmin_%1.qm").arg(locale)))
        qApp->installTranslator (&trans);

    // Prepare core.
    REA::Core       *c          = new REA::Core(lm, &a);
    REA::WndMain    *w          = 0;
    QMessageBox     *d          = 0;
    // License ok.
    if (c->licensed()) {
        w                       = new REA::WndMain();
        w->show                 ();
        if (argc>1)             c->openProject(argv[1]);
    }
    // License wrong.
    else {
        d                       = new QMessageBox(QMessageBox::Critical, QObject::tr("Licensing Issue"), lm->lastMessage(), QMessageBox::Ok);
        QObject::connect        (d, SIGNAL(buttonClicked(QAbstractButton*)), &a, SLOT(quit()));
        d->show                 ();
    }

    int             ret			= a.exec();
    if (w)                      delete w;
    if (d)                      delete d;
    if (c)                      delete c;
    return                      ret;
}
