// Self.
#include "Core.h"
// Licensing stuff.
#include <LMLibrary/LMTypes>
#include <LMLibrary/Core>
// Qt models and database related things.
#include "Models/GlobalPropertiesModel.h"
#include "Models/DocumentTypesModel.h"
#include "Models/SampleImagesModel.h"
#include "Models/TrainedAnnsModel.h"
#include "Models/AnnTestingResultsModel.h"
#include "JSMinifier.h"
#include "DBMigrator.h"
// Workers.
#include "Workers/AnnRunner.h"
// RE stuff.
#include <RELibrary/Engine>
#include <RELibrary/Filters/AbstractFilter>
#include <RELibrary/Analyzers/Recognizers/AbstractRecognizer>
// Various stuff.
#include <QtCore/QFile>
#include <QtCore/QDate>
#include <QtCore/QFileInfo>
#include <QtCore/QDataStream>
#include <QtCore/QMutexLocker>
#include <QtCore/QDebug>
#include <QtScript/QScriptValue>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtGui/QProxyModel>

REA::Core*	REA::Core::_Instance	= 0;

// Constructor / Destructor.
REA::Core::Core (LM::Core *lm, QObject *p)
: QObject(p), _re(0),
_lm(lm), _licensed(false), _obfCheesecake(0),
_database(QSqlDatabase::addDatabase("QSQLITE")),
_globalPropertiesModel(0), _documentTypesModel(0), _sampleImagesModel(0), _trainedAnnsModel(0),
_documentTypesModelProxy(0), _sampleImagesModelProxy(0), _trainedAnnsModelProxy(0),
_signerCount(0), _trainerCount(0), _testerCount(0), _annRunner(0) {
    // Initialize everything.
    REA::StaticInitializer					();

    // Set up licensing.
    _obfCheesecake                          = new quint8[16];
    _hostUuid                               = LM::Core::hostUuid();
    _licensed                               = _lm->isVerified() && LM::Core::LMObfuscatedCheesecakeForData(_lm, REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);

    // Instanciate recognition engine.
    _re										= new RE::Engine(_lm, this);
    // Prepare models / proxy models.
    _documentTypesModelProxy				= new QProxyModel(this);
    _sampleImagesModelProxy					= new QProxyModel(this);
    _trainedAnnsModelProxy					= new QProxyModel(this);

    // Prepare ann runner.
    _annRunner								= new AnnRunner(this);
    connect(_annRunner, SIGNAL(trainingStarted()), this, SLOT(annRunner_trainingStarted()), Qt::DirectConnection);
    connect(_annRunner, SIGNAL(trainingReport(quint32, double)), this, SLOT(annRunner_trainingReport(quint32, double)), Qt::DirectConnection);
    connect(_annRunner, SIGNAL(trainingFinished(bool, QByteArray)), this, SLOT(annRunner_trainingFinished(bool, QByteArray)), Qt::DirectConnection);
    connect(_annRunner, SIGNAL(testingStarted(RE::T::UIntToStringHash)), this, SLOT(annRunner_testingStarted(RE::T::UIntToStringHash)), Qt::DirectConnection);
    connect(_annRunner, SIGNAL(testingReport(quint32, RE::T::UIntToDoubleHash)), this, SLOT(annRunner_testingReport(quint32, RE::T::UIntToDoubleHash)), Qt::DirectConnection);
    connect(_annRunner, SIGNAL(testingFinished()), this, SLOT(annRunner_testingFinished()), Qt::DirectConnection);

    _Instance                               = this;
}
REA::Core::~Core () {
    if (_projectFilename.count())
        closeProject();
    delete      _re;
    delete      _documentTypesModelProxy;
    delete      _sampleImagesModelProxy;
    delete      _trainedAnnsModelProxy;
    delete[]    _obfCheesecake;
}
REA::Core& REA::Core::Instance () {
    return *_Instance;
}

bool REA::Core::licensed () const {
    return _licensed;
}

RE::Engine& REA::Core::recognitionEngine () {
    DeclareCheesecake       (REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    return *(_re+CKZ01);
}
QSqlDatabase& REA::Core::database () {
    return _database;
}
QAbstractItemModel& REA::Core::globalPropertiesModel () {
    DeclareCheesecake       (REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    return *(_globalPropertiesModel+CKZ02);
}
QAbstractItemModel& REA::Core::documentTypesModel () {
    DeclareCheesecake       (REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    return *(_documentTypesModelProxy+CKZ03);
}
REA::DocumentTypesModel* REA::Core::realDocumentTypesModel () {
    return _documentTypesModel;
}
QAbstractItemModel& REA::Core::sampleImagesModel () {
    DeclareCheesecake       (REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    return *(_sampleImagesModelProxy+CKZ04);
}
REA::SampleImagesModel* REA::Core::realSampleImagesModel () {
    return _sampleImagesModel;
}
QAbstractItemModel& REA::Core::trainedAnnsModel () {
    return *_trainedAnnsModelProxy;
}
REA::TrainedAnnsModel* REA::Core::realTrainedAnnsModel () {
    DeclareCheesecake       (REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    return _trainedAnnsModel+CKZ03;
}

// Flags to know the core activity.
bool REA::Core::signerIddle () const	{
    QMutexLocker		ml					(&_workerMutex);
    return !_signerCount;
}
bool REA::Core::annTrainerIdle () const	{
    QMutexLocker		ml					(&_workerMutex);
    return									!_trainerCount;
}
bool REA::Core::annTesterIdle () const	{
    QMutexLocker		ml					(&_workerMutex);
    return									!_testerCount;
}
bool REA::Core::allProcessesIdle () const	{
    QMutexLocker		ml					(&_workerMutex);
    return !(_signerCount + _trainerCount + _testerCount);
}

// Project management.
void REA::Core::createProject (QString const &path) {
    QFileInfo			fi				(path);
    if (fi.exists()) {
        QFile			f				(path);
        if (!f.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
            Wrn							<< "Unable to truncate file" << fi.baseName() << "!";
            return;
        }
        f.close();
    }
    openProject							(path, QStringList() << ":/Resources/SQL/Creation.sql" << ":/Resources/SQL/Sample.sql");
}
void REA::Core::openProject (QString const &path, QStringList const &sqlFiles) {
    // Close previously open project.
    if (_projectFilename.count())		closeProject();

    // Prepare DB.
    _projectFilename					= path;
    _database.setDatabaseName			(_projectFilename);
    if (!_database.open()) {
        Wrn								<< "The database failed to open.";
        return;
    }

    // Retrieve creation script.
    foreach (QString const &fn, sqlFiles) {
        QFile				f			(fn);
        f.open							(QIODevice::ReadOnly|QIODevice::Text);
        QString				l;
        while (!f.atEnd()) {
            QString		t				= f.readLine().simplified();
            if (t.startsWith("--"))		continue;
            l							+= " "+t;
            if (l.isEmpty() || !l.endsWith(';'))
                continue;
            QSqlError	e				= QSqlQuery(l).lastError();
            if (e.type()!=QSqlError::NoError) {
                Crt						<< "Query" << l << "failed with error" << e.text() << "!";
                _database.close			();
                return;
            }
            l.clear						();
        }
        f.close							();
    }
    // Try to migrate database.
    if (!DBMigrator(_database).migrate()) {
        Crt								<< "Database migration has failed. Please contact your application distributor.";
        _database.close					();
        return;
    }

    // Models.
    _globalPropertiesModel				= new GlobalPropertiesModel(this, _database);
    _documentTypesModel					= new DocumentTypesModel(this, _database);
    _documentTypesModelProxy->setModel	(_documentTypesModel);
    _sampleImagesModel					= new SampleImagesModel(this, _database);
    _sampleImagesModelProxy->setModel	(_sampleImagesModel);
    _trainedAnnsModel					= new TrainedAnnsModel(this, _database);
    _trainedAnnsModelProxy->setModel	(_trainedAnnsModel);

    // Iterate thru each required plugin and see if it's available.
    QStringList				reqPlgs		= projectRequiredPlugins();
    RE::PluginBaseInfos::List const &aPlgs	= _re->loadedPlugins();
    foreach (QString const &plgName, reqPlgs) {
        bool	found				= false;
        foreach (RE::PluginBaseInfos const &bi, aPlgs) {
            if (bi.name==plgName) {
                found				= true;
                break;
            }
        }
        if (!found)
            Crt						<< "A required plugin cannot be found:" << plgName << "!";
    }

    emit projectOpened					();
}
void REA::Core::generateClientBinary (ClientBinaryParameters const &cbps) const {
    DeclareCheesecake       (REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    // Prepare data.
    RE::T::ClientBinaryData				cbd;
    cbd.version									= RE::ClientBinaryDataVersion();
    cbd.generatorVersion						= REA::Version();
    cbd.libraryVersion							= RE::Version();
    cbd.name									= cbps.name;
    cbd.copyright								= CKT01 ? cbps.copyright : "Pierre qui Roule EURL";
    cbd.annThreshold							= cbps.annThreshold;
    cbd.annData									= _trainedAnnsModel->data(_trainedAnnsModel->index(cbps.annRow, TrainedAnnsModel::Data)).toByteArray();
    QHash<QString, quint32>				micrs;
    QStringList							scripts;
    scripts										<< QString("globs: { %1 }").arg(_globalPropertiesModel->valueForKey("Global.Script"));
    // Generate document types recognizers.
    for (ClientBinaryParameters::RecognizersRows::const_iterator i=cbps.recognizersRows.constBegin(); i!=cbps.recognizersRows.constEnd(); ++i) {
        // Store document type row.
        quint32 const					&dtRow	= i.key();
        // Retrieve raw preprocessinf filters data.
        QByteArray                      oBaPpfl = _documentTypesModel->data(_documentTypesModel->index(dtRow, DocumentTypesModel::PreprocessingFiltersData)).toByteArray();
        // Preprocessing filters list will be in ppfl.
        RE::T::ZoneFilterData::List     ppfl    = RE::valueFromStreamedByteArray<RE::T::ZoneFilterData::List>(oBaPpfl);
        // Retrieve raw recognizers data.
        QByteArray						oBaZrds	= _documentTypesModel->data(_documentTypesModel->index(dtRow, DocumentTypesModel::RecognitionData)).toByteArray();
        // All recognizers will be available in oZrds.
        RE::T::ZoneRecognizerData::List	oZrds	= RE::valueFromStreamedByteArray<RE::T::ZoneRecognizerData::List>(oBaZrds);
        // Filtered recognizers will be held in zrds.
        RE::T::ZoneRecognizerData::List	zrds;
        // Loop for each recognizer row to generate...
        RE::T::UIntList const			&rRows      = i.value();
        foreach (quint32 const &rRow, rRows)
            zrds                                    << oZrds[rRow];
        // Finally make document type info struct.
        RE::T::DocumentTypeData			dtd;
        DocumentTypesModel              *dtm        = _documentTypesModel+CKZ05;
        QString							rtScript    = JSMinifier().minify(dtm->index(dtRow, DocumentTypesModel::RejectionTestsScript).data().toString());
        QString                         stsScript   = JSMinifier().minify(dtm->index(dtRow, DocumentTypesModel::SubtypeSelectionScript).data().toString());
        dtd.code                                    = dtm->index(dtRow, DocumentTypesModel::Code).data().toUInt();
        dtd.name                                    = dtm->index(dtRow, DocumentTypesModel::Name).data().toString();
        dtd.noAnnTraining                           = dtm->index(dtRow, DocumentTypesModel::NoAnnTraining).data().toBool();
        dtd.micrRegExp                              = dtm->index(dtRow, DocumentTypesModel::MicrRegExp).data().toString();
        dtd.badMicrRejection                        = dtm->index(dtRow, DocumentTypesModel::BadMicrRejection).data().toBool();
        dtd.hasDocumentValidityScript               = rtScript.length();
        if (dtd.hasDocumentValidityScript)
            scripts                                 << QString("type%1Check: function (strings,acceptationReasons,rejectionReasons) { %2\r }").arg(dtd.code).arg(rtScript);
        dtd.hasSubtypeSelectionScript               = stsScript.length();
        if (dtd.hasSubtypeSelectionScript)
            scripts                                 << QString("type%1SubtypeSelector: function (strings) { %2\r }").arg(dtd.code).arg(stsScript);
        dtd.preprocessingFilters                    = ppfl;
        dtd.recognizers                             = zrds;
        cbd.documentTypesData                       << dtd;
        // Keep track of MICRs.
        micrs[dtd.micrRegExp]                       ++;
    }
    for (qint32 i=0; i<cbd.documentTypesData.count(); ++i) {
        RE::T::DocumentTypeData			&dtd        = cbd.documentTypesData[i];
        dtd.micrIsUnique                            = micrs[dtd.micrRegExp]==1;
        if (!dtd.micrIsUnique)
            Wrn                                     << "Type" << dtd.name << "has a non-unique MICR pattern. If MICR matches it then the ANN will be used to find the document's type.";
    }
    cbd.script                                      = JSMinifier().minify(QString("({%1})").arg(scripts.join(",")));
    // Generate ANN bytearray.
    QFile								fCb         (cbps.path);
    QDataStream							sCb         (&fCb);
    fCb.open                                        (QIODevice::WriteOnly);
    sCb                                             << cbd;
    fCb.close                                       ();
    Dbg                                             << "The Client Binary has been generated under" << cbps.path << "with a weight of" << fCb.size() << "bytes.";
}
void REA::Core::closeProject () {
    // Clear project filename.
    _projectFilename					= "";
    // Signal listeners that the project is no longer valid.
    emit projectClosed					();
    // Reset proxies.
    _documentTypesModelProxy->setModel	(0);
    _sampleImagesModelProxy->setModel	(0);
    _trainedAnnsModelProxy->setModel	(0);
    // Delete models.
    if (_globalPropertiesModel) {
        delete							_globalPropertiesModel;
        _globalPropertiesModel			= 0;
    }
    if (_documentTypesModel) {
        delete							_documentTypesModel;
        _documentTypesModel				= 0;
    }
    if (_sampleImagesModel) {
        delete							_sampleImagesModel;
        _sampleImagesModel				= 0;
    }
    if (_trainedAnnsModel) {
        delete							_trainedAnnsModel;
        _trainedAnnsModel				= 0;
    }
    // Finally close DB.
    _database.close						();
}
QString const& REA::Core::projectFilename () const {
    return _projectFilename;
}
QDir REA::Core::projectDirectory () const {
    return QFileInfo(_projectFilename).absoluteDir();
}
QStringList REA::Core::projectRequiredPlugins () const {
    // Value to be returned.
    QStringList							ret;
    // Prepare result and temp vars.
    QString								an;
    // Iterate for each document type.
    for (int i=0; i<_documentTypesModel->rowCount(); ++i) {
        // Get recognition data index.
        QModelIndex						idx			= _documentTypesModel->index(i, DocumentTypesModel::RecognitionData);
        // Retrieve recognition data, and iterate.
        RE::T::ZoneRecognizerData::List	const
                                        &zrds		= RE::valueFromStreamedByteArray<RE::T::ZoneRecognizerData::List>(idx.data().toByteArray());
        foreach (RE::T::ZoneRecognizerData const &zrd, zrds) {
            an										= zrd.assemblyName;
            if (!an.isEmpty() && !ret.contains(an))
                ret									<< an;
            // Retrieve filters data, and iterate.
            RE::T::ZoneFilterData::List const &zfds	= zrd.filterStack;
            foreach (RE::T::ZoneFilterData const &zfd, zfds) {
                an									= zfd.assemblyName;
                if (!an.isEmpty() && !ret.contains(an))
                    ret								<< an;
            }
        }
    }
    return ret;
}

// Sample image adding.
void REA::Core::addSampleImages (quint32 documentTypeId, QStringList const &filenames) {
    Dbg << "Adding" << filenames.count() << "to Document ID" << documentTypeId << ".";
    // Prepare a list of SampleImageInfo structs to process.
    //_databaseMutex.lockInline();
    foreach (QString const &filename, filenames) {
        QSqlRecord			r					= _sampleImagesModel->record();
        r.setNull			("id");
        r.setValue			("documentType_id",	documentTypeId);
        r.setValue			("filename",		filename);
        r.setValue			("signature",		QByteArray());
        r.setValue			("trainable",		false);
        _sampleImagesModel->insertRecord(-1, r);
    }
    //_databaseMutex.unlock();
    _sampleImagesModel->submit();
}

// Sample image signing request.
void REA::Core::signSampleImages (QModelIndexList const &rows) {
    if (!allProcessesIdle())
        return;
    // Store indexes to process.
    _sampleImagesToSign							= rows;
    int					count					= _sampleImagesToSign.count();
    // Spawn each thread.
    quint32				maxThreads				= qMin(SampleImagesSigner::idealThreadCount(), count);
    if (!maxThreads)							return;
    Dbg											<< "Spawning" << maxThreads << "worker threads...";
    for (quint16 i=0; i<maxThreads; i++) {
        _workerMutex.lockInline                 ();
        _signerCount							++;
        _workerMutex.unlockInline               ();
        if (_signerCount==1)
            emit sampleImagesProcessingStarted	(count);
        SampleImagesSigner	*sis				= new SampleImagesSigner();
        connect(sis, SIGNAL(processed(QModelIndex, QByteArray)), this, SLOT(sampleImagesSigner_processed(QModelIndex, QByteArray)));
        connect(sis, SIGNAL(finished()), this, SLOT(sampleImageSigner_finished()));
        connect(sis, SIGNAL(finished()), sis, SLOT(deleteLater()));
        sis->start								(*_re, *_sampleImagesModel, _databaseMutex);
    }
}
QModelIndex REA::Core::nextPendingSampleImageIndex () {
    QMutexLocker		ml					(&_workerMutex);
    return _sampleImagesToSign.isEmpty() ? QModelIndex() : _sampleImagesToSign.takeFirst();
};


// Ann training / testing.
void REA::Core::annStartTraining (RE::T::UIntList const &dtIds, double mse, double lr, double as, quint32 hnc, quint32 me, quint32 rf) {
    DeclareCheesecake           (REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    _annRunner->startTraining   (_database, *(_documentTypesModel+CKZ06), dtIds, mse, lr, as, hnc, me, rf);
}
void REA::Core::annStartTesting (RE::T::UIntList const &dtIds, QByteArray const &annData) {
    DeclareCheesecake           (REA::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    _annRunner->startTesting    (_database, *(_documentTypesModel+CKZ06), dtIds, annData);
}
void REA::Core::annStopRunning () {
    _annRunner->stop();
}

// Sample image related slots.
void REA::Core::sampleImagesSigner_processed (QModelIndex idx, QByteArray sig) {
    emit sampleImageProcessed();
    _databaseMutex.lock                     ();
    _sampleImagesModel->setData             (_sampleImagesModel->index(idx.row(), REA::SampleImagesModel::Signature), sig);
    _databaseMutex.unlock                   ();
}
void REA::Core::sampleImageSigner_finished () {
    _workerMutex.lockInline                 ();
    _signerCount							--;
    _workerMutex.unlockInline               ();
    if (!_signerCount) {
        _databaseMutex.lockInline           ();
        _sampleImagesModel->submitAll		();
        _databaseMutex.unlockInline         ();
        emit sampleImagesProcessingFinished	();
        return;
    }
}
// Ann training / testing related slots.
void REA::Core::annRunner_trainingStarted () {
    _workerMutex.lockInline                 ();
    _trainerCount							++;
    _workerMutex.unlockInline               ();
    emit	annTrainingStarted				();
}
void REA::Core::annRunner_trainingReport (quint32 epoch, double error) {
    emit	annTrainingReport				(epoch, error);
}
void REA::Core::annRunner_trainingFinished (bool success, QByteArray data) {
    _workerMutex.lockInline                 ();
    _trainerCount							--;
    _workerMutex.unlockInline               ();
    emit	annTrainingFinished				(success, data);
}
void REA::Core::annRunner_testingStarted (RE::T::UIntToStringHash documentTypesList) {
    _workerMutex.lockInline                 ();
    _testerCount							++;
    _workerMutex.unlockInline               ();
    emit	annTestingStarted				(documentTypesList);
}
void REA::Core::annRunner_testingReport (quint32 expectedType, RE::T::UIntToDoubleHash results) {
    emit	annTestingReport				(expectedType, results);
}
void REA::Core::annRunner_testingFinished () {
    _workerMutex.lockInline                 ();
    _testerCount							--;
    _workerMutex.unlockInline               ();
    emit	annTestingFinished				();
}
