#include "SampleImagesSigner.h"
// Required stuff.
#include "../Core.h"
#include <RELibrary/Filters/HistogramEqualizer>
#include <RELibrary/Filters/MedianFilter>
#include <RELibrary/Filters/ConvolutionMatrixFilter>
#include <RELibrary/Filters/BrightnessContrastFilter>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtSql/QSqlRecord>

// Constructor.
REA::SampleImagesSigner::SampleImagesSigner ()
: QThread(), _core(&REA::Core::Instance()), _re(0), _siMdl(0) {
}

// Start method.
void REA::SampleImagesSigner::start (RE::Engine &re, QSqlTableModel &siMdl, QMutex &mutex) {
	_re									= &re;
	_siMdl								= &siMdl;
	_mutex								= &mutex;
	QThread::start();
}

void REA::SampleImagesSigner::run () {
    QDir                projDir         = _core->projectDirectory();
	while (true) {
		_mutex->lock					();
        QModelIndex		idx				= _core->nextPendingSampleImageIndex();
		if (!idx.isValid()) {
			_mutex->unlock				();
			break;
		}
		int				row				= idx.row();
		QSqlRecord		r				= _siMdl->record(row);
		_mutex->unlock					();
		QByteArray		sig;
        QString		fn					= projDir.absoluteFilePath(r.value("filename").toString());
        if (QFile(fn).exists())			sig =  _re->signatureForImage(QImage(fn), QThread::idealThreadCount());
		else							Wrn << "Image named" << fn << "doesn't exist!";
		emit processed(idx, sig);
	}
}
