#ifndef SampleImagesSigner_h
#define SampleImagesSigner_h

// Base class.
#include <QtCore/QThread>
// Required stuff.
#include <RELibrary/Engine>
#include <QtCore/QMutex>
#include <QtSql/QSqlTableModel>

namespace REA {
    class Core;
}

namespace REA {

class SampleImagesSigner : public QThread {
Q_OBJECT
public:
	// Constructor.
    /**/				SampleImagesSigner		();
	// Start method.
	virtual void		start					(RE::Engine &re, QSqlTableModel &siMdl, QMutex &dbMutex);

protected:
	virtual void		run						();

private:
    Core                *_core;
    RE::Engine			*_re;
	QSqlTableModel		*_siMdl;
	QMutex				*_mutex;

signals:
	void				processed				(QModelIndex idx, QByteArray sig);

};

}

#endif
