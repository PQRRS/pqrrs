#ifndef AnnRunner_h
#define AnnRunner_h

// Base class.
#include <QtCore/QThread>
// Various stuff.
#include "../REATypes.h"
#include <RELibrary/RETypes>
#include <QtCore/QDebug>
#include <QtCore/QByteArray>
#include <QtCore/QAbstractItemModel>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlDatabase>

struct fann;
struct fann_train_data;

namespace REA {

class AnnRunner : public QThread {
Q_OBJECT
public:
	// Constructor.
							AnnRunner				(QObject *p) : QThread(p) {}
    virtual void			startTraining			(QSqlDatabase &db, QAbstractItemModel &dtMdl, RE::T::UIntList const &dtIds, double activationStepness, double learningRate, double targetMse, quint32 maxEpochs, quint32 epochsBetweenReports, quint32 hiddenNeuronsCount);
    virtual void			startTesting			(QSqlDatabase &db, QAbstractItemModel &dtMdl, RE::T::UIntList const &dtIds, QByteArray const &annData);
	void					stop					();

protected:
	// Threaded running method.
	void					run						();
	void					runTraining				();
	void					runTesting				();
	// Creates a list of all document types codes.
	RE::T::UIntToStringHash	makeDocumentTypesHash	(bool selectedOnly);

private:
	// Force the start method to be private to disallow it's call from outside.
	virtual void			start					() {
		Wrn					<< "Please either call startTraining or startTesting. The original start method is on purpose unimplemented.";
    }
    QSqlDatabase			*_db;
    QAbstractItemModel      *_dtMdl;
	RE::T::UIntList			_dtIds;
	double					_targetMse, _activationStepness, _learningRate;
	quint32					_hiddenNeuronsCount, _maxEpochs, _reportsFrequency;
	bool					_hasToRun;
	bool					_trainMode;
	QByteArray				_annData;

signals:
	void					trainingStarted		();
	void					trainingReport		(quint32 epoch, double mse);
	void					trainingFinished	(bool success, QByteArray data);
	void					testingStarted		(RE::T::UIntToStringHash documentTypesList);
	void					testingReport		(quint32 expectedType, RE::T::UIntToDoubleHash results);
	void					testingFinished		();
};

}

#endif
