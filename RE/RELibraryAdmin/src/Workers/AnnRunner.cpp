#include "AnnRunner.h"
// Various stuff.
#include "../../ThirdParty/Fann/floatfann.h"
#include "../Models/DocumentTypesModel.h"
#include "../Models/SampleImagesModel.h"
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <QtCore/QString>
#include <QtCore/QBuffer>
#include <QtCore/QStringList>
#include <QtCore/QDataStream>
#include <QtCore/QDebug>

void REA::AnnRunner::startTraining (QSqlDatabase &db, QAbstractItemModel &dtMdl, RE::T::UIntList const &dtIds, double targetMse, double learningRate, double activationStepness, quint32 hiddenNeuronsCount, quint32 maxEpochs, quint32 reportsFrequency) {
    _hasToRun			= true;
    _trainMode			= true;
    _db					= &db;
    _dtMdl				= &dtMdl;
    _dtIds				= dtIds;
    _targetMse			= targetMse;
    _learningRate		= learningRate;
    _activationStepness	= activationStepness;
    _hiddenNeuronsCount	= hiddenNeuronsCount;
    _maxEpochs			= maxEpochs;
    _reportsFrequency	= reportsFrequency;
    _annData.clear		();
    QThread::start		();
}
void REA::AnnRunner::startTesting (QSqlDatabase &db, QAbstractItemModel &dtMdl, RE::T::UIntList const &dtIds, QByteArray const &annData) {
    _hasToRun			= true;
    _trainMode			= false;
    _db					= &db;
    _dtMdl				= &dtMdl;
    _dtIds				= dtIds;
    _annData			= annData;
    QThread::start		();
}
void REA::AnnRunner::stop () {
    _hasToRun							= false;
}

// Threaded running method.
void REA::AnnRunner::run () {
    if (_trainMode)			runTraining();
    else					runTesting();
}

// Training routine.
void REA::AnnRunner::runTraining () {
    // Signal start of training.
    emit					trainingStarted		();
    // Prepare model access to sample mages database.
    QSqlTableModel			siMdl				(0, *_db);
    siMdl.setTable								("SampleImages");
    QStringList				idsList;
    foreach (quint32 i, _dtIds)					idsList << QString::number(i);
    QString					fltr				("trainable=1 AND documentType_id IN ("+idsList.join(",")+")");
    siMdl.setFilter								(fltr);
    siMdl.select								();
    // Force model to get every available row.
    while(siMdl.canFetchMore())					siMdl.fetchMore();
    quint32					siCount				= siMdl.rowCount();

    // List selected document types, sort them.
    qSort                                       (_dtIds);

    // Prepare in / out counts.
    quint32					inCount				= RE::DocumentTypesSignatureSize().width()*RE::DocumentTypesSignatureSize().height();
    quint32					outCount			= _dtIds.count();

    // Prepare ann inputs / outputs sets.
    fann_type				**inputSets			= new fann_type*[siCount];
    fann_type				**outputSets		= new fann_type*[siCount];
    fann_type				*inputs, *outputs;
    // Set each inputs / outputs pair in respective sets.
    for (quint32 i=0; i<siCount && _hasToRun; i++) {
        // Prepare memory.
        inputs									= new fann_type[inCount];
        outputs									= new fann_type[outCount];
        // Set sets to current inputs / outputs.
        inputSets[i]							= inputs;
        outputSets[i]							= outputs;
        // Store signature and document type.
        QByteArray			sigBA				= siMdl.data(siMdl.index(i, SampleImagesModel::Signature)).toByteArray();
        quint8 const		*sig				= (const quint8*)sigBA.constData();
        quint32				dtId				= siMdl.data(siMdl.index(i, SampleImagesModel::DocumentType)).toInt();
        for (quint32 j=0; j<inCount; j++)
            inputs[j]							= 1.0/255.0*sig[j];
        // Set up the correct output value.
        memset									(outputs, 0, outCount*sizeof(fann_type));
        outputs[_dtIds.indexOf(dtId)]			= 1.0;
    }

    // Prepare Ann training data set.
    fann_train_data			td;
    td.num_input								= inCount;
    td.input									= inputSets;
    td.num_output								= outCount;
    td.output									= outputSets;
    td.num_data									= siCount;
    fann_shuffle_train_data						(&td);

    bool					success				= false;
    double					mse					= 1.0;
    // Prepare ann.
    Dbg											<< "Creating ANN with neural layers:" << inCount << _hiddenNeuronsCount << _hiddenNeuronsCount << outCount
                                                << ", Target MSE:" << QString::number(_targetMse, 'f', 12)
                                                << ", Learning Rate:" << _learningRate
                                                << ", Stepness:" << _activationStepness << ".";
    fann					*ann				= fann_create_standard(3, inCount, _hiddenNeuronsCount, outCount);
    // Best seem to be RPROP + FANN_SIGMOID.
    fann_set_training_algorithm					(ann, FANN_TRAIN_INCREMENTAL);
    fann_set_activation_function_hidden			(ann, FANN_SIGMOID_SYMMETRIC);
    fann_set_activation_function_output			(ann, FANN_GAUSSIAN);
    fann_set_learning_rate						(ann, _learningRate);
    fann_set_activation_steepness_hidden		(ann, _activationStepness);
    fann_set_activation_steepness_output		(ann, _activationStepness);
    fann_randomize_weights                      (ann, -0.01, 0.00);
    // Loop for each epoch and train ann.
    for(quint32 i=1; i<=_maxEpochs && _hasToRun; i++) {
        mse										= fann_train_epoch(ann, &td);
        // Target reached!
        if (mse<=_targetMse) {
            success								= true;
            emit			trainingReport		(i, mse);
            break;
        }
        // Report reached!
        else if (i%_reportsFrequency==0)
            emit			trainingReport		(i, mse);
    }
    // Report the end of the training.
    emit					trainingFinished	(success, success ? RE::Engine::annToBinary(ann) : QByteArray());

    // Now free everything.
    fann_destroy								(ann);
    for (quint32 i=0; i<siCount; i++) {
        delete []								inputSets[i];
        delete []								outputSets[i];
    }
    delete []									inputSets;
    delete []									outputSets;

    /*
    if (success)	Dbg << "ANN trained successfully to MSE:" << QString::number(mse, 'f', 12) << ".";
    else			Wrn << "Failed to train ANN to the target MSE.";
    */
}
// Testing routine.
void REA::AnnRunner::runTesting () {
    // Prepare model access to sample mages database.
    QSqlTableModel			siMdl				(0, *_db);
    siMdl.setTable								("SampleImages");
    QStringList				idsList;
    foreach (quint32 i, _dtIds)					idsList << QString::number(i);
    QString					fltr				("");
    //Dbg											<< "Setting filter to" << fltr << "...";
    siMdl.setFilter								(fltr);
    siMdl.select								();
    while(siMdl.canFetchMore())					siMdl.fetchMore();
    // Count available images.
    quint32					siCount				= siMdl.rowCount();

    // Sort list of requested document types ids.
    qSort										(_dtIds);

    // List all document tyes, sort them.
    RE::T::UIntToStringHash	dtNamesHash			= makeDocumentTypesHash(false);
    RE::T::UIntList			allDtIds			= dtNamesHash.keys();
    qSort										(allDtIds);
    //Dbg											<< siCount << "images of" << allDtIds.count() << "types will be used against" << _dtIds.count() << "trained types.";

    // Signal start of training.
    emit					testingStarted		(dtNamesHash);

    // Prepare in / out counts.
    quint32					inCount				= RE::DocumentTypesSignatureSize().width()*RE::DocumentTypesSignatureSize().height();

    // Restore ann from data.
    fann					*ann				= RE::Engine::binaryToAnn(_annData);
    if (!ann) {
        emit testingFinished();
        return;
    }
    // Set ins / outs.
    fann_type				*inputs				= new fann_type[inCount];
    RE::T::UIntToDoubleHash	hashOutputs;
    QByteArray				sigBA;
    quint8 const			*sig;
    quint32					docId;
    // Loop for each testable image.
    for (quint32 i=0; i<siCount && _hasToRun; i++) {
        // Prepare inputs.
        sigBA									= siMdl.data(siMdl.index(i, SampleImagesModel::Signature)).toByteArray();
        sig										= (const quint8*)sigBA.constData();
        docId									= siMdl.data(siMdl.index(i, SampleImagesModel::DocumentType)).toInt();
        for (quint32 si=0; si<inCount; si++)
            inputs[si]							= 1.0/255.0*sig[si];
        // Run the ann.
        fann_type			*outputs			= fann_run(ann, inputs);
        // Loop for all document types.
        quint32		iOut						= 0;
        foreach (quint32 const &dtId, allDtIds) {
            if (_dtIds.contains(dtId))
                hashOutputs[dtId]				= outputs[iOut++];
            else
                hashOutputs[dtId]				= 0;
        }
        // Compute closest confidence.
        emit				testingReport		(docId, hashOutputs);
    }
    // Free memory.
    fann_destroy								(ann);
    delete[]									inputs;
    emit										testingFinished();
}

// Creates a list of all document types codes.
RE::T::UIntToStringHash REA::AnnRunner::makeDocumentTypesHash (bool selectedOnly) {
    RE::T::UIntToStringHash	toRet;
    quint32					c		= _dtMdl->rowCount();
    for (quint32 i=0; i<c; i++) {
        //quint32				dtId	= _dtMdl->record(i).value("id").toUInt();
        quint32             dtId    = _dtMdl->index(i, REA::DocumentTypesModel::Key).data().toUInt();
        if (selectedOnly && !_dtIds.contains(dtId))
            continue;
        QString				dtName	= _dtMdl->index(i, REA::DocumentTypesModel::Name).data().toString();
        toRet[dtId]					= dtName;
    }
    return							toRet;
}
