#ifndef REA_WgtResultDisplay_h
#define REA_WgtResultDisplay_h

#include <QWidget>

namespace Ui { class WgtResultDisplay; }

class WgtResultDisplay : public QWidget {
    Q_OBJECT

public:
    explicit                WgtResultDisplay            (QWidget *parent=0);
    /**/                    ~WgtResultDisplay           ();
    void                    setResult                   (QVariant const &v);

private:
    Ui::WgtResultDisplay    *_ui;
};

#endif
