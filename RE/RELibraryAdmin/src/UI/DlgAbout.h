#ifndef REA_DlgAbout_h
#define REA_DlgAbout_h

// Base class and private UI.
#include <QtGui/QDialog>
#include "ui_DlgAbout.h"
// Required stuff.
#include "../REATypes.h"
#include <RELibrary/RETypes>
#include <RELibrary/Engine>

namespace REA {

class DlgAbout : public QDialog {
Q_OBJECT
public:
    // Constructors, destructor.
    explicit			DlgAbout				(RE::Engine *re, QWidget *p=0)
        : QDialog(p), _ui(new Ui::DlgAbout) {
        // Make sure dialog is modal.
        setModal								(true);
        // Setup main UI.
        _ui->setupUi								(this);
        // Display admin / lib versions.
        _ui->txtRELibraryAdminVersion->setText	(REA::Version());
        _ui->txtRELibraryVersion->setText			(RE::Version());
        // Display plugins versions.
        _ui->tblPluginsVersions->setColumnCount	(2);
        quint32		row							= 0;
        foreach (RE::PluginBaseInfos const &bi, re->loadedPlugins()) {
            _ui->tblPluginsVersions->insertRow    (row);
            _ui->tblPluginsVersions->setItem		(row, 0, new QTableWidgetItem(bi.name));
            _ui->tblPluginsVersions->setItem		(row, 1, new QTableWidgetItem(bi.version));
            ++row;
        }
        _ui->tblPluginsVersions->setColumnWidth	(0, 120);
        _ui->tblPluginsVersions->setColumnWidth	(1, _ui->tblPluginsVersions->width()-125);
    }
    virtual             ~DlgAbout               () {
        delete _ui;
    }
private:
    Ui::DlgAbout	*_ui;
};

}

#endif
