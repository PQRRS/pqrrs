#ifndef REA_WgtGlobalProperties_h
#define REA_WgtGlobalProperties_h

// Base class and UI.
#include <QtGui/QTabWidget>
// Required stuff.
namespace REA { class Core; }
namespace Ui { class WgtGlobalProperties; }
class QScriptEngine;

namespace REA {

class WgtGlobalProperties : public QTabWidget {
Q_OBJECT
public:
	// Constructor.
    explicit                    WgtGlobalProperties			(QWidget *p=0);
    virtual                     ~WgtGlobalProperties        ();
	void						updateUi					();

protected slots:
	// Script syntax check.
	void						on_btnScriptCheck_clicked	();
	// Commit edited values to the database.
	void						commitValues				();

private:
    Ui::WgtGlobalProperties		*_ui;
	Core						*_core;
	QScriptEngine				*_scriptEngine;
};

}

#endif
