#ifndef REA_WgtAnnTrainingHistory_h
#define REA_WgtAnnTrainingHistory_h

// Base class.
#include <QtGui/QWidget>

namespace REA {

class WgtAnnTrainingHistory : public QWidget {
Q_OBJECT

// Constructors.
public:
    explicit                WgtAnnTrainingHistory   (QWidget *p=0);

public slots:
	void					addValues				(quint32 epoch, double mse);
	void					clear					();
	virtual void			paintEvent				(QPaintEvent *pe);

private:
	// Mean squared error history.
	QVector<double>			_mseHistory;
	double					_targetMse;
	quint32					_lastEpoch;
};

}

#endif
