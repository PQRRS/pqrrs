#ifndef REA_WgtDocumentTypesManager_h
#define REA_WgtDocumentTypesManager_h

// Base class and private UI.
#include <QtGui/QWidget>
#include <QtGui/QItemSelection>
// Forward declarations.
namespace REA { class Core; }
namespace Ui { class WgtDocumentTypesManager; }
class QScriptEngine;
class QAbstractItemModel;
class QDataWidgetMapper;

namespace REA {

class WgtDocumentTypesManager : public QWidget {
Q_OBJECT

public:
	// Constructor.
    explicit                    WgtDocumentTypesManager                     (QWidget *p=0);
    virtual                     ~WgtDocumentTypesManager                    ();
	// Various methods.
	void						initTables									();
	void						updateUi									();
	void						updateSampleImagesTableLabel				();

public slots:
	// Core projects management slots.
	void						core_projectOpened							();
	void						core_projectClosed							();
	void						core_destroyed								();
	// Type management slots.
	void						on_btnCreateDocumentType_clicked			();
	void						on_btnDeleteDocumentType_clicked			();
	void						documentTypesModel_selectionChanged			(QItemSelection const&, QItemSelection const&);
	// Document data & script actions.
    void						on_btnSubtypeSelectionScriptCheck_clicked   ();
    void						on_btnRejectionTestsScriptCheck_clicked     ();
    // Type modification management.
	void						on_chkBxDocumentTypeNoAnnTraining_toggled	(bool);
	// Sample images management.
	void						on_btnSampleImagesInsert_clicked			();
	void						on_btnSampleImagesRemove_clicked			();
	void						on_btnSampleImagesSign_clicked				();
	void						on_btnSampleImagesCheck_clicked				();
	void						on_btnSampleImagesUncheck_clicked			();
	void						sampleImagesModel_selectionChanged			(QItemSelection const&, QItemSelection const&);
	void						sampleImagesModel_dataChanged				(QModelIndex const&, QModelIndex const&);
	// Core sample image processing signals.
	void						core_sampleImagesProcessingStarted			(int count);
	void						core_sampleImagesProcessingFinished			();

private:
    Ui::WgtDocumentTypesManager	*_ui;
	Core						*_core;
	QAbstractItemModel			*_dtModel;
	QAbstractItemModel			*_siModel;
	bool						_tablesInitDone;
	QScriptEngine				*_scriptEngine;
	QDataWidgetMapper			*_dataMapper;
};

}

#endif
