#ifndef REA_WgtZoneRecognizersManager_h
#define REA_WgtZoneRecognizersManager_h

// Base class and private UI.
#include <QtGui/QMainWindow>
// Required stuff.
#include <QtCore/QModelIndex>
#include <QtGui/QItemSelection>
// Forward declarations.
namespace REA {
    class Core;
    class ZoneRecognizersScene;
    class ZoneRecognizersModel;
    class ZoneFiltersModel;
}
namespace Ui { class WgtZoneRecognizersManager; }
class QMenu;
class QAction;
class QStringListModel;
class QAbstractItemModel;
class QItemSelectionModel;

namespace REA {

class WgtZoneRecognizersManager : public QMainWindow {
Q_OBJECT

public:
    // Constructor.
    explicit                        WgtZoneRecognizersManager                   (QWidget *p=0);
    virtual                         ~WgtZoneRecognizersManager                  ();
protected:
    void                            closeEvent                                  (QCloseEvent *e);

public slots:
    // Accessors.
    void							setSelection								(QModelIndex const &idx);
    void							fitViewToCurrentImage						() const;

protected slots:
    // Core.
    void							core_projectOpened							();
    void							core_projectClosed							();
    void							core_destroyed								();
    // Sample image browsing.
    void                            on_chkBxDebuggingImage_toggled              ();
    void							on_btnPrevImageFast_clicked					();
    void							on_btnPrevImage_clicked						();
    void							on_btnNextImage_clicked						();
    void							on_btnNextImageFast_clicked					();
    void							on_btnZoomFitView_toggled					(bool);
    void							on_btnZoomIn_clicked						();
    void							on_btnZoomOut_clicked						();
    // Preprocessing thingies.
    void                            btnRefreshPrepro_clicked                    ();
    // Recognizers scene buttons.
    void                            on_cmbRecognizerPicker_currentIndexChanged  (int i);
    void							on_btnRevertZoneRecognizers_clicked			();
    void							on_btnSaveZoneRecognizers_clicked			();
    void							zoneRecognizers_selectionChanged			(QItemSelection const &, QItemSelection const &);
    void							zoneCreationMenu_triggered					(QAction *act);
    void							zoneImportMenu_aboutToShow					();
    void							zoneImportMenu_triggered					(QAction *act);
    void							on_btnDeleteZoneRecognizer_clicked			();
    void                            on_btnSaveZoneRecognizerPictures_clicked    ();
    // Recognizer details thingies.
    void                            on_tabWgtRecognitionZone_filtersChanged     ();
    void                            on_tabWgtRecognitionZone_parameterEdited    (QString const &name, QVariant const &value);
    void							btnRefreshRecognizer_clicked                ();

protected:
    void							updateUi									();
    void							updateSampleImageUi							();
    void							updateCreationMenus							();
    void							showEvent									(QShowEvent *e);
    void							hideEvent									(QHideEvent *e);
    void							toSampleImageAtRow							(qint32 r);

private:
    // UI.
    Ui::WgtZoneRecognizersManager	*_ui;
    bool                            _inUpdate;
    QMenu							*_recognizerCreationMenu,
                                    *_recognizerImportMenu;
    // External objects.
    Core							*_core;
    QAbstractItemModel				*_dtModel;
    QAbstractItemModel				*_siModel;
    // Internal recognition zones model.
    ZoneRecognizersModel			*_zoneRecognizersModel;
    QItemSelectionModel				*_recognizersSelectionModel;
    // View connected to the model.
    REA::ZoneRecognizersScene		*_scene;
    // Selected stuff indices.
    QModelIndex						_oldDtIndex,
                                    _dtIndex,
                                    _zrdIndex;
    // Selected image row.
    int								_sampleImageRow;
    QString							_currentSampleImageFilename;
    QList<QImage>					_currentSampleImages;
};

}

#endif
