#include "WgtDocumentTypesManager.h"
#include "ui_WgtDocumentTypesManager.h"
// Required stuff.
#include "../REATypes.h"
#include "../Core.h"
#include "../JSMinifier.h"
#include "../UI/EditorsAndDelegates/ItemDelegates.h"
#include "../Models/DocumentTypesModel.h"
#include "../Models/SampleImagesModel.h"
#include "../../ThirdParty/JSEdit/JSEdit.h"
#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtScript/QScriptEngine>
#include <QtGui/QProxyModel>
#include <QtGui/QFileDialog>
#include <QtGui/QDataWidgetMapper>
#include <QtCore/QDebug>

REA::WgtDocumentTypesManager::WgtDocumentTypesManager (QWidget *p)
: QWidget(p), _ui(new Ui::WgtDocumentTypesManager),
_core(&REA::Core::Instance()),
_dtModel(0), _siModel(0),
_tablesInitDone(false),
_scriptEngine(0) {
    _ui->setupUi									(this);
    // Set up document types splitter ratios.
    //_ui->spltrMain->setStretchFactor				(1, 100);

    _scriptEngine								= new QScriptEngine();

    // Configure script editors.
    QList<JSEdit*>      editors                     = QList<JSEdit*>() << _ui->txtDocumentTypeRejectionTestsScript << _ui->txtDocumentSubtypeSelectionScript;
    foreach (JSEdit *editor, editors) {
        editor->setColor							(JSEdit::Background,	QColor("#0c152b"));
        editor->setColor							(JSEdit::Normal,		QColor("#dddddd"));
        editor->setColor							(JSEdit::Comment,		QColor("#666666"));
        editor->setColor							(JSEdit::Number,		QColor("#dbf76c"));
        editor->setColor							(JSEdit::String,		QColor("#5ed363"));
        editor->setColor							(JSEdit::Operator,		QColor("#ff7729"));
        editor->setColor							(JSEdit::Identifier,	QColor("#fde15d"));
        editor->setColor							(JSEdit::Keyword,		QColor("#fde15d"));
        editor->setColor							(JSEdit::BuiltIn,		QColor("#9cb6d4"));
        editor->setColor							(JSEdit::Cursor,		QColor("#1e346b"));
        editor->setColor							(JSEdit::Marker,		QColor("#dbf76c"));
        editor->setColor							(JSEdit::BracketMatch,	QColor("#1ab0a6"));
        editor->setColor							(JSEdit::BracketError,	QColor("#a82224"));
        editor->setColor							(JSEdit::FoldIndicator,	QColor("#555555"));
        editor->setTabChangesFocus					(false);
        editor->setTabStopWidth                     (80);
        editor->setBracketsMatchingEnabled			(true);
        editor->setCodeFoldingEnabled				(true);
        editor->setTextWrapEnabled					(false);
        editor->setKeywords                         (editor->keywords() << "this" << "globs" << "strings" << "rejectionReasons" << "acceptationReasons");
    }

    // Prepare widgets data mapper.
    _dataMapper									= new QDataWidgetMapper(this);
    _dataMapper->setSubmitPolicy				(QDataWidgetMapper::AutoSubmit);

    // Restore state / geometry.
    QSettings settings                      ("Pierre qui Roule EURL", "RELibraryAdmin");
    restoreGeometry                         (settings.value("WndMain::DocumentTypes::Geometry").toByteArray());
    _ui->spltrMain->restoreState            (settings.value("WndMain::DocumentTypes::MainSplitter::State").toByteArray());
    _ui->spltrScripts->restoreState         (settings.value("WndMain::DocumentTypes::ScriptsSplitter::State").toByteArray());

    // Connect core stuff...
    connect(_core, SIGNAL(projectOpened()), this, SLOT(core_projectOpened()));
    connect(_core, SIGNAL(projectClosed()), this, SLOT(core_projectClosed()));
    connect(_core, SIGNAL(destroyed()), this, SLOT(core_destroyed()));
    connect(_core, SIGNAL(sampleImagesProcessingStarted(int)), this, SLOT(core_sampleImagesProcessingStarted(int)));
    connect(_core, SIGNAL(sampleImagesProcessingFinished()), this, SLOT(core_sampleImagesProcessingFinished()));
}
REA::WgtDocumentTypesManager::~WgtDocumentTypesManager () {
    // Save window state and geometry.
    QSettings s("Pierre qui Roule EURL", "RELibraryAdmin");
    s.setValue("WndMain::DocumentTypes::Geometry",                  saveGeometry());
    s.setValue("WndMain::DocumentTypes::MainSplitter::State",       _ui->spltrMain->saveState());
    s.setValue("WndMain::DocumentTypes::ScriptsSplitter::State",    _ui->spltrScripts->saveState());
    delete _ui;
}
void REA::WgtDocumentTypesManager::updateUi () {
    // Switch first and last column (Status).
    bool	projectOppenned						= _core->projectFilename().length();
    bool	allProcessesIdle					= projectOppenned && _core->allProcessesIdle();
    bool	dtSelected							= projectOppenned && _ui->tblVwDocumentTypes->selectionModel()->selectedRows().count()==1;
    bool	siSelected							= dtSelected && _ui->tblVwSampleImages->selectionModel()->selectedRows().count()!=0;

    // Document type list stuff.
    _ui->tblVwDocumentTypes->setEnabled			(allProcessesIdle);
    _ui->btnDeleteDocumentType->setEnabled		(allProcessesIdle && dtSelected);
    _ui->txtDocumentTypeMicrRegExp->setEnabled	(allProcessesIdle && dtSelected);
    // Document type details.
    _ui->tabWgtDetails->setEnabled				(allProcessesIdle && dtSelected);
    // Sample images stuff.
    _ui->btnSampleImagesCheck->setEnabled			(allProcessesIdle && siSelected);
    _ui->btnSampleImagesUncheck->setEnabled		(allProcessesIdle && siSelected);
    _ui->btnSampleImagesSign->setEnabled			(allProcessesIdle && siSelected);
    _ui->btnSampleImagesRemove->setEnabled		(allProcessesIdle && siSelected);
    // Zone editing tab.
    _ui->tabWgtDetails->setEnabled				(allProcessesIdle && dtSelected);
}
void REA::WgtDocumentTypesManager::updateSampleImagesTableLabel () {
    SampleImagesModel	*siModel	= _core->realSampleImagesModel();
    // Count all records.
    int					iTotal		= DocumentTypesModel::sampleImagesCount(siModel->filter());
    // Count trainable records.
    int					iTrainable	= DocumentTypesModel::trainableSampleImagesCount(siModel->filter());
    // Deduce runnable records.
    int					iRunnable	= iTotal-iTrainable;
    // Display table summary info.
    _ui->lblSampleImagesTableStatus->setText(tr("Total: %1 - Trainable: %2 - Runnable: %3").arg(iTotal).arg(iTrainable).arg(iRunnable));
}

// Core projects management slots.
void REA::WgtDocumentTypesManager::core_projectOpened () {
    // Cache models.
    _dtModel											= &_core->documentTypesModel();
    _siModel											= &_core->sampleImagesModel();

    // Set datamapper model.
    _dataMapper->clearMapping							();
    _dataMapper->setModel								(_dtModel);
    _dataMapper->addMapping								(_ui->spnDocumentTypeCode,					DocumentTypesModel::Code);
    _dataMapper->addMapping								(_ui->txtDocumentTypeMicrRegExp,				DocumentTypesModel::MicrRegExp);
    _dataMapper->addMapping								(_ui->chkBxDocumentTypeNoAnnTraining,			DocumentTypesModel::NoAnnTraining);
    _dataMapper->addMapping								(_ui->chkBxDocumentTypeBadMicrRejection,		DocumentTypesModel::BadMicrRejection);
    _dataMapper->addMapping                             (_ui->txtDocumentSubtypeSelectionScript,      DocumentTypesModel::SubtypeSelectionScript);
    _dataMapper->addMapping								(_ui->txtDocumentTypeRejectionTestsScript,	DocumentTypesModel::RejectionTestsScript);

    // Force model (re)selection.
    SampleImagesModel	*siModel						= _core->realSampleImagesModel();
    siModel->setFilter									("0=1");
    siModel->select										();

    // (Re)configure Document Types table.
    QItemSelectionModel	*oldSelMdl;

    oldSelMdl											= _ui->tblVwDocumentTypes->selectionModel();
    _ui->tblVwDocumentTypes->setModel						(_dtModel);
    if (oldSelMdl)										delete oldSelMdl;

    oldSelMdl											= _ui->tblVwSampleImages->selectionModel();
    _ui->tblVwSampleImages->setModel						(_siModel);
    if (oldSelMdl)										delete oldSelMdl;

    // Handle tables selection changes.
    connect(_ui->tblVwDocumentTypes->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)), this, SLOT(documentTypesModel_selectionChanged(QItemSelection, QItemSelection)));
    connect(_ui->tblVwSampleImages->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)), this, SLOT(sampleImagesModel_selectionChanged(QItemSelection, QItemSelection)));
    connect(_siModel, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(sampleImagesModel_dataChanged(QModelIndex, QModelIndex)));

    // Set up delegates.
    if (!_tablesInitDone) {
        _tablesInitDone						= true;
        // Document Types table
        _ui->tblVwDocumentTypes->setItemDelegateForColumn	(DocumentTypesModel::Status,	new REA::DocumentTypeStatus(this));
        // Sample Images table.
        _ui->tblVwSampleImages->setItemDelegateForColumn	(SampleImagesModel::Filename,	new REA::Filename(this));
        _ui->tblVwSampleImages->setItemDelegateForColumn	(SampleImagesModel::Signature,	new REA::Signature(this));
    }

    QHeaderView			*hdr				= 0;
    // Document Types table columns visibility.
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::Key,                               true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::ParentKey,                         true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::Code,                              true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::NoAnnTraining,                     true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::MicrRegExp,                        true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::BadMicrRejection,                  true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::SubtypeSelectionScript,            true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::SubtypeSelectionSimulationStrings, true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::RejectionTestsScript,              true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::RejectionTestsSimulationStrings,   true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::PreprocessingFiltersData,          true);
    _ui->tblVwDocumentTypes->setColumnHidden	(DocumentTypesModel::RecognitionData,                   true);
    // Document Types table widths.
    _ui->tblVwDocumentTypes->setColumnWidth	(DocumentTypesModel::Status,				35);
    _ui->tblVwDocumentTypes->setColumnWidth	(DocumentTypesModel::Name,					_ui->tblVwDocumentTypes->width()-40);
    // Document Types table headers.
    //hdr										= _ui->tblVwDocumentTypes->header();
    //hdr->moveSection						(DocumentTypesModel::Status, DocumentTypesModel::Key);

    // Sample Images columns visibility.
    _ui->tblVwSampleImages->setColumnHidden	(SampleImagesModel::Key,					true);
    _ui->tblVwSampleImages->setColumnHidden	(SampleImagesModel::DocumentType,			true);
    // Sample Images columns widths.
    _ui->tblVwSampleImages->setColumnWidth	(SampleImagesModel::Filename,				200);
    _ui->tblVwSampleImages->setColumnWidth	(SampleImagesModel::Signature,				_ui->tblVwSampleImages->width()-250);
    _ui->tblVwSampleImages->setColumnWidth	(SampleImagesModel::Training,				35);
    // Sample Images columns order.
    hdr										= _ui->tblVwSampleImages->horizontalHeader();
    hdr->moveSection						(SampleImagesModel::Training, 0);
}
void REA::WgtDocumentTypesManager::core_projectClosed () {
    // (Re)configure Document Types table.
    QItemSelectionModel	*oldSelMdl;
    // Reset document types table.
    oldSelMdl											= _ui->tblVwDocumentTypes->selectionModel();
    _ui->tblVwDocumentTypes->setModel						(0);
    if (oldSelMdl)										delete oldSelMdl;
    _dtModel											= 0;
    // Reset sample images table.
    oldSelMdl											= _ui->tblVwSampleImages->selectionModel();
    _ui->tblVwSampleImages->setModel						(0);
    if (oldSelMdl)										delete oldSelMdl;
    _siModel											= 0;
    // Refresh window.
    updateUi											();
}
void REA::WgtDocumentTypesManager::core_destroyed () {
}

// Type management slots.
void REA::WgtDocumentTypesManager::on_btnCreateDocumentType_clicked () {
    _core->realDocumentTypesModel()->insertRow(0, QModelIndex());
}
void REA::WgtDocumentTypesManager::on_btnDeleteDocumentType_clicked () {
    DocumentTypesModel	*dtModel	= _core->realDocumentTypesModel();
    QModelIndex			i			= _ui->tblVwDocumentTypes->selectionModel()->currentIndex();
    if (i.isValid()) {
        if (!dtModel->removeRow(i.row(), i.parent())) {
            Wrn						<< "Failed to delete rows!";
            return;
        }
    }
    else {
        Wrn							<< "Invalid index:" << i.row() << "," << i.column() << "!";
        return;
    }
    // Update the main GUI.
    updateUi								();
}

// Document data & script actions.
void REA::WgtDocumentTypesManager::on_btnSubtypeSelectionScriptCheck_clicked () {
    QString					script				= QString("({ typeDummyCheck : function (strings,acceptationReasons,rejectionReasons) { %1\r } })").arg(_ui->txtDocumentSubtypeSelectionScript->toPlainText());
    QScriptSyntaxCheckResult
                            res                 = _scriptEngine->checkSyntax(script);
    if (res.state()!=QScriptSyntaxCheckResult::Valid) {
        Wrn										<< "Script error:" << res.errorMessage() << "at line #" << res.errorLineNumber() << ".";
        return;
    }
}
void REA::WgtDocumentTypesManager::on_btnRejectionTestsScriptCheck_clicked () {
    QString					script				= QString("({ typeDummyCheck : function (strings,acceptationReasons,rejectionReasons) { %1\r } })").arg(_ui->txtDocumentTypeRejectionTestsScript->toPlainText());
    QScriptSyntaxCheckResult
                            res                 = _scriptEngine->checkSyntax(script);
    if (res.state()!=QScriptSyntaxCheckResult::Valid) {
        Wrn										<< "Script error:" << res.errorMessage() << "at line #" << res.errorLineNumber() << ".";
        return;
    }
    Dbg											<< "Script passed verification.";
}
void REA::WgtDocumentTypesManager::documentTypesModel_selectionChanged (QItemSelection const&, QItemSelection const&) {
    // Check selection.
    QModelIndexList const	&sel				= _ui->tblVwDocumentTypes->selectionModel()->selectedRows();
    if (sel.count()!=1)							return;
    QModelIndex const		&idx				= sel.first();
    // Update datamapped fields.
    _dataMapper->setRootIndex                   (idx.parent());
    _dataMapper->setCurrentModelIndex			(idx);
    // Cache real models.
    DocumentTypesModel		*dtModel			= _core->realDocumentTypesModel();
    SampleImagesModel		*siModel			= _core->realSampleImagesModel();
    // Cache selected index / record.
    quint32                 dtId                = dtModel->index(idx.row(), REA::DocumentTypesModel::Key, idx.parent()).data().toUInt();
    // Update models query and re-select rows.
    siModel->setDocumentTypeId					(dtId);
    // Update various dependent controls.
    _ui->wgtZoneRecognizersManager->setSelection	(idx);
    // Update the main GUI.
    updateUi									();
    // Update status labels.
    updateSampleImagesTableLabel				();
}

// Type modification management.
void REA::WgtDocumentTypesManager::on_chkBxDocumentTypeNoAnnTraining_toggled (bool f) {
    _ui->chkBxDocumentTypeBadMicrRejection->setEnabled	(!f);
}

// Sample images management.
void REA::WgtDocumentTypesManager::on_btnSampleImagesInsert_clicked () {
    if (!_core->allProcessesIdle()) {
        Wrn << "The core is busy!";
        return;
    }
    // Get files list from user.
    QSettings			s					("Pierre qui Roule EURL", "RELibraryAdmin");
    QString				path				= s.value("WndMain::AddImagesLastPath", QDir::homePath()).toString();
    QStringList			filenames			= QFileDialog::getOpenFileNames(this, tr("Choose file(s) to open..."), path, "Images (*.bmp *.gif *.jpg *.png *.tif *.tiff *.xpm)");
    quint32				count				= filenames.count();
    if (!count)								return;
    s.setValue								("WndMain::AddImagesLastPath", filenames.first());
    // Get selected doccument type id.
    DocumentTypesModel	*dtModel			= _core->realDocumentTypesModel();
    quint32             dtId                = dtModel->index(_ui->tblVwDocumentTypes->currentIndex().row(), REA::DocumentTypesModel::Key).data().toUInt();
    // Convert absolute to relative paths.
    QStringList			relFilenames;
    foreach (QString const &fn, filenames)
        relFilenames						<< _core->projectDirectory().relativeFilePath(fn);
    _core->addSampleImages					(dtId, relFilenames);
    // Update status labels.
    updateSampleImagesTableLabel			();
}
void REA::WgtDocumentTypesManager::on_btnSampleImagesRemove_clicked () {
    // Remove any selected row.
    QList<int>			rows;
    foreach(QModelIndex const &i, _ui->tblVwSampleImages->selectionModel()->selectedRows())
        if (!rows.contains(i.row()))
            rows.append						(i.row());
    qSort									(rows);
    SampleImagesModel	*siModel			= _core->realSampleImagesModel();
    while (!rows.isEmpty())
        siModel->removeRow(rows.takeLast());
    // Update status labels.
    updateSampleImagesTableLabel			();
}
void REA::WgtDocumentTypesManager::on_btnSampleImagesSign_clicked () {
    QModelIndexList     rows                = _ui->tblVwSampleImages->selectionModel()->selectedRows();
    _core->signSampleImages                 (rows);
}
void REA::WgtDocumentTypesManager::on_btnSampleImagesCheck_clicked () {
    QModelIndexList		rows				= _ui->tblVwSampleImages->selectionModel()->selectedRows();
    foreach (QModelIndex const &i, rows)
        _siModel->setData					(_siModel->index(i.row(), SampleImagesModel::Training),		1);
}
void REA::WgtDocumentTypesManager::on_btnSampleImagesUncheck_clicked () {
    QModelIndexList		rows				= _ui->tblVwSampleImages->selectionModel()->selectedRows();
    foreach (QModelIndex const &i, rows)
        _siModel->setData					(_siModel->index(i.row(), SampleImagesModel::Training),		0);
}

// Core sample image processing signals.
void REA::WgtDocumentTypesManager::sampleImagesModel_selectionChanged (QItemSelection const&, QItemSelection const&) {
    // Update models query and re-select rows.
    updateUi								();
}
void REA::WgtDocumentTypesManager::sampleImagesModel_dataChanged (QModelIndex const&, QModelIndex const&) {
    // Update status labels.
    updateSampleImagesTableLabel();
    QModelIndex			i					= _ui->tblVwDocumentTypes->selectionModel()->currentIndex();
    QModelIndex			fi					= _dtModel->index(i.row(), DocumentTypesModel::Status, i.parent());
    DocumentTypesModel	*dtModel			= _core->realDocumentTypesModel();
    dtModel->triggerUpdate					(fi);
}

void REA::WgtDocumentTypesManager::core_sampleImagesProcessingStarted (int count) {
    Q_UNUSED(count)
    updateUi								();
}
void REA::WgtDocumentTypesManager::core_sampleImagesProcessingFinished () {
    updateUi								();
}
