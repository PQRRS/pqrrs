#include "WgtAnnsManager.h"
#include "ui_WgtAnnsManager.h"
// Required stuff.
#include "../REATypes.h"
#include "../Models/DocumentTypesModel.h"
#include "../Models/DocumentTypesModel.h"
#include "../Models/TrainedAnnsModel.h"
#include "../Models/AnnTestingResultsModel.h"
#include "EditorsAndDelegates/ItemDelegates.h"
#include <QtCore/QDebug>
#include <QtCore/QBuffer>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>
#include <QtSql/QSqlRecord>
#include <QtGui/QProxyModel>
#include <QtGui/QDataWidgetMapper>
#include <QtGui/QItemSelectionRange>

REA::WgtAnnsManager::WgtAnnsManager (QWidget *p)
: QWidget(p), _ui(new Ui::WgtAnnsManager),
_core(&REA::Core::Instance()),
_dtModel(0), _taModel(0), _atrModel(0), _tablesInitDone(false), _dataMapper(0) {
    _ui->setupUi                                (this);

    // Hide fake editors.
    _ui->txtTrainingDocumentTypes->setHidden    (true);
    _ui->wgtTrainingData->setHidden				(true);

    _dataMapper									= new QDataWidgetMapper(this);
    _dataMapper->setSubmitPolicy				(QDataWidgetMapper::ManualSubmit);

    // Restore state / geometry.
    QSettings settings                          ("Pierre qui Roule EURL", "RELibraryAdmin");
    restoreGeometry                             (settings.value("WndMain::ANNsManager::Geometry").toByteArray());
    _ui->spltrMain->restoreState                (settings.value("WndMain::ANNsManager::MainSplitter::State").toByteArray());

    // Core project management slots.
    connect(_core, SIGNAL(projectOpened()), this, SLOT(core_projectOpened()));
    connect(_core, SIGNAL(projectClosed()), this, SLOT(core_projectClosed()));
    // Core Ann training signals.
    connect(_core, SIGNAL(annTrainingStarted()), this, SLOT(core_trainingStarted()));
    connect(_core, SIGNAL(annTrainingReport(qint32, double)), this, SLOT(core_trainingReport(qint32, double)));
    connect(_core, SIGNAL(annTrainingFinished(bool, QByteArray)), this, SLOT(core_trainingFinished(bool, QByteArray)));
    // Core Ann testing signals.
    connect(_core, SIGNAL(annTestingStarted(RE::T::UIntToStringHash)), this, SLOT(core_testingStarted(RE::T::UIntToStringHash)));
    connect(_core, SIGNAL(annTestingReport(quint32, RE::T::UIntToDoubleHash)), this, SLOT(core_testingReport(quint32, RE::T::UIntToDoubleHash)));
    connect(_core, SIGNAL(annTestingFinished()), this, SLOT(core_testingFinished()));
}
REA::WgtAnnsManager::~WgtAnnsManager () {
    // Save window state and geometry.
    QSettings s("Pierre qui Roule EURL", "RELibraryAdmin");
    s.setValue("WndMain::ANNsManager::Geometry",                  saveGeometry());
    s.setValue("WndMain::ANNsManager::MainSplitter::State",       _ui->spltrMain->saveState());
    // Destroy UI.
    delete _ui;
}

RE::T::UIntList REA::WgtAnnsManager::documentTypesKeys () const {
    RE::T::UIntList			dtIds;
    quint32 const			rowCount			= _dtModel->rowCount();
    for (quint32 row=0; row<rowCount; ++row) {
        QModelIndex const	&i					= _dtModel->index(row, DocumentTypesModel::Key);
        if (!i.isValid())						continue;
        quint32				dtId				= _dtModel->data(i).toUInt();
        if (!dtIds.contains(dtId))				dtIds << dtId;
    }
    QModelIndexList const	&rows				= _ui->lstTrainingDocumentTypeKeys->selectionModel()->selectedRows();
    foreach (QModelIndex const &i, rows) {
        if (!i.isValid())						continue;
        quint32				dtId				= _dtModel->data(_dtModel->index(i.row(), DocumentTypesModel::Key)).toUInt();
        if (!dtIds.contains(dtId))				dtIds << dtId;
    }
    return										dtIds;
}
RE::T::UIntList REA::WgtAnnsManager::selectedDocumentTypesKeys () const {
    RE::T::UIntList			dtIds;
    QModelIndexList const	&rows				= _ui->lstTrainingDocumentTypeKeys->selectionModel()->selectedRows();
    foreach (QModelIndex const &i, rows) {
        if (!i.isValid())						continue;
        quint32				dtId				= _dtModel->data(_dtModel->index(i.row(), DocumentTypesModel::Key)).toUInt();
        if (!dtIds.contains(dtId))				dtIds << dtId;
    }
    return										dtIds;
}
RE::T::UIntList REA::WgtAnnsManager::selectedTrainableDocumentTypesKeys () const {
    RE::T::UIntList			dtIds;
    QModelIndexList const	&rows				= _ui->lstTrainingDocumentTypeKeys->selectionModel()->selectedRows();
    foreach (QModelIndex const &i, rows) {
        if (!i.isValid())						continue;
        bool				noAnnTraining		= _dtModel->data(_dtModel->index(i.row(), DocumentTypesModel::NoAnnTraining)).toBool();
        if (noAnnTraining) {
            QString			n					= _dtModel->data(_dtModel->index(i.row(), DocumentTypesModel::Name)).toString();
            Wrn									<< "The type" << n << "is selected for training but is marked as only using MICR.";
            continue;
        }
        quint32				dtId				= _dtModel->data(_dtModel->index(i.row(), DocumentTypesModel::Key)).toUInt();
        if (!dtIds.contains(dtId))				dtIds << dtId;
    }
    return										dtIds;
}

void REA::WgtAnnsManager::updateUi () {
    bool	allProcessesIdle					= _core->allProcessesIdle();
    bool	projectOpened						= _core->projectFilename().length();
    bool	selected							= projectOpened && _taModel && _ui->tblVwList->selectionModel()->currentIndex().isValid();
    bool	trainerIdle							= _core->annTrainerIdle();
    bool	testerIdle							= _core->annTesterIdle();

    // Anns list.
    _ui->wgtList->setEnabled						(allProcessesIdle);
    // Anns deletion button.
    _ui->btnDelete->setEnabled					(allProcessesIdle && selected);
    // Ann training controls.
    _ui->wgtTrainingFields->setEnabled			(allProcessesIdle && selected);
    _ui->btnTrainingRevert->setEnabled			(allProcessesIdle && selected);
    _ui->btnTrainingSave->setEnabled				(allProcessesIdle && selected);
    _ui->btnTrainingRun->setEnabled				((allProcessesIdle || !trainerIdle) && selected);
    _ui->btnTrainingRun->setText					(trainerIdle ? tr("Start") : tr("Stop"));
    _ui->btnTrainingRun->setIcon					(QIcon(trainerIdle ? ":/Resources/Icons/control-play.gif" : ":/Resources/Icons/control-eject.gif"));
    _ui->btnTrainingRun->update					();
    // Ann testing controls.
    _ui->btnTestingRun->setEnabled				((allProcessesIdle || !testerIdle) && selected);
    _ui->btnTestingRun->setText					(testerIdle ? tr("Start") : tr("Stop"));
    _ui->btnTestingRun->setIcon					(QIcon(testerIdle ? ":/Resources/Icons/control-play.gif" : ":/Resources/Icons/control-eject.gif"));
}

// Core signals.
void REA::WgtAnnsManager::core_projectOpened () {
    // Cache models.
    _dtModel											= &_core->documentTypesModel();
    _taModel											= &_core->trainedAnnsModel();
    _atrModel											= new AnnTestingResultsModel(this);

    QItemSelectionModel						*oldSel;

    // Trained anns list model.
    oldSel												= _ui->tblVwList->selectionModel();
    _ui->tblVwList->setModel								(_taModel);
    if (oldSel)											delete oldSel;
    connect(_ui->tblVwList->selectionModel(), SIGNAL(selectionChanged(QItemSelection const&,QItemSelection const&)), this, SLOT(listModel_selectionChanged(QItemSelection const&,QItemSelection const&)));

    // Document types keys model.
    oldSel												= _ui->lstTrainingDocumentTypeKeys->selectionModel();
    _ui->lstTrainingDocumentTypeKeys->setModel			(_dtModel);
    _ui->lstTrainingDocumentTypeKeys->setModelColumn		(DocumentTypesModel::Name);
    if (oldSel)											delete oldSel;
    connect(_ui->lstTrainingDocumentTypeKeys->selectionModel(), SIGNAL(selectionChanged(QItemSelection const&, QItemSelection const&)), this, SLOT(dtList_selectionChanged(QItemSelection const&,QItemSelection const&)));

    // Results list model.
    oldSel												= _ui->tblVwTestingHistory->selectionModel();
    _ui->tblVwTestingHistory->setModel					(_atrModel);
    if (oldSel)											delete oldSel;

    // Force re-selection.
    _core->realTrainedAnnsModel()->select();

    // Set datamapper model.
    _dataMapper->clearMapping							();
    _dataMapper->setModel								(_taModel);
    _dataMapper->addMapping								(_ui->spnTrainingTargetMse,					TrainedAnnsModel::TargetMse);
    _dataMapper->addMapping								(_ui->spnTrainingLearningRate,				TrainedAnnsModel::LearningRate);
    _dataMapper->addMapping								(_ui->spnTrainingActivationStepness,			TrainedAnnsModel::ActivationStepness);
    _dataMapper->addMapping								(_ui->spnTrainingHiddenNeurons,				TrainedAnnsModel::HiddenNeuronsCount);
    _dataMapper->addMapping								(_ui->spnTrainingMaxEpoch,					TrainedAnnsModel::MaxEpoch);
    _dataMapper->addMapping								(_ui->spnTrainingReportsFrequency,			TrainedAnnsModel::ReportsFrequency);
    _dataMapper->addMapping								(_ui->txtTrainingDocumentTypes,				TrainedAnnsModel::DocumentTypesKeys);
    _dataMapper->addMapping								(_ui->wgtTrainingData,						TrainedAnnsModel::Data);

    // Set up delegates.
    if (!_tablesInitDone) {
        _tablesInitDone									= true;
        // Anns table.
        _ui->tblVwList->setItemDelegateForColumn			(TrainedAnnsModel::Key,						new REA::ReadOnly(this));
        _ui->tblVwList->setItemDelegateForColumn			(TrainedAnnsModel::Status,					new REA::TrainedAnnStatus(this));
    }

    QHeaderView							*hdr			= 0;
    // Anns columns visibility
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::Key,						true);
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::TargetMse,				true);
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::LearningRate,			true);
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::ActivationStepness,		true);
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::HiddenNeuronsCount,		true);
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::MaxEpoch,				true);
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::ReportsFrequency,		true);
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::DocumentTypesKeys,		true);
    _ui->tblVwList->setColumnHidden						(TrainedAnnsModel::Data,					true);
    // Anns columns widths.
    _ui->tblVwList->setColumnWidth						(TrainedAnnsModel::Name,					_ui->tblVwList->width()-25);
    _ui->tblVwList->setColumnWidth						(TrainedAnnsModel::Status,					20);
    // Anns columns order.
    hdr													= _ui->tblVwList->horizontalHeader();
    hdr->moveSection									(TrainedAnnsModel::Status, 0);

    // Training results column widths.
    _ui->tblVwTestingHistory->setColumnWidth				(AnnTestingResultsModel::Successes,			55);
    _ui->tblVwTestingHistory->setColumnWidth				(AnnTestingResultsModel::Failures,			55);
    _ui->tblVwTestingHistory->setColumnWidth				(AnnTestingResultsModel::Ratio,				55);
    _ui->tblVwTestingHistory->setColumnWidth				(AnnTestingResultsModel::HighestConfidence,	55);
    _ui->tblVwTestingHistory->setColumnWidth				(AnnTestingResultsModel::LowestConfidence,	55);
    _ui->tblVwTestingHistory->setColumnWidth				(AnnTestingResultsModel::AverageConfidence,	55);
}
void REA::WgtAnnsManager::core_projectClosed () {
    QItemSelectionModel						*oldSel;

    oldSel												= _ui->lstTrainingDocumentTypeKeys->selectionModel();
    _ui->lstTrainingDocumentTypeKeys->setModel			(0);
    if (oldSel)											delete oldSel;

    oldSel												= _ui->tblVwList->selectionModel();
    _ui->tblVwList->setModel								(0);
    if (oldSel)											delete oldSel;

    oldSel												= _ui->tblVwTestingHistory->selectionModel();
    _ui->tblVwTestingHistory->setModel					(0);
    if (oldSel)											delete oldSel;

    _dtModel											= 0;
    _taModel											= 0;
    if (_atrModel) {
        delete											_atrModel;
        _atrModel										= 0;
    }
    updateUi											();
}

// Ann management.
void REA::WgtAnnsManager::on_btnCreate_clicked () {
    TrainedAnnsModel	*taModel	= _core->realTrainedAnnsModel();
    QSqlRecord			r			= taModel->record();
    r.setNull						(TrainedAnnsModel::Key);
    r.setValue						(TrainedAnnsModel::Name,				tr("New ANN"));
    r.setValue						(TrainedAnnsModel::TargetMse,			0.0001);
    r.setValue						(TrainedAnnsModel::LearningRate,		0.6);
    r.setValue						(TrainedAnnsModel::ActivationStepness,	0.5);
    r.setValue						(TrainedAnnsModel::HiddenNeuronsCount,	32);
    r.setValue						(TrainedAnnsModel::MaxEpoch,			2500);
    r.setValue						(TrainedAnnsModel::ReportsFrequency,	1);
    r.setValue						(TrainedAnnsModel::DocumentTypesKeys,	"");
    r.setNull						(TrainedAnnsModel::Data);
    taModel->insertRecord			(-1, r);
}
void REA::WgtAnnsManager::on_btnDelete_clicked () {
    QModelIndexList const	&sel				= _ui->tblVwList->selectionModel()->selectedRows();
    if (sel.count()!=1) {
        Wrn << "No row selected!";
        return;
    }
    QModelIndex const		&i					= sel.first();
    TrainedAnnsModel		*taModel			= _core->realTrainedAnnsModel();
    if (i.isValid())
        taModel->removeRow						(i.row(), i.parent());
    else {
        Wrn << "Invalid index:" << i.row() << "," << i.column() << "!";
        return;
    }
    // Update the main GUI.
    updateUi									();
}
void REA::WgtAnnsManager::on_btnTrainingRevert_clicked () {
    QModelIndexList const	&sel				= _ui->tblVwList->selectionModel()->selectedRows();
    if (sel.count()!=1)							return;
    QModelIndex const		&i					= sel.first();
    if (!i.isValid()) {
        Wrn										<< "Invalid index:" << i.row() << "," << i.column() << "!";
        return;
    }
    // Set mapper's index.
    _dataMapper->setCurrentModelIndex(i);
    QStringList				szDtIds				= _ui->txtTrainingDocumentTypes->text().split(',');
    RE::T::UIntList			dtIds;
    foreach (QString const &dtId, szDtIds)		dtIds << dtId.toUInt();
    _ui->lstTrainingDocumentTypeKeys->selectionModel()->clear();
    for (int i=0; i<_dtModel->rowCount(); i++) {
        QModelIndex			idx					= _dtModel->index(i, DocumentTypesModel::Key);
        if (dtIds.contains(idx.data().toUInt())) {
            QItemSelection		is;
            is.append							(QItemSelectionRange(_dtModel->index(i, 0), _dtModel->index(i, _dtModel->columnCount()-1)));
            _ui->lstTrainingDocumentTypeKeys->selectionModel()->select(is, QItemSelectionModel::Select);
        }
    }
}
void REA::WgtAnnsManager::on_btnTrainingSave_clicked () {
    _ui->frmTrainingFields->activate				();
    _dataMapper->submit							();
}
void REA::WgtAnnsManager::listModel_selectionChanged (QItemSelection const&, QItemSelection const&) {
    on_btnTrainingRevert_clicked				();
    updateUi									();
}
void REA::WgtAnnsManager::dtList_selectionChanged (QItemSelection const&, QItemSelection const&) {
    RE::T::UIntList			dtIds				= selectedDocumentTypesKeys();
    QStringList				szDtIds;
    foreach (quint32 const &dtId, dtIds)		szDtIds << QString::number(dtId);
    _ui->txtTrainingDocumentTypes->setText		(szDtIds.join(","));
}

// Ann training.
void REA::WgtAnnsManager::on_btnTrainingRun_clicked () {
    if (!_core->allProcessesIdle() && _core->annTrainerIdle()) {
        Wrn << "The core is busy!";
        return;
    }
    else if (_core->annTrainerIdle()) {
        _ui->wgtTrainingHistory->clear			();
        RE::T::UIntList				dtIds		= selectedTrainableDocumentTypesKeys();
        _core->annStartTraining					(dtIds, _ui->spnTrainingTargetMse->value(), _ui->spnTrainingLearningRate->value(), _ui->spnTrainingActivationStepness->value(), _ui->spnTrainingHiddenNeurons->value(), _ui->spnTrainingMaxEpoch->value(), _ui->spnTrainingReportsFrequency->value());
    }
    else
        _core->annStopRunning					();
}
void REA::WgtAnnsManager::on_btnTestingRun_clicked () {
    if (!_core->allProcessesIdle() && _core->annTesterIdle()) {
        Wrn << "The core is busy!";
        return;
    }
    else if (_core->annTesterIdle()) {
        RE::T::UIntList				dtIds		= selectedTrainableDocumentTypesKeys();
        _core->annStartTesting					(dtIds, _ui->wgtTrainingData->value());
    }
    else										_core->annStopRunning	();
}

// Ann training core slots.
void REA::WgtAnnsManager::core_trainingStarted () {
    _atrModel->reset							(RE::T::UIntToStringHash());
    updateUi									();
}
void REA::WgtAnnsManager::core_trainingReport (qint32 epoch, double error) {
    _ui->wgtTrainingHistory->addValues			(epoch, error);
}
void REA::WgtAnnsManager::core_trainingFinished (bool, QByteArray data) {
    _ui->wgtTrainingData->setValue				(data);
    updateUi();
}

// Ann testing core slots.
void REA::WgtAnnsManager::core_testingStarted (RE::T::UIntToStringHash documentTypesList) {
    _atrModel->reset							(documentTypesList);
    updateUi									();
}
void REA::WgtAnnsManager::core_testingReport (quint32 expectedType, RE::T::UIntToDoubleHash results) {
    _atrModel->addResult						(expectedType, results);
}
void REA::WgtAnnsManager::core_testingFinished () {
    updateUi									();
}
