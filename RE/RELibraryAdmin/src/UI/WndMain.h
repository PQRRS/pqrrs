#ifndef REA_WndMain_h
#define REA_WndMain_h

// Base class and private UI.
#include <QtGui/QMainWindow>
// Required stuff.
#include "../Core.h"
#include <QtCore/QMutex>

// Forward declarations.
class QStandardItemModel;
namespace Ui { class WndMain; }

namespace REA {

class AnnTestingResultsModel;

class WndMain : public QMainWindow {
Q_OBJECT

public:
	// View mode enum.
	enum						ViewMode								{ ViewModeUndefined = 0, ViewModeDocumentTypes, ViewModeAnn, ViewModeGlobalProperties };

	// Public default constructor.
    /**/                        WndMain									(QWidget *p=0, Qt::WFlags f=0);
	// Destructor
								~WndMain								();

protected:
	// Debug messages handling.
	static void					staticMessageHandler					(QtMsgType type, const char *msg);
	// Other stuff.
	void						updateUi								(WndMain::ViewMode vm);
	void						updateRecentMenu						();
	void						closeEvent								(QCloseEvent *e);

protected slots:
	// Core project management signals.
	void						core_projectOpened						();
	void						core_projectClosed						();
	void						core_destroyed							();
	// Project management slots.
	void						on_actProjectNew_triggered				();
	void						on_actProjectOpen_triggered				();
	void						on_actProjectGenerate_triggered			();
	void						on_actProjectClose_triggered			();
	// View management slots.
	void						on_actDocumentTypesView_triggered		();
	void						on_actAnnView_triggered					();
	void						on_actGlobalPropertiesView_triggered	();
	// Help.
	void						on_actAbout_triggered					();
	// Console actions.
	void						on_btnClearConsole_clicked				();

private:
	void						updateRecentFileActions					();
    // UI Object.
    Ui::WndMain					*_ui;
    // Static instance for passing messages.
    static WndMain				*_instance;
    // Other members.
    ViewMode					_viewMode;
	Core						*_core;
	QStandardItemModel			*_consoleModel;
	QMutex						_consoleMutex;
};

}

#endif
