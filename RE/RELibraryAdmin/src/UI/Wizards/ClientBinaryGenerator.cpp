#include "ClientBinaryGenerator.h"
// Base class for pages.
#include <QtGui/QWizardPage>
// Required stuff.
#include "../../Models/GlobalPropertiesModel.h"
#include "../../Models/DocumentTypesModel.h"
#include "../../Models/TrainedAnnsModel.h"
#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtCore/QStringList>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>
#include <QtGui/QListView>
#include <QtGui/QTreeView>
#include <QtGui/QLineEdit>
#include <QtGui/QCheckBox>
#include <QtGui/QToolButton>
#include <QtGui/QFileDialog>
#include <QtGui/QVBoxLayout>
#include <QtGui/QFormLayout>
#include <QtGui/QItemSelection>
#include <QtGui/QStringListModel>
#include <QtGui/QStandardItemModel>
#include <QtGui/QSortFilterProxyModel>

// Introduction page.
class REA::ClientBinaryGenerator::IntroductionPage : public QWizardPage {
Q_OBJECT
public:
    // Constructor.
    /**/					IntroductionPage			(Core *c, QWidget *p=0)
    : QWizardPage(p), _core(c) {
        // Set title / subtitle.
        setTitle						(tr("Introduction"));
        setSubTitle						(tr("This wizard will help you to fullfill the informations required for generating a Client Binary."));
        setPixmap						(QWizard::WatermarkPixmap, QPixmap::fromImage(QImage(":/Resources/Images/MagicStick.png")));

        // Prepare layout.
        QVBoxLayout		*lyt			= new QVBoxLayout;

        // Create description label.
        QLabel			*lbl			= new QLabel(tr("You will first need to select what ANN to use for this Client Binary. Then you will then have to pick from the Zone Recognizers you want to make available in the Client Binary. Finally, the Client Binary will be generated and dependencies will be packed into one single file you will then be able to easily deploy. You may start by entering a name and a copyright for below."));
        lbl->setWordWrap				(true);
        lyt->addWidget					(lbl);

        // Prepare form.
        QFormLayout		*frmMain		= new QFormLayout();

        // Prepare path layout.
        QLabel			*lblPath		= new QLabel(tr("Path"));
        QHBoxLayout		*lytPath		= new QHBoxLayout();
        QLineEdit		*txtPath		= new QLineEdit();
        QToolButton		*btnPath		= new QToolButton();
        lblPath->setBuddy				(btnPath);
        btnPath->setText				("...");
        txtPath->setEnabled				(false);
        registerField					("clientBinaryPath*", txtPath);
        connect(btnPath, SIGNAL(clicked()), this, SLOT(btnPath_clicked()));
        lytPath->addWidget				(txtPath);
        lytPath->addWidget				(btnPath);
        frmMain->addRow					(lblPath, lytPath);

        // Prepare name fieldset.
        QLabel			*lblName		= new QLabel(tr("Name"));
        QLineEdit		*txtName		= new QLineEdit();
        lblName->setBuddy				(txtName);
        registerField					("clientBinaryName*", txtName);
        frmMain->addRow					(lblName, txtName);

        // Prepare copyright fieldset.
        QLabel			*lblCopyright	= new QLabel(tr("Copyright"));
        _txtCopyright					= new QLineEdit();
        lblCopyright->setBuddy			(_txtCopyright);
        registerField					("clientBinaryCopyright*", _txtCopyright);
        frmMain->addRow					(lblCopyright, _txtCopyright);

        // Add form to layout.
        lyt->addLayout					(frmMain);

        // Finally set layout.
        setLayout						(lyt);
    }
    // Updates selection to reflect the previous page choices.
    void					initializePage				() {
        // Reset page.
        QWizardPage::initializePage		();
        GlobalPropertiesModel	&mdl		= (GlobalPropertiesModel&)_core->globalPropertiesModel();
        _txtCopyright->setText			(mdl.valueForKey("Global.Copyright"));
    };

protected slots:
        void					btnPath_clicked				() {
        QSettings			s			("Pierre qui Roule EURL", "RELibraryAdmin");
        QString				path		= s.value("WndMain::NewProjectLastPath", QDir::homePath()).toString();
        path							= QFileDialog::getSaveFileName(this, tr("Save Client Binary..."), path, "RELibrary Client Binaries (*.recb)");
        if (!path.length())				return;
        s.setValue						("WndMain::NewProjectLastPath", QFileInfo(path).dir().path());
        QFileInfo			iFn			(path);
        setField						("clientBinaryPath", path);
        setField						("clientBinaryName", RE::ClassNameToNaturalString(QFileInfo(path).baseName()));
    };
private:
    Core					*_core;
    QLineEdit				*_txtCopyright;
};

// Ann selection Page.
class REA::ClientBinaryGenerator::AnnSelectionPage : public QWizardPage {
Q_OBJECT
public:
    // Constructor.
    /**/					AnnSelectionPage			(Core *c, QWidget *p=0)
    : QWizardPage(p), _core(c) {
        // Set title / subtitle.
        setTitle						(tr("ANN Selection"));
        setSubTitle						(tr("Please choose from the list which ANN you would like to use in the Client Binary."));
        setPixmap						(QWizard::WatermarkPixmap, QPixmap::fromImage(QImage(":/Resources/Images/Brain.png")));

        // Prepare layout.
        QFormLayout		*lyt			= new QFormLayout();

        // Create ANNs list.
        _annList						= new QListView();
        _annList->setModel				(&_core->trainedAnnsModel());
        _annList->setSelectionMode		(QAbstractItemView::SingleSelection);
        _annList->setSelectionBehavior	(QAbstractItemView::SelectRows);
        _annList->setModelColumn		(1);
        _annList->setSizePolicy			(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        lyt->addRow						(_annList);

        // ANN threshold box.
        QLabel			*lblAnnThresh	= new QLabel(tr("ANN Threshold"));
        _spnBxAnnThresh					= new QSpinBox(this);
        _spnBxAnnThresh->setRange		(50, 99);
        _spnBxAnnThresh->setValue		(80);
        lblAnnThresh->setBuddy			(_spnBxAnnThresh);
        registerField					("clientBinaryAnnThreshold", _spnBxAnnThresh);
        lyt->addRow						(lblAnnThresh, _spnBxAnnThresh);

        // Create bypass checkbox.
        _chkAnnBypass					= new QCheckBox();
        _chkAnnBypass->setChecked		(false);
        _chkAnnBypass->setText			(tr("Only export one single Document Type."));
        lyt->addRow						(_chkAnnBypass);

        // Create summary label.
        _summaryLabel					= new QLabel(tr("No ANN selected."));
        _summaryLabel->setWordWrap		(true);
        _summaryLabel->setSizePolicy	(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
        lyt->addRow						(_summaryLabel);

        // Finally set layout.
        setLayout						(lyt);

        // Connect events.
        connect(_chkAnnBypass, SIGNAL(toggled(bool)), this, SLOT(chkAnnBypass_toggled(bool)));
        connect(this, SIGNAL(annIndexChanged(QModelIndex)), this, SIGNAL(completeChanged()));
        connect(_annList->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(dtModel_selectionChanged(QItemSelection,QItemSelection)));
    }
    // Reimplemented isComplete flag.
    bool					isComplete					() const {
        return QWizardPage::isComplete() && (_chkAnnBypass->isChecked() || _annIndex.isValid());
    }
    // Accessors.
    QModelIndex const&		annIndex					() const {
        return _annIndex;
    }
protected slots:
    void					chkAnnBypass_toggled		(bool f) {
        _annList->setEnabled			(!f);
        _spnBxAnnThresh->setEnabled		(!f);
        if (f)							_annList->selectionModel()->clear();
        emit completeChanged			();
    }
    void					dtModel_selectionChanged	(QItemSelection const &s, QItemSelection const&) {
        QModelIndexList const			&sel		= s.indexes();
        if (sel.isEmpty()) {
            if (_annIndex.isValid()) {
                _annIndex							= QModelIndex();
                emit								annIndexChanged(_annIndex);
            }
            return;
        }
        QModelIndex						idx			= sel.first();
        // Prepare problems / summary / recognizers count.
        QMap<QString,QStringList>		dtMessages;
        QStringList						problems;
        quint32							rCount		= 0;
        // Get a pointer to the trained anns model.
        QAbstractItemModel				*taMdl		= &_core->trainedAnnsModel();

        // Get document types IDs associated to the selected ANN.
        QStringList						szDtIds		= taMdl->data(taMdl->index(idx.row(), TrainedAnnsModel::DocumentTypesKeys)).toString().split(',');
        RE::T::UIntList					dtIds;
        foreach (QString const &dtId, szDtIds)		dtIds << dtId.toUInt();

        // No document types in this ANN.
        if (dtIds.isEmpty())
            problems								<< tr("There are no recognizable document types for this ANN.");
        if (taMdl->data(taMdl->index(idx.row(), TrainedAnnsModel::Data)).toByteArray().isEmpty())
            problems								<< tr("The selected ANN is not trained.");
        // Process each document type.
        else {
            // Loop for every available document type.
            DocumentTypesModel			*dtMdl		= _core->realDocumentTypesModel();
            for (qint32 i=0; i<dtMdl->rowCount(); ++i) {
                // Store document type key.
                quint32	dtId						= dtMdl->data(dtMdl->index(i, DocumentTypesModel::Key)).toUInt();
                // Document type not in selected ANN!
                if (!dtIds.contains(dtId))			continue;
                // Store usefull document type data.
                QString const			&dtName		= dtMdl->data(dtMdl->index(i, DocumentTypesModel::Name)).toString();
                QByteArray const		&baZrds		= dtMdl->data(dtMdl->index(i, DocumentTypesModel::RecognitionData)).toByteArray();
                // Unpack recognition data.
                RE::T::ZoneRecognizerData::List
                                        zrds		= RE::valueFromStreamedByteArray<RE::T::ZoneRecognizerData::List>(baZrds);
                // Make a summary of everything.
                foreach (RE::T::ZoneRecognizerData const &zrd, zrds) {
                    QString				zrdName		= zrd.parameters["name"].toString();
                    if (zrdName.isEmpty())			problems << tr("%1: %2 doesn't have a name.").arg(dtName).arg(RE::ClassNameToNaturalString(zrd.objectClassName));
                    else {
                        rCount						++;
                        dtMessages[dtName]			<< zrdName;
                    }
                }
            }
        }

        // There were problems...
        if (problems.count())
            _summaryLabel->setText			(problems.join("\r\n"));
        // Everything looks good. Make a list of the exported recognizers for the user to review them.
        else {
            QStringList			lSum;
            for (QMap<QString,QStringList>::const_iterator i=dtMessages.constBegin(); i!=dtMessages.constEnd(); ++i)
                lSum						<< tr("%1: %n object(s)", "", i.value().count()).arg(i.key());
            lSum							<< tr("Total: %n object(s)", "", rCount);
            _summaryLabel->setText			(lSum.join(", "));
        }

        // Finally emit the completion flag if it has changed.
        bool						noProblem	= problems.isEmpty();
        idx										= noProblem ? idx : QModelIndex();
        if (_annIndex!=idx) {
            _annIndex							= idx;
            emit								annIndexChanged(_annIndex);
        }
    }
private:
    Core					*_core;
    QLabel					*_summaryLabel;
    QListView				*_annList;
    QCheckBox				*_chkAnnBypass;
    QSpinBox				*_spnBxAnnThresh;
    QModelIndex				_annIndex;
signals:
    void					annIndexChanged				(QModelIndex const&);
};

// Recognizers selection page.
class REA::ClientBinaryGenerator::RecognizersSelectionPage : public QWizardPage {
Q_OBJECT;
public:
    // Constructor.
    /**/					RecognizersSelectionPage	(Core *c, QWidget *p=0)
    : QWizardPage(p), _core(c) {
        // Set title / subtitle.
        setTitle						(tr("Recognizers Selection"));
        setSubTitle						(tr("Please choose from the list which Recognizers you would like to use in the Client Binary."));
        setPixmap						(QWizard::WatermarkPixmap, QPixmap::fromImage(QImage(":/Resources/Images/MagnifyingGlass.png")));

        // Prepare layout.
        QVBoxLayout		*lyt			= new QVBoxLayout;

        // Create recognizers model.
        _customModel					= new QStandardItemModel(0, 1, this);

        // Create recognizers list.
        _recognizerList					= new QTreeView;
        _recognizerList->setSelectionMode	(QAbstractItemView::ExtendedSelection);
        _recognizerList->setSelectionBehavior	(QAbstractItemView::SelectRows);
        _recognizerList->setSizePolicy	(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        _recognizerList->setModel		(_customModel);
        lyt->addWidget					(_recognizerList, 5);

        // Create summary label.
        _summaryLabel					= new QLabel(tr("No ANN selected."));
        _summaryLabel->setWordWrap		(true);
        _summaryLabel->setSizePolicy	(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
        lyt->addWidget					(_summaryLabel, 0);

        // Finally set layout.
        setLayout						(lyt);

        // Connect events.
        connect(this, SIGNAL(recognizersRowsChanged(REA::ClientBinaryParameters::RecognizersRows)), this, SIGNAL(completeChanged()));
        connect(_recognizerList->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(rgModel_selectionChanged(QItemSelection,QItemSelection)));
        connect(_recognizerList, SIGNAL(expanded(QModelIndex)), this, SLOT(rgList_expanded(QModelIndex)));
    }
    // Updates selection to reflect the previous page choices.
    void					initializePage				() {
        // Reset page.
        QWizardPage::initializePage				();
        // Reset custom model.
        _customModel->clear						();
        _customModel->setHorizontalHeaderLabels(QStringList() << "Name");

        // ANN has been selected.
        if (_annIndex.isValid()) {
            // Build a list of all document types keys in the selected ANN.
            TrainedAnnsModel		*taMdl			= _core->realTrainedAnnsModel();
            QStringList								szDtIds		= taMdl->data(taMdl->index(_annIndex.row(), TrainedAnnsModel::DocumentTypesKeys)).toString().split(',');
            RE::T::UIntList							dtIds;
            foreach (QString const &dtId, szDtIds)	dtIds << dtId.toUInt();

            // Loop for each available document type.
            DocumentTypesModel		*dtMdl			= _core->realDocumentTypesModel();
            for (qint32 row=0; row<dtMdl->rowCount(); ++row) {
                // Store document type key.
                quint32				dtId			= dtMdl->data(dtMdl->index(row, 0), DocumentTypesModel::Key).toUInt();
                // Document type not in selected ann!
                if (!dtIds.contains(dtId))			continue;
                // Store usefull document type data.
                QString const		&dtName			= dtMdl->data(dtMdl->index(row, DocumentTypesModel::Name)).toString();
                QByteArray const	&baZrds			= dtMdl->data(dtMdl->index(row, DocumentTypesModel::RecognitionData)).toByteArray();
                // Unpack recognition data.
                RE::T::ZoneRecognizerData::List
                    zrds			= RE::valueFromStreamedByteArray<RE::T::ZoneRecognizerData::List>(baZrds);
                // Craft list item.
                QStandardItem		*itm			= new QStandardItem(dtName);
                itm->setData						(row, Qt::UserRole);
                itm->setFlags						(Qt::ItemIsEnabled);
                itm->setRowCount					(zrds.count());

                quint32				rC				= 0;
                quint32				zrdRow			= 0;
                foreach (RE::T::ZoneRecognizerData const &zrd, zrds) {
                    QStandardItem	*cItm			= new QStandardItem(zrd.parameters["name"].toString());
                    cItm->setFlags					(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
                    cItm->setData					(zrdRow++, Qt::UserRole);
                    itm->setChild					(rC++, cItm);
                }
                _customModel->appendRow				(itm);
            }
            _recognizerList->expandAll				();
            _recognizerList->selectAll				();
        }
        // No ANN Selected.
        else {
            // Build a list of all document types keys in the selected ANN.
            // Loop for each available document type.
            DocumentTypesModel		*dtMdl			= _core->realDocumentTypesModel();
            for (qint32 row=0; row<dtMdl->rowCount(); ++row) {
                // Store usefull document type data.
                QString const		&dtName			= dtMdl->data(dtMdl->index(row, DocumentTypesModel::Name)).toString();
                QByteArray const	&baZrds			= dtMdl->data(dtMdl->index(row, DocumentTypesModel::RecognitionData)).toByteArray();
                // Unpack recognition data.
                RE::T::ZoneRecognizerData::List
                    zrds			= RE::valueFromStreamedByteArray<RE::T::ZoneRecognizerData::List>(baZrds);
                // Craft list item.
                QStandardItem		*itm			= new QStandardItem(dtName);
                itm->setData						(row, Qt::UserRole);
                itm->setFlags						(Qt::ItemIsEnabled);
                itm->setRowCount					(zrds.count());

                quint32				rC				= 0;
                quint32				zrdRow			= 0;
                foreach (RE::T::ZoneRecognizerData const &zrd, zrds) {
                    QStandardItem	*cItm			= new QStandardItem(zrd.parameters["name"].toString());
                    cItm->setFlags					(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
                    cItm->setData					(zrdRow++, Qt::UserRole);
                    itm->setChild					(rC++, cItm);
                }
                _customModel->appendRow				(itm);
            }
            _recognizerList->collapseAll			();
        }
    }
    // Reimplementation of the isComplete flag.
    bool					isComplete					() const {
        return QWizardPage::isComplete() && !_recognizersIndexes.isEmpty();
    }
    // Accessors.
    REA::ClientBinaryParameters::RecognizersRows
                            recognizersRows				() const {
        ClientBinaryParameters::RecognizersRows
                                    toRet;
        // Cache selection.
        QModelIndexList				sel				= _recognizerList->selectionModel()->selectedRows();
        // ANN selected, multiple selection possible.
        if (_annIndex.isValid()) {
            // Make document types list ready.
            for (qint32 row=0; row<_customModel->rowCount(); ++row) {
                // Retrieve document type row.
                quint32				dtRow			= _customModel->data(_customModel->index(row, 0), Qt::UserRole).toUInt();
                toRet[dtRow]						= RE::T::UIntList();
            }
            // Fill document types list.
            foreach (QModelIndex const &recIdx, sel) {
                QModelIndex				dtIdx		= recIdx.parent();
                quint32					dtRow		= _customModel->data(dtIdx, Qt::UserRole).toUInt();
                toRet[dtRow]						<< recIdx.row();
            }
        }
        // No ANN selected, only 1 document type to export.
        else {
            // Store single document type index to use.
            if (sel.isEmpty())						return toRet;
            QModelIndex				dtIdx			= sel.first().parent();
            // Retrieve document type row.
            quint32					dtRow			= _customModel->data(_customModel->index(dtIdx.row(), 0), Qt::UserRole).toUInt();
            toRet[dtRow]							= RE::T::UIntList();
            // Fill document types list.
            foreach (QModelIndex const &recIdx, sel) {
                quint32					dtRow		= _customModel->data(dtIdx, Qt::UserRole).toUInt();
                toRet[dtRow]						<< recIdx.row();
            }
        }
        return									toRet;
    }
protected slots:
    void					setAnnIndex					(QModelIndex const &v)		{ if (_annIndex!=v) _annIndex = v; }
    void					rgModel_selectionChanged	(QItemSelection const&, QItemSelection const&) {
        QModelIndexList			sel				= _recognizerList->selectionModel()->selectedRows();
        bool					noProblem		= !sel.isEmpty();
        _summaryLabel->setText					(noProblem ? "" : tr("You must select at least one recognizer to be exported in the Client Binary to be able to proceed to the next step."));
        _recognizersIndexes						= sel;
        emit recognizersRowsChanged(recognizersRows());
    }
    void					rgList_expanded				(QModelIndex const &i) {
        // Don't do anything if mode than a document type can be selected.
        if (_annIndex.isValid())				return;
        // Collapse previously expanded document type.
        if (_recognizersIndexes.count())
            _recognizerList->collapse(_recognizersIndexes.first().parent());
        // Collapse others.
        qint32					iRow			= i.row();
        qint32					dtCount			= _customModel->rowCount();
        for (qint32 row=0; row<dtCount; ++row)
            if (iRow!=row)
                _recognizerList->collapse		(_customModel->index(row, 0));
    }
private:
    Core					*_core;
    QTreeView				*_recognizerList;
    QStandardItemModel		*_customModel;
    QLabel					*_summaryLabel;
    RE::T::UIntList			_documentTypesKeys;
    QModelIndex				_annIndex;
    QModelIndexList			_recognizersIndexes;
signals:
    void					recognizersRowsChanged		(REA::ClientBinaryParameters::RecognizersRows const&);
};

// Final page.
class REA::ClientBinaryGenerator::FinalPage : public QWizardPage {
Q_OBJECT
public:
    // Constructor.
    /**/					FinalPage					(Core *c, QWidget *p=0) : QWizardPage(p), _core(c) {
        setTitle						(tr("Tweaking and Validation"));
        setSubTitle						(tr("Please review the options you have choosed below, and make sure everything is correct. The next step will finalize the Client Binary generation."));
        setPixmap						(QWizard::WatermarkPixmap, QPixmap::fromImage(QImage(":/Resources/Images/Archive.png")));

        // Prepare layout.
        QVBoxLayout		*lyt			= new QVBoxLayout;

        // Create description label.
        _lblSumarry						= new QLabel();
        _lblSumarry->setWordWrap		(true);
        lyt->addWidget					(_lblSumarry);

        // Finally set layout.
        setLayout						(lyt);
    }
    // Updates selection to reflect the previous page choices.
    void					initializePage							() {
        QWizardPage::initializePage		();
        // ANN selected, multiple document types selection possible.
        if (_annIndex.isValid()) {
            // Build a list of all document types keys in the selected ANN.
            TrainedAnnsModel		*taMdl			= _core->realTrainedAnnsModel();
            QString					annName			= taMdl->data(taMdl->index(_annIndex.row(), TrainedAnnsModel::Name)).toString();
            QStringList				szDtIds			= taMdl->data(taMdl->index(_annIndex.row(), TrainedAnnsModel::DocumentTypesKeys)).toString().split(',');
            RE::T::UIntList							dtIds;
            foreach (QString const &dtId, szDtIds)	dtIds << dtId.toUInt();

            // Loop for each available document type.
            quint32					rCount			= 0;
            DocumentTypesModel		*dtMdl			= _core->realDocumentTypesModel();
            for (qint32 row=0; row<dtMdl->rowCount(); ++row) {
                // Store document type key.
                quint32				dtId			= dtMdl->data(dtMdl->index(row, DocumentTypesModel::Key)).toUInt();
                // Document type not in selected ann!
                if (!dtIds.contains(dtId))			continue;
                // Store document type name.
                rCount								+= _recognizersRows[row].count();
            }
            // Labelize summary.
            _lblSumarry->setText			(tr(
                                                "The Client Binary will be generated with the following settings:\r\n\r\n"
                                                "Path: %0\r\n"
                                                "\r\n"
                                                "Name: %1\r\n"
                                                "Copyright: %2\r\n"
                                                "\r\n"
                                                "Base ANN: %3\r\n"
                                                "Total Document Types: %4\r\n"
                                                "Total Recognizers: %5"
                                            )
                                                .arg(field("clientBinaryPath").toString())
                                                .arg(field("clientBinaryName").toString())
                                                .arg(field("clientBinaryCopyright").toString())
                                                .arg(annName)
                                                .arg(dtIds.count())
                                                .arg(rCount)
                                            );
        }
        // No ANN selected, single document type selection.
        else {
            // Retrieve single document info.
            quint32					dtRow			= _recognizersRows.keys().first();
            quint32					rCount			= _recognizersRows[dtRow].count();
            DocumentTypesModel		*dtMdl			= _core->realDocumentTypesModel();
            QString					dtName			= dtMdl->data(dtMdl->index(dtRow, DocumentTypesModel::Name)).toString();
            // Labelize summary.
            _lblSumarry->setText			(tr(
                                                "The Client Binary will be generated with the following settings:\r\n\r\n"
                                                "Path: %0\r\n"
                                                "\r\n"
                                                "Name: %1\r\n"
                                                "Copyright: %2\r\n"
                                                "\r\n"
                                                "Document Type: %3\r\n"
                                                "Total Recognizers: %4"
                                            )
                                                .arg(field("clientBinaryPath").toString())
                                                .arg(field("clientBinaryName").toString())
                                                .arg(field("clientBinaryCopyright").toString())
                                                .arg(dtName)
                                                .arg(rCount)
                                            );
        }
    }
public slots:
    void					setAnnIndex					(QModelIndex const &v) {
        if (_annIndex!=v)
            _annIndex = v;
    }
    void					setRecognizersRows			(REA::ClientBinaryParameters::RecognizersRows const &v) {
        if (_recognizersRows!=v)
            _recognizersRows = v;
    }
private:
    Core					*_core;
    QLabel					*_lblSumarry;
    QModelIndex				_annIndex;
    ClientBinaryParameters::RecognizersRows
                            _recognizersRows;
};


REA::ClientBinaryGenerator::ClientBinaryGenerator (QWidget *p)
: QWizard(p), _core(&REA::Core::Instance()) {
    setWindowTitle					(tr("Client Binary Generation Wizard"));
    setMinimumSize					(QSize(640, 450));
    // Add all pages.
    addPage							(_pgIntro=new IntroductionPage(_core));
    addPage							(_pgAnnSel=new AnnSelectionPage(_core));
    addPage							(_pgRecSel=new RecognizersSelectionPage(_core));
    addPage							(_pgFinal=new FinalPage(_core));

    // Connect recognizers selection page to others.
    connect(_pgAnnSel, SIGNAL(annIndexChanged(QModelIndex)), _pgRecSel, SLOT(setAnnIndex(QModelIndex)));
    // Connect final page to other pages.
    connect(_pgAnnSel, SIGNAL(annIndexChanged(QModelIndex)), _pgFinal, SLOT(setAnnIndex(QModelIndex)));
    connect(_pgRecSel, SIGNAL(recognizersRowsChanged(REA::ClientBinaryParameters::RecognizersRows)), _pgFinal, SLOT(setRecognizersRows(REA::ClientBinaryParameters::RecognizersRows)));
}

REA::ClientBinaryParameters REA::ClientBinaryGenerator::parameters () const {
    // Prepare result.
    ClientBinaryParameters	toRet;
    toRet.path						= field("clientBinaryPath").toString();
    toRet.name						= field("clientBinaryName").toString();
    toRet.copyright					= field("clientBinaryCopyright").toString();
    toRet.annThreshold				= field("clientBinaryAnnThreshold").toFloat();
    toRet.annRow					= _pgAnnSel->annIndex().row();
    toRet.recognizersRows			= _pgRecSel->recognizersRows();

    return							toRet;
}

#include "ClientBinaryGenerator.moc"
