#ifndef ClientBinaryGenerator_h
#define ClientBinaryGenerator_h

// Base classes.
#include <QtGui/QWizard>
// Required stuff.
#include "../../REATypes.h"
#include "../../Core.h"

// Forward declarations.
class QLabel;
class QListView;
class QStringListModel;
class QAbstractItemModel;

namespace REA {

class ClientBinaryGenerator : public QWizard {
Q_OBJECT
public:
	// Types.
	enum						{ PageIntroduction, PageAnnSelection, PageRecognizersSelection, PageFinalization, Count };
	// Constructor.
    /**/						ClientBinaryGenerator				(QWidget *p=0);
	// Accessors.
	ClientBinaryParameters		parameters							() const;
private:
	// Pages forward declarations.
	class						IntroductionPage;
	class						AnnSelectionPage;
	class						RecognizersSelectionPage;
	class						FinalPage;
	// Pages.
	IntroductionPage			*_pgIntro;
	AnnSelectionPage			*_pgAnnSel;
	RecognizersSelectionPage	*_pgRecSel;
	FinalPage					*_pgFinal;
	// Other members.
	Core						*_core;
	QModelIndex					_annIndex;
	QModelIndexList				_documentTypesIndexes;
};

}

#endif
