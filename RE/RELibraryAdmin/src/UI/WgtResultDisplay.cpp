#include "WgtResultDisplay.h"
#include "ui_WgtResultDisplay.h"
#include <RELibrary/RETypes>

WgtResultDisplay::WgtResultDisplay(QWidget *parent)
: QWidget(parent), _ui(new Ui::WgtResultDisplay) {
    _ui->setupUi(this);
}

WgtResultDisplay::~WgtResultDisplay() {
    delete _ui;
}

void WgtResultDisplay::setResult (QVariant const &v) {
    _ui->txtResultText->setPlainText    (v.userType()==QMetaType::QString ? v.toString() : RE::variantToJson(v));
}
