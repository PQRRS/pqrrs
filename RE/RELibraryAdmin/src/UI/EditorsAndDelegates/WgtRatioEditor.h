#ifndef REA_WgtRatioEditor_h
#define REA_WgtRatioEditor_h

// Base class.
#include <QtGui/QDoubleSpinBox>
// Required stuff.
#include <RELibrary/RETypes>
#include <QtCore/QVariant>

namespace REA {

class WgtRatioEditor : public QDoubleSpinBox {
public:
    Q_OBJECT
    Q_PROPERTY(QVariant value READ value WRITE setValue USER true NOTIFY valueChanged)

public:
    // Constructors.
    explicit            WgtRatioEditor		(QWidget *p=0)
    : QDoubleSpinBox(p), _inSetValue(false) {
        setSingleStep						(.05);
        setDecimals                         (4);
        connect(this, SIGNAL(valueChanged(double)), this, SLOT(self_valueChanged(double)));
    }
    // Accessors.
    QVariant            value				() const {
        double						v		= QDoubleSpinBox::value();
        if (_t==qMetaTypeId<RE::T::PositiveRatio>())
            return QVariant::fromValue(RE::T::PositiveRatio(v));
        else if (_t==qMetaTypeId<RE::T::NegativeRatio>())
            return QVariant::fromValue(RE::T::NegativeRatio(v));
        else
            return QVariant::fromValue(RE::T::Ratio(v));
    }
    void					setValue			(QVariant const &v) {
        _inSetValue							= true;
        _t									= v.userType();
        bool	positive					= _t!=qMetaTypeId<RE::T::NegativeRatio>();
        bool	negative					= _t!=qMetaTypeId<RE::T::PositiveRatio>();
        setRange							(negative ? -1.0 : 0.0, positive ? 1.0 : 0.0);
        RE::T::Ratio const			*r		= (RE::T::Ratio const*)v.constData();
        QDoubleSpinBox::setValue			(*r);
        _inSetValue							= false;
    }

protected slots:
    void					self_valueChanged	(double) {
        if (!_inSetValue)
            emit valueChanged();
    }

private:
    bool					_inSetValue;
    int						_t;

signals:
    void					valueChanged		();
};

}

#endif
