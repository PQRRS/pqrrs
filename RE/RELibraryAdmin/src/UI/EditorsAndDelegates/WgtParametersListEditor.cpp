#include "WgtParametersListEditor.h"
// Required stuff.
#include "../../REATypes.h"
#include <RELibrary/RETypes>
#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtCore/QSignalMapper>
#include <QtGui/QFormLayout>
#include <QtGui/QItemEditorFactory>
#include <QtGui/QCheckBox>

REA::WgtParametersListEditor::WgtParametersListEditor (QWidget *p)
: QWidget(p), _signalMapper(0), _inSetValue(true) {
    // Create UI.
    _frmMain								= new QFormLayout(this);
    _frmMain->setSpacing					(3);
    setLayout								(_frmMain);
    setFocusPolicy							(Qt::StrongFocus);
    _inSetValue								= false;
}
REA::WgtParametersListEditor::~WgtParametersListEditor () {
    if (_signalMapper)						delete _signalMapper;
    delete									_frmMain;
}

QVariantMap const& REA::WgtParametersListEditor::parameters () const {
    return _parameters;
}
void REA::WgtParametersListEditor::setParameters (QVariantMap const &v) {
    // Parameters are the same...
    if (_parameters==v)						return;
    // Clear widgets.
    clear									();
    _parameters								= v;
    _signalMapper							= new QSignalMapper();
    _inSetValue								= true;
    for (QVariantMap::const_iterator i=_parameters.constBegin(); i!=_parameters.constEnd(); ++i) {
        QString const				&name	= i.key();
        QVariant const				&val	= i.value();
        QItemEditorFactory const	*f		= QItemEditorFactory::defaultFactory();
        int                         t       = val.userType();
        QWidget						*wgt	= f->createEditor((QVariant::Type)t, this);
        if (!wgt) {
            Wrn << "Unable to instanciate editor for variant type" << t << ". The parameter" << name << "will not be editable!";
            delete wgt;
            continue;
        }
        // Connect the widget to the signal mapper.
        QMetaObject const			*mo		= wgt->metaObject();
        if (!mo) {
            Wrn << "Cannot get editor's meta-object for variant type" << t << ". The parameter" << name << "will not be editable!";
            delete wgt;
            continue;
        }
        QByteArray					vpName	= f->valuePropertyName((QVariant::Type)t);
        if (vpName.isEmpty()) {
            Wrn << "Cannot get editor's value property name for variant type" << t << ". The parameter" << name << "will not be editable!";
            delete wgt;
            continue;
        }
        wgt->setProperty					(vpName, val);
        int                                 pIndex	= mo->indexOfProperty(vpName);
        if (pIndex<0) {
            Wrn << "Cannot get editor's value property index for variant type" << t << ". The parameter" << name << "will not be editable!";
            delete wgt;
            continue;
        }

        QMetaMethod					pSignal     = mo->property(pIndex).notifySignal();
        QString						sName       = QString("2%1").arg(pSignal.signature());
        if (pIndex<0) {
            Wrn << "Cannot get editor's value property notifier name for variant type" << t << ". The parameter" << name << "will not be editable!";
            delete wgt;
            continue;
        }
        _signalMapper->setMapping			(wgt, wgt);
        connect(wgt, sName.toUtf8().constData(), _signalMapper, SLOT(map()));
        // Store the widget in the cache, and add it to the layout.
        _widgets[wgt]						= name;
        if (t==qMetaTypeId<RE::T::ZoneFilterData::List>())
            _frmMain->addRow                (wgt);
        else
            _frmMain->addRow                (RE::ClassNameToNaturalString(name), wgt);
    }
    connect(_signalMapper, SIGNAL(mapped(QWidget*)), this, SLOT(child_valueEdition(QWidget*)));
    _inSetValue								= false;
}
void REA::WgtParametersListEditor::child_valueEdition (QWidget *wgt) {
    QString const					&name	= _widgets[wgt];
    // Retrieve editor widget object.
    QItemEditorFactory const		*f		= QItemEditorFactory::defaultFactory();
    // Retrieve value property name.
    int                             t       = _parameters[name].userType();
    QVariant						value	= wgt->property(f->valuePropertyName((QVariant::Type)t));
    _parameters[name]						= value;
    if (!_inSetValue)
        emit parameterEdited(name, value);
}
void REA::WgtParametersListEditor::clear () {
    if (_parameters.isEmpty())				return;
    if (_signalMapper) {
        delete								_signalMapper;
        _signalMapper						= 0;
    }
    while (_frmMain->count()) {
        QLayoutItem					*lytItm	= _frmMain->takeAt(0);
        QLayout						*lyt	= lytItm->layout();
        if (lyt)							delete lyt;
        QWidget						*wgt	= lytItm->widget();
        if (wgt)							delete wgt;
        delete								lytItm;
    }

    _widgets.clear							();
    _parameters.clear						();
}
