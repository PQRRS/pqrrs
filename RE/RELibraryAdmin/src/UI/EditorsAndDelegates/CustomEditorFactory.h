#ifndef CustomEditorFactory_h
#define CustomEditorFactory_h

// Base class.
#include <QtGui/QItemEditorFactory>

namespace RE { class Engine; }
namespace REA { class Core; }

namespace REA {

class CustomEditorFactory : public QItemEditorFactory {
protected:
	// Constructor / Destructor, protected to avoid external instanciation.
    explicit                    CustomEditorFactory         (const QItemEditorFactory* sf);
	
public:
	QWidget*					createEditor				(QVariant::Type type, QWidget* parent) const;
	QByteArray					valuePropertyName			(QVariant::Type type) const;
    static void					installAsStandardFactory	();

private:
	RE::Engine					*_re;
	const QItemEditorFactory	*_standardFactory;
};

}

#endif
