#ifndef REA_WgtColorEditor_h
#define REA_WgtColorEditor_h

// Base class.
#include <QtGui/QWidget>
#include "ui_WgtColorEditor.h"
// Required stuff.
#include "../../REATypes.h"
#include <RELibrary/RETypes>
#include <QtGui/QPixmap>
#include <QtGui/QColorDialog>
#include <QtCore/QDebug>

namespace REA {

class WgtColorEditor : public QWidget {
public:
    Q_OBJECT
    Q_PROPERTY(QColor   value   READ value  WRITE setValue  USER true   NOTIFY valueChanged)

public:
    // Constructors / Destructor.
    explicit            WgtColorEditor              (QWidget *p=0)
    : QWidget(p), _ui(new Ui::WgtColorEditor),
    _inSetValue(true) {
        _ui->setupUi                  (this);
        _inSetValue					= false;
    }
    virtual             ~WgtColorEditor             () { delete _ui; }

    // Accessors.
    QColor              value                       () const            { return _value; }
    void				setValue					(QColor const &v)   {
        _value                      = v;
        _inSetValue					= true;
        _ui->spnBxRed->setValue       (_value.red());
        _ui->spnBxGreen->setValue     (_value.green());
        _ui->spnBxBlue->setValue      (_value.blue());
        updateColorUi				();
        _inSetValue					= false;
    }

protected slots:
    void                on_btnPalette_clicked       () {
        QColor          col         = QColorDialog::getColor(_value, this, "", QColorDialog::DontUseNativeDialog);
        if (!col.isValid())
            return;
        setValue                    (col);
        emit valueChanged           ();
    }
    void                on_spnBxRed_valueChanged    () {
        _value.setRed               (_ui->spnBxRed->value());
        updateColorUi				();
        if (!_inSetValue)
            emit valueChanged       ();
    }
    void                on_spnBxGreen_valueChanged  () {
        _value.setGreen             (_ui->spnBxGreen->value());
        updateColorUi				();
        if (!_inSetValue)
            emit valueChanged       ();
    }
    void                on_spnBxBlue_valueChanged   () {
        _value.setBlue              (_ui->spnBxBlue->value());
        updateColorUi				();
        if (!_inSetValue)
            emit valueChanged       ();
    }
    void				updateColorUi				() {
        QPixmap             px      (_ui->btnPalette->iconSize());
        px.fill                     (_value);
        _ui->btnPalette->setIcon      (px);
    }

private:
    Ui::WgtColorEditor      *_ui;
    QColor                  _value;
    bool					_inSetValue;

signals:
    void					valueChanged			();
};

}

#endif
