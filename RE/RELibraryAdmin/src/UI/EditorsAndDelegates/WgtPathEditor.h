#ifndef REA_WgtPathEditor_h
#define REA_WgtPathEditor_h

// Base class.
#include <QtGui/QLineEdit>
// Required stuff.
#include <RELibrary/RETypes>
#include <QtCore/QVariant>

namespace REA {
class WgtPathEditor : public QLineEdit {
public:
    Q_OBJECT
    Q_PROPERTY(QVariant	value READ value WRITE setValue USER true NOTIFY valueChanged)

public:
    // Constructors.
    Q_INVOKABLE				WgtPathEditor		(QWidget *p=0)
    : QLineEdit(p), _inSetValue(false) {
        connect(this, SIGNAL(textEdited(QString const&)), this, SLOT(self_textEdited(QString const&)));
    }
    // Accessors.
    QVariant				value				() const {
        QString						v		= QLineEdit::text();
        if (_t==qMetaTypeId<RE::T::FilePath>())
            return QVariant::fromValue(RE::T::FilePath(v));
        else if (_t==qMetaTypeId<RE::T::FolderPath>())
            return QVariant::fromValue(RE::T::FolderPath(v));
        else
            return QVariant::fromValue(RE::T::Path(v));
    }
    void					setValue			(QVariant const &v) {
        _inSetValue							= true;
        _t									= v.userType();
        RE::T::Path const			*p		= (RE::T::Path const*)v.constData();
        QLineEdit::setText					(*p);
        _inSetValue							= false;
    }

    protected slots:
        void					self_textEdited	(QString const&) {
            if (!_inSetValue)
                emit valueChanged();
        }

private:
    bool					_inSetValue;
    int						_t;

signals:
    void					valueChanged		();
};

}

#endif
