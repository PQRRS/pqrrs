#include "CustomEditorFactory.h"
// Require stuff.
#include "../../Core.h"
#include "WgtGrayValueEditor.h"
#include "WgtColorEditor.h"
#include "WgtFillColorEditor.h"
#include "WgtRatioEditor.h"
#include "WgtEnumItemEditor.h"
#include "WgtCVMListEditor.h"
#include "WgtPathEditor.h"
#include "WgtFilterStackEditor.h"
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <QtGui/QLineEdit>
#include <QtGui/QDoubleSpinBox>
#include <QtCore/QDebug>

REA::CustomEditorFactory::CustomEditorFactory (const QItemEditorFactory* sf)
: QItemEditorFactory(),
_re(&REA::Core::Instance().recognitionEngine()), _standardFactory(sf) {
}

QWidget* REA::CustomEditorFactory::createEditor (QVariant::Type tp, QWidget* p) const {
    int t   = (int)tp;
    // Boolean exception.
    if (t==QVariant::Bool)
        return new QCheckBox(p);
    // Double exception.
    else if (t==QVariant::Double) {
        QDoubleSpinBox     *wgt        = (QDoubleSpinBox*)_standardFactory->createEditor(tp, p);
        wgt->setDecimals                (4);
        return wgt;
    }
    // Edit a QColor.
    else if (t==QVariant::Color)
        return new WgtColorEditor(p);
    // Edit a gray value.
    else if (t==qMetaTypeId<RE::T::GrayValue>())
        return new WgtGrayValueEditor(p);
    // Fill color.
    else if (t==qMetaTypeId<RE::T::FillColor>())
        return new WgtFillColorEditor(p);
    // Ratio of any kind.
    else if (t>=qMetaTypeId<RE::T::Ratio>() && t<=qMetaTypeId<RE::T::NegativeRatio>())
        return new WgtRatioEditor(p);
    // Path of any kind.
    else if (t>=qMetaTypeId<RE::T::Path>() && t<=qMetaTypeId<RE::T::FilePath>())
        return new WgtPathEditor(p);
    // CV Matrix.
    else if (t==qMetaTypeId<RE::T::CVMatrices>())
        return new WgtCVMListEditor(p);
    // String...
    else if (t==QMetaType::QString)
        return new QLineEdit(p);
    // Filter stack...
    else if (t==qMetaTypeId<RE::T::ZoneFilterData::List>())
        return new WgtFilterStackEditor(p);
    // Enumeration type...
    else if (QMetaObject const *mo=_re->containingMetaObjectForEnumType(t))
            return new WgtEnumItemEditor(tp, mo, p);
    return _standardFactory->createEditor(tp, p);
}
QByteArray REA::CustomEditorFactory::valuePropertyName (QVariant::Type tp) const {
    int t       = (int)tp;
    if (t==QVariant::Bool)
        return "checked";
    else if (t==QVariant::Double ||
             t==QVariant::Color ||
             t==qMetaTypeId<RE::T::GrayValue>() ||
             t==qMetaTypeId<RE::T::FillColor>() ||
             (t>=qMetaTypeId<RE::T::Ratio>() && t<=qMetaTypeId<RE::T::NegativeRatio>()) ||
             (t>=qMetaTypeId<RE::T::Path>() && t<=qMetaTypeId<RE::T::FilePath>()) ||
             t==qMetaTypeId<RE::T::CVMatrices>() || t==qMetaTypeId<RE::T::ZoneFilterData::List>() ||
             _re->containingMetaObjectForEnumType(t)!=0)
        return "value";
    else if (t==QMetaType::QString)
        return "text";
    return _standardFactory->valuePropertyName(tp);
}
void REA::CustomEditorFactory::installAsStandardFactory () {
    QItemEditorFactory::setDefaultFactory(new CustomEditorFactory(QItemEditorFactory::defaultFactory()));
}
