#ifndef WgtFakeByteArrayEditor_h
#define WgtFakeByteArrayEditor_h

// Base class.
#include <QtGui/QWidget>

namespace REA {

class WgtFakeByteArrayEditor : public QWidget {
public:
    Q_OBJECT
    Q_PROPERTY(QByteArray	value	READ value	WRITE setValue	NOTIFY valueChanged	USER true)

public:
	// Constructor.
    explicit                WgtFakeByteArrayEditor			(QWidget *p=0, Qt::WindowFlags f=0) : QWidget(p, f)	{}
	// Accessors.
    QByteArray const&		value							() const					{ return _value; }
    void					setValue						(QByteArray const &v)		{ _value = v; emit valueChanged(); }

private:
	QByteArray				_value;

signals:
	void					valueChanged					();
};

}

#endif
