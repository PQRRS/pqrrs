#ifndef WgtEnumItemEditor_h
#define WgtEnumItemEditor_h

// Base class.
#include <QtGui/QComboBox>
// Required stuff.
#include <QtCore/QVariant>
// Forward declarations.
namespace REA { class Core; }

namespace REA {

class WgtEnumItemEditor : public QComboBox {
public:
    Q_OBJECT
    Q_PROPERTY(QVariant	value	READ value	WRITE setValue	USER true	NOTIFY valueChanged)

public:
	// Constructor / Destructor.
    explicit            WgtEnumItemEditor			(QVariant::Type type, QMetaObject const *mo, QWidget *parent=0);
	virtual				~WgtEnumItemEditor			();
	// Accessors.
	QVariant			value						()	const;
	void				setValue					(QVariant const &v);

protected slots:
	void				this_currentIndexChanged	(int);

private:
	bool				_inSetValue;

signals:
	void				valueChanged				();
};

}

#endif
