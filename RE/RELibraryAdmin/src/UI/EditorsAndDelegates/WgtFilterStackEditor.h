#ifndef REA_WgtFilterStackEditor_h
#define REA_WgtFilterStackEditor_h

// Base class.
#include <QtGui/QWidget>
// Required stuff.
#include <RELibrary/RETypes>
#include <QtGui/QItemSelection>
// Forward declarations.
namespace REA   { class ZoneFiltersModel; }
namespace Ui    { class WgtFilterStackEditor; }
class QMenu;

namespace REA {

class WgtFilterStackEditor : public QWidget {
public:
    Q_OBJECT
    Q_PROPERTY(RE::T::ZoneFilterData::List   value   READ value  WRITE setValue  USER true   NOTIFY valueChanged)

public:
    // Constructors / Destructor.
    explicit                    WgtFilterStackEditor                    (QWidget *p=0);
    virtual                     ~WgtFilterStackEditor                   ();
    // Accessors.
    RE::T::ZoneFilterData::List const&
                                value                                   () const;
    void                        setValue                                (RE::T::ZoneFilterData::List const &v);

protected slots:
    // Recognizer details buttons.
    void                        updateUi                                ();
    void                        on_btnFilterUp_clicked                  ();
    void                        on_btnFilterDown_clicked                ();
    void                        filters_selectionChanged                (QItemSelection const &s, QItemSelection const &d, bool dontUpdateEditors=false);
    void                        filterCreationMenu_triggered            (QAction *act);
    void                        on_btnFilterRemove_clicked              ();
    void                        on_wgtFilterParameters_parameterEdited  (QString const &name, QVariant const &value);

private:
    // UI construction.
    Ui::WgtFilterStackEditor    *_ui;
    QModelIndex                 _zrfIndex;
    // Filters list menu.
    QMenu                       *_filterCreationMenu;
    // Various models.
    REA::ZoneFiltersModel       *_filtersModel;
    QItemSelectionModel         *_filtersSelectionModel;

signals:
    void                        valueChanged                            ();
};

}

#endif
