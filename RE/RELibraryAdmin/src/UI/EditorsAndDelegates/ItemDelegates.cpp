#include "ItemDelegates.h"
#include "../../Core.h"
#include "../../Models/DocumentTypesModel.h"
#include "../../Models/AnnTestingResultsModel.h"
#include <QtCore/QString>
#include <QtCore/QByteArray>
#include <QtCore/QFileInfo>
#include <QtGui/QImage>
#include <QtGui/QCheckBox>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>
#include <QtGui/QPainter>
#include <QtGui/QFontMetrics>

REA::ReadOnly::ReadOnly (QObject *p) : QItemDelegate(p) {}
QWidget* REA::ReadOnly::createEditor (QWidget*, const QStyleOptionViewItem&, const QModelIndex&) const {
	return 0;
}

// Filename renderer.
REA::Filename::Filename (QObject *p) : ReadOnly(p) {
}
void REA::Filename::paint (QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const {
	QPalette::ColorGroup	cg			= o.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
	drawBackground						(p, o, i);
	p->setPen							(o.palette.color(cg, o.state & QStyle::State_Selected ? QPalette::HighlightedText : QPalette::Text));
	p->drawText							(o.rect, Qt::TextSingleLine, QFileInfo(i.data().toString()).baseName());
}
QSize REA::Filename::sizeHint (const QStyleOptionViewItem &o, const QModelIndex &i) const {
	QFontMetrics			fm			(o.font);
	QString					fbn			= QFileInfo(i.data().toString()).baseName();
	return					QSize		(fm.width(fbn), fm.height());
}

REA::SpinBox::SpinBox (QObject *p, int min, int max) : QItemDelegate(p), _min(min), _max(max) {}
QWidget* REA::SpinBox::createEditor (QWidget *p, const QStyleOptionViewItem&, const QModelIndex&) const {
	QSpinBox				*ed			= new QSpinBox(p);
	ed->setRange						(_min, _max);
	return ed;
}

// Sample Image Counter delegate.
REA::DocumentTypeStatus::DocumentTypeStatus (QObject *p) : ReadOnly(p) {
}
void REA::DocumentTypeStatus::paint (QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const {
	// Get count for images used for training.
	int						cnt			= REA::DocumentTypesModel::trainableSampleImagesCount(QString("documentType_id=%1").arg(i.sibling(i.row(), 0).data().toInt()));
	// i.data().toInt();
	QImage					img;
    if		(cnt<50)		img			= QImage(":/Resources/Icons/exclamation");
    else if	(cnt<100)		img			= QImage(":/Resources/Icons/caution");
	else					img			= QImage(":/Resources/Icons/accept");
	// Draw everything.
	drawBackground						(p, o, i);
	p->drawImage						(o.rect.topLeft(), img, img.rect());
}
QSize REA::DocumentTypeStatus::sizeHint (const QStyleOptionViewItem&, const QModelIndex&) const {
    return QSize(16, 16);
}

// Trained Ann Status delegate.
REA::TrainedAnnStatus::TrainedAnnStatus (QObject *p) : ReadOnly(p) {
}
void REA::TrainedAnnStatus::paint (QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const {
	// Retrieve status severity level.
	bool					trained		= i.data(Qt::UserRole).toBool();
	QImage					img			(trained ? ":/Resources/Icons/accept" : ":/Resources/Icons/caution");
	// Draw everything.
	drawBackground			(p, o, i);
	p->drawImage			(o.rect.topLeft(), img, img.rect());
}
QSize REA::TrainedAnnStatus::sizeHint (const QStyleOptionViewItem &o, const QModelIndex &i) const {
	Q_UNUSED(o);
	Q_UNUSED(i);
	return QSize(16, 16);
}

// Signature renderer.
REA::Signature::Signature (QObject *p) : ReadOnly(p) {}
void REA::Signature::paint (QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const {
	// Retrieve signature data.
	QByteArray const		&sigData	= i.data().toByteArray();
	QPalette::ColorGroup	cg			= o.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
	
	if (!sigData.length()) {
		drawBackground					(p, o, i);
		p->setPen						(o.palette.color(cg, o.state & QStyle::State_Selected ? QPalette::HighlightedText : QPalette::Text));
		p->drawText						(o.rect, Qt::TextSingleLine, tr("Not yet computed."));
	}
	else {
		// Cached image not yet drawn?
        QImage				img			(sigData.length(), 1, QImage::Format_Indexed8);
        img.setColorTable				(RE::Engine::grayscalePalette());
        memcpy							(img.bits(), sigData, sigData.length());
        img								= img.convertToFormat(QImage::Format_ARGB32);
        // Draw image on surface.
		QRect	dr						= o.rect;
		dr.setX							(dr.x()+2);
		dr.setY							(dr.y()+2);
		dr.setWidth						(dr.width()-2);
		dr.setHeight					(dr.height()-2);
		p->drawImage					(dr, img, img.rect());
	}
}
QSize REA::Signature::sizeHint (const QStyleOptionViewItem &o, const QModelIndex &i) const {
	Q_UNUSED(o)
	Q_UNUSED(i)
	return QSize(1, 200);
}
