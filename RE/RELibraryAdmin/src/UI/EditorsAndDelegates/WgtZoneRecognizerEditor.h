#ifndef REA_WgtZoneRecognizerEditor_h
#define REA_WgtZoneRecognizerEditor_h

// Base class.
#include <QtGui/QTabWidget>
// Required stuff.
#include <RELibrary/RETypes>

// Forward declarations.
namespace Ui { class WgtZoneRecognizerEditor; }

namespace REA {

class WgtZoneRecognizerEditor : public QTabWidget {
public:
    Q_OBJECT
    Q_PROPERTY(RE::T::ZoneRecognizerData value READ value WRITE setValue USER true NOTIFY valueChanged)

public:
    // Constructor / Destructor.
    explicit        WgtZoneRecognizerEditor             (QWidget *p=0);
    virtual         ~WgtZoneRecognizerEditor            ();

    // Accessors.
    RE::T::ZoneRecognizerData const&
                    value                               () const;
    void            setValue                            (RE::T::ZoneRecognizerData const &v);

protected slots:
    void            on_wgtParameters_parameterEdited    (QString name, QVariant value);
    void            on_wgtFilters_valueChanged          ();

private:
    Ui::WgtZoneRecognizerEditor     *_ui;
    RE::T::ZoneRecognizerData       _value;

signals:
    void            parameterEdited                     (QString name, QVariant value);
    void            filtersChanged                      ();
    void            valueChanged                        ();
};

}

#endif
