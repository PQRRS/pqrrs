#include "WgtZoneRecognizerEditor.h"
#include "ui_WgtZoneRecognizerEditor.h"

REA::WgtZoneRecognizerEditor::WgtZoneRecognizerEditor(QWidget *parent)
: QTabWidget(parent), _ui(new Ui::WgtZoneRecognizerEditor) {
    _ui->setupUi            (this);
}
REA::WgtZoneRecognizerEditor::~WgtZoneRecognizerEditor() {
    delete _ui;
}

RE::T::ZoneRecognizerData const& REA::WgtZoneRecognizerEditor::value () const {
    return _value;
}
void REA::WgtZoneRecognizerEditor::setValue (RE::T::ZoneRecognizerData const &v) {
    _value = v;
    _ui->wgtParameters->setParameters   (_value.parameters);
    _ui->wgtFilters->setValue           (_value.filterStack);
}


void REA::WgtZoneRecognizerEditor::on_wgtParameters_parameterEdited (QString name, QVariant value) {
    _value.parameters[name] = value;
    emit parameterEdited(name, value);
    emit valueChanged();
}
void REA::WgtZoneRecognizerEditor::on_wgtFilters_valueChanged () {
    _value.filterStack  = _ui->wgtFilters->value();
    emit filtersChanged();
    emit valueChanged();
}
