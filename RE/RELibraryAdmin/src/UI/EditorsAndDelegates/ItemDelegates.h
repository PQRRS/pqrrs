#ifndef ItemDelegates_h
#define ItemDelegates_h

// Base classes and required stuff.
#include <QtSql/QSqlRelationalDelegate>
#include <QtGui/QItemDelegate>

namespace REA {

// Read-only renderer.
class ReadOnly : public QItemDelegate {
Q_OBJECT
public:
    explicit        ReadOnly				(QObject *p);
	QWidget*		createEditor			(QWidget *p, const QStyleOptionViewItem &option, const QModelIndex &i) const;
};

// Filename renderer.
class Filename : public ReadOnly {
Q_OBJECT
public:
    explicit        Filename				(QObject *p);
protected:
	void			paint					(QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const;
	QSize			sizeHint				(const QStyleOptionViewItem &o, const QModelIndex &i) const;
};

// Spinbox item delegate.
class SpinBox : public QItemDelegate {
Q_OBJECT
public:
    explicit        SpinBox					(QObject *p, int min=INT_MIN, int max=INT_MAX);
	QWidget*		createEditor			(QWidget *p, const QStyleOptionViewItem &option, const QModelIndex &i) const;
private:
	int				_min, _max;
};

// Sample images counter delegate.
class DocumentTypeStatus : public ReadOnly {
Q_OBJECT
public:
    explicit        DocumentTypeStatus		(QObject *p);
protected:
	void			paint					(QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const;
	QSize			sizeHint				(const QStyleOptionViewItem &o, const QModelIndex &i) const;
};

class TrainedAnnStatus : public ReadOnly {
Q_OBJECT
public:
    explicit        TrainedAnnStatus		(QObject *p);
protected:
	void			paint					(QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const;
	QSize			sizeHint				(const QStyleOptionViewItem &o, const QModelIndex &i) const;
};

// Signature renderer.
class Signature : public ReadOnly {
Q_OBJECT
public:
    explicit        Signature				(QObject *p);
protected:
	void			paint					(QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const;
	QSize			sizeHint				(const QStyleOptionViewItem &o, const QModelIndex &i) const;
};

}

#endif
