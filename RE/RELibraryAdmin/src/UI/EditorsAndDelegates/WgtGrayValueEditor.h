#ifndef REA_WgtGrayValueEditor_h
#define REA_WgtGrayValueEditor_h

// Base class.
#include <QtGui/QWidget>
#include "ui_WgtGrayValueEditor.h"
// Required stuff.
#include "../../REATypes.h"
#include <RELibrary/RETypes>
#include <QtGui/QPixmap>
#include <QtGui/QColorDialog>
#include <QtCore/QDebug>

namespace REA {

class WgtGrayValueEditor : public QWidget {
public:
    Q_OBJECT
    Q_PROPERTY(RE::T::GrayValue   value   READ value  WRITE setValue  USER true   NOTIFY valueChanged)

public:
    // Constructors / Destructor.
    explicit            WgtGrayValueEditor          (QWidget *p=0)
    : QWidget(p), _ui(new Ui::WgtGrayValueEditor),
    _inSetValue(true) {
        _ui->setupUi        (this);
        _inSetValue         = false;
    }
    virtual             ~WgtGrayValueEditor         () { delete _ui; }

    // Accessors.
    RE::T::GrayValue    value                       () const            { return _value; }
    void				setValue					(RE::T::GrayValue const &v)   {
        _inSetValue					= true;
        _value                      = v;
        _ui->spnBxGray->setValue      (_value.value());
        updateColorUi				();
        _inSetValue					= false;
    }

protected slots:
    void                on_btnPalette_clicked       () {
        QColor          col         = QColorDialog::getColor(qRgb(_value.value(),_value.value(),_value.value()), this, "", QColorDialog::DontUseNativeDialog);
        if (!col.isValid())
            return;
        setValue                    (qGray(col.rgb()));
        emit valueChanged           ();
    }
    void                on_spnBxGray_valueChanged   () {
        _value                      = _ui->spnBxGray->value();
        updateColorUi				();
        if (!_inSetValue)
            emit valueChanged       ();
    }
    void				updateColorUi				() {
        QPixmap             px      (_ui->btnPalette->iconSize());
        quint8 const        g       = _value.value();
        px.fill                     (qRgb(g,g,g));
        _ui->btnPalette->setIcon      (px);
    }

private:
    Ui::WgtGrayValueEditor  *_ui;
    RE::T::GrayValue        _value;
    bool					_inSetValue;

signals:
    void					valueChanged			();
};

}

#endif
