#include "WgtCVMListEditor.h"
#include "ui_WgtCVMListEditor.h"
// Required stuff.
#include "../../REATypes.h"
#include <RELibrary/RETypes>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaType>
#include <QtGui/QAction>
#include <QtGui/QMenu>
#include <QtCore/QDebug>

REA::WgtCVMListEditor::WgtCVMListEditor (QWidget *p)
: QWidget(p), _ui(new Ui::WgtCVMListEditor),
_inSetValue(true) {
    _ui->setupUi	(this);
    // Create zone creation menu and connect it to controller.
    QStringList				names;
    QMenu					*mnu	= new QMenu(this);
    _ui->btnAdd->setMenu				(mnu);
    connect(mnu, SIGNAL(triggered(QAction*)), this, SLOT(addMenu_triggered(QAction*)));
    int                     first   = RE::FirstMatrixMetatypeId();
    int                     last    = RE::LastMatrixMetatypeId();
    for (int i=first; i<last; i++) {
        char const			*n		= QMetaType::typeName(i);
        QAction				*act	= new QAction(RE::ClassNameToNaturalString(n), mnu);
        act->setData				(i);
        mnu->addAction				(act);
    }
    _inSetValue						= false;
}
REA::WgtCVMListEditor::~WgtCVMListEditor () {
    delete _ui;
}

RE::T::CVMatrices const& REA::WgtCVMListEditor::value () const {
    return _matrices;
}
void REA::WgtCVMListEditor::setValue (RE::T::CVMatrices const &v) {
    _ui->lstWgtMain->clear               ();
    _inSetValue                         = true;
    _matrices                           = v;
    quint32						count	= _matrices.count();
    for (quint32 i=0; i<count; i++) {
        RE::T::CVMatrix         *m		= &_matrices[i];
        int						mtId	= m->metaTypeId();
        char const				*n		= QMetaType::typeName(mtId);
        _ui->lstWgtMain->addItem         (RE::ClassNameToNaturalString(n));
    }
    _inSetValue							= false;
}

void REA::WgtCVMListEditor::addMenu_triggered (QAction *act) {
    int							mtId	= act->data().toInt();
    RE::T::CVMatrix             *pM		= (RE::T::CVMatrix*)QMetaType::construct(mtId);
    _matrices                       	<< *pM;
    if (!_inSetValue)
        emit    valueChanged            ();
    QMetaType::destroy                  (mtId, pM);
    _ui->lstWgtMain->addItem				(RE::ClassNameToNaturalString(QMetaType::typeName(mtId)));
}
void REA::WgtCVMListEditor::on_btnRemove_clicked () {
    int							row		= _ui->lstWgtMain->currentRow();
    if (row<0 || row>=_matrices.count())
        return;
    _matrices.takeAt					(row);
    if (!_inSetValue)
        emit    valueChanged            ();
    _ui->lstWgtMain->takeItem            (row);
}
void REA::WgtCVMListEditor::on_btnUp_clicked () {
    int							row		= _ui->lstWgtMain->currentRow();
    if (row<1 || row>=_matrices.count())
        return;
    _matrices.swap                      (row-1, row);
    if (!_inSetValue)
        emit    valueChanged            ();
    QListWidgetItem				*itm	= _ui->lstWgtMain->takeItem(row);
    _ui->lstWgtMain->insertItem			(row-1, itm);
    _ui->lstWgtMain->setCurrentItem		(itm);
}
void REA::WgtCVMListEditor::on_btnDown_clicked () {
    int							row		= _ui->lstWgtMain->currentRow();
    if (row<0 || row>=_matrices.count()-1)
        return;
    _matrices.swap						(row, row+1);
    if (!_inSetValue)
        emit    valueChanged            ();
    QListWidgetItem				*itm	= _ui->lstWgtMain->takeItem(row);
    _ui->lstWgtMain->insertItem			(row+1, itm);
    _ui->lstWgtMain->setCurrentItem		(itm);
}
