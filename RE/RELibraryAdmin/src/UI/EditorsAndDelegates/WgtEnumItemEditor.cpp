#include "WgtEnumItemEditor.h"
// Required stuff.
#include "../../Core.h"
#include "../../REATypes.h"
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

REA::WgtEnumItemEditor::WgtEnumItemEditor (QVariant::Type t, QMetaObject const *mo, QWidget *p)
: QComboBox(p), _inSetValue(true) {
    // First, find to which metaobject belongs the enum.
    QMetaEnum const		&me		(RE::MetaForEnumType(mo, t));
    QString				meName	(me.name());
    qint32				pfxCnt	(meName.count());
    quint32 const		cEnum	= me.keyCount();
    for (quint32 i=0; i<cEnum; i++) {
        QString			enName	(QString(me.key(i)));
        if (enName.left(pfxCnt)==meName)
            enName				= enName.mid(pfxCnt);
        addItem					(RE::ClassNameToNaturalString(enName), RE::VariantForEnumMetatypeId(t, me.value(i)));
    }
    connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(this_currentIndexChanged(int)));
    _inSetValue					= false;
}
REA::WgtEnumItemEditor::~WgtEnumItemEditor () {
}

QVariant REA::WgtEnumItemEditor::value () const {
    return itemData(currentIndex());
}
void REA::WgtEnumItemEditor::setValue (QVariant const &v) {
    _inSetValue					= true;
    for (int i=0; i<count(); i++) {
        if (RE::IntValueForEnumVariant(itemData(i))!=RE::IntValueForEnumVariant(v))
            continue;
        setCurrentIndex(i);
        break;
    }
    _inSetValue					= false;
}

void REA::WgtEnumItemEditor::this_currentIndexChanged (int) {
    if (!_inSetValue)
        emit valueChanged();
}
