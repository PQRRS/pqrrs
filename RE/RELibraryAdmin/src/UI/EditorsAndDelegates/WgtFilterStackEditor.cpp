#include "WgtFilterStackEditor.h"
#include "ui_WgtFilterStackEditor.h"
// Required stuff.
#include "../../Core.h"
#include "../../REATypes.h"
#include "../../Models/ZoneFiltersModel.h"
#include <QtGui/QMenu>
#include <QtCore/QDebug>

REA::WgtFilterStackEditor::WgtFilterStackEditor (QWidget *p)
: QWidget(p), _ui(new Ui::WgtFilterStackEditor) {
    _ui->setupUi                                 (this);

    // Create filters creation menu and connect it to controller.
    _filterCreationMenu                         = new QMenu(this);
    _ui->btnFilterAdd->setMenu                   (_filterCreationMenu);
    connect                                     (_filterCreationMenu, SIGNAL(triggered(QAction*)), this, SLOT(filterCreationMenu_triggered(QAction*)));

    // Prepare vars for creation menus.
    QStringList                 names;
    RE::Engine                  &re             = REA::Core::Instance().recognitionEngine();
    // Get and sort all recognizers names.
    names                                       = re.filtersFactory().registeredTypes().keys();
    qSort                                       (names);
    // Populate the preprocessing filters and filters creation menu.
    foreach (QString const &n, names) {
        QAction                 *act1           = new QAction(RE::ClassNameToNaturalString(n), _filterCreationMenu);
        act1->setData                           (n);
        _filterCreationMenu->addAction          (act1);
    }

    // Create models etc.
    _filtersModel                               = new REA::ZoneFiltersModel(this);
    _filtersSelectionModel                      = new QItemSelectionModel(_filtersModel);
    QItemSelectionModel         *oldSelMdl      = _ui->lstVwFilters->selectionModel();
    _ui->lstVwFilters->setModel                  (_filtersModel);
    _ui->lstVwFilters->setSelectionModel         (_filtersSelectionModel);
    if (oldSelMdl && oldSelMdl!=_filtersSelectionModel)
        delete oldSelMdl;

    // Connect stuff...
    connect(_filtersSelectionModel, SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(filters_selectionChanged(QItemSelection,QItemSelection)));
}
REA::WgtFilterStackEditor::~WgtFilterStackEditor () {
    delete _ui;
}

RE::T::ZoneFilterData::List const& REA::WgtFilterStackEditor::value () const {
    return _filtersModel->filters();
}
void REA::WgtFilterStackEditor::setValue (RE::T::ZoneFilterData::List const &v) {
    _ui->wgtFilterParameters->clear();
    _filtersModel->setFilters(v);
}

void REA::WgtFilterStackEditor::updateUi () {
    bool        isFilterSelected        = _zrfIndex.isValid();
    _ui->btnFilterUp->setEnabled        (isFilterSelected && _zrfIndex.row()>0);
    _ui->btnFilterDown->setEnabled      (isFilterSelected && _zrfIndex.row()<_filtersModel->rowCount()-1);
    _ui->btnFilterRemove->setEnabled    (isFilterSelected);
    _ui->btnFilterExport->setEnabled    (isFilterSelected);
}

// Recognizer details buttons.
void REA::WgtFilterStackEditor::on_btnFilterUp_clicked () {
    if (!_zrfIndex.isValid() || !_filtersModel->moveUp(_zrfIndex.row()))
        return;
    filters_selectionChanged        (QItemSelection(), QItemSelection(), true);
    emit valueChanged();
}
void REA::WgtFilterStackEditor::on_btnFilterDown_clicked () {
    if (!_zrfIndex.isValid() || !_filtersModel->moveDown(_zrfIndex.row()))
        return;
    filters_selectionChanged        (QItemSelection(), QItemSelection(), true);
    emit valueChanged();
}
void REA::WgtFilterStackEditor::filters_selectionChanged (QItemSelection const &s, QItemSelection const &d, bool dontUpdateEditors) {
    Q_UNUSED(s)
    Q_UNUSED(d)
    Q_UNUSED(dontUpdateEditors);
    QModelIndexList const       &sel        = _filtersSelectionModel->selectedRows();
    _zrfIndex                               = sel.count()==1 ? sel.first() : QModelIndex();
    updateUi                        ();
    // Retrieve filter data information.
    RE::T::ZoneFilterData       zrd         = _filtersModel->data(_zrfIndex, Qt::UserRole).value<RE::T::ZoneFilterData>();
    QVariantMap const           &params     = Core::Instance().recognitionEngine().filterParameters(zrd.objectClassName);
    foreach (QString const &key, params.keys()) {
        if (!zrd.parameters.contains(key)) {
            zrd.parameters[key]             = params[key];
            Wrn                             << "New parameter" << key << "has been added to editor.";
        }
    }
    foreach (QString const &key, zrd.parameters.keys()) {
        if (!params.contains(key)) {
            zrd.parameters.remove           (key);
            _filtersModel->removeParameter(_zrfIndex, key);
            Wrn                             << "Old parameter" << key << "has been removed from editor.";
        }
    }
    // Populate the parameters list.
    _ui->wgtFilterParameters->setParameters(zrd.parameters);
}
void REA::WgtFilterStackEditor::filterCreationMenu_triggered (QAction *act) {
    _filtersModel->createFilter(REA::Core::Instance().recognitionEngine().makeZoneFilterData(act->data().toString()));
    emit valueChanged();
}
void REA::WgtFilterStackEditor::on_btnFilterRemove_clicked () {
    if (!_zrfIndex.isValid())   return;
    _filtersModel->removeRows   (_zrfIndex.row(), 1, _zrfIndex.parent());
    emit valueChanged();
}
void REA::WgtFilterStackEditor::on_wgtFilterParameters_parameterEdited (QString const &name, QVariant const &value) {
    if (!_zrfIndex.isValid())   return;
    // Set parameter for selected filter.
    _filtersModel->setParameter (_zrfIndex, name, value);
    emit valueChanged();
}
