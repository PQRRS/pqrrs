#ifndef REA_WgtFillColorEditor_h
#define REA_WgtFillColorEditor_h

// Base class.
#include <QtGui/QWidget>
#include "ui_WgtFillColorEditor.h"
// Required stuff.
#include "../../REATypes.h"
#include <RELibrary/RETypes>
#include <QtCore/QDebug>

namespace REA {

class WgtFillColorEditor : public QWidget {
public:
    Q_OBJECT
    Q_PROPERTY(RE::T::FillColor	value	READ value	WRITE setValue	USER true	NOTIFY valueChanged)

public:
	// Constructors / Destructor.
    explicit                WgtFillColorEditor                  (QWidget *p=0)
    : QWidget(p), _ui(new Ui::WgtFillColorEditor),
    _inSetValue(true) {
        _ui->setupUi            (this);
        _inSetValue             = false;
	}
    virtual                 ~WgtFillColorEditor                 () { delete _ui; }

    // Accessors.
	RE::T::FillColor const&	value								() const {
		return _fillColor;
	}
	void					setValue							(RE::T::FillColor const &v) {
		_inSetValue							= true;
		_fillColor							= v;
        _ui->chkBxMain->setChecked			(_fillColor.useSourceImage);
        _ui->wgtGrayValueEditor->setValue	(_fillColor.color);
		_inSetValue							= false;
	}

protected slots:
	void					on_chkBxMain_toggled				() {
        _fillColor.useSourceImage			= _ui->chkBxMain->isChecked();
		if (!_inSetValue)
			emit valueChanged();
	}
	void					on_wgtGrayValueEditor_valueChanged	() {
        _fillColor.color					= _ui->wgtGrayValueEditor->value();
		if (!_inSetValue)
			emit valueChanged();
	}

private:
    Ui::WgtFillColorEditor	*_ui;
	RE::T::FillColor		_fillColor;
	bool					_inSetValue;

signals:
	void					valueChanged	();
};

}

#endif
