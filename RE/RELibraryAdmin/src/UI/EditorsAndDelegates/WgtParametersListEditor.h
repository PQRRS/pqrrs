#ifndef REA_WgtParametersListEditor_h
#define REA_WgtParametersListEditor_h

// Base class and private UI.
#include <QtGui/QWidget>
// Required stuff.
#include <RELibrary/RETypes>

// Forward declarations.
class QFormLayout;
class QSignalMapper;

namespace REA {

class WgtParametersListEditor : public QWidget {
public:
    Q_OBJECT

public:
	// Constructors / Destructor.
    explicit                    WgtParametersListEditor     (QWidget *parent=0);
	virtual						~WgtParametersListEditor	();
	// Accessors.
	QVariantMap const&			parameters					() const;
	void						setParameters				(QVariantMap const &v);

public slots:
	void						child_valueEdition			(QWidget *wgt);
	void						clear						();

private:
	typedef QHash<QWidget*,QString> WidgetsCache;
	// Widgets and UI.
    QFormLayout					*_frmMain;
	// Widgets cache.
	QVariantMap					_parameters;
	QSignalMapper				*_signalMapper;
	WidgetsCache				_widgets;
	bool						_inSetValue;

signals:
	void						parameterEdited				(QString name, QVariant value);
};

}

#endif
