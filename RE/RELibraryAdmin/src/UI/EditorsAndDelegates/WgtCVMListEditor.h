#ifndef REA_WgtCVMListEditor_h
#define REA_WgtCVMListEditor_h

// Base class and UI.
#include <QtGui/QWidget>
// Required stuff.
#include <RELibrary/RETypes>
// Forward declarations.
namespace Ui { class WgtCVMListEditor; }
class QMenu;
class QAction;

namespace REA {

class WgtCVMListEditor : public QWidget {
public:
    Q_OBJECT
    Q_PROPERTY(RE::T::CVMatrices    value	READ value	WRITE setValue NOTIFY valueChanged)

public:
    // Constructors / Destructor.
    explicit                    WgtCVMListEditor		(QWidget *parent=0);
    virtual						~WgtCVMListEditor		();
    // Accessors.
    RE::T::CVMatrices const&    value					() const;
    void						setValue				(RE::T::CVMatrices const &v);

protected slots:
    void						addMenu_triggered		(QAction *act);
    void						on_btnRemove_clicked	();
    void						on_btnUp_clicked		();
    void						on_btnDown_clicked		();

private:
    Ui::WgtCVMListEditor		*_ui;
    RE::T::CVMatrices           _matrices;
    bool						_inSetValue;

signals:
    void						valueChanged			();
};

}

#endif
