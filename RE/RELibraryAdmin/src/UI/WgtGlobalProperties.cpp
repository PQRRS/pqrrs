#include "WgtGlobalProperties.h"
#include "ui_WgtGlobalProperties.h"
// Required stuff.
#include "../Core.h"
#include "../Models/GlobalPropertiesModel.h"
#include <QtCore/QDebug>
#include <QtScript/QScriptEngine>

REA::WgtGlobalProperties::WgtGlobalProperties (QWidget *p)
: QTabWidget(p), _ui(new Ui::WgtGlobalProperties),
_core(&REA::Core::Instance()), _scriptEngine(0) {
    _ui->setupUi(this);

	_scriptEngine								= new QScriptEngine();
	
	// Configure script editor.
    JSEdit				*editor					= _ui->txtGlobalScript;
    editor->setColor							(JSEdit::Background,	QColor("#0c152b"));
    editor->setColor							(JSEdit::Normal,		QColor("#dddddd"));
    editor->setColor							(JSEdit::Comment,		QColor("#666666"));
    editor->setColor							(JSEdit::Number,		QColor("#dbf76c"));
    editor->setColor							(JSEdit::String,		QColor("#5ed363"));
    editor->setColor							(JSEdit::Operator,		QColor("#ff7729"));
    editor->setColor							(JSEdit::Identifier,	QColor("#fde15d"));
    editor->setColor							(JSEdit::Keyword,		QColor("#fde15d"));
    editor->setColor							(JSEdit::BuiltIn,		QColor("#9cb6d4"));
    editor->setColor							(JSEdit::Cursor,		QColor("#1e346b"));
    editor->setColor							(JSEdit::Marker,		QColor("#dbf76c"));
    editor->setColor							(JSEdit::BracketMatch,	QColor("#1ab0a6"));
    editor->setColor							(JSEdit::BracketError,	QColor("#a82224"));
    editor->setColor							(JSEdit::FoldIndicator,	QColor("#555555"));
    editor->setTabChangesFocus					(false);
    editor->setTabStopWidth                     (80);
    editor->setBracketsMatchingEnabled			(true);
    editor->setCodeFoldingEnabled				(true);
    editor->setTextWrapEnabled					(false);
    editor->setKeywords                         (editor->keywords() << "this" << "globs" << "strings" << "rejectionReasons" << "acceptationReasons");

    // Connect edition events to database commit function.
    connect(_ui->txtGlobalCopyright, SIGNAL(editingFinished()), this, SLOT(commitValues()));
    connect(_ui->txtGlobalScript, SIGNAL(textChanged()), this, SLOT(commitValues()));
}
REA::WgtGlobalProperties::~WgtGlobalProperties () {
    delete _ui;
}
void REA::WgtGlobalProperties::updateUi () {
    bool	projectOpened						= _core->projectFilename().length();
    if (!projectOpened) {
        _ui->txtGlobalCopyright->setText			("");
        _ui->txtGlobalScript->setPlainText		("");
        _ui->txtDatabaseVersion->setText			("");
        return;
    }
    GlobalPropertiesModel			&mdl		= (GlobalPropertiesModel&)_core->globalPropertiesModel();
    _ui->txtGlobalCopyright->setText				(mdl.valueForKey("Global.Copyright"));
    _ui->txtGlobalScript->setPlainText			(mdl.valueForKey("Global.Script"));
    _ui->txtDatabaseVersion->setText				(mdl.valueForKey("Database.Version"));
}

// Script checking.
void REA::WgtGlobalProperties::on_btnScriptCheck_clicked () {
    QString					script				= QString("({ globs: { %1 } })").arg(_ui->txtGlobalScript->toPlainText());
	QScriptValue			eval				= _scriptEngine->evaluate(script);
	if (_scriptEngine->hasUncaughtException()) {
		Wrn										<< "Script error:" << _scriptEngine->uncaughtException().toString() << "at line #" << _scriptEngine->uncaughtExceptionLineNumber() << ".";
		return;
	}
	Dbg											<< "Script passed verification.";
}
// When an editor releases focus, save database.
void REA::WgtGlobalProperties::commitValues () {
    bool	projectOpened						= _core->projectFilename().length();
    if (!projectOpened)
        return;
    GlobalPropertiesModel			&mdl		= (GlobalPropertiesModel&)_core->globalPropertiesModel();
    mdl.setValueForKey							("Global.Copyright", _ui->txtGlobalCopyright->text());
    mdl.setValueForKey							("Global.Script", _ui->txtGlobalScript->toPlainText());
}
