#include "WndMain.h"
#include "ui_WndMain.h"
// Required stuff.
#include "DlgAbout.h"
#include "EditorsAndDelegates/ItemDelegates.h"
#include "EditorsAndDelegates/CustomEditorFactory.h"
#include "Wizards/ClientBinaryGenerator.h"
#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtSql/QSqlRecord>
#include <QtGui/QMenu>
#include <QtGui/QAction>
#include <QtGui/QCloseEvent>
#include <QtGui/QFileDialog>
#include <QtGui/QStandardItem>
#include <QtGui/QStandardItemModel>

REA::WndMain*		REA::WndMain::_instance		= 0;

REA::WndMain::WndMain (QWidget *parent, Qt::WFlags flags)
: QMainWindow(parent, flags), _ui(new Ui::WndMain),
_viewMode(WndMain::ViewModeUndefined), _core(&REA::Core::Instance()), _consoleModel(0) {
	
	// For debugging only:
    #ifndef QT_NO_DEBUG
    #define TypeFullRegistration(name)      qRegisterMetaType<name>(#name);
    TypeFullRegistration                    (QtMsgType);
    #undef TypeFullRegistration
    #endif

    // Setup main UI.
    _ui->setupUi                            (this);
	
    // Create output model.
    _consoleModel                           = new QStandardItemModel(0, 2, this);
    _ui->tblVwOutput->setModel                (_consoleModel);
    on_btnClearConsole_clicked				();

    _instance                               = this;

    //#ifdef QT_RELEASE
    qInstallMsgHandler                      (REA::WndMain::staticMessageHandler);
    //#endif

    // Restore state / geometry.
    QSettings settings                      ("Pierre qui Roule EURL", "RELibraryAdmin");
    restoreGeometry                         (settings.value("WndMain::Geometry").toByteArray());
    restoreState                            (settings.value("WndMain::State").toByteArray());

    // Preparing User Interface.
    updateUi                                (WndMain::ViewModeDocumentTypes);
    updateRecentMenu                        ();
    _ui->mnuView->addAction                   (_ui->dkWgtConsole->toggleViewAction());
	
	// Prepare editor widgets factory.
    CustomEditorFactory::installAsStandardFactory();

	// Connect to core.
	connect(_core, SIGNAL(projectOpened()), this, SLOT(core_projectOpened()));
	connect(_core, SIGNAL(projectClosed()), this, SLOT(core_projectClosed()));
	connect(_core, SIGNAL(destroyed()), this, SLOT(core_destroyed()));
}
REA::WndMain::~WndMain () {
    // Save window state and geometry.
    QSettings s("Pierre qui Roule EURL", "RELibraryAdmin");
    s.setValue("WndMain::Geometry", saveGeometry());
    s.setValue("WndMain::State", saveState());
    // Delete UI and remove static instance from message handling.
    _instance       = 0;
    delete          _ui;
}

void REA::WndMain::staticMessageHandler (QtMsgType t, const char *m) {
	// No MainWindow yet instanciated.
	if (!_instance)
		return;
	else if (_instance->_consoleModel) {
		// Prepare color and icon.
		QColor					clr;
		switch (t) {
            case QtDebugMsg:	clr.setRgb(0x88, 0xff, 0x88); break;
            case QtWarningMsg:	clr.setRgb(0xff, 0xcc, 0x88); break;
            case QtCriticalMsg:	clr.setRgb(0xff, 0x88, 0x88); break;
            case QtFatalMsg:	clr.setRgb(0xff, 0x55, 0x55); break;
		}
		
		// Reformat message.
		QString					message		(m);
		// Split message to extract method / message.
        QString					sender      ("Unknown");
		if (message.contains(" # ")) {
			QStringList			msgs		= message.split(" # ");
            sender                          = msgs.takeAt(0);
            message                         = msgs.join(" # ");
        }
		
        _instance->_consoleMutex.lockInline ();
		// Cache model.
		QStandardItemModel		*mdl		= _instance->_consoleModel;
		// Create items row.
		quint32					rowCount	= mdl->rowCount();
		QList<QStandardItem*>	row;
        row									<< new QStandardItem(sender) << new QStandardItem(message);
		mdl->appendRow						(row);
		mdl->setData						(mdl->index(rowCount, 1), clr, Qt::ForegroundRole);
		mdl->setData						(mdl->index(rowCount, 2), clr, Qt::ForegroundRole);
        QMetaObject::invokeMethod			(_instance->_ui->tblVwOutput, "scrollToBottom", Qt::QueuedConnection);
        _instance->_consoleMutex.unlockInline();
	}
	if (t==QtFatalMsg)
		abort();
}
void REA::WndMain::updateUi (WndMain::ViewMode vm) {
	// Switch view mode if necessary.
	if (vm!=_viewMode)
		_viewMode								= vm;
    bool    licensed                            = _core->licensed();
    bool	projectOpened						= _core->projectFilename().length();

    _ui->actProjectNew->setEnabled				(licensed && !projectOpened);
    _ui->actProjectOpen->setEnabled				(licensed && !projectOpened);
    _ui->mnuProjectRecent->setEnabled				(licensed && !projectOpened);
    _ui->actProjectGenerate->setEnabled			(licensed && projectOpened);
    _ui->actProjectClose->setEnabled				(licensed && projectOpened);

    _ui->wgtMain->setEnabled						(licensed && projectOpened);
    _ui->stkWgtMain->setCurrentIndex				((int)_viewMode-1);
	// Switch view based on current mode.
    _ui->actDocumentTypesView->setChecked			(_viewMode==WndMain::ViewModeDocumentTypes);
    _ui->actAnnView->setChecked					(_viewMode==WndMain::ViewModeAnn);
    _ui->actGlobalPropertiesView->setChecked		(_viewMode==WndMain::ViewModeGlobalProperties);
	
    _ui->wgtDocumentTypes->updateUi				();
    _ui->wgtAnns->updateUi						();
    _ui->wgtGlobalProperties->updateUi            ();
}
void REA::WndMain::updateRecentMenu () {
	// Restore recent files menu.
    QSettings		settings			("Pierre qui Roule EURL", "RELibraryAdmin");
	QStringList filenames				= settings.value("WndMain::mnuProjectRecent").toStringList();
    _ui->mnuProjectRecent->clear();
	QStringList		cleanedFilenames;
	foreach (QString const &filename, filenames) {
		if (!QFileInfo(filename).exists())
			continue; 
		cleanedFilenames << filename;
        QAction	*act					= new QAction(QFileInfo(filename).baseName(), _ui->mnuProjectRecent);
		act->setData(filename);
		connect(act, SIGNAL(triggered()), this, SLOT(on_actProjectOpen_triggered()));
        _ui->mnuProjectRecent->addAction(act);
	}
	settings.setValue("WndMain::mnuProjectRecent", cleanedFilenames);
}

void REA::WndMain::closeEvent (QCloseEvent *e) {
	if (!_core->allProcessesIdle()) {
		e->ignore();
		return;
	}
    _core->closeProject		();
	QMainWindow::closeEvent(e);
}

void REA::WndMain::core_projectOpened () {
    QSettings settings			("Pierre qui Roule EURL", "RELibraryAdmin");
	QStringList filenames		= settings.value("WndMain::mnuProjectRecent").toStringList();
	filenames.removeAll			(_core->projectFilename());
	filenames.push_front		(_core->projectFilename());
	while (filenames.count()>5)
		filenames.removeLast	();
	settings.setValue			("WndMain::mnuProjectRecent", filenames);
	updateRecentMenu			();
	updateUi					(_viewMode);
}
void REA::WndMain::core_projectClosed () {
	updateUi					(WndMain::ViewModeDocumentTypes);
}

void REA::WndMain::core_destroyed () {
    _ui->tblVwOutput->setModel	(0);
}

void REA::WndMain::on_actDocumentTypesView_triggered () {
	updateUi(WndMain::ViewModeDocumentTypes);
}
void REA::WndMain::on_actAnnView_triggered () {
	updateUi(WndMain::ViewModeAnn);
}
void REA::WndMain::on_actGlobalPropertiesView_triggered () {
	updateUi(WndMain::ViewModeGlobalProperties);
}
void REA::WndMain::on_actAbout_triggered () {
	DlgAbout(&_core->recognitionEngine(), this).exec();
}

void REA::WndMain::on_actProjectNew_triggered () {
    QSettings			s					("Pierre qui Roule EURL", "RELibraryAdmin");
	QString				path				= s.value("WndMain::NewProjectLastPath", QDir::homePath()).toString();
	path									= QFileDialog::getSaveFileName(this, tr("New Project..."), path, "RELibraryAdmin Projects (*.reproj)");
	if (!path.length())						return;
	s.setValue								("WndMain::NewProjectLastPath", QFileInfo(path).dir().path());
	_core->createProject					(path);
}
void REA::WndMain::on_actProjectOpen_triggered () {
	QAction				*sndr				= qobject_cast<QAction*>(sender());
	if (sndr->data().isValid()) {
		_core->openProject(sndr->data().toString());
		return;
	}
    QSettings			s					("Pierre qui Roule EURL", "RELibraryAdmin");
	QString				path				= s.value("WndMain::OpenProjectLastPath", QDir::homePath()).toString();
    path									= QFileDialog::getOpenFileName(this, tr("Open Project..."), path, "RELibraryAdmin Projects (*.reproj)");
	if (!path.length() || !QFile::exists(path))
		return;
	s.setValue								("WndMain::OpenProjectLastPath", QFileInfo(path).dir().path());
	_core->openProject						(path);
}

void REA::WndMain::on_actProjectGenerate_triggered () {
	Dbg										<< "Generation.";
    ClientBinaryGenerator	wiz				(this);
	wiz.setModal							(true);
	if (wiz.exec()!=QDialog::Accepted)		return;
	_core->generateClientBinary				(wiz.parameters());
}
void REA::WndMain::on_actProjectClose_triggered () {
	_core->closeProject						();
}

void REA::WndMain::on_btnClearConsole_clicked () {
	_consoleModel->clear					();
	// Update headers.
    _consoleModel->setHorizontalHeaderLabels	(QStringList() << tr("Sender") << tr("Message"));
}
