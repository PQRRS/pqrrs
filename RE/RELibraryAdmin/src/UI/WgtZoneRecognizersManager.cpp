#include "WgtZoneRecognizersManager.h"
#include "ui_WgtZoneRecognizersManager.h"
// Required stuff.
#include "../Core.h"
#include "../Models/DocumentTypesModel.h"
#include "../Models/SampleImagesModel.h"
#include "../Models/ZoneRecognizersModel.h"
#include "../Models/ZoneFiltersModel.h"
#include "../UI/WgtResultDisplay.h"
#include "ZoneRecognizersScene/ZoneRecognizersScene.h"
#include <RELibrary/RETypes>
#include <RELibrary/Filters/AbstractFilter>
#include <RELibrary/Analyzers/Recognizers/AbstractRecognizer>
#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtCore/QSettings>
#include <QtSql/QSqlRecord>
#include <QtGui/QMenu>
#include <QtGui/QMessageBox>
#include <QtGui/QFileDialog>
#include <QtGui/QPlainTextEdit>

REA::WgtZoneRecognizersManager::WgtZoneRecognizersManager (QWidget *p)
: QMainWindow(p), _ui(new Ui::WgtZoneRecognizersManager),
_inUpdate(false), _recognizerCreationMenu(0), _core(&REA::Core::Instance()),
_dtModel(0), _siModel(0), _scene(0),_sampleImageRow(0) {
    _ui->setupUi                                (this);
    setWindowFlags                              (Qt::Widget);

    // Put refresh button in tab corner.
    QToolButton         *btnRefreshPrepro       = new QToolButton(this);
    btnRefreshPrepro->setIcon                   (QIcon(":/Resources/Icons/refresh.gif"));
    _ui->tabWgtPreprocessing->setCornerWidget   (btnRefreshPrepro);
    connect(btnRefreshPrepro, SIGNAL(pressed()), this, SLOT(btnRefreshPrepro_clicked()));

    // Put refresh button in tab corner.
    QToolButton         *btnRefreshRecognizer   = new QToolButton(this);
    btnRefreshRecognizer->setGeometry           (_ui->btnCreateZoneRecognizer->geometry());
    btnRefreshRecognizer->setIcon               (QIcon(":/Resources/Icons/refresh.gif"));
    _ui->tabWgtRecognitionZone->setCornerWidget (btnRefreshRecognizer);
    connect(btnRefreshRecognizer, SIGNAL(pressed()), this, SLOT(btnRefreshRecognizer_clicked()));

    // Restore state / geometry.
    QSettings s                                 ("Pierre qui Roule EURL", "RELibraryAdmin");
    restoreGeometry                             (s.value("WndMain::DocumentTypes::Recognition::Geometry").toByteArray());
    restoreState                                (s.value("WndMain::DocumentTypes::Recognition::State").toByteArray());

    // Prepare models.
    _zoneRecognizersModel						= new ZoneRecognizersModel(this);
    _recognizersSelectionModel					= new QItemSelectionModel(_zoneRecognizersModel);
    _scene										= new ZoneRecognizersScene(_zoneRecognizersModel, _recognizersSelectionModel, this);
    _ui->grphVwMain->setScene					((QGraphicsScene*)_scene);

    // Set up the zone recognizer picker combo box.
    _ui->cmbRecognizerPicker->setModel          (_zoneRecognizersModel);

    // Create zone creation menu and connect it to controller.
    _recognizerCreationMenu						= new QMenu(this);
    _ui->btnCreateZoneRecognizer->setMenu		(_recognizerCreationMenu);
    connect										(_recognizerCreationMenu, SIGNAL(triggered(QAction*)), this, SLOT(zoneCreationMenu_triggered(QAction*)));

    // Create zone import menu.
    _recognizerImportMenu						= new QMenu(this);
    _ui->btnImportZoneRecognizer->setMenu		(_recognizerImportMenu);
    connect										(_recognizerImportMenu, SIGNAL(aboutToShow()), this, SLOT(zoneImportMenu_aboutToShow()));
    connect										(_recognizerImportMenu, SIGNAL(triggered(QAction*)), this, SLOT(zoneImportMenu_triggered(QAction*)));

    // Prepare vars for creation menus.
    QStringList					names;
    RE::Engine					&re			= _core->recognitionEngine();
    // Get and sort all recognizers names.
    names									= re.recognizersFactory().registeredTypes().keys();
    qSort									(names);
    // Populate the zone creation menu.
    foreach (QString const &n, names) {
        QAction					*act		= new QAction(RE::ClassNameToNaturalString(n), _recognizerCreationMenu);
        act->setData						(n);
        _recognizerCreationMenu->addAction	(act);
    }

    // Connect stuff...
    connect(_core, SIGNAL(projectOpened()), this, SLOT(core_projectOpened()));
    connect(_core, SIGNAL(projectClosed()), this, SLOT(core_projectClosed()));
    connect(_core, SIGNAL(destroyed()), this, SLOT(core_destroyed()));
    connect(_recognizersSelectionModel, SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(zoneRecognizers_selectionChanged(QItemSelection,QItemSelection)));
}
REA::WgtZoneRecognizersManager::~WgtZoneRecognizersManager () {
    // Save window state / geometry.
    QSettings s("Pierre qui Roule EURL", "RELibraryAdmin");
    s.setValue("WndMain::DocumentTypes::Recognition::Geometry", saveGeometry());
    s.setValue("WndMain::DocumentTypes::Recognition::State", saveState());
    // Destroy UI.
    delete _ui;
}
void REA::WgtZoneRecognizersManager::closeEvent (QCloseEvent *e) {
    QMainWindow::closeEvent(e);
}

void REA::WgtZoneRecognizersManager::setSelection (QModelIndex const &idx) {
    if (!idx.isValid()) {
        _oldDtIndex = _dtIndex		= idx;
        hideEvent					(0);
        return;
    }
    _dtIndex						= _dtModel->index(idx.row(), DocumentTypesModel::RecognitionData);
    // Set preprocessing filter data to model.
    QByteArray const &ppfData       = _dtModel->index(idx.row(), DocumentTypesModel::PreprocessingFiltersData).data().toByteArray();
    RE::T::ZoneFilterData::List
            const   &ppfs           = RE::valueFromStreamedByteArray<RE::T::ZoneFilterData::List>(ppfData);
    // Update prepro filters list view, taking care of the memory leaks.
    _ui->wgtPreproFilterStack->setValue         (ppfs);
    if (_dtIndex==_oldDtIndex)                  return;
    else if (isVisible())                       showEvent(0);
}

void REA::WgtZoneRecognizersManager::fitViewToCurrentImage () const {
    QRect	imageRect				= _scene->backgroundImage().rect();
    if (imageRect.isNull())			return;
    _ui->grphVwMain->fitInView(imageRect, Qt::KeepAspectRatio);
    _ui->grphVwMain->centerOn		(imageRect.width()/2, imageRect.height()/2);
}

// Core.
void REA::WgtZoneRecognizersManager::core_projectOpened () {
    _dtModel							= &_core->documentTypesModel();
    _siModel							= &_core->sampleImagesModel();
}
void REA::WgtZoneRecognizersManager::core_projectClosed () {
    _zoneRecognizersModel->setZoneRecognizers(RE::T::ZoneRecognizerData::List());
    setSelection						(QModelIndex());
    _scene->setBackgroundImage			(QImage());
    _ui->tabWgtRecognitionZone->setValue(RE::T::ZoneRecognizerData());
    _ui->wgtPreproFilterStack->setValue (RE::T::ZoneFilterData::List());
    _dtModel							= 0;
    _siModel							= 0;
}
void REA::WgtZoneRecognizersManager::core_destroyed () {
}

// Sample image browsing.
void REA::WgtZoneRecognizersManager::on_chkBxDebuggingImage_toggled () {
    toSampleImageAtRow      (_sampleImageRow);
}
void REA::WgtZoneRecognizersManager::on_btnPrevImageFast_clicked () {
    toSampleImageAtRow		(_sampleImageRow-10);
}
void REA::WgtZoneRecognizersManager::on_btnPrevImage_clicked () {
    toSampleImageAtRow		(_sampleImageRow-1);
}
void REA::WgtZoneRecognizersManager::on_btnNextImage_clicked () {
    toSampleImageAtRow		(_sampleImageRow+1);
}
void REA::WgtZoneRecognizersManager::on_btnNextImageFast_clicked () {
    toSampleImageAtRow		(_sampleImageRow+10);
}

void REA::WgtZoneRecognizersManager::on_btnZoomFitView_toggled (bool f) {
    if (!f)					return;
    fitViewToCurrentImage	();
}
void REA::WgtZoneRecognizersManager::on_btnZoomIn_clicked () {
    _ui->grphVwMain->scale	(2, 2);
}
void REA::WgtZoneRecognizersManager::on_btnZoomOut_clicked () {
    _ui->grphVwMain->scale	(.5, .5);
}

void REA::WgtZoneRecognizersManager::btnRefreshPrepro_clicked () {
    toSampleImageAtRow                          (_sampleImageRow);
}

// Zones management.
void REA::WgtZoneRecognizersManager::on_cmbRecognizerPicker_currentIndexChanged (int i) {
    if (!_inUpdate) {
        _inUpdate                                   = true;
        QModelIndex const               &idx        = _zoneRecognizersModel->index(i, 0);
        _recognizersSelectionModel->setCurrentIndex (idx, QItemSelectionModel::ClearAndSelect);
        _inUpdate                                   = false;
    }
}
void REA::WgtZoneRecognizersManager::on_btnRevertZoneRecognizers_clicked () {
    // Set recognizers.
    RE::T::ZoneRecognizerData::List	zrds		= RE::valueFromStreamedByteArray<RE::T::ZoneRecognizerData::List>(_dtIndex.data().toByteArray());
    _zoneRecognizersModel->setZoneRecognizers	(zrds);
    setSelection                                (_dtIndex);
    // Update GUI.
    updateUi									();
}
void REA::WgtZoneRecognizersManager::on_btnSaveZoneRecognizers_clicked () {
    RE::T::ZoneRecognizerData::List	zrds		= _zoneRecognizersModel->zoneRecognizers();
    _dtModel->setData							(_dtIndex, RE::byteArrayFromStreamedValue(zrds));
    _dtModel->setData                           (_dtModel->index(_dtIndex.row(), DocumentTypesModel::PreprocessingFiltersData), RE::byteArrayFromStreamedValue(_ui->wgtPreproFilterStack->value()));
}
void REA::WgtZoneRecognizersManager::zoneRecognizers_selectionChanged (QItemSelection const&, QItemSelection const&) {
    QModelIndexList const			&sel		= _recognizersSelectionModel->selectedRows();
    _zrdIndex									= sel.count()==1 ? sel.first() : QModelIndex();
    if (!_inUpdate) {
        _inUpdate                               = true;
        _ui->cmbRecognizerPicker->setCurrentIndex(_zrdIndex.row());
        _inUpdate                               = false;
    }
    // Clear the recognizer's parameters editor.
    _ui->tabWgtRecognitionZone->setValue(RE::T::ZoneRecognizerData());
    // Selection contains an item...
    if (_zrdIndex.isValid()) {
        // Retrieve zone recognition data.
        RE::T::ZoneRecognizerData       zrd     = _zoneRecognizersModel->data(_zrdIndex, Qt::UserRole).value<RE::T::ZoneRecognizerData>();
        QVariantMap const               &params = _core->recognitionEngine().recognizerParameters(zrd.objectClassName);
        foreach (QString const &key, params.keys()) {
            if (!zrd.parameters.contains(key)) {
                zrd.parameters[key]             = params[key];
                Wrn                             << "New parameter" << key << "has been added to editor.";
            }
        }
        foreach (QString const &key, zrd.parameters.keys()) {
            if (!params.contains(key)) {
                zrd.parameters.remove           (key);
                _zoneRecognizersModel->removeParameter(_zrdIndex, key);
                Wrn                             << "Old parameter" << key << "has been removed from editor.";
            }
        }
        // Display the recognizer's parameters and filters.
        _ui->tabWgtRecognitionZone->setValue (zrd);
    }
    else {
        while (_ui->tabWgtResults->count())
            _ui->tabWgtResults->removeTab       (0);
    }
    updateUi									();
    btnRefreshRecognizer_clicked                ();
}
void REA::WgtZoneRecognizersManager::zoneCreationMenu_triggered (QAction *act) {
    RE::T::ZoneRecognizerData		zrd	= _core->recognitionEngine().makeZoneRecognizerData(act->data().toString(), QRectF(30,30, 40,40));
    if (!_zoneRecognizersModel->createRecognizer(zrd))
        Wrn								<< "Failed to create a new Recognizer!";
}
void REA::WgtZoneRecognizersManager::zoneImportMenu_aboutToShow () {
    _recognizerImportMenu->clear		();
    // Retrieve all document types.
    for (qint32 row=0; row<_dtModel->rowCount(); ++row) {
        QString					dtName	= _dtModel->data(_dtModel->index(row, DocumentTypesModel::Name)).toString();
        QMenu					*mnuDt	= new QMenu(dtName, _recognizerImportMenu);
        RE::T::ZoneRecognizerData::List
                                zrds	= RE::valueFromStreamedByteArray<RE::T::ZoneRecognizerData::List>(_dtModel->data(_dtModel->index(row, DocumentTypesModel::RecognitionData)).toByteArray());
        // Iterate thru each zone.
        quint32					zone	= 0;
        foreach (RE::T::ZoneRecognizerData const &zrd, zrds) {
            QString const	&zrdName	= zrd.parameters["name"].toString();
            if (zrdName.isEmpty())
                continue;
            QAction				*act	= new QAction(zrdName, mnuDt);
            act->setData				(QString("%1:->%2").arg(row).arg(zone));
            mnuDt->addAction			(act);
            zone						++;
        }
        _recognizerImportMenu->addMenu	(mnuDt);
    }
}
void REA::WgtZoneRecognizersManager::zoneImportMenu_triggered (QAction *act) {
    QStringList const			dt		= act->data().toString().split(":->");
    // Retrieve right document type data.
    RE::T::ZoneRecognizerData::List
                                zrds	= RE::valueFromStreamedByteArray<RE::T::ZoneRecognizerData::List>(_dtModel->data(_dtModel->index(dt[0].toUInt(), DocumentTypesModel::RecognitionData)).toByteArray());
    RE::T::ZoneRecognizerData const
                                &zrd	= zrds[dt[1].toUInt()];
    if (!_zoneRecognizersModel->createRecognizer(zrd))
        Wrn								<< "Failed to create a new Recognizer!";
}
void REA::WgtZoneRecognizersManager::on_btnDeleteZoneRecognizer_clicked () {
    QModelIndexList const		&sel	= _recognizersSelectionModel->selectedRows();
    if (sel.count()!=1)					return;
    QModelIndex const			&idx	= sel.first();
    _zoneRecognizersModel->removeRows	(idx.row(), 1, idx.parent());
}
void REA::WgtZoneRecognizersManager::on_btnSaveZoneRecognizerPictures_clicked () {
    // Ask for folder / base filename.
    QString const               &oDir   = QFileDialog::getExistingDirectory(this, tr("Directory for Batch Saving..."), QDir::homePath());
    // Retrieve recognizer data...
    RE::T::ZoneRecognizerData const
                                &zrd	= _zoneRecognizersModel->data(_zrdIndex, Qt::UserRole).value<RE::T::ZoneRecognizerData>();
    QString const               &zrdN   = zrd.parameters["name"].toString();
    // Loop for each sample image.
    RE::Engine                  &re     = _core->recognitionEngine();
    for (qint32 i=0; i<_siModel->rowCount(); ++i) {
        QString                 docFn   = _siModel->index(i, REA::SampleImagesModel::Filename).data().toString();
        docFn                           = _core->projectDirectory().absoluteFilePath(docFn);
        if (!QFile::exists(docFn))      continue;
        // Filter image prior to run recognizer on it.
        QImage                  img     = QImage(docFn);
        img                             = re.filteredImage(_ui->wgtPreproFilterStack->value(), "", img, QThread::idealThreadCount());
        img                             = re.filteredRecognizerImage(zrd, "", img, QThread::idealThreadCount());
        // Build unique output filename and save.
        QString const           &oIFn   = QString("%1/%2_%3.png")
                                            .arg(oDir)
                                            .arg(zrdN)
                                            .arg(i, 4, 10, QChar('0'));
        img.save                        (oIFn);
    }
}

void REA::WgtZoneRecognizersManager::btnRefreshRecognizer_clicked () {
    // Don't do anything unless the selected recognizer is valid.
    if (!_zrdIndex.isValid())                       return;
    // Retrieve recognizer data...
    RE::T::ZoneRecognizerData const		zrd         = _zrdIndex.data(Qt::UserRole).value<RE::T::ZoneRecognizerData>();;
    // Filter image prior to run recognizer on it.
    QImage                              fImg        = _core->recognitionEngine().filteredRecognizerImage(zrd, "", _currentSampleImages[0], QThread::idealThreadCount());
    _scene->setSelectedRecognizerImage              (fImg);
    RE::T::StringToVariantHash          rs          = _core->recognitionEngine().recognizerResults(zrd, "", fImg, QThread::idealThreadCount());

    QStringList const                   &keys       = rs.keys();
    QStringList                         missing     = keys;
    // This first loop removes any unused tabs. It also builds a list of missing tabs.
    for (qint32 tabIdx=0; tabIdx<_ui->tabWgtResults->count(); tabIdx++) {
        QString const                   &title      = _ui->tabWgtResults->tabText(tabIdx);
        if (!keys.contains(title)) {
            _ui->tabWgtResults->removeTab           (tabIdx);
            tabIdx                                  --;
            continue;
        }
        else
            missing.removeAll                       (title);
    }
    // This second loop adds missing tabs.
    foreach (QString const &key, missing) {
        WgtResultDisplay    *wgt                    = new WgtResultDisplay(_ui->tabWgtResults);
        _ui->tabWgtResults->addTab                  (wgt, key);
    }
    // This last loop fills up data in each tab.
    for (qint32 tabIdx=0; tabIdx<_ui->tabWgtResults->count(); tabIdx++) {
        QString const                   &title      = _ui->tabWgtResults->tabText(tabIdx);
        WgtResultDisplay                *wgt        = (WgtResultDisplay*)_ui->tabWgtResults->widget(tabIdx);
        wgt->setResult                              (rs[title]);
    }
}
void REA::WgtZoneRecognizersManager::on_tabWgtRecognitionZone_filtersChanged () {
    _zoneRecognizersModel->setData              (_zrdIndex, QVariant::fromValue(_ui->tabWgtRecognitionZone->value()), Qt::UserRole);
}
void REA::WgtZoneRecognizersManager::on_tabWgtRecognitionZone_parameterEdited (QString const &name, QVariant const &value) {
    QModelIndexList const			&sel	= _recognizersSelectionModel->selectedRows();
    if (sel.count()!=1)						return;
    QModelIndex const				&pIdx	= sel.first();
    _zoneRecognizersModel->setParameter	(pIdx, name, value);
}

void REA::WgtZoneRecognizersManager::updateUi () {
    // Store some flags.
    bool		recognizerSelected		= _zrdIndex.isValid();
    // Update sample images UI.
    updateSampleImageUi					();
    // Zones management.
    _ui->tabWgtRecognitionZone->setEnabled          (recognizerSelected);
    _ui->btnSaveZoneRecognizerPictures->setEnabled  (recognizerSelected);
    _ui->btnRecognizersExport->setEnabled           (recognizerSelected);
}
void REA::WgtZoneRecognizersManager::updateSampleImageUi () {
    // Store some flags.
    bool		indexIsValid			= _dtIndex.isValid();
    bool		atFirstImage			= indexIsValid && _sampleImageRow==0;
    bool		atLastImage				= indexIsValid && _sampleImageRow>=_siModel->rowCount()-1;
    // Sample images browsing.
    _ui->btnPrevImageFast->setEnabled		(!atFirstImage);
    _ui->btnPrevImage->setEnabled			(!atFirstImage);
    _ui->btnNextImage->setEnabled			(!atLastImage);
    _ui->btnNextImageFast->setEnabled		(!atLastImage);
    if (indexIsValid)
        _ui->lblZoneRecognizersStatus->setText(tr("Viewing Image %1 of %2 (%3).").arg(_sampleImageRow+1).arg(_siModel->rowCount()).arg(QFileInfo(_currentSampleImageFilename).baseName()));
    else
        _ui->lblZoneRecognizersStatus->setText(tr("Invalid Document Type selection, make sure only one is selected."));
}
void REA::WgtZoneRecognizersManager::showEvent (QShowEvent *e) {
    if (_oldDtIndex!=_dtIndex)			_oldDtIndex = _dtIndex;
    on_btnRevertZoneRecognizers_clicked	();
    toSampleImageAtRow					(0);
    if (e)								QWidget::showEvent(e);
}
void REA::WgtZoneRecognizersManager::hideEvent (QHideEvent *e) {
    _scene->setBackgroundImage			();
    if (e)								QWidget::hideEvent(e);
}

void REA::WgtZoneRecognizersManager::toSampleImageAtRow (qint32 row) {
    if (!_siModel)						return;
    _sampleImageRow						= qBound(0, row, _siModel->rowCount()-1);
    QModelIndex const	idx				= _siModel->index(_sampleImageRow, SampleImagesModel::Filename);
    _currentSampleImageFilename			= _core->projectDirectory().absoluteFilePath(_siModel->data(idx).toString());
    QImage              img             = QImage(_currentSampleImageFilename);
    RE::T::ZoneFilterData::List const
                        &ppfs           = _ui->wgtPreproFilterStack->value();
    QStringList         imageHashes;
    _currentSampleImages                = _core->recognitionEngine().filteredImages(ppfs, "", img, QThread::idealThreadCount(), true);
    _scene->setBackgroundImage			(_currentSampleImages[_ui->chkBxDebuggingImage->isChecked() ? 1 : 0]);
    if (_ui->btnZoomFitView->isChecked())
        fitViewToCurrentImage			();
    updateSampleImageUi					();
    btnRefreshRecognizer_clicked		();
}
