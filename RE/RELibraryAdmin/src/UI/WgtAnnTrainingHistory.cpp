#include "WgtAnnTrainingHistory.h"
// Required stuff.
#include <QtCore/QRect>
#include <QtCore/QVector>
#include <QtGui/QPaintEvent>
#include <QtGui/QPainter>
#include <math.h>

#define	EPOCH_COLOR			0x7777ff
#define	MSE_COLOR			0xff7777

REA::WgtAnnTrainingHistory::WgtAnnTrainingHistory (QWidget *p) : QWidget(p), _targetMse(0), _lastEpoch(0) {
}

void REA::WgtAnnTrainingHistory::addValues (quint32 epoch, double mse) {
	_lastEpoch						= epoch;
	_mseHistory.append(mse);
	update();
}
void REA::WgtAnnTrainingHistory::clear () {
	_lastEpoch						= 0;
	_mseHistory.clear();
	_mseHistory << 1.0;
	update();
}

void REA::WgtAnnTrainingHistory::paintEvent (QPaintEvent *pe) {
	// Prepare work variables.
	QRect				r			(this->rect());
	// Retrieve widget painter.
	QPainter			p			(this);
	p.setClipRegion(pe->region());
	// Fill dark.
	p.fillRect						(r, 0x222222);
	
	// Prepare for drawing curve.
	p.setPen						(MSE_COLOR);
	double			lastMse			= 1.0;
	// Prepare a path for each curve.
	double const	logScaleMaxR	= 1.0 / log(250.0);
	double const	logOrigin		= 1.0-_targetMse;
	#define	LOGFUNC	(1.0 - (logScaleMaxR * log(249.0 * (lastMse - _targetMse) + logOrigin)))
	// Draw actual curve.
	quint32			count			= _mseHistory.count();
	if (count>1) {
		// Cache some graphical dimensions.
		quint32			hDrawable	= r.height() - 30;
		double			wStep		= r.width() / (double)(count-1);
		QPainterPath	msePath		(QPointF(0, 20.0+hDrawable*LOGFUNC));
		// Work vars.
		quint32			pix;
		quint32			lastPix		= 0;
		// Move each painter to its original point.
		for (quint32 i=1; i<count; i++) {
			lastMse					= _mseHistory[i];
			pix						= wStep*i;
			if (pix==lastPix)		continue;
			lastPix					= pix;
			msePath.lineTo(pix, 20.0+hDrawable*LOGFUNC);
		}
		p.drawPath(msePath);
	}
	
	// Draw texts.
	QFont				f			(p.font());
	f.setStyleHint					(QFont::Monospace);
	f.setPointSize					(8);
	p.setFont						(f);
	p.setPen						(EPOCH_COLOR);
	p.drawText						(5, 15, QString("Epoch = %1").arg(_lastEpoch));
	p.setPen						(MSE_COLOR);
	p.drawText						(85, 15, QString("MSE = %1").arg(lastMse, 0, 'f', 14));
}
