#ifndef ZoneRecognizerItem_h
#define ZoneRecognizerItem_h

// Base class.
#include <QtGui/QGraphicsRectItem>
// Required stuff.
#include <RELibrary/RETypes>
#include <QtCore/QModelIndex>
#include <QtCore/QPointF>
#include <QtGui/QGraphicsItem>

namespace REA {

class ZoneRecognizerItem : public QObject, public QGraphicsRectItem {
public:
    Q_OBJECT
    Q_PROPERTY(QString text				READ text			WRITE setText)
    Q_PROPERTY(QString details          READ details        WRITE setDetails)
    Q_PROPERTY(QImage image				READ image			WRITE setImage)

public:
	enum { Type = UserType + 1 };
	int						type					() const { return Type; }
	
	// Types.
	typedef QList<ZoneRecognizerItem*> List;
	// Constructor.
    /**/					ZoneRecognizerItem		(QGraphicsItem *p=0);
    QString const&			text					() const;
    void					setText					(QString const &v);
    QString const&			details					() const;
    void					setDetails				(QString const &v);
    QImage const&			image					() const;
	void					setImage				(QImage const &v);
    QVariant				itemChange				(GraphicsItemChange c, const QVariant &v);

protected:
	void					mousePressEvent			(QGraphicsSceneMouseEvent *e);
	void					mouseMoveEvent			(QGraphicsSceneMouseEvent *e);
	void					mouseReleaseEvent		(QGraphicsSceneMouseEvent *e);
	void					resizeFromEvent			(QGraphicsSceneMouseEvent *e);
	void					paint					(QPainter *p, QStyleOptionGraphicsItem const *o, QWidget *w=0);

	
private:
	bool					_inResize,
							_hasMoved;
	QPointF					_resizeDelta;
    QString					_text, _details;
	QImage					_image;

signals:
	void					changed					(REA::ZoneRecognizerItem *zri);
};

}

#endif
