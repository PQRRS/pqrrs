#include "ZoneRecognizerItem.h"
// Required stuff.
#include <QtGui/QBrush>
#include <QtGui/QGraphicsSceneMouseEvent>
#include <QtGui/QPainter>
#include <QtCore/QDebug>

#define MIN_RECT_SIZE 20
#define RESIZE_HANDLE_SIZE 10

REA::ZoneRecognizerItem::ZoneRecognizerItem (QGraphicsItem *p)
: QGraphicsRectItem(p),
_inResize(false), _hasMoved(false),
_text(tr("Unknown Recognizer")) {
	setFlags				(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);
	setBrush				(QColor(128, 255, 128, 128));
	setPen					(Qt::NoPen);
}

QString const& REA::ZoneRecognizerItem::text () const {
    return _text;
}
void REA::ZoneRecognizerItem::setText (QString const &v) {
    _text = v;
    update	();
}
QString const& REA::ZoneRecognizerItem::details () const {
    return _details;
}
void REA::ZoneRecognizerItem::setDetails (QString const &v) {
    _details = v;
    update	();
}
QImage const& REA::ZoneRecognizerItem::image () const {
	return _image;
}
void REA::ZoneRecognizerItem::setImage (QImage const &v) {
	_image	= v;
	update	();
}

QVariant REA::ZoneRecognizerItem::itemChange (GraphicsItemChange c, const QVariant &v) {
	switch (c) {
		case ItemSelectedHasChanged:
			setBrush		(v.toBool() ? QColor(255, 128, 128, 128) : QColor(128, 255, 128, 128));
		default:
			break;
	}
	return QGraphicsRectItem::itemChange(c, v);
}
void REA::ZoneRecognizerItem::mousePressEvent (QGraphicsSceneMouseEvent *e) {
	QGraphicsRectItem::mousePressEvent(e);
	QPointF		sp			= e->pos();
	qint32		x			= sp.x();
	qint32		y			= sp.y();
	_hasMoved				= false;
	_inResize				= x>=rect().width()-RESIZE_HANDLE_SIZE && y>=rect().height()-RESIZE_HANDLE_SIZE;
	_resizeDelta.setX		(rect().right()-x);
	_resizeDelta.setY		(rect().bottom()-y);
	setFlag					(QGraphicsItem::ItemIsMovable, !_inResize);
}
void REA::ZoneRecognizerItem::mouseMoveEvent (QGraphicsSceneMouseEvent *e) {
	QGraphicsRectItem::mouseMoveEvent(e);
	if (!_image.size().isNull())
		_image				= QImage();
	if (_inResize)
		resizeFromEvent		(e);
	else
		_hasMoved			= true;
}
void REA::ZoneRecognizerItem::mouseReleaseEvent (QGraphicsSceneMouseEvent *e) {
	QGraphicsRectItem::mouseReleaseEvent(e);
	if (_inResize)
		resizeFromEvent		(e);
	if (_inResize || _hasMoved) {
		_inResize			= false;
		_hasMoved			= false;
		emit changed(this);
	}
}

void REA::ZoneRecognizerItem::resizeFromEvent (QGraphicsSceneMouseEvent *e) {
	QRectF		nr					(QPointF(0, 0), QPointF(e->pos().x()+_resizeDelta.x(), e->pos().y()+_resizeDelta.y()));
	if (nr.width()<MIN_RECT_SIZE)	nr.setWidth(MIN_RECT_SIZE);
	if (nr.height()<MIN_RECT_SIZE)	nr.setHeight(MIN_RECT_SIZE);
	setRect							(nr);
}

void REA::ZoneRecognizerItem::paint (QPainter *p, QStyleOptionGraphicsItem const *o, QWidget *w) {
    QGraphicsRectItem::paint            (p, o, w);
    QRectF const            &r          = rect();
    // Change painter font.
    QFont               f           = p->font();
    f.setStyleHint                  (QFont::Fantasy);
    f.setPointSize                  (f.pointSize()-2);
    // Paint image.
    if (isSelected()) {
		if (!_inResize && !_hasMoved && !_image.size().isNull())
            p->drawImage                (r, _image);
	}
    // Draw main text.
    else {
        p->setFont                      (f);
        p->setPen                       (Qt::darkBlue);
        p->drawText                     (r, Qt::AlignCenter, _text);
    }
    // Draw details text.
    p->setFont                      (f);
    p->setPen                       (Qt::red);
    p->drawText                     (r, Qt::AlignRight | Qt::AlignBottom, _details);
}
