#include "ZoneRecognizersScene.h"
// Required stuff.
#include "ZoneRecognizerItem.h"
#include "../../Core.h"
#include "../../Models/ZoneRecognizersModel.h"
#include <RELibrary/Filters/AbstractFilter>
#include <QAbstractItemModel>
#include <QtCore/QtAlgorithms>
#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QModelIndex>
#include <QtGui/QItemSelectionModel>
#include <QtGui/QGraphicsPixmapItem>
#include <QtGui/QRadialGradient>

REA::ZoneRecognizersScene::ZoneRecognizersScene (ZoneRecognizersModel *m, QItemSelectionModel *sm, QObject *p)
: QGraphicsScene(p), _imageItem(0), _zrModel(m), _zrsModel(sm), _selectedItem(0) {
    setBackgroundBrush	(QImage(":/Resources/Images/CheckerLight.jpg"));

    connect(this, SIGNAL(selectionChanged()), this, SLOT(self_selectionChanged()));
    connect(_zrsModel, SIGNAL(currentChanged(QModelIndex, QModelIndex)), this, SLOT(zoneRecognizersModel_currentChanged(QModelIndex, QModelIndex)));
    connect(_zrModel, SIGNAL(modelAboutToBeReset()), this, SLOT(zoneRecognizersModel_aboutToBeReset()));
    connect(_zrModel, SIGNAL(modelReset()), this, SLOT(zoneRecognizersModel_reset()));
    connect(_zrModel, SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(zoneRecognizersModel_rowsInserted(QModelIndex,int,int)));
    connect(_zrModel, SIGNAL(rowsAboutToBeRemoved(QModelIndex,int,int )), this, SLOT(zoneRecognizersModel_rowsAboutToBeRemoved(QModelIndex,int,int)));
    connect(_zrModel, SIGNAL(rowsRemoved(QModelIndex,int,int )), this, SLOT(zoneRecognizersModel_rowsRemoved(QModelIndex,int,int)));
}

QImage const& REA::ZoneRecognizersScene::backgroundImage () const {
    return _currentImage;
}
void REA::ZoneRecognizersScene::setBackgroundImage (QImage const &i) {
    if (_currentImage==i)
        return;
    _currentImage				= i;
    if (i.isNull()) {
        if (_imageItem) {
            removeItem			(_imageItem);
            delete				_imageItem;
            _imageItem			= 0;
            update				();
        }
    }
    else {
        if (_imageItem) {
            removeItem			(_imageItem);
            delete				_imageItem;
        }
        _imageItem				= addPixmap(QPixmap::fromImage(_currentImage));
        _imageItem->setPos		(0, 0);
        _sizeRatios.setWidth	(_currentImage.width() / 100.0);
        _sizeRatios.setHeight	(_currentImage.height() / 100.0);
        _imageItem->setZValue	(-1);
        setSceneRect			(_imageItem->boundingRect());
    }
    if (_imageItem)
        foreach (ZoneRecognizerItem *zri, _graphicsItems)
            updateRecognizer		(zri);
}

QImage const& REA::ZoneRecognizersScene::selectedRecognizerImage () const {
    static const QImage invalidImage;
    return _selectedItem ? _selectedItem->image() : invalidImage;
}
void REA::ZoneRecognizersScene::setSelectedRecognizerImage (QImage const &img) {
    Dbg << "Updating filtered image for Recognizer at" << (void*)_selectedItem << "...";
    // No passed recognizer, update the selected one.
    if (!_selectedItem)						return;
    _selectedItem->setImage					(img);
    //RE::T::ZoneRecognizerData const	&zrd	= _graphicsItems.key(_selectedItem).data(Qt::UserRole).value<RE::T::ZoneRecognizerData>();
    //_selectedItem->setDetails               (tr("%n filters", "filter", zrd.filterStack.count()));
}

void REA::ZoneRecognizersScene::updateRecognizer (ZoneRecognizerItem *zri) {
    // No passed recognizer, update the selected one.
    if (!zri) {
        QList<QGraphicsItem*> const	&itms	= selectedItems();
        // No selection. Return.
        if (itms.isEmpty())					return;
        zri									= (ZoneRecognizerItem*)itms.first();
    }
    RE::T::ZoneRecognizerData const	&zrd	= _graphicsItems.key(zri).data(Qt::UserRole).value<RE::T::ZoneRecognizerData>();
    zri->setText							(RE::ClassNameToNaturalString(zrd.objectClassName));
    zri->setDetails                         (tr("%n filters", "filter", zrd.filterStack.count()));
    if (!_imageItem)						return;
    QRectF							rR		= zrd.relativeRect;
    QRect							rA		= QRect(rR.x()*_sizeRatios.width(), rR.y()*_sizeRatios.height(), rR.width()*_sizeRatios.width(), rR.height()*_sizeRatios.height());
    zri->setPos								(rA.left(), rA.top());
    zri->setRect							(0, 0, rA.width(), rA.height());

}
REA::ZoneRecognizerItem* REA::ZoneRecognizersScene::graphicsItemForIndex (QModelIndex const &idx) {
    ZoneRecognizerItem				*zri	= 0;
    if (_graphicsItems.contains(idx)) {
        zri							= _graphicsItems[idx];
        Dbg << "Retrieving cached Recognizer Item at" << (void*)zri << "for index" << idx << "...";
        return zri;
    }
    zri										= new ZoneRecognizerItem();
    connect(zri, SIGNAL(changed(REA::ZoneRecognizerItem*)), this, SLOT(zoneRecognizerItem_changed(REA::ZoneRecognizerItem*)));
    return							_graphicsItems[idx] = zri;
}

void REA::ZoneRecognizersScene::self_selectionChanged () {
    // Remove image from previously selected recognizer item.
    if (_selectedItem) {
        Dbg << "Cleaning up previously selected Recognizer Item at" << (void*)_selectedItem << ".";
        _selectedItem->setImage				(QImage());
        updateRecognizer                    (_selectedItem);
        _selectedItem						= 0;
    }
    // Loop for each selected item and make model selection...
    QList<QGraphicsItem*> const		&itms	= selectedItems();
    QItemSelection					sel;
    foreach (QGraphicsItem *itm, itms) {
        if (itm->type()!=ZoneRecognizerItem::Type)
            continue;
        ZoneRecognizerItem			*zri	= (ZoneRecognizerItem*)itm;
        _selectedItem						= zri;
        QModelIndex const			&idx	= _graphicsItems.key(zri);
        sel.select							(idx, idx);
        _selectedItem->setZValue            (1000);
        Dbg << "Zone Recognizers Model selection changed to Recognizer Item at" << (void*)zri << ".";
        break;
    }
    Dbg << "Setting Zone Recognizers Model new selection...";
    _zrsModel->select						(sel, QItemSelectionModel::ClearAndSelect);
}
void REA::ZoneRecognizersScene::zoneRecognizersModel_currentChanged (QModelIndex const &n, QModelIndex const&) {
    if (_selectedItem)
        _selectedItem->setSelected  (false);
    REA::ZoneRecognizerItem *itm    = graphicsItemForIndex(n);
    itm->setSelected                (true);
    self_selectionChanged           ();
}
void REA::ZoneRecognizersScene::zoneRecognizersModel_aboutToBeReset () {
    for (GraphicsItemsCache::iterator i=_graphicsItems.begin(); i!=_graphicsItems.end(); ++i) {
        ZoneRecognizerItem			*zri	= i.value();
        removeItem					(zri);
        delete						zri;
    }
    _graphicsItems.clear			();
}
void REA::ZoneRecognizersScene::zoneRecognizersModel_reset () {
    quint32							count	= _zrModel->rowCount();
    for (quint32 i=0; i<count; i++) {
        QModelIndex const	&idx	= _zrModel->index(i, 0, QModelIndex());
        if (!_zrModel->isRecognizer(idx))
            continue;
        ZoneRecognizerItem	*zri	= graphicsItemForIndex(idx);
        addItem						(zri);
        updateRecognizer			(zri);
    }
}
void REA::ZoneRecognizersScene::zoneRecognizersModel_rowsInserted (QModelIndex const &parent, int start, int end) {
    // Parent not root, dont do anything.
    if (!_zrModel->isRoot(parent))		return;
    // Add rows...
    for (int i=start; i<=end; i++) {
        QModelIndex const	&idx		= _zrModel->index(i, 0, parent);
        if (!_zrModel->isRecognizer(idx))
            continue;
        ZoneRecognizerItem	*zri		= graphicsItemForIndex(idx);
        addItem							(zri);
        updateRecognizer				(zri);
    }
}
void REA::ZoneRecognizersScene::zoneRecognizersModel_rowsAboutToBeRemoved (QModelIndex const &parent, int start, int end) {
    Dbg << "Zone Recognizers Model rows are about to be removed in parent" << parent << ".";
    // Parent not root, dont do anything.
    if (!_zrModel->isRoot(parent))		return;
    // Delete rows...
    for (int i=start; i<=end; i++) {
        QModelIndex const	&idx		= _zrModel->index(i, 0, parent);
        ZoneRecognizerItem	*zri		= _graphicsItems.take(idx);
        removeItem						(zri);
        delete							zri;
    }
}
void REA::ZoneRecognizersScene::zoneRecognizersModel_rowsRemoved (QModelIndex const &parent, int, int) {
    Dbg << "Zone Recognizers Model rows were removed in parent" << parent << ".";
    // Parent is root, update recognizer if it's the selected one.
    if (_zrModel->isRoot(parent)) {
        // Refresh whole scene.
        zoneRecognizersModel_aboutToBeReset	();
        zoneRecognizersModel_reset			();
    }
}
void REA::ZoneRecognizersScene::zoneRecognizerItem_changed (REA::ZoneRecognizerItem *zri) {
    QRectF					rR			(zri->pos(), zri->rect().size());
    qreal					x			= qBound(0.0, rR.x()/_sizeRatios.width(), 100.0);
    qreal					y			= qBound(0.0, rR.y()/_sizeRatios.height(), 100.0);
    qreal					w			= qBound(0.0, rR.width()/_sizeRatios.width(), 100.0-x);
    qreal					h			= qBound(0.0, rR.height()/_sizeRatios.height(), 100.0-y);
    QRectF					relRect		(x, y, w, h);
    _zrModel->setRecognizerRelativeRect	(_graphicsItems.key(zri), relRect);
}
