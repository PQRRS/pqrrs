#ifndef ZoneRecognizersScene_h
#define ZoneRecognizersScene_h

// Base class.
#include <QtGui/QGraphicsScene>
// Required stuff.
#include <RELibrary/RETypes>
#include <QtCore/QModelIndex>

// Forward declarations.
class QGraphicsPixmapItem;
class QAbstractItemModel;
class QItemSelectionModel;
namespace REA {
	class ZoneRecognizerItem;
	class ZoneRecognizersModel;
}

namespace REA {

class ZoneRecognizersScene : public QGraphicsScene {
Q_OBJECT
public:
	// Types.
	typedef QHash<QModelIndex, ZoneRecognizerItem*> GraphicsItemsCache;
	// Constructor.
									ZoneRecognizersScene						(ZoneRecognizersModel *m, QItemSelectionModel *sm, QObject *p=0);

public slots:
	QImage const&					backgroundImage								() const;
	void							setBackgroundImage							(QImage const &i=QImage());
	QImage const&					selectedRecognizerImage						() const;
	void							setSelectedRecognizerImage					(QImage const &img);
	QSizeF const&					sizeRatios									() const { return _sizeRatios; }
	void							updateRecognizer							(ZoneRecognizerItem *zri=0);

protected:
	ZoneRecognizerItem*				graphicsItemForIndex						(QModelIndex const &idx);

protected slots:
	void							self_selectionChanged						();
    void                            zoneRecognizersModel_currentChanged         (QModelIndex const &n, QModelIndex const &o);
	void							zoneRecognizersModel_aboutToBeReset			();
	void							zoneRecognizersModel_reset					();
	void							zoneRecognizersModel_rowsInserted			(QModelIndex const &parent, int start, int end);
	void							zoneRecognizersModel_rowsAboutToBeRemoved	(QModelIndex const &parent, int start, int end);
	void							zoneRecognizersModel_rowsRemoved			(QModelIndex const &parent, int start, int end);
	void							zoneRecognizerItem_changed					(REA::ZoneRecognizerItem *zri);

private:
	// Members.
	QImage							_currentImage;
	QGraphicsPixmapItem				*_imageItem;
	QSizeF							_sizeRatios;
	GraphicsItemsCache				_graphicsItems;
	ZoneRecognizersModel			*_zrModel;
	QItemSelectionModel				*_zrsModel;
	ZoneRecognizerItem				*_selectedItem;
};

}

#endif
