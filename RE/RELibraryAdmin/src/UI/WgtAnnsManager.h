#ifndef REA_WgtAnnsManager_h
#define REA_WgtAnnsManager_h

// Base class and private UI.
#include <QtGui/QWidget>
// Various stuff.
#include "../Core.h"
#include <RELibrary/RETypes>
#include <QtCore/QModelIndexList>
#include <QtGui/QItemSelection>

// Forward declarations.
namespace REA {
    class AnnTestingResultsModel;
    class WgtFakeByteArrayEditor;
}
namespace Ui { class WgtAnnsManager; }
class QAbstractItemModel;
class QDataWidgetMapper;

namespace REA {

class WgtAnnsManager : public QWidget {
Q_OBJECT

public:
	// Constructor.
    explicit                    WgtAnnsManager                          (QWidget *p=0);
    virtual                     ~WgtAnnsManager                         ();
	// GUI refreshing method.
	void						updateUi								();

protected:
	RE::T::UIntList				documentTypesKeys						() const;
	RE::T::UIntList				selectedDocumentTypesKeys				() const;
	RE::T::UIntList				selectedTrainableDocumentTypesKeys		() const;

protected slots:
	// Core projects management slots.
	void						core_projectOpened						();
	void						core_projectClosed						();
	// Ann management.
	void						on_btnCreate_clicked					();
	void						on_btnDelete_clicked					();
	void						on_btnTrainingRevert_clicked			();
	void						on_btnTrainingSave_clicked				();
	void						listModel_selectionChanged				(QItemSelection const&, QItemSelection const&);
	void						dtList_selectionChanged					(QItemSelection const&, QItemSelection const&);
	// Ann training / testing triggers.
	void						on_btnTrainingRun_clicked				();
	void						on_btnTestingRun_clicked				();
	// Core Ann training signals.
	void						core_trainingStarted					();
	void						core_trainingReport						(qint32 epoch, double error);
	void						core_trainingFinished					(bool success, QByteArray data);
	// Core ann testing signals.
	void						core_testingStarted						(RE::T::UIntToStringHash codesList);
	void						core_testingReport						(quint32 expectedType, RE::T::UIntToDoubleHash results);
	void						core_testingFinished					();

private:
    Ui::WgtAnnsManager			*_ui;
	Core						*_core;
	QAbstractItemModel			*_dtModel;
	QAbstractItemModel			*_taModel;
	AnnTestingResultsModel		*_atrModel;
	bool						_tablesInitDone;
	QDataWidgetMapper			*_dataMapper;
};

}

#endif
