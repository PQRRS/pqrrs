#ifndef REA_DBMigrator_h
#define REA_DBMigrator_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include "REATypes.h"
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlDatabase>

namespace REA {

class DBMigrator : public QObject {
Q_OBJECT
public:
	/**/				DBMigrator				(QSqlDatabase &db, QObject *p=0)
		: QObject(p), _database(db) {
	};
	/**/				~DBMigrator				() {
	};
	// Migration methods.
	bool				migrate					() {
		// Is database open?
		if (!_database.isOpen())
			return false;
		// Read database version.
		QString const		&curVersion	= REA::Version();
		QString				dbVersion	= "0.0.0.0";
		{ // Memguard.
			QSqlQuery		q;
			q.prepare					("SELECT value FROM Properties WHERE key=:key;");
			q.bindValue					(":key",	"Database.Version");
			q.exec						();
            QSqlError	e				= q.lastError();
            if (e.type()!=QSqlError::NoError) {
                Crt						<< "Query" << q.lastQuery() << "failed with error" << e.text() << "!";
                _database.close			();
                return                  false;
            }
            if (q.next())
				dbVersion				= q.value(0).toString();
		}
		Dbg								<< "Database is at version" << dbVersion << ".";
		// Is the db outdated?
		if (dbVersion==curVersion) {
			Dbg							<< "No migration required.";
			return true;
		}
		
		// Loop for each available migration. As soon as we get one for a higher version of the database, run it.
		QDir					migsDir	(":/Resources/SQL/Migrations");
		QStringList				list	= migsDir.entryList(QStringList() << "*.sql");
		foreach (QString const &migFile, list) {
			QString				migVersion
										= migFile.left(migFile.length()-4);
			if (migVersion>dbVersion) {
                Dbg						<< "Migrating to" << migVersion << "...";
				QFile			f		(QString(":/Resources/SQL/Migrations/%1").arg(migFile));
				if (!f.open(QIODevice::ReadOnly|QIODevice::Text)) {
					Crt					<< "Failed to open migration file" << migVersion << ".";
					return false;
				}
				QString		l;
				while (!f.atEnd()) {
					QString		t		= f.readLine().simplified();
					if (t.startsWith("--"))	continue;
					l				+= " "+t;
					if (l.isEmpty() || !l.endsWith(';'))
						continue;
					QSqlError	e		= QSqlQuery(l).lastError();
					if (e.type()!=QSqlError::NoError) {
						Crt				<< "Query" << l << "failed with error" << e.text() << "!";
						return false;
					}
					l.clear				();
				}
			}
		}
		return true;
	};

private:
	QSqlDatabase		_database;
};

}

#endif
