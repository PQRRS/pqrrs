#ifndef REATypes_h
#define REATypes_h

// Required stuff.
#include <RELibrary/RETypes>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaType>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QByteArray>
#include <QtCore/QSize>

#ifndef Dbg
#   define Dbg      qDebug()    << Q_FUNC_INFO << "#"
#   define Wrn      qWarning()  << Q_FUNC_INFO << "#"
#   define Crt      qCritical() << Q_FUNC_INFO << "#"
#endif

namespace REA {
    static const char * const   SoftwareIdentifier = "fr.PierreQR.RecognitionSuite.Gui.RELibraryAdmin";

    // ------------------------------------			Structures			------------------------------------ //
	// Sample image informations.
	class SampleImageInfo {
	public:
		// Types.
		typedef QList<REA::SampleImageInfo>
										List;
		// Constructors.
										SampleImageInfo			(quint32 dtId=0, QString const &fn="", QString const &h="", QString m="", QSize sz=QSize(), QByteArray s=QByteArray());
		// QDataStream friend serialization stuff.
		friend QDataStream&				operator<<				(QDataStream& s, SampleImageInfo const &sii)	{ return s << sii.documentTypeId << sii.filename << sii.hash << sii.micr << sii.size << sii.signature; }
		friend QDataStream&				operator>>				(QDataStream& s, SampleImageInfo &sii)			{ return s >> sii.documentTypeId >> sii.filename >> sii.hash >> sii.micr >> sii.size >> sii.signature; }
		// Members
		quint32							documentTypeId;
		QString							filename, hash, micr;
		QSize							size;
		QByteArray						signature;
	};

    /*! @brief Client Binary Generation Parameters Structure.
     * This structure holds parameters used when generating the Client Binary file based on the current project.
     * It is filled-up by the Client BinaryGenerator Wizard and passed to the REA::Core::generateClientBinary() method.
     * @see REA::ClientBinaryGenerator, REA::Core::generateClientBinary()
     */
	struct ClientBinaryParameters {
        //! @brief Recognizers Model Row Indexes Type Definition.
		typedef QMap<quint32,RE::T::UIntList> RecognizersRows;
        //! @brief Client Binary File Generation Full Path.
		QString					path;
        //! @brief Client Binary Human-Readable Name.
        QString					name;
        //! @brief Client Binary Human-Readable Copyright.
        QString					copyright;
        //! @brief Client Binary ANN Threshold for Type-Recognition Failure.
        quint8					annThreshold;
        //! @brief ANN Training Model Row Index to be Used for Generation.
        quint32					annRow;
         //! @brief Recognizers Model Row Indexes to be Used for Generation.
		RecognizersRows			recognizersRows;
	};
	
	// -------------------------------------		Other stuff			------------------------------------- //
	// Complete types registration for streaming and register metatypes.
	void								StaticInitializer		();
    char const*                         Version					();
}

#endif
