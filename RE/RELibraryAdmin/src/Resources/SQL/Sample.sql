INSERT INTO
    DocumentTypes (id, documentType_id, name, code, regExp, noAnnTraining, badMicrRejection, rejectionTestsScript, recognitionData)
    VALUES (NULL, 0, "Sans Nom", 1, "(\?|@)*", 0, 0, "", NULL);

INSERT INTO
	TrainedAnns (id, name, targetMse, learningRate, activationStepness, hiddenNeuronsCount, maxEpoch, reportsFrequency, documentTypeKeys, data)
    VALUES (NULL, "All", 0.00005, 0.5, 0.5, 32, 2500, 1, "1", NULL);
