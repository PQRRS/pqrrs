-- Backup old table.
ALTER TABLE		DocumentTypes
    RENAME TO	DocumentTypes_pre0_1_72;

-- Create the new table.
CREATE TABLE IF NOT EXISTS
    DocumentTypes (
        id                                  INTEGER PRIMARY KEY AUTOINCREMENT,
        documentType_id                     INTEGER DEFAULT 0,
        name					            TEXT,
        code					            INTEGER,
        noAnnTraining			            INTEGER,
        regExp					            TEXT,
        badMicrRejection		            INTEGER,
        subtypeSelectionScript              TEXT,
        subtypeSelectionSimulationStrings   BLOB,
        rejectionTestsScript	            TEXT,
        rejectionTestsSimulationStrings     BLOB,
        preprocessingFiltersData            BLOB,
        recognitionData			            BLOB
    );

-- Copy old values to new table.
INSERT INTO
    DocumentTypes		(id,documentType_id,name,code,noAnnTraining,regExp,badMicrRejection,subtypeSelectionScript,subtypeSelectionSimulationStrings,rejectionTestsScript,rejectionTestsSimulationStrings,preprocessingFiltersData,recognitionData)
    SELECT id,documentType_id,name,code,noAnnTraining,regExp,badMicrRejection,"",NULL,rejectionTestsScript,NULL,NULL,recognitionData
        FROM DocumentTypes_pre0_1_72;

-- Drop old DocumentTypes table.
DROP TABLE DocumentTypes_pre0_1_72;

-- Update version number.
INSERT OR REPLACE INTO Properties (key,value) VALUES ("Database.Version", "0.1.72");
