-- Backup old table.
ALTER TABLE		DocumentTypes
    RENAME TO	DocumentTypes_pre0_1_64_rc1;

-- Create the new table.
CREATE TABLE IF NOT EXISTS
    DocumentTypes (
        id                                  INTEGER PRIMARY KEY AUTOINCREMENT,
        documentType_id                     INTEGER DEFAULT 0,
        name					            TEXT,
        code					            INTEGER,
        regExp					            TEXT,
        noAnnTraining			            INTEGER,
        badMicrRejection		            INTEGER,
        subtypeSelectionScript              TEXT,
        subtypeSelectionSimulationStrings   BLOB,
        rejectionTestsScript	            TEXT,
        rejectionTestsSimulationStrings     BLOB,
        recognitionData			            BLOB
    );

-- Copy old values to new table.
INSERT INTO
    DocumentTypes		(id,documentType_id,name,code,regExp,noAnnTraining,badMicrRejection,subtypeSelectionScript,subtypeSelectionSimulationStrings,rejectionTestsScript,rejectionTestsSimulationStrings,recognitionData)
    SELECT id,documentType_id,name,code,regExp,noAnnTraining,badMicrRejection,"",NULL,rejectionTestsScript,NULL,recognitionData
        FROM DocumentTypes_pre0_1_64_rc1;

-- Drop old DocumentTypes table.
DROP TABLE DocumentTypes_pre0_1_64_rc1;

-- Update version number.
INSERT OR REPLACE INTO Properties (key,value) VALUES ("Database.Version", "0.1.64.rc1");
