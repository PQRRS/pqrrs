-- Update global script property number.
INSERT OR REPLACE INTO Properties (key,value) VALUES ("Global.Script", "// You can declare global functions here.");
INSERT OR REPLACE INTO Properties (key,value) VALUES ("Global.Copyright", "");

-- Update version number.
INSERT OR REPLACE INTO Properties (key,value) VALUES ("Database.Version", "0.1.54.rc1");
