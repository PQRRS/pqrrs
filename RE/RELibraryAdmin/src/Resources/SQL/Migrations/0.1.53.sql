-- Backup old table.
ALTER TABLE		DocumentTypes
	RENAME TO	DocumentTypes_pre0_1_53_rc1;

-- Create the new table.
CREATE TABLE IF NOT EXISTS
	DocumentTypes (
		id						INTEGER PRIMARY KEY AUTOINCREMENT,
		name					TEXT,
		code					INTEGER,
		regExp					TEXT,
		noAnnTraining			INTEGER,
		badMicrRejection		INTEGER,
		rejectionTestsScript	TEXT,
		recognitionData			BLOB
	);

-- Copy old values to new table.
INSERT INTO
	DocumentTypes		(id,name,code,regExp,noAnnTraining,badMicrRejection,recognitionData)
	SELECT id,name,code,regExp,noAnnTraining,badMicrRejection,recognitionData
		FROM DocumentTypes_pre0_1_53_rc1;

-- Make sure Properties table exists.
CREATE TABLE IF NOT EXISTS
	Properties (
		key						TEXT,
		value					TEXT
	);
CREATE UNIQUE INDEX IF NOT EXISTS idxPropertyKey ON Properties(key);


-- Drop old DocumentTypes table.
DROP TABLE DocumentTypes_pre0_1_53_rc1;

-- Update version number.
INSERT OR REPLACE INTO Properties (key,value) VALUES ("Database.Version", "0.1.53.rc1");
