-- Backup old table.
ALTER TABLE		DocumentTypes
    RENAME TO	DocumentTypes_pre0_1_63_rc1;

-- Create the new table.
CREATE TABLE IF NOT EXISTS
    DocumentTypes (
        id						INTEGER PRIMARY KEY AUTOINCREMENT,
        documentType_id         INTEGER DEFAULT 0,
        name					TEXT,
        code					INTEGER,
        regExp					TEXT,
        noAnnTraining			INTEGER,
        badMicrRejection		INTEGER,
        rejectionTestsScript	TEXT,
        recognitionData			BLOB
    );

-- Copy old values to new table.
INSERT INTO
    DocumentTypes		(id,documentType_id,name,code,regExp,noAnnTraining,badMicrRejection,rejectionTestsScript,recognitionData)
    SELECT id,0,name,code,regExp,noAnnTraining,badMicrRejection,rejectionTestsScript,recognitionData
        FROM DocumentTypes_pre0_1_63_rc1;

-- Drop old DocumentTypes table.
DROP TABLE DocumentTypes_pre0_1_63_rc1;

-- Update version number.
INSERT OR REPLACE INTO Properties (key,value) VALUES ("Database.Version", "0.1.63.rc1");
