CREATE TABLE IF NOT EXISTS
	DocumentTypes (
		id						INTEGER PRIMARY KEY AUTOINCREMENT,
        documentType_id         INTEGER DEFAULT 0,
		name					TEXT,
		code					INTEGER,
		regExp					TEXT,
		noAnnTraining			INTEGER,
		badMicrRejection		INTEGER,
		rejectionTestsScript	TEXT,
		recognitionData			BLOB
	);

CREATE TABLE IF NOT EXISTS
	SampleImages (
		id						INTEGER PRIMARY KEY AUTOINCREMENT,
		documentType_id			INTEGER,
		filename				TEXT,
		trainable				INTEGER,
		signature				BLOB,
		FOREIGN KEY (documentType_id) REFERENCES DocumentTypes(id) ON DELETE CASCADE
	);

CREATE TABLE IF NOT EXISTS
	TrainedAnns (
		id						INTEGER PRIMARY KEY AUTOINCREMENT,
		name					TEXT,
		targetMse				REAL,
		learningRate			REAL,
		activationStepness		REAL,
		hiddenNeuronsCount		INTEGER,
		maxEpoch				INTEGER,
		reportsFrequency		INTEGER,
		documentTypeKeys		TEXT,
		data					BLOB
	);

CREATE TABLE IF NOT EXISTS
	Properties (
		key						TEXT,
		value					TEXT
	);
CREATE UNIQUE INDEX			idxPropertyKey			ON Properties(key);
