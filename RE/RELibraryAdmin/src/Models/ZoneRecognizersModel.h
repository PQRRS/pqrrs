#ifndef REA_ZoneRecognizersModel_h
#define REA_ZoneRecognizersModel_h

// Base class.
#include <QtCore/QAbstractItemModel>
#include <RELibrary/RETypes>

namespace REA {

class ZoneRecognizersModel : public QAbstractItemModel {
Q_OBJECT
public:
	// Constructor.
										ZoneRecognizersModel			(QObject *p=0);
	// Destructor.
										~ZoneRecognizersModel			();
	void								setZoneRecognizers				(RE::T::ZoneRecognizerData::List const &zrds);
	RE::T::ZoneRecognizerData::List const&	zoneRecognizers					() const;
	// Reimplemented methods.
	QModelIndex							index							(int row, int column, const QModelIndex &parent=QModelIndex()) const;
	bool								isRoot							(QModelIndex const &idx) const;
	bool								isRecognizer					(QModelIndex const &idx) const;
	bool								isFilter						(QModelIndex const &idx) const;
	QModelIndex							parent							(const QModelIndex &index) const;
	int									rowCount						(const QModelIndex &parent=QModelIndex()) const;
	int									columnCount						(const QModelIndex &parent=QModelIndex()) const;
    Qt::ItemFlags						flags							(const QModelIndex &index) const;
    bool                                insertRows                      (int position, int count, const QModelIndex &parent=QModelIndex());
	bool								removeRows						(int position, int count, const QModelIndex &parent=QModelIndex());
	QVariant							data							(const QModelIndex &index, int role) const;
	bool								setData							(QModelIndex const &index, QVariant const &value, int role=Qt::EditRole);
	
	bool								createRecognizer				(RE::T::ZoneRecognizerData const &zrd);
	bool								createFilter					(QModelIndex const &parent, RE::T::ZoneFilterData const &zrf);
	bool								setRecognizerRelativeRect		(QModelIndex const &index, QRectF const &relRect);
	bool								setParameter					(QModelIndex const &parent, QString const &name, QVariant const &value);
    bool                                removeParameter                 (QModelIndex const &parent, QString const &name);

	bool								moveUp							(int row, QModelIndex const &parent);
	bool								moveDown						(int row, QModelIndex const &parent);

private:
	class TreeItem;
	TreeItem							*_root;
	RE::T::ZoneRecognizerData::List		_recognizers;
};

}

#endif
