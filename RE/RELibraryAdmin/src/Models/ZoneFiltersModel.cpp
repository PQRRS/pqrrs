#include "ZoneFiltersModel.h"
#include <RELibrary/Engine>

REA::ZoneFiltersModel::ZoneFiltersModel(QObject *parent) :
    QAbstractItemModel(parent) {
}

RE::T::ZoneFilterData::List const& REA::ZoneFiltersModel::filters () const {
    return _filters;
}
void REA::ZoneFiltersModel::setFilters (RE::T::ZoneFilterData::List const &filters) {
    beginResetModel ();
    _filters        = filters;
    endResetModel   ();
}

QModelIndex REA::ZoneFiltersModel::index (int r, int c, QModelIndex const &p) const {
    return hasIndex(r, c, p) ? createIndex(r, c) : QModelIndex();
}
QModelIndex REA::ZoneFiltersModel::parent (QModelIndex const&) const {
    return QModelIndex();
}
int REA::ZoneFiltersModel::rowCount (QModelIndex const &p) const {
    return p.isValid()? 0 : _filters.count();
}
int REA::ZoneFiltersModel::columnCount (QModelIndex const&) const {
    return 1;
}
Qt::ItemFlags REA::ZoneFiltersModel::flags (const QModelIndex &i) const {
    return i.isValid() ? Qt::ItemIsEnabled | Qt::ItemIsSelectable : Qt::NoItemFlags;
}
bool REA::ZoneFiltersModel::insertRows (int position, int count, const QModelIndex &parent) {
    beginInsertRows						(parent, position, position+count-1);
    _filters.insert                     (position, RE::T::ZoneFilterData());
    endInsertRows						();
    return true;
}
bool REA::ZoneFiltersModel::removeRows (int position, int count, const QModelIndex &parent) {
    if (parent.isValid())               return false;
    beginRemoveRows						(parent, position, position+count-1);
    for (int i=0; i<count; i++)
        _filters.removeAt               (position);
    endRemoveRows						();
    return true;
}

QVariant REA::ZoneFiltersModel::data (QModelIndex const &i, int r) const {
    if (!i.isValid() || (r!=Qt::DisplayRole && r!=Qt::UserRole))
        return	QVariant();
    else if (r==Qt::UserRole) {
        if (!i.isValid())
            return QVariant::fromValue(_filters);
        else
            return QVariant::fromValue(_filters[i.row()]);
    }
    else
        return RE::ClassNameToNaturalString(_filters[i.row()].objectClassName);
    return			QVariant();
}
bool REA::ZoneFiltersModel::setData (QModelIndex const &i, QVariant const &v, int r) {
    Q_UNUSED(i);
    Q_UNUSED(v);
    Q_UNUSED(r);
    return true;
}

bool REA::ZoneFiltersModel::createFilter (RE::T::ZoneFilterData const &zrf) {
    bool						toRet;
    quint32						row		= _filters.count();
    beginInsertRows						(QModelIndex(), row, row);
    if ((toRet=insertRows(row, 1, QModelIndex())))
        _filters[row]                   = zrf;
    else
        Wrn                             << "Failed to insert a Filter at row" << row << "!";
    endInsertRows                       ();
    return toRet;
}
bool REA::ZoneFiltersModel::setParameter (QModelIndex const &index, QString const &name, QVariant const &value) {
    if (!index.isValid())               return false;
    RE::T::ZoneFilterData		&zrf	= _filters[index.row()];
    zrf.parameters[name]				= value;
    emit								dataChanged(index, index);
    return								true;
}
bool REA::ZoneFiltersModel::removeParameter (QModelIndex const &index, QString const &name) {
    if (!index.isValid())               return false;
    RE::T::ZoneFilterData		&zrf	= _filters[index.row()];
    if (!zrf.parameters.contains(name))
        return                          false;
    zrf.parameters.remove(name);
    emit								dataChanged(index, index);
    return								true;
}

bool REA::ZoneFiltersModel::moveUp (int row) {
    quint32         dstRow          = row-1;
    if (row==0)                     return false;
    beginMoveRows                   (QModelIndex(), row, row, QModelIndex(), dstRow);
    _filters.swap                   (row, dstRow);
    endMoveRows                     ();
    return true;
}
bool REA::ZoneFiltersModel::moveDown (int row) {
    quint32         dstRow          = row+1;
    if (row==_filters.count()-1)    return false;
    beginMoveRows                   (QModelIndex(), dstRow, dstRow, QModelIndex(), row);
    _filters.swap                   (row, dstRow);
    endMoveRows                     ();
    return                          true;
}
