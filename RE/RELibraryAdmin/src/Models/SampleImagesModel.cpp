#include "SampleImagesModel.h"
// Required stuff.
#include "../Core.h"
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlIndex>
#include <QtSql/QSqlRelation>

REA::SampleImagesModel::SampleImagesModel (QObject *p, QSqlDatabase const &db)
: QSqlTableModel(p, db), _core(&Core::Instance()) {
    // Setup model table and edition strategy.
    setTable				("SampleImages");
    setEditStrategy			(QSqlTableModel::OnFieldChange);
    // Setup headers.
    setHeaderData			(Key,			Qt::Horizontal, tr("Key"));
    setHeaderData			(DocumentType,	Qt::Horizontal, tr("Document Type"));
    setHeaderData			(Filename,		Qt::Horizontal, tr("Filename"));
    setHeaderData			(Training,		Qt::Horizontal, tr("Train."));
    setHeaderData			(Signature,		Qt::Horizontal, tr("Signature"));
}

Qt::ItemFlags REA::SampleImagesModel::flags (const QModelIndex &i) const {
    Qt::ItemFlags		flags	= QSqlTableModel::flags(i) | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    if (i.column()==Training) {
        flags					|= Qt::ItemIsUserCheckable;
        flags					&= Qt::ItemIsEditable;
    }
    return						flags;
}

void REA::SampleImagesModel::setDocumentTypeId (quint32 const &dtId) {
    setFilter					(QString("documentType_id=%1 OR documentType_id=NULL").arg(dtId));
    select						();
}

bool REA::SampleImagesModel::select () {
    bool	toRet	= QSqlTableModel::select();
    while (canFetchMore()) fetchMore();
    return	toRet;
}
int REA::SampleImagesModel::columnCount (const QModelIndex&) const {
    return Count;
}
QVariant REA::SampleImagesModel::data (const QModelIndex &i, int r) const {
    quint32					col					= i.column();
    // Invalid index...
    if (!i.isValid())							return QVariant();
    // Display tooltip...
    else if (r==Qt::ToolTipRole) {
        QSqlRecord const	&rec		= record(i.row());
        QStringList			messages;
        QString             filename    = rec.value("filename").toString();
        filename                        = _core->projectDirectory().absoluteFilePath(filename);
        if (!QFileInfo(filename).exists())
            messages                    << tr("The specified file doesn't exist.");
        if (!rec.value("signature").toByteArray().size())
            messages                    << tr("Signature hasn't been computed yet.");
        return tr("Item #%1 - Status:\r\n%2").arg(rec.value("id").toInt()).arg(messages.count() ? messages.join("\r\n") : tr("Sane."));
    }
    // Is Training column.
    else if (col==Training)
        return r==Qt::CheckStateRole ? (QSqlTableModel::data(i).toInt()!=0 ? Qt::Checked : Qt::Unchecked) : QVariant();
    return QSqlTableModel::data(i, r);
}
bool REA::SampleImagesModel::setData (const QModelIndex &i, const QVariant &v, int r) {
    QMutexLocker    ml      (&_mutex);
    if (i.column()==Training && r==Qt::CheckStateRole)
        return QSqlTableModel::setData(i, v==Qt::Checked ? 1 : 0, Qt::EditRole);
    return QSqlTableModel::setData(i, v, r);
}
