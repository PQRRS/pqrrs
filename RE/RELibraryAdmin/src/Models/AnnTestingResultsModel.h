#ifndef REA_AnnTestingResultsModel_h
#define REA_AnnTestingResultsModel_h

// Base class.
#include <QtCore/QAbstractTableModel>
// Required stuff.
#include "../REATypes.h"
#include <RELibrary/RETypes>
#include <QtCore/QMutex>
#include <QtCore/QMutex>
#include <QtGui/QColor>

namespace REA {

class AnnTestingResultsModel : public QAbstractTableModel {
Q_OBJECT
public:
    typedef QHash<quint32, RE::T::UIntToDoubleHashList> AnnResultSetsHash;
    // Columns indices helpers.
    enum { Successes = 0, Failures, Ratio, HighestConfidence, LowestConfidence, AverageConfidence, MisstakenWith, Count };
    // Constructor.
    /**/					AnnTestingResultsModel	(QObject *p);
    // Reimplemented methods.
    void					reset					(RE::T::UIntToStringHash const &documentTypesList);
    void					addResult				(quint32 dt, RE::T::UIntToDoubleHash const &rs);
    virtual QVariant		headerData				(int c, Qt::Orientation o, int r=Qt::DisplayRole) const;
    Qt::ItemFlags			flags					(QModelIndex const &i) const;
    virtual QVariant		data					(QModelIndex const &i, int r=Qt::EditRole) const;
    virtual int				rowCount				(QModelIndex const &p=QModelIndex()) const;
    virtual int				columnCount				(QModelIndex const &i) const;

private:
    RE::T::UIntToStringHash	_dtNames;
    RE::T::UIntList			_dtIds;
    AnnResultSetsHash		_resultSets;
    // Data caching.
    mutable QMutex			_mutex;
    mutable QHash<quint32,bool>
                            _dataChanged;
    mutable QHash<quint32,QHash<quint32, QHash<quint32, QVariant > > >
                            _dataCache;
    // Colors...
    static QColor			goodColor,
                            badColor;
};

}

#endif
