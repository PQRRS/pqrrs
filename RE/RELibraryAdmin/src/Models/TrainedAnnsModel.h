#ifndef REA_TrainedAnnsModel_hpp
#define REA_TrainedAnnsModel_hpp

// Base class.
#include <QtSql/QSqlTableModel>

namespace REA {

class TrainedAnnsModel : public QSqlTableModel {
Q_OBJECT
public:
	// Columns.
	enum { Key = 0, Name, TargetMse, LearningRate, ActivationStepness, HiddenNeuronsCount, MaxEpoch, ReportsFrequency, DocumentTypesKeys, Data, Status, Count };
	// Constructor.
	/**/				TrainedAnnsModel	(QObject *p, QSqlDatabase const &db);
	Qt::ItemFlags		flags				(const QModelIndex &i) const;
	virtual int			columnCount			(const QModelIndex &i) const;
	virtual QVariant	data				(const QModelIndex &i, int r=Qt::EditRole) const;
	bool				setData				(QModelIndex const &i, QVariant const &v, int r=Qt::EditRole);
};

}

#endif
