#include "AnnTestingResultsModel.h"
// Required stuff.
#include "../Core.h"
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <math.h>

QColor	REA::AnnTestingResultsModel::goodColor	(0xccffcc);
QColor	REA::AnnTestingResultsModel::badColor	(0xffcccc);

REA::AnnTestingResultsModel::AnnTestingResultsModel (QObject *p) : QAbstractTableModel(p) {
}

void REA::AnnTestingResultsModel::reset (RE::T::UIntToStringHash const &documentTypesList) {
	_resultSets.clear							();
	_dataCache.clear							();
	_dtIds.clear								();
	
	_dtNames									= documentTypesList;
	foreach (quint32 dtId, documentTypesList.keys()) {
		_dtIds									<< dtId;
		_resultSets[dtId]						= RE::T::UIntToDoubleHashList();
	}
	emit headerDataChanged (Qt::Horizontal, 0, 1);
}
void REA::AnnTestingResultsModel::addResult (quint32 dtId, RE::T::UIntToDoubleHash const &rs) {
	QMutexLocker					ml			(&_mutex);
	_resultSets[dtId].append					(rs);
	_dataCache[dtId].clear						();
	emit dataChanged (index(_dtIds.indexOf(dtId), Successes), index(_dtIds.indexOf(dtId), AverageConfidence));
}

QVariant REA::AnnTestingResultsModel::headerData (int c, Qt::Orientation o, int r) const {
	if (r!=Qt::DisplayRole)				return QVariant();
	if (o==Qt::Horizontal) {
		switch (c) {
			case Successes:				return tr("Successes");
			case Failures:				return tr("Failures");
			case Ratio:					return tr("Ratio");
			case HighestConfidence:		return tr("Highest");
			case LowestConfidence:		return tr("Lowest");
			case AverageConfidence:		return tr("Average");
			case MisstakenWith:			return tr("Misstaken with");
			default:					return QVariant();
		}
	}
	else
		if (_dtIds.contains(c) || _dtNames.contains(_dtIds[c]))
			return						_dtNames[_dtIds[c]];
	return QVariant();
}
Qt::ItemFlags REA::AnnTestingResultsModel::flags (QModelIndex const &i) const {
	Q_UNUSED(i)
	return Qt::ItemIsEnabled;
}
// Get data for a given cell.
QVariant REA::AnnTestingResultsModel::data (QModelIndex const &i, int r) const {
	qint32						row					= i.row();
	qint32						col					= i.column();
	
	if ((r!=Qt::DisplayRole && r!=Qt::BackgroundColorRole) || !i.isValid() || row<0 || row>=_dtIds.count())
		return QVariant();
	
	quint32						dtId				= _dtIds[i.row()];
	RE::T::UIntToDoubleHashList	rss					= _resultSets[dtId];
	if (rss.isEmpty())								return tr("No Data");
	
	QMutexLocker				ml					(&_mutex);
	// If data cache contains precomputed value, use it.
	if (_dataCache.contains(dtId) && _dataCache[dtId].contains(col) && _dataCache[dtId][col].contains(r))
		return										_dataCache[dtId][col][r];
	ml.unlock										();

	QVariant					toRet;
	switch (col) {
		// Success percentage.
		case Successes:
		case Failures:
		case Ratio:
		case MisstakenWith: {
			quint32			good				= 0;
			quint32			bad					= 0;
            quint32			maxId               = 0;
            double			val, max;
			RE::T::UIntToUIntHash	mw;
			RE::T::UIntToDoubleHash	mwMax, mwMin;
			foreach (RE::T::UIntToDoubleHash rs, rss) {
				max								= 0.0;
				for (RE::T::UIntToDoubleHash::const_iterator i=rs.constBegin(); i!=rs.constEnd(); ++i) {
					val							= i.value();
					if (val>max) {
						maxId					= i.key();
						max						= val;
					}
				}
				if (dtId==maxId)
					good++;
				else {
					mw[maxId]					++;
					if (!mwMax.contains(maxId) || mwMax[maxId]<max)
						mwMax[maxId]	= max;
					if (!mwMin.contains(maxId) || mwMin[maxId]>max)
						mwMin[maxId]	= max;
					bad++;
				}
			}
			// Display value.
			if (r==Qt::DisplayRole) {
				if (col==Ratio)
					toRet						= 100.0 / rss.count() * good;
				else if (col==MisstakenWith) {
					if (mw.isEmpty())
						break;
					QStringList	sz;
					for (RE::T::UIntToUIntHash::const_iterator i=mw.constBegin(); i!=mw.constEnd(); ++i)
						sz						<< tr("%1 x %2 (%3 to %4)")
													.arg(_dtNames[i.key()])
													.arg(i.value())
													.arg((quint32)(mwMin[i.key()]*100.0))
													.arg((quint32)(mwMax[i.key()]*100.0));
					toRet						= sz.join(", ");
				}
				else
					toRet						= col==Successes ? good : bad;
			}
			// BG color.
			else
				return							bad ? badColor : goodColor;
			break;
		}
		// Lowest rate column.
		case HighestConfidence:
		case LowestConfidence:
		case AverageConfidence: {
			double			rate;
			double			min					= 1.0;
			double			max					= 0.0;
			double			avg				= 0.0;
			foreach (RE::T::UIntToDoubleHash rs, rss) {
				rate						= rs[dtId];
				max							= qMax(max, rate);
				min							= qMin(min, rate);
				avg							+= rate;
			}
			avg								/= rss.count();
				
			// Display value.
			if (r==Qt::DisplayRole) {
				if (col==HighestConfidence)		toRet = floor(max*100);
				else if (col==LowestConfidence)	toRet = floor(min*100);
				else if (col==AverageConfidence)toRet = floor(avg*100);
			}
			// BG color.
			else {
				if (col==HighestConfidence)		toRet = max>=.75 ? goodColor : badColor;
				else if (col==LowestConfidence)	toRet = min>=.75 ? goodColor : badColor;
				else if (col==AverageConfidence)toRet = avg>=.75 ? goodColor : badColor;
			}
			break;
		}
	}
	ml.relock									();
	_dataCache[dtId][col][r]					= toRet;
	// Not a valid column.
	return toRet;
}

int REA::AnnTestingResultsModel::rowCount (QModelIndex const &i) const {
	return i.isValid() ? 0 : _dtIds.count();
}
int REA::AnnTestingResultsModel::columnCount (QModelIndex const &i) const {
	return i.isValid() ? 0 : Count;
}
