#ifndef REA_DocumentTypesModel_h
#define REA_DocumentTypesModel_h

// Base class.
#include <QtCore/QAbstractItemModel>
// Required stuff.
#include <QtCore/QVariant>

class QSqlDatabase;

namespace REA {

class DocumentTypesModel : public QAbstractItemModel {
Q_OBJECT
public:
	// Columns indices helpers.
    enum { Key = 0,
           ParentKey, Name, Code,
           NoAnnTraining,
           MicrRegExp, BadMicrRejection,
           SubtypeSelectionScript, SubtypeSelectionSimulationStrings,
           RejectionTestsScript, RejectionTestsSimulationStrings,
           PreprocessingFiltersData, RecognitionData, Status, Count };
    // Constructor / Destructor.
	/**/				DocumentTypesModel			(QObject *p, QSqlDatabase const &db);
    /**/                ~DocumentTypesModel         ();
    // Methods.
	void				triggerUpdate				(QModelIndex const &idx);
	static int			sampleImagesCount			(QString const &filter);
	static int			trainableSampleImagesCount	(QString const &filter);
    void                populate                    (quint32 pKey=0, void *pItem=0);
    // Reimplemented methods.
    QVariant            headerData                  (int s, Qt::Orientation o, int r=Qt::DisplayRole) const;
    Qt::ItemFlags		flags						(QModelIndex const&i) const;
    int					columnCount					(QModelIndex const &i=QModelIndex()) const;
    int                 rowCount                    (QModelIndex const &i=QModelIndex()) const;
    QModelIndex         index                       (int r, int c, QModelIndex const &p=QModelIndex()) const;
    QModelIndex         parent                      (QModelIndex const &c) const;
    bool                hasChildren                 (QModelIndex const &i) const;
    QVariant			data						(QModelIndex const &i, int r=Qt::DisplayRole) const;
    bool				setData						(QModelIndex const &i, QVariant const &v, int r=Qt::EditRole);
    bool                insertRow                   (int r, QModelIndex const &p);
    bool                removeRow                   (int r, QModelIndex const &p);

private:
    QSqlDatabase const  *_db;
    void                *_rootItem;
};

}

#endif
