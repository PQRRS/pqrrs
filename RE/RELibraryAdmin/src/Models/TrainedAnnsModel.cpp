#include "TrainedAnnsModel.h"
// Required stuff.
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlIndex>

REA::TrainedAnnsModel::TrainedAnnsModel (QObject *p, QSqlDatabase const &db)
: QSqlTableModel(p, db) {
	// Setup model table and edition strategy.
	setTable				("TrainedAnns");
	setEditStrategy			(QSqlTableModel::OnFieldChange);
	// Setup headers.
	setHeaderData			(Key,						Qt::Horizontal, tr("Key"));
	setHeaderData			(Name,						Qt::Horizontal, tr("Name"));
	setHeaderData			(TargetMse,					Qt::Horizontal, tr("Target MSE"));
	setHeaderData			(ActivationStepness,		Qt::Horizontal, tr("Act. Step."));
	setHeaderData			(LearningRate,				Qt::Horizontal, tr("Learn. Rate"));
	setHeaderData			(HiddenNeuronsCount,		Qt::Horizontal, tr("Hid. Neur."));
	setHeaderData			(MaxEpoch,					Qt::Horizontal, tr("Max. Epoch"));
	setHeaderData			(ReportsFrequency,			Qt::Horizontal, tr("Reports Freq."));
	setHeaderData			(DocumentTypesKeys,			Qt::Horizontal, tr("Selection"));
	setHeaderData			(Data,						Qt::Horizontal, tr("Data"));
	setHeaderData			(Status,					Qt::Horizontal, tr("S"));
}

Qt::ItemFlags REA::TrainedAnnsModel::flags (const QModelIndex &i) const {
	Qt::ItemFlags	f	= QSqlTableModel::flags(i) | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	if (i.column()==Status)
        f				|= Qt::ItemIsEditable|Qt::ItemIsUserCheckable;
	return				f;
}
int REA::TrainedAnnsModel::columnCount (const QModelIndex &i) const {
	Q_UNUSED(i);
	return QSqlTableModel::columnCount(i)+1;
}
QVariant REA::TrainedAnnsModel::data (const QModelIndex &i, int r) const {
	if (!i.isValid())
		return QVariant();
	// Virtual "Sample Images Count" column.
	else if (i.column()==Status) {
		QVariant	v	= record(i.row()).value("data");
		return v.isValid() ? v.toByteArray().length()!=0 : false;
	}
	else
		return QSqlTableModel::data(i, r);
	return i.isValid() ? QSqlTableModel::data(i, r) : QVariant();
}
bool REA::TrainedAnnsModel::setData (QModelIndex const &i, QVariant const &v, int r) {
	bool			block	= i.column()!=Name && !signalsBlocked();
	if (block)				blockSignals(true);
	bool			ok		= QSqlTableModel::setData(i, v, r);
	if (block)				blockSignals(false);
	emit					dataChanged(i, i);
	return ok;
}
