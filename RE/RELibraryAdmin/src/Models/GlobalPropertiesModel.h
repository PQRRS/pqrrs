#ifndef REA_GlobalPropertiesModel_h
#define REA_GlobalPropertiesModel_h

// Base class.
#include <QtSql/QSqlTableModel>
// Required stuff.
#include <QtCore/QVariant>

namespace REA {

class GlobalPropertiesModel : public QSqlTableModel {
Q_OBJECT
public:
	// Columns indices helpers.
	enum { Key, Value /**/, Count };
	// Constructor.
	/**/				GlobalPropertiesModel		(QObject *p, QSqlDatabase const &db);
	// Reimplemented methods.
	int					columnCount					(QModelIndex const &i) const;
	// K/V stuff.
	QString				valueForKey					(QString const &key) const;
	void				setValueForKey				(QString const &key, QString const &value);
};

};

#endif
