// Base class.
#include "ZoneRecognizersModel.h"
#include "../REATypes.h"
#include <QtCore/QDebug>

// Internal structure to hold data.
namespace REA {
    class ZoneRecognizersModel::TreeItem {
    public:
        // Constructor.
                                    TreeItem				(TreeItem *parnt=0) : parent(parnt) {}
        // Destructor.
                                    ~TreeItem				() { qDeleteAll(children); }
        // Identity getters.
        bool						isRoot					() const { return !parent; }
        bool						isRecognizer			() const { return parent && !parent->parent; }
        bool						isFilter				() const { return parent && parent->isRecognizer(); }
        // Row getter.
        quint32						row						() const { return parent->children.indexOf((TreeItem* const)this); }
        // Children count.
        quint32						chilrenCount			() const { return children.count(); }
        // Members.
        TreeItem					*parent;
        QList<TreeItem*>			children;
    };
}

// Constructor.
REA::ZoneRecognizersModel::ZoneRecognizersModel	(QObject *p)
: QAbstractItemModel(p), _root(0) {
}
// Destructor.
REA::ZoneRecognizersModel::~ZoneRecognizersModel () {
    if (_root)	delete _root;
}
void REA::ZoneRecognizersModel::setZoneRecognizers (RE::T::ZoneRecognizerData::List const &zrds) {
    beginResetModel();
    if (_root)									delete	_root;
    _recognizers								= zrds;
    _root										= new TreeItem();
    // Loop for each recognizer.
    quint32								rC		= _recognizers.count();
    for (quint32 i=0; i<rC; i++) {
        RE::T::ZoneRecognizerData		&zrd	= _recognizers[i];
        TreeItem						*rItm	= new TreeItem(_root);//, QVariant::fromValue(zrd));
        _root->children							<< rItm;
        // Loop for each filter.
        RE::T::ZoneFilterData::List		&zfds	= zrd.filterStack;
        quint32							fC		= zfds.count();
        for (quint32 j=0; j<fC; j++) {
            // TODO???
            //RE::T::ZoneFilterData		&zfd	= zfds[j];
            TreeItem					*fItm	= new TreeItem(rItm);//, QVariant::fromValue(zfd));
            rItm->children						<< fItm;
        }
    }
    endResetModel();
}
RE::T::ZoneRecognizerData::List const& REA::ZoneRecognizersModel::zoneRecognizers () const {
    return _recognizers;
}

// Reimplemented methods.
QModelIndex REA::ZoneRecognizersModel::index (int row, int column, const QModelIndex &parent) const {
    if (!hasIndex(row, column, parent))
        return QModelIndex();
    TreeItem	*pItm	= parent.isValid() ? (TreeItem*)parent.internalPointer() : _root;
    TreeItem	*itm	= pItm->children[row];
    return				itm ? createIndex(row, column, itm) : QModelIndex();
}
bool REA::ZoneRecognizersModel::isRoot (QModelIndex const &idx) const {
    TreeItem	*itm	= (TreeItem*)idx.internalPointer();
    return !itm || itm->isRoot();
}
bool REA::ZoneRecognizersModel::isRecognizer (QModelIndex const &idx) const {
    TreeItem	*itm	= (TreeItem*)idx.internalPointer();
    return itm && itm->isRecognizer();
}
bool REA::ZoneRecognizersModel::isFilter (QModelIndex const &idx) const {
    TreeItem	*itm	= (TreeItem*)idx.internalPointer();
    return itm && itm->isFilter();
}

QModelIndex REA::ZoneRecognizersModel::parent (const QModelIndex &index) const {
    if (!index.isValid())
        return QModelIndex();
    TreeItem	*itm	= (TreeItem*)index.internalPointer();
    TreeItem	*pItm	= itm->parent;
    return				pItm==_root ? QModelIndex() : createIndex(pItm->row(), 0, pItm);
}
int REA::ZoneRecognizersModel::rowCount (const QModelIndex &parent) const {
    TreeItem	*pItm	= parent.isValid() ? (TreeItem*)parent.internalPointer() : _root;
    return parent.column()>0 || !pItm ? 0 : pItm->children.count();
}
int REA::ZoneRecognizersModel::columnCount (const QModelIndex&) const {
    return 1;
}
Qt::ItemFlags REA::ZoneRecognizersModel::flags (const QModelIndex &index) const {
    return index.isValid() ? Qt::ItemIsEnabled|Qt::ItemIsSelectable : Qt::NoItemFlags;
}
bool REA::ZoneRecognizersModel::insertRows (int position, int count, const QModelIndex &parent) {
    TreeItem			*pItm			= (TreeItem*)parent.internalPointer();
    if (!pItm)							pItm = _root;
    if (position<0 || position>pItm->children.count() || pItm->isFilter()) {
        Wrn << "Insert rows failed!";
        return false;
    }
    beginInsertRows						(parent, position, position+count-1);
    for (int i=0; i<count; i++) {
        if (pItm->isRoot())
            _recognizers.insert(position, RE::T::ZoneRecognizerData());
        else if (pItm->isRecognizer())
            _recognizers[parent.row()].filterStack.insert(position, RE::T::ZoneFilterData());
        pItm->children.insert			(position, new TreeItem(pItm));
    }
    endInsertRows						();
    return true;
}
bool REA::ZoneRecognizersModel::removeRows (int position, int count, const QModelIndex &parent) {
    TreeItem			*pItm			= (TreeItem*)parent.internalPointer();
    if (!pItm)							pItm = _root;
    if (position<0 || position+count>pItm->children.count() || pItm->isFilter()) {
        Wrn << "Delete rows failed!";
        return false;
    }
    beginRemoveRows						(parent, position, position+count-1);
    for (int i=0; i<count; i++) {
        if (pItm->isRoot())				_recognizers.takeAt(position);
        else if (pItm->isRecognizer())	_recognizers[parent.row()].filterStack.takeAt(position);
        delete pItm->children.takeAt	(position);
    }
    endRemoveRows						();
    return true;
}
QVariant REA::ZoneRecognizersModel::data (const QModelIndex &index, int role) const {
    TreeItem			*item	= (TreeItem*)index.internalPointer();
    if (!index.isValid() || (role!=Qt::DisplayRole && role!=Qt::UserRole) || !item || item->isRoot())
        return	QVariant();
    else if (role==Qt::UserRole) {
        if (item->isRoot())
            return QVariant::fromValue(_recognizers);
        else if (item->isRecognizer())
            return QVariant::fromValue(_recognizers[index.row()]);
        else if (item->isFilter())
            return QVariant::fromValue(_recognizers[index.parent().row()].filterStack[index.row()]);
    }
    else {
        if (item->isRecognizer())
            return _recognizers.at(index.row()).objectClassName;
        else if (item->isFilter())
            return RE::ClassNameToNaturalString(_recognizers[index.parent().row()].filterStack[index.row()].objectClassName);
    }
    return			QVariant();
}
bool REA::ZoneRecognizersModel::setData (QModelIndex const &index, QVariant const &value, int role) {
    TreeItem			*item	= (TreeItem*)index.internalPointer();
    if (!index.isValid() || (role!=Qt::DisplayRole && role!=Qt::UserRole) || !item || item->isRoot()) {
        Wrn << "Invalid index (" << index.row() << "," << index.column() << ") or role (" << role << ") for setting data!";
        return false;
    }
    else if (item->isRecognizer()) {
        quint32		rRow	= index.row();
        Dbg << "Setting data for Recognizer at" << index << "...";
        _recognizers[rRow] = value.value<RE::T::ZoneRecognizerData>();
        emit	dataChanged(index, index);
        return true;
    }
    else if (item->isFilter()) {
        Dbg << "Setting data for Filter at" << index << "...";
        quint32		rRow	= index.parent().row();
        quint32		fRow	= index.row();
        _recognizers[rRow].filterStack[fRow] = value.value<RE::T::ZoneFilterData>();
        emit	dataChanged(index, index);
        return true;
    }
    Wrn << "Something went wrong for index (" << index.row() << "," << index.column() << ") or role (" << role << ") while setting data!";
    return false;
}

bool REA::ZoneRecognizersModel::createRecognizer (RE::T::ZoneRecognizerData const &zrd) {
    bool						toRet;
    quint32						row		= _recognizers.count();
    QModelIndex					parent;
    quint32						fCount	= zrd.filterStack.count();
    beginInsertRows						(parent, row, row);
    if ((toRet=insertRows(row, 1, parent))) {
        _recognizers[row]				= zrd;
        // The added recognizer contains filters...
        if (zrd.filterStack.count()) {
            TreeItem			*zrdItm	= (TreeItem*)index(row, 0).internalPointer();
            // Add as many tree items to the one just added as there are filters.
            for (quint32 i=0; i<fCount; ++i)
                zrdItm->children		<< new TreeItem(zrdItm);
        }
    }
    else
        Wrn << "Failed to insert a Recognizer at row" << row << "!";
    endInsertRows						();
    return toRet;
}
bool REA::ZoneRecognizersModel::createFilter (QModelIndex const &parent, RE::T::ZoneFilterData const &zrf) {
    if (!isRecognizer(parent)) {
        Wrn << "An atempt to add a filter to a parent which was not a Recognizer was made!";
        return false;
    }
    bool						toRet;
    RE::T::ZoneRecognizerData	&zrd	= _recognizers[parent.row()];
    RE::T::ZoneFilterData::List	&zrfs	= zrd.filterStack;
    quint32						row		= zrfs.count();
    beginInsertRows						(parent, row, row);
    if ((toRet=insertRows(row, 1, parent)))
        zrfs[row]						= zrf;
    else
        Wrn << "Failed to insert a Filter at row" << row << "in Recognizer at" << parent.row() << "," << parent.column() << "!";
    endInsertRows						();
    return toRet;
}
bool REA::ZoneRecognizersModel::setRecognizerRelativeRect (QModelIndex const &index, QRectF const &relRect) {
    TreeItem					*pItm	= (TreeItem*)index.internalPointer();
    if (!pItm->isRecognizer()) {
        Wrn << "Something went wrong for index (" << index.row() << "," << index.column() << ") while setting Relative Rect!";
        return false;
    }
    // Set recognizer parameter.
    Dbg << "Setting Relative Rect for Recognizer at" << index << "...";
    RE::T::ZoneRecognizerData	&zrd	= _recognizers[index.row()];
    if (zrd.relativeRect==relRect)		return true;
    zrd.relativeRect					= relRect;
    emit dataChanged					(index, index);
    return true;
}
bool REA::ZoneRecognizersModel::setParameter (QModelIndex const &index, QString const &name, QVariant const &value) {
    TreeItem					*pItm		= (TreeItem*)index.internalPointer();
    // Set recognizer parameter.
    if (pItm->isRecognizer()) {
        RE::T::ZoneRecognizerData	&zrd	= _recognizers[index.row()];
        zrd.parameters[name]				= value;
        emit dataChanged					(index, index);
        return true;
    }
    // Set filter parameter.
    else if (pItm->isFilter()) {
        RE::T::ZoneFilterData		&zrf	= _recognizers[index.parent().row()].filterStack[index.row()];
        zrf.parameters[name]				= value;
        emit								dataChanged(index, index);
        return								true;
    }
    else {
        Wrn << "Inconsistency detected while trying to set a parameter named" << name << "on index" << index.row() << "," << index.column() << "!";
        return false;
    }
}
bool REA::ZoneRecognizersModel::removeParameter (QModelIndex const &index, QString const &name) {
    TreeItem					*pItm		= (TreeItem*)index.internalPointer();
    // Set recognizer parameter.
    if (pItm->isRecognizer()) {
        RE::T::ZoneRecognizerData	&zrd	= _recognizers[index.row()];
        zrd.parameters.remove(name);
        emit dataChanged					(index, index);
        return true;
    }
    // Set filter parameter.
    else if (pItm->isFilter()) {
        RE::T::ZoneFilterData		&zrf	= _recognizers[index.parent().row()].filterStack[index.row()];
        zrf.parameters.remove(name);
        emit								dataChanged(index, index);
        return								true;
    }
    else {
        Wrn << "Inconsistency detected while trying to remove a parameter named" << name << "on index" << index.row() << "," << index.column() << "!";
        return false;
    }
}

bool REA::ZoneRecognizersModel::moveUp (int row, QModelIndex const &parent) {
    TreeItem					*pItm		= (TreeItem*)parent.internalPointer();
    if (!pItm)					pItm		= _root;
    if (row<=0 || row>=pItm->children.count()) {
        Wrn << "Cannot move row (" << row << ") or parent (" << parent.row() << "," << parent.column() << ") up!";
        return false;
    }
    quint32						dstRow		= row-1;
    if (pItm->isRoot()) {
        Dbg << "Moving up recognizer at" << row << " for parent" << parent.row() << "," << parent.column() << ".";
        beginMoveRows						(parent, row, row, parent, dstRow);
        _recognizers.swap					(row, dstRow);
        endMoveRows							();
        return true;
    }
    else if (pItm->isRecognizer()) {
        Dbg << "Moving up filter at" << row << " for parent" << parent.row() << "," << parent.column() << ".";
        beginMoveRows						(parent, row, row, parent, dstRow);
        RE::T::ZoneRecognizerData	&zrd		= _recognizers[pItm->row()];
        zrd.filterStack.swap				(row, dstRow);
        endMoveRows							();
        return true;
    }
    Wrn << "Cannot move row (" << row << ") or parent (" << parent.row() << "," << parent.column() << ") up!";
    return false;
}
bool REA::ZoneRecognizersModel::moveDown (int row, QModelIndex const &parent) {
    TreeItem					*pItm		= (TreeItem*)parent.internalPointer();
    if (!pItm)					pItm		= _root;
    if (row<0 || row>=pItm->children.count()-1) {
        Wrn									<< "Cannot move row (" << row << ") or parent (" << parent.row() << "," << parent.column() << ") down!";
        return false;
    }
    quint32						dstRow		= row+1;
    if (pItm->isRoot()) {
        Dbg									<< "Moving down recognizer at" << row << " for parent" << parent.row() << "," << parent.column() << ".";
        beginMoveRows						(parent, row, row, parent, dstRow);
        _recognizers.swap					(dstRow, row);
        endMoveRows							();
        return true;
    }
    else if (pItm->isRecognizer()) {
        Dbg									<< "Moving down filter at" << row << " for parent" << parent.row() << "," << parent.column() << ".";
        beginMoveRows						(parent, dstRow, dstRow, parent, row);
        RE::T::ZoneRecognizerData	&zrd	= _recognizers[pItm->row()];
        zrd.filterStack.swap				(row, dstRow);
        endMoveRows							();
        return true;
    }
    Wrn										<< "Cannot move row (" << row << ") or parent (" << parent.row() << "," << parent.column() << ") down!";
    return false;
}
