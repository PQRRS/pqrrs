#ifndef REA_ZoneFiltersModel_h
#define REA_ZoneFiltersModel_h

#include <QtCore/QAbstractItemModel>
#include <RELibrary/RETypes>

namespace REA {

class ZoneFiltersModel : public QAbstractItemModel {
Q_OBJECT
public:
    /**/                                ZoneFiltersModel                (QObject *p=0);
    RE::T::ZoneFilterData::List const&  filters                         () const;
    void                                setFilters                      (RE::T::ZoneFilterData::List const &filters);
    // Reimplemented methods.
    QModelIndex							index							(int row, int column, QModelIndex const &parent=QModelIndex()) const;
    QModelIndex							parent							(QModelIndex const &index) const;
    int									rowCount						(QModelIndex const &parent=QModelIndex()) const;
    int									columnCount						(QModelIndex const &parent=QModelIndex()) const;
    Qt::ItemFlags						flags							(const QModelIndex &index) const;
    bool                                insertRows                      (int position, int count, const QModelIndex &parent=QModelIndex());
    bool								removeRows						(int position, int count, const QModelIndex &parent=QModelIndex());
    QVariant							data							(QModelIndex const &index, int role) const;
    bool								setData							(QModelIndex const &index, QVariant const &value, int role=Qt::EditRole);

    bool								createFilter					(RE::T::ZoneFilterData const &zrf);
    bool								setParameter					(QModelIndex const &parent, QString const &name, QVariant const &value);
    bool                                removeParameter                 (QModelIndex const &parent, QString const &name);

    bool								moveUp							(int row);
    bool								moveDown						(int row);

private:
    RE::T::ZoneFilterData::List         _filters;
};

}
#endif

