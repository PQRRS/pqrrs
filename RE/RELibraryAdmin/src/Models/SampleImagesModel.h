#ifndef REA_SampleImagesModel_h
#define REA_SampleImagesModel_h

// Base class.
#include <QtSql/QSqlTableModel>
// Required stuff.
#include <RELibrary/RETypes>
#include <QtCore/QVariant>

namespace REA {
    class Core;
}

namespace REA {

class SampleImagesModel : public QSqlTableModel {
Q_OBJECT
public:
	// Columns indices helpers.
	enum { Key = 0, DocumentType, Filename, Training, Signature, Count };
	// Default constructor.
                        SampleImagesModel		(QObject *p, QSqlDatabase const &db);
	Qt::ItemFlags		flags					(const QModelIndex &i) const;
	void				setDocumentTypeId		(quint32 const &dtId);
	
	virtual bool		select					();
	virtual int			columnCount				(const QModelIndex &i) const;
	virtual QVariant	data					(const QModelIndex &i, int r=Qt::UserRole) const;
	bool				setData					(const QModelIndex &i, const QVariant &v, int r=Qt::EditRole);

private:
    QMutex              _mutex;
    Core                *_core;
};

}

#endif
