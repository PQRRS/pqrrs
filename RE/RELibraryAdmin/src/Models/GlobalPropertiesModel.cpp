#include "GlobalPropertiesModel.h"
// Required stuff.
#include "../Core.h"
#include <RELibrary/RETypes>
#include <QtCore/QDebug>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

REA::GlobalPropertiesModel::GlobalPropertiesModel (QObject *p, QSqlDatabase const &db)
: QSqlTableModel(p, db) {
    // Setup model table and edition strategy.
    setTable				("Properties");
    setEditStrategy			(QSqlTableModel::OnFieldChange);
    // Setup headers.
    setHeaderData			(Key,					Qt::Horizontal, tr("Key"));
    setHeaderData			(Value,					Qt::Horizontal, tr("Value"));
}

int REA::GlobalPropertiesModel::columnCount (const QModelIndex&) const {
    return Count;
}
QString REA::GlobalPropertiesModel::valueForKey (QString const &key) const {
    QSqlQuery		q;
    q.prepare				("SELECT value FROM Properties WHERE key=:key;");
    q.bindValue				(":key",	key);
    q.exec					();
    QSqlError	e           = q.lastError();
    if (e.type()!=QSqlError::NoError) {
        Crt                 << "Query" << q.lastQuery() << "failed with error" << e.text() << "!";
        return              QString();
    }
    return q.next() ? q.value(0).toString() : QString();
}
void REA::GlobalPropertiesModel::setValueForKey (QString const &key, QString const &value) {
    QSqlQuery		q;
    q.prepare				("INSERT OR REPLACE INTO Properties (key,value) VALUES (:key, :value);");
    q.bindValue				(":key",	key);
    q.bindValue				(":value",	value);
    if (!q.exec()) {
        QSqlError	e       = q.lastError();
        if (e.type()!=QSqlError::NoError) {
            Crt             << "Query" << q.lastQuery() << "failed with error" << e.text() << "!";
            return;
        }
    }
    select                  ();
}
