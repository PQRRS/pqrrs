#include "DocumentTypesModel.h"
// Required stuff.
#include "../REATypes.h"
#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlIndex>
#include <QtSql/QSqlError>
#include <QtGui/QFileIconProvider>

#define rootItem ((TreeItem*)_rootItem)
class TreeItem {
public:
    /**/                TreeItem        (QList<QVariant> const &d, TreeItem *p=0) : parent(p), data(d) { if (p) p->children << this; }
    /**/                ~TreeItem       ()                      { qDeleteAll(children); }
    int                 row             () const                { return parent ? parent->children.indexOf((TreeItem * const)this) : 0; }
    QList<TreeItem*>    children;
    TreeItem            *parent;
    QList<QVariant>     data;
};

REA::DocumentTypesModel::DocumentTypesModel (QObject *p, QSqlDatabase const &db)
: QAbstractItemModel(p), _db(&db), _rootItem(0) {
    // Setup headers.
    QList<QVariant>         headerTexts;
    headerTexts             << tr("Key")
                            << tr("Parent Key")
                            << tr("Name")
                            << tr("Code")
                            << tr("No ANN Training")
                            << tr("MICR RegExp")
                            << tr("Bad MICR Rejection")
                            << tr("Sub-Type Selection Script")
                            << tr("Sub-Type Selection Simulation Strings")
                            << tr("Rejection Tests Scripts")
                            << tr("Rejection Tests Simulation Strings")
                            << tr("Preprocessing Filters Data")
                            << tr("Recognition Data")
                            << tr("S");
    _rootItem               = new TreeItem(headerTexts);
    populate                (0, _rootItem);
}
REA::DocumentTypesModel::~DocumentTypesModel () {
    delete rootItem;
}

void REA::DocumentTypesModel::populate (quint32 pKey, void *pItem) {
    QString             sz                  = QString("SELECT "
                                                      "id,documentType_id,name,code,"
                                                      "noAnnTraining,"
                                                      "regExp,badMicrRejection,"
                                                      "subtypeSelectionScript,subtypeSelectionSimulationStrings,"
                                                      "rejectionTestsScript,rejectionTestsSimulationStrings,"
                                                      "preprocessingFiltersData,recognitionData"
                                                      " FROM DocumentTypes WHERE documentType_id = %1").arg(pKey);
    QSqlQuery           q                   (sz);
    QSqlError           e                   = q.lastError();
    if (e.type()!=QSqlError::NoError) {
        Crt						<< "Query" << q.lastQuery() << "failed with error" << e.text() << "!";
        return;
    }
    while (q.next()) {
        QList<QVariant> dt;
        for (quint32 i=0; i<Count; ++i)
            dt                              << q.value(i);
        populate                            (dt[Key].toUInt(), new TreeItem(dt, (TreeItem*)pItem));
    }
}
void REA::DocumentTypesModel::triggerUpdate (QModelIndex const &idx) {
    emit dataChanged(idx, idx);
}
int REA::DocumentTypesModel::sampleImagesCount (QString const &filter) {
    QString				sq;
    if (filter.length())
        sq									= "SELECT COUNT(id) FROM SampleImages WHERE "+filter+";";
    else
        sq									= "SELECT COUNT(id) FROM SampleImages WHERE 1;";
    QSqlQuery			q					(sq);
    return q.next() ? q.value(0).toInt() : 0;
}
int REA::DocumentTypesModel::trainableSampleImagesCount (QString const &filter) {
    QString				sq;
    if (filter.length())
        sq									= "SELECT COUNT(id) FROM SampleImages WHERE ("+filter+") AND trainable=1;";
    else
        sq									= "SELECT COUNT(id) FROM SampleImages WHERE trainable=1;";
    QSqlQuery			q					(sq);
    return q.next() ? q.value(0).toInt() : 0;
}

QVariant REA::DocumentTypesModel::headerData (int s, Qt::Orientation o, int r) const {
    return o==Qt::Horizontal && r==Qt::DisplayRole ? rootItem->data[s] : QVariant();
}
Qt::ItemFlags REA::DocumentTypesModel::flags (const QModelIndex &i) const {
    Qt::ItemFlags       f                   = QAbstractItemModel::flags(i) | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    if (i.column()>0 && i.column()<8)
        f                                   |= Qt::ItemIsEditable;
    return f;
}
int REA::DocumentTypesModel::columnCount (const QModelIndex&) const {
    return Count;
}
int REA::DocumentTypesModel::rowCount (const QModelIndex &i) const {
    if (i.column()>0)
        return 0;
    TreeItem *parentItem    = i.isValid() ? (TreeItem*)i.internalPointer() : rootItem;
    return parentItem->children.count();
}
QModelIndex REA::DocumentTypesModel::index (int r, int c, QModelIndex const &p) const {
    if (!hasIndex(r, c, p))
        return QModelIndex();
    TreeItem *parentItem    = p.isValid() ? (TreeItem*)p.internalPointer() : rootItem;
    TreeItem *childItem     = parentItem->children[r];
    return childItem ? createIndex(r, c, childItem) : QModelIndex();
}
QModelIndex REA::DocumentTypesModel::parent (QModelIndex const &c) const {
    if (!c.isValid())
        return QModelIndex();
    TreeItem *childItem     = (TreeItem*)c.internalPointer();
    TreeItem *parentItem    = childItem->parent;
    return parentItem==_rootItem ? QModelIndex() : createIndex(parentItem->row(), 0, parentItem);
}
bool REA::DocumentTypesModel::hasChildren (const QModelIndex &i) const {
    TreeItem *parentItem    = i.isValid() ? (TreeItem*)i.internalPointer() : rootItem;
    return parentItem->children.count();
}
QVariant REA::DocumentTypesModel::data (const QModelIndex &i, int r) const {
    TreeItem *item = (TreeItem*)i.internalPointer();
    if (!i.isValid() || (r!=Qt::DisplayRole && r!=Qt::EditRole && r!=Qt::ToolTipRole && r!=Qt::DecorationRole))
        return QVariant();
    else if (r==Qt::DecorationRole) {
        if (item->children.count())
            return QFileIconProvider().icon(QFileIconProvider::Folder);
        else
            return QIcon(":/Resources/Icons/book.gif");
    }
    else if (r==Qt::ToolTipRole) {
        quint32             dtId			= item->data[Key].toUInt();
        quint32             siCount			= trainableSampleImagesCount(QString("documentType_id=%1").arg(dtId));
        return tr("Item #%1 - Status:\r\n%2").arg(dtId).arg(siCount<50 ? tr("You may want to add a few sample images to this type to optimally train the ANN on it.") : tr("Sane."));
    }
    // Virtual "Sample Images Count" column.
    else if (i.column()==Status)
        return sampleImagesCount(QString("documentType_id=%1").arg(item->data[Key].toUInt()));
    else if (r==Qt::DisplayRole || r==Qt::EditRole)
        return item->data[i.column()];
    else
        return QVariant();
}
bool REA::DocumentTypesModel::setData (QModelIndex const &i, QVariant const &v, int) {
    TreeItem *item = (TreeItem*)i.internalPointer();
    item->data[i.column()]  = v;
    quint32         c       = i.column();
    QSqlQuery       q       (*_db);
    QString         sz      = QString("UPDATE DocumentTypes SET %2=? WHERE id=%1").arg(item->data[Key].toUInt());
    if (c==Name)
        q.prepare           (sz.arg("name"));
    else if (c==Code)
        q.prepare           (sz.arg("code"));
    else if (c==MicrRegExp)
        q.prepare           (sz.arg("regExp"));
    else if (c==NoAnnTraining)
        q.prepare           (sz.arg("noAnnTraining"));
    else if (c==BadMicrRejection)
        q.prepare           (sz.arg("badMicrRejection"));
    else if (c==SubtypeSelectionScript)
        q.prepare           (sz.arg("subtypeSelectionScript"));
    else if (c==SubtypeSelectionSimulationStrings)
        q.prepare           (sz.arg("subtypeSelectionSimulationStrings"));
    else if (c==RejectionTestsScript)
        q.prepare           (sz.arg("rejectionTestsScript"));
    else if (c==RejectionTestsSimulationStrings)
        q.prepare           (sz.arg("rejectionTestsSimulationStrings"));
    else if (c==PreprocessingFiltersData)
        q.prepare           (sz.arg("preprocessingFiltersData"));
    else if (c==RecognitionData)
        q.prepare           (sz.arg("recognitionData"));
    q.addBindValue          (v);
    if (!q.exec()) {
        Wrn                 << q.lastError().text();
        return              false;
    }
    return                  true;
}

bool REA::DocumentTypesModel::insertRow(int, QModelIndex const &p) {
    QString             sz                  = "INSERT INTO DocumentTypes ("
                                              "documentType_id,name,code,"
                                              "noAnnTraining,"
                                              "regExp,badMicrRejection,"
                                              "subtypeSelectionScript,subtypeSelectionSimulationStrings,"
                                              "rejectionTestsScript,rejectionTestsSimulationStrings,"
                                              "preprocessingFiltersData,recognitionData) "
                                              "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    // Prepare blank record.
    QVariantList        dt;
    dt                                      << 0
                                            << (quint32)(p.isValid() ? index(p.row(), Key, p.parent()).data().toUInt() : 0)
                                            << tr("New Document Type")
                                            << (quint32)1
                                            << false
                                            << ".*"
                                            << false
                                            << tr("// Subtype selection script.\r// This script should return a code matching a document subtype within this document type.\r")
                                            << QByteArray()
                                            << tr("// Rejection tests script.\r// Access to strings, acceptationReasons and rejectionReasons is permited.\r")
                                            << QStringList()
                                            << QByteArray()
                                            << QByteArray();
    // Prepare query.
    QSqlQuery           q                   (*_db);
    q.prepare                               (sz);
    q.addBindValue                          (dt[ParentKey]);
    q.addBindValue                          (dt[Name]);
    q.addBindValue                          (dt[Code]);
    q.addBindValue                          (dt[NoAnnTraining]);
    q.addBindValue                          (dt[MicrRegExp]);
    q.addBindValue                          (dt[BadMicrRejection]);
    q.addBindValue                          (dt[SubtypeSelectionScript]);
    q.addBindValue                          (dt[SubtypeSelectionSimulationStrings]);
    q.addBindValue                          (dt[RejectionTestsScript]);
    q.addBindValue                          (dt[RejectionTestsSimulationStrings]);
    q.addBindValue                          (dt[PreprocessingFiltersData]);
    q.addBindValue                          (dt[RecognitionData]);

    // Run SQL insert.
    if (!q.exec()) {
        QSqlError	e                       = q.lastError();
        if (e.type()!=QSqlError::NoError) {
            Crt                             << "Query" << q.lastQuery() << "failed with error" << e.text() << "!";
            return                          false;
        }
    }
    dt[Key]                                 = q.lastInsertId();
    beginInsertRows                         (p, rowCount(p), rowCount(p));
    new TreeItem                            (dt, p.isValid() ? (TreeItem*)p.internalPointer() : rootItem);
    endInsertRows                           ();
    return                                  true;
}
bool REA::DocumentTypesModel::removeRow (int r, const QModelIndex &p) {
    beginRemoveRows                 (p, r, r);
    TreeItem        *pItm           = p.isValid() ? (TreeItem*)p.internalPointer() : rootItem;
    QString         sz              = QString("DELETE FROM DocumentTypes WHERE id=%1").arg(pItm->children[r]->data[Key].toUInt());
    QSqlQuery       q               (sz, *_db);
    if (!q.exec()) {
        QSqlError	e                       = q.lastError();
        if (e.type()!=QSqlError::NoError) {
            Crt                             << "Query" << q.lastQuery() << "failed with error" << e.text() << "!";
            return                          false;
        }
    }
    delete                          pItm->children.takeAt(r);
    endRemoveRows                   ();
    return                          true;
}
