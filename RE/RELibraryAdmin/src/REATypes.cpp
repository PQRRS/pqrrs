#include "REATypes.h"
#include "../../Versioning/RELibraryAdminVersion.h"
// Required stuff.
#include <RELibrary/RETypes>
// Various stuff.
#include <QtCore/QSettings>

REA::SampleImageInfo::SampleImageInfo (quint32 dtId, QString const &fn, QString const &h, QString m, QSize sz, QByteArray s)
: documentTypeId(dtId), filename(fn), hash(h), micr(m), size(sz), signature(s) {
}

void REA::StaticInitializer () {
    RE::StaticInitializer   ();
    static bool	initDone	= false;
    if (initDone)			return;
    initDone				= true;
}
char const* REA::Version () {
    return const_cast<char*>(STRINGIZE(RELibraryAdminVersion));
}
