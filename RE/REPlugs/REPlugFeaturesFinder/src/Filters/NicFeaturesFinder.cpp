#include "NicFeaturesFinder.h"
#include "../Helper.h"
#include <RELibrary/Engine>
#include <RELibrary/Filters/TiltCorrector>
#include <QtCore/QDebug>

#include <stdio.h>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/features2d.hpp"

using namespace cv;

Point2f rotPoint(const Mat &R, const Point2f &p)
{
    Point2f rp;
    rp.x        = (float)(R.at<double>(0,0)*p.x + R.at<double>(0,1)*p.y + R.at<double>(0,2));
    rp.y        = (float)(R.at<double>(1,0)*p.x + R.at<double>(1,1)*p.y + R.at<double>(1,2));
    return rp;
}
Size rotatedImageBB (const Mat &R, const Rect &bb) {
    //Rotate the rectangle coordinates.
    vector<Point2f>     rp;
    rp.push_back                (rotPoint(R,Point2f(bb.x,bb.y)));
    rp.push_back                (rotPoint(R,Point2f(bb.x + bb.width,bb.y)));
    rp.push_back                (rotPoint(R,Point2f(bb.x + bb.width,bb.y+bb.height)));
    rp.push_back                (rotPoint(R,Point2f(bb.x,bb.y+bb.height)));
    //Find float bounding box r
    float               x       = rp[0].x;
    float               y       = rp[0].y;
    float               left    = x, right = x, up = y, down = y;
    for (int i=1; i<4; ++i) {
        x                       = rp[i].x;
        y                       = rp[i].y;
        if (left>x)             left = x;
        if (right<x)            right = x;
        if (up>y)               up = y;
        if (down<y)             down = y;
    }
    return Size((int)(right - left + 0.5), (int)(down - up + 0.5));
}

struct REPlugs::FeaturesFinder::NicFeaturesFinder::PrivData {
    SurfFeatureDetector         detector;
    SurfDescriptorExtractor     extractor;
    cv::Mat                     feats;
    Mat                         featsDescriptors;
    std::vector<cv::KeyPoint>   featsKeyPts;
    std::vector<Point2f>        featsCrnrs;
    QString                     paramsHash;
};

REPlugs::FeaturesFinder::NicFeaturesFinder::NicFeaturesFinder (QObject *p)
: RE::AbstractFilter(p), _data(0) {
    resetTemplateFilterStack        ();
    resetFilterStack                ();
    resetExtended                   ();
    resetUpright                    ();
    resetHessianThreshold           ();
    resetOctaves                    ();
    resetOctaveLayers               ();
    resetRectangularExtract         ();
    resetLeftBorder                 ();
    resetTopBorder                  ();
    resetRightBorder                ();
    resetBottomBorder               ();
    resetFraming                    ();

}
REPlugs::FeaturesFinder::NicFeaturesFinder::~NicFeaturesFinder () {
    if (_data) {
        delete  _data;
        _data   = 0;
    }
}
QImage REPlugs::FeaturesFinder::NicFeaturesFinder::doApply () {
    // Retrieve runtime context pointer.
    RE::RuntimeContext const
                    &ctx        = runtimeContext();
    // Filter source image with input preprocessing stack.
    QImage          qSrc        = ctx.engine->filteredImage(_filterStack, ctx.name, source(), parallelizedCoresCount());
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter requires a grayscale image as the result of it's input filter stack, conversion will merge channels!";
        qSrc                    = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }

    QString         hash            = QString("%1:%2:%3:%4:%5:%6")
                                        .arg(_extended)
                                        .arg(_upright)
                                        .arg(_hessianThreshold)
                                        .arg(_octaves)
                                        .arg(_octaveLayers)
                                        .arg(QString(RE::byteArrayFromStreamedValue(_templateFilterStack).toHex()));
    if (_data && _data->paramsHash!=hash) {
        delete                      _data;
        _data                       = 0;
    }
    if (!_data) {
        Wrn                         << "Recomputing template features...";
        _data                       = new PrivData();
        _data->paramsHash           = hash;
        // Load the features.
        Wrn                         << "  -> Loading template...";
        QImage          qFeats      (":/Resources/Images/Template_NIC_Fra.png");
        Wrn                         << "  -> Applying template filter stack...";
        qFeats                      = ctx.engine->filteredImage(_templateFilterStack, ctx.name, qFeats, parallelizedCoresCount());
        Wrn                         << "  -> Converting template to cv::Mat...";
        _data->feats                = REPlugs::FeaturesFinder::Helper::qImage2cvMat(&qFeats);
        Wrn                         << "  -> Detecting template features...";
        _data->detector             = SurfFeatureDetector(_hessianThreshold, _octaves, _octaveLayers, _extended, _upright);
        Wrn                         << "  -> Detecting template keypoints...";
        _data->detector.detect      (_data->feats, _data->featsKeyPts);

        Wrn                         << "  -> Extracting template corners...";
        _data->featsCrnrs           = std::vector<Point2f>(4);
        _data->featsCrnrs[0]        = cvPoint(0, 0);
        _data->featsCrnrs[1]        = cvPoint(_data->feats.cols, 0);
        _data->featsCrnrs[2]        = cvPoint(_data->feats.cols-1, _data->feats.rows-1);
        _data->featsCrnrs[3]        = cvPoint( 0, _data->feats.rows-1);
        Wrn                         << "  -> Computing template extraction descriptors...";
        _data->extractor.compute    (_data->feats, _data->featsKeyPts, _data->featsDescriptors);
    }

    // Convert source to cv::Mat format.
    cv::Mat         tmp         = REPlugs::FeaturesFinder::Helper::qImage2cvMat(&qSrc);

    std::vector<KeyPoint> srcKeyPts;
    _data->detector.detect              (tmp, srcKeyPts);

    Mat                 srcDescriptors;
    _data->extractor.compute            (tmp, srcKeyPts, srcDescriptors);

    std::vector<vector<DMatch> >
                        matches;
    FlannBasedMatcher   matcher;
    matcher.knnMatch                (_data->featsDescriptors, srcDescriptors, matches, 2);
    std::vector<DMatch > matchesOk;
    for (int i=0; i<min(srcDescriptors.rows-1, (int)matches.size()); ++i)
        if ((matches[i][0].distance<0.6*(matches[i][1].distance)) && ((int) matches[i].size()<=2 && (int) matches[i].size()>0))
            matchesOk.push_back(matches[i][0]);

    // Compute the homography between the two sets of points.
    std::vector<Point2f>    objPts, scnPts;
    Mat                     H;
    if (matchesOk.size()<4)
        return qSrc;

    for(size_t i=0; i<matchesOk.size(); i++) {
        //Get the keypoints from the good matches
        objPts.push_back        (_data->featsKeyPts[ matchesOk[i].queryIdx ].pt);
        scnPts.push_back        (srcKeyPts[ matchesOk[i].trainIdx ].pt);
    }
    // Finds homography between template corners and object corners.
    H                           = findHomography(objPts, scnPts, CV_RANSAC);
    // Translate original corners to scene corners.
    std::vector<Point2f>    scnCrnrs(4);
    perspectiveTransform        (_data->featsCrnrs, scnCrnrs, H);

    // Debugging?
    if (debuggingEnabled()) {
        Wrn                         << "Found" << _data->featsKeyPts.size() << "keypoints," << matchesOk.size() << "are matches.";
        Mat                 dstDbg;
        drawMatches                 (_data->feats, _data->featsKeyPts, tmp, srcKeyPts, matchesOk, dstDbg, Scalar::all(-1), Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
        line                        (dstDbg, scnCrnrs[0]+Point2f(_data->feats.cols, 0), scnCrnrs[1]+Point2f(_data->feats.cols, 0), Scalar(0, 255, 0), 4);
        line                        (dstDbg, scnCrnrs[1]+Point2f(_data->feats.cols, 0), scnCrnrs[2]+Point2f(_data->feats.cols, 0), Scalar(0, 255, 0), 4);
        line                        (dstDbg, scnCrnrs[2]+Point2f(_data->feats.cols, 0), scnCrnrs[3]+Point2f(_data->feats.cols, 0), Scalar(0, 255, 0), 4);
        line                        (dstDbg, scnCrnrs[3]+Point2f(_data->feats.cols, 0), scnCrnrs[0]+Point2f(_data->feats.cols, 0), Scalar(0, 255, 0), 4);
        setDebuggingImage           (REPlugs::FeaturesFinder::Helper::cvMat2qImage(&dstDbg));
    }

    // Finally transform the source image through the inverse homography matrix.
    qSrc                        = source();
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter requires a grayscale image as the result of it's input filter stack, conversion will merge channels!";
        qSrc                     = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }
    tmp                         = REPlugs::FeaturesFinder::Helper::qImage2cvMat(&qSrc);

    // User requested a rectangular extract.
    if (_rectangularExtract) {
        // Get new bounds.
        Rect                bounds  = boundingRect(scnCrnrs);
        // Contain bounds to image but add a border around.
        qint32                  lb  = bounds.x<_leftBorder ? bounds.x : _leftBorder;
        qint32                  rb  = bounds.x+bounds.width+_rightBorder>=tmp.cols ? tmp.cols-(bounds.x+bounds.width+1) : _rightBorder;
        qint32                  tb  = bounds.y<_topBorder ? bounds.y : _topBorder;
        qint32                  bb  = bounds.y+bounds.height+_bottomBorder>=tmp.rows ? tmp.rows-(bounds.y+bounds.height+1) : _bottomBorder;
        bounds.x                    = min(tmp.cols-2, max(0, bounds.x - lb));
        bounds.y                    = min(tmp.rows-2, max(0, bounds.y - tb));
        bounds.width                = min(max(1, tmp.cols-bounds.x), bounds.width + lb + rb);
        bounds.height               = min(max(1, tmp.rows-bounds.y), bounds.height + tb + bb);
        // Crop image to bounds.
        Mat                 dst     = tmp(bounds);
        // Get rotation based on top line.
        float               rad     = atan2((float)(scnCrnrs[1].y-scnCrnrs[0].y), (float)(scnCrnrs[1].x-scnCrnrs[0].x));
        // Get destination center.
        Point2f             cx      ((float)dst.cols/2.0, (float)dst.rows/2.0);
        Mat                 rot     = getRotationMatrix2D(cx, 180.0*rad/M_PI, 1);
        Mat                 rotI;
        Size                rs      = rotatedImageBB(rot, bounds);
        rotI.create                 (rs, tmp.type());
        rot.at<double>(0,2)         -= cx.x-rotI.cols/2.0;
        rot.at<double>(1,2)         -= cx.y-rotI.rows/2.0;
        warpAffine                  (dst, dst, rot, rotI.size(), INTER_LINEAR, BORDER_CONSTANT, Scalar(_framing.red(), _framing.green(), _framing.blue()));
        tmp                         = dst;
    }
    else
        warpPerspective             (tmp, tmp, H.inv(), _data->feats.size(), INTER_LINEAR, BORDER_REPLICATE);

    return REPlugs::FeaturesFinder::Helper::cvMat2qImage(&tmp);
}
QSize REPlugs::FeaturesFinder::NicFeaturesFinder::minimumImageSize () const {
    return QSize(3, 3);
}
