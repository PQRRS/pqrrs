#ifndef REPlug_FeaturesFinder_NicFeaturesFinder_h
#define REPlug_FeaturesFinder_NicFeaturesFinder_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Required stuff.
#include <QtGui/QColor>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace FeaturesFinder {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class NicFeaturesFinder : public RE::AbstractFilter {
private:
    struct PrivData;

public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(RE::T::ZoneFilterData::List  templateFilterStack READ templateFilterStack    WRITE setTemplateFilterStack    RESET resetTemplateFilterStack)
    Q_PROPERTY(RE::T::ZoneFilterData::List  filterStack         READ filterStack            WRITE setFilterStack            RESET resetFilterStack)
    Q_PROPERTY(bool                         extended            READ extended               WRITE setExtended               RESET resetExtended)
    Q_PROPERTY(bool                         upright             READ upright                WRITE setUpright                RESET resetUpright)
    Q_PROPERTY(quint32                      hessianThreshold    READ hessianThreshold       WRITE setHessianThreshold       RESET resetHessianThreshold)
    Q_PROPERTY(quint32                      octaves             READ octaves                WRITE setOctaves                RESET resetOctaves)
    Q_PROPERTY(quint32                      octaveLayers        READ octaveLayers           WRITE setOctaveLayers           RESET resetOctaveLayers)
    //! @brief Extract From a Rectangle rather than Homography.
    Q_PROPERTY(bool                         rectangularExtract  READ rectangularExtract     WRITE setRectangularExtract     RESET resetRectangularExtract)
    Q_PROPERTY(qint32                       leftBorder          READ leftBorder             WRITE setLeftBorder             RESET resetLeftBorder)
    Q_PROPERTY(qint32                       topBorder           READ topBorder              WRITE setTopBorder              RESET resetTopBorder)
    Q_PROPERTY(qint32                       rightBorder         READ rightBorder            WRITE setRightBorder            RESET resetRightBorder)
    Q_PROPERTY(qint32                       bottomBorder        READ bottomBorder           WRITE setBottomBorder           RESET resetBottomBorder)
    //! @brief Framing Color Property for Rectangular Extract.
    Q_PROPERTY(QColor                       framing             READ framing                WRITE setFraming                RESET resetFraming)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				NicFeaturesFinder          (QObject *p=0);
    /**/                    ~NicFeaturesFinder          ();
    // Accessors.
    RE::T::ZoneFilterData::List const&
                            templateFilterStack         () const                                { return _templateFilterStack; }
    void                    setTemplateFilterStack      (RE::T::ZoneFilterData::List const &v)  { _templateFilterStack = v; }
    void                    resetTemplateFilterStack    ()                                      { _templateFilterStack = RE::T::ZoneFilterData::List(); }
    RE::T::ZoneFilterData::List const&
                            filterStack                 () const                                { return _filterStack; }
    void                    setFilterStack              (RE::T::ZoneFilterData::List const &v)  { _filterStack = v; }
    void                    resetFilterStack            ()                                      { _filterStack = RE::T::ZoneFilterData::List(); }

    bool const&             extended                    () const                                { return _extended; }
    void                    setExtended                 (bool const &v)                         { _extended = v; }
    void                    resetExtended               ()                                      { _extended = true; }
    bool const&             upright                     () const                                { return _upright; }
    void                    setUpright                  (bool const &v)                         { _upright = v; }
    void                    resetUpright                ()                                      { _upright = false; }
    quint32 const&          hessianThreshold            () const                                { return _hessianThreshold; }
    void                    setHessianThreshold         (quint32 const &v)                      { _hessianThreshold = v; }
    void                    resetHessianThreshold       ()                                      { _hessianThreshold = 50; }
    quint32 const&          octaves                     () const                                { return _octaves; }
    void                    setOctaves                  (quint32 const &v)                      { _octaves = v; }
    void                    resetOctaves                ()                                      { _octaves = 4; }
    quint32 const&          octaveLayers                () const                                { return _octaveLayers; }
    void                    setOctaveLayers             (quint32 const &v)                      { _octaveLayers = v; }
    void                    resetOctaveLayers           ()                                      { _octaveLayers = 8; }
    bool const&             rectangularExtract          () const                                { return _rectangularExtract; }
    void                    setRectangularExtract       (bool const &v)                         { _rectangularExtract = v; }
    void                    resetRectangularExtract     ()                                      { _rectangularExtract = false; }
    qint32 const&           leftBorder                  () const                                { return _leftBorder; }
    void                    setLeftBorder               (qint32 const &v)                       { _leftBorder = v; }
    void                    resetLeftBorder             ()                                      { _leftBorder = 0; }
    qint32 const&           topBorder                   () const                                { return _topBorder; }
    void                    setTopBorder                (qint32 const &v)                       { _topBorder = v; }
    void                    resetTopBorder              ()                                      { _topBorder = 0; }
    qint32 const&           rightBorder                 () const                                { return _rightBorder; }
    void                    setRightBorder              (qint32 const &v)                       { _rightBorder = v; }
    void                    resetRightBorder            ()                                      { _rightBorder = 0; }
    qint32 const&           bottomBorder                () const                                { return _bottomBorder; }
    void                    setBottomBorder             (qint32 const &v)                       { _bottomBorder = v; }
    void                    resetBottomBorder           ()                                      { _bottomBorder = 0; }
    QColor const&           framing                     () const                                { return _framing; }
    void                    setFraming                  (QColor const &v)                       { _framing = v; }
    void                    resetFraming                ()                                      { _framing = QColor::fromRgb(255, 255, 255); }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    PrivData                *_data;

    RE::T::ZoneFilterData::List
                            _templateFilterStack,
                            _filterStack;
    bool                    _extended,
                            _upright;
    quint32                 _hessianThreshold,
                            _octaves,
                            _octaveLayers;
    bool                    _rectangularExtract;
    qint32                  _leftBorder;
    qint32                  _topBorder;
    qint32                  _rightBorder;
    qint32                  _bottomBorder;
    QColor                  _framing;
};

}
}

#endif
