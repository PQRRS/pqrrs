#include "Helper.h"
#include "../../../../Versioning/REPlugFeaturesFinderVersion.h"
#include <RELibrary/Engine>
#include "Filters/NicFeaturesFinder.h"
// Required stuff.
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/qplugin.h>

REPlugs::FeaturesFinder::Helper::Helper (QObject *p)
: RE::AbstractPlugin(p) {
    // Enums.
    #define EnumFullRegistration(name)	qRegisterMetaType<name>(#name); qRegisterMetaTypeStreamOperators<int>(#name)
    EnumFullRegistration	(REPlugs::FeaturesFinder::E::First);
    EnumFullRegistration	(REPlugs::FeaturesFinder::E::Last);
    #undef EnumFullRegistration
}

RE::PluginBaseInfos REPlugs::FeaturesFinder::Helper::baseInfos () const {
    QMetaObject const           *mo = &REPlugs::FeaturesFinder::E::staticMetaObject;
    RE::PluginBaseInfos         bi;
    bi.name                         = "REPlugFeaturesFinder";
    bi.version                      = STRINGIZE(REPlugFeaturesFinderVersion);
    bi.description                  = tr("Helps pre-processing pictures from the Fuji scanner.");
    bi.enumeratorRanges[mo]         << RE::MetaTypesRange(qMetaTypeId<REPlugs::FeaturesFinder::E::First>(), qMetaTypeId<REPlugs::FeaturesFinder::E::Last>());
    return                          bi;
}
// Plugin functionality.
RE::PluginMetaObjectList REPlugs::FeaturesFinder::Helper::filters () const {
    RE::PluginMetaObjectList    lst;
    lst                             << &REPlugs::FeaturesFinder::NicFeaturesFinder::staticMetaObject;
    return                          lst;
}
RE::PluginMetaObjectList REPlugs::FeaturesFinder::Helper::analyzers () const {
    return                          RE::PluginMetaObjectList();
}
RE::PluginMetaObjectList REPlugs::FeaturesFinder::Helper::recognizers () const {
    return                          RE::PluginMetaObjectList();
}

cv::Mat const REPlugs::FeaturesFinder::Helper::qImage2cvMat (QImage const *qImg) {
    int             cvType;
    switch (qImg->format()) {
    case QImage::Format_Indexed8:
        Dbg                 << "From QImage::Format_Indexed8 to cv::Mat::CV_8UC1.";
        cvType              = CV_8UC1;
        break;
    case QImage::Format_RGB888:
        Dbg                 << "From QImage::Format_RGB888 to cv::Mat::CV_8UC3.";
        cvType              = CV_8UC3;
        break;
    case QImage::Format_RGB32:
        Dbg                 << "From QImage::Format_RGB32 to cv::Mat::CV_8UC4.";
        cvType              = CV_8UC4;
        break;
    case QImage::Format_ARGB32:
        Dbg                 << "From QImage::Format_ARGB32 to cv::Mat::CV_8UC4.";
        cvType              = CV_8UC4;
        break;
    default:
        Wrn                 << "QImage could not be converted to Mat.";
        return              cv::Mat();
    }
    cv::Mat         ret     = cv::Mat(qImg->height(), qImg->width(), cvType, (void*)qImg->bits(), qImg->bytesPerLine()).clone();
    if (cvType==CV_8UC4) {
        Wrn                 << "Removing alpha plane...";
        cv::cvtColor        (ret, ret, CV_RGBA2RGB);
    }
    return                  ret;
}
QImage REPlugs::FeaturesFinder::Helper::cvMat2qImage (cv::Mat const *cvMat) {
    uchar const     *buff;
    QImage          img;
    switch (cvMat->type()) {
    case CV_8UC1:
        Dbg                 << "From cv::Mat::CV_8UC1 to QImage::Format_Indexed8.";
        buff                = (const uchar*)cvMat->data;
        img                 = QImage(buff, cvMat->cols, (int)cvMat->rows, (int)cvMat->step, QImage::Format_Indexed8);
        img.setColorTable   (RE::Engine::grayscalePalette());
        return              img;
    case CV_8UC3:
        Dbg                 << "From cv::Mat::CV_8UC3 to QImage::Format_RGB888.";
        buff                = (uchar const*)cvMat->data;
        return              QImage(buff, cvMat->cols, (int)cvMat->rows, (int)cvMat->step, QImage::Format_RGB888).convertToFormat(QImage::Format_RGB32).rgbSwapped();
    case CV_8UC4:
        Dbg                 << "From cv::Mat::CV_8UC4 to QImage::Format_ARGB32.";
        buff                = (uchar const*)cvMat->data;
        return              QImage(buff, cvMat->cols, (int)cvMat->rows, (int)cvMat->step, QImage::Format_ARGB32).convertToFormat(QImage::Format_RGB32).rgbSwapped();
    default:
        Dbg                 << "Mat could not be converted to QImage.";
        return              QImage();
    }
}

Q_EXPORT_PLUGIN2(REPlugFeaturesFinder, REPlugs::FeaturesFinder::Helper)
