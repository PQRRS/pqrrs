#ifndef REPlugs_FeaturesFinder_Helper_h
#define REPlugs_FeaturesFinder_Helper_h

#include <RELibrary/RETypes>
#include <RELibrary/Plugins/AbstractPlugin>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace REPlugs {

namespace FeaturesFinder {

// ------------------------------------				Enums			------------------------------------ //
class E {
Q_GADGET
public:
Q_ENUMS	(First Last)
    //! @brief First Enumeration Value Marker. Shouldn't be used directly.
    enum		First				{ FirstValue };
    //! @brief Last Enumeration Value Marker. Shouldn't be used directly.
    enum		Last				{ LastValue };
    RegisterEnumStream	(REPlugs::FeaturesFinder::E::First)
    RegisterEnumStream	(REPlugs::FeaturesFinder::E::Last)
};

class Helper : public RE::AbstractPlugin {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractPlugin)

public:
    Q_INVOKABLE                 Helper                          (QObject *p=0);
    // Plugin information strings.
    RE::PluginBaseInfos			baseInfos                       () const;
	// Plugin functionality.
    RE::PluginMetaObjectList	filters                         () const;
    RE::PluginMetaObjectList	analyzers                       () const;
    RE::PluginMetaObjectList	recognizers                     () const;

    //! @brief QImage to OpenCV::Mat Conversion.
    static cv::Mat const        qImage2cvMat                    (QImage const *qImg);
    //! @brief OpenCV::Mat to QImage Conversion.
    static QImage               cvMat2qImage                    (cv::Mat const *cvMat);
};

}

}

// Enums.
Q_DECLARE_TYPEINFO(REPlugs::FeaturesFinder::E::First, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(REPlugs::FeaturesFinder::E::First)
Q_DECLARE_TYPEINFO(REPlugs::FeaturesFinder::E::Last, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(REPlugs::FeaturesFinder::E::Last)

#endif
