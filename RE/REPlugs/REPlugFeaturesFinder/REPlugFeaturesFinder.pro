# Base settings.
include         (../../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   REPlugFeaturesFinder
TEMPLATE        =   lib
CONFIG          +=  plugin
QT              +=  gui

include         (../../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                        install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/../../lib/libRELibrary.dylib" \
                                "$${DESTDIR}/lib$${TARGET}.dylib"
}

# OpenCV includes and libs.
unix|linux: {
    INCLUDEPATH     +=  /opt/PQRRS/include
    LIBS            +=  -L/opt/PQRRS/lib -lopencv_core -lopencv_imgproc -lopencv_calib3d -lopencv_features2d -lopencv_flann -lopencv_nonfree
}
win32 {
    INCLUDEPATH     +=  $${BASEDIR}/Dependencies/opencv-2.4.9/include
    # Based on the configuration name, either link against debug or release OpenCV libraries.
    CONFIG(debug, debug|release) {
        LIBS        +=  -L"$${BASEDIR}/Dependencies/opencv-2.4.9/$$ARCH/vc11/lib" -lopencv_core248d -lopencv_imgproc248d -lopencv_calib3d248d -lopencv_features2d248d -lopencv_flann248d -lopencv_nonfree248d
    } else {
        LIBS        +=  -L"$${BASEDIR}/Dependencies/opencv-2.4.9/$$ARCH/vc11/lib" -lopencv_core248 -lopencv_imgproc248 -lopencv_calib3d248 -lopencv_features2d248 -lopencv_flann248 -lopencv_nonfree248
    }
}

target.path         =   $${INSTALLDIR}/plugins/RELibrary
INSTALLS            +=  target

VERSION             =   $${REPlugFeaturesFinderVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

RESOURCES           +=  src/REPlugFeaturesFinder.qrc

HEADERS             +=  src/Helper.h \
                        src/Filters/NicFeaturesFinder.h

SOURCES             +=  src/Helper.cpp \
                        src/Filters/NicFeaturesFinder.cpp

