#ifndef _A2iARC_INCLUDED
#define _A2iARC_INCLUDED
#ifdef __cplusplus
extern "C" {
#endif
/**
*
* A2iA Recognition Library Interface.
*
* Copyright A2iA 2001. All rights reserved.
*   
*/
 
// Standard includes
#include <stddef.h>
#include <stdio.h>
#include <limits.h>

// Private/Public DLL linkage
#ifdef _DOXYGEN
   #define A2iARC_DLL 
   #define A2IA_FUNC_DCL 
   #define A2iARC_ThreadProof
#else
#ifdef _MSC_VER
   #ifdef A2iARC_Private
   #define A2iARC_DLL __declspec( dllexport )
   #elif A2iARC_Devlp
   #define A2iARC_DLL extern
   #else
   #define A2iARC_DLL __declspec( dllimport )
   #endif

   // Standard call for functions.
   #define A2IA_FUNC_DCL __stdcall

#elif defined __linux__ || defined __CYGWIN__
   #define A2iARC_DLL 

   // Standard call for functions.
   #define A2IA_FUNC_DCL 
#endif


// Thread definitions : for thread proof implementations
#define A2iARC_ThreadProof __declspec(thread) 

#endif // _DOXYGEN

// Platform specification
#define A2iARC_Platform_PC_NT       1

#define A2iARC_Platform             A2iARC_Platform_PC_NT

// Return codes
#define A2iARC_Ok                   0     // OK return code : the rest will be defined in Err enum
#define A2iARC_OK                   0
#define A2iARC_Err_OK               0     // This is the code in the documentation.

// Identifier type
typedef unsigned long A2iARC_Id ;
#define A2iARC_Id_MAX ULONG_MAX


// General Constants
#define A2iARC_ConnectUnit_Msec     500      // Default timeout for waiting after a connection to a process unit
#define A2iARC_Name_Size            32       // Size of a document identifier
#define A2iARC_ProcessTimeout_Msec  600000   // Default timeout (10 minutes) for processing a document
#define A2iARC_SleepWaitResult_Msec 10      // Default sleep time between two result presence checks

// General Constants, France
#define A2iARC_CMC7_LEN             31       // Length of CMC7 code (french magnetic field)

/**
* Identity.
*
* Retrieve the product name, version and release.
*
* @return This function returns a formatted string containing this information
*/
A2iARC_DLL const char * A2IA_FUNC_DCL A2iARC_GetId() ;

/**
* Global initialisation.
* Initialize the complete DLL
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_Init(
   const char *   paramPath   ///< Root directory for ICR parameters
   ) ;


/**
* Error handling.
* Get last error.
* @return The label for the last returned error code
*/
A2iARC_DLL const char * A2IA_FUNC_DCL A2iARC_GetLastError () ;



/// Some country specific constants
#define A2iARC_CMC7_LEN	31

/// Special constant for giving error: API not initialized.
#define A2iARC_Err_APINotInitialized      65535

/// Default value for fields.
#define A2iARC_NotDefined 0

// Making sure data alignment is correct.
#pragma pack(push,8)

/// Input/Output request data structures

#define A2iARCP_CRC 1684695313
#define A2iARC_ProductBuild "1224"
#define A2iARC_ProductName "A2iA CheckReader"
#define A2iARC_ProductRelease "1"
#define A2iARC_ProductVersion "5.5"
 // Internal constants
#define A2iARCP_VisibleType_Buffer 0
#define A2iARCP_VisibleType_Byte 1
#define A2iARCP_VisibleType_Char 2
#define A2iARCP_VisibleType_Dir 3
#define A2iARCP_VisibleType_File 4
#define A2iARCP_VisibleType_SaveFile 5
#define A2iARCP_VisibleType_Float 6
#define A2iARCP_VisibleType_Int 7
#define A2iARCP_VisibleType_String 8
#define A2iARCP_VisibleType_UserId 9
#define A2iARCP_VisibleType_Unsigned 10
#define A2iARCP_VisibleType_Boolean 11
#define A2iARCP_VisibleType_Country 12
#define A2iARCP_VisibleType_PersistentId 13
#define A2iARCP_VisibleType_PrivatePersistentId 14
#define A2iARCP_VisibleType_Err 15
#define A2iARCP_VisibleType_ImageSourceType 16
#define A2iARCP_VisibleType_InputFormat 17
#define A2iARCP_VisibleType_TransportModel 18
#define A2iARCP_VisibleType_ImageOrientation 19
#define A2iARCP_VisibleType_Image 20
#define A2iARCP_VisibleType_ReverseValue 21
#define A2iARCP_VisibleType_IQAOptimisation 22
#define A2iARCP_VisibleType_ImageQuality_UndersizeImage 23
#define A2iARCP_VisibleType_ImageQuality_FoldedOrTornDocumentCorners 24
#define A2iARCP_VisibleType_ImageQuality_FoldedOrTornDocumentEdges 25
#define A2iARCP_VisibleType_ImageQuality_DocumentFramingError 26
#define A2iARCP_VisibleType_ImageQuality_DocumentSkew 27
#define A2iARCP_VisibleType_ImageQuality_OversizeImage 28
#define A2iARCP_VisibleType_ImageQuality_PiggybackDocument 29
#define A2iARCP_VisibleType_ImageQuality_ImageTooLight 30
#define A2iARCP_VisibleType_ImageQuality_ImageTooDark 31
#define A2iARCP_VisibleType_ImageQuality_HorizontalStreaks 32
#define A2iARCP_VisibleType_ImageQuality_BelowMinimumCompressedImageSize 33
#define A2iARCP_VisibleType_ImageQuality_AboveMaximumCompressedImageSize 34
#define A2iARCP_VisibleType_ImageQuality_SpotNoise 35
#define A2iARCP_VisibleType_ImageQuality_FrontRearImageDimensionMismatch 36
#define A2iARCP_VisibleType_ImageQuality_CarbonStrip 37
#define A2iARCP_VisibleType_ImageQuality_OutOfFocus 38
#define A2iARCP_VisibleType_StreakValues 39
#define A2iARCP_VisibleType_ImageQualityInput 40
#define A2iARCP_VisibleType_ImageQualityValues 41
#define A2iARCP_VisibleType_ImageQualityResults 42
#define A2iARCP_VisibleType_InvertedText 43
#define A2iARCP_VisibleType_Gender 44
#define A2iARCP_VisibleType_Currency 45
#define A2iARCP_VisibleType_InternalProcessing 46
#define A2iARCP_VisibleType_CleanStrip 47
#define A2iARCP_VisibleType_DataPersistence 48
#define A2iARCP_VisibleType_DataFormat 49
#define A2iARCP_VisibleType_OrientationCorrection 50
#define A2iARCP_VisibleType_OutputFormat 51
#define A2iARCP_VisibleType_ImageConversion 52
#define A2iARCP_VisibleType_ImagePreprocessing 53
#define A2iARCP_VisibleType_DocumentModel 54
#define A2iARCP_VisibleType_PostalVendor 55
#define A2iARCP_VisibleType_SQLServer 56
#define A2iARCP_VisibleType_PostalDictionary 57
#define A2iARCP_VisibleType_Unit 58
#define A2iARCP_VisibleType_Origin 59
#define A2iARCP_VisibleType_Box 60
#define A2iARCP_VisibleType_Orientation 61
#define A2iARCP_VisibleType_LocationRuleType 62
#define A2iARCP_VisibleType_LocationRule 63
#define A2iARCP_VisibleType_Location 64
#define A2iARCP_VisibleType_WriteType 65
#define A2iARCP_VisibleType_HandwritingStyle 66
#define A2iARCP_VisibleType_WritingPeriod 67
#define A2iARCP_VisibleType_BoxingType 68
#define A2iARCP_VisibleType_ImageQuality 69
#define A2iARCP_VisibleType_IdentityDocumentType 70
#define A2iARCP_VisibleType_CharactersEncoding 71
#define A2iARCP_VisibleType_CleanedImageResult 72
#define A2iARCP_VisibleType_AmountScore 73
#define A2iARCP_VisibleType_AmountProb 74
#define A2iARCP_VisibleType_CharactersScore 75
#define A2iARCP_VisibleType_CharactersProb 76
#define A2iARCP_VisibleType_CharactersResult 77
#define A2iARCP_VisibleType_DicoWord 78
#define A2iARCP_VisibleType_StringProb 79
#define A2iARCP_VisibleType_SingleWord 80
#define A2iARCP_VisibleType_WordProb 81
#define A2iARCP_VisibleType_WordScore 82
#define A2iARCP_VisibleType_WordResult 83
#define A2iARCP_VisibleType_Date 84
#define A2iARCP_VisibleType_DateScore 85
#define A2iARCP_VisibleType_DateProb 86
#define A2iARCP_VisibleType_DateResult 87
#define A2iARCP_VisibleType_Time 88
#define A2iARCP_VisibleType_TimeScore 89
#define A2iARCP_VisibleType_TimeProb 90
#define A2iARCP_VisibleType_TimeResult 91
#define A2iARCP_VisibleType_BooleanScore 92
#define A2iARCP_VisibleType_AddressLineType 93
#define A2iARCP_VisibleType_AddressLineScore 94
#define A2iARCP_VisibleType_Address 95
#define A2iARCP_VisibleType_FieldDecision 96
#define A2iARCP_VisibleType_SingleCharacterScore 97
#define A2iARCP_VisibleType_DetailedCharacterResults 98
#define A2iARCP_VisibleType_SingleWordScore 99
#define A2iARCP_VisibleType_DetailedWordResults 100
#define A2iARCP_VisibleType_CARResultLocation 101
#define A2iARCP_VisibleType_LARResultLocation 102
#define A2iARCP_VisibleType_PrintedOCRType 103
#define A2iARCP_VisibleType_DefineFontSize 104
#define A2iARCP_VisibleType_Optimisation 105
#define A2iARCP_VisibleType_CharacterFieldType 106
#define A2iARCP_VisibleType_VocabFormat 107
#define A2iARCP_VisibleType_VocabItem 108
#define A2iARCP_VisibleType_WordProcess 109
#define A2iARCP_VisibleType_Alphabet 110
#define A2iARCP_VisibleType_WordInput 111
#define A2iARCP_VisibleType_PossibleAnswers 112
#define A2iARCP_VisibleType_DateFormat 113
#define A2iARCP_VisibleType_MonthFormat 114
#define A2iARCP_VisibleType_YearFormat 115
#define A2iARCP_VisibleType_PayeeNameLines 116
#define A2iARCP_VisibleType_PrintedLARType 117
#define A2iARCP_VisibleType_AddressSemFieldType 118
#define A2iARCP_VisibleType_BarcodeType 119
#define A2iARCP_VisibleType_ControlType 120
#define A2iARCP_VisibleType_AddressGeoSemFieldInput 121
#define A2iARCP_VisibleType_GeometricFieldInput 122
#define A2iARCP_VisibleType_AddressGeoFieldInput 123
#define A2iARCP_VisibleType_AddressFieldContext 124
#define A2iARCP_VisibleType_AddressFieldResultRequest 125
#define A2iARCP_VisibleType_AddressBookEntry 126
#define A2iARCP_VisibleType_AddressBookFormat 127
#define A2iARCP_VisibleType_AddressBook 128
#define A2iARCP_VisibleType_CustomAddressInput 129
#define A2iARCP_VisibleType_AddressBookCell 130
#define A2iARCP_VisibleType_AddressGeoFieldOutput 131
#define A2iARCP_VisibleType_CustomAddressItemOutput 132
#define A2iARCP_VisibleType_CustomAddressScore 133
#define A2iARCP_VisibleType_CustomAddressResult 134
#define A2iARCP_VisibleType_CustomAddressOutput 135
#define A2iARCP_VisibleType_FieldType 136
#define A2iARCP_VisibleType_FieldInfo 137
#define A2iARCP_VisibleType_StringScoreLocation 138
#define A2iARCP_VisibleType_GenderScoreLocation 139
#define A2iARCP_VisibleType_CountryScoreLocation 140
#define A2iARCP_VisibleType_AmountScoreLocation 141
#define A2iARCP_VisibleType_DateScoreLocation 142
#define A2iARCP_VisibleType_FeatureInput 143
#define A2iARCP_VisibleType_RIBResult 144
#define A2iARCP_VisibleType_CheckBoxType 145
#define A2iARCP_VisibleType_CheckBoxSize 146
#define A2iARCP_VisibleType_CheckBoxSpacing 147
#define A2iARCP_VisibleType_CentPresence 148
#define A2iARCP_VisibleType_MultipleTicks 149
#define A2iARCP_VisibleType_SubValueType 150
#define A2iARCP_VisibleType_FieldInput 151
#define A2iARCP_VisibleType_CheckBoxLineResult 152
#define A2iARCP_VisibleType_FieldOutput 153
#define A2iARCP_VisibleType_ToRecognize 154
#define A2iARCP_VisibleType_CodelineFont 155
#define A2iARCP_VisibleType_Line1RegexFilters 156
#define A2iARCP_VisibleType_CheckFieldsInput 157
#define A2iARCP_VisibleType_LarCallPolicy 158
#define A2iARCP_VisibleType_US_Check_Kind 159
#define A2iARCP_VisibleType_CheckStockDetailedEnablingFlags 160
#define A2iARCP_VisibleType_CheckStockDescr 161
#define A2iARCP_VisibleType_CheckStockFieldResult 162
#define A2iARCP_VisibleType_CheckStockTestResult 163
#define A2iARCP_VisibleType_CheckInvalidityDetails 164
#define A2iARCP_VisibleType_CheckDetectDocumentTypeDetails 165
#define A2iARCP_VisibleType_EndorsementCleaning 166
#define A2iARCP_VisibleType_BillPolicy 167
#define A2iARCP_VisibleType_PaymentDocumentType 168
#define A2iARCP_VisibleType_CheckInput 169
#define A2iARCP_VisibleType_CheckBranch 170
#define A2iARCP_VisibleType_Line1FieldsResultLocation 171
#define A2iARCP_VisibleType_FieldsResultLocation 172
#define A2iARCP_VisibleType_FieldsCharacterResults 173
#define A2iARCP_VisibleType_CheckInvalidityCauses 174
#define A2iARCP_VisibleType_Line1Results 175
#define A2iARCP_VisibleType_CheckOutput 176
#define A2iARCP_VisibleType_OperatorType 177
#define A2iARCP_VisibleType_Operand 178
#define A2iARCP_VisibleType_OperatorInput 179
#define A2iARCP_VisibleType_OperatorOutput 180
#define A2iARCP_VisibleType_GuideMark 181
#define A2iARCP_VisibleType_CleanZoneType 182
#define A2iARCP_VisibleType_CleanZone 183
#define A2iARCP_VisibleType_CustomInput 184
#define A2iARCP_VisibleType_CustomOutput 185
#define A2iARCP_VisibleType_DongleQueryOutput 186
#define A2iARCP_VisibleType_DevInput 187
#define A2iARCP_VisibleType_DevOutput 188
#define A2iARCP_VisibleType_DepositTicketInput 189
#define A2iARCP_VisibleType_DepositTicketOutput 190
#define A2iARCP_VisibleType_DocumentType 191
#define A2iARCP_VisibleType_VerboseDetails 192
#define A2iARCP_VisibleType_MarkerType 193
#define A2iARCP_VisibleType_Marker 194
#define A2iARCP_VisibleType_PageProcess 195
#define A2iARCP_VisibleType_AutoDefineProcess 196
#define A2iARCP_VisibleType_SpecificDocumentSubType 197
#define A2iARCP_VisibleType_GiroInput 198
#define A2iARCP_VisibleType_RIBInput 199
#define A2iARCP_VisibleType_RIBAddressInput 200
#define A2iARCP_VisibleType_SpecificInput 201
#define A2iARCP_VisibleType_TransactionItem 202
#define A2iARCP_VisibleType_TransactionInput 203
#define A2iARCP_VisibleType_RequestDictionaryType 204
#define A2iARCP_VisibleType_RequestDictionary 205
#define A2iARCP_VisibleType_RequestParameters 206
#define A2iARCP_VisibleType_ClickCountHistoryUsage 207
#define A2iARCP_VisibleType_Input 208
#define A2iARCP_VisibleType_ClassificationResult 209
#define A2iARCP_VisibleType_ClassificationScore 210
#define A2iARCP_VisibleType_MarkerLocation 211
#define A2iARCP_VisibleType_DateResultLocation 212
#define A2iARCP_VisibleType_BankAccount 213
#define A2iARCP_VisibleType_AmountResult 214
#define A2iARCP_VisibleType_AmountResultLocation 215
#define A2iARCP_VisibleType_GiroOutput 216
#define A2iARCP_VisibleType_SpecificOutput 217
#define A2iARCP_VisibleType_TransactionOutput 218
#define A2iARCP_VisibleType_ClassificationResults 219
#define A2iARCP_VisibleType_Output 220
#define A2iARCP_VisibleType_MICR 221
#define A2iARCP_VisibleType_KeyType 222
#define A2iARCP_VisibleType_KeyDefinition 223
#define A2iARCP_VisibleType_PersistentVocabularyType 224
#define A2iARCP_VisibleType_PersistentVocabulary 225
#define A2iARCP_VisibleType_ModelDefinition 226
#define A2iARCP_VisibleType_DocumentClassifierParameters 227
#define A2iARCP_VisibleType_DocumentClassifierType 228
#define A2iARCP_VisibleType_DocumentClassifier 229
#define A2iARCP_VisibleType_SpecificPassport 230
#define A2iARCP_VisibleType_PersistentDataType 231
#define A2iARCP_VisibleType_ModelImageDefinition 232
#define A2iARCP_VisibleType_SimpleModelDefinition 233
#define A2iARCP_VisibleType_PersistentResult 234
#define A2iARCP_VisibleType_DongleServer 235
#define A2iARCP_VisibleType_PriorityType 236
#define A2iARCP_VisibleType_CPU 237
#define A2iARCP_VisibleType_Channel 238
#define A2iARCP_VisibleType_PersistentData 239
#define A2iARCP_VisibleType_ActionType 240
#define A2iARCP_VisibleType_PersistentDataAction 241
#define A2iARCP_VisibleType_GlobalValues 242
#define A2iARCP_VisibleType_MaskTable 243
#define A2iARCP_VisibleType_MaskKey 244
#define A2iARCP_VisibleType_CPUStatus 245
#define A2iARCP_VisibleType_ChannelStatus 246
#define A2iARCP_VisibleType_RequestType 247
#define A2iARCP_VisibleType_TranscriptionSource 248
#define A2iARCP_VisibleType_TrainingAlgo 249
#define A2iARCP_VisibleType_ClassifyAlgo 250
#define A2iARCP_VisibleType_RequestInput 251
#define A2iARCP_VisibleType_PersistentDataTrainResult 252
#define A2iARCP_VisibleType_RequestOutput 253
#define A2iARCP_VisibleType_MessageType 254
#define A2iARCP_VisibleType_NodeVisibility 255
#define A2iARCP_VisibleType_MessageMapInitializeParam 256
#define A2iARCP_VisibleType_MessageMap 257

/**
* Buffer
*/
typedef struct
{
   unsigned char* pTab;
   unsigned long cTab;
} A2iARC_Buffer ;


/**
* Basic type for raw data
*/
typedef unsigned char A2iARC_Byte ;

/**
* Basic type for characters
*/
typedef char A2iARC_Char ;

/**
* Basic type for directory name
*/
typedef char* A2iARC_Dir ;

/**
* Basic type for file name
*/
typedef char* A2iARC_File ;

/**
* Basic type for save file name
*/
typedef char* A2iARC_SaveFile ;

/**
* Basic type for floating point values
*/
typedef float A2iARC_Float ;

/**
* Basic type for integers
*/
typedef long A2iARC_Int ;

/**
* Null terminated strings
*/
typedef char* A2iARC_String ;

/**
* Null terminated strings for user defined IDs
*/
typedef char* A2iARC_UserId ;

/**
* Basic type for unsigned integers
*/
typedef unsigned long A2iARC_Unsigned ;

/**
* Persistent Id
*/
typedef char* A2iARC_PersistentId ;

/**
* Private Persistent Id
*/
typedef char* A2iARC_PrivatePersistentId ;

typedef unsigned long A2iARC_Enum;

/**
* Boolean type
*/
typedef A2iARC_Enum A2iARC_Boolean ;
#define _A2iARC_Boolean_Count 2
#define A2iARC_Boolean_Yes 1UL   ///< 
#define A2iARC_Boolean_No 2UL   ///< 

/**
* Nationality specification
*/
typedef A2iARC_Enum A2iARC_Country ;
#define _A2iARC_Country_Count 47
#define A2iARC_Country_US 1UL   ///< United States
#define A2iARC_Country_France 2UL   ///< France
#define A2iARC_Country_UK 3UL   ///< United Kingdom
#define A2iARC_Country_Europe 4UL   ///< Europe
#define A2iARC_Country_HK 5UL   ///< Hong Kong
#define A2iARC_Country_AU 6UL   ///< Australia
#define A2iARC_Country_IE 7UL   ///< Ireland
#define A2iARC_Country_CA 8UL   ///< Canada
#define A2iARC_Country_BR 9UL   ///< Brazil
#define A2iARC_Country_PT 10UL   ///< Portugal
#define A2iARC_Country_MX 11UL   ///< Mexico
#define A2iARC_Country_NL 12UL   ///< Netherland
#define A2iARC_Country_TH 13UL   ///< Thailand
#define A2iARC_Country_DE 14UL   ///< Germany
#define A2iARC_Country_NC 15UL   ///< New Caledonia
#define A2iARC_Country_CL 16UL   ///< Chile
#define A2iARC_Country_BE 17UL   ///< Belgium
#define A2iARC_Country_IT 18UL   ///< Italy
#define A2iARC_Country_SG 19UL   ///< Singapore
#define A2iARC_Country_MY 20UL   ///< Malaysia
#define A2iARC_Country_IN 21UL   ///< India
#define A2iARC_Country_ZA 22UL   ///< South Africa
#define A2iARC_Country_Generic 23UL   ///< Generic
#define A2iARC_Country_SP 24UL   ///< Spain
#define A2iARC_Country_CO 25UL   ///< Columbia
#define A2iARC_Country_DO 26UL   ///< Dominican Republic
#define A2iARC_Country_GR 27UL   ///< Greece
#define A2iARC_Country_KE 28UL   ///< Kenya
#define A2iARC_Country_AT 29UL   ///< Austria
#define A2iARC_Country_DK 30UL   ///< Denmark
#define A2iARC_Country_LU 31UL   ///< Luxemburg
#define A2iARC_Country_RU 32UL   ///< Russian Federation
#define A2iARC_Country_SE 33UL   ///< Sweden
#define A2iARC_Country_PL 34UL   ///< Poland
#define A2iARC_Country_AD 35UL   ///< Andorra
#define A2iARC_Country_FI 36UL   ///< Finland
#define A2iARC_Country_RO 37UL   ///< Romania
#define A2iARC_Country_BG 38UL   ///< Bulgaria
#define A2iARC_Country_CH 39UL   ///< Switzerland
#define A2iARC_Country_CY 40UL   ///< Cyprus
#define A2iARC_Country_LT 41UL   ///< Lithuania
#define A2iARC_Country_CZ 42UL   ///< Czech Republic
#define A2iARC_Country_SI 43UL   ///< Slovenia
#define A2iARC_Country_SK 44UL   ///< Slovakia
#define A2iARC_Country_LV 45UL   ///< Latvia
#define A2iARC_Country_HU 46UL   ///< Hungary
#define A2iARC_Country_EC 47UL   ///< Ecuador

/**
* Error Return codes
*/
typedef A2iARC_Enum A2iARC_Err ;
#define _A2iARC_Err_Count 154
#define A2iARC_Err_BadVersion 1UL   ///< Incompatible version 
#define A2iARC_Err_BadPlatform 2UL   ///< Unsupported Platform
#define A2iARC_Err_BadType 3UL   ///< Unexpected type
#define A2iARC_Err_BadParameters 4UL   ///< Bad parameter(s)
#define A2iARC_Err_BadIdentifier 5UL   ///< Bad identifier
#define A2iARC_Err_Limits 6UL   ///< Size exeeds limits
#define A2iARC_Err_BadValue 7UL   ///< Unsupported value
#define A2iARC_Err_OneSessionOnly 8UL   ///< One Session only
#define A2iARC_Err_SingleRequestOnly 9UL   ///< One single request only
#define A2iARC_Err_UninitializedSpecifications 10UL   ///< Uninitialized specifications
#define A2iARC_Err_Comm 11UL   ///< Communication problem
#define A2iARC_Err_CreateProcess 12UL   ///< Problem creating a process
#define A2iARC_Err_NoRequest 13UL   ///< No pending request
#define A2iARC_Err_NoAnsw 14UL   ///< No answer available for any request
#define A2iARC_Err_Exception 15UL   ///< An exception was raised : please exit
#define A2iARC_Err_Memory 16UL   ///< No memory left
#define A2iARC_Err_NoProcess 17UL   ///< The request was not processed
#define A2iARC_Err_CreateThread 18UL   ///< Could not create the thread
#define A2iARC_Err_BadImageFormat 19UL   ///< Bad image format
#define A2iARC_Err_BadValue_TransportModel 20UL   ///< Unsupported transport model
#define A2iARC_Err_BadValue_Resolution 21UL   ///< Unsupported image resolution
#define A2iARC_Err_BadValue_DocumentType 22UL   ///< Unsupported document type
#define A2iARC_Err_BadValue_Country 23UL   ///< Unsupported country
#define A2iARC_Err_BadValue_Currency 24UL   ///< Unsupported currency
#define A2iARC_Err_Dongle 25UL   ///< Dongle problem
#define A2iARC_Err_Brake 26UL   ///< Problem with speed checking
#define A2iARC_Err_BadValue_LevelCount 27UL   ///< Unsupported number of gray levels
#define A2iARC_Err_Load 28UL   ///< Error while loading data
#define A2iARC_Err_Dump 29UL   ///< Error while dumping data
#define A2iARC_Err_BadArrays 30UL   ///< Uninitialized or corrupted data
#define A2iARC_Err_Unknown 31UL   ///< Unknown error
#define A2iARC_Err_OncePerProcess 32UL   ///< This function should be called one time per process
#define A2iARC_Err_Kill 33UL   ///< The process was killed
#define A2iARC_Err_NoValue_ImageDimensions 34UL   ///< Explicit image dimensions are needed inside A2iARC_Image
#define A2iARC_Err_BadValue_Origin 35UL   ///< Bad value for enum A2iARC_Origin
#define A2iARC_Err_SuspendThread 36UL   ///< Unable to suspend a thread
#define A2iARC_Err_BadValue_Orientation 37UL   ///< Unsupported orientation
#define A2iARC_Err_BadValue_FieldType 38UL   ///< Unsupported field type
#define A2iARC_Err_BadValue_WriteType 39UL   ///< Unsupported write type
#define A2iARC_Err_BadValue_FontType 40UL   ///< Unsupported font type
#define A2iARC_Err_BadValue_KeyType 41UL   ///< Unsupported key type
#define A2iARC_Err_DifferentKeyType 42UL   ///< Different values for keyType for tab and document
#define A2iARC_Err_BadValue_Unit 43UL   ///< Unsupported unit value
#define A2iARC_Err_OpenFile 44UL   ///< Cannot open file
#define A2iARC_Err_BadParameterFiles 45UL   ///< Bad parameter files
#define A2iARC_Err_BadValue_InputFormat 46UL   ///< Unsupported input format
#define A2iARC_Err_BadValue_ImageSourceType 47UL   ///< Unsupported image source type
#define A2iARC_Err_BadValue_ImageSize 48UL   ///< Image size is not valid
#define A2iARC_Err_BadValue_currencySignLocator 49UL   ///< Bad value for the field currencySignLocator
#define A2iARC_Err_BadRegex 50UL   ///< Bad Syntax for Regular Expression 
#define A2iARC_Err_Duplicate 51UL   ///< Error while duplicating data
#define A2iARC_Err_BadValue_ImageOrientation 52UL   ///< Bad value for the ImageOrientation
#define A2iARC_Err_BabValue_ImageCleanStrip 53UL   ///< Bad value for the Clean strip field
#define A2iARC_Err_BadValue_CleanZoneType 54UL   ///< Bad value for cleaning zone type
#define A2iARC_Err_BadValue_CharacterFieldType 55UL   ///< Bad value for character field type
#define A2iARC_Err_BadValue_InvalidFieldLength 56UL   ///< Bad value : minChar > maxChar
#define A2iARC_Err_BadValue_OperatorType 57UL   ///< Bad value for operator type
#define A2iARC_Err_OperandNotDefined 58UL   ///< Unknown name of operand
#define A2iARC_Err_RecursiveOperator 59UL   ///< Recursive operators not allowed
#define A2iARC_Err_BadValue_InternalProcessing 60UL   ///< Bad value for internal processing type
#define A2iARC_Err_BadNumberOfOperands 61UL   ///< Bad number of operands in operator
#define A2iARC_Err_BadType_Operand 62UL   ///< Bad operand type in operator
#define A2iARC_Err_BadFieldId 63UL   ///< Bad field identificator
#define A2iARC_Err_BadValue_Optimisation 64UL   ///< Bad Optimisation type
#define A2iARC_Err_Fail 65UL   ///< Unspecified error
#define A2iARC_Err_No_Vocabulary 66UL   ///< No vocabulary was provided
#define A2iARC_Err_BadNumberOfLocationRules 67UL   ///< Only one location rule allowed per field.
#define A2iARC_Err_DuplicateIds 68UL   ///< Fields, operators, and markers Ids are unique (or empty) in an Input
///<                     structure.
#define A2iARC_Err_BadValue_MarkerType 69UL   ///< Unimplemented marker type.
#define A2iARC_Err_BadValue_LocationRuleType 70UL   ///< Unimplemented location rule type.
#define A2iARC_Err_BadValue_HandwritingStyle 71UL   ///< Bad handwriting style.
#define A2iARC_Err_UnexpectedVocabulary 72UL   ///< No vocabulary guided recognition in printed and hand printed fields.
#define A2iARC_Err_BadMarkerInLocationRule 73UL   ///< Specified marker doesn't exist, or has the wrong type.
#define A2iARC_Err_BadPersistentDataType 74UL   ///< Bad kind of persistent data (not implemented).
#define A2iARC_Err_BadValue_AcceptPostDated 75UL   ///< acceptPostDated can't be set if a reference date is not provided.
#define A2iARC_Err_BadValue_SpaceDetection 76UL   ///< Bad value for space detection.
#define A2iARC_Err_BadValue_FieldImageQuality 77UL   ///< Bad value for field image quality.
#define A2iARC_Err_BadValue_PrintedOCRType 78UL   ///< Bad value for printedOCRType. Only valid for Characters fields.
#define A2iARC_Err_BadVocabulary_Cursive 79UL   ///< In cursive recognition, words can only be found in the dictionary.
#define A2iARC_Err_BadVocabulary_Printed 80UL   ///< In machine or hand print recognition, words come from dictionary and
///<                     recognition results.
#define A2iARC_Err_BadValue_NumberOfWords 81UL   ///< The minimum number of words should be less than the maximum in a Word
///<                     field.
#define A2iARC_Err_BadIdentification 82UL   ///< The identification you gave is incorrect. Call A2iARC_Identify again.
#define A2iARC_Err_Obsolete 83UL   ///< Obsolete API function, can not be used anymore.
#define A2iARC_Err_WrongDLLPath 84UL   ///< Wrong DLL path for the postal integration unit.
#define A2iARC_Err_PostalDictionaryNotFound 85UL   ///< No postal dictionary was found at the specified location.
#define A2iARC_Err_PostalDictionaryLoadError 86UL   ///< Unknown error during the loading of the postal dictionary.
#define A2iARC_Err_HWWordVerificationNotImpl 87UL   ///< Handwritten word verification is not implemented.
#define A2iARC_Err_NotAvailable_RLMC 88UL   ///< RLMC recogntition not available for this country.
#define A2iARC_Err_BadActionType 89UL   ///< This action type is not supported for the specified persistent.
#define A2iARC_Err_InvalidRegularExpression 90UL   ///< The passed regular expression can not be used.
#define A2iARC_Err_BadValue_PayeeNameLines 91UL   ///< Incorrect value for payeeNameLines field in CheckFieldsInput
#define A2iARC_Err_NoAddressBookFormat 92UL   ///< No format was passed for the address book.
#define A2iARC_Err_NoPostalDictionary 93UL   ///< A postal dictionary is required to use this feature.
#define A2iARC_Err_NoAddressBook 94UL   ///< An address book is required to use this feature.
#define A2iARC_Err_WrongAddressBook 95UL   ///< The address book doesn't supply the correct data for the request.
#define A2iARC_Err_OnlyOneWord 96UL   ///< In verification mode, the input dictionary must contain only one word.
#define A2iARC_Err_RAD_Mismatch 97UL   ///< There was something incoherent with the Document Identification parameters
#define A2iARC_Err_OnlyAmountsInMoneyOrders 98UL   ///< Only amounts can be recognized on a money order.
#define A2iARC_Err_PersistentNotReady 99UL   ///< The persistent data is not ready for recognition (no training).
#define A2iARC_Err_PostalDictionaryOutOfDate 100UL   ///< The postal dictionary is out of date.
#define A2iARC_Err_RAD_BadVersion 101UL   ///< Document Identification files do not match current version. Need a retrain.
#define A2iARC_Err_NotOk 102UL   ///< Global error in Channel with CPU
#define A2iARC_Err_NoStatus 103UL   ///< Status of CPU not defined
#define A2iARC_Err_Initializing 104UL   ///< CPU is still initializing
#define A2iARC_Err_InitializingDongle 105UL   ///< CPU is still initializing dongle connection
#define A2iARC_Err_InitializingPersistent 106UL   ///< CPU is initializing persistent data
#define A2iARC_Err_DestrConnection 107UL   ///< Connection with remote machine was destroyed
#define A2iARC_Err_ComServer 108UL   ///< Communication problem with Server
#define A2iARC_Err_BusySocket 109UL   ///< Socket is already connected - busy socket
#define A2iARC_Err_NoServer 110UL   ///< The target machine refused connection
#define A2iARC_Err_UnknownServer 111UL   ///< Unknown hostname Server
#define A2iARC_Err_CreateSocket 112UL   ///< Cannot create socket
#define A2iARC_Err_BindSocket 113UL   ///< Cannot bind socket
#define A2iARC_Err_SelectSocket 114UL   ///< Cannot select socket
#define A2iARC_Err_ReceiveData 115UL   ///< Cannot receive data from Server
#define A2iARC_Err_CloseServer 116UL   ///< Connection closed by Server
#define A2iARC_Err_TimeoutRecvData 117UL   ///< Time out to receive data from the socket
#define A2iARC_Err_TimeoutConnect 118UL   ///< Time of connection with a server has expired
#define A2iARC_Err_TimeoutRequest 119UL   ///< The request timed out in the recognition engine and couldn't get
///<                     intermediate results.
#define A2iARC_Err_SetupDistPort 120UL   ///< Incorrect setup of distribution port
#define A2iARC_Err_BadNet 121UL   ///< Bad network connection to computer
#define A2iARC_Err_AccessDenied 122UL   ///< Access to remote machine denied
#define A2iARC_Err_BadVersionNW 123UL   ///< Incompatible version on remote machine
#define A2iARC_Err_BadParameterFilesNW 124UL   ///< Bad parameter files on remote machine
#define A2iARC_Err_MemoryNW 125UL   ///< No memory left on remote machine
#define A2iARC_Err_WrongProtocol 126UL   ///< Wrong protocol communication between API and Service
#define A2iARC_Err_BadEventLog 127UL   ///< Bad parameter to event logging: see A2iARC_Boolean
#define A2iARC_Err_DongleGlobal 128UL   ///< No more alive Dongle Server
#define A2iARC_Err_ChannelTimeout 129UL   ///< Open Channel Timeout
#define A2iARC_Err_BadValue_CBills 130UL   ///< Invalid number of bills 
#define A2iARC_Err_BadValue_LarCallPolicy 131UL   ///< A bad value was given for the LAR call policy
#define A2iARC_Err_BadValue_LarCallThreshold 132UL   ///< LarCallThreshold should be between 0. and 1.
#define A2iARC_Err_NotAvailable_PrintedLAR 133UL   ///< Printed LAR is not available for this country.
#define A2iARC_Err_NotAvailable_HWLAR 134UL   ///< Handwritten LAR is not available for this country.
#define A2iARC_Err_NotAvailable_CheckDate 135UL   ///< Check date recognition not available for this country.
#define A2iARC_Err_NotAvailable_CheckAddress 136UL   ///< Check address recognition not available for this country.
#define A2iARC_Err_NotAvailable_CheckPayeeName 137UL   ///< Check payee name recognition not available for this country.
#define A2iARC_Err_NotAvailable_CheckSignature 138UL   ///< Check signature detection not available for this country.
#define A2iARC_Err_NotAvailable_CheckCodeline 139UL   ///< Check MICR/CMC7 recognition not available for this country.
#define A2iARC_Err_NotAvailable_CheckNumber 140UL   ///< Check number recognition not available for this country.
#define A2iARC_Err_NotAvailable_CheckMemoline 141UL   ///< Check memoline recognition not available for this country.
#define A2iARC_Err_NotAvailable_FontTypeAutoDetect 142UL   ///< Font type auto detect not available for this country.
#define A2iARC_Err_NotAvailable_ImageQuality 143UL   ///< Image quality not available for this country.
#define A2iARC_Err_NotAvailable_CheckPayeeAddress 144UL   ///< Check payee address recognition not available for this country.
#define A2iARC_Err_NotAvailable 145UL   ///< Following feature not available for this country :
#define A2iARC_Err_WriteFile 146UL   ///< Cannot write file
#define A2iARC_Err_WrongKeyType 147UL   ///< Wrong KeyType value
#define A2iARC_Err_KeyNotFound 148UL   ///< Key not found
#define A2iARC_Err_TableDef_NotEmpty 149UL   ///< Table of definition not empty
#define A2iARC_Err_A2iARCPgNotFound 150UL   ///< A2iARCPg not found
#define A2iARC_Err_PageOutOfRange 151UL   ///< Number of pages in collection exceeds the limit maxPagesPerCollection
#define A2iARC_Err_BadValue_WritingPeriod 152UL   ///< Bad writing period
#define A2iARC_Err_CustomAddress_SemanticError 153UL   ///< CustomAddress semantic error
#define A2iARC_Err_Database 154UL   ///< Error in A2iA database

/**
* Source for an image
*/
typedef A2iARC_Enum A2iARC_ImageSourceType ;
#define _A2iARC_ImageSourceType_Count 3
#define A2iARC_ImageSourceType_File 1UL   ///< Get image from a file
#define A2iARC_ImageSourceType_Memory 2UL   ///< image is available in memory
#define A2iARC_ImageSourceType_SharedMemory 3UL   ///< image is available as a shared memory object memory

/**
* Input format for an image
*/
typedef A2iARC_Enum A2iARC_InputFormat ;
#define _A2iARC_InputFormat_Count 7
#define A2iARC_InputFormat_Byte 1UL   ///< Raw pixels buffer, interlaced Z-aligned components.
#define A2iARC_InputFormat_Tiff 2UL   ///< Tagged Image File format.
#define A2iARC_InputFormat_Bmp 3UL   ///< Microsoft Windows Bitmap format.
#define A2iARC_InputFormat_Jpeg 4UL   ///< Joint Photographic Experts Group format.
#define A2iARC_InputFormat_JP2 5UL   ///< JPEG-2000 JP2 format.
#define A2iARC_InputFormat_Png 6UL   ///< Portable Network Graphics format.
#define A2iARC_InputFormat_Pdf 7UL   ///< Portable Document format.

/**
* Model of transport
*/
typedef A2iARC_Enum A2iARC_TransportModel ;
#define _A2iARC_TransportModel_Count 15
#define A2iARC_TransportModel_Generic 1UL   ///< 
#define A2iARC_TransportModel_Unisys_DP500 2UL   ///< 
#define A2iARC_TransportModel_NCR_7780 3UL   ///< 
#define A2iARC_TransportModel_BANCTEC 4UL   ///< 
#define A2iARC_TransportModel_SEAC 5UL   ///< 
#define A2iARC_TransportModel_JMD 6UL   ///< 
#define A2iARC_TransportModel_CTS_ATHIC 7UL   ///< 
#define A2iARC_TransportModel_CTS_A2IA 8UL   ///< 
#define A2iARC_TransportModel_BANCTEC_UT 9UL   ///< 
#define A2iARC_TransportModel_PANINI_S1 10UL   ///< 
#define A2iARC_TransportModel_VIPS 11UL   ///< 
#define A2iARC_TransportModel_Diebold 12UL   ///< 
#define A2iARC_TransportModel_Standard_Register 13UL   ///< 
#define A2iARC_TransportModel_Brazil_Special 14UL   ///< 
#define A2iARC_TransportModel_MobilePhone 15UL   ///< 

typedef A2iARC_Enum A2iARC_ImageOrientation ;
#define _A2iARC_ImageOrientation_Count 8
#define A2iARC_ImageOrientation_LeftRight_TopBottom 1UL   ///< 
#define A2iARC_ImageOrientation_RightLeft_TopBottom 2UL   ///< 
#define A2iARC_ImageOrientation_RightLeft_BottomTop 3UL   ///< 
#define A2iARC_ImageOrientation_LeftRight_BottomTop 4UL   ///< 
#define A2iARC_ImageOrientation_TopBottom_LeftRight 5UL   ///< 
#define A2iARC_ImageOrientation_TopBottom_RightLeft 6UL   ///< 
#define A2iARC_ImageOrientation_BottomTop_RightLeft 7UL   ///< 
#define A2iARC_ImageOrientation_BottomTop_LeftRight 8UL   ///< 

typedef struct _A2iARC_Image
{
   A2iARC_ImageOrientation imageOrientation ;
   A2iARC_ImageSourceType imageSourceType ;
   A2iARC_InputFormat inputFormat ;
   union    // Depends on inputFormat
   {
      struct // Case inputFormat == Tiff
      {
         A2iARC_Unsigned pageIndex ;
      } CaseTiff ;
      struct // Case inputFormat == Pdf
      {
         A2iARC_Unsigned pageIndex ;
      } CasePdf ;
   } inputFormatInfo ;
   A2iARC_TransportModel transportModel ;
   A2iARC_Boolean readResolutionFromImageHeader ;
   A2iARC_Unsigned resolution_Dpi ;
   A2iARC_Unsigned X_resolution_Dpi ;
   A2iARC_Unsigned Y_resolution_Dpi ;
   A2iARC_Unsigned levelCount ;
   A2iARC_Unsigned rowSize ;
   A2iARC_Unsigned colSize ;
   union    // Depends on imageSourceType
   {
      struct // Case imageSourceType == File
      {
         A2iARC_File fileName ;
      } CaseFile ;
      struct // Case imageSourceType == Memory
      {
         A2iARC_Buffer buffer ;
      } CaseMemory ;
      struct // Case imageSourceType == SharedMemory
      {
         A2iARC_String sharedMemoryName ;
         A2iARC_Unsigned imageSize ;
         A2iARC_Unsigned offset ;
      } CaseSharedMemory ;
   } imageSourceTypeInfo ;
   A2iARC_String imageId ;
} A2iARC_Image;

typedef A2iARC_Enum A2iARC_ReverseValue ;
#define _A2iARC_ReverseValue_Count 3
#define A2iARC_ReverseValue_Yes 1UL   ///< 
#define A2iARC_ReverseValue_No 2UL   ///< 
#define A2iARC_ReverseValue_AutoDetect 3UL   ///< 

typedef A2iARC_Enum A2iARC_IQAOptimisation ;
#define _A2iARC_IQAOptimisation_Count 2
#define A2iARC_IQAOptimisation_MaximizeSpeed 1UL   ///< 
#define A2iARC_IQAOptimisation_MaximizeAccuracy 2UL   ///< 

typedef struct _A2iARC_ImageQuality_UndersizeImage
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned minimumImageWidthThreshold ;
   A2iARC_Unsigned minimumImageHeightThreshold ;
} A2iARC_ImageQuality_UndersizeImage;

typedef struct _A2iARC_ImageQuality_FoldedOrTornDocumentCorners
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned maximumTopLeftCornerFoldTearWidthThreshold ;
   A2iARC_Unsigned maximumTopLeftCornerFoldTearHeightThreshold ;
   A2iARC_Unsigned maximumBottomLeftCornerFoldTearWidthThreshold ;
   A2iARC_Unsigned maximumBottomLeftCornerFoldTearHeightThreshold ;
   A2iARC_Unsigned maximumTopRightCornerFoldTearWidthThreshold ;
   A2iARC_Unsigned maximumTopRightCornerFoldTearHeightThreshold ;
   A2iARC_Unsigned maximumBottomRightCornerFoldTearWidthThreshold ;
   A2iARC_Unsigned maximumBottomRightCornerFoldTearHeightThreshold ;
} A2iARC_ImageQuality_FoldedOrTornDocumentCorners;

typedef struct _A2iARC_ImageQuality_FoldedOrTornDocumentEdges
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned maximumTopEdgeWidth ;
   A2iARC_Unsigned maximumTopEdgeHeight ;
   A2iARC_Unsigned maximumRightEdgeWidth ;
   A2iARC_Unsigned maximumRightEdgeHeight ;
   A2iARC_Unsigned maximumBottomEdgeWidth ;
   A2iARC_Unsigned maximumBottomEdgeHeight ;
   A2iARC_Unsigned maximumLeftEdgeWidth ;
   A2iARC_Unsigned maximumLeftEdgeHeight ;
} A2iARC_ImageQuality_FoldedOrTornDocumentEdges;

typedef struct _A2iARC_ImageQuality_DocumentFramingError
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned maximumAdditionalLeftScanLinesWidth ;
   A2iARC_Unsigned maximumAdditionalTopScanLinesHeight ;
   A2iARC_Unsigned maximumAdditionalRightScanLinesWidth ;
   A2iARC_Unsigned maximumAdditionalBottomScanLinesHeight ;
} A2iARC_ImageQuality_DocumentFramingError;

typedef struct _A2iARC_ImageQuality_DocumentSkew
{
   A2iARC_Boolean enable ;
   A2iARC_Int positiveDocumentSkewAngle ;
   A2iARC_Int negativeDocumentSkewAngle ;
} A2iARC_ImageQuality_DocumentSkew;

typedef struct _A2iARC_ImageQuality_OversizeImage
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned maximumImageWidthThreshold ;
   A2iARC_Unsigned maximumImageHeightThreshold ;
} A2iARC_ImageQuality_OversizeImage;

typedef struct _A2iARC_ImageQuality_PiggybackDocument
{
   A2iARC_Boolean enable ;
   A2iARC_Boolean enableThreshold ;
   A2iARC_Float threshold ;
} A2iARC_ImageQuality_PiggybackDocument;

typedef struct _A2iARC_ImageQuality_ImageTooLight
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned minimumPercentBlackPixels ;
   A2iARC_Unsigned maximumPercentBrightness ;
   A2iARC_Unsigned minimumPercentContrast ;
} A2iARC_ImageQuality_ImageTooLight;

typedef struct _A2iARC_ImageQuality_ImageTooDark
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned maximumPercentBlackPixels ;
   A2iARC_Unsigned minimumPercentBrightness ;
} A2iARC_ImageQuality_ImageTooDark;

typedef struct _A2iARC_ImageQuality_HorizontalStreaks
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned maximumStreakHeightToBeDetectedThreshold ;
   A2iARC_Unsigned maxBlackStreakCount ;
   A2iARC_Unsigned maxBlackStreakMaxHeight ;
   A2iARC_Unsigned blackStreakPercentageThreshold ;
   A2iARC_Unsigned maxGrayLevelStreakCount ;
   A2iARC_Unsigned maxGrayLevelStreakMaxHeight ;
   A2iARC_Unsigned streakContrastThreshold ;
} A2iARC_ImageQuality_HorizontalStreaks;

typedef struct _A2iARC_ImageQuality_BelowMinimumCompressedImageSize
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned minimumBitonalCompressedImageSize ;
   A2iARC_Unsigned minimumGrayLevelCompressedImageSize ;
} A2iARC_ImageQuality_BelowMinimumCompressedImageSize;

typedef struct _A2iARC_ImageQuality_AboveMaximumCompressedImageSize
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned maximumBitonalCompressedImageSize ;
   A2iARC_Unsigned maximumGrayLevelCompressedImageSize ;
} A2iARC_ImageQuality_AboveMaximumCompressedImageSize;

typedef struct _A2iARC_ImageQuality_SpotNoise
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned maximumAverageSpotNoiseGroupingsPerSquareInch ;
} A2iARC_ImageQuality_SpotNoise;

typedef struct _A2iARC_ImageQuality_FrontRearImageDimensionMismatch
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned frontRearWidthDifference ;
   A2iARC_Unsigned frontRearHeightDifference ;
} A2iARC_ImageQuality_FrontRearImageDimensionMismatch;

typedef struct _A2iARC_ImageQuality_CarbonStrip
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned minimumCarbonStripHeight ;
} A2iARC_ImageQuality_CarbonStrip;

typedef struct _A2iARC_ImageQuality_OutOfFocus
{
   A2iARC_Boolean enable ;
   A2iARC_Float minimumImageFocusScore ;
} A2iARC_ImageQuality_OutOfFocus;

typedef struct _A2iARC_StreakValues
{
   A2iARC_Unsigned streakMinPixelHeight ;
   A2iARC_Unsigned streakMinPixelWidth ;
   A2iARC_Unsigned streakPixelOffset ;
   A2iARC_Unsigned streakDarkness ;
} A2iARC_StreakValues;

typedef struct _A2iARC_ImageQualityInput
{
   A2iARC_Boolean enable ;
   A2iARC_IQAOptimisation optimisation ;
   A2iARC_ReverseValue reverseValues ;
   A2iARC_Unsigned compressedImageSize ;
   A2iARC_Unsigned compressedRearImageSize ;
   A2iARC_ImageQuality_UndersizeImage undersizeImage ;
   A2iARC_ImageQuality_UndersizeImage undersizeRearImage ;
   A2iARC_ImageQuality_FoldedOrTornDocumentCorners foldedOrTornDocumentCorners ;
   A2iARC_ImageQuality_FoldedOrTornDocumentCorners foldedOrTornRearDocumentCorners ;
   A2iARC_ImageQuality_FoldedOrTornDocumentEdges foldedOrTornDocumentEdges ;
   A2iARC_ImageQuality_FoldedOrTornDocumentEdges foldedOrTornRearDocumentEdges ;
   A2iARC_ImageQuality_DocumentFramingError documentFramingError ;
   A2iARC_ImageQuality_DocumentFramingError rearDocumentFramingError ;
   A2iARC_ImageQuality_DocumentSkew documentSkew ;
   A2iARC_ImageQuality_DocumentSkew rearDocumentSkew ;
   A2iARC_ImageQuality_OversizeImage oversizeImage ;
   A2iARC_ImageQuality_OversizeImage oversizeRearImage ;
   A2iARC_ImageQuality_PiggybackDocument piggybackDocument ;
   A2iARC_ImageQuality_PiggybackDocument piggybackRearDocument ;
   A2iARC_ImageQuality_ImageTooLight imageTooLight ;
   A2iARC_ImageQuality_ImageTooLight rearImageTooLight ;
   A2iARC_ImageQuality_ImageTooDark imageTooDark ;
   A2iARC_ImageQuality_ImageTooDark rearImageTooDark ;
   A2iARC_ImageQuality_HorizontalStreaks horizontalStreaks ;
   A2iARC_ImageQuality_HorizontalStreaks rearHorizontalStreaks ;
   A2iARC_ImageQuality_BelowMinimumCompressedImageSize belowMinimumCompressedFrontImageSize ;
   A2iARC_ImageQuality_AboveMaximumCompressedImageSize aboveMaximumCompressedFrontImageSize ;
   A2iARC_ImageQuality_BelowMinimumCompressedImageSize belowMinimumCompressedRearImageSize ;
   A2iARC_ImageQuality_AboveMaximumCompressedImageSize aboveMaximumCompressedRearImageSize ;
   A2iARC_ImageQuality_SpotNoise spotNoise ;
   A2iARC_ImageQuality_SpotNoise rearSpotNoise ;
   A2iARC_ImageQuality_FrontRearImageDimensionMismatch frontRearImageDimensionMismatch ;
   A2iARC_ImageQuality_CarbonStrip carbonStrip ;
   A2iARC_ImageQuality_OutOfFocus outOfFocus ;
   A2iARC_ImageQuality_OutOfFocus rearOutOfFocus ;
} A2iARC_ImageQualityInput;

typedef struct _A2iARC_ImageQualityValues
{
   A2iARC_Unsigned imageWidth ;
   A2iARC_Unsigned imageHeight ;
   A2iARC_Unsigned topleftCornerWidth ;
   A2iARC_Unsigned topleftCornerHeight ;
   A2iARC_Unsigned bottomleftCornerWidth ;
   A2iARC_Unsigned bottomleftCornerHeight ;
   A2iARC_Unsigned toprightCornerWidth ;
   A2iARC_Unsigned toprightCornerHeight ;
   A2iARC_Unsigned bottomrightCornerWidth ;
   A2iARC_Unsigned bottomrightCornerHeight ;
   A2iARC_Unsigned topEdgeTearWidth ;
   A2iARC_Unsigned topEdgeTearHeight ;
   A2iARC_Unsigned rightEdgeTearWidth ;
   A2iARC_Unsigned rightEdgeTearHeight ;
   A2iARC_Unsigned bottomEdgeTearWidth ;
   A2iARC_Unsigned bottomEdgeTearHeight ;
   A2iARC_Unsigned leftEdgeTearWidth ;
   A2iARC_Unsigned leftEdgeTearHeight ;
   A2iARC_Unsigned additionalLeftScanLinesWidth ;
   A2iARC_Unsigned additionalTopScanLinesHeight ;
   A2iARC_Unsigned additionalRightScanLinesWidth ;
   A2iARC_Unsigned additionalBottomScanLinesHeight ;
   A2iARC_Int documentSkewAngle ;
   A2iARC_Unsigned percentBlackPixels ;
   A2iARC_Unsigned percentAverageImageBrightness ;
   A2iARC_Unsigned percentAverageImageContrast ;
   A2iARC_Unsigned blackStreakCount ;
   A2iARC_Unsigned blackStreakMaxHeight ;
   A2iARC_Unsigned grayLevelStreakCount ;
   A2iARC_Unsigned grayLevelStreakMaxHeight ;
   struct
   {
      struct _A2iARC_StreakValues * pTab ;
      unsigned long  cTab ;
   } streakValues ;
   A2iARC_Unsigned averageSpotNoiseGroupingsPerSquareInch ;
   A2iARC_Unsigned frontRearWidthDifference ;
   A2iARC_Unsigned frontRearHeightDifference ;
   A2iARC_Unsigned carbonStripHeight ;
   A2iARC_Float imageFocusScore ;
   A2iARC_Unsigned compressedFrontImageSize ;
   A2iARC_Unsigned compressedRearImageSize ;
} A2iARC_ImageQualityValues;

typedef struct _A2iARC_ImageQualityResults
{
   A2iARC_ImageQualityValues values ;
   A2iARC_ImageQualityValues rearValues ;
   A2iARC_Unsigned undersizeImageFlag ;
   A2iARC_Unsigned undersizeRearImageFlag ;
   A2iARC_Unsigned foldedOrTornDocumentCornersFlag ;
   A2iARC_Unsigned foldedOrTornRearDocumentCornersFlag ;
   A2iARC_Unsigned foldedOrTornDocumentEdgesFlag ;
   A2iARC_Unsigned foldedOrTornRearDocumentEdgesFlag ;
   A2iARC_Unsigned documentFramingErrorFlag ;
   A2iARC_Unsigned rearDocumentFramingErrorFlag ;
   A2iARC_Unsigned documentSkewFlag ;
   A2iARC_Unsigned rearDocumentSkewFlag ;
   A2iARC_Unsigned oversizeImageFlag ;
   A2iARC_Unsigned oversizeRearImageFlag ;
   A2iARC_Unsigned piggybackDocumentFlag ;
   A2iARC_Unsigned piggybackRearDocumentFlag ;
   A2iARC_Unsigned imageTooLightFlag ;
   A2iARC_Unsigned rearImageTooLightFlag ;
   A2iARC_Unsigned imageTooDarkFlag ;
   A2iARC_Unsigned rearImageTooDarkFlag ;
   A2iARC_Unsigned horizontalStreaksFlag ;
   A2iARC_Unsigned rearHorizontalStreaksFlag ;
   A2iARC_Unsigned belowMinimumCompressedFrontImageSizeFlag ;
   A2iARC_Unsigned aboveMaximumCompressedFrontImageSizeFlag ;
   A2iARC_Unsigned belowMinimumCompressedRearImageSizeFlag ;
   A2iARC_Unsigned aboveMaximumCompressedRearImageSizeFlag ;
   A2iARC_Unsigned spotNoiseFlag ;
   A2iARC_Unsigned rearSpotNoiseFlag ;
   A2iARC_Unsigned frontRearImageDimensionMismatchFlag ;
   A2iARC_Unsigned carbonStripFlag ;
   A2iARC_Unsigned outOfFocusFlag ;
   A2iARC_Unsigned rearOutOfFocusFlag ;
} A2iARC_ImageQualityResults;

typedef A2iARC_Enum A2iARC_InvertedText ;
#define _A2iARC_InvertedText_Count 3
#define A2iARC_InvertedText_Yes 1UL   ///< 
#define A2iARC_InvertedText_No 2UL   ///< 
#define A2iARC_InvertedText_AutoDetect 3UL   ///< 

typedef A2iARC_Enum A2iARC_Gender ;
#define _A2iARC_Gender_Count 2
#define A2iARC_Gender_Female 1UL   ///< 
#define A2iARC_Gender_Male 2UL   ///< 

typedef A2iARC_Enum A2iARC_Currency ;
#define _A2iARC_Currency_Count 24
#define A2iARC_Currency_USD 1UL   ///< Us Dollar
#define A2iARC_Currency_FRF 2UL   ///< French Franc
#define A2iARC_Currency_EUR 3UL   ///< Euro currency
#define A2iARC_Currency_GBP 4UL   ///< Sterling pound
#define A2iARC_Currency_HKD 5UL   ///< Hong-Kong dollar
#define A2iARC_Currency_AUD 6UL   ///< Australian dollar
#define A2iARC_Currency_IEP 7UL   ///< Irish punt
#define A2iARC_Currency_CAD 8UL   ///< Canadian dollar
#define A2iARC_Currency_BRL 9UL   ///< brazilian real
#define A2iARC_Currency_PTE 10UL   ///< Portugese Escudo
#define A2iARC_Currency_MXP 11UL   ///< Mexican Peso
#define A2iARC_Currency_NLG 12UL   ///< Dutch Guilder
#define A2iARC_Currency_THB 13UL   ///< Thai Baht
#define A2iARC_Currency_DEM 14UL   ///< Deutschmark
#define A2iARC_Currency_XPF 15UL   ///< Francs Comptoirs Francais du Pacifique
#define A2iARC_Currency_CLP 16UL   ///< Chilian Peso
#define A2iARC_Currency_BEF 17UL   ///< Belgian Franc
#define A2iARC_Currency_SGD 18UL   ///< Singapore Dollar
#define A2iARC_Currency_MYR 19UL   ///< Malaysian Ringgit
#define A2iARC_Currency_INR 20UL   ///< Indian Rupee
#define A2iARC_Currency_ZAR 21UL   ///< South African Rand
#define A2iARC_Currency_COP 22UL   ///< 
#define A2iARC_Currency_DOP 23UL   ///< 
#define A2iARC_Currency_KES 24UL   ///< 

/**
* Use only for A2iA; describe a pocessing mode for operateur.
*/
typedef A2iARC_Enum A2iARC_InternalProcessing ;
#define _A2iARC_InternalProcessing_Count 1
#define A2iARC_InternalProcessing_Soge 1UL   ///< 

typedef A2iARC_Enum A2iARC_CleanStrip ;
#define _A2iARC_CleanStrip_Count 3
#define A2iARC_CleanStrip_No 1UL   ///< default value
#define A2iARC_CleanStrip_Black 2UL   ///< 
#define A2iARC_CleanStrip_White 3UL   ///< 

typedef A2iARC_Enum A2iARC_DataPersistence ;
#define _A2iARC_DataPersistence_Count 3
#define A2iARC_DataPersistence_Request 1UL   ///< 
#define A2iARC_DataPersistence_Persistent 2UL   ///< 
#define A2iARC_DataPersistence_None 3UL   ///< 

typedef A2iARC_Enum A2iARC_DataFormat ;
#define _A2iARC_DataFormat_Count 2
#define A2iARC_DataFormat_Simple 1UL   ///< 
#define A2iARC_DataFormat_File 2UL   ///< 

typedef A2iARC_Enum A2iARC_OrientationCorrection ;
#define _A2iARC_OrientationCorrection_Count 3
#define A2iARC_OrientationCorrection_UpsideDownFromMICR 1UL   ///< 
#define A2iARC_OrientationCorrection_AutoCorrect 2UL   ///< 
#define A2iARC_OrientationCorrection_None 3UL   ///< 

typedef A2iARC_Enum A2iARC_OutputFormat ;
#define _A2iARC_OutputFormat_Count 3
#define A2iARC_OutputFormat_Tiff 1UL   ///< 
#define A2iARC_OutputFormat_Bmp 2UL   ///< 
#define A2iARC_OutputFormat_Jpeg 3UL   ///< 

typedef struct _A2iARC_ImageConversion
{
   A2iARC_Boolean enableImageConversion ;
   A2iARC_Boolean stopAfterConversion ;
   A2iARC_OutputFormat outputFormat ;
   A2iARC_ImageSourceType imageSourceType ;
   union    // Depends on imageSourceType
   {
      struct // Case imageSourceType == File
      {
         A2iARC_SaveFile fileName ;
      } CaseFile ;
   } imageSourceTypeInfo ;
} A2iARC_ImageConversion;

typedef struct _A2iARC_ImagePreprocessing
{
   A2iARC_CleanStrip TopClean ;
   A2iARC_CleanStrip LeftClean ;
   A2iARC_CleanStrip RightClean ;
   A2iARC_CleanStrip BottomClean ;
   A2iARC_ReverseValue reverseValues ;
   A2iARC_OrientationCorrection orientationCorrection ;
   A2iARC_Boolean modelRegistration ;
   A2iARC_Boolean crop ;
   union    // Depends on crop
   {
      struct // Case crop == Yes
      {
         A2iARC_Unsigned left ;
         A2iARC_Unsigned top ;
         A2iARC_Unsigned right ;
         A2iARC_Unsigned bottom ;
      } CaseYes ;
   } cropInfo ;
   A2iARC_Boolean extractIRD ;
   A2iARC_Float binarControl ;
   A2iARC_Boolean skewCorrection ;
   A2iARC_Boolean horizontalStreakRemoval ;
   A2iARC_Boolean rejectSkewedDocument ;
} A2iARC_ImagePreprocessing;

typedef struct _A2iARC_DocumentModel
{
   A2iARC_String parameterFile ;
   A2iARC_Image modelImage ;
   A2iARC_String modelFolder ;
   struct
   {
      struct _A2iARC_Image * pTab ;
      unsigned long  cTab ;
   } sampleImages ;
   A2iARC_ImagePreprocessing imagePreprocessing ;
   A2iARC_Boolean useGuideMarks ;
} A2iARC_DocumentModel;

typedef A2iARC_Enum A2iARC_PostalVendor ;
#define _A2iARC_PostalVendor_Count 7
#define A2iARC_PostalVendor_MediaPost 1UL   ///< 
#define A2iARC_PostalVendor_FirstLogicUS 2UL   ///< 
#define A2iARC_PostalVendor_USPS 3UL   ///< 
#define A2iARC_PostalVendor_DeutschePost 4UL   ///< 
#define A2iARC_PostalVendor_SynaxioFR 5UL   ///< 
#define A2iARC_PostalVendor_AVINC 6UL   ///< 
#define A2iARC_PostalVendor_Custom 7UL   ///< 

typedef A2iARC_Enum A2iARC_SQLServer ;
#define _A2iARC_SQLServer_Count 3
#define A2iARC_SQLServer_MSSQL 1UL   ///< 
#define A2iARC_SQLServer_MySQL 2UL   ///< 
#define A2iARC_SQLServer_A2iA 3UL   ///< 

typedef struct _A2iARC_PostalDictionary
{
   A2iARC_Country country ;
   A2iARC_File dllPath ;
   A2iARC_Dir dataPath ;
   A2iARC_PostalVendor vendor ;
   union    // Depends on vendor
   {
      struct // Case vendor == MediaPost
      {
         A2iARC_String cityZipFilename ;
         A2iARC_String cityZipStreetFilename ;
         A2iARC_String houseFilename ;
      } CaseMediaPost ;
      struct // Case vendor == FirstLogicUS
      {
         A2iARC_String pathDLL ;
         A2iARC_String cityZipStateFilename ;
         A2iARC_String zipStreetFilename ;
      } CaseFirstLogicUS ;
      struct // Case vendor == USPS
      {
         A2iARC_String serverName ;
         A2iARC_String userName ;
         A2iARC_String password ;
         A2iARC_String databaseName ;
         A2iARC_SQLServer SQLServer ;
      } CaseUSPS ;
      struct // Case vendor == DeutschePost
      {
         A2iARC_String serverName ;
         A2iARC_String userName ;
         A2iARC_String password ;
         A2iARC_String databaseName ;
         A2iARC_SQLServer SQLServer ;
      } CaseDeutschePost ;
   } vendorInfo ;
} A2iARC_PostalDictionary;

/**
* Object location inside an image
*/
typedef A2iARC_Enum A2iARC_Unit ;
#define _A2iARC_Unit_Count 4
#define A2iARC_Unit_Meters 1UL   ///< 
#define A2iARC_Unit_Millimetres 2UL   ///< 
#define A2iARC_Unit_Pixels 3UL   ///< 
#define A2iARC_Unit_Inches 4UL   ///< 

typedef A2iARC_Enum A2iARC_Origin ;
#define _A2iARC_Origin_Count 9
#define A2iARC_Origin_TopLeft 1UL   ///< Coordinates are relative to the top left corner
#define A2iARC_Origin_BotLeft 2UL   ///< Coordinates are relative to the bottom left corner
#define A2iARC_Origin_TopRight 3UL   ///< Coordinates are relative to the top right corner
#define A2iARC_Origin_BotRight 4UL   ///< Coordinates are relative to the bottom right corner
#define A2iARC_Origin_Top 5UL   ///< Coordinates are relative to the top edge (horizontal strip : x coord
///<                     ignored)
#define A2iARC_Origin_Bottom 6UL   ///< Coordinates are relative to the bottom edge (horizontal strip : x coord
///<                     ignored)
#define A2iARC_Origin_Left 7UL   ///< Coordinates are relative to the left edge (vertical strip : y coord
///<                     ignored)
#define A2iARC_Origin_Right 8UL   ///< Coordinates are relative to the right edge (vertical strip : y coord
///<                     ignored)
#define A2iARC_Origin_Marker 9UL   ///< Coordinates are relative to the top left corner of a marker box

typedef struct _A2iARC_Box
{
   A2iARC_Origin origin ;
   A2iARC_Float x_tl ;
   A2iARC_Float x_br ;
   A2iARC_Float y_tl ;
   A2iARC_Float y_br ;
   A2iARC_Unit unit ;
   A2iARC_String markerId ;
   A2iARC_Unsigned pageIndex ;
} A2iARC_Box;

/**
* Object orientation (field to be recognized or others)
*/
typedef A2iARC_Enum A2iARC_Orientation ;
#define _A2iARC_Orientation_Count 4
#define A2iARC_Orientation_LeftRight 1UL   ///< 
#define A2iARC_Orientation_TopDown 2UL   ///< 
#define A2iARC_Orientation_RightLeft 3UL   ///< 
#define A2iARC_Orientation_BottomUp 4UL   ///< 

typedef A2iARC_Enum A2iARC_LocationRuleType ;
#define _A2iARC_LocationRuleType_Count 4
#define A2iARC_LocationRuleType_LeftShift 1UL   ///< 
#define A2iARC_LocationRuleType_RightShift 2UL   ///< 
#define A2iARC_LocationRuleType_TopShift 3UL   ///< 
#define A2iARC_LocationRuleType_BottomShift 4UL   ///< 

typedef struct _A2iARC_LocationRule
{
   A2iARC_LocationRuleType type ;
   union    // Depends on type
   {
      struct // Case type == LeftShift
      {
         A2iARC_String markerId ;
      } CaseLeftShift ;
      struct // Case type == RightShift
      {
         A2iARC_String markerId ;
      } CaseRightShift ;
      struct // Case type == TopShift
      {
         A2iARC_String markerId ;
      } CaseTopShift ;
      struct // Case type == BottomShift
      {
         A2iARC_String markerId ;
      } CaseBottomShift ;
   } typeInfo ;
} A2iARC_LocationRule;

typedef struct _A2iARC_Location
{
   struct
   {
      struct _A2iARC_LocationRule * pTab ;
      unsigned long  cTab ;
   } rules ;
} A2iARC_Location;

/**
* Type of writing
*/
typedef A2iARC_Enum A2iARC_WriteType ;
#define _A2iARC_WriteType_Count 3
#define A2iARC_WriteType_Handwritten 1UL   ///< 
#define A2iARC_WriteType_Printed 2UL   ///< 
#define A2iARC_WriteType_AutoDetect 3UL   ///< 

typedef A2iARC_Enum A2iARC_HandwritingStyle ;
#define _A2iARC_HandwritingStyle_Count 5
#define A2iARC_HandwritingStyle_SeparatedProportional 1UL   ///< 
#define A2iARC_HandwritingStyle_SeparatedFixed 2UL   ///< 
#define A2iARC_HandwritingStyle_HandPrinted 3UL   ///< 
#define A2iARC_HandwritingStyle_Cursive 4UL   ///< 
#define A2iARC_HandwritingStyle_Regular 5UL   ///< Obsolete

/**
* Define the writing period for antique write type recognition.
*/
typedef A2iARC_Enum A2iARC_WritingPeriod ;
#define _A2iARC_WritingPeriod_Count 2
#define A2iARC_WritingPeriod_Contemporary 1UL   ///< 
#define A2iARC_WritingPeriod_MidNineteenthEarlyTwentieth 2UL   ///< 

/**
* Cleaning results
*/
typedef A2iARC_Enum A2iARC_BoxingType ;
#define _A2iARC_BoxingType_Count 10
#define A2iARC_BoxingType_BestGuess 1UL   ///< 
#define A2iARC_BoxingType_None 2UL   ///< 
#define A2iARC_BoxingType_Full 3UL   ///< 
#define A2iARC_BoxingType_Comb 4UL   ///< 
#define A2iARC_BoxingType_Broken 5UL   ///< 
#define A2iARC_BoxingType_Underlined 6UL   ///< 
#define A2iARC_BoxingType_PreBoxed 7UL   ///< 
#define A2iARC_BoxingType_SeparatedBoxes 8UL   ///< 
#define A2iARC_BoxingType_DottedUnderline 9UL   ///< 
#define A2iARC_BoxingType_ModelCleaning 10UL   ///< 

typedef A2iARC_Enum A2iARC_ImageQuality ;
#define _A2iARC_ImageQuality_Count 2
#define A2iARC_ImageQuality_Standard 1UL   ///< 
#define A2iARC_ImageQuality_NoNoise 2UL   ///< 

typedef A2iARC_Enum A2iARC_IdentityDocumentType ;
#define _A2iARC_IdentityDocumentType_Count 4
#define A2iARC_IdentityDocumentType_IdentityCardFront 1UL   ///< 
#define A2iARC_IdentityDocumentType_Passport 2UL   ///< 
#define A2iARC_IdentityDocumentType_Visa 3UL   ///< 
#define A2iARC_IdentityDocumentType_ResidencePermit 4UL   ///< 

/**
* Type of Characters Encoding Format
*/
typedef A2iARC_Enum A2iARC_CharactersEncoding ;
#define _A2iARC_CharactersEncoding_Count 2
#define A2iARC_CharactersEncoding_CP1252 1UL   ///< 
#define A2iARC_CharactersEncoding_UTF8 2UL   ///< 

typedef struct _A2iARC_CleanedImageResult
{
   A2iARC_Image image ;
   A2iARC_Box location ;
} A2iARC_CleanedImageResult;

typedef struct _A2iARC_AmountScore
{
   A2iARC_Unsigned amount ;
   A2iARC_Unsigned score ;
   A2iARC_Currency detectedCurrency ;
} A2iARC_AmountScore;

typedef struct _A2iARC_AmountProb
{
   A2iARC_Unsigned amount ;
   A2iARC_Float prob ;
} A2iARC_AmountProb;

typedef struct _A2iARC_CharactersScore
{
   struct
   {
      A2iARC_Char * pTab ;
      unsigned long  cTab ;
   } reco ;
   A2iARC_String wreco ;
   A2iARC_Unsigned score ;
} A2iARC_CharactersScore;

typedef struct _A2iARC_CharactersProb
{
   struct
   {
      A2iARC_Char * pTab ;
      unsigned long  cTab ;
   } reco ;
   A2iARC_String wreco ;
   A2iARC_Float prob ;
} A2iARC_CharactersProb;

typedef struct _A2iARC_CharactersResult
{
   A2iARC_CharactersScore result ;
   struct
   {
      struct _A2iARC_CharactersProb * pTab ;
      unsigned long  cTab ;
   } prob ;
} A2iARC_CharactersResult;

typedef struct _A2iARC_DicoWord
{
   A2iARC_String word ;
   A2iARC_Unsigned index ;
} A2iARC_DicoWord;

typedef struct _A2iARC_StringProb
{
   A2iARC_String reco ;
   A2iARC_Float confidence ;
} A2iARC_StringProb;

typedef struct _A2iARC_SingleWord
{
   A2iARC_String reco ;
   A2iARC_Unsigned itemIndex ;
   A2iARC_Unsigned imageIndex ;
   struct
   {
      struct _A2iARC_DicoWord * pTab ;
      unsigned long  cTab ;
   } originals ;
} A2iARC_SingleWord;

typedef struct _A2iARC_WordProb
{
   struct
   {
      struct _A2iARC_SingleWord * pTab ;
      unsigned long  cTab ;
   } words ;
   A2iARC_Float prob ;
} A2iARC_WordProb;

typedef struct _A2iARC_WordScore
{
   A2iARC_String reco ;
   A2iARC_Unsigned score ;
   struct
   {
      struct _A2iARC_SingleWord * pTab ;
      unsigned long  cTab ;
   } words ;
} A2iARC_WordScore;

typedef struct _A2iARC_WordResult
{
   A2iARC_WordScore result ;
   struct
   {
      struct _A2iARC_WordProb * pTab ;
      unsigned long  cTab ;
   } finalList ;
   struct
   {
      struct _A2iARC_CleanedImageResult * pTab ;
      unsigned long  cTab ;
   } wordImages ;
} A2iARC_WordResult;

typedef struct _A2iARC_Date
{
   A2iARC_Unsigned day ;
   A2iARC_Unsigned month ;
   A2iARC_Unsigned year ;
} A2iARC_Date;

typedef struct _A2iARC_DateScore
{
   A2iARC_Date reco ;
   A2iARC_Unsigned score ;
} A2iARC_DateScore;

typedef struct _A2iARC_DateProb
{
   A2iARC_Date reco ;
   A2iARC_Float prob ;
} A2iARC_DateProb;

typedef struct _A2iARC_DateResult
{
   A2iARC_DateScore result ;
   struct
   {
      struct _A2iARC_DateProb * pTab ;
      unsigned long  cTab ;
   } prob ;
} A2iARC_DateResult;

typedef struct _A2iARC_Time
{
   A2iARC_Unsigned hour ;
   A2iARC_Unsigned minute ;
   A2iARC_Unsigned second ;
} A2iARC_Time;

typedef struct _A2iARC_TimeScore
{
   A2iARC_Time reco ;
   A2iARC_Unsigned score ;
} A2iARC_TimeScore;

typedef struct _A2iARC_TimeProb
{
   A2iARC_Time reco ;
   A2iARC_Float prob ;
} A2iARC_TimeProb;

typedef struct _A2iARC_TimeResult
{
   A2iARC_TimeScore result ;
   struct
   {
      struct _A2iARC_TimeProb * pTab ;
      unsigned long  cTab ;
   } prob ;
} A2iARC_TimeResult;

typedef struct _A2iARC_BooleanScore
{
   A2iARC_Boolean answer ;
   A2iARC_Unsigned score ;
} A2iARC_BooleanScore;

/**
* -- Adresses
*/
typedef A2iARC_Enum A2iARC_AddressLineType ;
#define _A2iARC_AddressLineType_Count 5
#define A2iARC_AddressLineType_Name 1UL   ///< 
#define A2iARC_AddressLineType_Destination 2UL   ///< 
#define A2iARC_AddressLineType_PhoneNum 3UL   ///< 
#define A2iARC_AddressLineType_CityZip 4UL   ///< 
#define A2iARC_AddressLineType_PlainText 5UL   ///< 

typedef struct _A2iARC_AddressLineScore
{
   struct
   {
      A2iARC_Char * pTab ;
      unsigned long  cTab ;
   } reco ;
   A2iARC_String wreco ;
   A2iARC_Unsigned score ;
   A2iARC_AddressLineType type ;
} A2iARC_AddressLineScore;

typedef struct _A2iARC_Address
{
   struct
   {
      struct _A2iARC_AddressLineScore * pTab ;
      unsigned long  cTab ;
   } lines ;
   A2iARC_Unsigned score ;
} A2iARC_Address;

/**
* ICR branch for a field recognition.
*/
typedef A2iARC_Enum A2iARC_FieldDecision ;
#define _A2iARC_FieldDecision_Count 4
#define A2iARC_FieldDecision_Handwritten 1UL   ///< The field was considered handwritten
#define A2iARC_FieldDecision_Printed 2UL   ///< The field was considered machine printed
#define A2iARC_FieldDecision_Empty 3UL   ///< The field was considered empty
#define A2iARC_FieldDecision_NotFound 4UL   ///< The field was not found

typedef struct _A2iARC_SingleCharacterScore
{
   A2iARC_String reco ;
   A2iARC_Unsigned score ;
   A2iARC_Box box ;
} A2iARC_SingleCharacterScore;

typedef struct _A2iARC_DetailedCharacterResults
{
   struct
   {
      struct _A2iARC_SingleCharacterScore * pTab ;
      unsigned long  cTab ;
   } characters ;
   A2iARC_Unsigned score ;
} A2iARC_DetailedCharacterResults;

typedef struct _A2iARC_SingleWordScore
{
   A2iARC_String reco ;
   A2iARC_Unsigned score ;
   A2iARC_Box box ;
} A2iARC_SingleWordScore;

typedef struct _A2iARC_DetailedWordResults
{
   struct
   {
      struct _A2iARC_SingleWordScore * pTab ;
      unsigned long  cTab ;
   } words ;
} A2iARC_DetailedWordResults;

typedef struct _A2iARC_CARResultLocation
{
   A2iARC_CleanedImageResult CARImage ;
   A2iARC_Box currencyLocation ;
   A2iARC_FieldDecision decision ;
} A2iARC_CARResultLocation;

typedef struct _A2iARC_LARResultLocation
{
   struct
   {
      struct _A2iARC_CleanedImageResult * pTab ;
      unsigned long  cTab ;
   } LARLines ;
   A2iARC_FieldDecision decision ;
   A2iARC_Float probEmpty ;
} A2iARC_LARResultLocation;

/**
* OCR Kind ? Special OCRs only usable on Characters Field (special character sets)
*/
typedef A2iARC_Enum A2iARC_PrintedOCRType ;
#define _A2iARC_PrintedOCRType_Count 11
#define A2iARC_PrintedOCRType_Generic 1UL   ///< 
#define A2iARC_PrintedOCRType_Fixed 2UL   ///< 
#define A2iARC_PrintedOCRType_Proportional 3UL   ///< 
#define A2iARC_PrintedOCRType_CMC7 4UL   ///< 
#define A2iARC_PrintedOCRType_MICR 5UL   ///< 
#define A2iARC_PrintedOCRType_A 6UL   ///< 
#define A2iARC_PrintedOCRType_B 7UL   ///< 
#define A2iARC_PrintedOCRType_A_Ext 8UL   ///< 
#define A2iARC_PrintedOCRType_B_Ext 9UL   ///< 
#define A2iARC_PrintedOCRType_7B 10UL   ///< 
#define A2iARC_PrintedOCRType_Stamp 11UL   ///< 

typedef A2iARC_Enum A2iARC_DefineFontSize ;
#define _A2iARC_DefineFontSize_Count 3
#define A2iARC_DefineFontSize_Default 1UL   ///< 
#define A2iARC_DefineFontSize_Fixed 2UL   ///< 
#define A2iARC_DefineFontSize_Auto 3UL   ///< 

/**
* -- All fields
*/
typedef A2iARC_Enum A2iARC_Optimisation ;
#define _A2iARC_Optimisation_Count 3
#define A2iARC_Optimisation_MaximizeSpeed 1UL   ///< 
#define A2iARC_Optimisation_MaximizeAccuracy 2UL   ///< 
#define A2iARC_Optimisation_OptimizeBinarization 3UL   ///< 

/**
* -- Characters
*/
typedef A2iARC_Enum A2iARC_CharacterFieldType ;
#define _A2iARC_CharacterFieldType_Count 3
#define A2iARC_CharacterFieldType_Digits 1UL   ///< 
#define A2iARC_CharacterFieldType_Alpha 2UL   ///< 
#define A2iARC_CharacterFieldType_AlphaNum 3UL   ///< 

/**
* -- Words
*/
typedef A2iARC_Enum A2iARC_VocabFormat ;
#define _A2iARC_VocabFormat_Count 5
#define A2iARC_VocabFormat_None 1UL   ///< 
#define A2iARC_VocabFormat_Simple 2UL   ///< No special compilation of the vocabulary
#define A2iARC_VocabFormat_File 3UL   ///< 
#define A2iARC_VocabFormat_SingleString 4UL   ///< 
#define A2iARC_VocabFormat_Persistent 5UL   ///< 

typedef struct _A2iARC_VocabItem
{
   A2iARC_String item ;
} A2iARC_VocabItem;

typedef A2iARC_Enum A2iARC_WordProcess ;
#define _A2iARC_WordProcess_Count 2
#define A2iARC_WordProcess_OneWordPerField 1UL   ///< Only one word in the field. The whole result is compared to vocabulary
///<                     entries.
#define A2iARC_WordProcess_NWordsPerField 2UL   ///< Multiple words per field. Each found word is compared to vocabulary
///<                     entries.

typedef A2iARC_Enum A2iARC_Alphabet ;
#define _A2iARC_Alphabet_Count 3
#define A2iARC_Alphabet_AutoDetect 1UL   ///< 
#define A2iARC_Alphabet_Latin 2UL   ///< 
#define A2iARC_Alphabet_Arabic 3UL   ///< 

typedef struct _A2iARC_WordInput
{
   A2iARC_VocabFormat vocabFormat ;
   union    // Depends on vocabFormat
   {
      struct // Case vocabFormat == Simple
      {
         struct
         {
            struct _A2iARC_VocabItem * pTab ;
            unsigned long  cTab ;
         } names ;
      } CaseSimple ;
      struct // Case vocabFormat == File
      {
         A2iARC_CharactersEncoding encoding ;
         A2iARC_File fileName ;
      } CaseFile ;
      struct // Case vocabFormat == SingleString
      {
         A2iARC_String words ;
      } CaseSingleString ;
      struct // Case vocabFormat == Persistent
      {
         A2iARC_PersistentId vocabularyId ;
      } CasePersistent ;
   } vocabFormatInfo ;
   A2iARC_WordProcess wordProcess ;
   union    // Depends on wordProcess
   {
      struct // Case wordProcess == NWordsPerField
      {
         A2iARC_Unsigned maxNumberOfWords ;
         A2iARC_Unsigned minNumberOfWords ;
      } CaseNWordsPerField ;
   } wordProcessInfo ;
   A2iARC_Boolean keepAllAnswers ;
   A2iARC_Boolean verification ;
   A2iARC_Boolean aliases ;
   A2iARC_CharacterFieldType charactersType ;
   A2iARC_Alphabet alphabet ;
   A2iARC_String coeffsFileName ;
} A2iARC_WordInput;

typedef struct _A2iARC_PossibleAnswers
{
   A2iARC_VocabFormat vocabFormat ;
   union    // Depends on vocabFormat
   {
      struct // Case vocabFormat == Simple
      {
         struct
         {
            struct _A2iARC_VocabItem * pTab ;
            unsigned long  cTab ;
         } names ;
      } CaseSimple ;
      struct // Case vocabFormat == File
      {
         A2iARC_CharactersEncoding encoding ;
         A2iARC_File fileName ;
      } CaseFile ;
      struct // Case vocabFormat == SingleString
      {
         A2iARC_String words ;
      } CaseSingleString ;
      struct // Case vocabFormat == Persistent
      {
         A2iARC_PersistentId vocabularyId ;
      } CasePersistent ;
   } vocabFormatInfo ;
   A2iARC_Boolean keepAllAnswers ;
} A2iARC_PossibleAnswers;

/**
* -- Dates
*/
typedef A2iARC_Enum A2iARC_DateFormat ;
#define _A2iARC_DateFormat_Count 7
#define A2iARC_DateFormat_DayMonthYear 1UL   ///< In full digits dates, the day is provided first.
#define A2iARC_DateFormat_MonthDayYear 2UL   ///< In full digits dates, the month is provided first.
#define A2iARC_DateFormat_YearMonthDay 3UL   ///< 
#define A2iARC_DateFormat_MonthYear 4UL   ///< 
#define A2iARC_DateFormat_YearMonth 5UL   ///< 
#define A2iARC_DateFormat_DayMonth 6UL   ///< 
#define A2iARC_DateFormat_MonthDay 7UL   ///< 

typedef A2iARC_Enum A2iARC_MonthFormat ;
#define _A2iARC_MonthFormat_Count 2
#define A2iARC_MonthFormat_Numerical 1UL   ///< 
#define A2iARC_MonthFormat_Word 2UL   ///< 

typedef A2iARC_Enum A2iARC_YearFormat ;
#define _A2iARC_YearFormat_Count 2
#define A2iARC_YearFormat_FourDigits 1UL   ///< 
#define A2iARC_YearFormat_TwoDigits 2UL   ///< 

typedef A2iARC_Enum A2iARC_PayeeNameLines ;
#define _A2iARC_PayeeNameLines_Count 9
#define A2iARC_PayeeNameLines_AutoFindMulti 1UL   ///< 
#define A2iARC_PayeeNameLines_AutoFindFirst 2UL   ///< 
#define A2iARC_PayeeNameLines_AutoFindTwoFirst 3UL   ///< 
#define A2iARC_PayeeNameLines_AutoFindThreeFirst 4UL   ///< 
#define A2iARC_PayeeNameLines_FirstOnly 5UL   ///< 
#define A2iARC_PayeeNameLines_FirstTwoOnly 6UL   ///< 
#define A2iARC_PayeeNameLines_FirstThreeOnly 7UL   ///< 
#define A2iARC_PayeeNameLines_FirstFourOnly 8UL   ///< 
#define A2iARC_PayeeNameLines_WholeBlock 9UL   ///< 

typedef A2iARC_Enum A2iARC_PrintedLARType ;
#define _A2iARC_PrintedLARType_Count 2
#define A2iARC_PrintedLARType_Words 1UL   ///< 
#define A2iARC_PrintedLARType_Digits 2UL   ///< 

/**
* Address field
*/
typedef A2iARC_Enum A2iARC_AddressSemFieldType ;
#define _A2iARC_AddressSemFieldType_Count 28
#define A2iARC_AddressSemFieldType_Civility 1UL   ///< 
#define A2iARC_AddressSemFieldType_Company 2UL   ///< 
#define A2iARC_AddressSemFieldType_LastName 3UL   ///< 
#define A2iARC_AddressSemFieldType_MiddleName 4UL   ///< 
#define A2iARC_AddressSemFieldType_FirstName 5UL   ///< 
#define A2iARC_AddressSemFieldType_Name 6UL   ///< FirstName and Name in any order
#define A2iARC_AddressSemFieldType_FirstMiddleLastName 7UL   ///< 
#define A2iARC_AddressSemFieldType_LastFirstMiddleName 8UL   ///< 
#define A2iARC_AddressSemFieldType_AppartNumber 9UL   ///< 
#define A2iARC_AddressSemFieldType_Building 10UL   ///< 
#define A2iARC_AddressSemFieldType_HouseNumber 11UL   ///< 
#define A2iARC_AddressSemFieldType_StreetType 12UL   ///< 
#define A2iARC_AddressSemFieldType_StreetName 13UL   ///< 
#define A2iARC_AddressSemFieldType_Street 14UL   ///< Street type and StreetName
#define A2iARC_AddressSemFieldType_Destination 15UL   ///< Building, House Number, Street type and StreetName City Zip
#define A2iARC_AddressSemFieldType_City 16UL   ///< 
#define A2iARC_AddressSemFieldType_CityZip 17UL   ///< CityZip
#define A2iARC_AddressSemFieldType_State 18UL   ///< 
#define A2iARC_AddressSemFieldType_CityZipState 19UL   ///< CityZip and State in any order
#define A2iARC_AddressSemFieldType_ShortZip 20UL   ///< 
#define A2iARC_AddressSemFieldType_LongZip 21UL   ///< 
#define A2iARC_AddressSemFieldType_FullZip 22UL   ///< 
#define A2iARC_AddressSemFieldType_POBox 23UL   ///< 
#define A2iARC_AddressSemFieldType_Address 24UL   ///< Complete address except name
#define A2iARC_AddressSemFieldType_NameAndAddress 25UL   ///< 
#define A2iARC_AddressSemFieldType_Country 26UL   ///< 
#define A2iARC_AddressSemFieldType_CityState 27UL   ///< 
#define A2iARC_AddressSemFieldType_Department 28UL   ///< 

typedef A2iARC_Enum A2iARC_BarcodeType ;
#define _A2iARC_BarcodeType_Count 5
#define A2iARC_BarcodeType_AutoDetect 1UL   ///< 
#define A2iARC_BarcodeType_Code39 2UL   ///< 
#define A2iARC_BarcodeType_Interleaved 3UL   ///< 
#define A2iARC_BarcodeType_Codabar 4UL   ///< 
#define A2iARC_BarcodeType_Code128 5UL   ///< 

typedef A2iARC_Enum A2iARC_ControlType ;
#define _A2iARC_ControlType_Count 3
#define A2iARC_ControlType_None 1UL   ///< 
#define A2iARC_ControlType_Luhn 2UL   ///< 
#define A2iARC_ControlType_Mod97 3UL   ///< 

typedef struct _A2iARC_AddressGeoSemFieldInput
{
   A2iARC_AddressSemFieldType semFieldType ;
} A2iARC_AddressGeoSemFieldInput;

typedef struct _A2iARC_GeometricFieldInput
{
   A2iARC_Box box ;
   A2iARC_Location location ;
   A2iARC_WriteType writeType ;
   A2iARC_HandwritingStyle handwritingStyle ;
   A2iARC_PrintedOCRType printedOCRType ;
   A2iARC_Orientation orientation ;
   A2iARC_Unsigned maxChar ;
   A2iARC_Unsigned minChar ;
   A2iARC_BoxingType boxingType ;
   A2iARC_Boolean cleaningDeactivated ;
   A2iARC_Unsigned maxLines ;
   A2iARC_Unsigned minLines ;
   struct
   {
      struct _A2iARC_Box * pTab ;
      unsigned long  cTab ;
   } lineBoxes ;
} A2iARC_GeometricFieldInput;

typedef struct _A2iARC_AddressGeoFieldInput
{
   A2iARC_GeometricFieldInput geometric ;
   struct
   {
      struct _A2iARC_AddressGeoSemFieldInput * pTab ;
      unsigned long  cTab ;
   } semantics ;
} A2iARC_AddressGeoFieldInput;

typedef struct _A2iARC_AddressFieldContext
{
   A2iARC_AddressSemFieldType semField ;
   A2iARC_WordInput context ;
} A2iARC_AddressFieldContext;

typedef struct _A2iARC_AddressFieldResultRequest
{
   A2iARC_AddressSemFieldType semField ;
   A2iARC_UserId Id ;
} A2iARC_AddressFieldResultRequest;

typedef struct _A2iARC_AddressBookEntry
{
   struct
   {
      struct _A2iARC_AddressBookCell * pTab ;
      unsigned long  cTab ;
   } columns ;
} A2iARC_AddressBookEntry;

typedef struct _A2iARC_AddressBookFormat
{
   struct
   {
      A2iARC_AddressSemFieldType * pTab ;
      unsigned long  cTab ;
   } columns ;
} A2iARC_AddressBookFormat;

typedef struct _A2iARC_AddressBook
{
   A2iARC_DataFormat dataFormat ;
   A2iARC_AddressBookFormat format ;
   union    // Depends on dataFormat
   {
      struct // Case dataFormat == Simple
      {
         struct
         {
            struct _A2iARC_AddressBookEntry * pTab ;
            unsigned long  cTab ;
         } entries ;
      } CaseSimple ;
      struct // Case dataFormat == File
      {
         A2iARC_CharactersEncoding encoding ;
         A2iARC_File fileName ;
      } CaseFile ;
   } dataFormatInfo ;
   A2iARC_Country country ;
} A2iARC_AddressBook;

typedef struct _A2iARC_CustomAddressInput
{
   struct
   {
      struct _A2iARC_AddressGeoFieldInput * pTab ;
      unsigned long  cTab ;
   } geoFields ;
   A2iARC_PersistentId postalDictionary ;
   A2iARC_DataPersistence addressBookPersistence ;
   union    // Depends on addressBookPersistence
   {
      struct // Case addressBookPersistence == Persistent
      {
         A2iARC_PersistentId addressBookId ;
      } CasePersistent ;
      struct // Case addressBookPersistence == Request
      {
         A2iARC_AddressBook addressBook ;
      } CaseRequest ;
   } addressBookInfo ;
   struct
   {
      struct _A2iARC_AddressFieldContext * pTab ;
      unsigned long  cTab ;
   } fieldContexts ;
   struct
   {
      struct _A2iARC_AddressFieldResultRequest * pTab ;
      unsigned long  cTab ;
   } fieldResultRequests ;
} A2iARC_CustomAddressInput;

typedef struct _A2iARC_AddressBookCell
{
   A2iARC_String cell ;
} A2iARC_AddressBookCell;

typedef struct _A2iARC_AddressGeoFieldOutput
{
   struct
   {
      struct _A2iARC_DetailedCharacterResults * pTab ;
      unsigned long  cTab ;
   } detailedCharacterResults ;
   A2iARC_CleanedImageResult image ;
} A2iARC_AddressGeoFieldOutput;

typedef struct _A2iARC_CustomAddressItemOutput
{
   A2iARC_AddressSemFieldType type ;
   A2iARC_UserId Id ;
   A2iARC_String reco ;
   A2iARC_Float prob ;
} A2iARC_CustomAddressItemOutput;

typedef struct _A2iARC_CustomAddressScore
{
   A2iARC_Unsigned score ;
   struct
   {
      struct _A2iARC_CustomAddressItemOutput * pTab ;
      unsigned long  cTab ;
   } items ;
} A2iARC_CustomAddressScore;

typedef struct _A2iARC_CustomAddressResult
{
   A2iARC_Float prob ;
   struct
   {
      struct _A2iARC_CustomAddressItemOutput * pTab ;
      unsigned long  cTab ;
   } items ;
} A2iARC_CustomAddressResult;

typedef struct _A2iARC_CustomAddressOutput
{
   struct
   {
      struct _A2iARC_AddressGeoFieldOutput * pTab ;
      unsigned long  cTab ;
   } geoResults ;
   A2iARC_CustomAddressScore result ;
   struct
   {
      struct _A2iARC_CustomAddressResult * pTab ;
      unsigned long  cTab ;
   } finalList ;
} A2iARC_CustomAddressOutput;

typedef A2iARC_Enum A2iARC_FieldType ;
#define _A2iARC_FieldType_Count 16
#define A2iARC_FieldType_Amount 1UL   ///< An amount 
#define A2iARC_FieldType_Characters 2UL   ///< A string of characters
#define A2iARC_FieldType_CheckBox 3UL   ///< A box, or a system of box matrix with or without a cross in it
#define A2iARC_FieldType_HWDetecting 4UL   ///< A field where some writing should be detected (Signature also)
#define A2iARC_FieldType_Word 5UL   ///< Isolated hand writing recognition words
#define A2iARC_FieldType_Date 6UL   ///< A string which contains a date
#define A2iARC_FieldType_Address 7UL   ///< An address block
#define A2iARC_FieldType_LitteralAmount 8UL   ///< Literal amount
#define A2iARC_FieldType_CustomAddress 9UL   ///< 
#define A2iARC_FieldType_ZIP 10UL   ///< 
#define A2iARC_FieldType_PhoneNumber 11UL   ///< 
#define A2iARC_FieldType_RIB 12UL   ///< 
#define A2iARC_FieldType_CreditCardNumber 13UL   ///< 
#define A2iARC_FieldType_SocialSecurityNumber 14UL   ///< 
#define A2iARC_FieldType_Time 15UL   ///< A string which contains a time in format "hh:mm:ss", "hh,mm,ss" or
///<                     "hh-mm-ss"
#define A2iARC_FieldType_Barcode 16UL   ///< 

typedef struct _A2iARC_FieldInfo
{
   A2iARC_Country country ;
   A2iARC_WriteType writeType ;
   A2iARC_HandwritingStyle handwritingStyle ;
   A2iARC_WritingPeriod writingPeriod ;
   A2iARC_PrintedOCRType printedOCRType ;
   A2iARC_DefineFontSize defineFontSize ;
   A2iARC_Unsigned fontSize ;
   A2iARC_Orientation orientation ;
   A2iARC_Unsigned maxChar ;
   A2iARC_Unsigned minChar ;
   A2iARC_Boolean cleaningDeactivated ;
   A2iARC_BoxingType boxingType ;
   A2iARC_ImageQuality fieldImageQuality ;
   A2iARC_Boolean printedTextPresent ;
   A2iARC_Boolean sideObjectsPresent ;
   A2iARC_Boolean holesInCharacters ;
   A2iARC_String preconfiguredParameters ;
   A2iARC_InvertedText invertedText ;
   A2iARC_String regexpFilter ;
   A2iARC_Boolean disableRegexp ;
   A2iARC_ControlType controlType ;
   A2iARC_Boolean acceptEmptyResults ;
} A2iARC_FieldInfo;

typedef struct _A2iARC_StringScoreLocation
{
   A2iARC_String reco ;
   A2iARC_Unsigned score ;
   A2iARC_Boolean accepted ;
   A2iARC_CleanedImageResult extractedImage ;
} A2iARC_StringScoreLocation;

typedef struct _A2iARC_GenderScoreLocation
{
   A2iARC_Gender reco ;
   A2iARC_Unsigned score ;
   A2iARC_CleanedImageResult extractedImage ;
} A2iARC_GenderScoreLocation;

typedef struct _A2iARC_CountryScoreLocation
{
   A2iARC_Country reco ;
   A2iARC_Unsigned score ;
   A2iARC_Boolean accepted ;
   A2iARC_CleanedImageResult extractedImage ;
} A2iARC_CountryScoreLocation;

typedef struct _A2iARC_AmountScoreLocation
{
   A2iARC_Unsigned reco ;
   A2iARC_Unsigned score ;
   A2iARC_Boolean accepted ;
   A2iARC_CleanedImageResult extractedImage ;
} A2iARC_AmountScoreLocation;

typedef struct _A2iARC_DateScoreLocation
{
   A2iARC_Date reco ;
   A2iARC_Unsigned score ;
   A2iARC_Boolean accepted ;
   A2iARC_CleanedImageResult extractedImage ;
} A2iARC_DateScoreLocation;

typedef struct _A2iARC_FeatureInput
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned threshold ;
} A2iARC_FeatureInput;

typedef struct _A2iARC_RIBResult
{
   A2iARC_StringScoreLocation result ;
   A2iARC_StringScoreLocation codeBanque ;
   A2iARC_StringScoreLocation codeGuichet ;
   A2iARC_StringScoreLocation numeroDeCompte ;
   A2iARC_StringScoreLocation cle ;
} A2iARC_RIBResult;

typedef A2iARC_Enum A2iARC_CheckBoxType ;
#define _A2iARC_CheckBoxType_Count 6
#define A2iARC_CheckBoxType_Rectangular 1UL   ///< 
#define A2iARC_CheckBoxType_Round 2UL   ///< 
#define A2iARC_CheckBoxType_FromModel 3UL   ///< 
#define A2iARC_CheckBoxType_AutoDetect 4UL   ///< 
#define A2iARC_CheckBoxType_Oval 5UL   ///< 
#define A2iARC_CheckBoxType_Parenthesis 6UL   ///< 

typedef A2iARC_Enum A2iARC_CheckBoxSize ;
#define _A2iARC_CheckBoxSize_Count 4
#define A2iARC_CheckBoxSize_Small 1UL   ///< 
#define A2iARC_CheckBoxSize_Middle 2UL   ///< 
#define A2iARC_CheckBoxSize_Large 3UL   ///< 
#define A2iARC_CheckBoxSize_Any 4UL   ///< 

typedef A2iARC_Enum A2iARC_CheckBoxSpacing ;
#define _A2iARC_CheckBoxSpacing_Count 2
#define A2iARC_CheckBoxSpacing_Fixed 1UL   ///< 
#define A2iARC_CheckBoxSpacing_Variable 2UL   ///< 

typedef A2iARC_Enum A2iARC_CentPresence ;
#define _A2iARC_CentPresence_Count 3
#define A2iARC_CentPresence_Always 1UL   ///< 
#define A2iARC_CentPresence_Never 2UL   ///< 
#define A2iARC_CentPresence_Unknown 3UL   ///< 

typedef A2iARC_Enum A2iARC_MultipleTicks ;
#define _A2iARC_MultipleTicks_Count 3
#define A2iARC_MultipleTicks_Yes 1UL   ///< 
#define A2iARC_MultipleTicks_No 2UL   ///< 
#define A2iARC_MultipleTicks_SingleMandatory 3UL   ///< 

typedef A2iARC_Enum A2iARC_SubValueType ;
#define _A2iARC_SubValueType_Count 16
#define A2iARC_SubValueType_FirstName 1UL   ///< 
#define A2iARC_SubValueType_LastName 2UL   ///< 
#define A2iARC_SubValueType_MiddleInitial 3UL   ///< 
#define A2iARC_SubValueType_Suffix 4UL   ///< 
#define A2iARC_SubValueType_Civility 5UL   ///< 
#define A2iARC_SubValueType_Name 6UL   ///< 
#define A2iARC_SubValueType_City 7UL   ///< 
#define A2iARC_SubValueType_State 8UL   ///< 
#define A2iARC_SubValueType_Zip 9UL   ///< 
#define A2iARC_SubValueType_Street 10UL   ///< 
#define A2iARC_SubValueType_HouseNumber 11UL   ///< 
#define A2iARC_SubValueType_StreetAddress 12UL   ///< 
#define A2iARC_SubValueType_CityState 13UL   ///< 
#define A2iARC_SubValueType_CityZip 14UL   ///< 
#define A2iARC_SubValueType_CityStateZip 15UL   ///< 
#define A2iARC_SubValueType_PostalAddress 16UL   ///< 

typedef struct _A2iARC_FieldInput
{
   A2iARC_UserId Id ;
   A2iARC_Box box ;
   A2iARC_Location location ;
   A2iARC_Boolean looseLocation ;
   A2iARC_FieldType fieldType ;
   A2iARC_FieldInfo fieldInfo ;
   A2iARC_Optimisation optimisation ;
   A2iARC_Unsigned threshold ;
   A2iARC_File internalParameterFile ;
   union    // Depends on fieldType
   {
      struct // Case fieldType == Amount
      {
         A2iARC_Currency currency ;
         A2iARC_Boolean currencySignLocator ;
         A2iARC_LocationRuleType amountShiftRelativeToCurrencySign ;
         A2iARC_CentPresence centPresence ;
         A2iARC_Box centPos ;
      } CaseAmount ;
      struct // Case fieldType == Characters
      {
         A2iARC_CharacterFieldType charactersType ;
         A2iARC_Boolean spaceDetection ;
      } CaseCharacters ;
      struct // Case fieldType == Word
      {
         A2iARC_WordInput wordInput ;
      } CaseWord ;
      struct // Case fieldType == SocialSecurityNumber
      {
         A2iARC_PossibleAnswers possibleAnswers ;
      } CaseSocialSecurityNumber ;
      struct // Case fieldType == Date
      {
         A2iARC_Boolean definePivotYear ;
         A2iARC_Unsigned pivotYear ;
         A2iARC_Boolean acceptPostDated ;
         A2iARC_Date referenceDate ;
         A2iARC_DateFormat format ;
         A2iARC_MonthFormat monthFormat ;
         A2iARC_YearFormat yearFormat ;
         A2iARC_Date minimum ;
         A2iARC_Date maximum ;
         A2iARC_String dateFormat ;
      } CaseDate ;
      struct // Case fieldType == CheckBox
      {
         A2iARC_Boolean isPrintedBox ;
         A2iARC_CheckBoxType type ;
         A2iARC_Unsigned count ;
         A2iARC_Unsigned count_cols ;
         A2iARC_Unsigned count_lines ;
         A2iARC_MultipleTicks multipleTicks ;
         A2iARC_CheckBoxSize size ;
         A2iARC_CheckBoxSpacing spacing ;
      } CaseCheckBox ;
      struct // Case fieldType == HWDetecting
      {
         A2iARC_Boolean signatureDetection ;
      } CaseHWDetecting ;
      struct // Case fieldType == CustomAddress
      {
         A2iARC_CustomAddressInput address ;
      } CaseCustomAddress ;
      struct // Case fieldType == LitteralAmount
      {
         A2iARC_PrintedLARType printedLARType ;
      } CaseLitteralAmount ;
      struct // Case fieldType == RIB
      {
         A2iARC_Unsigned detailedThreshold ;
      } CaseRIB ;
      struct // Case fieldType == Barcode
      {
         A2iARC_BarcodeType barcodeType ;
      } CaseBarcode ;
   } fieldTypeInfo ;
} A2iARC_FieldInput;

typedef struct _A2iARC_CheckBoxLineResult
{
   struct
   {
      struct _A2iARC_BooleanScore * pTab ;
      unsigned long  cTab ;
   } cellResults ;
} A2iARC_CheckBoxLineResult;

typedef struct _A2iARC_FieldOutput
{
   A2iARC_FieldInput input ;
   A2iARC_Boolean accepted ;
   union    // Depends on input.fieldType
   {
      struct // Case input.fieldType == Amount
      {
         A2iARC_AmountScore result ;
         struct
         {
            struct _A2iARC_AmountProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
         struct
         {
            struct _A2iARC_AmountProb * pTab ;
            unsigned long  cTab ;
         } roundList ;
         struct
         {
            struct _A2iARC_AmountProb * pTab ;
            unsigned long  cTab ;
         } fractionalList ;
      } CaseAmount ;
      struct // Case input.fieldType == Characters
      {
         A2iARC_CharactersScore result ;
         struct
         {
            struct _A2iARC_CharactersProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
      } CaseCharacters ;
      struct // Case input.fieldType == Word
      {
         A2iARC_WordResult word ;
      } CaseWord ;
      struct // Case input.fieldType == HWDetecting
      {
         A2iARC_BooleanScore result ;
      } CaseHWDetecting ;
      struct // Case input.fieldType == Date
      {
         A2iARC_DateResult date ;
      } CaseDate ;
      struct // Case input.fieldType == CheckBox
      {
         A2iARC_BooleanScore result ;
         A2iARC_Unsigned tickCount ;
         A2iARC_Unsigned index ;
         struct
         {
            struct _A2iARC_CheckBoxLineResult * pTab ;
            unsigned long  cTab ;
         } lineResults ;
      } CaseCheckBox ;
      struct // Case input.fieldType == Address
      {
         A2iARC_Address result ;
      } CaseAddress ;
      struct // Case input.fieldType == CustomAddress
      {
         A2iARC_CustomAddressOutput address ;
      } CaseCustomAddress ;
      struct // Case input.fieldType == LitteralAmount
      {
         A2iARC_AmountScore result ;
         struct
         {
            struct _A2iARC_AmountProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
         struct
         {
            struct _A2iARC_AmountProb * pTab ;
            unsigned long  cTab ;
         } roundList ;
         struct
         {
            struct _A2iARC_AmountProb * pTab ;
            unsigned long  cTab ;
         } fractionalList ;
         struct
         {
            struct _A2iARC_DetailedWordResults * pTab ;
            unsigned long  cTab ;
         } detailedWordResults ;
      } CaseLitteralAmount ;
      struct // Case input.fieldType == ZIP
      {
         A2iARC_CharactersScore result ;
         struct
         {
            struct _A2iARC_CharactersProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
      } CaseZIP ;
      struct // Case input.fieldType == PhoneNumber
      {
         A2iARC_CharactersScore result ;
         struct
         {
            struct _A2iARC_CharactersProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
      } CasePhoneNumber ;
      struct // Case input.fieldType == RIB
      {
         A2iARC_RIBResult result ;
      } CaseRIB ;
      struct // Case input.fieldType == CreditCardNumber
      {
         A2iARC_CharactersScore result ;
         struct
         {
            struct _A2iARC_CharactersProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
      } CaseCreditCardNumber ;
      struct // Case input.fieldType == SocialSecurityNumber
      {
         A2iARC_CharactersScore result ;
         struct
         {
            struct _A2iARC_CharactersProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
      } CaseSocialSecurityNumber ;
      struct // Case input.fieldType == Time
      {
         A2iARC_TimeResult time ;
      } CaseTime ;
      struct // Case input.fieldType == Barcode
      {
         A2iARC_CharactersScore result ;
         struct
         {
            struct _A2iARC_CharactersProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
      } CaseBarcode ;
   } fieldTypeInfo ;
   A2iARC_CleanedImageResult extractedField ;
   A2iARC_Boolean modelCleaned ;
   A2iARC_FieldDecision decision ;
   struct
   {
      struct _A2iARC_DetailedCharacterResults * pTab ;
      unsigned long  cTab ;
   } detailedCharacterResults ;
   A2iARC_Buffer internalResults ;
} A2iARC_FieldOutput;

/**
* Check Inputs
*/
typedef A2iARC_Enum A2iARC_ToRecognize ;
#define _A2iARC_ToRecognize_Count 3
#define A2iARC_ToRecognize_Yes 1UL   ///< 
#define A2iARC_ToRecognize_No 2UL   ///< 
#define A2iARC_ToRecognize_WithDetection 3UL   ///< 

typedef A2iARC_Enum A2iARC_CodelineFont ;
#define _A2iARC_CodelineFont_Count 5
#define A2iARC_CodelineFont_E13B 1UL   ///< 
#define A2iARC_CodelineFont_CMC7 2UL   ///< 
#define A2iARC_CodelineFont_OCRA 3UL   ///< 
#define A2iARC_CodelineFont_OCRB 4UL   ///< 
#define A2iARC_CodelineFont_AutoDetect 5UL   ///< 

typedef struct _A2iARC_Line1RegexFilters
{
   A2iARC_String account ;
   A2iARC_String sery ;
   A2iARC_String checkNumber ;
} A2iARC_Line1RegexFilters;

typedef struct _A2iARC_CheckFieldsInput
{
   A2iARC_ToRecognize lar ;
   A2iARC_Boolean CarLarDifferenceDetection ;
   A2iARC_Boolean CarLarCouponDifferenceDetection ;
   A2iARC_ToRecognize nameOfPayee ;
   A2iARC_WordInput possibleNameOfPayee ;
   A2iARC_PrintedOCRType nameOfPayeeOCRType ;
   A2iARC_PayeeNameLines payeeNameLines ;
   A2iARC_Box payeeNameZone ;
   A2iARC_Unsigned nameOfPayeeThreshold ;
   A2iARC_ToRecognize date ;
   A2iARC_Unsigned dateThreshold ;
   A2iARC_Boolean definePivotYear ;
   A2iARC_Unsigned pivotYear ;
   A2iARC_Boolean acceptPostDated ;
   A2iARC_MonthFormat monthFormat ;
   A2iARC_Date referenceDate ;
   A2iARC_Date minimum ;
   A2iARC_Date maximum ;
   A2iARC_Boolean address ;
   A2iARC_Unsigned addressThreshold ;
   A2iARC_WordInput payerNameVocabulary ;
   A2iARC_Boolean payeeAddress ;
   A2iARC_Unsigned payeeAddressThreshold ;
   A2iARC_DataPersistence postalDictionaryPersistence ;
   union    // Depends on postalDictionaryPersistence
   {
      struct // Case postalDictionaryPersistence == Persistent
      {
         A2iARC_PersistentId dictionaryId ;
      } CasePersistent ;
      struct // Case postalDictionaryPersistence == Request
      {
         A2iARC_PostalDictionary dictionary ;
      } CaseRequest ;
   } postalDictionary ;
   A2iARC_DataPersistence addressBookPersistence ;
   union    // Depends on addressBookPersistence
   {
      struct // Case addressBookPersistence == Persistent
      {
         A2iARC_PersistentId addressBookId ;
      } CasePersistent ;
      struct // Case addressBookPersistence == Request
      {
         A2iARC_AddressBook addressBook ;
      } CaseRequest ;
   } addressBookInfo ;
   A2iARC_Boolean signature ;
   A2iARC_Unsigned signatureThreshold ;
   A2iARC_Boolean codeline ;
   A2iARC_Unsigned codelineThreshold ;
   A2iARC_Boolean codelineAmount ;
   A2iARC_CodelineFont codelineFont ;
   A2iARC_Boolean codelineSpaceDetection ;
   A2iARC_Boolean blackPayee ;
   A2iARC_Boolean blackPayer ;
   A2iARC_WordInput blackListVocabulary ;
   A2iARC_Boolean rlmc ;
   A2iARC_Unsigned rlmcThreshold ;
   A2iARC_Boolean memoline ;
   A2iARC_Unsigned memolineThreshold ;
   A2iARC_WordInput memolineVocabulary ;
   A2iARC_Boolean checkNumber ;
   A2iARC_Unsigned checkNumberThreshold ;
   A2iARC_Boolean bankBranch ;
   A2iARC_Unsigned bankBranchThreshold ;
   A2iARC_Unsigned accountNumberThreshold ;
   A2iARC_Boolean accountNumber ;
   A2iARC_Boolean line1 ;
   A2iARC_Unsigned line1Threshold ;
   A2iARC_Line1RegexFilters line1RegexFilters ;
   A2iARC_Boolean boaDate ;
   A2iARC_Unsigned boaDateThreshold ;
   A2iARC_Boolean cpfCnpj ;
   A2iARC_Unsigned cpfCnpjThreshold ;
   A2iARC_Unsigned rearAccountNumberThreshold ;
   A2iARC_Boolean rearAccountNumber ;
} A2iARC_CheckFieldsInput;

/**
* Says how LAR is used on a check
*/
typedef A2iARC_Enum A2iARC_LarCallPolicy ;
#define _A2iARC_LarCallPolicy_Count 2
#define A2iARC_LarCallPolicy_Auto 1UL   ///< LAR is called accordingly to A2iA 's internal variables
#define A2iARC_LarCallPolicy_LarCallThreshold 2UL   ///< LAR is used depending on the value of larCallThreshold

typedef A2iARC_Enum A2iARC_US_Check_Kind ;
#define _A2iARC_US_Check_Kind_Count 2
#define A2iARC_US_Check_Kind_Personal 1UL   ///< 
#define A2iARC_US_Check_Kind_Business 2UL   ///< 

typedef struct _A2iARC_CheckStockDetailedEnablingFlags
{
   A2iARC_Boolean CAPos ;
   A2iARC_Boolean Sign ;
   A2iARC_Boolean LAPos ;
   A2iARC_Boolean Payee ;
   A2iARC_Boolean TTOO ;
   A2iARC_Boolean Date ;
   A2iARC_Boolean KeyDate ;
   A2iARC_Boolean Address ;
   A2iARC_Boolean CheckNum ;
   A2iARC_Boolean ShRst ;
   A2iARC_Boolean GeomGen ;
   A2iARC_Boolean ChNumMICR ;
   A2iARC_Boolean HwrLAR ;
   A2iARC_Boolean SgDetect ;
   A2iARC_Boolean PayorName ;
} A2iARC_CheckStockDetailedEnablingFlags;

typedef struct _A2iARC_CheckStockDescr
{
   A2iARC_Buffer CIR ;
   struct
   {
      A2iARC_Buffer * pTab ;
      unsigned long  cTab ;
   } CIRs ;
   A2iARC_Country country ;
   A2iARC_Boolean multipleModels ;
   A2iARC_Boolean detailedEnabling ;
   A2iARC_CheckStockDetailedEnablingFlags detailedEnablingFlags ;
} A2iARC_CheckStockDescr;

typedef struct _A2iARC_CheckStockFieldResult
{
   A2iARC_Boolean checkedForValidation ;
   A2iARC_Float fraudProb ;
} A2iARC_CheckStockFieldResult;

typedef struct _A2iARC_CheckStockTestResult
{
   A2iARC_Unsigned fraudScore ;
   A2iARC_Unsigned bestCandidateIndex ;
   A2iARC_Unsigned bestCIRIndex ;
   A2iARC_CheckStockFieldResult courtesyAmount ;
   A2iARC_CheckStockFieldResult currencySign ;
   A2iARC_CheckStockFieldResult legalAmount ;
   A2iARC_CheckStockFieldResult payeeName ;
   A2iARC_CheckStockFieldResult payToTheOrderOfKeyword ;
   A2iARC_CheckStockFieldResult date ;
   A2iARC_CheckStockFieldResult checkNumber ;
   A2iARC_CheckStockFieldResult dateKeyword ;
   A2iARC_CheckStockFieldResult payerAddress ;
   A2iARC_CheckStockFieldResult smoothRasterMatching ;
   A2iARC_CheckStockFieldResult genGeometryFeatures ;
   A2iARC_CheckStockFieldResult checkNumMICR ;
   A2iARC_CheckStockFieldResult handwritingLAR ;
   A2iARC_CheckStockFieldResult sgDetect ;
   A2iARC_CheckStockFieldResult payorsName ;
} A2iARC_CheckStockTestResult;

typedef struct _A2iARC_CheckInvalidityDetails
{
   A2iARC_Boolean returnAtLeastOneInvalidityCause ;
   A2iARC_Boolean CAR ;
   A2iARC_Boolean LAR ;
   A2iARC_Boolean Signature ;
   A2iARC_Boolean PayeeName ;
   A2iARC_Boolean Date ;
   A2iARC_Boolean Codeline ;
   A2iARC_Boolean PayorsNameAndAddress ;
   A2iARC_Boolean PayeeEndorsement ;
   A2iARC_Boolean BankOfFirstDepositEndorsement ;
   A2iARC_Boolean TransitEndorsement ;
   A2iARC_Boolean PODEndorsement ;
   A2iARC_Boolean PlaceOfIssue ;
   A2iARC_Boolean RearAccountNumber ;
   A2iARC_Boolean Memoline ;
   A2iARC_Boolean PayorsBank ;
   A2iARC_Unsigned invalidityThreshold ;
   A2iARC_Boolean considerStampAsPayeeEndorsment ;
} A2iARC_CheckInvalidityDetails;

typedef struct _A2iARC_CheckDetectDocumentTypeDetails
{
   A2iARC_Boolean PersonalCheck ;
   A2iARC_Boolean BusinessCheck ;
   A2iARC_Boolean MoneyOrder ;
   A2iARC_Boolean DepositTicket ;
   A2iARC_Boolean CashInOutTicket ;
   A2iARC_Boolean TravellersCheck ;
   A2iARC_Boolean SaveBond ;
   A2iARC_Boolean PAD ;
   A2iARC_Boolean CashiersCheck ;
   A2iARC_Boolean BankDraft ;
} A2iARC_CheckDetectDocumentTypeDetails;

typedef A2iARC_Enum A2iARC_EndorsementCleaning ;
#define _A2iARC_EndorsementCleaning_Count 3
#define A2iARC_EndorsementCleaning_Yes 1UL   ///< 
#define A2iARC_EndorsementCleaning_No 2UL   ///< 
#define A2iARC_EndorsementCleaning_BestGuess 3UL   ///< 

typedef A2iARC_Enum A2iARC_BillPolicy ;
#define _A2iARC_BillPolicy_Count 2
#define A2iARC_BillPolicy_Recognition 1UL   ///< 
#define A2iARC_BillPolicy_Verification 2UL   ///< 

/**
* Payment document type
*/
typedef A2iARC_Enum A2iARC_PaymentDocumentType ;
#define _A2iARC_PaymentDocumentType_Count 11
#define A2iARC_PaymentDocumentType_Check 1UL   ///< 
#define A2iARC_PaymentDocumentType_PersonalCheck 2UL   ///< Personal check
#define A2iARC_PaymentDocumentType_BusinessCheck 3UL   ///< Business check
#define A2iARC_PaymentDocumentType_MoneyOrder 4UL   ///< Money order
#define A2iARC_PaymentDocumentType_DepositTicket 5UL   ///< Deposit ticket
#define A2iARC_PaymentDocumentType_CashInOutTicket 6UL   ///< Cash in/out Ticket
#define A2iARC_PaymentDocumentType_TravellersCheck 7UL   ///< Traveller's check
#define A2iARC_PaymentDocumentType_SaveBond 8UL   ///< US saving bonds
#define A2iARC_PaymentDocumentType_PAD 9UL   ///< US pre-authorized draft
#define A2iARC_PaymentDocumentType_CashiersCheck 10UL   ///< Cashiers check
#define A2iARC_PaymentDocumentType_BankDraft 11UL   ///< Bank draft

typedef struct _A2iARC_CheckInput
{
   A2iARC_Country country ;
   A2iARC_Currency currency ;
   A2iARC_WriteType writeType ;
   A2iARC_Boolean amountRecognition ;
   A2iARC_Unsigned threshold ;
   A2iARC_LarCallPolicy larCallPolicy ;
   A2iARC_Float larCallThreshold ;
   A2iARC_LarCallPolicy prnLarCallPolicy ;
   A2iARC_Float prnLarCallThreshold ;
   A2iARC_PrintedLARType printedLARType ;
   A2iARC_CheckFieldsInput fields ;
   A2iARC_Boolean invalidityDetection ;
   A2iARC_Boolean rearInvalidityDetection ;
   A2iARC_CheckInvalidityDetails invalidityDetails ;
   A2iARC_CheckDetectDocumentTypeDetails documentTypePresence ;
   A2iARC_Optimisation Optimisation ;
   A2iARC_Boolean MoneyOrder ;
   A2iARC_PaymentDocumentType paymentDocumentType ;
   A2iARC_Boolean detectDocumentType ;
   A2iARC_InvertedText InvertedText ;
   A2iARC_EndorsementCleaning endorsementCleaning ;
   A2iARC_Boolean rearFrontOrderCorrection ;
   A2iARC_Boolean autoLocate ;
   struct
   {
      A2iARC_Unsigned * pTab ;
      unsigned long  cTab ;
   } bills ;
   A2iARC_BillPolicy billPolicy ;
   union    // Depends on country
   {
      struct // Case country == HK
      {
         A2iARC_Boolean activateChineseLAR ;
      } CaseHK ;
      struct // Case country == SG
      {
         A2iARC_Boolean activateChineseLAR ;
      } CaseSG ;
      struct // Case country == US
      {
         A2iARC_US_Check_Kind kind ;
      } CaseUS ;
      struct // Case country == France
      {
         A2iARC_String CMC7 ;
      } CaseFrance ;
   } countryInfo ;
} A2iARC_CheckInput;

/**
* ICR branch for Check recognition
*/
typedef A2iARC_Enum A2iARC_CheckBranch ;
#define _A2iARC_CheckBranch_Count 2
#define A2iARC_CheckBranch_Handwritten 1UL   ///< The cheque was considered handwritten
#define A2iARC_CheckBranch_Printed 2UL   ///< The cheque was considered printed

typedef struct _A2iARC_Line1FieldsResultLocation
{
   A2iARC_CleanedImageResult comp ;
   A2iARC_CleanedImageResult bank ;
   A2iARC_CleanedImageResult agency ;
   A2iARC_CleanedImageResult dv ;
   A2iARC_CleanedImageResult c1 ;
   A2iARC_CleanedImageResult c1_complementary ;
   A2iARC_CleanedImageResult account ;
   A2iARC_CleanedImageResult account_complementary ;
   A2iARC_CleanedImageResult c2 ;
   A2iARC_CleanedImageResult sery ;
   A2iARC_CleanedImageResult checkNumber ;
   A2iARC_CleanedImageResult c3 ;
} A2iARC_Line1FieldsResultLocation;

typedef struct _A2iARC_FieldsResultLocation
{
   A2iARC_CleanedImageResult dateImage ;
   A2iARC_FieldDecision dateDecision ;
   A2iARC_Float probDateEmpty ;
   A2iARC_CleanedImageResult addressImage ;
   A2iARC_FieldDecision addressDecision ;
   A2iARC_CleanedImageResult payeeAddressImage ;
   A2iARC_FieldDecision payeeAddressDecision ;
   A2iARC_CleanedImageResult signatureImage ;
   A2iARC_CleanedImageResult payeeNameImage ;
   A2iARC_FieldDecision payeeNameDecision ;
   A2iARC_Float probPayeeNameEmpty ;
   A2iARC_CleanedImageResult codelineImage ;
   A2iARC_FieldDecision codelineDecision ;
   A2iARC_CleanedImageResult rlmcImage ;
   A2iARC_FieldDecision rlmcDecision ;
   A2iARC_CleanedImageResult checkNumberImage ;
   A2iARC_FieldDecision checkNumberDecision ;
   A2iARC_CleanedImageResult memolineImage ;
   A2iARC_FieldDecision memolineDecision ;
   A2iARC_CleanedImageResult bankBranchImage ;
   A2iARC_FieldDecision bankBranchDecision ;
   A2iARC_CleanedImageResult accountNumberImage ;
   A2iARC_FieldDecision accountNumberDecision ;
   struct
   {
      struct _A2iARC_CleanedImageResult * pTab ;
      unsigned long  cTab ;
   } cpfCnpjImages ;
   A2iARC_CleanedImageResult rearAccountNumberImage ;
   A2iARC_FieldDecision rearAccountNumberDecision ;
   A2iARC_CleanedImageResult boaDateImage ;
   A2iARC_FieldDecision boaDateDecision ;
   A2iARC_Line1FieldsResultLocation line1Images ;
} A2iARC_FieldsResultLocation;

typedef struct _A2iARC_FieldsCharacterResults
{
   struct
   {
      struct _A2iARC_DetailedCharacterResults * pTab ;
      unsigned long  cTab ;
   } payeeName ;
   struct
   {
      struct _A2iARC_DetailedCharacterResults * pTab ;
      unsigned long  cTab ;
   } codeline ;
} A2iARC_FieldsCharacterResults;

typedef struct _A2iARC_CheckInvalidityCauses
{
   A2iARC_Unsigned invalidityScore ;
   A2iARC_Unsigned rearInvalidityScore ;
   A2iARC_Boolean noCAR ;
   A2iARC_Boolean noLAR ;
   A2iARC_Boolean noDate ;
   A2iARC_Boolean noPayeeName ;
   A2iARC_Boolean noSignature ;
   A2iARC_Boolean noCodeline ;
   A2iARC_Boolean noPayorsNameAndAddress ;
   A2iARC_Boolean noMemoline ;
   A2iARC_Boolean noPayorsBank ;
   A2iARC_Boolean noPayeeEndorsement ;
   A2iARC_Boolean noBankOfFirstDepositEndorsement ;
   A2iARC_Boolean noTransitEndorsement ;
   A2iARC_Boolean noPODEndorsement ;
   A2iARC_Boolean noPlaceOfIssue ;
   A2iARC_Boolean noRearAccountNumber ;
   A2iARC_Boolean CAR_LAR_Different ;
   A2iARC_Unsigned noCARScore ;
   A2iARC_Unsigned noLARScore ;
   A2iARC_Unsigned noDateScore ;
   A2iARC_Unsigned noPayeeNameScore ;
   A2iARC_Unsigned noSignatureScore ;
   A2iARC_Unsigned noCodelineScore ;
   A2iARC_Unsigned noPayorsNameAndAddressScore ;
   A2iARC_Unsigned noMemolineScore ;
   A2iARC_Unsigned noPayorsBankScore ;
   A2iARC_Unsigned noPayeeEndorsementScore ;
   A2iARC_Unsigned noBankOfFirstDepositEndorsementScore ;
   A2iARC_Unsigned noTransitEndorsementScore ;
   A2iARC_Unsigned noPODEndorsementScore ;
   A2iARC_Unsigned noPlaceOfIssueScore ;
   A2iARC_Unsigned noRearAccountNumberScore ;
} A2iARC_CheckInvalidityCauses;

typedef struct _A2iARC_Line1Results
{
   A2iARC_CharactersResult comp ;
   A2iARC_CharactersResult bank ;
   A2iARC_CharactersResult agency ;
   A2iARC_CharactersResult dv ;
   A2iARC_CharactersResult c1 ;
   A2iARC_CharactersResult c1_complementary ;
   A2iARC_CharactersResult account ;
   A2iARC_CharactersResult account_complementary ;
   A2iARC_CharactersResult c2 ;
   A2iARC_CharactersResult sery ;
   A2iARC_CharactersResult checkNumber ;
   A2iARC_CharactersResult c3 ;
} A2iARC_Line1Results;

typedef struct _A2iARC_CheckOutput
{
   A2iARC_Boolean accepted ;
   A2iARC_Boolean rearFrontOrderCorrected ;
   A2iARC_CheckBranch branch ;
   A2iARC_PaymentDocumentType paymentDocumentType ;
   A2iARC_Unsigned paymentDocumentTypeScore ;
   A2iARC_AmountScore result ;
   A2iARC_Boolean isLarCalled ;
   A2iARC_CARResultLocation CARResult ;
   A2iARC_LARResultLocation LARResult ;
   A2iARC_Unsigned CarLarDifferenceScore ;
   A2iARC_Unsigned CarLarCouponDifferenceScore ;
   A2iARC_FieldsResultLocation fieldsResult ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } finalList ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } roundList ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } fractionalList ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } carList ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } larList ;
   A2iARC_CharactersResult nameOfPayee ;
   A2iARC_WordResult payeeName ;
   A2iARC_WordResult blackListPayee ;
   A2iARC_DateResult date ;
   A2iARC_Address address ;
   A2iARC_Address payeeAddress ;
   A2iARC_WordResult blackListPayer ;
   A2iARC_BooleanScore signature ;
   A2iARC_CharactersResult codeline ;
   A2iARC_CharactersResult rlmc ;
   A2iARC_FieldsCharacterResults characterResults ;
   A2iARC_CheckInvalidityCauses invalidity ;
   A2iARC_WordResult memoline ;
   A2iARC_CharactersResult checkNumber ;
   A2iARC_Address bankBranch ;
   A2iARC_CharactersResult accountNumber ;
   A2iARC_Line1Results line1 ;
   A2iARC_DateResult boaDate ;
   struct
   {
      struct _A2iARC_CharactersResult * pTab ;
      unsigned long  cTab ;
   } cpfCnpj ;
   A2iARC_CharactersResult rearAccountNumber ;
} A2iARC_CheckOutput;

/**
* Operator field
*/
typedef A2iARC_Enum A2iARC_OperatorType ;
#define _A2iARC_OperatorType_Count 2
#define A2iARC_OperatorType_Concatenation 1UL   ///< The result is a concatenation of several fields
#define A2iARC_OperatorType_Merge 2UL   ///< The result is a merge of several sources of information

typedef struct _A2iARC_Operand
{
   A2iARC_String name ;
} A2iARC_Operand;

typedef struct _A2iARC_OperatorInput
{
   A2iARC_UserId id ;
   A2iARC_OperatorType operatorType ;
   A2iARC_Unsigned threshold ;
   struct
   {
      struct _A2iARC_Operand * pTab ;
      unsigned long  cTab ;
   } operands ;
} A2iARC_OperatorInput;

typedef struct _A2iARC_OperatorOutput
{
   A2iARC_OperatorInput input ;
   A2iARC_Boolean accepted ;
   A2iARC_FieldType fieldType ;
   union    // Depends on fieldType
   {
      struct // Case fieldType == Amount
      {
         A2iARC_AmountScore result ;
         struct
         {
            struct _A2iARC_AmountProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
      } CaseAmount ;
      struct // Case fieldType == Characters
      {
         A2iARC_CharactersScore result ;
         struct
         {
            struct _A2iARC_CharactersProb * pTab ;
            unsigned long  cTab ;
         } finalList ;
      } CaseCharacters ;
   } fieldTypeInfo ;
} A2iARC_OperatorOutput;

typedef struct _A2iARC_GuideMark
{
   A2iARC_Box box ;
} A2iARC_GuideMark;

/**
* Zone to be cleaned 
*/
typedef A2iARC_Enum A2iARC_CleanZoneType ;
#define _A2iARC_CleanZoneType_Count 2
#define A2iARC_CleanZoneType_PrePrinted 1UL   ///< 
#define A2iARC_CleanZoneType_Line 2UL   ///< 

typedef struct _A2iARC_CleanZone
{
   A2iARC_CleanZoneType type ;
   A2iARC_Orientation orientation ;
   A2iARC_Box box ;
} A2iARC_CleanZone;

typedef struct _A2iARC_CustomInput
{
   struct
   {
      struct _A2iARC_GuideMark * pTab ;
      unsigned long  cTab ;
   } guideMarks ;
   struct
   {
      struct _A2iARC_CleanZone * pTab ;
      unsigned long  cTab ;
   } cleanZones ;
   struct
   {
      struct _A2iARC_FieldInput * pTab ;
      unsigned long  cTab ;
   } fields ;
   struct
   {
      struct _A2iARC_OperatorInput * pTab ;
      unsigned long  cTab ;
   } operators ;
} A2iARC_CustomInput;

typedef struct _A2iARC_CustomOutput
{
   struct
   {
      struct _A2iARC_FieldOutput * pTab ;
      unsigned long  cTab ;
   } fields ;
   struct
   {
      struct _A2iARC_OperatorOutput * pTab ;
      unsigned long  cTab ;
   } operators ;
} A2iARC_CustomOutput;

typedef struct _A2iARC_DongleQueryOutput
{
   A2iARC_Unsigned DocsHour ;
   A2iARC_Unsigned DocsRemaining ;
   A2iARC_Unsigned DocsUsed ;
   A2iARC_Unsigned DaysRemaining ;
   A2iARC_Unsigned DaysUsed ;
} A2iARC_DongleQueryOutput;

typedef struct _A2iARC_DevInput
{
   A2iARC_File gaFile ;
   A2iARC_File pythonFile ;
   struct
   {
      struct _A2iARC_CustomInput * pTab ;
      unsigned long  cTab ;
   } input ;
   struct
   {
      struct _A2iARC_CustomOutput * pTab ;
      unsigned long  cTab ;
   } intermediateResults ;
} A2iARC_DevInput;

typedef struct _A2iARC_DevOutput
{
   struct
   {
      struct _A2iARC_CustomOutput * pTab ;
      unsigned long  cTab ;
   } results ;
} A2iARC_DevOutput;

typedef struct _A2iARC_DepositTicketInput
{
   A2iARC_Country country ;
   A2iARC_Unsigned threshold ;
   A2iARC_CustomInput custom ;
} A2iARC_DepositTicketInput;

typedef struct _A2iARC_DepositTicketOutput
{
   A2iARC_Boolean accepted ;
   A2iARC_AmountScore result ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } finalList ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } roundList ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } fractionalList ;
   A2iARC_CARResultLocation resultLocation ;
   A2iARC_CustomOutput custom ;
} A2iARC_DepositTicketOutput;

/**
* Document
*/
typedef A2iARC_Enum A2iARC_DocumentType ;
#define _A2iARC_DocumentType_Count 12
#define A2iARC_DocumentType_Check 1UL   ///< Any kind of cheque
#define A2iARC_DocumentType_Custom 2UL   ///< A customized document : a set of fields with coordinates and attributes
#define A2iARC_DocumentType_SingleField 3UL   ///< One single field to recognize
#define A2iARC_DocumentType_DepositTicket 4UL   ///< Document for which a specific reco algorithms are available 
#define A2iARC_DocumentType_NoProcess 5UL   ///< The ICR system should not handle this document
#define A2iARC_DocumentType_DongleQuery 6UL   ///< Query about the dongle status
#define A2iARC_DocumentType_AutoDefine 7UL   ///< 
#define A2iARC_DocumentType_DevReco 8UL   ///< 
#define A2iARC_DocumentType_CheckStockTrain 9UL   ///< 
#define A2iARC_DocumentType_CheckStockTest 10UL   ///< 
#define A2iARC_DocumentType_Specific 11UL   ///< 
#define A2iARC_DocumentType_Transaction 12UL   ///< 

typedef struct _A2iARC_VerboseDetails
{
   A2iARC_Boolean preprocessedImage ;
   A2iARC_Boolean extractedImages ;
   A2iARC_Boolean characterResults ;
   A2iARC_Boolean wordResults ;
   A2iARC_Boolean originalWords ;
} A2iARC_VerboseDetails;

typedef A2iARC_Enum A2iARC_MarkerType ;
#define _A2iARC_MarkerType_Count 2
#define A2iARC_MarkerType_Keyword 1UL   ///< 
#define A2iARC_MarkerType_Logo 2UL   ///< 

typedef struct _A2iARC_Marker
{
   A2iARC_UserId id ;
   A2iARC_MarkerType markerType ;
   union    // Depends on markerType
   {
      struct // Case markerType == Keyword
      {
         A2iARC_String keyword ;
      } CaseKeyword ;
      struct // Case markerType == Logo
      {
         A2iARC_Image LogoImage ;
      } CaseLogo ;
   } markerTypeInfo ;
   A2iARC_Box seekingZone ;
   A2iARC_Boolean inverted ;
} A2iARC_Marker;

typedef A2iARC_Enum A2iARC_PageProcess ;
#define _A2iARC_PageProcess_Count 3
#define A2iARC_PageProcess_ClassifyAndProcess 1UL   ///< 
#define A2iARC_PageProcess_ClassifyOnly 2UL   ///< 
#define A2iARC_PageProcess_ProcessOnly 3UL   ///< 

typedef A2iARC_Enum A2iARC_AutoDefineProcess ;
#define _A2iARC_AutoDefineProcess_Count 3
#define A2iARC_AutoDefineProcess_ClassifyAndProcess 1UL   ///< 
#define A2iARC_AutoDefineProcess_ClassifyOnly 2UL   ///< 
#define A2iARC_AutoDefineProcess_ProcessOnly 3UL   ///< 

typedef A2iARC_Enum A2iARC_SpecificDocumentSubType ;
#define _A2iARC_SpecificDocumentSubType_Count 5
#define A2iARC_SpecificDocumentSubType_RIB 1UL   ///< 
#define A2iARC_SpecificDocumentSubType_Passport 2UL   ///< 
#define A2iARC_SpecificDocumentSubType_AutorisationDePrelevement 3UL   ///< 
#define A2iARC_SpecificDocumentSubType_Giro 4UL   ///< 
#define A2iARC_SpecificDocumentSubType_BorLcr 5UL   ///< 

typedef struct _A2iARC_GiroInput
{
   A2iARC_Currency currency ;
   A2iARC_WriteType writeType ;
   A2iARC_FeatureInput amount ;
   A2iARC_FeatureInput date ;
   A2iARC_Date referenceDate ;
   A2iARC_Date minimumDate ;
   A2iARC_Date maximumDate ;
   A2iARC_FeatureInput beneficiaryAccount ;
   A2iARC_FeatureInput originatorAccount ;
   A2iARC_FeatureInput communication ;
} A2iARC_GiroInput;

typedef struct _A2iARC_RIBInput
{
   A2iARC_Boolean enable ;
   A2iARC_Unsigned threshold ;
   A2iARC_Unsigned detailedThreshold ;
   A2iARC_PossibleAnswers possibleGuichet ;
   A2iARC_PossibleAnswers possibleEtablissement ;
} A2iARC_RIBInput;

typedef struct _A2iARC_RIBAddressInput
{
   A2iARC_Boolean enable ;
} A2iARC_RIBAddressInput;

typedef struct _A2iARC_SpecificInput
{
   A2iARC_Country country ;
   A2iARC_Optimisation optimisation ;
   A2iARC_SpecificDocumentSubType subType ;
   union    // Depends on subType
   {
      struct // Case subType == AutorisationDePrelevement
      {
         A2iARC_RIBInput RIB ;
         A2iARC_FeatureInput address ;
         A2iARC_FeatureInput name ;
         A2iARC_FeatureInput date ;
         A2iARC_FeatureInput numeroNationalDEmetteur ;
         A2iARC_PersistentId postalDictionary ;
         A2iARC_WordInput nameVocabulary ;
      } CaseAutorisationDePrelevement ;
      struct // Case subType == RIB
      {
         A2iARC_RIBInput RIB ;
         A2iARC_FeatureInput IBAN ;
         A2iARC_FeatureInput address ;
         A2iARC_PersistentId postalDictionary ;
      } CaseRIB ;
      struct // Case subType == Passport
      {
         A2iARC_PersistentId persistentPassportId ;
         A2iARC_Boolean GlobalEnabling ;
         A2iARC_FeatureInput Name ;
         A2iARC_FeatureInput FirstName ;
         A2iARC_FeatureInput LastName ;
         A2iARC_FeatureInput Gender ;
         A2iARC_FeatureInput BirthDate ;
         A2iARC_FeatureInput PlaceOfBirth ;
         A2iARC_FeatureInput IssueDate ;
         A2iARC_FeatureInput ValidUntil ;
      } CasePassport ;
      struct // Case subType == Giro
      {
         A2iARC_GiroInput giro ;
      } CaseGiro ;
      struct // Case subType == BorLcr
      {
         A2iARC_Boolean globalEnabling ;
         A2iARC_FeatureInput amount ;
         A2iARC_FeatureInput date ;
         A2iARC_RIBInput RIB ;
         A2iARC_FeatureInput reference ;
      } CaseBorLcr ;
   } subTypeInfo ;
} A2iARC_SpecificInput;

typedef struct _A2iARC_TransactionItem
{
   A2iARC_Buffer outputBuffer ;
   A2iARC_UserId fieldId ;
} A2iARC_TransactionItem;

typedef struct _A2iARC_TransactionInput
{
   A2iARC_Boolean deactivateAutobalancing ;
   A2iARC_Boolean useExplicitThreshold ;
   A2iARC_Unsigned threshold ;
   A2iARC_TransactionItem Sum ;
   struct
   {
      struct _A2iARC_TransactionItem * pTab ;
      unsigned long  cTab ;
   } terms ;
} A2iARC_TransactionInput;

typedef A2iARC_Enum A2iARC_RequestDictionaryType ;
#define _A2iARC_RequestDictionaryType_Count 1
#define A2iARC_RequestDictionaryType_String 1UL   ///< 

typedef struct _A2iARC_RequestDictionary
{
   A2iARC_RequestDictionaryType dictionaryType ;
   union    // Depends on dictionaryType
   {
      struct // Case dictionaryType == String
      {
         struct
         {
            A2iARC_String * pTab ;
            unsigned long  cTab ;
         } key ;
      } CaseString ;
   } dictionaryTypeInfo ;
} A2iARC_RequestDictionary;

typedef struct _A2iARC_RequestParameters
{
   A2iARC_RequestDictionary keys ;
} A2iARC_RequestParameters;

typedef A2iARC_Enum A2iARC_ClickCountHistoryUsage ;
#define _A2iARC_ClickCountHistoryUsage_Count 2
#define A2iARC_ClickCountHistoryUsage_None 1UL   ///< 
#define A2iARC_ClickCountHistoryUsage_PerPage 2UL   ///< 

typedef struct _A2iARC_Input
{
   A2iARC_String documentName ;
   A2iARC_String labelString ;
   A2iARC_String paramString ;
   A2iARC_Boolean verbose ;
   A2iARC_VerboseDetails verboseDetails ;
   A2iARC_Boolean preprocessOnly ;
   A2iARC_ImagePreprocessing imagePreprocessing ;
   A2iARC_ImageConversion originalImageConversion ;
   struct
   {
      struct _A2iARC_ImagePreprocessing * pTab ;
      unsigned long  cTab ;
   } detailedPagesPreprocessing ;
   A2iARC_Image image ;
   struct
   {
      struct _A2iARC_Image * pTab ;
      unsigned long  cTab ;
   } additionalPages ;
   A2iARC_InternalProcessing internalProcessing ;
   struct
   {
      struct _A2iARC_Marker * pTab ;
      unsigned long  cTab ;
   } markers ;
   A2iARC_DocumentType documentType ;
   A2iARC_File internalParameterFile ;
   A2iARC_DataPersistence modelPersistence ;
   union    // Depends on modelPersistence
   {
      struct // Case modelPersistence == Request
      {
         A2iARC_DocumentModel model ;
      } CaseRequest ;
      struct // Case modelPersistence == Persistent
      {
         A2iARC_PersistentId modelId ;
      } CasePersistent ;
   } modelPersistenceInfo ;
   union    // Depends on documentType
   {
      struct // Case documentType == Check
      {
         A2iARC_CheckInput check ;
         A2iARC_CustomInput custom ;
      } CaseCheck ;
      struct // Case documentType == Custom
      {
         A2iARC_CustomInput custom ;
      } CaseCustom ;
      struct // Case documentType == SingleField
      {
         A2iARC_FieldInput field ;
      } CaseSingleField ;
      struct // Case documentType == DepositTicket
      {
         A2iARC_DepositTicketInput depositTicket ;
      } CaseDepositTicket ;
      struct // Case documentType == AutoDefine
      {
         A2iARC_PrivatePersistentId documentClassifier ;
         A2iARC_Unsigned classificationThreshold ;
         A2iARC_AutoDefineProcess processRequest ;
         A2iARC_String modelToProcess ;
      } CaseAutoDefine ;
      struct // Case documentType == DevReco
      {
         A2iARC_PersistentId devInputId ;
         A2iARC_DevInput devInput ;
      } CaseDevReco ;
      struct // Case documentType == CheckStockTrain
      {
         A2iARC_CheckStockDescr descr ;
      } CaseCheckStockTrain ;
      struct // Case documentType == CheckStockTest
      {
         A2iARC_CheckStockDescr descr ;
      } CaseCheckStockTest ;
      struct // Case documentType == Specific
      {
         A2iARC_SpecificInput specificInput ;
      } CaseSpecific ;
      struct // Case documentType == Transaction
      {
         A2iARC_TransactionInput transactionInput ;
      } CaseTransaction ;
   } documentTypeInfo ;
   A2iARC_ImageQualityInput imageQuality ;
   A2iARC_Dir configFolder ;
   A2iARC_Unsigned availableTime ;
   A2iARC_Boolean AccumulateTransaction ;
   A2iARC_RequestParameters parameters ;
   A2iARC_ClickCountHistoryUsage clickCountHistoryUsage ;
   union    // Depends on clickCountHistoryUsage
   {
      struct // Case clickCountHistoryUsage == PerPage
      {
         A2iARC_String imageClickCounter ;
         struct
         {
            A2iARC_String * pTab ;
            unsigned long  cTab ;
         } additionalImageClickCounters ;
      } CasePerPage ;
   } clickCountHistoryUsageInfo ;
   A2iARC_CharactersEncoding encoding ;
   A2iARC_CharactersEncoding outputEncoding ;
} A2iARC_Input;

typedef struct _A2iARC_ClassificationResult
{
   A2iARC_String className ;
   A2iARC_Float prob ;
} A2iARC_ClassificationResult;

typedef struct _A2iARC_ClassificationScore
{
   A2iARC_String className ;
   A2iARC_Unsigned score ;
} A2iARC_ClassificationScore;

typedef struct _A2iARC_MarkerLocation
{
   A2iARC_UserId id ;
   A2iARC_Boolean found ;
   A2iARC_Box box ;
} A2iARC_MarkerLocation;

typedef struct _A2iARC_DateResultLocation
{
   A2iARC_DateResult reco ;
   A2iARC_CleanedImageResult extractedImage ;
} A2iARC_DateResultLocation;

typedef struct _A2iARC_BankAccount
{
   A2iARC_RIBResult rib ;
   A2iARC_StringScoreLocation iban ;
   A2iARC_StringScoreLocation bic ;
   A2iARC_StringScoreLocation name ;
} A2iARC_BankAccount;

typedef struct _A2iARC_AmountResult
{
   A2iARC_AmountScore result ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } finalList ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } roundList ;
   struct
   {
      struct _A2iARC_AmountProb * pTab ;
      unsigned long  cTab ;
   } fractionalList ;
} A2iARC_AmountResult;

typedef struct _A2iARC_AmountResultLocation
{
   A2iARC_AmountResult reco ;
   A2iARC_CleanedImageResult extractedImage ;
} A2iARC_AmountResultLocation;

typedef struct _A2iARC_GiroOutput
{
   A2iARC_AmountScoreLocation amount ;
   A2iARC_DateScoreLocation date ;
   A2iARC_StringScoreLocation beneficiaryAccount ;
   A2iARC_StringScoreLocation originatorAccount ;
   A2iARC_StringScoreLocation communication ;
} A2iARC_GiroOutput;

typedef struct _A2iARC_SpecificOutput
{
   A2iARC_SpecificDocumentSubType subType ;
   union    // Depends on subType
   {
      struct // Case subType == RIB
      {
         A2iARC_RIBResult RIB ;
         A2iARC_StringScoreLocation IBAN ;
         A2iARC_Address address ;
         A2iARC_CleanedImageResult addressLocation ;
      } CaseRIB ;
      struct // Case subType == AutorisationDePrelevement
      {
         A2iARC_RIBResult RIB ;
         A2iARC_Address address ;
         A2iARC_DateScoreLocation date ;
         A2iARC_StringScoreLocation NumeroNationalDEmetteur ;
         A2iARC_StringScoreLocation name ;
      } CaseAutorisationDePrelevement ;
      struct // Case subType == Passport
      {
         A2iARC_StringScoreLocation Name ;
         A2iARC_StringScoreLocation FirstName ;
         A2iARC_StringScoreLocation LastName ;
         A2iARC_GenderScoreLocation Gender ;
         A2iARC_DateScoreLocation BirthDate ;
         A2iARC_StringScoreLocation PlaceOfBirth ;
         A2iARC_DateScoreLocation IssueDate ;
         A2iARC_DateScoreLocation ValidUntil ;
      } CasePassport ;
      struct // Case subType == Giro
      {
         A2iARC_GiroOutput giro ;
      } CaseGiro ;
      struct // Case subType == BorLcr
      {
         A2iARC_AmountScoreLocation amount ;
         A2iARC_DateScoreLocation date ;
         A2iARC_RIBResult RIB ;
         A2iARC_StringScoreLocation reference ;
      } CaseBorLcr ;
   } subTypeInfo ;
} A2iARC_SpecificOutput;

typedef struct _A2iARC_TransactionOutput
{
   struct
   {
      struct _A2iARC_Output * pTab ;
      unsigned long  cTab ;
   } terms ;
   struct
   {
      struct _A2iARC_Output * pTab ;
      unsigned long  cTab ;
   } Sum ;
   A2iARC_Unsigned score ;
} A2iARC_TransactionOutput;

typedef struct _A2iARC_ClassificationResults
{
   A2iARC_Unsigned identificationScore ;
   A2iARC_String identificationModelName ;
} A2iARC_ClassificationResults;

typedef struct _A2iARC_Output
{
   A2iARC_Input input ;
   A2iARC_Image convertedOriginalImage ;
   A2iARC_Err status ;
   A2iARC_String statusContext ;
   A2iARC_Unsigned millisec_delay ;
   A2iARC_Unsigned process_time ;
   A2iARC_Image preprocessedImage ;
   struct
   {
      struct _A2iARC_Image * pTab ;
      unsigned long  cTab ;
   } additionalPreprocessedPages ;
   A2iARC_Float orientationCorrection ;
   A2iARC_String hostName ;
   A2iARC_Unsigned clickCount ;
   struct
   {
      struct _A2iARC_MarkerLocation * pTab ;
      unsigned long  cTab ;
   } markersLocation ;
   union    // Depends on input.documentType
   {
      struct // Case input.documentType == Check
      {
         A2iARC_CheckOutput check ;
         A2iARC_CustomOutput custom ;
      } CaseCheck ;
      struct // Case input.documentType == Custom
      {
         A2iARC_CustomOutput custom ;
      } CaseCustom ;
      struct // Case input.documentType == SingleField
      {
         A2iARC_FieldOutput field ;
      } CaseSingleField ;
      struct // Case input.documentType == DepositTicket
      {
         A2iARC_DepositTicketOutput depositTicket ;
      } CaseDepositTicket ;
      struct // Case input.documentType == DongleQuery
      {
         A2iARC_DongleQueryOutput result ;
      } CaseDongleQuery ;
      struct // Case input.documentType == DevReco
      {
         A2iARC_DevOutput devOutput ;
      } CaseDevReco ;
      struct // Case input.documentType == CheckStockTrain
      {
         A2iARC_Buffer CIR ;
      } CaseCheckStockTrain ;
      struct // Case input.documentType == CheckStockTest
      {
         A2iARC_CheckStockTestResult result ;
      } CaseCheckStockTest ;
      struct // Case input.documentType == Specific
      {
         A2iARC_SpecificOutput specificOutput ;
      } CaseSpecific ;
      struct // Case input.documentType == Transaction
      {
         A2iARC_TransactionOutput transactionOutput ;
      } CaseTransaction ;
   } documentTypeInfo ;
   A2iARC_ImageQualityResults imageQuality ;
   A2iARC_ClassificationResults classificationResults ;
   A2iARC_Buffer OutputResult ;
   union    // Depends on input.clickCountHistoryUsage
   {
      struct // Case input.clickCountHistoryUsage == PerPage
      {
         A2iARC_String imageClickCounter ;
         struct
         {
            A2iARC_String * pTab ;
            unsigned long  cTab ;
         } additionalImageClickCounters ;
      } CasePerPage ;
   } clickCountHistoryUsageInfo ;
} A2iARC_Output;

typedef struct _A2iARC_MICR
{
   A2iARC_String OnUs ;
   A2iARC_String RTN ;
   A2iARC_String AN ;
   A2iARC_String TC ;
} A2iARC_MICR;

typedef A2iARC_Enum A2iARC_KeyType ;
#define _A2iARC_KeyType_Count 3
#define A2iARC_KeyType_None 1UL   ///< 
#define A2iARC_KeyType_String 2UL   ///< 
#define A2iARC_KeyType_MICR 3UL   ///< 

typedef struct _A2iARC_KeyDefinition
{
   A2iARC_Unsigned index ;
   A2iARC_Input doc ;
   A2iARC_File imageName ;
   A2iARC_UserId label ;
} A2iARC_KeyDefinition;

/**
* The persistent vocabulary definitions are not finalized yet. They may change.
*/
typedef A2iARC_Enum A2iARC_PersistentVocabularyType ;
#define _A2iARC_PersistentVocabularyType_Count 3
#define A2iARC_PersistentVocabularyType_Simple 1UL   ///< 
#define A2iARC_PersistentVocabularyType_File 2UL   ///< 
#define A2iARC_PersistentVocabularyType_SingleString 3UL   ///< 

typedef struct _A2iARC_PersistentVocabulary
{
   A2iARC_PersistentVocabularyType type ;
   A2iARC_String coeffsFileName ;
   A2iARC_WriteType writeType ;
   A2iARC_HandwritingStyle handwritingStyle ;
   A2iARC_Country country ;
   A2iARC_Boolean aliases ;
   union    // Depends on type
   {
      struct // Case type == Simple
      {
         struct
         {
            struct _A2iARC_VocabItem * pTab ;
            unsigned long  cTab ;
         } names ;
      } CaseSimple ;
      struct // Case type == File
      {
         A2iARC_CharactersEncoding encoding ;
         A2iARC_File fileName ;
      } CaseFile ;
      struct // Case type == SingleString
      {
         A2iARC_String names ;
      } CaseSingleString ;
   } typeInfo ;
   A2iARC_Boolean extendedAliases ;
} A2iARC_PersistentVocabulary;

typedef struct _A2iARC_ModelDefinition
{
   A2iARC_KeyDefinition model ;
   A2iARC_File imageModelName ;
} A2iARC_ModelDefinition;

typedef struct _A2iARC_DocumentClassifierParameters
{
   A2iARC_ImagePreprocessing preprocessing ;
   A2iARC_KeyDefinition preprocessingAndExtraction ;
} A2iARC_DocumentClassifierParameters;

typedef A2iARC_Enum A2iARC_DocumentClassifierType ;
#define _A2iARC_DocumentClassifierType_Count 2
#define A2iARC_DocumentClassifierType_StructuredDocs 1UL   ///< 
#define A2iARC_DocumentClassifierType_AllDocs 2UL   ///< 

typedef struct _A2iARC_DocumentClassifier
{
   A2iARC_DocumentClassifierType documentClassifierType ;
   A2iARC_DocumentClassifierParameters parameters ;
   struct
   {
      struct _A2iARC_ModelDefinition * pTab ;
      unsigned long  cTab ;
   } models ;
   A2iARC_ModelDefinition reject ;
   A2iARC_Unsigned classificationThreshold ;
   A2iARC_Boolean importedRAD ;
   A2iARC_File IRDBRun ;
   A2iARC_File IRDBBuild ;
   A2iARC_File IRDBTemp ;
} A2iARC_DocumentClassifier;

typedef struct _A2iARC_SpecificPassport
{
   A2iARC_AddressBook book ;
} A2iARC_SpecificPassport;

typedef A2iARC_Enum A2iARC_PersistentDataType ;
#define _A2iARC_PersistentDataType_Count 9
#define A2iARC_PersistentDataType_Vocabulary 1UL   ///< 
#define A2iARC_PersistentDataType_PostalDictionary 2UL   ///< 
#define A2iARC_PersistentDataType_Identification 3UL   ///< 
#define A2iARC_PersistentDataType_DocumentModel 4UL   ///< 
#define A2iARC_PersistentDataType_AddressBook 5UL   ///< 
#define A2iARC_PersistentDataType_DocumentClassifier 6UL   ///< 
#define A2iARC_PersistentDataType_DevRecoDocument 7UL   ///< 
#define A2iARC_PersistentDataType_SpecificPassport 8UL   ///< 
#define A2iARC_PersistentDataType_Python 9UL   ///< 

typedef struct _A2iARC_ModelImageDefinition
{
   A2iARC_File filename ;
   A2iARC_Unsigned index ;
} A2iARC_ModelImageDefinition;

typedef struct _A2iARC_SimpleModelDefinition
{
   A2iARC_String name ;
   A2iARC_Unsigned index ;
   struct
   {
      struct _A2iARC_ModelImageDefinition * pTab ;
      unsigned long  cTab ;
   } filenames ;
} A2iARC_SimpleModelDefinition;

typedef struct _A2iARC_PersistentResult
{
   A2iARC_PersistentDataType type ;
   A2iARC_String Id ;
   union    // Depends on type
   {
      struct // Case type == DocumentClassifier
      {
         struct
         {
            struct _A2iARC_SimpleModelDefinition * pTab ;
            unsigned long  cTab ;
         } trainedModels ;
      } CaseDocumentClassifier ;
      struct // Case type == DocumentModel
      {
         A2iARC_Image cleanedImage ;
      } CaseDocumentModel ;
   } typeInfo ;
} A2iARC_PersistentResult;

typedef struct _A2iARC_DongleServer
{
   A2iARC_String name ;
} A2iARC_DongleServer;

typedef A2iARC_Enum A2iARC_PriorityType ;
#define _A2iARC_PriorityType_Count 4
#define A2iARC_PriorityType_High 1UL   ///< 
#define A2iARC_PriorityType_Idle 2UL   ///< 
#define A2iARC_PriorityType_Normal 3UL   ///< 
#define A2iARC_PriorityType_RealTime 4UL   ///< 

typedef struct _A2iARC_CPU
{
   A2iARC_PriorityType priority ;
   A2iARC_String dongleServer ;
   A2iARC_String donglePort ;
   A2iARC_String cpuServer ;
   A2iARC_String paramDir ;
   A2iARC_String portServer ;
   struct
   {
      struct _A2iARC_DongleServer * pTab ;
      unsigned long  cTab ;
   } dongleServers ;
} A2iARC_CPU;

typedef struct _A2iARC_Channel
{
   A2iARC_Unsigned sleepWaitResult_Msec ;
   struct
   {
      struct _A2iARC_CPU * pTab ;
      unsigned long  cTab ;
   } Cpu ;
} A2iARC_Channel;

typedef struct _A2iARC_PersistentData
{
   A2iARC_String Id ;
   A2iARC_PersistentDataType type ;
   A2iARC_Boolean privatePersistent ;
   union    // Depends on type
   {
      struct // Case type == Vocabulary
      {
         A2iARC_PersistentVocabulary vocabulary ;
      } CaseVocabulary ;
      struct // Case type == PostalDictionary
      {
         A2iARC_PostalDictionary dictionary ;
      } CasePostalDictionary ;
      struct // Case type == DocumentModel
      {
         A2iARC_DocumentModel model ;
      } CaseDocumentModel ;
      struct // Case type == AddressBook
      {
         A2iARC_AddressBook book ;
      } CaseAddressBook ;
      struct // Case type == DocumentClassifier
      {
         A2iARC_DocumentClassifier classifier ;
      } CaseDocumentClassifier ;
      struct // Case type == DevRecoDocument
      {
         A2iARC_DevInput devInput ;
      } CaseDevRecoDocument ;
      struct // Case type == SpecificPassport
      {
         A2iARC_SpecificPassport specificPassport ;
      } CaseSpecificPassport ;
      struct // Case type == Python
      {
         A2iARC_File scriptFilename ;
      } CasePython ;
   } typeInfo ;
   A2iARC_Dir configFolder ;
   struct
   {
      struct _A2iARC_PersistentResult * pTab ;
      unsigned long  cTab ;
   } results ;
   A2iARC_Boolean readyForRecognition ;
   A2iARC_Boolean edited ;
   A2iARC_RequestParameters parameters ;
} A2iARC_PersistentData;

typedef A2iARC_Enum A2iARC_ActionType ;
#define _A2iARC_ActionType_Count 2
#define A2iARC_ActionType_AddToVocabulary 1UL   ///< 
#define A2iARC_ActionType_RemoveFromVocabulary 2UL   ///< 

typedef struct _A2iARC_PersistentDataAction
{
   A2iARC_PersistentId Id ;
   A2iARC_ActionType type ;
   union    // Depends on type
   {
      struct // Case type == AddToVocabulary
      {
         A2iARC_String fileWords ;
         struct
         {
            struct _A2iARC_VocabItem * pTab ;
            unsigned long  cTab ;
         } words ;
      } CaseAddToVocabulary ;
      struct // Case type == RemoveFromVocabulary
      {
         A2iARC_String fileWords ;
         struct
         {
            struct _A2iARC_VocabItem * pTab ;
            unsigned long  cTab ;
         } words ;
      } CaseRemoveFromVocabulary ;
   } typeInfo ;
} A2iARC_PersistentDataAction;

typedef struct _A2iARC_GlobalValues
{
   A2iARC_Country country ;
   A2iARC_Currency currency ;
   A2iARC_Boolean useVocabularyHWR ;
   A2iARC_Boolean useVocabularyPRN ;
   A2iARC_Optimisation optimisation ;
   A2iARC_Unsigned dataExtractionThreshold ;
   A2iARC_CharactersEncoding inputEncoding ;
   A2iARC_CharactersEncoding outputEncoding ;
} A2iARC_GlobalValues;

typedef struct _A2iARC_MaskTable
{
   A2iARC_GlobalValues globalValues ;
   A2iARC_KeyDefinition defaultDefinition ;
   struct
   {
      struct _A2iARC_KeyDefinition * pTab ;
      unsigned long  cTab ;
   } tIndexedDefinition ;
   A2iARC_KeyType keyType ;
   union    // Depends on keyType
   {
      struct // Case keyType == String
      {
         struct
         {
            A2iARC_String * pTab ;
            unsigned long  cTab ;
         } tString ;
      } CaseString ;
      struct // Case keyType == MICR
      {
         struct
         {
            struct _A2iARC_MICR * pTab ;
            unsigned long  cTab ;
         } tMICR ;
      } CaseMICR ;
   } tKey ;
   struct
   {
      struct _A2iARC_PersistentData * pTab ;
      unsigned long  cTab ;
   } persistentData ;
   A2iARC_String productName ;
   A2iARC_String productVersion ;
   A2iARC_String productRelease ;
   A2iARC_CharactersEncoding tblEncoding ;
} A2iARC_MaskTable;

typedef struct _A2iARC_MaskKey
{
   A2iARC_KeyType keyType ;
   union    // Depends on keyType
   {
      struct // Case keyType == String
      {
         A2iARC_String stringVal ;
      } CaseString ;
      struct // Case keyType == MICR
      {
         A2iARC_MICR micr ;
      } CaseMICR ;
   } keyTypeInfo ;
} A2iARC_MaskKey;

typedef struct _A2iARC_CPUStatus
{
   A2iARC_Err status ;
   A2iARC_String statusContext ;
} A2iARC_CPUStatus;

typedef struct _A2iARC_ChannelStatus
{
   A2iARC_Unsigned faultCpu ;
   struct
   {
      struct _A2iARC_CPUStatus * pTab ;
      unsigned long  cTab ;
   } Cpu ;
} A2iARC_ChannelStatus;

typedef A2iARC_Enum A2iARC_RequestType ;
#define _A2iARC_RequestType_Count 26
#define A2iARC_RequestType_ICR 1UL   ///< 
#define A2iARC_RequestType_PersistentDataLoad 2UL   ///< 
#define A2iARC_RequestType_PersistentDataFree 3UL   ///< 
#define A2iARC_RequestType_PersistentDataFreeAll 4UL   ///< 
#define A2iARC_RequestType_PersistentDataTrain 5UL   ///< 
#define A2iARC_RequestType_PDALoad 6UL   ///< 
#define A2iARC_RequestType_InitDongleServer 7UL   ///< 
#define A2iARC_RequestType_LogEvent 8UL   ///< 
#define A2iARC_RequestType_TerminateProcess 9UL   ///< 
#define A2iARC_RequestType_ICRStatus 10UL   ///< 
#define A2iARC_RequestType_IsAlive 11UL   ///< 
#define A2iARC_RequestType_IsPersistentLoaded 12UL   ///< 
#define A2iARC_RequestType_PagePreClassif 13UL   ///< 
#define A2iARC_RequestType_PageClassifierTraining 14UL   ///< 
#define A2iARC_RequestType_PageClassifyAndExtract 15UL   ///< 
#define A2iARC_RequestType_PageMultiClassifyFromIRDB 16UL   ///< 
#define A2iARC_RequestType_PageConsolidatedExtraction 17UL   ///< 
#define A2iARC_RequestType_PagePreClassifFromIRDB 18UL   ///< 
#define A2iARC_RequestType_CollectionPreClassif 19UL   ///< 
#define A2iARC_RequestType_CollectionPreClassifFromIRDB 20UL   ///< 
#define A2iARC_RequestType_CollectionClassifierTraining 21UL   ///< 
#define A2iARC_RequestType_CollectionClassifyAndExtract 22UL   ///< 
#define A2iARC_RequestType_CollectionMultiClassifyFromIRDB 23UL   ///< 
#define A2iARC_RequestType_AutoDefinePreClassif 24UL   ///< 
#define A2iARC_RequestType_AutoDefineClassifierTraining 25UL   ///< 
#define A2iARC_RequestType_AutoDefineClassifyAndExtract 26UL   ///< 

typedef A2iARC_Enum A2iARC_TranscriptionSource ;
#define _A2iARC_TranscriptionSource_Count 1
#define A2iARC_TranscriptionSource_IRDB 1UL   ///< 

typedef A2iARC_Enum A2iARC_TrainingAlgo ;
#define _A2iARC_TrainingAlgo_Count 3
#define A2iARC_TrainingAlgo_SearchByTemplate 1UL   ///< 
#define A2iARC_TrainingAlgo_ActiveLearning 2UL   ///< 
#define A2iARC_TrainingAlgo_Clustering 3UL   ///< 

typedef A2iARC_Enum A2iARC_ClassifyAlgo ;
#define _A2iARC_ClassifyAlgo_Count 3
#define A2iARC_ClassifyAlgo_SearchByTemplate 1UL   ///< 
#define A2iARC_ClassifyAlgo_ActiveLearning 2UL   ///< 
#define A2iARC_ClassifyAlgo_Clustering 3UL   ///< 

typedef struct _A2iARC_RequestInput
{
   A2iARC_RequestType type ;
   union    // Depends on type
   {
      struct // Case type == ICR
      {
         A2iARC_Input input ;
      } CaseICR ;
      struct // Case type == PersistentDataLoad
      {
         A2iARC_PersistentData data ;
      } CasePersistentDataLoad ;
      struct // Case type == IsPersistentLoaded
      {
         A2iARC_UserId id ;
      } CaseIsPersistentLoaded ;
      struct // Case type == PersistentDataFree
      {
         A2iARC_UserId id ;
      } CasePersistentDataFree ;
      struct // Case type == PersistentDataTrain
      {
         A2iARC_PersistentData data ;
      } CasePersistentDataTrain ;
      struct // Case type == PDALoad
      {
         A2iARC_PersistentDataAction data ;
      } CasePDALoad ;
      struct // Case type == LogEvent
      {
         A2iARC_Boolean flagLogEvent ;
      } CaseLogEvent ;
      struct // Case type == InitDongleServer
      {
         struct
         {
            struct _A2iARC_DongleServer * pTab ;
            unsigned long  cTab ;
         } dongleServers ;
      } CaseInitDongleServer ;
      struct // Case type == TerminateProcess
      {
         A2iARC_Err status ;
      } CaseTerminateProcess ;
      struct // Case type == PagePreClassif
      {
         A2iARC_Input input ;
      } CasePagePreClassif ;
      struct // Case type == PageClassifierTraining
      {
         A2iARC_PersistentData data ;
         A2iARC_TrainingAlgo trainingAlgo ;
         union    // Depends on trainingAlgo
         {
            struct // Case trainingAlgo == Clustering
            {
               A2iARC_Unsigned NbClusters ;
            } CaseClustering ;
         } trainingAlgoInfo ;
      } CasePageClassifierTraining ;
      struct // Case type == PageClassifyAndExtract
      {
         A2iARC_Input input ;
         A2iARC_Boolean mustExtract ;
         A2iARC_TranscriptionSource transcriptionSource ;
         A2iARC_ClassifyAlgo classifyAlgo ;
      } CasePageClassifyAndExtract ;
      struct // Case type == PageMultiClassifyFromIRDB
      {
         A2iARC_Input input ;
         A2iARC_ClassifyAlgo classifyAlgo ;
      } CasePageMultiClassifyFromIRDB ;
      struct // Case type == PageConsolidatedExtraction
      {
         A2iARC_Input input ;
      } CasePageConsolidatedExtraction ;
      struct // Case type == PagePreClassifFromIRDB
      {
         A2iARC_Input input ;
      } CasePagePreClassifFromIRDB ;
      struct // Case type == CollectionPreClassif
      {
         A2iARC_Input input ;
      } CaseCollectionPreClassif ;
      struct // Case type == CollectionPreClassifFromIRDB
      {
         A2iARC_Input input ;
      } CaseCollectionPreClassifFromIRDB ;
      struct // Case type == CollectionClassifierTraining
      {
         A2iARC_PersistentData data ;
         A2iARC_TrainingAlgo trainingAlgo ;
         union    // Depends on trainingAlgo
         {
            struct // Case trainingAlgo == Clustering
            {
               A2iARC_Unsigned NbClusters ;
            } CaseClustering ;
         } trainingAlgoInfo ;
      } CaseCollectionClassifierTraining ;
      struct // Case type == CollectionClassifyAndExtract
      {
         A2iARC_Input input ;
         A2iARC_Boolean mustExtract ;
         A2iARC_TranscriptionSource transcriptionSource ;
         A2iARC_ClassifyAlgo classifyAlgo ;
      } CaseCollectionClassifyAndExtract ;
      struct // Case type == CollectionMultiClassifyFromIRDB
      {
         A2iARC_Input input ;
         A2iARC_ClassifyAlgo classifyAlgo ;
      } CaseCollectionMultiClassifyFromIRDB ;
      struct // Case type == AutoDefinePreClassif
      {
         A2iARC_Input input ;
      } CaseAutoDefinePreClassif ;
      struct // Case type == AutoDefineClassifierTraining
      {
         A2iARC_Input input ;
      } CaseAutoDefineClassifierTraining ;
      struct // Case type == AutoDefineClassifyAndExtract
      {
         A2iARC_Input input ;
      } CaseAutoDefineClassifyAndExtract ;
   } typeInfo ;
} A2iARC_RequestInput;

typedef struct _A2iARC_PersistentDataTrainResult
{
   A2iARC_Err status ;
   A2iARC_String statusContext ;
   struct
   {
      struct _A2iARC_PersistentResult * pTab ;
      unsigned long  cTab ;
   } results ;
   A2iARC_Boolean readyForRecognition ;
} A2iARC_PersistentDataTrainResult;

typedef struct _A2iARC_RequestOutput
{
   A2iARC_RequestType type ;
   union    // Depends on type
   {
      struct // Case type == ICR
      {
         A2iARC_Output output ;
      } CaseICR ;
      struct // Case type == PersistentDataLoad
      {
         A2iARC_Err status ;
         A2iARC_String statusContext ;
      } CasePersistentDataLoad ;
      struct // Case type == PersistentDataFree
      {
         A2iARC_Err status ;
      } CasePersistentDataFree ;
      struct // Case type == PersistentDataTrain
      {
         A2iARC_PersistentDataTrainResult result ;
      } CasePersistentDataTrain ;
      struct // Case type == PDALoad
      {
         A2iARC_Err status ;
      } CasePDALoad ;
      struct // Case type == LogEvent
      {
         A2iARC_Boolean flagLogEvent ;
      } CaseLogEvent ;
      struct // Case type == InitDongleServer
      {
         A2iARC_Err status ;
         A2iARC_String statusContext ;
      } CaseInitDongleServer ;
      struct // Case type == ICRStatus
      {
         A2iARC_Float progressStatus ;
         A2iARC_String progressStatusDesc ;
      } CaseICRStatus ;
      struct // Case type == IsAlive
      {
         A2iARC_Boolean alive ;
      } CaseIsAlive ;
      struct // Case type == IsPersistentLoaded
      {
         A2iARC_Boolean isPersistentLoaded ;
      } CaseIsPersistentLoaded ;
      struct // Case type == PagePreClassif
      {
         A2iARC_Output output ;
      } CasePagePreClassif ;
      struct // Case type == PageClassifierTraining
      {
         A2iARC_PersistentDataTrainResult output ;
      } CasePageClassifierTraining ;
      struct // Case type == PageClassifyAndExtract
      {
         A2iARC_Output output ;
      } CasePageClassifyAndExtract ;
      struct // Case type == PageMultiClassifyFromIRDB
      {
         A2iARC_Output output ;
      } CasePageMultiClassifyFromIRDB ;
      struct // Case type == PageConsolidatedExtraction
      {
         A2iARC_Output output ;
      } CasePageConsolidatedExtraction ;
      struct // Case type == PagePreClassifFromIRDB
      {
         A2iARC_Output output ;
      } CasePagePreClassifFromIRDB ;
      struct // Case type == CollectionPreClassif
      {
         A2iARC_Output output ;
      } CaseCollectionPreClassif ;
      struct // Case type == CollectionPreClassifFromIRDB
      {
         A2iARC_Output output ;
      } CaseCollectionPreClassifFromIRDB ;
      struct // Case type == CollectionClassifierTraining
      {
         A2iARC_PersistentDataTrainResult output ;
      } CaseCollectionClassifierTraining ;
      struct // Case type == CollectionMultiClassifyFromIRDB
      {
         A2iARC_Output output ;
      } CaseCollectionMultiClassifyFromIRDB ;
      struct // Case type == CollectionClassifyAndExtract
      {
         A2iARC_Output output ;
      } CaseCollectionClassifyAndExtract ;
      struct // Case type == AutoDefinePreClassif
      {
         A2iARC_Output output ;
      } CaseAutoDefinePreClassif ;
      struct // Case type == AutoDefineClassifierTraining
      {
         A2iARC_Output output ;
      } CaseAutoDefineClassifierTraining ;
      struct // Case type == AutoDefineClassifyAndExtract
      {
         A2iARC_Output output ;
      } CaseAutoDefineClassifyAndExtract ;
   } typeInfo ;
   A2iARC_Err status ;
   A2iARC_String statusContext ;
} A2iARC_RequestOutput;

typedef A2iARC_Enum A2iARC_MessageType ;
#define _A2iARC_MessageType_Count 8
#define A2iARC_MessageType_ReadyToBeInitialized 1UL   ///< Sent by the son when ready to exchange message
#define A2iARC_MessageType_Initialize 2UL   ///< Sent by the father for first child initialization
#define A2iARC_MessageType_ReadyToReceiveData 3UL   ///< Sent by the son after receiving INITIALIZE message
#define A2iARC_MessageType_Load 4UL   ///< Sent by the father for sending object 
#define A2iARC_MessageType_Save 5UL   ///< Sent by the son to save TBL modification on the father
#define A2iARC_MessageType_NotifyReset 6UL   ///< Sent by the son when it needs to reset TBL values
#define A2iARC_MessageType_Close 7UL   ///< Sent by the son to notify father about closing
#define A2iARC_MessageType_StopChild 8UL   ///< Sent by the father to close the child process

typedef struct _A2iARC_NodeVisibility
{
   A2iARC_String type ;
   A2iARC_String member ;
   A2iARC_Boolean value ;
} A2iARC_NodeVisibility;

typedef struct _A2iARC_MessageMapInitializeParam
{
   struct
   {
      struct _A2iARC_NodeVisibility * pTab ;
      unsigned long  cTab ;
   } nodeVisibility ;
} A2iARC_MessageMapInitializeParam;

typedef struct _A2iARC_MessageMap
{
   A2iARC_MessageType messageType ;
   A2iARC_Boolean recursive ;
   union    // Depends on messageType
   {
      struct // Case messageType == Initialize
      {
         A2iARC_String EngineFolder ;
         A2iARC_String altA2iARCPg ;
         A2iARC_String dongleServer ;
         A2iARC_Boolean disableInput ;
         A2iARC_String helpFile ;
         A2iARC_MessageMapInitializeParam messageMapInitializeParam ;
      } CaseInitialize ;
      struct // Case messageType == Load
      {
         A2iARC_MaskTable TBL ;
         A2iARC_String tblPath ;
         A2iARC_String objectPath ;
      } CaseLoad ;
      struct // Case messageType == Save
      {
         A2iARC_MaskTable TBL ;
      } CaseSave ;
   } messageTypeInfo ;
} A2iARC_MessageMap;

#pragma pack(pop)



// Session Handling


/// Initialize a session specification.
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitChannelFormat (
      A2iARC_Channel * pChannel ///< Input data structure defining channel
      ) ;


/// Opens a new queue (session) identifier.
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_OpenChannelExt (
      A2iARC_Id * retChannelId,        ///< Returned Channel ID
      A2iARC_Channel * pChannel,       ///< Input data structure defining channel
      A2iARC_Unsigned TimeOutMs        ///< Time used to check the thread
      ) ;


/// Opens a new queue (session) identifier
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_OpenChannel (
      A2iARC_Id * retChannelId         ///< Returned channel ID
      ) ;

/// Closes a queue
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_CloseChannel(
      A2iARC_Id channelId              ///< channelId
      ) ;

/// Query a channel to check for errors
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_CheckStatusChannelExt (
      A2iARC_Id channelId  ,                 ///< Channel ID to be queried
      A2iARC_ChannelStatus *pChannelStatus   ///< Filled data structure 
                                             ///< containing the error status
      ) ;

#pragma pack(push,8)

/// Structure for handling document tables
typedef struct
{
   A2iARC_MaskTable loaded ;
   void * internal ;
   void * tRegex ;
   void * reserved ;
} A2iARC_DocumentTable ;

#pragma pack(pop)

/// Default values for a document table
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_OpenDocumentTable (
      A2iARC_DocumentTable * pTable,         ///< Document table to be initialized
      const char * fileName                  ///< FileName of the configuration file
      ) ;

/// Default values for a document table
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitDocumentTable_Default (
      A2iARC_DocumentTable * pTable          ///< Document table to be initialized
      ) ;

/// Default values for a document table
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitDocumentTable (
      A2iARC_DocumentTable * pTable,         ///< Document table to be initialized
      const char * fileName                  ///< FileName of the configuration file
      ) ;

///< Closes a document table
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_CloseDocumentTable (
      A2iARC_DocumentTable * pTable          ///< Document table to be closed
      ) ;

///< load a TBL out of a memory string with the contents of a TBL file.
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_OpenDocumentTableMemory (
      A2iARC_DocumentTable * pTable,         ///< Document table to be initialized
      const char * contents                  ///< NULL terminated string with TBL contents
      ) ;

///< Get a persistent data from a TBL
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_GetDocumentTablePersistentData ( 
      A2iARC_DocumentTable *pTable,          ///< Document table (constant)
      const char* iPersistentName,           ///< persistent name to get
      A2iARC_Id *oPersistentId);             ///< Persistent ID returned

///< close the persistent data
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_ClosePersistentData (
      A2iARC_Id PersistentId
      );

///< return a pointer to the persistent data
A2iARC_DLL A2iARC_PersistentData* A2IA_FUNC_DCL A2iARC_GetDocumentPersistentDataAdd(
      A2iARC_Id persistentId                 ///< Persistent ID
      );      

// Event logging management

/// Switch Event Logging between EventJournal and A2iATrace
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_EventLog (
      A2iARC_Boolean loggingFlag			 ///< A2iA Boolean value to switcn
	  );

/// Switch Event Loggin in the Channel (in all ICR)
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_SwitchLogEventInChannel (
      A2iARC_Id               sessionId,     ///< Identifier of Channel
      A2iARC_Boolean          flagLogEvent   ///< Flag activate/deactivate logging
      );

// Persistent data blocks management.

/// Send PD to all ICR-process attached to channel
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_UploadPersistentData (
      A2iARC_PersistentData *pPersistent,       ///< Pointer to a valid Persistent Data (const)
      A2iARC_Id sessionId                       ///< Pointer to an initialized channel
      ) ;


/// Send PD defined in the table to all ICR-process in the channel
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_UploadDocumentTablePersistentData (
      A2iARC_DocumentTable *pDocumentTable,        ///< Pointer to a valid table (constant)
      A2iARC_Id  sessionId                         ///< Pointer to an initialized channel
      ) ;


/// Update Persistent Data in accordence with PD Action (PDA)
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_UpdatePersistentData (
      A2iARC_PersistentDataAction *pPDA,           ///< Pointer to PDA block (const)
      A2iARC_Id sessionId                          ///< Pointer to an initialized channel
      ) ;

/// Free allocated memory for the named PD
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_FreePersistentData (
      A2iARC_String name,                          ///< Name of persistent Data block
      A2iARC_Id sessionId                          ///< Pointer to an initialized channel
      ) ;


/// Free allocated memory for the named PD
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_FreeAllPersistent(A2iARC_Id sessionId);



// Document forms from table of documents

/// Retrieve the default document
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_GetDefaultDocument (
      A2iARC_DocumentTable * pTable,               ///< Tab of document forms
      A2iARC_Id * retDocId                         ///< New doc Id for the default document.
      ) ;

/// Retrieve a document form from an int definition
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_GetDocument_String (
      A2iARC_DocumentTable * pTable,               ///< Tab of document forms
      A2iARC_String vString,                       ///< String definition
      A2iARC_Id * retDocId,                        ///< Resulting document
      A2iARC_Unsigned * pIndex                     ///< Index of the document
      ) ;

A2iARC_DLL A2iARC_Input * A2IA_FUNC_DCL A2iARC_GetInputDocumentAdd (
      A2iARC_Id docId 
      ) ;


A2iARC_DLL int A2IA_FUNC_DCL A2iARC_CloseDocument (
      A2iARC_Id docId
      ) ;

/// Init a dongle query
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitInputDongleQuery (
      A2iARC_Input * pInput                        ///< Data to be initialized
      ) ;



// Image initialisations for a particular document

/** Defines a A2iARC_ImageSource_File tiff image 
* Sets the appropriate fields for loading an image.
* In pInput->image field :
*   - imageSourceType       A2iARC_ImageSourceType_File
*   - inputFormat           A2iARC_InputFormat_Tiff
*   - info.transportModel   0 (undefined) => will need to be defined later
*   - info.resolution_Dpi   0 (undefined)
*   - info.levelCount       0 (undefined)
*   - info.rowSize          0 (undefined)
*   - info.colSize          0 (undefined)
*   - imageSource           defined array with fileName and strlen (fileName)
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageTiffFile (
      A2iARC_Input * pInput,                       ///< Modified input 
      const char * fileName                        ///< Filename
      ) ;



/** Defines a A2iARC_ImageSource_Memory tiff image.
* Sets the following fields of the pInput->image field :
*  - imageSourceType       A2iARC_ImageSourceType_Memory
*  - inputFormat           A2iARC_InputFormat_Tiff
*  - info.resolution_Dpi   0 (undefined)
*  - info.levelCount       0 (undefined)
*  - info.rowSize          0 (undefined)
*  - info.colSize          0 (undefined)
*  - imageSource           defined array with data and dataSize
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageTiffMemory (
      A2iARC_Input * pInput,                       ///< Modified input
      void * data,                                 ///< Tiff image buffer
      unsigned long dataSize                       ///< size of the image buffer
      ) ;

/// Defines a A2iARC_ImageSource_Memory Jpeg image.
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageJpegMemory(
      A2iARC_Input * pInput,                       ///< Modified input
      void * data,                                 ///< Jpeg image buffer
      unsigned long dataSize                       ///< Size of the image buffer
      );

/// Defines a A2iARC_ImageSource_Memory Bmp image.
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageBmpMemory(
      A2iARC_Input * pInput,                       ///< Modified input
      void * data,                                 ///< Bmp image buffer
      unsigned long dataSize                       ///< Size of the image buffer
      );

/** Defines a A2iARC_ImageSource_SharedMemory tiff image.
* Sets the following fields of the pInput->image field :
*  - imageSourceType       A2iARC_ImageSourceType_Memory
*  - inputFormat           A2iARC_InputFormat_Tiff
*  - info.resolution_Dpi   0 (undefined)
*  - info.levelCount       0 (undefined)
*  - info.rowSize          0 (undefined)
*  - info.colSize          0 (undefined)
*  - imageSource           defined array with data and dataSize
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageTiffSharedMemory (
      A2iARC_Input * pInput,                       ///< Modified input
      const char* name,                            ///< Shared buffer name
      unsigned long dataSize,                      ///< data size of the image buffer
      unsigned long offset                         ///< offset 
      ) ;

/// Defines a A2iARC_ImageSource_SharedMemory Jpeg image.
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageJpegSharedMemory(
      A2iARC_Input * pInput,                       ///< Modified input
      const char* name,                            ///< Shared buffer name
      unsigned long dataSize,                      ///< data size of the image buffer
      unsigned long offset                         ///< offset 
      );

/// Defines a A2iARC_ImageSource_SharedMemory Bmp image.
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageBmpSharedMemory(
      A2iARC_Input * pInput,                       ///< Modified input
      const char* name,                            ///< Shared buffer name
      unsigned long dataSize,                      ///< data size of the image buffer
      unsigned long offset                         ///< offset 
      );

/// Defines a A2iARC_ImageSource_SharedMemory byte image.
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageByteSharedMemory(
      A2iARC_Input * pInput,                       ///< Modified input
      int nl, int nc,                              ///< dimensions of the image
      const char* name,                            ///< Shared buffer name
      unsigned long offset                         ///< offset 
      );

/// Defines a A2iARC_ImageSource_File bmp image.
/** Sets the following fields of the pInput->image field :
*  - imageSourceType          A2iARC_ImageSourceType_File
*  - inputFormat              A2iARC_InputFormat_Bmp
*  - info.transportModel      0 (undefined) => will need to be defined later
*  - info.resolution_Dpi      0 (undefined)
*  - info.levelCount          0 (undefined)
*  - info.rowSize             0 (undefined)
*  - info.colSize             0 (undefined)
*  - imageSource              defined array with fileName and strlen (fileName)
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageBmpFile (
      A2iARC_Input * pInput,                       ///< Modified input 
      const char * fileName                        ///< Filename
      ) ;

/** Defines a A2iARC_ImageSource_Memory byte image.
* Sets the following fields of the pInput->image field :
*  - imageSourceType          A2iARC_ImageSourceType_Memory
*  - inputFormat              A2iARC_InputFormat_Byte
*  - info.resolution_Dpi      0 (undefined)
*  - info.levelCount          0 (undefined)
*  - info.rowSize             nl (undefined)
*  - info.colSize             nc (undefined)
*  - imageSource              defined array with data and dataSize = nl * nc
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageByteMemory ( 
      A2iARC_Input * pInput,                       ///< Modified input
      unsigned char * data,                        ///< vector-matrix
      int nl, int nc                               ///< dimensions of the image
      ) ;

/** Defines a A2iARC_ImageSource_File JPEG image.
* Sets the following fields of the pInput->image field :
*  - imageSourceType          A2iARC_ImageSourceType_File
*  - inputFormat              A2iARC_InputFormat_Jpeg
*  - info.transportModel      0 (undefined) => will need to be defined later
*  - info.resolution_Dpi      0 (undefined)
*  - info.levelCount          0 (undefined)
*  - info.rowSize             0 (undefined)
*  - info.colSize             0 (undefined)
*  - imageSource              defined array with fileName and strlen (fileName)
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageJpegFile (
      A2iARC_Input * pInput,                       ///< Modified input 
      const char * fileName                        ///< Filename
      ) ;

/** Returns an Id on an Image Object cerated from a file name. This object 
contains all the pages included in the image file. The order of pages is take 
as it is in the file. 
*/
A2iARC_DLL A2iARC_Id A2IA_FUNC_DCL A2iARC_OpenImageObjectFromTiffFile (char *FileName); 

/** Sets an image object for any document mask before sending a process request 
to an ICR channel. If it is required to change the order of the pages  just update 
the value index input->additionalPages[i].inputFormatInfo.CaseTiff.pageIndex accordingly.
( Where i is the page position in the MTiff). The memory for additionalPages
array is allocated with an alloc group stored in the image object.
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DefineImageFromObject(A2iARC_Input*, A2iARC_Id ImageId) ; 

/**  Closes an image object, releasing the 
associated allocated memory. This function has to be called on every object
Image after every call to A2iARC_OpenRequest
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_CloseImageObject(A2iARC_Id ImageId);

/**  Add an
additionalpage at the end of the current additional pages defined for the
imageObject. This methode updates the property AdditionalPages accordingly with
the extension file provided inside FileName.
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_AddPageToImageObject(A2iARC_Id ImageId, char* FileName);


/// ICR request
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_OpenRequest (
      A2iARC_Id sessionID,
      A2iARC_Id * ret_requestId,                   ///< returned request id
      const A2iARC_Input * input                   ///< request 
      ) ;



#define A2iARC_Receive_NoWait	0
#define A2iARC_Receive_Wait		1

/// ICR results
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_ReceiveResult (
      A2iARC_Id sessionID,	
      A2iARC_Id * ret_requestId,                   ///< Returned request id
      A2iARC_Output * result,                      ///< buffer to receive the result of operation
      A2iARC_Enum waitMode                         ///< one of A2iARC_Receive_*
      ) ;

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_GetResult (
      A2iARC_Id sessionId, 
      A2iARC_Id * retRequestId, 
      A2iARC_Output * pResult, 
      A2iARC_Unsigned TimeOutMs
      );

A2iARC_DLL A2iARC_Input* A2IA_FUNC_DCL A2iARC_GetInput(
      A2iARC_Id channelId,
      A2iARC_Id requestId
      );

      
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_CloseRequest (
      A2iARC_Id sessionID,
      A2iARC_Id requestId
      ) ;

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_Identify (
      const char* identifier
      ) ;

A2iARC_DLL int A2IA_FUNC_DCL  A2iARC_EventLog (
      A2iARC_Boolean loggingFlag
      ) ;

//BEN
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_PutDefaultDocument(
      A2iARC_DocumentTable* pTable, 
      A2iARC_Input* pInput               // pInput is constant
      );
                        
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DelDocument_String(
      A2iARC_DocumentTable* pTable, 
      const A2iARC_String pString
      );

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_PutDocument_String(
      A2iARC_DocumentTable* pTable, 
      const A2iARC_String pString, 
      A2iARC_Input* pInput             // pInput is constant
      );

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_CreateDocumentTable(
      A2iARC_DocumentTable* pTable
      );

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_SaveDocumentTable(
      const A2iARC_DocumentTable* pTable, 
      const char* filename
      );

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_SetDocumentTableKeyType(
      A2iARC_DocumentTable* pTable,
      const A2iARC_KeyType ketType
      );

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_GetDocumentTableKeyType(
      A2iARC_DocumentTable* pTable,       /// pTable is constant
      A2iARC_KeyType* keyType
      );


/**
* Gets the description string for one particular error code.
* This function only works if A2iARC_Init was called before.
* @param err The error code to retrieve the message for.
* @param errorDescription The buffer in which the description will be copied.
* @param maxSizeDescription The size of errorDescription.
* @return 
*    - A2iARC_Ok if success. The buffer contains the description.
*    - A2iARC_Err_Memory if maxSizeDescription is smaller than the size required
*                     for the message.
*    - A2iARC_Err_BadValue if the error code doesn't exist.
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_GetErrorText(A2iARC_Err err, char * errorDescription, unsigned int maxSizeDescription);


/** MICR documents.
* Retrieve a document form from an MICR definition.
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_GetDocument_MICR (
      A2iARC_DocumentTable * pTable,      ///< Tab of document masks (constant)
      A2iARC_MICR * pMICR,                ///< MICR definition (constant)
      A2iARC_Id * retDocId,               ///< Resulting document
      A2iARC_Unsigned * pIndex            ///< Index of the document
      ) ;


/** Init any kind of check
*  - documentType                                  A2iaRC_DocumentType_Check 
*  - documentTypeInfo.CaseCheck.check.larUsage     A2iARC_LarUsage_Default
*  - documentTypeInfo.CaseCheck.check.bills.cTab   0 
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitInputCheck (
      A2iARC_Input * pInput               ///< Data to be initialized
      ) ;

/** Init a US check.
*  - documentType                                              A2iaRC_DocumentType_Check 
*  - documentTypeInfo.CaseCheck.check.country                  A2iARC_Country_US
*  - documentTypeInfo.CaseCheck.check.currency                 A2iARC_Currency_USD
*  - documentTypeInfo.CaseCheck.check.larUsage                 A2iARC_LarUsage_Default
*  - documentTypeInfo.CaseCheck.check.bills.cTab               0 
*  - documentTypeInfo.CaseCheck.check.countryInfo.CaseUS.kind  A2iARC_Check_US_Kind_Any ;
*
* Image still needs to be set after this call.
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitInputCheckUS (
      A2iARC_Input * pInput               ///< Data to be initialized
      ) ;


/** Singlefield.
*  - documentType                A2iARC_DocumentType_SingleField
*
* Image still needs to be set after this call.
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitInputSingleField (
      A2iARC_Input * pInput               ///< Data to be initialized
      ) ;


/** Init a UK check.
*
*  - documentType                                  A2iaRC_DocumentType_Check 
*  - documentTypeInfo.CaseCheck.check.country      A2iARC_Country_UK
*  - documentTypeInfo.CaseCheck.check.currency     A2iARC_Currency_GBP
*  - documentTypeInfo.CaseCheck.check.larUsage     A2iARC_LarUsage_Default
*  - documentTypeInfo.CaseCheck.check.bills.cTab   0 
*
* Image still needs to be set after this call
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitInputCheckUK (
      A2iARC_Input * pInput               ///< Data to be initialized
      ) ;

/** Init a British deposit ticket.
*  - documentType                               A2iaRC_DocumentType_DepositTicket
*  - documentTypeInfo.CaseCheck.check.country   A2iARC_Country_UK
*
* Image still needs to be set after this call
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitInputDepositTicketUK (
      A2iARC_Input * pInput               ///< Data to be initialized
      );


/** Init a French check
*  - documentType                                  A2iaRC_DocumentType_Check 
*  - documentTypeInfo.CaseCheck.check.country      A2iARC_Country_France
*  - documentTypeInfo.CaseCheck.check.currency     A2iARC_Currency_FRF
*  - documentTypeInfo.CaseCheck.check.larUsage     A2iARC_LarUsage_Default
*  - documentTypeInfo.CaseCheck.check.bills.cTab   0 
*
* Image still needs to be set after this call.
*/
A2iARC_DLL int A2IA_FUNC_DCL A2iARC_InitInputCheckFrance (
      A2iARC_Input * pInput               ///< Data to be initialized
      ) ;

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_DelDocument_MICR(
      A2iARC_DocumentTable* pTable, 
      A2iARC_MICR* pMICR
      );

A2iARC_DLL int A2IA_FUNC_DCL A2iARC_PutDocument_MICR(
      A2iARC_DocumentTable* pTable, 
      A2iARC_MICR* pMICR, 
      A2iARC_Input* pInput             ///< pInput is constant
      );



#ifdef __cplusplus
}
#endif
#endif // _A2iARC_INCLUDED
