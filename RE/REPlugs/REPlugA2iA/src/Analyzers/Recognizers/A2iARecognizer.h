#ifndef REPlugs_A2iA_A2iARecognizer_h
#define REPlugs_A2iA_A2iARecognizer_h

// Base class.
#include <RELibrary/Analyzers/Recognizers/AbstractRecognizer>

namespace REPlugs {

namespace A2iA {

class A2iARecognizer : public RE::AbstractRecognizer {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractRecognizer)
    Q_PROPERTY(RE::T::FilePath  documentTablePath   READ documentTablePath  WRITE setDocumentTablePath  RESET resetDocumentTablePath)

public:
    // Constructors.
    Q_INVOKABLE				A2iARecognizer				(QObject *p=0);
    // Accessors.
    RE::T::FilePath const&  documentTablePath			() const					{ return _documentTablePath; }
    void					setDocumentTablePath		(RE::T::FilePath const &v)  { _documentTablePath = v; }
    void                    resetDocumentTablePath      ()                          { _documentTablePath = QString(""); }

protected:
    bool					initialize					() const;
    QString					a2iaStringError				(int e) const;
    virtual bool			doRun						();

private:
    RE::T::FilePath         _documentTablePath;
};

}

}

#endif
