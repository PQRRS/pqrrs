#include "A2iARecognizer.h"
#include <QtCore/QDebug>
#include <QtCore/QByteArray>
#include <QtCore/QBuffer>
#include <QtGui/QImageWriter>

// The A2iA API isn't made for MinGW compilation. It tests _MSC_VER but nothing else.
// So let's manually define what is needed for it to properly import.
//#define A2iARC_DLL      __declspec(dllimport)
//#define A2IA_FUNC_DCL   __cdecl
#include "../../../ThirdParty/win32/include/A2iA/A2iACheckReader.h"

REPlugs::A2iA::A2iARecognizer::A2iARecognizer (QObject *p)
: RE::AbstractRecognizer(p) {
}

bool REPlugs::A2iA::A2iARecognizer::initialize () const {
    // Make sure A2iA library is only initialized once.
    static bool	initDone				= false;
    if (initDone)						return true;
    initDone							= true;
    // Try to init...
    int				ret					= A2iARC_Init("C:\\Program Files\\A2iA\\A2iA CheckReader V4.7 R2\\Parms\\France\\Parms");
    if (ret!=A2iARC_Err_OK) {
        Wrn								<< "The A2iA API failed to initialize:" << ret << ", " << a2iaStringError(ret) << ")!";
        return							false;
    }
    return								true;
}

bool REPlugs::A2iA::A2iARecognizer::doRun () {
    // Temporary variable.
    int						ret;
    // Initialize the API.
    if (!initialize()) {
        Wrn								<< "Cannot run because initialization failed!";
        return false;
    }

    // Open recognition channel.
    A2iARC_Id				a2iaChan;
    ret									= A2iARC_OpenChannel(&a2iaChan);
    if (ret!=A2iARC_Err_OK) {
        Wrn								<< "Unable to open A2iA ICR channel:" << ret << "" << a2iaStringError(ret) << ")!";
        return							false;
    }
    // Define document table.
    A2iARC_DocumentTable	a2iaTbl;
    ret									= A2iARC_OpenDocumentTable(&a2iaTbl, _documentTablePath.value().toUtf8().constData());
    if (ret!=A2iARC_Err_OK) {
        Wrn								<< "Unable to open A2iA document table definitions at" << _documentTablePath.value() << ":" << ret << "" << a2iaStringError(ret) << ")!";
        A2iARC_CloseChannel				(a2iaChan);
        return							false;
    }

    // Load default document definition.
    A2iARC_Id				a2iaDocId;
    ret									= A2iARC_GetDefaultDocument(&a2iaTbl, &a2iaDocId);
    if (ret!=A2iARC_Err_OK) {
        Wrn								<< "Unable to retrieve A2iA default document definitions:" << ret << "" << a2iaStringError(ret) << ")!";
        A2iARC_CloseDocumentTable		(&a2iaTbl);
        A2iARC_CloseChannel				(a2iaChan);
        return							false;
    }

    // Prepare input document.
    A2iARC_Input			*a2iaInput	= A2iARC_GetInputDocumentAdd(a2iaDocId);
    if (!a2iaInput) {
        Wrn								<< "Unable to initialize A2iA input document!";
        A2iARC_CloseDocumentTable		(&a2iaTbl);
        A2iARC_CloseChannel				(a2iaChan);
        return							false;
    }
    A2iARC_CheckInput		*a2iaChkIn	= &a2iaInput->documentTypeInfo.CaseCheck.check;
    a2iaInput->documentType				= A2iARC_DocumentType_Check;
    a2iaChkIn->fields.signature			= true;

    // Convert image to in-memory bitmap.
    QByteArray				ba;
    QBuffer					buf			(&ba);
    QImageWriter			iw			(&buf, "tif");
    iw.write							(*source());

    // Pass image to the API.
    ret									= A2iARC_DefineImageTiffMemory(a2iaInput, ba.data(), ba.size());
    if (ret!=A2iARC_Err_OK) {
        Wrn								<< "Unable to define A2iA image byte memory area:" << ret << "" << a2iaStringError(ret) << ")!";
        A2iARC_CloseDocument			(a2iaDocId);
        A2iARC_CloseDocumentTable		(&a2iaTbl);
        A2iARC_CloseChannel				(a2iaChan);
        return							false;
    }
    // Set up image properties.
    a2iaInput->image.transportModel		= A2iARC_TransportModel_Generic;
    a2iaInput->image.resolution_Dpi		= 200;
    a2iaInput->image.levelCount			= 255;
    a2iaInput->image.imageOrientation	= A2iARC_ImageOrientation_LeftRight_TopBottom;

    // Feed request to channel.
    A2iARC_Id				a2iaReqId;
    ret									= A2iARC_OpenRequest(a2iaChan, &a2iaReqId, a2iaInput);
    if (ret!=A2iARC_Err_OK) {
        Wrn								<< "Unable to feed request to the A2iA channel:" << ret << "" << a2iaStringError(ret) << ")!";
        A2iARC_CloseDocument			(a2iaDocId);
        A2iARC_CloseDocumentTable		(&a2iaTbl);
        A2iARC_CloseChannel				(a2iaChan);
        return							false;
    }

    // Fetch result from request.
    A2iARC_Output			a2iaOut;
    ret									= A2iARC_GetResult(a2iaChan, &a2iaReqId, &a2iaOut, 2500);
    if (ret!=A2iARC_Err_OK) {
        Wrn								<< "Unable to retrieve request results from the A2iA channel:" << ret << "" << a2iaStringError(ret) << ")!";
        A2iARC_CloseRequest				(a2iaChan, a2iaReqId);
        A2iARC_CloseDocument			(a2iaDocId);
        A2iARC_CloseDocumentTable		(&a2iaTbl);
        A2iARC_CloseChannel				(a2iaChan);
        return							false;
    }

    A2iARC_CheckOutput		a2iaCheOut	= a2iaOut.documentTypeInfo.CaseCheck.check;
    setResult							("Score",		QString::number(a2iaCheOut.result.score));
    setResult							("Amount",		QString::number(a2iaCheOut.result.amount/100.0f, 'f', 2));
    setResult							("Signature",	a2iaCheOut.signature.answer ? "1" : "0" );

    // Free resources.
    A2iARC_CloseRequest					(a2iaChan, a2iaReqId);
    A2iARC_CloseDocument				(a2iaDocId);
    A2iARC_CloseDocumentTable			(&a2iaTbl);
    A2iARC_CloseChannel					(a2iaChan);
    return								true;
}

QString REPlugs::A2iA::A2iARecognizer::a2iaStringError (int e) const {
    static char				msg[256];
    A2iARC_GetErrorText     (e, msg, 256);
    return					QString(msg);

    return                  "";
}
