#include "Helper.h"
#include "../../../../Versioning/REPlugA2iAVersion.h"
// Required stuff.
#include "Analyzers/Recognizers/A2iARecognizer.h"
#include <QtCore/qplugin.h>
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>
#include <QtCore/QFile>

REPlugs::A2iA::Helper::Helper (QObject *p)
: RE::AbstractPlugin(p) {
	// Extract required DLL resources.
    Dbg						<< "Constructing with version" << STRINGIZE(REPlugA2iAVersion) << "...";
	foreach (QString const &dep, QStringList() << "A2iACheckReader")
		if (!QFileInfo(QString("./%1.dll").arg(dep)).exists())
			QFile::copy						(QString(":/Resources/%1.dll").arg(dep), QString("./%1.dll").arg(dep));
}

RE::PluginBaseInfos REPlugs::A2iA::Helper::baseInfos () const {
    RE::PluginBaseInfos             bi;
    bi.name                         = "REPlugA2iA";
    bi.version                      = STRINGIZE(REPlugA2iAVersion);
    bi.description                  = tr("Provides a way to recognize handwitten texts on documents using the A2iA architecture.");
    return                          bi;
}
// Plugin functionality.
RE::PluginMetaObjectList REPlugs::A2iA::Helper::filters () const {
    return                          RE::PluginMetaObjectList();
}
RE::PluginMetaObjectList REPlugs::A2iA::Helper::analyzers () const {
    return                          RE::PluginMetaObjectList();
}
RE::PluginMetaObjectList REPlugs::A2iA::Helper::recognizers () const {
    RE::PluginMetaObjectList    lst;
    lst                             << &REPlugs::A2iA::A2iARecognizer::staticMetaObject;
    return                          lst;
}

Q_EXPORT_PLUGIN2(REPlugA2iA, REPlugs::A2iA::Helper)
