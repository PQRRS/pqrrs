# Base settings.
include         (../../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   REPlugA2iA
TEMPLATE        =   lib
CONFIG          +=  plugin
QT              +=  gui

include         (../../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                        install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/libRELibrary.dylib" \
                                "$${DESTDIR}/lib$${TARGET}.dylib"
}

target.path     =   $${INSTALLDIR}/lib
INSTALLS        +=  target

VERSION             =   $${REPlugA2iAVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

win32 {
    LIBS        +=  -L"$${PWD}/ThirdParty/win32/lib/$$ARCH" \
                    -lA2iACheckReader
}

HEADERS         +=  src/Analyzers/Recognizers/A2iARecognizer.h \
                    src/Helper.h
SOURCES         +=  src/Analyzers/Recognizers/A2iARecognizer.cpp \
                    src/Helper.cpp
