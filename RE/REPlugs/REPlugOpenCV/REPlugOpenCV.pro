# Base settings.
include         (../../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   REPlugOpenCV
TEMPLATE        =   lib
CONFIG          +=  plugin
QT              +=  gui

include         (../../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                        install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/../../lib//libRELibrary.dylib" \
                                "$${DESTDIR}/lib$${TARGET}.dylib"
}

# OpenCV includes and libs.
unix|linux: {
    INCLUDEPATH     +=  /opt/PQRRS/include
    LIBS            +=  -L/opt/PQRRS/lib -lopencv_core -lopencv_imgproc
}
win32 {
    INCLUDEPATH     +=  $${BASEDIR}/Dependencies/opencv-2.4.9/include
    # Based on the configuration name, either link against debug or release OpenCV libraries.
    CONFIG(debug, debug|release) {
        LIBS        +=  -L"$${BASEDIR}/Dependencies/opencv-2.4.9/$$ARCH/vc11/lib" -lopencv_core248d -lopencv_imgproc248d
    } else {
        LIBS        +=  -L"$${BASEDIR}/Dependencies/opencv-2.4.9/$$ARCH/vc11/lib" -lopencv_core248 -lopencv_imgproc248
    }
}

target.path         =   $${INSTALLDIR}/plugins/RELibrary
INSTALLS            +=  target

VERSION             =   $${REPlugOpenCVVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

HEADERS             +=  src/Helper.h \
                        src/Filters/AdaptiveHistogramEqualzer.h \
                        src/Filters/BlurFilter.h \
                        src/Filters/BrightnessContrastFilter.h \
                        src/Filters/CannyEdgesFinder.h \
                        src/Filters/GaussianBlur.h \
                        src/Filters/HistogramEqualizer.h \
                        src/Filters/HoughTiltCorrector.h \
                        src/Filters/LaplacianFilter.h \
                        src/Filters/LocalThresholder.h \
                        src/Filters/MedianFilter.h \
                        src/Filters/ThinningFilter.h \
                        src/Filters/Thresholder.h

SOURCES             +=  src/Helper.cpp \
                        src/Filters/AdaptiveHistogramEqualzer.cpp \
                        src/Filters/BlurFilter.cpp \
                        src/Filters/BrightnessContrastFilter.cpp \
                        src/Filters/CannyEdgesFinder.cpp \
                        src/Filters/GaussianBlur.cpp \
                        src/Filters/HistogramEqualizer.cpp \
                        src/Filters/HoughTiltCorrector.cpp \
                        src/Filters/LaplacianFilter.cpp \
                        src/Filters/LocalThresholder.cpp \
                        src/Filters/MedianFilter.cpp \
                        src/Filters/ThinningFilter.cpp \
                        src/Filters/Thresholder.cpp

