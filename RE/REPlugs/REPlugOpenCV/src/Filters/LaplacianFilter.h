#ifndef REPlug_OpenCV_LaplacianFilter_h
#define REPlug_OpenCV_LaplacianFilter_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
#include "../Helper.h"
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class LaplacianFilter : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(quint32  apertureRadius  READ apertureRadius WRITE setApertureRadius RESET resetApertureRadius)
    Q_PROPERTY(double   scaleFactor     READ scaleFactor    WRITE setScaleFactor    RESET resetScaleFactor)
    Q_PROPERTY(double   delta           READ delta          WRITE setDelta          RESET resetDelta)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				LaplacianFilter             (QObject *p=0);
    // Accessors.
    quint32 const&          apertureRadius              () const            { return _apertureRadius; }
    void                    setApertureRadius           (quint32 const &v)  { _apertureRadius = v; }
    void                    resetApertureRadius         ()                  { _apertureRadius = 1; }
    double const&           scaleFactor                 () const            { return _scaleFactor; }
    void                    setScaleFactor              (double const &v)   { _scaleFactor = v; }
    void                    resetScaleFactor            ()                  { _scaleFactor = 1; }
    double const&           delta                       () const            { return _delta; }
    void                    setDelta                    (double const &v)   { _delta = v; }
    void                    resetDelta                  ()                  { _delta = 0; }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    quint32                 _apertureRadius;
    double                  _scaleFactor,
                            _delta;
};

}
}

#endif
