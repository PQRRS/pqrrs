#ifndef REPlug_OpenCV_MedianFilter_h
#define REPlug_OpenCV_MedianFilter_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class MedianFilter : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(quint32  apertureRadius  READ apertureRadius WRITE setApertureRadius RESET resetApertureRadius)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				MedianFilter                      (QObject *p=0);
    // Accessors.
    quint32 const&          apertureRadius              () const            { return _apertureRadius; }
    void                    setApertureRadius           (quint32 const &v)  { _apertureRadius = v; }
    void                    resetApertureRadius         ()                  { _apertureRadius = 1; }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    quint32                 _apertureRadius;
};

}
}

#endif
