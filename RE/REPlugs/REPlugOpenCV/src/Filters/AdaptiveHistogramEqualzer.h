#ifndef REPlug_OpenCV_AdaptiveHistogramEqualzer_h
#define REPlug_OpenCV_AdaptiveHistogramEqualzer_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class AdaptiveHistogramEqualzer : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(double   clipLimit       READ clipLimit      WRITE setClipLimit      RESET resetClipLimit)
    Q_PROPERTY(quint32  horizontalSize  READ horizontalSize WRITE setHorizontalSize RESET resetHorizontalSize)
    Q_PROPERTY(quint32  verticalSize    READ verticalSize   WRITE setVerticalSize   RESET resetVerticalSize)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				AdaptiveHistogramEqualzer   (QObject *p=0);
    // Accessors.
    double const&           clipLimit                   () const            { return _clipLimit; }
    void                    setClipLimit                (double const &v)   { _clipLimit = v; }
    void                    resetClipLimit              ()                  { _clipLimit = 40.0; }
    quint32 const&          horizontalSize              () const            { return _horizontalSize; }
    void                    setHorizontalSize           (quint32 const &v)  { _horizontalSize = v; }
    void                    resetHorizontalSize         ()                  { _horizontalSize = 8; }
    quint32 const&          verticalSize                () const            { return _verticalSize; }
    void                    setVerticalSize             (quint32 const &v)  { _verticalSize = v; }
    void                    resetVerticalSize           ()                  { _verticalSize = 8; }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    double                  _clipLimit;
    quint32                 _horizontalSize,
                            _verticalSize;
};

}
}

#endif
