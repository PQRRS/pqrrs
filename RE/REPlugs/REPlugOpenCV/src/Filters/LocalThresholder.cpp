#include "LocalThresholder.h"
#include "../Helper.h"
#include <RELibrary/PixelHelper>
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::LocalThresholder::LocalThresholder (QObject *p)
: RE::AbstractFilter(p) {
    resetMethod     ();
    resetRadius     ();
    resetC          ();
}
QImage REPlugs::OpenCV::LocalThresholder::doApply () {
    // Store source.
    QImage          qSrc        = source();
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter only processes grayscale images, conversion will merge channels!";
        qSrc                    = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }
    // Convert source to cv::Mat format.
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&qSrc);

    cv::adaptiveThreshold       (src, src, 255, _method, CV_THRESH_BINARY, _radius*2+1, _c);
    return REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::LocalThresholder::minimumImageSize () const {
    return QSize(3, 3);
}
