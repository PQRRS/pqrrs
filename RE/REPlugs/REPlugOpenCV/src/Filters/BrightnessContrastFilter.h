#ifndef REPlug_OpenCV_BrightnessContrastFilter_h
#define REPlug_OpenCV_BrightnessContrastFilter_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class BrightnessContrastFilter : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(RE::T::Ratio     contrast    READ contrast   WRITE setContrast   RESET resetContrast)
    Q_PROPERTY(RE::T::Ratio     brightness  READ brightness WRITE setBrightness RESET resetBrightness)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				BrightnessContrastFilter    (QObject *p=0);
    // Accessors.
    RE::T::Ratio const&     contrast                    () const                { return _contrast; }
    void                    setContrast                 (RE::T::Ratio const &v) { _contrast = v; }
    void                    resetContrast               ()                      { _contrast = 0.; }
    RE::T::Ratio const&     brightness                  () const                { return _brightness; }
    void                    setBrightness               (RE::T::Ratio const &v) { _brightness = v; }
    void                    resetBrightness             ()                      { _brightness = 0.; }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    RE::T::Ratio            _contrast,
                            _brightness;
};

}
}

#endif
