#ifndef REPlug_OpenCV_HoughTiltCorrector_h
#define REPlug_OpenCV_HoughTiltCorrector_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class HoughTiltCorrector : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(RE::T::ZoneFilterData::List
                        filterStack     READ filterStack    WRITE setFilterStack    RESET resetFilterStack)
    Q_PROPERTY(double   rho             READ rho            WRITE setRho            RESET resetRho)
    Q_PROPERTY(double   theta           READ theta          WRITE setTheta          RESET resetTheta)
    Q_PROPERTY(quint32  threshold       READ threshold      WRITE setThreshold      RESET resetThreshold)
    Q_PROPERTY(quint32  minLineLength   READ minLineLength  WRITE setMinLineLength  RESET resetMinLineLength)
    Q_PROPERTY(quint32  maxLineGap      READ maxLineGap     WRITE setMaxLineGap     RESET resetMaxLineGap)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				HoughTiltCorrector          (QObject *p=0);
    // Accessors.
    RE::T::ZoneFilterData::List const&
                            filterStack                 () const                                { return _filterStack; }
    void                    setFilterStack              (RE::T::ZoneFilterData::List const &v)  { _filterStack = v; }
    void                    resetFilterStack            ()                                      { _filterStack.clear(); }
    double const&           rho                         () const                { return _rho; }
    void                    setRho                      (double const &v)       { _rho = v; }
    void                    resetRho                    ()                      { _rho = 1.0; }
    double const&           theta                       () const                { return _theta; }
    void                    setTheta                    (double const &v)       { _theta = v; }
    void                    resetTheta                  ()                      { _theta = 180.0; }
    quint32 const&          threshold                   () const                { return _threshold; }
    void                    setThreshold                (quint32 const &v)      { _threshold = v; }
    void                    resetThreshold              ()                      { _threshold = 128; }
    quint32 const&          minLineLength               () const                { return _minLineLength; }
    void                    setMinLineLength            (quint32 const &v)      { _minLineLength = v; }
    void                    resetMinLineLength          ()                      { _minLineLength = 0; }
    quint32 const&          maxLineGap                  () const                { return _maxLineGap; }
    void                    setMaxLineGap               (quint32 const &v)      { _maxLineGap = v; }
    void                    resetMaxLineGap             ()                      { _maxLineGap = 0; }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    RE::T::ZoneFilterData::List
                            _filterStack;
    double                  _rho,
                            _theta;
    quint32                 _threshold,
                            _minLineLength,
                            _maxLineGap;
};

}
}

#endif
