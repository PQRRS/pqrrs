#include "BrightnessContrastFilter.h"
#include <RELibrary/Engine>
#include "../Helper.h"
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::BrightnessContrastFilter::BrightnessContrastFilter (QObject *p)
: RE::AbstractFilter(p) {
    resetContrast   ();
    resetBrightness ();
}
QImage REPlugs::OpenCV::BrightnessContrastFilter::doApply () {
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&source());
    double const    ccCoef      = _contrast+1.0;
    double const    cbCoef      = 255.0*_brightness;
    src.convertTo               (src, -1, ccCoef, cbCoef);
    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::BrightnessContrastFilter::minimumImageSize () const {
    return QSize(1, 1);
}
