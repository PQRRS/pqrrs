#ifndef REPlug_OpenCV_HistogramEqualizer_h
#define REPlug_OpenCV_HistogramEqualizer_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class HistogramEqualizer : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				HistogramEqualizer          (QObject *p=0);

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    quint32                 _horizontalSize,
                            _verticalSize;
    double                  _sigmaX,
                            _sigmaY;
};

}
}

#endif
