#ifndef REPlug_OpenCV_CannyEdgesFinder_h
#define REPlug_OpenCV_CannyEdgesFinder_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class CannyEdgesFinder : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(double threshold1        READ threshold1         WRITE setThreshold1         RESET resetThreshold1)
    Q_PROPERTY(double threshold2        READ threshold2         WRITE setThreshold2         RESET resetThreshold2)
    Q_PROPERTY(qint32 apertureRadius    READ apertureRadius     WRITE setApertureRadius     RESET resetApertureRadius)
    Q_PROPERTY(bool level2Gradients     READ level2Gradients    WRITE setLevel2Gradients    RESET resetLevel2Gradients)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				CannyEdgesFinder            (QObject *p=0);
    // Accessors.
    void                    resetFilterStack            ()                                      { _filterStack = RE::T::ZoneFilterData::List(); }
    double const&           threshold1                  () const            { return _threshold1; }
    void                    setThreshold1               (double const &v)   { _threshold1 = v; }
    void                    resetThreshold1             ()                  { _threshold1 = 120; }
    double const&           threshold2                  () const            { return _threshold2; }
    void                    setThreshold2               (double const &v)   { _threshold2 = v; }
    void                    resetThreshold2             ()                  { _threshold2 = 220; }
    qint32 const&           apertureRadius              () const            { return _apertureRadius; }
    void                    setApertureRadius           (qint32 const &v)   { _apertureRadius = qMin(3, qMax(0, v)); }
    void                    resetApertureRadius         ()                  { _apertureRadius = 1; }
    bool const&             level2Gradients             () const            { return _level2Gradients; }
    void                    setLevel2Gradients          (bool const &v)     { _level2Gradients = v; }
    void                    resetLevel2Gradients        ()                  { _level2Gradients = false; }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    RE::T::ZoneFilterData::List
                            _filterStack;
    double                  _threshold1,
                            _threshold2;
    qint32                  _apertureRadius;
    bool                    _level2Gradients;
};

}
}

#endif
