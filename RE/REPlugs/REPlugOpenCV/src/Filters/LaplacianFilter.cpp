#include "LaplacianFilter.h"
#include <RELibrary/Engine>
#include "../Helper.h"
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::LaplacianFilter::LaplacianFilter (QObject *p)
: RE::AbstractFilter(p) {
    resetApertureRadius ();
    resetScaleFactor    ();
    resetDelta          ();
}
QImage REPlugs::OpenCV::LaplacianFilter::doApply () {
    // Store source.
    QImage          qSrc        = source();
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter only processes grayscale images, conversion will merge channels!";
        qSrc                    = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }
    // Convert source to cv::Mat format.
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&qSrc);

    cv::Mat         dst;
    cv::Laplacian               (src, dst, CV_16S, (_apertureRadius*2)+1, _scaleFactor, _delta, cv::BORDER_REPLICATE);
    cv::Mat         absDst;
    convertScaleAbs             (dst, absDst);
    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&absDst);
}
QSize REPlugs::OpenCV::LaplacianFilter::minimumImageSize () const {
    return QSize(3, 3);
}
