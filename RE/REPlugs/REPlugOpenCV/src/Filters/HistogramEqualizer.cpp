#include "HistogramEqualizer.h"
#include "../Helper.h"
#include <RELibrary/Engine>
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::HistogramEqualizer::HistogramEqualizer (QObject *p)
: RE::AbstractFilter(p) {
}
QImage REPlugs::OpenCV::HistogramEqualizer::doApply () {
    // Store source.
    QImage          qSrc        = source();
    // Convert source to cv::Mat format.
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&qSrc);

    if (src.channels()==1)
        cv::equalizeHist        (src, src);
    else {
        cv::vector<cv::Mat>
                    chans;
        cv::Mat     YCbCr;
        cv::cvtColor            (src, YCbCr, CV_BGR2YCrCb);
        cv::split               (YCbCr, chans);
        cv::equalizeHist        (chans[0], chans[0]);
        cv::merge               (chans, YCbCr);
        cv::cvtColor            (YCbCr, src, CV_YCrCb2BGR);
        //#endif
    }
    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::HistogramEqualizer::minimumImageSize () const {
    return QSize(10, 10);
}
