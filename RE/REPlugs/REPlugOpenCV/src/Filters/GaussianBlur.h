#ifndef REPlug_OpenCV_GaussianBlur_h
#define REPlug_OpenCV_GaussianBlur_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class GaussianBlur : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(quint32  horizontalSize  READ horizontalSize WRITE setHorizontalSize RESET resetHorizontalSize)
    Q_PROPERTY(quint32  verticalSize    READ verticalSize   WRITE setVerticalSize   RESET resetVerticalSize)
    Q_PROPERTY(double   sigmaX          READ sigmaX         WRITE setSigmaX         RESET resetSigmaX)
    Q_PROPERTY(double   sigmaY          READ sigmaY         WRITE setSigmaY         RESET resetSigmaY)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				GaussianBlur                (QObject *p=0);
    // Accessors.
    quint32 const&          horizontalSize              () const            { return _horizontalSize; }
    void                    setHorizontalSize           (quint32 const &v)  { _horizontalSize = v; }
    void                    resetHorizontalSize         ()                  { _horizontalSize = 1; }
    quint32 const&          verticalSize                () const            { return _verticalSize; }
    void                    setVerticalSize             (quint32 const &v)  { _verticalSize = v; }
    void                    resetVerticalSize           ()                  { _verticalSize = 1; }
    double const&           sigmaX                      () const            { return _sigmaX; }
    void                    setSigmaX                   (double const &v)   { _sigmaX = v; }
    void                    resetSigmaX                 ()                  { _sigmaX = 0; }
    double const&           sigmaY                      () const            { return _sigmaY; }
    void                    setSigmaY                   (double const &v)   { _sigmaY = v; }
    void                    resetSigmaY                 ()                  { _sigmaY = 0; }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    quint32                 _horizontalSize,
                            _verticalSize;
    double                  _sigmaX,
                            _sigmaY;
};

}
}

#endif
