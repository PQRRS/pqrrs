#include "GaussianBlur.h"
#include "../Helper.h"
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::GaussianBlur::GaussianBlur (QObject *p)
: RE::AbstractFilter(p) {
    resetHorizontalSize ();
    resetVerticalSize   ();
    resetSigmaX         ();
    resetSigmaY         ();
}
QImage REPlugs::OpenCV::GaussianBlur::doApply () {
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&source());
    cv::GaussianBlur            (src, src, cv::Size((_horizontalSize*2)+1, (_verticalSize*2)+1), _sigmaX, _sigmaY, cv::BORDER_REPLICATE);
    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::GaussianBlur::minimumImageSize () const {
    return QSize(10, 10);
}
