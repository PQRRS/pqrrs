#include "MedianFilter.h"
#include "../Helper.h"
#include <RELibrary/PixelHelper>
#include <RELibrary/Filters/TiltCorrector>
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::MedianFilter::MedianFilter (QObject *p)
: RE::AbstractFilter(p) {
    resetApertureRadius ();
}
QImage REPlugs::OpenCV::MedianFilter::doApply () {
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&source());
    cv::medianBlur              (src, src, (_apertureRadius*2)+1);
    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::MedianFilter::minimumImageSize () const {
    return QSize(3, 3);
}
