#ifndef REPlug_OpenCV_Thresholder_h
#define REPlug_OpenCV_Thresholder_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
#include "../Helper.h"
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class Thresholder : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)

    Q_PROPERTY(REPlugs::OpenCV::E::ThresholdingType type
                                        READ type       WRITE setType       RESET resetType)
    Q_PROPERTY(double       threshold   READ threshold  WRITE setThreshold  RESET resetThreshold)
    Q_PROPERTY(double       maxValue    READ maxValue   WRITE setMaxValue   RESET resetMaxValue)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				Thresholder                 (QObject *p=0);
    // Accessors.
    E::ThresholdingType     type                        () const                    { return _type; }
    void                    setType                     (E::ThresholdingType v)     { _type = v; }
    void                    resetType                   ()                          { _type = REPlugs::OpenCV::E::ThresholdingTypeOtsu; }
    double const&           threshold                   () const                    { return _threshold; }
    void                    setThreshold                (double const &v)           { _threshold = v; }
    void                    resetThreshold              ()                          { _threshold = 128; }
    double const&           maxValue                    () const                    { return _maxValue; }
    void                    setMaxValue                 (double const &v)           { _maxValue = v; }
    void                    resetMaxValue               ()                          { _maxValue = 255; }

protected:
    void                    thinningIteration           (QImage *dest, int iter);
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    E::ThresholdingType     _type;
    double                  _threshold;
    double                  _maxValue;
};

}
}

#endif
