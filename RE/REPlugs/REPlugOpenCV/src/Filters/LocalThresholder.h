#ifndef REPlug_OpenCV_LocalThresholder_h
#define REPlug_OpenCV_LocalThresholder_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
#include "../Helper.h"
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class LocalThresholder : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(REPlugs::OpenCV::E::ThresholdingMethod
                            method  READ method WRITE setMethod RESET resetMethod)
    Q_PROPERTY(quint32      radius  READ radius WRITE setRadius RESET resetRadius)
    Q_PROPERTY(quint32      c       READ c      WRITE setC      RESET resetC)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				LocalThresholder            (QObject *p=0);
    // Accessors.
    E::ThresholdingMethod   method                      () const                    { return _method; }
    void                    setMethod                   (E::ThresholdingMethod v)   { _method = v; }
    void                    resetMethod                 ()                          { _method = E::ThresholdingMethodMean; }
    quint32                 radius                      () const                    { return _radius; }
    void                    setRadius                   (quint32 const &v)          { _radius = v; }
    void                    resetRadius                 ()                          { _radius = 1; }
    quint32                 c                           () const                    { return _c; }
    void                    setC                        (quint32 const &v)          { _c = v; }
    void                    resetC                      ()                          { _c = 10; }

protected:
    void                    thinningIteration           (QImage *dest, int iter);
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    E::ThresholdingMethod   _method;
    quint32                 _radius;
    quint32                 _c;
};

}
}

#endif
