#include "Thresholder.h"
#include "../Helper.h"
#include <RELibrary/PixelHelper>
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::Thresholder::Thresholder (QObject *p)
: RE::AbstractFilter(p) {
    resetType       ();
    resetThreshold  ();
    resetMaxValue   ();
}
QImage REPlugs::OpenCV::Thresholder::doApply () {
    // Store source.
    QImage          qSrc        = source();
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter only processes grayscale images, conversion will merge channels!";
        qSrc                    = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }
    // Convert source to cv::Mat format.
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&qSrc);
    cv::threshold               (src, src, _threshold, _maxValue, _type);
    return REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::Thresholder::minimumImageSize () const {
    return QSize(3, 3);
}
