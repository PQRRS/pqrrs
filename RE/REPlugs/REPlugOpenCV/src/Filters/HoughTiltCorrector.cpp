#include "HoughTiltCorrector.h"
#include "../Helper.h"
#include <RELibrary/Engine>
#include <RELibrary/Filters/TiltCorrector>
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::HoughTiltCorrector::HoughTiltCorrector (QObject *p)
: RE::AbstractFilter(p) {
    resetFilterStack    ();
    resetRho            ();
    resetTheta          ();
    resetThreshold      ();
    resetMinLineLength  ();
    resetMaxLineGap     ();
}
QImage REPlugs::OpenCV::HoughTiltCorrector::doApply () {
    // Retrieve runtime context pointer.
    RE::RuntimeContext const
                    &ctx        = runtimeContext();
    // Filter source image with input preprocessing stack.
    QImage          qSrc        = ctx.engine->filteredImage(_filterStack, ctx.name, source(), parallelizedCoresCount());
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter requires a grayscale image as the result of it's input filter stack, conversion will merge channels!";
        qSrc                    = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }
    // Convert source to cv::Mat format.
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&qSrc);

    cv::vector<cv::Vec4i>
                    lines;
    cv::Vec4i       longLine;
    float           longest     = 0;
    cv::HoughLinesP             (src, lines, _rho, CV_PI/_theta, _threshold, _minLineLength, _maxLineGap);
    for (size_t i=0; i<lines.size(); ++i) {
        cv::Vec4i   line        = lines[i];
        cv::Point   delta       = cv::Point(line[0], line[1]) - cv::Point(line[2], line[3]);
        if (abs(delta.x)<abs(delta.y))
            continue;
        float       dist        = sqrt((float)(delta.x*delta.x + delta.y*delta.y));
        if (dist>longest) {
            longest             = dist;
            longLine            = line;
        }
    }
    if (debuggingEnabled()) {
        cv::cvtColor            (src, src, CV_GRAY2BGR );
        cv::line                (src, cv::Point(longLine[0], longLine[1]), cv::Point(longLine[2], longLine[3]), cv::Scalar(0,0,255), 1, 1);
        setDebuggingImage       (REPlugs::OpenCV::Helper::cvMat2qImage(&src));
    }

    RE::TiltCorrector   tc;
    tc.setAngle                 (atan2((float)(longLine[3]-longLine[1]), (float)(longLine[2]-longLine[0])));
    tc.setMode                  (RE::E::TransformModeSmooth);
    tc.setFraming               (Qt::white);
    tc.setSource                (source());
    return tc.apply();
}
QSize REPlugs::OpenCV::HoughTiltCorrector::minimumImageSize () const {
    return QSize(3, 3);
}
