#ifndef REPlug_OpenCV_ThinningFilter_h
#define REPlug_OpenCV_ThinningFilter_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Forward declarations.
namespace cv { class Mat; }

namespace REPlugs {
namespace OpenCV {

/*! @brief Image Thinning Filter Class.
 * @todo Documentation. */
class ThinningFilter : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    //! @brief Brightness Factor Property.
    Q_PROPERTY(RE::T::GrayValue threshold   READ threshold  WRITE setThreshold  RESET resetThreshold)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				ThinningFilter              (QObject *p=0);
    // Accessors.
    RE::T::GrayValue const& threshold                   () const                    { return _threshold; }
    void                    setThreshold                (RE::T::GrayValue const &v) { _threshold = v; }
    void                    resetThreshold              ()                          { _threshold = 128; }

protected:
    void                    thinningIteration           (QImage *dest, int iter);
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    void                    thinningIteration           (cv::Mat *im, int iter);
    RE::T::GrayValue        _threshold;
};

}
}

#endif
