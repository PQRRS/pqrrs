#include "BlurFilter.h"
#include <RELibrary/Engine>
#include "../Helper.h"
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::BlurFilter::BlurFilter (QObject *p)
: RE::AbstractFilter(p) {
    resetHorizontalSize ();
    resetVerticalSize   ();
}
QImage REPlugs::OpenCV::BlurFilter::doApply () {
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&source());
    cv::blur                    (src, src, cv::Size(_horizontalSize, _verticalSize), cv::Point(-1, -1), cv::BORDER_REPLICATE);
    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::BlurFilter::minimumImageSize () const {
    return QSize(3, 3);
}
