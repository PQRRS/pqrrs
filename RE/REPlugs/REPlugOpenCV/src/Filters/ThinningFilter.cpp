#include "ThinningFilter.h"
#include "../Helper.h"
#include <RELibrary/PixelHelper>
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::ThinningFilter::ThinningFilter (QObject *p)
: RE::AbstractFilter(p) {
    resetThreshold      ();
}
QImage REPlugs::OpenCV::ThinningFilter::doApply () {
    // Store source.
    QImage          qSrc        = source();
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter only processes grayscale images, conversion will merge channels!";
        qSrc                    = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }
    // Convert source to cv::Mat format.
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&qSrc);

    // Binary threshold image.
    cv::threshold               (255-src, src, _threshold, 255, CV_THRESH_BINARY);

    // Force binaries values.
    src                         /= 255;
    cv::Mat         diff, prev  = cv::Mat::zeros(src.size(), CV_8UC1);
    do {
        thinningIteration       (&src, 0);
        thinningIteration       (&src, 1);
        cv::absdiff             (src, prev, diff);
        src.copyTo              (prev);
    } while (cv::countNonZero(diff)>0);
    src                         = 255 - src*255;

    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::ThinningFilter::minimumImageSize () const {
    return QSize(3, 3);
}

void REPlugs::OpenCV::ThinningFilter::thinningIteration (cv::Mat *imPtr, int iter) {
    cv::Mat         &im = *imPtr;
    cv::Mat         m   = cv::Mat::zeros(im.size(), CV_8UC1);
    for (int i=1; i<im.rows-1; ++i) {
        for (int j=1; j<im.cols-1; ++j) {
            uchar   p2  = im.at<uchar>(i-1, j);
            uchar   p3  = im.at<uchar>(i-1, j+1);
            uchar   p4  = im.at<uchar>(i,   j+1);
            uchar   p5  = im.at<uchar>(i+1, j+1);
            uchar   p6  = im.at<uchar>(i+1, j);
            uchar   p7  = im.at<uchar>(i+1, j-1);
            uchar   p8  = im.at<uchar>(i,   j-1);
            uchar   p9  = im.at<uchar>(i-1, j-1);

            int     A   = (p2 == 0 && p3 == 1) + (p3 == 0 && p4 == 1) +
                            (p4 == 0 && p5 == 1) + (p5 == 0 && p6 == 1) +
                            (p6 == 0 && p7 == 1) + (p7 == 0 && p8 == 1) +
                            (p8 == 0 && p9 == 1) + (p9 == 0 && p2 == 1);
            int     B   = p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
            int     m1  = iter == 0 ? (p2 * p4 * p6) : (p2 * p4 * p8);
            int     m2  = iter == 0 ? (p4 * p6 * p8) : (p2 * p6 * p8);

            if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
                m.at<uchar>(i,j) = 1;
        }
    }
    im                  &= ~m;
}
