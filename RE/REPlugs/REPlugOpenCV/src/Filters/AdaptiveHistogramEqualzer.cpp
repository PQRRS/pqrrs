#include "AdaptiveHistogramEqualzer.h"
#include "../Helper.h"
#include <RELibrary/Engine>
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::AdaptiveHistogramEqualzer::AdaptiveHistogramEqualzer (QObject *p)
: RE::AbstractFilter(p) {
    resetClipLimit      ();
    resetHorizontalSize ();
    resetVerticalSize   ();
}
QImage REPlugs::OpenCV::AdaptiveHistogramEqualzer::doApply () {
    // Store source.
    QImage          qSrc        = source();
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter only processes grayscale images, conversion will merge channels!";
        qSrc                    = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }
    // Convert source to cv::Mat format.
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&qSrc);

    cv::Ptr<cv::CLAHE>
                    clahe       = cv::createCLAHE(_clipLimit, cv::Size(_horizontalSize, _verticalSize));
    clahe->apply                (src, src);
    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&src);
}
QSize REPlugs::OpenCV::AdaptiveHistogramEqualzer::minimumImageSize () const {
    return QSize(10, 10);
}
