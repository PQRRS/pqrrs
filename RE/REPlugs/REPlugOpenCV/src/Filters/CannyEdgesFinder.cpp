#include "CannyEdgesFinder.h"
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include "../Helper.h"
#include <QtCore/QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

REPlugs::OpenCV::CannyEdgesFinder::CannyEdgesFinder (QObject *p)
: RE::AbstractFilter(p) {
    resetThreshold1         ();
    resetThreshold2         ();
    resetApertureRadius     ();
    resetLevel2Gradients    ();
}
QImage REPlugs::OpenCV::CannyEdgesFinder::doApply () {
    // Store source.
    QImage          qSrc        = source();
    // Force grayscale conversion for this filter.
    if (!qSrc.isGrayscale()) {
        Wrn                     << "This filter only processes grayscale images, conversion will merge channels!";
        qSrc                    = RE::Engine::paletizeIfGrayscale(qSrc, true);
    }
    // Convert source to cv::Mat format.
    cv::Mat         src         = REPlugs::OpenCV::Helper::qImage2cvMat(&qSrc);

    // Detect edges via a Canny operator..
    cv::Canny                   (src, src, _threshold1, _threshold2, (_apertureRadius*2)+1, _level2Gradients);
    // Convert the result to QImage, return.
    return                      REPlugs::OpenCV::Helper::cvMat2qImage(&src);;
}
QSize REPlugs::OpenCV::CannyEdgesFinder::minimumImageSize () const {
    return QSize(10, 10);
}
