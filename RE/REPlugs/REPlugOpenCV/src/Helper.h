#ifndef REPlugs_FujiTools_Helper_h
#define REPlugs_FujiTools_Helper_h

#include <RELibrary/RETypes>
#include <RELibrary/Plugins/AbstractPlugin>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace REPlugs {

namespace OpenCV {

// ------------------------------------				Enums			------------------------------------ //
class E {
Q_GADGET
public:
Q_ENUMS	(First ThresholdingMethod ThresholdingType Depth Last)
    //! @brief First Enumeration Value Marker. Shouldn't be used directly.
    enum		First				{ FirstValue };
    //! @brief Thresholding methods.
    enum		ThresholdingMethod  { ThresholdingMethodMean = CV_ADAPTIVE_THRESH_MEAN_C, ThresholdingMethodGaussian = CV_ADAPTIVE_THRESH_GAUSSIAN_C };
    //! @brief Thresholding types.
    enum		ThresholdingType    { ThresholdingTypeBinary = CV_THRESH_BINARY, ThresholdingTypeBinaryInverted = CV_THRESH_BINARY_INV, ThresholdingTypeTruncate = CV_THRESH_TRUNC, ThresholdingTypeToZero = CV_THRESH_TOZERO, ThresholdingTypeToZeroInverted = CV_THRESH_TOZERO_INV, ThresholdingTypeOtsu = CV_THRESH_OTSU  };
    // Depth for laplacian.
    enum        Depth               { Depth8BitUnsigned = CV_8U, Depth8BitSigned = CV_8S, Depth16BitUnsigned = CV_16U, Depth16BitSigned = CV_16S, Depth32BitSigned = CV_32S, Depth32BitFloat = CV_32F, Depth64BitFloat = CV_64F };
    //! @brief Last Enumeration Value Marker. Shouldn't be used directly.
    enum		Last				{ LastValue };
    RegisterEnumStream	(REPlugs::OpenCV::E::First)
    RegisterEnumStream	(REPlugs::OpenCV::E::ThresholdingMethod)
    RegisterEnumStream	(REPlugs::OpenCV::E::ThresholdingType)
    RegisterEnumStream	(REPlugs::OpenCV::E::Depth)
    RegisterEnumStream	(REPlugs::OpenCV::E::Last)
};

class Helper : public RE::AbstractPlugin {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractPlugin)

public:
    Q_INVOKABLE                 Helper                          (QObject *p=0);
    // Plugin information strings.
    RE::PluginBaseInfos			baseInfos                       () const;
	// Plugin functionality.
    RE::PluginMetaObjectList	filters                         () const;
    RE::PluginMetaObjectList	analyzers                       () const;
    RE::PluginMetaObjectList	recognizers                     () const;

    //! @brief QImage to OpenCV::Mat Conversion.
    static cv::Mat const        qImage2cvMat                    (QImage const *qImg);
    //! @brief OpenCV::Mat to QImage Conversion.
    static QImage               cvMat2qImage                    (cv::Mat const *cvMat);
};

}

}

// Enums.
Q_DECLARE_TYPEINFO(REPlugs::OpenCV::E::First, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(REPlugs::OpenCV::E::First)
Q_DECLARE_TYPEINFO(REPlugs::OpenCV::E::ThresholdingMethod, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(REPlugs::OpenCV::E::ThresholdingMethod)
Q_DECLARE_TYPEINFO(REPlugs::OpenCV::E::ThresholdingType, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(REPlugs::OpenCV::E::ThresholdingType)
Q_DECLARE_TYPEINFO(REPlugs::OpenCV::E::Depth, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(REPlugs::OpenCV::E::Depth)
Q_DECLARE_TYPEINFO(REPlugs::OpenCV::E::Last, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(REPlugs::OpenCV::E::Last)

#endif
