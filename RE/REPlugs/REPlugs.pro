TEMPLATE        =   subdirs
CONFIG          +=  ordered

SUBDIRS         +=  REPlugOpenCV REPlugFeaturesFinder  #REPlugFujiTools
win32 {
    contains(ARCH, "x86") {
        SUBDIRS     +=  #REPlugA2iA
    }
}
