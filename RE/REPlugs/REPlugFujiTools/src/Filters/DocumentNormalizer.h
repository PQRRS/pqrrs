#ifndef REPlugs_FujiTools_DocumentNormalizer_h
#define REPlugs_FujiTools_DocumentNormalizer_h

// Base class.
#include <RELibrary/Filters/AbstractFilter>
// Required stuff.
#include <RELibrary/Filters/HistogramStretcher>
#include <RELibrary/Analyzers/RectangleLocator>
#include <QtGui/QColor>

namespace REPlugs {
namespace FujiTools {

class DocumentNormalizer : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    // Stretcher properties.
    Q_PROPERTY(RE::T::GrayValue stretchingLowBoundary   READ lowBoundary	WRITE setLowBoundary    RESET resetLowBoundary  STORED false)
    Q_PROPERTY(RE::T::GrayValue stretchingHighBoundary  READ highBoundary	WRITE setHighBoundary   RESET resetHighBoundary STORED false)
    // First locator properties.
    Q_PROPERTY(quint16          firstLocatorStep        READ firstStep          WRITE setFirstStep          RESET resetFirstStep        STORED false)
    Q_PROPERTY(QColor           firstLocatorLowerLimit  READ firstLowerLimit    WRITE setFirstLowerLimit    RESET resetFirstLowerLimit  STORED false)
    Q_PROPERTY(QColor           firstLocatorUpperLimit  READ firstUpperLimit    WRITE setFirstUpperLimit    RESET resetFirstUpperLimit  STORED false)
    Q_PROPERTY(bool             firstLocatorFromLeft    READ firstFromLeft      WRITE setFirstFromLeft      RESET resetFirstFromLeft    STORED false)
    Q_PROPERTY(bool             firstLocatorFromRight   READ firstFromRight     WRITE setFirstFromRight     RESET resetFirstFromRight   STORED false)
    Q_PROPERTY(bool             firstLocatorFromTop     READ firstFromTop       WRITE setFirstFromTop       RESET resetFirstFromTop     STORED false)
    Q_PROPERTY(bool             firstLocatorFromBottom  READ firstFromBottom    WRITE setFirstFromBottom    RESET resetFirstFromBottom  STORED false)

public:
    // Constructor.
    Q_INVOKABLE						DocumentNormalizer      (QObject *p=0);

    // Stretcher properties.
    RE::T::GrayValue const&			lowBoundary				() const                            { return _firstStretcher.lowBoundary(); }
    void							setLowBoundary			(RE::T::GrayValue const &v)         { _firstStretcher.setLowBoundary(v); }
    void                            resetLowBoundary        ()                                  { _firstStretcher.resetLowBoundary(); }
    RE::T::GrayValue const&			highBoundary			() const                            { return _firstStretcher.highBoundary(); }
    void							setHighBoundary			(RE::T::GrayValue const &v)         { _firstStretcher.setHighBoundary(v); }
    void                            resetHighBoundary       ()                                  { _firstStretcher.resetHighBoundary(); }
    // First step accessors.
    quint16 const&                  firstStep               () const                            { return _firstLocator.step(); }
    void                            setFirstStep            (quint16 const &v)                  { _firstLocator.setStep(v); }
    void                            resetFirstStep          ()                                  { _firstLocator.resetStep(); }
    // Limits accessors.
    QColor const&                   firstLowerLimit         () const                            { return _firstLocator.lowerLimit(); }
    void							setFirstLowerLimit      (QColor const &v)                   { _firstLocator.setLowerLimit(v); }
    void                            resetFirstLowerLimit    ()                                  { _firstLocator.resetLowerLimit(); }
    QColor const&                   firstUpperLimit         () const                            { return _firstLocator.upperLimit(); }
    void							setFirstUpperLimit      (QColor const &v)                   { _firstLocator.setUpperLimit(v); }
    void                            resetFirstUpperLimit    ()                                  { _firstLocator.resetUpperLimit(); }
    // Direction flags setters.
    bool                            firstFromLeft           () const                            { return _firstLocator.fromLeft(); }
    void                            setFirstFromLeft        (bool v)                            { _firstLocator.setFromLeft(v); }
    void                            resetFirstFromLeft      ()                                  { _firstLocator.resetFromLeft(); }
    bool                            firstFromRight          () const                            { return _firstLocator.fromRight(); }
    void                            setFirstFromRight       (bool v)                            { _firstLocator.setFromRight(v); }
    void                            resetFirstFromRight     ()                                  { _firstLocator.resetFromRight(); }
    bool                            firstFromTop            () const                            { return _firstLocator.fromTop(); }
    void                            setFirstFromTop         (bool v)                            { _firstLocator.setFromTop(v); }
    void                            resetFirstFromTop       ()                                  { _firstLocator.resetFromTop(); }
    bool                            firstFromBottom         () const                            { return _firstLocator.fromBottom(); }
    void                            setFirstFromBottom      (bool v)                            { _firstLocator.setFromBottom(v); }
    void                            resetFirstFromBottom    ()                                  { _firstLocator.resetFromBottom(); }

    inline static QImage			Apply					(quint16 const &st, QColor const &ll, QColor const &ul, bool fl, bool fr, bool ft, bool fb, QImage const &src, bool const &fgs=false) {
        DocumentNormalizer fltr;
        fltr.setFirstStep           (st);
        fltr.setFirstLowerLimit     (ll);
        fltr.setFirstUpperLimit     (ul);
        fltr.setFirstFromLeft       (fl);
        fltr.setFirstFromRight      (fr);
        fltr.setFirstFromTop        (ft);
        fltr.setFirstFromBottom     (fb);
        fltr.setSource              (src);
        fltr.setForceGrayscale      (fgs);
        return fltr.apply();
    }

protected:
    virtual QImage					doApply					();
    virtual QSize					minimumImageSize		() const;
    QImage                          apply                   ();

    template<typename K>
    QImage                          algo                    ();

private:
    RE::HistogramStretcher          _firstStretcher;
    RE::RectangleLocator            _firstLocator;
    RE::HistogramStretcher          _secondStretcher;
};

}

}

#endif
