#include "DocumentNormalizer.h"
#include <RELibrary/PixelHelper>
#include <QtCore/QDebug>

REPlugs::FujiTools::DocumentNormalizer::DocumentNormalizer (QObject *p)
: RE::AbstractFilter(p) {
}

QImage REPlugs::FujiTools::DocumentNormalizer::apply () {
   return doApply();
}
QImage REPlugs::FujiTools::DocumentNormalizer::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
        case QImage::Format_ARGB32_Premultiplied:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize REPlugs::FujiTools::DocumentNormalizer::minimumImageSize () const {
    return QSize(3, 3);
}

template<typename K>
QImage REPlugs::FujiTools::DocumentNormalizer::algo () {
    QImage const            &src        = source();
    // Convert part of source image to grayscale, then contrast.
    QImage                  contrasted  = src.copy(src.width()/8, 0, src.width()/2, src.height()/5);
    contrasted                          = RE::Engine::paletizeIfGrayscale(contrasted, true);
    contrasted                          = RE::HistogramStretcher::Apply(45, 70, contrasted, parallelizedCoresCount());
    // Now that we have a contrasted image, try to locate the top edge of the object.
    double                  x1          = 0;
    double                  y1          = 0;
    QList<double>           angles;
    RE::PixelWriter<quint8> pw          (&contrasted);
    for (qint32 x=0; x<pw.width; x+=pw.width/20)
        for (qint32 y=0; y<pw.height; ++y) {
            if (pw.byte(x, y, 0)<=50) {
                if (x1!=0.0 && y1!=0.0)
                    angles              << atan2(y-y1, x-x1);
                x1                      = x;
                y1                      = y;
                break;
            }
        }
    // Compute mean angle.
    double                  angle       = 0;
    foreach (double const &v, angles)   angle += v;
    angle                               /= angles.count();
    // Rotate picture.
    //QImage                  altered     = RE::RotationCorrector::Apply(angle, 255, RE::E::TransformModeSmooth, altered);
    // Store debugging constrasted image if needed.
    //setDebuggingImage                   (contrasted);
    // Return cropped result.
    //return altered;
    return QImage();
}
