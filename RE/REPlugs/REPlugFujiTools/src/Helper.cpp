#include "Helper.h"
#include "../../../../Versioning/REPlugFujiToolsVersion.h"
#include "Filters/DocumentNormalizer.h"
// Required stuff.
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/qplugin.h>

REPlugs::FujiTools::Helper::Helper (QObject *p)
: RE::AbstractPlugin(p) {
}

RE::PluginBaseInfos REPlugs::FujiTools::Helper::baseInfos () const {
    RE::PluginBaseInfos         bi;
    bi.name                         = "REPlugFujiTools";
    bi.version                      = STRINGIZE(REPlugFujiToolsVersion);
    bi.description                  = tr("Helps pre-processing pictures from the Fuji scanner.");
    return                          bi;
}
// Plugin functionality.
RE::PluginMetaObjectList REPlugs::FujiTools::Helper::filters () const {
    RE::PluginMetaObjectList    lst;
    lst                             << &REPlugs::FujiTools::DocumentNormalizer::staticMetaObject;
    return                          lst;
}
RE::PluginMetaObjectList REPlugs::FujiTools::Helper::analyzers () const {
    return                          RE::PluginMetaObjectList();
}
RE::PluginMetaObjectList REPlugs::FujiTools::Helper::recognizers () const {
    return                          RE::PluginMetaObjectList();
}

Q_EXPORT_PLUGIN2(REPlugFujiTools, REPlugs::FujiTools::Helper)
