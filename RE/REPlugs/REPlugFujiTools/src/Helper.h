#ifndef REPlugs_FujiTools_Helper_h
#define REPlugs_FujiTools_Helper_h

#include <RELibrary/RETypes>
#include <RELibrary/Plugins/AbstractPlugin>

namespace REPlugs {

namespace FujiTools {

class Helper : public RE::AbstractPlugin {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractPlugin)

public:
    Q_INVOKABLE                 Helper                          (QObject *p=0);
	// Plugin information strings.
    RE::PluginBaseInfos			baseInfos                       () const;
	// Plugin functionality.
    RE::PluginMetaObjectList	filters                         () const;
    RE::PluginMetaObjectList	analyzers                       () const;
    RE::PluginMetaObjectList	recognizers                     () const;
};

}

}

#endif
