# Base settings.
include         (../../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   REPlugFujiTools
TEMPLATE        =   lib
CONFIG          +=  plugin
QT              +=  gui

include         (../../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                        install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/libRELibrary.dylib" \
                                "$${DESTDIR}/lib$${TARGET}.dylib"
}

target.path         =   $${INSTALLDIR}/plugins/RELibrary
INSTALLS            +=  target

VERSION             =   $${REPlugFujiToolsVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

HEADERS         +=  \
                    src/Helper.h \
    src/Filters/DocumentNormalizer.h
SOURCES         +=  \
                    src/Helper.cpp \
    src/Filters/DocumentNormalizer.cpp
