#include "Scheduler.h"
#include <QtCore/QDir>
#include <QtCore/QTimer>
#include <QtCore/QSettings>
#include <QtCore/QCoreApplication>
#include <RELibrary/RELibrary>

int main (int argc, char *argv[]) {
    QCoreApplication a          (argc, argv);
    if (a.argc()<5) {
        qWarning()              << "Usage: RELibraryApiTester SE TC PCC IPT [RECB] [IMG]";
        qWarning()              << "  SE  - SharedEngine: Use 1 to share one engine across all threads or 0 to share the engine across all threads.";
        qWarning()              << "  TC  - ThreadCount: Number of threads to spawn or 0 to use the number of cores.";
        qWarning()              << " PCC  - Parallelization Cores Count: Number of cores to use when filtering images, or 0 to use an ideal value.";
        qWarning()              << " IPT  - Iterations per Thread: Number of document recognition iterations to perform per thread.";
        qWarning()              << " RECB - A Client Recognition Binary file, or \"Embeded\" to use the embeded one.";
        qWarning()              << " IMG  - A document image to use, or \"Embeded\" to use the embeded one.";
        qWarning()              << "  eg: - ./RELibraryApiTester 0 100 0 1 Embeded Embeded";
        return                  1;
    }

    // Parse engine sharing argument.
    bool            shareEngine = a.arguments()[1].toUInt()!=0;
    // Parse threadcount argument.
    quint32         threadCount = a.arguments()[2].toUInt();
    if (!threadCount)
        threadCount             = QThread::idealThreadCount();
    // Parse parallelization argument.
    quint32         ppcCount    = a.arguments()[3].toUInt();
    if (!ppcCount)
        ppcCount                = qMax(QThread::idealThreadCount()-threadCount, (quint32)1);
    // Parse iterations per thread.
    quint32 const   iterations  = a.arguments()[4].toUInt();
    // Parse client binary name.
    QString         recb        = a.argc()>=6 ? a.arguments()[5] : "Embeded";
    if (recb=="Embeded") {
        qWarning()              << "Using embeded client binary.";
        recb                    = ":/Resources/ClientBinary.recb";
    }
    // Parse image.
    QString         image       = a.argc()>=7 ? a.arguments()[6] : "Embeded";
    if (image=="Embeded") {
        qWarning()              << "Using embeded image.";
        image                   = ":/Resources/Document.jpg";
    }
    REAT::Scheduler *sc         = new REAT::Scheduler(shareEngine, threadCount, ppcCount, iterations, recb, image, &a);
    QObject::connect(sc, SIGNAL(finished()), &a, SLOT(quit()));
    sc->start                   ();
    return                      a.exec();
}
