#ifndef REAT_Task_h
#define REAT_Task_h

#include <QtCore/QThread>
#include <QtCore/QDataStream>

namespace REAT {

// Task parameters and results container.
struct TaskIO {
    // Those are parameters for the task.
    quint32         id;
    void            *recognitionEngine;
    // Those will be set by the task itself.
    QList<double>   loopTimings,
                    loopSpeedupTimings,
                    guessingTimings,
                    recognitionTimings;
    // Those will store computed data after the task ends.
    double          totalTime,
                    avgLoopTime,            medLoopTime,
                    avgLoopSpeedupTime,     medLoopSpeedupTime,
                    avgGuessingTime,        medGuessingTime,
                    avgRecognitionTime,     medRecognitionTime,
                    documentsPerSecond;
    // Constructor with cleanup.
    /**/            TaskIO              () {
        loopTimings.clear       ();
        loopSpeedupTimings.clear();
        guessingTimings.clear   ();
        recognitionTimings.clear();
        totalTime               = 0.0;
        avgLoopTime             = 0.0;
        medLoopTime             = 0.0;
        avgLoopSpeedupTime      = 0.0;
        medLoopSpeedupTime      = 0.0;
        avgGuessingTime         = 0.0;
        medGuessingTime         = 0.0;
        avgRecognitionTime      = 0.0;
        medRecognitionTime      = 0.0;
        documentsPerSecond      = 0.0;
    }
};

class Scheduler : public QThread {
Q_OBJECT

public:
    /**/            Scheduler           (bool sharedEngine, quint32 threadCount, quint32 parallelizedCoresCount, quint32 iterations, QString const &recb, QString const &image, QObject *p=0);
    void            run                 ();

protected:
    REAT::TaskIO    task                (TaskIO io) const;

private:
    bool            _sharedEngine;
    quint32         _threadCount;
    quint32         _parallelizedCoresCount;
    quint32         _iterations;
    QString         _recb;
    QString         _image;
};

}

#endif
