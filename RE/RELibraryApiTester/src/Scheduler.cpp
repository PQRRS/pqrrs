#include "Scheduler.h"

#include <RELibrary/RETypes>
#include <RELibrary/RELibrary>

#include <cstdio>
#include <QList>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QCoreApplication>
#include <QtCore/QFutureSynchronizer>

REAT::Scheduler::Scheduler (bool sharedEngine, quint32 threadCount, quint32 parallelizedCoresCount, quint32 iterations, QString const &recb, QString const &image, QObject *parent)
: QThread(parent), _sharedEngine(sharedEngine), _threadCount(threadCount), _parallelizedCoresCount(parallelizedCoresCount), _iterations(iterations),
_recb(recb), _image(image) {
}

void* initializedEngine (QString const &cbName) {
    void    *re         = RECreateEngine(QString("%1/.PQRRSLicense.lic").arg(QDir::homePath()).toUtf8().constData());
    if (!re) {
        qCritical()     << "Failed to instanciate recognition engine.";
        return          0;
    }
    if (!RELoadEngineClientBinary(re, cbName.toUtf8().constData())) {
        qCritical()     << "Failed to load client binary file into recognognition engine.";
        REDeleteEngine  (re);
        return          0;
    }
    return              re;
}

template <typename T>
void averageAndMedian(QList<T> list, T &avg, T &med) {
    avg                         = 0;
    foreach (T const &d, list)
        avg                     += d;
    avg                         = avg / list.count();
    qSort                       (list);
    med                         = list.at((list.count()-1)/2);
}

void REAT::Scheduler::run () {
    qWarning()                      << "Static data is being initialized...";
    // Run all required initialization.
    REStaticInitializer             ();
    // Licensing setup.
    qWarning()                      << "Engine Mode:" << (_sharedEngine ? "Shared Across Threads" : "One per Thread")
                                    << "- Threads:" << _threadCount << "- Filtering CPU Count:" << _parallelizedCoresCount
                                    << "- Iterations:" << _iterations;


    // Instanciate engine if shared mode is enabled.
    void                *re         = 0;
    if (_sharedEngine && !(re=initializedEngine(_recb)))
        return;

    qWarning()                      << "Spawning threads...";
    QFutureSynchronizer<REAT::TaskIO>
                        tasks;
    for (quint32 i=0; i<_threadCount; ++i) {
        TaskIO          io;
        // Only create an engine if shared mode is disabled.
        io.recognitionEngine        = _sharedEngine ? re : initializedEngine(_recb);
        if (!io.recognitionEngine)
            continue;
        io.id                       = i;
        tasks.addFuture             (QtConcurrent::run<REAT::TaskIO>(this, &REAT::Scheduler::task, io));
    }
    // Wait for completion.
    tasks.waitForFinished           ();
    printf                          ("\n");

    qWarning()                      << "Deleting recognition engine(s)...";
    if (_sharedEngine)
        REDeleteEngine              (re);
    else
        for (quint16 i=0; i<_threadCount; ++i)
            REDeleteEngine          (tasks.futures().at(i).result().recognitionEngine);

    // Overall data temps.
    double              avgLoopTime                     = 0.0,
                        avgLoopSpeedupTime              = 0.0,
                        avgGuessingTime                 = 0.0,
                        avgRecognitionTime              = 0.0,
                        documentsPerSecond              = 0.0;
    // Loop for each thread result, compute overall, and generate report.
    for (quint16 i=0; i<_threadCount; ++i) {
        TaskIO          io          = tasks.futures().at(i).result();
        // Average loop time and individual speedups.
        double          prev        = io.loopTimings.at(0);
        foreach (double const &d, io.loopTimings)
            io.loopSpeedupTimings   << prev / 100.0 * (prev-d);
        // Compute averages and medians.
        averageAndMedian            (io.loopTimings,        io.avgLoopTime,         io.medLoopTime);
        averageAndMedian            (io.loopSpeedupTimings, io.avgLoopSpeedupTime,  io.medLoopSpeedupTime);
        averageAndMedian            (io.guessingTimings,    io.avgGuessingTime,     io.medGuessingTime);
        averageAndMedian            (io.recognitionTimings, io.avgRecognitionTime,  io.medRecognitionTime);
        // Compute Document-per-second time.
        io.documentsPerSecond       = 1.0 / io.avgLoopTime;
        // Compute overall stuff.
        avgLoopTime                 += io.avgLoopTime;
        avgLoopSpeedupTime          += io.avgLoopSpeedupTime;
        avgGuessingTime             += io.avgGuessingTime;
        avgRecognitionTime          += io.avgRecognitionTime;
        documentsPerSecond          += io.documentsPerSecond;
        // Finally write report.
        qWarning()                  << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
        qWarning()                  << "Thread" << io.id+1 << "Report:";
        qWarning()                  << "  Loop:";
        qWarning()                  << "                  Average:" << io.avgLoopTime;
        qWarning()                  << "                   Median:" << io.medLoopTime;
        qWarning()                  << "       Average Speed-Up %:" << io.avgLoopSpeedupTime;
        qWarning()                  << "        Median Speed-Up %:" << io.medLoopSpeedupTime;
        qWarning()                  << "              Average DpS:" << io.documentsPerSecond;
        qWarning()                  << "  Guessing:";
        qWarning()                  << "                  Average:" << io.avgGuessingTime;
        qWarning()                  << "                   Median:" << io.medGuessingTime;
        qWarning()                  << "  Recognition:";
        qWarning()                  << "                  Average:" << io.avgRecognitionTime;
        qWarning()                  << "                   Median:" << io.medRecognitionTime;
    }
    avgLoopTime                     = avgLoopTime / _threadCount;
    avgGuessingTime                 = avgGuessingTime / _threadCount;
    avgRecognitionTime              = avgRecognitionTime / _threadCount;
    documentsPerSecond              = documentsPerSecond / _threadCount;
    qWarning()                      << "= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =";
    qWarning()                      << "Global Report:";
    qWarning()                      << "             Loop Average:" << avgLoopTime;
    qWarning()                      << "          Loop Speed-Up %:" << avgLoopSpeedupTime;
    qWarning()                      << "         Guessing Average:" << avgGuessingTime;
    qWarning()                      << "      Recognition Average:" << avgRecognitionTime;
    qWarning()                      << "   Average Docs. / Second:" << documentsPerSecond;
}

REAT::TaskIO REAT::Scheduler::task (REAT::TaskIO io) const {
    QByteArray      imgData;
    { // Memguard.
        QFile       f       (_image);
        f.open              (QIODevice::ReadOnly);
        imgData             = f.readAll();
        f.close             ();
    }

    QTime               totalTimer,
                        loopTimer,
                        individualTimer;
    totalTimer.start            ();
    loopTimer.start             ();
    individualTimer.start       ();

    for (quint32 i=1; i<=_iterations; ++i) {
        printf                  (i%10==1 ? "|" : ".");
        // Create a recognition document.
        void            *rd     = RECreateDocument();
        // Load a picture, and attach it to the document.
        if (!RESetDocumentImageForKey(rd, "Front", imgData.constData(), imgData.length())) {
            qCritical()         << "Thread:" << io.id+1 << "- Failed to set Recognition Document Front image.";
            REDeleteDocument    (rd);
            continue;
        }
        // Try to guess the document's type.
        individualTimer.restart ();
        if (!REGuessDocumentType(io.recognitionEngine, rd, _parallelizedCoresCount)) {
            qCritical()         << "Thread:" << io.id+1 << "- Failed to guess document type.";
            REDeleteDocument    (rd);
            continue;
        }
        io.guessingTimings      << individualTimer.restart() / 1000.0;
        // Run recognition.
        if (!RERecognizeDocumentData(io.recognitionEngine, rd, _parallelizedCoresCount, io.id)) {
            qCritical()         << "Thread:" << io.id+1 << "- Failed to recognize document data.";
            REDeleteDocument    (rd);
            continue;
        }

        //char    *json           = 0;
        //REGetDocumentJsonResults    (rd, &json);
        //Wrn << json;
        //REReleaseString             (json);

        SAStruct                catKeys = REGetDocumentCategoryKeys(rd);
        for (int cId=0; cId<catKeys.count; ++cId) {
            char                *catKey = catKeys.elements[cId];
            qWarning()                  << "Inspecting category:" << catKey << "...";
            SAStruct            szKeys  = REGetDocumentStringKeys(rd, catKey);
            for (int sId=0; sId<szKeys.count; ++sId) {
                char                *szKey  = szKeys.elements[sId];
                char                *szVal;
                REGetDocumentStringForKey   (rd, catKey, szKey, &szVal);
                qWarning()                  << "    " << szKey << ":" << szVal;
                REReleaseString             (szVal);
            }
            REReleaseStringArray    (szKeys);
        }
        REReleaseStringArray    (catKeys);

        io.recognitionTimings   << individualTimer.restart() / 1000.0;
        // Finally destroy the recognition document.
        REDeleteDocument        (rd);
        // Store loop timing.
        io.loopTimings          << loopTimer.restart() / 1000.0;
    } // End main loop.
    io.totalTime                = totalTimer.elapsed() / 1000.0;
    return                      io;
}
