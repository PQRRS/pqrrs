# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   RELibraryApiTester
TEMPLATE        =   app
CONFIG          +=  console

include         (../../LMLibrary.pri)
include         (../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                        install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/../../../libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}"
}

target.path     =   $${INSTALLDIR}/bin
INSTALLS        +=  target

VERSION             =   $${RELibraryApiTesterVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

RESOURCES       +=  src/RELibraryApiTester.qrc

SOURCES         +=  src/main.cpp \
                    src/Scheduler.cpp

HEADERS         +=  src/Scheduler.h
