#include "TUTypes.h"
#include "UI/WndMain.h"
#include <QtGui/QApplication>
#include <QtGui/QStyleFactory>

int main(int argc, char *argv[]) {
    QApplication	a			(argc, argv);
    TU::WndMain		w;
    a.setStyle					(QStyleFactory::create("Plastique"));
    w.show						();
    return						a.exec();
}
