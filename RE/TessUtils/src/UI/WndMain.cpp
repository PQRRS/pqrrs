#include "WndMain.h"
// Required stuff.
#include "../TUTypes.h"
#include "../Core.h"
#include "DlgPictureGenerator.h"
// Models.
#include "../Models/FontsModel.h"
#include "../Models/PicturesModel.h"
#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtCore/QSettings>
#include <QtGui/QStandardItemModel>
#include <QtGui/QStandardItem>
#include <QtGui/QFileDialog>

TU::WndMain*		TU::WndMain::_instance		= 0;

TU::WndMain::WndMain (QWidget *p, Qt::WindowFlags f)
: QMainWindow(p, f), _core(&TU::Core::Instance()), _pictureGeneratorDlg(0) {
    // Setup main UI.
    ui.setupUi									(this);

    // Create output model.
    _consoleModel								= new QStandardItemModel(0, 3, this);
    // Update headers.
    QStringList				labels;
    labels										<< tr("#")
                                                << tr("Sender")
                                                << tr("Method")
                                                << tr("Message");
    _consoleModel->setHorizontalHeaderLabels	(labels);
    // Assign model to table.
    ui.tblVwOutput->setModel					(_consoleModel);
    _instance									= this;

    // For debugging only:
    #ifndef QT_NO_DEBUG
    #define TypeSimpleRegistration(name)	qRegisterMetaType<name>(#name);
    TypeSimpleRegistration	(QtMsgType);
    #undef TypeFullRegistration
    qInstallMsgHandler							(TU::WndMain::staticMessageHandler);
    #endif

    Dbg << "Connecting actions...";
    ui.btnAddFont->setDefaultAction				(ui.actAddFont);
    ui.btnRemoveFont->setDefaultAction			(ui.actRemoveFont);
    ui.btnAddPicture->setDefaultAction			(ui.actAddPicture);
    ui.btnRemovePicture->setDefaultAction		(ui.actRemovePicture);
    ui.btnGeneratePicture->setDefaultAction		(ui.actGeneratePicture);
    ui.btnDetectPictureBoxes->setDefaultAction	(ui.actDetectPictureBoxes);
    ui.btnAddBox->setDefaultAction				(ui.actAddBox);
    ui.btnRemoveBox->setDefaultAction			(ui.actRemoveBox);
    ui.btnMoveBoxUp->setDefaultAction			(ui.actMoveBoxUp);
    ui.btnMoveBoxDown->setDefaultAction			(ui.actMoveBoxDown);
    ui.btnSelectPreviousBox->setDefaultAction	(ui.actSelectPreviousBox);
    ui.btnSelectNextBox->setDefaultAction		(ui.actSelectNextBox);

    Dbg << "Restoring geometry...";
    QSettings settings							("Pierre qui Roule EURL", "TessUtils");
    restoreGeometry								(settings.value("WndMain::Geometry").toByteArray());
    restoreState								(settings.value("WndMain::State").toByteArray());

    Dbg << "Preparing User Interface...";
    // Splitters ratios.
    ui.spltrBoxer->setStretchFactor				(0, 1);
    ui.spltrBoxer->setStretchFactor				(1, 3);
    // UI.
    updateUi									();
    updateRecentMenu							();

    Dbg << "Connecting to core...";
    connect(_core, SIGNAL(projectOpened()), this, SLOT(core_projectOpened()));
    connect(_core, SIGNAL(projectClosed()), this, SLOT(core_projectClosed()));
}
TU::WndMain::~WndMain () {
    if (_pictureGeneratorDlg)	_pictureGeneratorDlg->deleteLater();
    _instance					= 0;
}

void TU::WndMain::staticMessageHandler (QtMsgType t, const char *m) {
    // No MainWindow yet instanciated.
    if (!_instance)
        return;
    else if (_instance->_consoleModel) {
        // Prepare color and icon.
        QColor					clr;
        QIcon					icn;
        switch (t) {
            case QtDebugMsg:	clr.setRgb(0x88, 0xff, 0x88);	icn = QIcon(":/Resources/Icons/checkmark.gif");		break;
            case QtWarningMsg:	clr.setRgb(0xff, 0x88, 0x88);	icn = QIcon(":/Resources/Icons/caution.gif");		break;
            case QtCriticalMsg:	clr.setRgb(0xff, 0xcc, 0x88);	icn = QIcon(":/Resources/Icons/exclamation.gif");	break;
            case QtFatalMsg:	clr.setRgb(0xff, 0x55, 0x55);	icn = QIcon(":/Resources/Icons/stop.gif");			break;
        }

        // Reformat message.
        QString					message		(m);
        // Split message to extract method / message.
        QString					sender, method;
        if (message.contains(" # ")) {
            QStringList			msgs		= message.split(" # ");
            QStringList			origin		= msgs.takeAt(0).split("::");
            method							= origin.takeLast();
            sender							= origin.join("::");
            message							= msgs.join(" # ");
        } else
            sender = method					= "Unknown";

        // Cache model.
        QStandardItemModel		*mdl		= _instance->_consoleModel;
        // Lock mutex...
        _instance->_consoleMutex.lockInline ();
        // Create items row.
        quint32					rowCount	= mdl->rowCount();
        QList<QStandardItem*>	row;
        row									<< new QStandardItem(icn,"") << new QStandardItem(sender) << new QStandardItem(method) << new QStandardItem(message);
        mdl->appendRow						(row);
        mdl->setData						(mdl->index(rowCount, 1), clr, Qt::ForegroundRole);
        mdl->setData						(mdl->index(rowCount, 2), clr, Qt::ForegroundRole);
        mdl->setData						(mdl->index(rowCount, 3), clr, Qt::ForegroundRole);
        QMetaObject::invokeMethod			(_instance->ui.tblVwOutput, "scrollToBottom", Qt::QueuedConnection);
        _instance->_consoleMutex.unlockInline();
    }
    if (t==QtFatalMsg)
        abort();
}
void TU::WndMain::updateUi () {
    bool	projectOpened					= _core->projectFilename().length();
    ui.wgtMain->setEnabled					(projectOpened);
    ui.dkWgtFontsExplorer->setEnabled		(projectOpened);
    ui.dkWgtBoxPreview->setEnabled			(projectOpened);
    ui.dkWgtOutput->setEnabled				(projectOpened);
}
void TU::WndMain::updateRecentMenu () {
    // Restore recent files menu.
    QSettings		settings			("Pierre qui Roule EURL", "TessUtils");
    QStringList filenames				= settings.value("WndMain::mnuProjectRecent").toStringList();
    ui.mnuProjectRecent->clear();
    QStringList		cleanedFilenames;
    foreach (QString const &filename, filenames) {
        if (!QFileInfo(filename).exists())
            continue;
        cleanedFilenames << filename;
        QAction	*act					= new QAction(QFileInfo(filename).baseName(), ui.mnuProjectRecent);
        act->setData(filename);
        connect(act, SIGNAL(triggered()), this, SLOT(on_actOpenProject_triggered()));
        ui.mnuProjectRecent->addAction(act);
    }
    settings.setValue("WndMain::mnuProjectRecent", cleanedFilenames);
}
void TU::WndMain::closeEvent (QCloseEvent *e) {
    QSettings	s				("Pierre qui Roule EURL", "TessUtils");
    s.setValue					("WndMain::Geometry", saveGeometry());
    s.setValue					("WndMain::State", saveState());
    QMainWindow::closeEvent		(e);
}

// Core signals.
void TU::WndMain::core_projectOpened () {
    QSettings settings			("Pierre qui Roule EURL", "TessUtils");
    QStringList filenames		= settings.value("WndMain::mnuProjectRecent").toStringList();
    filenames.removeAll			(_core->projectFilename());
    filenames.push_front		(_core->projectFilename());
    while (filenames.count()>5)
        filenames.removeLast	();
    settings.setValue			("WndMain::mnuProjectRecent", filenames);
    updateRecentMenu			();
    updateUi					();
}
void TU::WndMain::core_projectClosed () {
    updateUi					();
}

// Project management.
void TU::WndMain::on_actNewProject_triggered () {
    QString	path	= QFileDialog::getExistingDirectory(this, tr("Choose a directory to save the project in."));
    if (!path.length())
        return;
    _core->createProject(path);
}
void TU::WndMain::on_actOpenProject_triggered () {
    QAction				*sndr				= qobject_cast<QAction*>(sender());
    QString				pFn					= sndr->data().isValid() ? sndr->data().toString() : QFileDialog::getOpenFileName(this, tr("Open Project..."), QDir::homePath(), "TessUtils Projects (*.tuproj)");
    if		(!pFn.length())					return;
    _core->openProject(pFn);
}
void TU::WndMain::on_actProjectClose_triggered () {
    _core->closeProject						();
}

// Fonts management.
void TU::WndMain::on_actAddFont_triggered () {
    TU::FontsModel		*fiModel			= _core->realFontsModel();
    fiModel->insertRow						(fiModel->rowCount());
}
void TU::WndMain::on_actRemoveFont_triggered () {
}
// Pictures management.
void TU::WndMain::on_actAddPicture_triggered () {
}
void TU::WndMain::on_actRemovePicture_triggered () {
}
void TU::WndMain::on_actGeneratePicture_triggered () {
    if (!_pictureGeneratorDlg) {
        Dbg << "Instanciating new Picture Generator Dialog.";
        _pictureGeneratorDlg		= new DlgPictureGenerator(this);
    } else {
        _pictureGeneratorDlg->reset	();
        Dbg << "Recycling existing Picture Generator Dialog.";
    }
    if (_pictureGeneratorDlg->exec()!=QDialog::Accepted)
        return;

    QFont const&	fnt				= _pictureGeneratorDlg->paragraphFont();
    QString const&	txt				= _pictureGeneratorDlg->text();
    bool			useTess			= _pictureGeneratorDlg->useTesseract();
    Wrn	<< "Selected font"				<< fnt.family()
        << "(Size:"						<< fnt.pointSize()
        << "Bold:"						<< fnt.bold()
        << "Italic"						<< fnt.italic() << ")"
        << ", Text length is"			<< txt.count()
        << "and use of Tesseract is"	<< useTess
        << ".";

}

// Boxes management.
void TU::WndMain::on_actDetectPictureBoxes_triggered () {
}
void TU::WndMain::on_actAddBox_triggered () {
}
void TU::WndMain::on_actRemoveBox_triggered () {
}
void TU::WndMain::on_actMoveBoxUp_triggered () {
}
void TU::WndMain::on_actMoveBoxDown_triggered () {
}
void TU::WndMain::on_actSelectPreviousBox_triggered () {
}
void TU::WndMain::on_actSelectNextBox_triggered () {
}
