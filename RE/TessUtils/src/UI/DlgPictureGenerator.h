#ifndef TU_DlgPictureGenerator_h
#define TU_DlgPictureGenerator_h

// Base class and UI.
#include "ui_DlgPictureGenerator.h"
#include <QDialog>
#include <QFont>

namespace TU {

class DlgPictureGenerator : public QDialog {
public:
    Q_OBJECT
    Q_PROPERTY(QFont	paragraphFont	READ font			WRITE setParagraphFont)
    Q_PROPERTY(QString	text			READ text			WRITE setText)
    Q_PROPERTY(bool		useTesseract	READ useTesseract	WRITE setUseTesseract)

public:
	// Constructors.
							DlgPictureGenerator					(QWidget *p=0, Qt::WindowFlags f=0);
	// Accessors.
	QFont const&			paragraphFont						() const;
	void					setParagraphFont					(QFont const &v);
	QString					text								() const;
	void					setText								(QString const &v);
	bool					useTesseract						() const;
	void					setUseTesseract						(bool v);
	// Reset all fields to their default.
	void					reset								();

protected slots:
	void					on_btnFontPicker_clicked			();

private:
	QFont					_paragraphFont;
	QString					_text;
	Ui::DlgPictureGenerator	_ui;
};

}

#endif
