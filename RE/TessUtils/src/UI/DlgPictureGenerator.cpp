#include "DlgPictureGenerator.h"
#include "../TUTypes.h"
#include <QtCore/QFile>
#include <QtGui/QFontDialog>
#include <QtCore/QDebug>

TU::DlgPictureGenerator::DlgPictureGenerator (QWidget *p, Qt::WindowFlags f)
: QDialog(p, f) {
	_ui.setupUi					(this);
	setModal					(true);
	reset						();
}
QFont const& TU::DlgPictureGenerator::paragraphFont () const {
	return _paragraphFont;
}
void TU::DlgPictureGenerator::setParagraphFont (QFont const &v) {
	_paragraphFont				= v;
	_paragraphFont.setUnderline	(false);
	_paragraphFont.setStrikeOut	(false);
	
	_ui.btnFontPicker->setText	(tr("%1 (Size %2, Bold: %3, Italic: %4)")
								.arg(_paragraphFont.family())
								.arg(_paragraphFont.pointSize())
								.arg(_paragraphFont.bold())
								.arg(_paragraphFont.italic()));
	
	QFont			edFont		= _paragraphFont;
	edFont.setPointSize			(qBound(8, _paragraphFont.pointSize(), 16));
	_ui.txtText->setFont		(edFont);
}

QString TU::DlgPictureGenerator::text () const {
	return _ui.txtText->toPlainText();
}
void TU::DlgPictureGenerator::setText (QString const &v) {
	_ui.txtText->setPlainText	(v);
}
bool TU::DlgPictureGenerator::useTesseract () const {
	return _ui.chkBoxWithTesseract->isChecked();
}
void TU::DlgPictureGenerator::setUseTesseract (bool v) {
	_ui.chkBoxWithTesseract->setChecked(v);
}

void TU::DlgPictureGenerator::reset () {
	// Load paragraph to display in the box.
	QFile			fBlock		(":/Resources/Text/Paragraph.txt");
	// Read the whole content.
	fBlock.open					(QIODevice::ReadOnly);
	quint64			size		= fBlock.size();
	char			*data		= new char[size];
	fBlock.read					(data, size);
	fBlock.close				();
	// Set the text and delete the buffer.
	setText						(QString::fromUtf8(data, size));
	delete[]					data;
	// Set paragraph font so it displays the choosen one.
	_paragraphFont				= QFont(tr("Arial"), 24, -1, false);
	setParagraphFont			(_paragraphFont);
	// Reset boxer choice.
	_ui.chkBoxWithTesseract->setChecked(false);
}

void TU::DlgPictureGenerator::on_btnFontPicker_clicked () {
	Dbg;
	bool			ok;
	QFont			fnt			= QFontDialog::getFont(&ok, _paragraphFont, this);
	if (ok)						setParagraphFont(fnt);
}
