#ifndef TU_WndMain_h
#define TU_WndMain_h

// Base class and private UI.
#include "ui_WndMain.h"
#include <QtGui/QMainWindow>
#include <QtCore/QMutex>

class QStandardItemModel;

namespace TU {
	class Core;
	class DlgPictureGenerator;
}

namespace TU {

class WndMain : public QMainWindow {
Q_OBJECT
public:
	// Constructors.
					WndMain								(QWidget *p=0, Qt::WindowFlags f=0);
					~WndMain							();
	
protected:
	// QDebug messaging.
	static void		staticMessageHandler				(QtMsgType type, const char *msg);
	void			updateUi							();
	void			updateRecentMenu					();
	void			closeEvent							(QCloseEvent *e);

protected slots:
	// Core signals.
	void			core_projectOpened					();
	void			core_projectClosed					();
	// Project management.
	void			on_actNewProject_triggered			();
	void			on_actOpenProject_triggered			();
	void			on_actProjectClose_triggered		();
	// Fonts management.
	void			on_actAddFont_triggered				();
	void			on_actRemoveFont_triggered			();
	// Pictures management.
	void			on_actAddPicture_triggered			();
	void			on_actRemovePicture_triggered		();
	void			on_actGeneratePicture_triggered		();
	// Boxes management.
	void			on_actDetectPictureBoxes_triggered	();
	void			on_actAddBox_triggered				();
	void			on_actRemoveBox_triggered			();
	void			on_actMoveBoxUp_triggered			();
	void			on_actMoveBoxDown_triggered			();
	void			on_actSelectPreviousBox_triggered	();
	void			on_actSelectNextBox_triggered		();

private:
	static WndMain		*_instance;
	Ui::WndMain			ui;
	TU::Core			*_core;
	DlgPictureGenerator	*_pictureGeneratorDlg;
	QStandardItemModel	*_consoleModel;
	QMutex				_consoleMutex;
};

}

#endif
