#include "PicturesModel.h"
// Required stuff.
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlRecord>

TU::PicturesModel::PicturesModel (QObject *p, QSqlDatabase const &db) : QSqlTableModel(p, db) {
	// Setup model table and edition strategy.
	setTable				("Pictures");
	setEditStrategy			(QSqlTableModel::OnFieldChange);
	// Setup headers.
	setHeaderData			(Key,			Qt::Horizontal, tr("Key"));
	setHeaderData			(Font,			Qt::Horizontal, tr("Font"));
}

Qt::ItemFlags TU::PicturesModel::flags (const QModelIndex &i) const {
	Qt::ItemFlags		flags	= QSqlTableModel::flags(i) | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	return flags;
}

void TU::PicturesModel::setFontId (quint64 const &fiId) {
	QString			filter		= QString("font_id=%1 OR font_id=NULL").arg(fiId);
	setFilter					(filter);
	select						();
}

QVariant TU::PicturesModel::data (const QModelIndex &i, int r) const {
	return QSqlTableModel::data(i, r);
}
bool TU::PicturesModel::setData (const QModelIndex &i, const QVariant &v, int r) {
	return QSqlTableModel::setData(i, v, r);
}
