#include "FontsModel.h"
// Required stuff.
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>

TU::FontsModel::FontsModel (QObject *p, QSqlDatabase const &db) : QSqlTableModel(p, db) {
	// Setup model table and edition strategy.
	setTable				("Fonts");
	setEditStrategy			(QSqlTableModel::OnFieldChange);
	// Setup headers.
	setHeaderData			(Key,		Qt::Horizontal, tr("Key"));
	setHeaderData			(Family,	Qt::Horizontal, tr("Family"));
}

Qt::ItemFlags TU::FontsModel::flags (const QModelIndex &i) const {
	Qt::ItemFlags			flags			= QSqlTableModel::flags(i) | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	return flags;
}
void TU::FontsModel::triggerUpdate (QModelIndex const &idx) {
	emit dataChanged(idx, idx);
}

int TU::FontsModel::columnCount (const QModelIndex &i) const {
	return QSqlTableModel::columnCount(i)+1;
}
QVariant TU::FontsModel::data (const QModelIndex &i, int r) const {
	if (!i.isValid())
		return QVariant();
	// Display tooltip...
	else
		return QSqlTableModel::data(i, r);
}
