#ifndef TU_FontsModel_h
#define TU_FontsModel_h

// Base class.
#include <QtSql/QSqlTableModel>
// Required stuff.
#include <QtCore/QVariant>

namespace TU {

class FontsModel : public QSqlTableModel {
Q_OBJECT
public:
	// Columns indices helpers.
        enum ColumnIndices {
		Key				= 0,
		Family			= 1
	};
		
						FontsModel					(QObject *p, QSqlDatabase const &db);
	Qt::ItemFlags		flags						(const QModelIndex &i) const;
	void				triggerUpdate				(QModelIndex const &idx);
	int					columnCount					(const QModelIndex &i) const;
	QVariant			data						(const QModelIndex &i, int r=Qt::DisplayRole) const;
};

};

#endif
