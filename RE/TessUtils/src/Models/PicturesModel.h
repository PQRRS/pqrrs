#ifndef TU_PicturesModel_h
#define TU_PicturesModel_h

// Base class.
#include <QtSql/QSqlTableModel>
// Required stuff.
#include "../TUTypes.h"
#include <QtCore/QVariant>

namespace TU {

class PicturesModel : public QSqlTableModel {
Q_OBJECT
public:
	// Columns indices helpers.
	enum ColumnIndices {
		Key					= 0,
		Font				= 1
	};
	
	// Default constructor.
						PicturesModel			(QObject *p, QSqlDatabase const &db);
	Qt::ItemFlags		flags					(const QModelIndex &i) const;
	void				setFontId				(quint64 const &fiId);

	virtual QVariant	data					(const QModelIndex &i, int r=Qt::UserRole) const;
	bool				setData					(const QModelIndex &i, const QVariant &v, int r=Qt::EditRole);
};

}

#endif
