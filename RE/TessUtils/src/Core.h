#ifndef Core_h
#define Core_h

// Base class.
#include <QtCore/QObject>
// Base types.
#include "TUTypes.h"
// Various required stuff.
#include <QtCore/QMutex>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlDatabase>

// Forward declarations.
class QProxyModel;
namespace TU {
	class FontsModel;
	class PicturesModel;
}

namespace TU {

class Core : public QObject {
Q_OBJECT
protected:
	// Constructor / Destructor, private to avoid external instanciation.
										Core							();
	virtual								~Core							();

public:
	// Singleton getters.
	static Core&						Instance						();
	static void							Destroy							();

	// Project management.
	void								createProject					(QString const &path);
	void								openProject						(QString const &path);
	void								closeProject					();
	QString const&						projectFilename					() const;

	// Various getters on database and models.
	QSqlDatabase&						database						();
	QAbstractItemModel&					fontsModel						();
	FontsModel*							realFontsModel					();
	QAbstractItemModel&					picturesModel					();
	PicturesModel*						realPicturesModel				();

private:
	// Static instance.
	static Core							*_Instance;
	QString								_projectFilename;
	// Database.
	QSqlDatabase						_database;
	// Mutexes.
	mutable QMutex						_databaseMutex;
	// Models and their proxy counterpart.
	FontsModel							*_fontsModel;
	PicturesModel						*_picturesModel;
	QProxyModel							*_fontsModelProxy,
										*_picturesModelProxy;

signals:
	// Project slots.
	void								projectOpened					();
	void								projectClosed					();
};

}

#endif
