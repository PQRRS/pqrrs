#ifndef TU_TUTypes_h
#define TU_TUTypes_h

#include <QtCore/QObject>

#ifndef Dbg
#   define Dbg      qDebug()    << Q_FUNC_INFO << "#"
#   define Wrn      qWarning()  << Q_FUNC_INFO << "#"
#   define Crt      qCritical() << Q_FUNC_INFO << "#"
#endif

#ifdef Q_MOC_RUN
class TU {
Q_GADGET
Q_ENUMS(ViewMode)
public:
#else
namespace TU {
#endif
	// ------------------------------------	Constants / Basic types		------------------------------------ //
	extern const QMetaObject			staticMetaObject;

	// -------------------------------------		Other stuff			------------------------------------- //
	// Complete types registration for streaming and register metatypes.
	void								StaticInitializer		();
}

#endif
