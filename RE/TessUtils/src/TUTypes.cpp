#include "TUTypes.h"
#include <QtCore/QTextCodec>
#include <QtCore/QSettings>

void TU::StaticInitializer () {
	static bool	initDone	= false;
	if (initDone)			return;
	initDone				= true;
	
	// Make sure translation uses UTF8.
	QTextCodec::setCodecForTr	(QTextCodec::codecForName("UTF-8"));
}
