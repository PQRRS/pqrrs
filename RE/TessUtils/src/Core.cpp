// Self.
#include "Core.h"
#include "TUTypes.h"
// Models.
#include "Models/FontsModel.h"
#include "Models/PicturesModel.h"
// Other stuff.
#include <QtCore/QDebug>
#include <QtGui/QProxyModel>

TU::Core*	TU::Core::_Instance	= 0;

// Constructor / Destructor.
TU::Core::Core ()
: QObject(),_database(QSqlDatabase::addDatabase("QSQLITE")) {
    TU::StaticInitializer		();

    _fontsModelProxy					= new QProxyModel(this);
    _picturesModelProxy					= new QProxyModel(this);
}
TU::Core::~Core () {
    Dbg;
    if (_projectFilename.count())
        closeProject						();
    delete	_fontsModelProxy;
    delete  _picturesModelProxy;
    delete  _fontsModel;
    delete  _picturesModel;
}

TU::Core& TU::Core::Instance () {
    return _Instance ? *_Instance: *(_Instance = new Core());
}
void TU::Core::Destroy () {
    if (_Instance) {
        delete _Instance;
        _Instance	= 0;
    }
}

QSqlDatabase& TU::Core::database () {
    return _database;
}
QAbstractItemModel& TU::Core::fontsModel () {
    return *_fontsModelProxy;
}
TU::FontsModel* TU::Core::realFontsModel () {
    return _fontsModel;
}
QAbstractItemModel& TU::Core::picturesModel () {
    return *_picturesModelProxy;
}
TU::PicturesModel* TU::Core::realPicturesModel () {
    return _picturesModel;
}

// Project management.
void TU::Core::createProject (QString const &path) {
    Q_UNUSED(path);
}
void TU::Core::openProject (QString const &path) {
    Dbg;
    if (_projectFilename.count())			closeProject();
    // Prepare DB.
    _projectFilename						= path;
    _database.setDatabaseName				(_projectFilename);
    _database.open							();

    // Models.
    _fontsModel								= new FontsModel(this, _database);
    _fontsModelProxy->setModel				(_fontsModel);
    _picturesModel							= new PicturesModel(this, _database);
    _picturesModelProxy->setModel			(_picturesModel);

    emit projectOpened					();
}
void TU::Core::closeProject () {
    Dbg;
    // Clear project filename.
    _projectFilename					= "";
    // Signal listeners that the project is no longer valid.
    emit projectClosed					();
    // Finally close DB.
    _database.close						();
}
QString const& TU::Core::projectFilename () const {
    return _projectFilename;
}
