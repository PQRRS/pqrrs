# Base settings.
include         (../../Common.pri)
DESTDIR         =   $$MAINBINDIR
TARGET          =   TessUtils
TEMPLATE        =   app
QT              +=  gui sql

target.path     =   $${INSTALLDIR}/bin
INSTALLS        +=  target

VERSION             =   $${TessUtilsVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

RESOURCES       +=  TessUtils.qrc
FORMS           +=  Forms/DlgPictureGenerator.ui \
                    Forms/WndMain.ui
HEADERS         +=  src/Core.h \
                    src/TUTypes.h \
                    src/Models/FontsModel.h \
                    src/Models/PicturesModel.h \
                    src/UI/DlgPictureGenerator.h \
                    src/UI/WndMain.h

SOURCES         +=  src/main.cpp \
                    src/TUTypes.cpp \
                    src/Core.cpp \
                    src/Models/FontsModel.cpp \
                    src/Models/PicturesModel.cpp \
                    src/UI/DlgPictureGenerator.cpp \
                    src/UI/WndMain.cpp
