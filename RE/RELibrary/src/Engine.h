// Lib exports and types.
#include "RETypes.h"

#ifndef RE_Engine_h
#define RE_Engine_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include "ObjectsFactory.hpp"

// Forward declarations.
namespace LM {
    class Core;
}

namespace RE {
    class PluginsManager;
    class AbstractAnalyzer;
    class AbstractRecognizer;
    class AbstractFilter;
}
struct fann;
class QImage;
class QStringList;
class QScriptValue;
struct QMetaObject;
class QScriptEngine;

namespace RE {

class RELibOpt Engine : public QObject {
Q_OBJECT

public:
    /*! @brief Engine Constructor.
     * Constructs a new RE::Engine object, and proceeds with initialization in the following order:
     * - Factories configuration (Filters Factory then Recognizers Factory),
     * - Licensing data.
     * - Plugins discovery.
     * @param lm is the Licensing Manager to use for the constructed engine.
     * @param p is the parent object for this engine.
     */
                                        Engine    						(LM::Core *lm, QObject *p=0);
    /*! @brief Engine Destructor.
     * In the following order, the following members are destroyed:
     * - If a Client Binary is loaded, the destructor destroys:
     *   - The ANN object (Held in RE::Engine::_clientBinaryAnn),
     *   - Script Objects and Script Engines (Held in RE::Engine::_scriptObject and RE::Engine::_scriptEngine),
     *   - Any cached recognizer (Held in RE::Engine::_instanciatedRecognizers),
     *   - Any cached filter (Held in RE::Engine::_instanciatedFilters),
     *   - Cheesecake and Obfuscation data.
     */
    virtual								~Engine							();
    /*! @brief Licensing Manager Getter.
     * @return The instance of the licensing manager in use for this RE::Engine instance.
     * @see LM::Core()
     */
    LM::Core                            *licensingManagerCore           () { return _lm; }

    /*! @brief Licensed Flag Getter.
     * This method always returns true if the engine was compiled with the SECURITYLESS_BUILD.
     * @return A boolean telling wether the engine is running under a license or not.
     */
    bool                                licensed                        () const;

    /*! @brief Loaded Plugins List Getter.
     * The returned list lists the loaded plugins informative objects.
     * @return A PluginBaseInfos::List list of currently loaded plugins.
     */
    PluginBaseInfos::List				loadedPlugins					() const;
    /*! @brief Location of Available Binary Objects Getter.
     * The returned hash gives information about where a binary object (Filter, Analyzer, Recognizer) is located (Binary-wise speaking).
     * As most of the engine objects will be located in the engine binary itself, this method makes possible querying of object location
     * for plug-ins as-well.
     * @return A list of objects locations.
     */
    QHash<QString,QString> const&		objectsLocations				() const;

    /*! @brief Enumeration Ranges Getter.
     * As the engine needs to know what enumeration owns an enumerator, this getter allows the retrieval of QMetaObject*-based hash
     * access on meta-object type range information for each of them.
     * @see RE::E, RE::Engine::containingMetaObjectForEnumType()
     * @return A hash containing ranges of enumerable types.
     */
    MetaTypesRangeListHash const&		enumeratorRanges				() const;
    /*! @brief Enum Meta-Object Getter.
     * This method returns the owing QMetaObject enumeration for any given enumerator meta-type.
     * @see RE::E, RE::Engine::enumeratorRanges()
     * @param type The enumerator meta-type-id for which to retrieve its owner.
     * @return The QMetaObject owning this enumeration.
     */
    QMetaObject const*					containingMetaObjectForEnumType	(int type) const;

    /*! @brief Object Parameters Builder.
     * When the engine instanciates on-the-fly objects (Filter, Analyzer, Recognizer), it needs to have a list of its parameters
     * as well. This method builds a QVariantMap containing each of them, as well as their default values, with the right type information.
     * This method allows the engine to pre-fetch a compatible parameter list prior to configuring an object, avoiding setting / getting object
     * with properties that don't exist or have an incompatible runtime type.
     * @param object is the object to inspect.
     * @return A QVariantMap with every available parameter, and it's default value.
     */
    QVariantMap							defaultObjectParameters			(QObject *object) const;

    /*! @brief Filter Factory Getter.
     * Returns a read-only list that holds in-memory available filters for faster instanciation, based on their name and binary location.
     * @see ObjectsFactory
     * @return The instance currently being used.
     */
    ObjectsFactory const&				filtersFactory					() const;
    /*! @brief Zone Filter Data Creator.
     * This method creates a new zone filter data object to be filled up later, based on a class name.
     * @param className is the filter's class name for which to generate the mapping.
     * @return A RE::T::ZoneFilterData preconfigured to hold the filter parameters.
     */
    RE::T::ZoneFilterData				makeZoneFilterData				(QString const &className) const;
    /*! @brief Filter Instanciator.
     * This method returns an instance of a ready-to-be-used filter based on a zone filter data descriptor object.
     * The filter is filled with the parameters present in the RE::T::ZoneFilterData::parameters member.
     * Optionnaly, a cache hash can be specified, allowing caching of the resulting object for later re-use.
     * If the caching hash is already present, then the object that was previously created is returned. This is why the resulting object
     * has to not be directly deleted by the developper himself.
     * @see RE::T::ZoneFilterData
     * @param zfd is the zone filter data descriptor object from which to build the filter.
     * @param cacheHash is a reuse-identified that will be used for caching purposes.
     * @return A ready-to-be-used filter configured from the zone filter data descriptor.
     */
    RE::AbstractFilter*                 imageFilterFromData             (RE::T::ZoneFilterData const &zfd, QString cacheHash) const;
    static RE::AbstractFilter*          imageFilterFromData             (RE::T::ZoneFilterData const &zfd);

    /*! @brief Recognizer Factory Getter.
     * Returns a read-only list that holds in-memory available recognizers for faster instanciation, based on their name and binary location.
     * @see ObjectsFactory
     * @return The instance currently being used.
     */
    ObjectsFactory const&				recognizersFactory				() const;
    /*! @brief Zone Recognizer Data Creator.
     * This method creates a new zone recognizer data object to be filled up later, based on a class name.
     * @param className is the recognizer's class name for which to generate the mapping.
     * @param relRect a RE::T::ZoneRecognizerData preconfigured to hold the filter parameters.
     * @return A RE::T::ZoneRecognizerData pre-filled with informations based on the inputs.
     */
    RE::T::ZoneRecognizerData			makeZoneRecognizerData			(QString const &className, QRectF relRect) const;
    /*! @brief Recognizer Instanciator.
     * This method returns an instance of a ready-to-be-used recognizer based on a zone recognizer data descriptor object.
     * The recognizer is filled with the parameters present in the RE::T::ZoneRecognizerData::parameters member, as well as its
     * rectangle and filters stack (RE::T::ZoneRecognizerData::relativeRect and RE::T::ZoneRecognizerData::filterStack).
     * Optionnaly, a cache hash can be specified, allowing caching of the resulting object for later re-use.
     * If the caching hash is already present, then the object that was previously created is returned. This is why the resulting object
     * has to not be directly deleted by the developper himself.
     * @see RE::T::ZoneRecognizerData
     * @param zrd is the zone filter data descriptor object from which to build the filter.
     * @param cacheHash is a reuse-identified that will be used for caching purposes.
     * @return A ready-to-be-used filter configured from the zone filter data descriptor.
     */
    AbstractRecognizer*					imageRecognizerFromData         (RE::T::ZoneRecognizerData const &zrd, QString cacheHash) const;

    /*! @brief Preprocessed Image Runner.
     * Convenience method for RE::Engine::filteredImages().
     * @see RE::Engine::filteredImages()
     * @param zrfs
     * @param cacheHash
     * @param img
     * @param pcc is the number of physical cores to use. This parameter is later passed to each filter, and the behavior induced by changing this parameter depends on how filters are built.
     * @return A QImage being the result of the filtering.
     */
    QImage                              filteredImage                   (RE::T::ZoneFilterData::List const &zrfs, QString const &cacheHash, QImage const &img, quint32 const &pcc) const;
    /*! @brief Preprocessed Images Runner.
     * This method preprocesses an image based on a filter stack. It allows to retrieve two images:
     * - One being the preprocessed image,
     * - The second being the last debugging image given by any of the filters stack.
     * @param zrfs is the filters stack to be applied on the image.
     * @param cacheHash is the reuse-identified to allow sub-methods to re-use already instanciated objects.
     * @param img is the input image to be filtered.
     * @param pcc is the number of physical cores to use. This parameter is later passed to each filter, and the behavior induced by changing this parameter depends on how filters are built.
     * @param debug When set to true, it instructs the method to keep the debugging images for every and each filter.
     * @return An array (QList<QImage>) of images, the first being the result of the filtering, the second being the debugging image, if any.
     */
    QList<QImage>                       filteredImages                  (RE::T::ZoneFilterData::List const &zrfs, QString const &cacheHash, QImage const &img, quint32 const &pcc, bool debug) const;

    /*! @brief Per-Zone Image-Filtering Runner.
     * This method applies a stack of filters on a given image as specified in the passed zone recognizer data filters stack. This image can later be used
     * with the zone recognizer data recognizer.
     * @param zrd is the zone recognizer data whose filters stack will be applied to the image.
     * @param cacheHash is the reuse-identified to allow sub-methods to re-use already instanciated objects.
     * @param img is the input image to be filtered.
     * @param pcc is the number of physical cores to use. This parameter is later passed to each filter, and the behavior induced by changing this parameter depends on how filters are built.
     * @return The input image processed with the filters stack present in the zone recognizer data object.
     */
    QImage								filteredRecognizerImage			(RE::T::ZoneRecognizerData const &zrd, QString const &cacheHash, QImage const &img, quint32 const &pcc) const;
    /*! @brief Recognition Runner.
     * This method is in charge of extracting data off an image, based on an informationnal zone recognizer data object.
     * The engine runs this method once the image has been filtered for the same zone recognizer data, for its given rectangle with its given
     * filters stack. The result of this method is a hash containing pairs of strings (Key / Value), as the recognizer exports them.
     * @param zrd is the zone recognizer data object to run the recognition against.
     * @param cacheHash is the reuse-identified to allow sub-methods to re-use already instanciated objects.
     * @param img is the image to run recognition on.
     * @param pcc is the number of physical cores to use. This parameter is passed to the recognizer, and the behavior induced by changing this parameter depends on how the recognizer is built.
     * @return An key-value based map (QHash<QString,QString>) containing the result of the recognizer.
     */
    RE::T::StringToVariantHash          recognizerResults               (RE::T::ZoneRecognizerData const &zrd, QString const &cacheHash, QImage const &img, quint32 const &pcc) const;

    /*! @brief Filters Parameters Getter.
     * When the engine builds parameters list for objects (Filters, Recognizers), it needs a fast way to configure them with their parameters
     * set to the right type. This method uses a cache that is built once upon running the engine and which contains default parameters lists for each
     * available filter.
     * @see RE::Engine::filtersFactory()
     * @param n is the filter name for which to get a parameters list.
     * @return A key-value based-map (QVariantMap) containing the filter parameters defaults.
     */
    QVariantMap                         filterParameters                (QString const &n) const { return _filtersParameters[n]; }
    /*! @brief Recognizers Parameters Getter.
     * When the engine builds parameters list for objects (Filters, Recognizers), it needs a fast way to configure them with their parameters
     * set to the right type. This method uses a cache that is built once upon running the engine and which contains default parameters lists for each
     * available recognizer.
     * @see RE::Engine::recognizersFactory()
     * @param n is the recognizer name for which to get a parameters list.
     * @return A key-value based-map (QVariantMap) containing the recognizer parameters defaults.
     */
    QVariantMap                         recognizerParameters            (QString const &n) const { return _recognizersParameters[n]; }
    /*! @brief Relative Rectangle Conversion Helper.
     * As the client binary zones are defined a relative way, this method offers a help to convert those to absolute notation.
     * @param relR is the relative rect to be converted. Its dimentions are considered to be percentages of the given size.
     * @param sz is the reference size to use when converting.
     * @return An absolute rectangle (QRect) that gives specific coordinates based on the inputs.
     */
    static QRect						relativeRectToAbsolute			(QRectF const &relR, QSize const &sz);

    /*! @brief Client Binary.
     * This method is in charge of loading a client binary (In RELibraryAdmin export format) to it's expanded form, usable by the engine.
     * In turn, the following data are read and setup in the engine:
     * - Load document-specific data (Zones, scripts, etc).
     * - Load FANN data and populate binary FANN object.
     * To ensure that the client binary is compatible with the binary version of the engine, a stream version number is compared to the expected
     * client binary version. If the client binary is older than the expected one, the client binary is loaded with in-memory conversion. If the
     * client binary is more recent than the expected one, the method returns a failure.
     * @see RE::T::ClientBinaryData, RE::Engine::isClientBinaryDataCompatible()
     * @param path is the full path from where the client binary data will be loaded.
     * @return true for a success, false for a failure.
     */
    bool								loadClientBinaryData			(QString const &path);
    /*! @brief Client Binary Data getter.
     * This method allows to retrieve the previously loaded Client Binary Data object.
     * @see RE::T::ClientBinaryData, RE::Engine::loadClientBinaryData()
     * @return A RE::T::ClientBinaryData object.
     */
    RE::T::ClientBinaryData const&		clientBinaryData				() const;
    /*! @brief Client Binary Version Checker.
     * @see RE::T::ClientBinaryData::version, RE::ClientBinaryDataVersion(), RE::Engine::loadClientBinaryData()
     * @param cbd is the Client Binary Data to be checked.
     * @return true for a success, false for a failure.
     */
    static bool                         isClientBinaryDataCompatible    (RE::T::ClientBinaryData const &cbd);

    /*! @brief Recognition Document Type Detection Runner.
     * This method uses the information provided within the currently loaded Client Binary Data object to determine the type of a given Recognition
     * Document. In turn, these checks are performed:
     * - MICR checks,
     * - Image ANN type finding.
     * Once the type has been found, it is attached to the Recognition Document via the RE::T::Document::typeData member as a pointer to the
     * Client Binary Data real type. Also, the RE::T::Document::micrMatches flag is set by this method based on the Client Binary Data object defined workflow.
     * To be successful, the Recognition Document has to correctly match the Client Binary Data rules, including the required ANN thrshold when defined.
     * @see RE::T::Document, RE::T::ClientBinaryData, RE::Engine::recognizeDocumentData()
     * @param doc is the Recognition Document for which to dermine the type.
     * @param pcc is the number of physical cores to use. This parameter is later passed to each recognizer and filter, and the behavior induced by changing this parameter depends on how filters are built.
     * @return true for a successful type guessing, false for a failure.
     */
    bool								guessDocumentType				(RE::T::Document *doc, quint32 const &pcc) const;
    /*! @brief Recognition Document Recognition Runner.
     * For a successful recognition, the Recognition Document has to have its type properly discovered.
     * @see RE::Engine::guessDocumentType()
     * @param doc is the Recognition Document for which to attempt a data recognition.
     * @param pcc is the number of physical cores to use. This parameter is passed to each recognizer and filter, and the behavior induced by changing this parameter depends on how the recognizer is built.
     * @param threadId is the identifier of a thread to run the process under. It basically creates a subset of all caches to avoid collisions.
     * @return true for a successful extraction, false for an error.
     */
    bool								recognizeDocumentData			(RE::T::Document *doc, quint32 const &pcc, quint32 const &threadId=0) const;
    /*! @brief Recognition Document Validity Runner.
     * @param doc is the Recognition Document for which to determine the validity.
     * @return true for a valid document, false for a failure.
     */
    bool								determineDocumentValidity		(RE::T::Document *doc) const;
    /*! @brief Recognition Document Type Helper.
     * @param code
     * @return
     */
    RE::T::DocumentTypeData const*		documentTypeDataForCode			(qint32 code) const;

    /*! @brief ANN Data to Binary Format Converter.
     * @param ann
     * @return
     */
    static QByteArray                   annToBinary						(fann *ann);
    /*! @brief Binary Format to ANN Data converter.
     * @param ad
     * @return
     */
    static fann*                        binaryToAnn						(QByteArray &ad);

    /*! @brief Image Signature Algorythm Runner.
     * @param img
     * @param pcc is the number of physical cores to use. This parameter is passed to each filter, and the behavior induced by changing this parameter depends on how the recognizer is built.
     * @return
     */
    QByteArray							signatureForImage				(QImage const &img, quint32 const &pcc) const;

    //! @brief QImage to OpenCV::Mat Conversion.
    //static cv::Mat                      qImage2cvMat                    (QImage const *qImg);
    //! @brief OpenCV::Mat to QImage Conversion.
    //static QImage                       cvMat2qImage                    (cv::Mat const *cvMat);

    /*! @brief Normalized Palette Building Helper.
     * @return
     */
    static QVector<QRgb> const&         grayscalePalette                ();
    /*! @brief Image Palette Normalizer Helper.
     * @param img
     * @param force
     * @return
     */
    static QImage                       paletizeIfGrayscale             (QImage const &img, bool force=false);

protected:
    /*! @brief Plugins Manager Instance Getter.
     * @return
     */
    PluginsManager&						pluginsManager					();
    /*! @brief Plugins Discovery Runner.
     */
    void								parsePlugins					();

private:
    //! @brief Filter Cache Type Definition.
    typedef QHash<QString,RE::AbstractFilter*>      InstanciatedFilters;
    //! @brief Recognizer Cache Type Definition.
    typedef QHash<QString,RE::AbstractRecognizer*>  InstanciatedRecognizers;

    //! @brief Licensing Manager Instance.
    LM::Core                            *_lm;
    //! @brief Licenseing Status Flag.
    bool                                _licensed;
    //! @brief Hardware UUID.
    QString                             _hostUuid;
    //! @brief Obfuscation Cheesecake.
    quint8                              *_obfCheesecake;

    //! @brief Filters Factory.
    ObjectsFactory						_filtersFactory;
    //! @brief Recognizers Factory.
    ObjectsFactory						_recognizersFactory;

    //! @brief Filters Parameters Cache.
    QHash<QString,QVariantMap>			_filtersParameters;
    //! @brief Recognizer Parameters Cache.
    QHash<QString,QVariantMap>			_recognizersParameters;

    //! @brief Filters Caching Mutex.
    mutable QMutex                      _filterCacheLock;
    //! @brief Filters Cache.
    mutable InstanciatedFilters			_instanciatedFilters;
    //! @brief Recognizers Caching Mutex.
    mutable QMutex                      _recognizerCacheLock;
    //! @brief Recognizers Cache.
    mutable InstanciatedRecognizers		_instanciatedRecognizers;

    //! @brief Main Plugins Manager Instance.
    PluginsManager						*_pluginsManager;
    //! @brief Instanciated Objects Location Cache.
    QHash<QString,QString>				_objectsLocations;
    //! @brief Available Enumerators Ranges.
    MetaTypesRangeListHash				_enumeratorRanges;

    //! @brief Main Scripting Engine.
    mutable QScriptEngine				*_scriptEngine;
    //! @brief Scripting Objects.
    mutable QScriptValue				*_scriptObject;
    //! @brief Per-Document-Type Scripting Function Cache.
    mutable QHash<quint32,QScriptValue>	_scriptFunctionsByType;
    //! @brief Main Scripting Engine Lock.
    mutable QMutex                      _scriptLock;

    //! @brief Loaded Client Binary Path.
    QString								_clientBinaryPath;
    //! @brief Current Client Binary Data.
    T::ClientBinaryData					_clientBinaryData;
    //! @brief Current Client Binary ANN.
    fann								*_clientBinaryAnn;
};

}

#endif
