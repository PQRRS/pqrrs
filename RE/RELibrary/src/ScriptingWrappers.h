#ifndef RE_ScriptingWrappers_h
#define RE_ScriptingWrappers_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include "RETypes.h"
#include <QtCore/QMap>
#include <QtCore/QDebug>
#include <QtCore/QStringList>

/*! @brief The SWStringList class.
 * @todo Better Documentation, Use Case.
 * This class is a mapping object from QList<String> to the Scripting Engine. Any member marked with Q_INVOKABLE
 * will be accessible from the Scripting Engine.
 */
class SWStringList : public QObject {
Q_OBJECT
public:
    /**/						SWStringList		(QStringList *list, QObject *p=0) : QObject(p), _list(list) {}
    /**/						~SWStringList		() {}
	Q_INVOKABLE bool			contains			(QString const &v) const {
		return _list->contains(v);
    }
	Q_INVOKABLE void			append				(QString const &v) {
		_list->append(v);
    }
	Q_INVOKABLE void			remove				(QString const &v) {
		_list->removeAll(v);
    }
private:
	QStringList					*_list;
};

/*! @brief The SWStringList class.
 * @todo Better Documentation, Use Case.
 * This class is a mapping object from QMap<String,String> to the Scripting Engine. Any member marked with Q_INVOKABLE
 * will be accessible from the Scripting Engine. */
class SWStringToStringMap : public QObject {
Q_OBJECT
public:
    /**/						SWStringToStringMap	(RE::T::StringToStringMap *map, QObject *p=0) : QObject(p), _map(map) {}
    /**/						~SWStringToStringMap () {}
	Q_INVOKABLE bool			contains			(QString const &k) const {
		return _map->contains(k);
    }
	Q_INVOKABLE QString			value				(QString const &k) const {
		return _map->contains(k) ? _map->value(k) : QString();
    }
	Q_INVOKABLE void			setValue			(QString const &k, QString const &v) {
		(*_map)[k]	= v;
    }
	Q_INVOKABLE void			remove				(QString const &k) {
		_map->remove(k);
	}
private:
	RE::T::StringToStringMap	*_map;
};

class SWResults : public QObject {
    Q_OBJECT
    public:
        /**/						SWResults           (RE::T::Results *results, QObject *p=0) : QObject(p), _results(results) {}
        /**/						~SWResults          () {}
        Q_INVOKABLE bool			contains			(QString const &k) const {
            return _results->contains(k);
        }
        Q_INVOKABLE RE::T::StringToVariantHash&
                                    value				(QString const &k) {
            return (*_results)[k];
        }
        Q_INVOKABLE void			setValue			(QString const &k, RE::T::StringToVariantHash const &v) {
            (*_results)[k]	= v;
        }
        Q_INVOKABLE void			remove				(QString const &k) {
            _results->remove(k);
        }
    private:
        RE::T::Results              *_results;
};

class SWStringToVariangHash : public QObject {
    Q_OBJECT
    public:
        /**/						SWStringToVariangHash   (RE::T::StringToVariantHash *hash, QObject *p=0) : QObject(p), _hash(hash) {}
        /**/						~SWStringToVariangHash  () {}
        Q_INVOKABLE bool			contains			(QString const &k) const {
            return _hash->contains(k);
        }
        Q_INVOKABLE QVariant&       value				(QString const &k) {
            return (*_hash)[k];
        }
        Q_INVOKABLE void			setValue			(QString const &k, QVariant const &v) {
            (*_hash)[k]	= v;
        }
        Q_INVOKABLE void			remove				(QString const &k) {
            _hash->remove(k);
        }
    private:
        RE::T::StringToVariantHash  *_hash;
};


#endif
