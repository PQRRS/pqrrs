#ifndef RE_ObjectsFactory_hpp
#define RE_ObjectsFactory_hpp

// Required stuff.
#include <QtCore/QHash>
#include <QtCore/QString>
#include <QtCore/QMetaObject>

namespace RE {

/*!
 * @brief The ObjectsFactory Class.
 * This class exposes a way to register objects with any string key as an associative reminder.
 * It allows instanciation of any QObject-based derivative to be instanciated based on an arbitrary
 * key (Usually it's full class name including it's namespace).
 * In the context of the RELibrary suite, it allows fast reflective instanciation based on names stored
 * in Client Binary objects, without going through the hassle of manipulating underlying Qt meta-data every time.
 * Also, it is important to remember that the use of an HPP file is on purpose, as it allows the compiler to optimize
 * inline unit compilation in binary code when possible. This speeds up the runtime execution of objects instanciation
 * and shouldn't be changed in the future.
 *
 * The typical workflow of usage for the ObjectsFactory class follows:
 *
 * @code{.cpp}
 * // Create a factory, usually stored as another class member.
 * ObjectsFactory   factory;
 * // Objects can be registered this way, without instanciation:
 * factory.registerClass<QObject::Based::Class::Including::NameSpace>();
 * // Objects can also be registered this way, but it requires meta-object lookup prior to registration:
 * factory.registerClass(SomeMetaObjectPointer);
 * // Then, the objects can be instanciated the following way (Beware that there is no type-check assertions to speed-up execution):
 * QObject::Based::Class *instance = _factory.instanciateClass("QObject::Based::Class::Including::NameSpace");
 * // After using *instance, it needs to be deleted as any other C++ object, as it was instanciated on the heap rather than the stack.
 * delete instance;
 * // Optionnaly, the un-registration of the object can be performed:
 * factory.unregisterClass<QObject::Based::Class::Including::NameSpace>();
 * // Or:
 * factory.unregisterClass(SomeMetaObjectPointer);
 * @endcode
 */
class ObjectsFactory {
public:
    //! @brief Caching Hash Type Definition.
	typedef QHash<QString,const QMetaObject*> CacheHash;
	
    /*! @brief Class Registration Method by Template.
     * This method allows registration of any QObject-based subclass within the
     * current objects factory instance. As it doesn't take any parameter,
     * it is important to understand that the type of object will be used to template
     * the method rather than anything else.
     */
    template<class T>
    void                registerClass			() {
		QString				cn		(T::staticMetaObject.className());
        if (!_registeredTypes.contains(cn))
            _registeredTypes.insert(cn, &T::staticMetaObject);
	}
    /*! @brief Class Registration Method by Parameter.
     * This method allows registration of any QObject-based subclass within the
     * current objects factory instance. It takes one parameter that will be used
     * later for object re-instanciation.
     * @param[in] mo is the meta-object to be registered in association with it's full class name (Including its namespace).
     */
    void                registerClass			(QMetaObject const *mo) {
		QString				cn		(mo->className());
        if (!_registeredTypes.contains(cn))
            _registeredTypes.insert		(cn, mo);
	}
	
    /*! @brief Class Unregistration Method by Template.
     * This method allows unregistration of any QObject-based subclass within the
     * current objects factory instance, if it has been previously registered with it.
     * it is important to understand that the type of object that will be used to template
     * the method used rather than anything else.
     */
    template<class T>
    void                unregisterClass			() {
		_registeredTypes.remove(T::staticMetaObject.className());
	}
    /*! @brief Class Unregistration Method by Parameter.
     * This method allows registration of any QObject-based subclass within the
     * current objects factory instance. It takes one parameter that will be used
     * later for object re-instanciation.
     * @param[in] mo is the meta-object to be unregistered from the factory.
     */
    void				unregisterClass			(QMetaObject const *mo) {
		_registeredTypes.remove(mo->className());
	}

    /*! @brief Main Instanciation Method.
     * This method instanciates any previously-registered class based on it's full class name including namespace.
     * @param[in] type is the name of the class to be instanciated.
     * @return An instance of the requested class, which later needs to be released like if it has been created using the "new" keyword.
     */
    QObject*			instanciateClass		(QString const &type) const {
        const QMetaObject	*m		= _registeredTypes.value(type);
        return m ? m->newInstance() : 0;
	}

    /*! @brief Registration Manifest Getter.
     * This method allows the retrieval of all successfully registered classes for inspection.
     * @return A RE::ObjectsFactory::CacheHash object containing all registered types along with their associative name.
     */
	CacheHash const&	registeredTypes			() const {
		return _registeredTypes;
	}
private:
	CacheHash			_registeredTypes;
};

}

#endif
