#ifndef RE_TypeWrapper_hpp
#define RE_TypeWrapper_hpp

#include <QtCore/QDataStream>
#include <QtCore/QVariant>

namespace RE {

#ifndef DECLARE_TYPE_WRAPPER
/*! @brief Primitive Wrapper from Platform-Native to Strongly Typed Object.
 * This macro helps the developper with super-fast declaration of wrappers around native types.
 * @see RE::TypeWrapper
 */
#define DECLARE_TYPE_WRAPPER(Cls, typ)				\
class Cls : public TypeWrapper<typ> {				\
public:												\
    Cls ()					: TypeWrapper<typ>(){}	\
    Cls (typ const &t)		: TypeWrapper<typ>(t){}	\
    Cls (Cls const &c)		: TypeWrapper<typ>(c){}	\
};
#endif

#ifndef DECLARE_INDIRECT_WRAPPER
/*! @brief Primitive Wrapper to a Strongly Typed Inherited Object.
 * This macro helps the developper with super-fast declaration of wrappers around native types, with a layer of in-between
 * inheritence.
 * @see RE::TypeWrapper
 */
#define DECLARE_INDIRECT_WRAPPER(Cls, Bas, typ)		\
class Cls : public Bas {						\
public:												\
    Cls ()					: Bas()				{}	\
    Cls (typ const &t)		: Bas(t)			{}	\
    Cls (Cls const &c)		: Bas(c)			{}	\
};
#endif

/*! @brief Native to Strongly-Typed Object Wrapper.
 * As it is important for the engine to be able to do the distinction between types occupying the same amount of
 * memory at run-time, this class allows easy-wrapping of such native type into full recognizible objects. This class
 * offers the advantage of enabling meta-object declaration of any native type more than once, and exposes basic and common
 * operator overloading (=, Stream<<, Stream>>, ByRef const access, ByRef access) all at once.
 */
template<typename T>
class TypeWrapper {
public:
    //! @brief Default Constructor.
                        TypeWrapper			()							: _t()				{}
    //! @brief Constructor with Templated Argument.
                        TypeWrapper			(T const &t)				: _t(t)				{}
    //! @brief Copy-Constructor.
                        TypeWrapper			(TypeWrapper<T> const &w)	: _t(w._t)			{}
    //! @brief Constructor with Templated QVariant Argument.
                        TypeWrapper			(QVariant const &v)			: _t(v.toDouble)	{}

    //! @brief Cast to Template.
                        operator T const&	() const								{ return _t; }
    //! @brief Cast to QVariant.
                        operator QVariant	() const								{ return qVariantFromData(*this); }
    //! @brief By-Constant-Reference Accessor.
    T const&			value				() const								{ return _t; }
    //! @brief By-Reference Accessor.
    T&					value				()										{ return _t; }

    //! @brief Operator Overloading with Templated Argument.
    TypeWrapper<T>&		operator=			(T const &t)							{ _t = t; return *this; }

    //! @brief To-Stream Operator Overload.
    friend QDataStream&	operator<<		(QDataStream& s, TypeWrapper<T> const &w)	{ return s << w._t; }
    //! @brief From-Stream Operator Overload.
    friend QDataStream&	operator>>		(QDataStream& s, TypeWrapper<T> &w)			{ return s >> w._t; }

private:
    //! @brief Template Container.
    T					_t;
};

}

#endif
