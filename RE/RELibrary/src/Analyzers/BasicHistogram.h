#ifndef RE_BasicHistogram_h
#define RE_BasicHistogram_h

// Base class.
#include "AbstractAnalyzer.h"
// Required stuff.
#include <QtCore/QList>

namespace RE {

/*! @brief Basic Histogram Analyzer Class.
 * This class computes histogram for any plane of a given image.
 * As it will honor the plane property, it's important to understand that computing an histogram on a multi-component
 * image for a specific plane would ignore the other plane(s).
 * Once computed, the result (RE::T::UIntVector) will be a 256-element array, containing occurences of each color per index.
 * For instance, at index #0 it will tell the number of times pure black was found on the image, while it would tell how many times
 * pure white was found in the image at its #255 index.
 * @see RE::CumulativeHistogram */
class RELibOpt BasicHistogram : public AbstractAnalyzer {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractAnalyzer)

public:
    //! @see RE::AbstractAnalyzer::AbstractAnalyzer()
    Q_INVOKABLE				BasicHistogram	(QObject *p=0);
    //! @brief Results Accessor.
	T::UIntVector const&	data			()	const	{ return _data; }

protected:
    //! @see RE::AbstractAnalyzer::doRun()
    virtual bool			doRun			();
    //! @see RE::AbstractAnalyzer::doRun()
    template<typename K>
    bool                    algo            ();

private:
	T::UIntVector			_data;

};

}

#endif
