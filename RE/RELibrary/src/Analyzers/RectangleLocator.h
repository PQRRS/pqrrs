#ifndef RE_RectangleLocator_h
#define RE_RectangleLocator_h

// Base class.
#include "AbstractAnalyzer.h"
// Required stuff.
#include <QtCore/QList>

namespace RE {

/*! @brief Object Locator Class.
 * This analyzer finds object within given image limits and within given gray thresholds.
 * It can be used to determine the location of an object within a large background. Its output is a QRect pointing to the
 * found object's boundaries in the image.
 * @see RE::CumulativeHistogram */
class RELibOpt RectangleLocator : public AbstractAnalyzer {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractAnalyzer)
    /*! @brief Step Accessors.
     * This property holds the step (In pixels) with which to iterate in each enabled direction.
     * The larger this property is, the less accurate will be the result, but the fastest the processing will be. */
    Q_PROPERTY(quint16  step                        READ step                   WRITE setStep               RESET resetStep)
    /*! @brief Lower Limit Color Threshold.
     * This property marks the lower limit below which any matching pixel will be considered "background". */
    Q_PROPERTY(QColor   lowerLimit                  READ lowerLimit             WRITE setLowerLimit         RESET resetLowerLimit)
    /*! @brief Upper Limit Color Threshold.
     * This property marks the upper limit above which any matching pixel will be considered "background". */
    Q_PROPERTY(QColor   upperLimit                  READ upperLimit             WRITE setUpperLimit         RESET resetUpperLimit)
    //! @brief Search From Left Border Flag Property.
    Q_PROPERTY(bool     fromLeft                    READ fromLeft               WRITE setFromLeft           RESET resetFromLeft)
    //! @brief Search From Right Border Flag Property.
    Q_PROPERTY(bool     fromRight                   READ fromRight              WRITE setFromRight          RESET resetFromRight)
    //! @brief Search From Top Border Flag Property.
    Q_PROPERTY(bool     fromTop                     READ fromTop                WRITE setFromTop            RESET resetFromTop)
    //! @brief Search From Bottom Border Flag Property.
    Q_PROPERTY(bool     fromBottom                  READ fromBottom             WRITE setFromBottom         RESET resetFromBottom)
    //! @brief Result Getter Property.
    Q_PROPERTY(QRect    boundaries                  READ boundaries)

public:
    //! @see RE::AbstractAnalyzer::AbstractAnalyzer()
    Q_INVOKABLE                     RectangleLocator        (QObject *p=0);

    // Step accessors.
    quint16 const&                  step                    () const                            { return _step; }
    void                            setStep                 (quint16 const &v)                  { _step = v; }
    void                            resetStep               ()                                  { _step = 10; }
    // Limits accessors.
    QColor const&                   lowerLimit              () const                            { return _lowerLimit; }
    void							setLowerLimit           (QColor const &v)                   { _lowerLimit = v; }
    void                            resetLowerLimit         ()                                  { setLowerLimit(QColor::fromRgb(0,0,0)); }
    QColor const&                   upperLimit              () const                            { return _upperLimit; }
    void							setUpperLimit           (QColor const &v)                   { _upperLimit = v; }
    void                            resetUpperLimit         ()                                  { setUpperLimit(QColor::fromRgb(255,255,255)); }
    // Direction flags accessors.
    bool                            fromLeft                () const                            { return _fromLeft; }
    void                            setFromLeft             (bool v)                            { _fromLeft = v; }
    void                            resetFromLeft           ()                                  { _fromLeft = false; }
    bool                            fromRight               () const                            { return _fromRight; }
    void                            setFromRight            (bool v)                            { _fromRight = v; }
    void                            resetFromRight          ()                                  { _fromRight = false; }
    bool                            fromTop                 () const                            { return _fromTop; }
    void                            setFromTop              (bool v)                            { _fromTop = v; }
    void                            resetFromTop            ()                                  { _fromTop = false; }
    bool                            fromBottom              () const                            { return _fromBottom; }
    void                            setFromBottom           (bool v)                            { _fromBottom = v; }
    void                            resetFromBottom         ()                                  { _fromBottom = false; }
    // Result accessor.
    QRect const&                    boundaries              () const                            { return _boundaries; }

protected:
    //! @see RE::AbstractAnalyzer::doRun()
    virtual bool                    doRun                   ();
    //! @see RE::AbstractAnalyzer::doRun()
    template<typename K>
    bool                            algo                    ();

private:
    quint16                         _step;
    QColor                          _lowerLimit;
    QColor                          _upperLimit;
    bool                            _fromLeft,
                                    _fromRight,
                                    _fromTop,
                                    _fromBottom;
    QRect                           _boundaries;

};

}

#endif
