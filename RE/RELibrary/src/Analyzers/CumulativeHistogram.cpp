#include "CumulativeHistogram.h"

RE::CumulativeHistogram::CumulativeHistogram (QObject *p)
: RE::BasicHistogram(p) {
}

bool RE::CumulativeHistogram::doRun () {
	// Build superclass histogram.
    RE::BasicHistogram::doRun();
	// Get parent results.
    quint32 const	*data		= RE::BasicHistogram::data().constData();
    _cumulativeData.clear		();
    _cumulativeData.resize		(256);
    quint32			*cumData	= _cumulativeData.data();
    memset                      (cumData, 0, sizeof(quint32)*256);
    quint32         total       = 0;
	// Build cumulative array.
    for (quint16 i=0; i<256 ; ++i) {
        total					+= data[i];
        cumData[i]				= total;
    }
    return true;
}
