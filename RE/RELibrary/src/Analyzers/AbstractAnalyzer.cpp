#include "AbstractAnalyzer.h"
#include <QtCore/QDebug>

RE::AbstractAnalyzer::AbstractAnalyzer (QObject *p)
: QObject(p), _runtimeContext("", 0), _parallelizedCoresCount(1), _debuggingEnabled(false) {
}

bool RE::AbstractAnalyzer::run () {
    if (!_source || _source->isNull()) {
		Crt				<< "Invalid source image!";
		return			false;
	}
    return doRun();
}
