#ifndef RE_AbstractAnalyzer_h
#define RE_AbstractAnalyzer_h

// Required stuff.
#include "../RETypes.h"
#include "../Engine.h"
#include <QtGui/QColor>
#include <QtGui/QImage>

namespace RE {

/*! @brief Base Image Analysis Class.
 * This is the base class for any image analyzer, as it makes the process of creating an image analyzis object simpler while normalizing IOs.
 * As every single analyzer has different results, there is no generic result getter. It's up to the developper to check
 * within each Analyzer subclass documentation to understand what property to access for reading the results.
 * The main processing logic will be executed by calling RE::AbstractFilter::run() from the outside, internally checking for problems with the image
 * and then calling the doRun() method.
 * Also, as any filter / analyzer / recognizer accepts QImage as it's main input, it's common practice to
 * relocate complex computation in a templatized method (Eg. template<typename K> bool algo()), and then derive
 * processing for chunks of images when possible according to the parallelizedCoresCount property (Eg. in a template<typename K>
bool parallelizedByteAlgo (QImage const *src, qint32 z, qint32 yStart, qint32 yEnd) synchronized by QFuture-like helpers.). */
class RELibOpt AbstractAnalyzer : public QObject {
public:
    Q_OBJECT
    /*! @brief Useable Parallelization Physical Cores Property.
     * As the image processing takes place in the inherited class, there is no guarantee that this property
     * will induce the same result on different analyzers, however using existing analyzer source code as a foundation
     * when creating a new one will ensure coherence is respected. */
    Q_PROPERTY(quint32  parallelizedCoresCount  READ parallelizedCoresCount WRITE setParallelizedCoresCount RESET resetParallelizedCoresCount)
    /*! @brief Plane Identifier to Run the Analyzer On Property.
     * As some analyzers can be CPU-intensive, it's possible to specify which plane will be processed via this property.
     * However, as the image processing takes place in the inherited class, there is no guarantee that this property
     * will induce the same result on different analyzers, however using existing analyzer source code as a foundation
     * when creating a new one will ensure coherence is respected. For instance, a BasicHistogram object will use the plane
     * index directive while doing the math, while an OtsuThresholdFinder won't. */
    Q_PROPERTY(quint8   plane                   READ plane                  WRITE setPlane                  RESET resetPlane)
    //! @brief Full Image Analysis Flag Property.
    Q_PROPERTY(bool     useFullImage            READ useFullImage           WRITE setUseFullImage           RESET resetUseFullImage)
    //! @brief Keep Filtered Image After Analysis Flag Property.
    Q_PROPERTY(bool     keepFilteredImage       READ keepFilteredImage      WRITE setKeepFilteredImage      RESET resetKeepFilteredImage)
    //! @brief Debugging Mode Flag Property.
    Q_PROPERTY(bool     debuggingEnabled        READ debuggingEnabled       WRITE setDebuggingEnabled       RESET resetDebuggingEnabled)

protected:
    //! @brief Default Constructor.
    Q_INVOKABLE             AbstractAnalyzer        (QObject *p=0);

public:
    // Context accessors.
    RE::RuntimeContext const&
                            runtimeContext              () const                { return _runtimeContext; }
    void                    setRuntimeContext           (RE::RuntimeContext  const &v) { _runtimeContext = v; }
    // Internal data accessors.
    quint32 const&          parallelizedCoresCount      () const                { return _parallelizedCoresCount; }
    void                    setParallelizedCoresCount   (quint32 const &v)      { _parallelizedCoresCount = v; }
    void                    resetParallelizedCoresCount ()                      { _parallelizedCoresCount = 1; }
    virtual QImage const*	source                      () const                { return _source; }
    virtual void			setSource                   (QImage const *v)       { _source = v; }
    virtual void            resetSource                 ()                      { _source = 0; }
    virtual quint8          plane                       () const                { return _plane; }
    virtual void            setPlane                    (quint8 const &v)       { _plane = v; }
    virtual void            resetPlane                  ()                      { _plane = 0; }
    virtual bool            useFullImage                () const                { return _useFullImage; }
    virtual void            setUseFullImage             (bool v)                { _useFullImage = v; }
    virtual void            resetUseFullImage           ()                      { _useFullImage = false; }
    virtual bool            keepFilteredImage           () const                { return _keepFilteredImage; }
    virtual void            setKeepFilteredImage        (bool v)                { _keepFilteredImage = v; }
    virtual void            resetKeepFilteredImage      ()                      { _keepFilteredImage = false; }
    // Debug output accessors.
    virtual bool const&     debuggingEnabled            () const                { return _debuggingEnabled; }
    virtual void            setDebuggingEnabled         (bool const &v)         { _debuggingEnabled = v; }
    virtual void            resetDebuggingEnabled       ()                      { _debuggingEnabled = false; }
    /*! @brief Main Run Method.
     * This method on the parent abstract class will in-turn call the AbstractAnalyzer::doRun() method
     * which has to be implemented on derived classes.
     * The doRun method, excepted for very special cases, will be called after the source image validity has
     * been checked.
     * @return A boolean flagging the success (true) or failure (false) result of the run method. */
    virtual bool            run                         ();

protected:
    /*! @brief Subclass Pure Virtual Logic Method.
     * This method has to be implemented in subclasses, and is the main place to put logic in.
     * @return A boolean flagging the success (true) or failure (false) result of the doRun method. */
    virtual bool			doRun                       () = 0;

private:
    RE::RuntimeContext      _runtimeContext;
    quint32                 _parallelizedCoresCount;
    QImage const            *_source;
    quint32                 _plane;
    bool                    _useFullImage;
    bool                    _keepFilteredImage;
    bool                    _debuggingEnabled;
};

}

Q_DECLARE_INTERFACE(RE::AbstractAnalyzer, "fr.PierreQR.RecognitionSuite.RELibrary.AbstractAnalyzer/1.0")

#endif
