#include "RectangleLocator.h"
#include "../PixelHelper.hpp"

RE::RectangleLocator::RectangleLocator (QObject *p)
: RE::AbstractAnalyzer(p) {
}

bool RE::RectangleLocator::doRun () {
    QImage::Format  fmt     = source()->format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      false;
    }
}

template<typename K>
bool RE::RectangleLocator::algo () {
    QImage const       *src        = source();
    PixelReader<K> const
                        pr          (src);

    quint8 const        lowGray    = qGray(_lowerLimit.rgba());
    quint8 const        highGray   = qGray(_upperLimit.rgba());

    // Start from top, and find most-left matching pixel.
    qint32              left    = 0;
    if (_fromLeft) {
        quint8          gray;
        bool            search  = true;
        left                    = pr.width-1;
        for (qint32 x=0; search && x<pr.width; ++x) {
            for (qint32 y=0; y<pr.height-_step; y+=_step) {
                gray            = pr.gray(x, y);
                if (gray>=lowGray && gray<=highGray && left>x) {
                    left        = x;
                    search      = false;
                    break;
                }
            }
        }
        if (left>=pr.width-1)   left = 0;
    }
    // Start from top, and find the most right-matching pixel.
    qint32              right   = pr.width-1;
    if (_fromRight) {
        quint8          gray;
        bool            search  = true;
        right                   = 0;
        for (qint32 x=pr.width-1; search && x>=0; --x) {
            for (qint32 y=0; y<pr.height-_step; y+=_step) {
                gray            = pr.gray(x, y);
                if (gray>=lowGray && gray<=highGray && right<x) {
                    right       = x;
                    search      = false;
                    break;
                }
            }
        }
        if (right<=0)           right = 0;
    }
    // Start from the left, and find the most top-matching pixel.
    qint32              top     = 0;
    if (_fromTop) {
        quint8          gray;
        bool            search  = true;
        qint32          rOffset;
        top                     = pr.height-1;
        for (qint32 y=0; search && y<pr.height; ++y) {
            rOffset             = pr.valueOffset(y);
            for (qint32 x=0; x<pr.width-_step; x+=_step) {
                gray            = pr.gray(x+rOffset);
                if (gray>=lowGray && gray<=highGray && top>y) {
                    top         = y;
                    search      = false;
                    break;
                }
            }
        }
        if (top<=0)             top = 0;
    }
    // Start from the right, and find the most bottom-matching pixel.
    qint32              bottom  = pr.height-1;
    if (_fromBottom) {
        quint8          gray;
        bool            search  = true;
        qint32          rOffset;
        bottom                  = 0;
        for (qint32 y=pr.height-1; search && y>=0; --y) {
            rOffset             = pr.valueOffset(y);
            for (qint32 x=0; x<pr.width-_step; x+=_step) {
                gray            = pr.gray(x+rOffset);
                if (gray>=lowGray && gray<=highGray && bottom<y) {
                    bottom      = y;
                    search      = false;
                    break;
                }
            }
        }
        if (bottom>=pr.height-1) bottom = pr.height-1;
    }
    _boundaries                 = QRect(left, top, right-left, bottom-top);
    return true;
}
