#include "BasicHistogram.h"
#include "../PixelHelper.hpp"

RE::BasicHistogram::BasicHistogram (QObject *p)
: RE::AbstractAnalyzer(p) {
}

bool RE::BasicHistogram::doRun () {
    QImage::Format  fmt     = source()->format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      false;
    }
}
template<typename K>
bool RE::BasicHistogram::algo () {
    // Reset.
    _data.clear					();
    _data.resize				(256);
    quint32     *data           = _data.data();
    memset(data, 0, sizeof(quint32)*256);
    // Loop!
    qint16 const &z             = plane();
    PixelReader<K> const pr     (source());
    qint32      rOffset;
    for (qint32 y=0; y<pr.height; ++y) {
        rOffset                 = pr.byteOffset(y, z);
        for (qint32 x=0; x<pr.width; ++x)
            data[pr.byte(x*pr.bpc+rOffset)]++;
    }
    return                      true;
}
