#ifndef RE_CumulativeHistogram_h
#define RE_CumulativeHistogram_h

// Base class.
#include "BasicHistogram.h"
// Required stuff.
#include "../RETypes.h"
#include <QtCore/QList>

namespace RE {

/*! @brief Cumulative Histogram Analyzer Class.
 * This class computes cumulative histogram for any plane of a given image.
 * As it will honor the plane property, it's important to understand that computing an histogram on a multi-component
 * image for a specific plane would ignore the other plane(s).
 * Once computed, the result (RE::T::UIntVector) will be a 256-element array, containing cummulative occurences of each color per index.
 * This Analyzer internally uses the RE::BasicHistogram to first get an histogram on the requested plane, and then iterate through each
 * element for building the cumulative data.
 * @see RE::BasicHistogram */
class RELibOpt CumulativeHistogram : public BasicHistogram {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractAnalyzer)

public:
    //! @see RE::AbstractAnalyzer::AbstractAnalyzer()
    Q_INVOKABLE					CumulativeHistogram		(QObject *p=0);

    //! @brief Results Accessor.
    T::UIntVector const&		cumulativeData			()	const	{ return _cumulativeData; }

protected:
    //! @see RE::AbstractAnalyzer::doRun()
    virtual bool				doRun					();

private:
    T::UIntVector				_cumulativeData;

};

}

#endif
