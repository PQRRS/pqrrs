#include "DummyRecognizer.h"

RE::DummyRecognizer::DummyRecognizer (QObject *p)
: AbstractRecognizer(p) {}
RE::DummyRecognizer::~DummyRecognizer () {}

bool RE::DummyRecognizer::doRun () {
    return true;
}
