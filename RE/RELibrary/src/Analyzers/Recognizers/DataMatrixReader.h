#ifndef RE_DataMatrixReader_h
#define RE_DataMatrixReader_h

// Base class.
#include "AbstractRecognizer.h"
// Required stuff.
#include "../../RETypes.h"
#include <QtCore/QString>
#include <QtGui/QImage>

namespace RE {

class RELibOpt DataMatrixReader : public AbstractRecognizer {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractRecognizer)
public:
    // Constructors / Destructor.
    Q_INVOKABLE						DataMatrixReader        (QObject *p=0);
    virtual							~DataMatrixReader       ();

protected:
    // Main reading function.
    virtual bool					doRun					();
};

}

#endif
