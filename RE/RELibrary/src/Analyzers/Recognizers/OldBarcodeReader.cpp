#include "OldBarcodeReader.h"
#include <QtCore/QDebug>
#include <QtGui/QColor>

// Private types definitions.
struct BarInfo {
	unsigned char		color;
	unsigned int		pixelSize;
} ;
typedef QList<BarInfo> Bars;

// Constructor.
RE::OldBarcodeReader::OldBarcodeReader (QObject *p)
: RE::AbstractRecognizer(p), _readingDirection(RE::E::ReadingDirectionLeftToRight), _expectedType(RE::E::BarcodeStandardUnknown) {
}

// Reads a barcode picture given a rect and a reading direction.
bool RE::OldBarcodeReader::doRun () {
	//setSource	(BlackAndWhiteEqualizer::Apply(127, source()));
	clearResult	("Barcode");
	// Try to extract bars.
	if (!extractBars())
		return false;
	// If no expected type, try to guess.
	if (_expectedType==RE::E::BarcodeStandardUnknown)
		_guessedType	= guessType(_extractedBars);
	// Make sure there is a possible guessed type, and make sure it matches the expected one if any.
	if (_guessedType==RE::E::BarcodeStandardUnknown || (_expectedType!=RE::E::BarcodeStandardUnknown && _expectedType!=_guessedType))
		return false;
	// Try to decode.
	bool toRet	= decode(_guessedType);
	if (toRet)
		setResult("Barcode", _decodedData.toLatin1());
	return toRet;
}

bool RE::OldBarcodeReader::extractBars () {
	QImage const	&src					= source();
	quint8 const	*srcBits				= src.bits();
	quint32 const	srcBpl					= src.bytesPerLine();

	// Barcode reads from right to left or from top to bottom.
	bool			reverse					= _readingDirection==RE::E::ReadingDirectionRightToLeft || _readingDirection==RE::E::ReadingDirectionTopToBottom;
	int				agroPasses				= (agressivity()+1)*2;
	if ((_readingDirection==RE::E::ReadingDirectionLeftToRight || _readingDirection==RE::E::ReadingDirectionRightToLeft) && agroPasses>=src.width())
		agroPasses	= src.width()-2;
	else if ((_readingDirection==RE::E::ReadingDirectionBottomToTop || _readingDirection==RE::E::ReadingDirectionTopToBottom) && agroPasses>=src.height())
		agroPasses	= src.height()-2;
	
	// Check input image format.
	if (src.format()!=QImage::Format_Indexed8) {
		Wrn << "Bad image format.";
		return false;
	}

	// Reset internal data.
	_guessedType							= RE::E::BarcodeStandardUnknown;
	_extractedBars.clear					();
	_decodedData.clear						();

	// Prepare increment variables.
	quint32		x, y;
	quint32		*incPtr, *fixPtr, incLimit;
	double		incPass;
	// Horizontal scanning requested.
	if (_readingDirection==RE::E::ReadingDirectionLeftToRight || _readingDirection==RE::E::ReadingDirectionRightToLeft) {
		// Increment will be on x, static on y.
		incPtr		= &x;
		fixPtr		= &y;
		// y will stay at middle height of image, x increment limit is image width.
		incLimit	= src.width();
		incPass		= src.height()/(agroPasses+1);
		y			= incPass;
	}
	// Vertical scanning detected.
	else {
		// Increment will be on y, static on x.
		incPtr		= &y;
		fixPtr		= &x;
		// x will stay at middle width of image, y increment limit is image height.
		incLimit	= src.height()-1;
		incPass		= (double)src.width()/(double)(agroPasses+2);
		x			= incPass;
	}
	
	// Those two are used to guess the sizes of wide / narrow bars.
	quint32				narrowest, widest;
	// This is the loop current pixel's color.
	quint8				color;
	// This is the bar being iterated over.
	BarInfo				currentBar;
	// Variable containing one iteration bars.
	Bars				bars;
	QString				result;
	// Execute for each pass.
	for (int curPass=0; curPass<agroPasses; ++curPass) {
		// Reset everything
		narrowest							= UINT_MAX;
		widest								= 0;
		result.clear						();
		bars.clear							();
		currentBar.color					= 255;
		currentBar.pixelSize				= 0;
		
		// Increment fixed pointer.
		(*fixPtr)	= incPass*(curPass+1);
		// Increment x if horizontal, y if vertical.
		for ((*incPtr)=0; (*incPtr)<incLimit; ++(*incPtr)) {
			// Store pixel color.
			color	= srcBits[x+y*srcBpl];
			// Color is different than current one.
			if (color!=currentBar.color) {
				// Barcode has started.
				if (currentBar.pixelSize>0) {
					// Append the current bar to the results list.
					if (!bars.isEmpty() || currentBar.color==0) {
						// Largest bar seen so far?
						if (currentBar.pixelSize>widest)
							widest			= currentBar.pixelSize;
						// Smallest bar seen so far?
						else if (currentBar.pixelSize<narrowest)
							narrowest		= currentBar.pixelSize;
						// Depending on reading direction, add bar to list.
						if (reverse)		bars.insert(0, currentBar);
						else				bars.append(currentBar);
					}
					
					// Prepare a new bar matching the new color.
					currentBar.color		= color;
					currentBar.pixelSize	= 0;
				}
			}
			// Increment current bar's size.
			currentBar.pixelSize++;
		}
		
		// Narrowest bars is too close of widest bars.
		if (narrowest>=widest)
			continue;
		
		// Now, based on the total processed width, try to deduce bars relative sizes.
		unsigned int	maxNarrow	= narrowest + ((widest-narrowest)/2);
		BarInfo			bar;
		for (Bars::const_iterator i=bars.constBegin(); i!=bars.constEnd(); ++i) {
			bar	= *i;
			if (bar.pixelSize<=maxNarrow)	result	+= bar.color==0?"b":"w";
			else							result	+= bar.color==0?"B":"W";
		}
		
		// Read type is of expected type, just break whatever the agressivity is.
		if (_expectedType!=RE::E::BarcodeStandardUnknown && isOfType(result, _expectedType)) {
			_guessedType	= _expectedType;
			_extractedBars	= result;
			return true;
		}
	}
	// Reading failed.
	return false;
}

// Guess the barcode type based on it's content.
RE::E::BarcodeStandard RE::OldBarcodeReader::guessType (QString const &rawBars) {
	if	(isOfType(rawBars, RE::E::BarcodeStandardInterleaved5of2))	return RE::E::BarcodeStandardInterleaved5of2;
	else if	(isOfType(rawBars, RE::E::BarcodeStandardCode39))		return RE::E::BarcodeStandardCode39;
	else															return RE::E::BarcodeStandardUnknown;
}

#define thinBlackCount	bc[0]
#define	wideBlackCount	bc[1]
#define thinWhiteCount	bc[2]
#define wideWhiteCount	bc[3]
bool RE::OldBarcodeReader::isOfType (QString const &rawBars, RE::E::BarcodeStandard type) {
	// Prepare some buffer.
	unsigned int	bc[4];
	memset(bc, 0, sizeof(int)*4);
	
	// Based on requested type, run tests.
	switch (type) {
	case RE::E::BarcodeStandardInterleaved5of2:
		// Count bars excluding delimiters;
		thinBlackCount	= rawBars.count('b')-3;
		wideBlackCount	= rawBars.count('B')-1;
		thinWhiteCount	= rawBars.count('w')-3;
		wideWhiteCount	= rawBars.count('W');
		// Check delimiters.
		if (rawBars.startsWith("bwbw") &&	rawBars.endsWith("Bwb") &&
				// Check that there is the same number of black and white bars.
				thinBlackCount==thinWhiteCount && wideBlackCount==wideWhiteCount &&
				// Check black and white counts.
				(thinBlackCount+wideBlackCount)%5==0 && thinBlackCount%3==0 && wideBlackCount%2==0)
			return true;
		break;

	case RE::E::BarcodeStandardCode39:
		// Count bars excluding delimiters.
		thinBlackCount	= rawBars.count('b')-3;
		wideBlackCount	= rawBars.count('B')-2;
		thinWhiteCount	= rawBars.count('w')-3;
		wideWhiteCount	= rawBars.count('W')-1;
		// Check delimiters and counts.
		if (rawBars.startsWith("bWbwBwBwb") && rawBars.endsWith("bWbwBwBwb") &&
				// Check black and white counts (5 black and 4 white per char, + 1 white delimiter in between).
				(thinBlackCount+wideBlackCount)%5==0 && (thinWhiteCount+wideWhiteCount)%5==0)
			return true;
		break;

	case RE::E::BarcodeStandardUnknown:
		break;
	};
	// Type doesn't match.
	return false;
}

// Decodes a barcode, optionnally forcing a type to use.
bool RE::OldBarcodeReader::decode (RE::E::BarcodeStandard type) {
	// Try to get a type of the current barcode.
	QChar	c;
	bool	decodingFailed		= false;
	switch (type) {
		case RE::E::BarcodeStandardInterleaved5of2: {
			// Exclude delimiters.
			QString		blackBars	= _extractedBars.mid(4, _extractedBars.length()-7);
			QString		whiteBars	= blackBars;
			blackBars.replace('w', "", Qt::CaseInsensitive);
			whiteBars.replace('b', "", Qt::CaseInsensitive);
			// Loop for each group of 5 bars.
			for (quint32 idx=0; idx<(quint32)blackBars.length(); idx+=5) {
				c	= digitDecipher_i5o2(blackBars.midRef(idx, 5));
				if (c.unicode())	_decodedData += c;
				else				return false;
				c	= digitDecipher_i5o2(whiteBars.midRef(idx, 5));
				if (c.unicode())	_decodedData += c;
				else				return false;
			}
			break;
		};
		case RE::E::BarcodeStandardCode39:
			for (quint32 idx=0; idx<(quint32)_extractedBars.length(); idx+=10) {
				c	= digitDecipher_c39(_extractedBars.midRef(idx, 9));
				if (c.unicode())	_decodedData += c;
				else				return false;
			}
			break;
		case RE::E::BarcodeStandardUnknown:
		default:
			return false;
	};
	
	return true;
}

QChar RE::OldBarcodeReader::digitDecipher_i5o2 (QStringRef const &block) {
	static QHash<QString, QChar>	table;
	if (table.isEmpty()) {
		table.reserve(10);
		table["nnWWn"] = '0'; table["WnnnW"] = '1';
		table["nWnnW"] = '2'; table["WWnnn"] = '3'; 
		table["nnWnW"] = '4'; table["WnWnn"] = '5';
		table["nWWnn"] = '6'; table["nnnWW"] = '7'; 
		table["WnnWn"] = '8'; table["nWnWn"] = '9';
	}
	QString	szDigit	= block.toString().replace('b', 'n').replace('w', 'n').replace('B', 'W');
	return table.contains(szDigit) ? table[szDigit] : 0;
}

QChar RE::OldBarcodeReader::digitDecipher_c39 (QStringRef const &block) {
	static QHash<QString, QChar>	table;
	if (table.isEmpty()) {
		table.reserve(44);
		table["bwbWBwBwb"] = '0'; table["BwbWbwbwB"] = '1'; table["bwBWbwbwB"] = '2'; table["BwBWbwbwb"] = '3';
		table["bwbWBwbwB"] = '4'; table["BwbWBwbwb"] = '5'; table["bwBWBwbwb"] = '6'; table["bwbWbwBwB"] = '7';
		table["BwbWbwBwb"] = '8'; table["bwBWbwBwb"] = '9'; table["BwbwbWbwB"] = 'A'; table["bwBwbWbwB"] = 'B';
		table["BwBwbWbwb"] = 'C'; table["bwbwBWbwB"] = 'D'; table["BwbwBWbwb"] = 'E'; table["bwBwBWbwb"] = 'F';
		table["bwbwbWBwB"] = 'G'; table["BwbwbWBwb"] = 'H'; table["bwBwbWBwb"] = 'I'; table["bwbwBWBwb"] = 'J';
		table["BwbwbwbWB"] = 'K'; table["bwBwbwbWB"] = 'L'; table["BwBwbwbWb"] = 'M'; table["bwbwBwbWB"] = 'N';
		table["BwbwBwbWb"] = 'O'; table["bwBwBwbWb"] = 'P'; table["bwbwbwBWB"] = 'Q'; table["BwbwbwBWb"] = 'R';
		table["bwBwbwBWb"] = 'S'; table["bwbwBwBWb"] = 'T'; table["BWbwbwbwB"] = 'U'; table["bWBwbwbwB"] = 'V';
		table["BWBwbwbwb"] = 'W'; table["bWbwBwbwB"] = 'X'; table["BWbwBwbwb"] = 'Y'; table["bWBwBwbwb"] = 'Z';
		table["bWbwbwBwB"] = '-'; table["BWbwbwBwb"] = '.'; table["bWBwbwBwb"] = ' '; table["bWbWbWbwb"] = '$';
		table["bWbWbwbWb"] = '/'; table["bWbwbWbWb"] = '+'; table["bwbWbWbWb"] = '%'; table["bWbwBwBwb"] = '*';
	}
	QString	szDigit	= block.toString();
	return table.contains(szDigit) ? table[szDigit] : 0;
}
