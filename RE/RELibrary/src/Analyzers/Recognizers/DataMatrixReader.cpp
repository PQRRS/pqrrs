#include "DataMatrixReader.h"
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtGui/QColor>
#include <dmtx.h>

// Constructor.
RE::DataMatrixReader::DataMatrixReader (QObject *p)
: RE::AbstractRecognizer(p) {
}
RE::DataMatrixReader::~DataMatrixReader () {
}

// Reads a barcode picture given a rect and a reading direction.
bool RE::DataMatrixReader::doRun () {
    // Prepare source image pointer.
    QImage          cpImg               (*source());
    // Create LibDMTX image.
    DmtxImage       *dmtxImg            = 0;
    switch (cpImg.format()) {
    case QImage::Format_Indexed8:
        dmtxImg                         = dmtxImageCreate(cpImg.bits(), cpImg.width(), cpImg.height(), DmtxPack8bppK);
        break;
    case QImage::Format_RGB16:
        dmtxImg                         = dmtxImageCreate(cpImg.bits(), cpImg.width(), cpImg.height(), DmtxPack16bppRGB);
        break;
    case QImage::Format_RGB32:
        dmtxImg                         = dmtxImageCreate(cpImg.bits(), cpImg.width(), cpImg.height(), DmtxPack32bppRGBX);
        break;
    case QImage::Format_ARGB32:
        dmtxImg                         = dmtxImageCreate(cpImg.bits(), cpImg.width(), cpImg.height(), DmtxPack32bppXRGB);
        break;
    default:
        Wrn                             << "Unhandled image format:" << cpImg.format() << ". Please fix DataMatrixReader.cpp.";
        return                          false;
        break;
    }
    // Check image conversion.
    if (!dmtxImg) {
        Wrn                             << "Unable to create exchange format image.";
        return                          false;
    }
    dmtxImageSetProp                    (dmtxImg, DmtxPropRowSizeBytes, cpImg.bytesPerLine());
    // Instanciate decoder.
    DmtxDecode      *dmtxDec            = dmtxDecodeCreate(dmtxImg, 1);
    if (!dmtxImg) {
        dmtxImageDestroy                (&dmtxImg);
        Wrn                             << "Unable to create decoder.";
        return false;
    }
    // Loop for each found region.
    //while (true) {
        DmtxRegion  *dmtxReg            = dmtxRegionFindNext(dmtxDec, NULL);
        // No region found, just get out of the loop.
        if (!dmtxReg)                   return false; //break;
        // Read message.
        DmtxMessage *dmtxMsg            = dmtxDecodeMatrixRegion(dmtxDec, dmtxReg, DmtxUndefined);
        if (dmtxMsg) {
            setResult                   ("Result", QByteArray((const char*)dmtxMsg->output, dmtxMsg->outputSize));
            dmtxMessageDestroy          (&dmtxMsg);
        }
        dmtxRegionDestroy               (&dmtxReg);
    //}
    // Get rid of decoder.
    dmtxDecodeDestroy                   (&dmtxDec);
    // Get rid of image.
    dmtxImageDestroy                    (&dmtxImg);
    return								true;
}
