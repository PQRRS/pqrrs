#ifndef RE_DummyRecognizer_h
#define RE_DummyRecognizer_h

// Base class.
#include "AbstractRecognizer.h"
// Required stuff.
#include "../../RETypes.h"
#include <QtCore/QString>
#include <QtGui/QImage>

namespace RE {

class RELibOpt DummyRecognizer : public AbstractRecognizer {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractRecognizer)

public:
    // Constructor.
    Q_INVOKABLE                     DummyRecognizer             (QObject *p=0);
    virtual                         ~DummyRecognizer            ();

protected:
    // Main reading function.
    virtual bool                    doRun						();
};

}

#endif
