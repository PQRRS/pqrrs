#ifndef RE_RawOpticalCharacterRecognizer_h
#define RE_RawOpticalCharacterRecognizer_h

// Base class.
#include "AbstractRecognizer.h"
// Required stuff.
#include "../../RETypes.h"
#include <QtCore/QString>
#include <QtGui/QImage>

namespace tesseract {
    class TessBaseAPI;
    class ResultIterator;
}

namespace RE {

class RELibOpt RawOpticalCharacterRecognizer : public AbstractRecognizer {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractRecognizer)
    Q_PROPERTY(QString					whiteList           READ whiteList              WRITE setWhiteList          RESET resetWhiteList)
    Q_PROPERTY(QString					blackList           READ blackList              WRITE setBlackList          RESET resetBlackList)
    Q_PROPERTY(bool                     enableDictionary    READ enableDictionary       WRITE setEnableDictionary   RESET resetEnableDictionary)
    Q_PROPERTY(bool                     alwaysResetBlobs    READ alwaysResetBlobs       WRITE setAlwaysResetBlobs   RESET resetAlwaysResetBlobs)
    Q_PROPERTY(QString                  predicate           READ predicate              WRITE setPredicate          RESET resetPredicate)
    // Iteration level props.
    Q_PROPERTY(bool                     oldRawOutput            READ oldRawOutput           WRITE setOldRawOutput           RESET resetOldRawOutput)
    Q_PROPERTY(bool                     iterateBlocks           READ iterateBlocks          WRITE setIterateBlocks          RESET resetIterateBlocks)
    Q_PROPERTY(bool                     iterateParagraphs       READ iterateParagraphs      WRITE setIterateParagraphs      RESET resetIterateParagraphs)
    Q_PROPERTY(bool                     iterateWords            READ iterateWords           WRITE setIterateWords           RESET resetIterateWords)
    Q_PROPERTY(bool                     iterateSymbols          READ iterateSymbols         WRITE setIterateSymbols         RESET resetIterateSymbols)
    Q_PROPERTY(bool                     includeSymbolChoices    READ includeSymbolChoices   WRITE setIncludeSymbolChoices   RESET resetIncludeSymbolChoices)

public:
    // Constructor.
    Q_INVOKABLE                     RawOpticalCharacterRecognizer      (QObject *p=0);
    virtual                         ~RawOpticalCharacterRecognizer     ();

    // Accessors.
    QString const&              	whiteList					() const						{ return _whiteList; }
    void                        	setWhiteList				(QString const &v)				{ _whiteList = v; }
    void                        	resetWhiteList				()								{ setWhiteList(""); }
    QString const&              	blackList					() const						{ return _blackList; }
    void                        	setBlackList				(QString const &v)				{ _blackList = v; }
    void                            resetBlackList				()								{ setBlackList(""); }
    bool const&                     enableDictionary            () const                        { return _enableDictionary; }
    void                            setEnableDictionary         (bool const &v)                 { _enableDictionary = v; }
    void                            resetEnableDictionary       ()                              { setEnableDictionary(true); }
    bool const&                     alwaysResetBlobs            () const                        { return _alwaysResetBlobs; }
    void                            setAlwaysResetBlobs         (bool const &v)                 { _alwaysResetBlobs = v; }
    void                            resetAlwaysResetBlobs       ()                              { setAlwaysResetBlobs(false); }
    QString const&                  predicate                   () const                        { return _predicate; }
    void                            setPredicate                (QString const &v)              { _predicate = v; }
    void                            resetPredicate              ()                              { setPredicate("eng"); }
    // Iteration level props.
    bool const&                     oldRawOutput                () const                        { return _oldRawOutput; }
    void                            setOldRawOutput             (bool const &v)                 { _oldRawOutput = v; }
    void                            resetOldRawOutput           ()                              { _oldRawOutput = false; }
    bool const&                     iterateBlocks               () const                        { return _iterateBlocks; }
    void                            setIterateBlocks            (bool const &v)                 { _iterateBlocks = v; }
    void                            resetIterateBlocks          ()                              { _iterateBlocks = false; }
    bool const&                     iterateParagraphs           () const                        { return _iterateParagraphs; }
    void                            setIterateParagraphs        (bool const &v)                 { _iterateParagraphs = v; }
    void                            resetIterateParagraphs      ()                              { _iterateParagraphs = false; }
    bool const&                     iterateWords                () const                        { return _iterateWords; }
    void                            setIterateWords             (bool const &v)                 { _iterateWords = v; }
    void                            resetIterateWords           ()                              { _iterateWords = false; }
    bool const&                     iterateSymbols              () const                        { return _iterateSymbols; }
    void                            setIterateSymbols           (bool const &v)                 { _iterateSymbols = v; }
    void                            resetIterateSymbols         ()                              { _iterateSymbols = false; }
    bool const&                     includeSymbolChoices        () const                        { return _includeSymbolChoices; }
    void                            setIncludeSymbolChoices     (bool const &v)                 { _includeSymbolChoices = v; }
    void                            resetIncludeSymbolChoices   ()                              { _includeSymbolChoices = false; }


protected:
    // Main reading function.
    virtual bool                    doRun						();
    void                            runIteration                (tesseract::ResultIterator *it, quint32 level, QString const &name);
    void                            destroyApi                  ();

private:
    tesseract::TessBaseAPI          *_tessApi;
    bool                            _updateVariables;
    QString                         _whiteList;
    QString                         _blackList;
    bool                            _enableDictionary;
    bool                            _alwaysResetBlobs;
    QString                         _lastInitLanguage;
    QString                         _predicate;
    bool                            _cleanInit;
    bool                            _oldRawOutput,
                                    _iterateBlocks,
                                    _iterateParagraphs,
                                    _iterateWords,
                                    _iterateSymbols,
                                    _includeSymbolChoices;
};

}

#endif
