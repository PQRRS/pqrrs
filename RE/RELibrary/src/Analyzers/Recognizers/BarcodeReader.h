#ifndef RE_BarcodeReader_h
#define RE_BarcodeReader_h

// Base class.
#include "AbstractRecognizer.h"
// Required stuff.
#include "../../RETypes.h"
#include <QtCore/QString>
#include <QtGui/QImage>

// Forward decls.
namespace zbar { class ImageScanner; }

namespace RE {

class RELibOpt BarcodeReader : public AbstractRecognizer {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractRecognizer)
    Q_PROPERTY(RE::E::Agressivity		agressivity			READ agressivity		WRITE setAgressivity		RESET resetAgressivity)
    Q_PROPERTY(RE::E::ReadingDirection	readingDirection	READ readingDirection	WRITE setReadingDirection	RESET resetReadingDirection)
    Q_PROPERTY(RE::E::BarcodeStandard	expectedType		READ expectedType		WRITE setExpectedType		RESET resetExpectedType)
    Q_PROPERTY(quint32                  expectedLength		READ expectedLength		WRITE setExpectedLength		RESET resetExpectedLength)

public:
    // Constructors / Destructor.
    Q_INVOKABLE						BarcodeReader			(QObject *p=0);
    virtual							~BarcodeReader			();
    // Accessors.
    RE::E::Agressivity const&		agressivity				() const						{ return _agressivity; }
    void							setAgressivity			(RE::E::Agressivity const& v)	{ _agressivity = v; }
    void							resetAgressivity		()								{ _agressivity = E::AgressivityNormal; }
    E::ReadingDirection const&		readingDirection		() const						{ return _readingDirection; }
    void							setReadingDirection		(E::ReadingDirection const &v)	{ _readingDirection = v; }
    void							resetReadingDirection	()								{ _readingDirection = E::ReadingDirectionLeftToRight; }
    E::BarcodeStandard const&		expectedType			() const						{ return _expectedType; }
    void							setExpectedType			(E::BarcodeStandard const &v)	{ _expectedType = v; }
    void							resetExpectedType		()								{ _expectedType = E::BarcodeStandardUnknown; }
    quint32 const&                  expectedLength			() const						{ return _expectedLength; }
    void							setExpectedLength		(quint32 const &v)              { _expectedLength = v; }
    void							resetExpectedLength		()								{ _expectedLength = 0; }

protected:
    // Main reading function.
    virtual bool					doRun					();

private:
    // Internal scanner.
    zbar::ImageScanner				*_imageScanner;
    // Private members.
    RE::E::Agressivity				_agressivity;
    E::ReadingDirection				_readingDirection;
    E::BarcodeStandard				_expectedType;
    quint32                         _expectedLength;
};

}

#endif
