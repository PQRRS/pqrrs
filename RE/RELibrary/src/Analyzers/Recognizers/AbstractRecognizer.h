#ifndef RE_AbstractRecognizer_h
#define RE_AbstractRecognizer_h

// Base class.
#include "../AbstractAnalyzer.h"
// Required stuff.
#include "../../RETypes.h"
#include <QtCore/QStringList>

namespace RE {

/*! @brief Recognition Base Interface Class.
 * This class exposes a generic and standardized results property holder.
 */
class RELibOpt AbstractRecognizer : public AbstractAnalyzer {
public:
    Q_OBJECT
    //! @brief The Unique Identifier for this Instance Property (Usually assigned via the RELibraryAdmin Tool).
    Q_PROPERTY(QString	name                    READ name           WRITE setName           RESET resetName)
    //! @brief Wether or not to keep a hashed checksum of image bits after each filter.
    Q_PROPERTY(bool     makeChecksums           READ makeChecksums  WRITE setMakeChecksums  RESET resetMakeChecksums)
    //! @brief The Results Generic Standardized Container Property.
    Q_PROPERTY(RE::T::StringToVariantHash
                        results                 READ results)

protected:
    //! @brief Default Constructor.
    Q_INVOKABLE             AbstractRecognizer          (QObject *p=0) : AbstractAnalyzer(p)    {
        resetName           ();
        resetMakeChecksums  ();
    }

public:
    // Accessors.
    QString const&			name                        () const								{ return _name; }
    void					setName                     (QString const &v)						{ _name = v; }
    void					resetName                   ()										{ _name = ""; }
    bool const&             makeChecksums               () const                                { return _makeChecksums; }
    void                    setMakeChecksums            (bool const &v)                         { _makeChecksums = v; }
    void                    resetMakeChecksums          ()                                      { _makeChecksums = false; }
    RE::T::StringToVariantHash const&
                            results                     () const								{ return _results; }
    virtual bool            run                         ();

protected:
    void					setResult                   (QString const &n, QVariant const &v)	{ _results[n] = v; }
    //! @see RE::AbstractAnalyzer::doRun()
    virtual bool			doRun                       () = 0;

private:
    QString					_name;
    bool                    _makeChecksums;
    RE::T::StringToVariantHash
                            _results;
};

}

Q_DECLARE_INTERFACE(RE::AbstractRecognizer, "fr.PierreQR.RecognitionSuite.RE.AbstractRecognizer/1.0")

#endif
