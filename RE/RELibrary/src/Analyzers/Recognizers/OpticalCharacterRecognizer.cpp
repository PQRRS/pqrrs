#include "OpticalCharacterRecognizer.h"
#include "../../Engine.h"
// Tesseract required stuff.
#include <QtCore/QCoreApplication>
#include <tesseract/baseapi.h>

RE::OpticalCharacterRecognizer::OpticalCharacterRecognizer (QObject *p)
: AbstractRecognizer(p), _tessApi(0), _enableDictionary(true), _alwaysResetBlobs(false) {
}
RE::OpticalCharacterRecognizer::~OpticalCharacterRecognizer () {
    destroyApi                          ();
}
void RE::OpticalCharacterRecognizer::destroyApi () {
    if (_tessApi) {
        _tessApi->End                       ();
        #ifdef Q_OS_WIN
        Wrn                                 << "Skipping deletion.";
        #else
        delete                              _tessApi;
        #endif
        _tessApi                            = 0;
    }
}

bool RE::OpticalCharacterRecognizer::doRun () {
    // No predicate?! Use default.
    if (_predicate.isEmpty())
        _predicate                          = "eng";
    // New predicate requested.
    if (_lastInitLanguage!=_predicate) {
        if (!_tessApi)
            _tessApi                        = new tesseract::TessBaseAPI();
        else
            _tessApi->End                   ();
        // Finally load language.
        Wrn                                 << "Loading predicate" << _predicate << "...";
        _lastInitLanguage					= _predicate;
        _tessApi->SetVariable               ("enable_new_segsearch",                "1");
        //_tessApi->SetVariable               ("tessedit_enable_doc_dict",            _enableDictionary ? "1" : "0");
        //_tessApi->SetVariable               ("classify_enable_learning",            _alwaysResetBlobs ? "0" : "1");
        //_tessApi->SetVariable               ("classify_enable_adaptive_matcher",    _alwaysResetBlobs ? "0" : "1");
        if (_tessApi->Init("./", _predicate.toUtf8().constData(), tesseract::OEM_DEFAULT))
            destroyApi                      ();
    }
    // Recognition cannot start.
    if (!_tessApi) {
        Crt                                 << "Unable to initialize recognizer. Aborting recognition.";
        return false;
    }
    // Clear API and reset classifier.
    _tessApi->Clear                         ();
    //if (_alwaysResetBlobs)
    //    _tessApi->ClearAdaptiveClassifier   ();
    // Set up variables.
    _tessApi->SetVariable                   ("tessedit_char_whitelist",             _whiteList.toUtf8().constData());
    _tessApi->SetVariable                   ("tessedit_char_blacklist",             _blackList.toUtf8().constData());
    _tessApi->SetVariable                   ("enable_new_segsearch",                "1");
    //_tessApi->SetVariable                   ("tessedit_enable_doc_dict",            _enableDictionary ? "1" : "0");
    //_tessApi->SetVariable                   ("classify_enable_learning",            _alwaysResetBlobs ? "0" : "1");
    //_tessApi->SetVariable                   ("classify_enable_adaptive_matcher",    _alwaysResetBlobs ? "0" : "1");

    //_tessApi->SetVariable                   ("tessedit_debug_fonts",                "1");
    //_tessApi->SetVariable                   ("classify_enable_adaptive_debugger",   "1");
    //_tessApi->SetVariable                   ("classify_save_adapted_templates",     "1");
    //_tessApi->SetVariable                   ("classify_use_pre_adapted_templates",  "1");

    // Change page segmentation.
    _tessApi->SetPageSegMode                ((tesseract::PageSegMode)_segmentation);

    // Processing takes place here.
    QImage  src;
    if (source()->format()==QImage::Format_Indexed8) {
        src                                 = RE::Engine::paletizeIfGrayscale(*source(), true);
        _tessApi->SetImage                  (src.bits(), src.width(), src.height(), src.depth()/8, src.bytesPerLine());
    }
    else {
        src                                 = source()->convertToFormat(QImage::Format_RGB32).rgbSwapped();
        _tessApi->SetImage                  (src.bits(), src.width(), src.height(), src.depth()/8, src.bytesPerLine());
    }
    // Grab result.
    char			*text					= _tessApi->GetUTF8Text();
    // Free tesseract API internals.
    _tessApi->Clear                         ();
    // Grab a QString on the results.
    QString			r						(text);
    // Delete results buffer.
    #ifndef Q_OS_WIN
    delete[]                                text;
    #endif
    // Remove any end of line chars if unnecessary.
    if (_segmentation>=RE::E::PageSegmentationSingleLine)
        r									= r.replace(QRegExp("\r|\n"), "");
    setResult								("Raw", r);

    return true;
}
