#ifndef OldBarcodeReader_h
#define OldBarcodeReader_h

// Base class.
#include "AbstractRecognizer.h"
// Required stuff.
#include "../../RETypes.h"
#include <QtCore/QString>
#include <QtGui/QImage>

namespace RE {

class RELibOpt OldBarcodeReader : public AbstractRecognizer {
Q_OBJECT;
Q_INTERFACES(RE::AbstractRecognizer);
Q_PROPERTY(RE::E::Agressivity		agressivity			READ agressivity		WRITE setAgressivity		RESET resetAgressivity);
Q_PROPERTY(RE::E::ReadingDirection	readingDirection	READ readingDirection	WRITE setReadingDirection	RESET resetReadingDirection);
Q_PROPERTY(RE::E::BarcodeStandard	expectedType		READ expectedType		WRITE setExpectedType		RESET resetExpectedType);
Q_PROPERTY(QString					extractedBars		READ extractedBars);

public:
	// Constructor.
	Q_INVOKABLE						OldBarcodeReader			(QObject *p=0);
	// Accessors.
	RE::E::Agressivity const&		agressivity				() const						{ return _agressivity; }
	void							setAgressivity			(RE::E::Agressivity const& v)	{ _agressivity = v; }
	void							resetAgressivity		()								{ _agressivity = E::AgressivityNormal; }
	E::ReadingDirection const&		readingDirection		() const						{ return _readingDirection; }
	void							setReadingDirection		(E::ReadingDirection const &v)	{ _readingDirection = v; }
	void							resetReadingDirection	()								{ _readingDirection = E::ReadingDirectionUnknown; }
	E::BarcodeStandard const&		expectedType			() const						{ return _expectedType; }
	void							setExpectedType			(E::BarcodeStandard const &v)	{ _expectedType = v; }
	void							resetExpectedType		()								{ _expectedType = E::BarcodeStandardUnknown; }
	QString const&					extractedBars			() const						{ return _extractedBars; }
	QString const&					decodedData				() const						{ return _decodedData; }

protected:
	// Main reading function.
	virtual bool					doRun					();
	// Various methods.
	bool							extractBars				();
	bool							decode					(E::BarcodeStandard type);
	// Type checking.
	static bool						isOfType				(QString const &rawBars, E::BarcodeStandard type);
	static E::BarcodeStandard		guessType				(QString const &rawBars);

private:
	// Decyphering functions.
	static QChar					digitDecipher_i5o2		(QStringRef const &block);
	static QChar					digitDecipher_c39		(QStringRef const &block);
	
	// Private members.
	RE::E::Agressivity				_agressivity;
	E::ReadingDirection				_readingDirection;
	E::BarcodeStandard				_expectedType;
	QString							_extractedBars;
	E::BarcodeStandard				_guessedType;
	QString							_decodedData;
};

}

#endif
