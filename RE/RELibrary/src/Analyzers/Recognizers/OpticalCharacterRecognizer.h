#ifndef RE_OpticalCharacterRecognizer_h
#define RE_OpticalCharacterRecognizer_h

// Base class.
#include "AbstractRecognizer.h"
// Required stuff.
#include "../../RETypes.h"
#include <QtCore/QString>
#include <QtGui/QImage>
namespace tesseract {
    class TessBaseAPI;
}

namespace RE {

class RELibOpt OpticalCharacterRecognizer : public AbstractRecognizer {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractRecognizer)
    Q_PROPERTY(QString					whiteList           READ whiteList          WRITE setWhiteList          RESET resetWhiteList)
    Q_PROPERTY(QString					blackList           READ blackList          WRITE setBlackList          RESET resetBlackList)
    Q_PROPERTY(bool                     enableDictionary    READ enableDictionary   WRITE setEnableDictionary   RESET resetEnableDictionary)
    Q_PROPERTY(bool                     alwaysResetBlobs    READ alwaysResetBlobs   WRITE setAlwaysResetBlobs   RESET resetAlwaysResetBlobs)
    Q_PROPERTY(RE::E::PageSegmentation	segmentation        READ segmentation       WRITE setSegmentation       RESET resetSegmentation)
    Q_PROPERTY(QString                  predicate           READ predicate          WRITE setPredicate          RESET resetPredicate)

public:
    // Constructor.
    Q_INVOKABLE                     OpticalCharacterRecognizer	(QObject *p=0);
    virtual                         ~OpticalCharacterRecognizer	();

    // Accessors.
    QString const&              	whiteList					() const						{ return _whiteList; }
    void                        	setWhiteList				(QString const &v)				{ _whiteList = v; }
    void                        	resetWhiteList				()								{ setWhiteList(""); }
    QString const&              	blackList					() const						{ return _blackList; }
    void                        	setBlackList				(QString const &v)				{ _blackList = v; }
    void                            resetBlackList				()								{ setBlackList(""); }
    bool                            enableDictionary            () const                        { return _enableDictionary; }
    void                            setEnableDictionary         (bool v)                        { _enableDictionary = v; }
    void                            resetEnableDictionary       ()                              { setEnableDictionary(true); }
    bool                            alwaysResetBlobs            () const                        { return _alwaysResetBlobs; }
    void                            setAlwaysResetBlobs         (bool v)                        { _alwaysResetBlobs = v; }
    void                            resetAlwaysResetBlobs       ()                              { setAlwaysResetBlobs(false); }
    E::PageSegmentation const&      segmentation				() const						{ return _segmentation; }
    void                            setSegmentation				(E::PageSegmentation const &v)	{ _segmentation = v; }
    void                            resetSegmentation			()								{ setSegmentation(RE::E::PageSegmentationSingleBlock); }
    QString const&                  predicate                   () const                        { return _predicate; }
    void                            setPredicate                (QString const &v)              { _predicate = v; }
    void                            resetPredicate              ()                              { setPredicate("eng"); }

protected:
    // Main reading function.
    virtual bool                    doRun						();
    void                            destroyApi                  ();

private:
    tesseract::TessBaseAPI          *_tessApi;
    QString                         _whiteList;
    QString                         _blackList;
    bool                            _enableDictionary;
    bool                            _alwaysResetBlobs;
    E::PageSegmentation             _segmentation;
    QString                         _lastInitLanguage;
    QString                         _predicate;
};

}

#endif
