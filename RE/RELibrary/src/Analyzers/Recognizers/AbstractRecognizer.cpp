#include "AbstractRecognizer.h"
#include <QtCore/QByteArray>
#include <QtCore/QCryptographicHash>

bool RE::AbstractRecognizer::run () {
    // Remove all previous results.
    _results.clear                      ();
    // Add checksum?
    if (_makeChecksums) {
        QCryptographicHash  hasher      (QCryptographicHash::Sha1);
        QImage const        *src        = source();
        qint32              realWidth   = src->width() * (src->format()==QImage::Format_Indexed8 ? 1 : 3);
        quint32             height      = src->height();
        for (quint32 y=0; y<height; ++y) {
            hasher.addData              ((char const*)src->scanLine(y), realWidth);
        }
        setResult                       ("Checksums", QString(hasher.result().toHex()));
    }
    // Finally run below abstraction.
    return AbstractAnalyzer::run();
}
