#include "RawOpticalCharacterRecognizer.h"
#include "../../Engine.h"
// Tesseract required stuff.
#include <tesseract/baseapi.h>
#include <tesseract/resultiterator.h>
#include <QtCore/QRect>
#include <QtCore/QDebug>

RE::RawOpticalCharacterRecognizer::RawOpticalCharacterRecognizer (QObject *p)
: AbstractRecognizer(p), _tessApi(0) {
    resetWhiteList              ();
    resetBlackList              ();
    resetEnableDictionary       ();
    resetAlwaysResetBlobs       ();
    resetPredicate              ();
    resetOldRawOutput           ();
    resetIterateBlocks          ();
    resetIterateParagraphs      ();
    resetIterateWords           ();
    resetIterateSymbols         ();
    resetIncludeSymbolChoices   ();
}
RE::RawOpticalCharacterRecognizer::~RawOpticalCharacterRecognizer () {
    destroyApi                         ();
}
void RE::RawOpticalCharacterRecognizer::destroyApi () {
    if (_tessApi) {
        _tessApi->End                       ();
        #ifdef Q_OS_WIN
        Wrn                                 << "Skipping deletion.";
        #else
        delete                              _tessApi;
        #endif
        _tessApi                            = 0;
    }
}

bool RE::RawOpticalCharacterRecognizer::doRun () {
    // No predicate?! Use default.
    if (_predicate.isEmpty())
        _predicate                          = "eng";
    // New predicate requested.
    if (_lastInitLanguage!=_predicate) {
        if (!_tessApi)
            _tessApi                        = new tesseract::TessBaseAPI();
        else
            _tessApi->End                   ();
        // Finally load language.
        Wrn                                 << "Loading predicate" << _predicate << "...";
        _lastInitLanguage					= _predicate;
        if (_tessApi->Init("./", _predicate.toUtf8().constData()))
            destroyApi                      ();
    }
    // Recognition cannot start.
    if (!_tessApi) {
        Crt                                 << "Unable to initialize recognizer. Aborting recognition.";
        return false;
    }
    // Clear API and reset classifier.
    _tessApi->Clear                         ();
    if (_alwaysResetBlobs) {
        _tessApi->ClearAdaptiveClassifier   ();
    }
    // Set up variables.
    _tessApi->SetVariable                   ("tessedit_char_whitelist",         _whiteList.toUtf8().constData());
    _tessApi->SetVariable                   ("tessedit_char_blacklist",         _blackList.toUtf8().constData());
    _tessApi->SetVariable                   ("tessedit_enable_doc_dict",        _enableDictionary ? "1" : "0");
    //_tessApi->SetVariable                   ("tessedit_debug_fonts",            "1");
    // Reset classifier blobs.
    if (_alwaysResetBlobs)
        _tessApi->ClearAdaptiveClassifier   ();

    // Change page segmentation.
    _tessApi->SetPageSegMode                (tesseract::PSM_AUTO);

    // Processing takes place here.
    QImage              src;
    if (source()->format()==QImage::Format_Indexed8) {
        src                                 = *source();
        _tessApi->SetImage                  (src.bits(), src.width(), src.height(), src.depth()/8, src.bytesPerLine());
    }
    else {
        src                                 = source()->convertToFormat(QImage::Format_RGB32).rgbSwapped();
        _tessApi->SetImage                  (src.bits(), src.width(), src.height(), src.depth()/8, src.bytesPerLine());
    }

    _tessApi->Recognize                     (0);
    tesseract::ResultIterator
                    *it                     = _tessApi->GetIterator();
    if (it) {
        // Get raw text.
        QStringList text;
        if (_oldRawOutput) {
            int             le, to, ri, bo;
            float           conf;
            char            *txt;
            it->Begin                               ();
            do {
                it->BoundingBox                     (tesseract::RIL_TEXTLINE, &le, &to, &ri, &bo);
                txt                                 = it->GetUTF8Text(tesseract::RIL_TEXTLINE);
                conf                                = it->Confidence(tesseract::RIL_TEXTLINE);
                text                                << QString("%1,%2 %3,%4, %5: %6").arg(le).arg(to).arg(ri).arg(bo).arg(conf).arg(QString::fromUtf8(txt));
                #ifndef Q_OS_WIN
                delete[]                            txt;
                #endif
            } while (it->Next(tesseract::RIL_TEXTLINE));
            text                                    << "----------------";
            it->Begin                               ();
            do {
                it->BoundingBox                     (tesseract::RIL_WORD, &le, &to, &ri, &bo);
                txt                                  = it->GetUTF8Text(tesseract::RIL_WORD);
                conf                                = it->Confidence(tesseract::RIL_WORD);
                text                                << QString("%1,%2 %3,%4, %5: %6").arg(le).arg(to).arg(ri).arg(bo).arg(conf).arg(QString::fromUtf8(txt));
                #ifndef Q_OS_WIN
                delete[]                            txt;
                #endif
            } while (it->Next(tesseract::RIL_WORD));
            setResult                               ("Raw", text.join("\r\n"));
        }
        else {
            it->Begin                               ();
            do {
                char    *txt                        = it->GetUTF8Text(tesseract::RIL_BLOCK);
                text                                += QString::fromUtf8(txt);
                #ifndef Q_OS_WIN
                delete[]                            txt;
                #endif
            } while (it->Next(tesseract::RIL_BLOCK));
            setResult                               ("Raw", text.join("\r\n"));
        }

        // Get data based on requested iteration levels.
        if (_iterateBlocks)
            runIteration                        (it, tesseract::RIL_BLOCK,  "Blocks");
        if (_iterateParagraphs)
            runIteration                        (it, tesseract::RIL_PARA,   "Paragraphs");
        if (_iterateWords)
            runIteration                        (it, tesseract::RIL_WORD,   "Words");
        if (_iterateSymbols)
            runIteration                        (it, tesseract::RIL_SYMBOL, "Symbols");
        delete                                  it;
    }

    return true;
}

void RE::RawOpticalCharacterRecognizer::runIteration (tesseract::ResultIterator *it, quint32 level, QString const &name) {
    QVariantList    blocks;
    QVariantHash    block;
    it->Begin                               ();
    int         le, to, ri, bo;
    float       conf;
    do {
        it->BoundingBox                     ((tesseract::PageIteratorLevel)level, &le, &to, &ri, &bo);
        char        *txt                    = it->GetUTF8Text((tesseract::PageIteratorLevel)level);
        conf                                = it->Confidence((tesseract::PageIteratorLevel)level);
        block["Text"]                       = QString::fromUtf8(txt);
        block["Geometry"]                   = QRect(le, to, ri-le, bo-to);
        block["Confidence"]                 = conf;
        blocks                              << block;
        #ifndef Q_OS_WIN
        delete[]                            txt;
        #endif
    } while (it->Next((tesseract::PageIteratorLevel)level));
    setResult                               (name, blocks);
}
