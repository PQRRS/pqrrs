#include "BarcodeReader.h"
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtGui/QColor>

#if defined(x86) || !defined(Q_OS_WIN)
#   include <zbar.h>
#endif

// Constructor.
RE::BarcodeReader::BarcodeReader (QObject *p)
: RE::AbstractRecognizer(p), _imageScanner(0) {
}
RE::BarcodeReader::~BarcodeReader () {
#if defined(x86) || !defined(Q_OS_WIN)
    if (_imageScanner) {
        delete _imageScanner;
        _imageScanner			= 0;
    }
    #endif
}

// Reads a barcode picture given a rect and a reading direction.
bool RE::BarcodeReader::doRun () {
    #if defined(x86) || !defined(Q_OS_WIN)
    // Instanciate zbar image scanner if not ready.
    if (!_imageScanner)
        _imageScanner					= new zbar::ImageScanner();

    // Configure right decoder.
    _imageScanner->set_config			(zbar::ZBAR_NONE, zbar::ZBAR_CFG_ENABLE, 0);
    zbar::zbar_symbol_type_e	e;
    switch (_expectedType) {
        case E::BarcodeStandardEAN8:			e	= zbar::ZBAR_EAN8;		break;
        case E::BarcodeStandardUPCE:			e	= zbar::ZBAR_UPCE;		break;
        case E::BarcodeStandardISBN10:			e	= zbar::ZBAR_ISBN10;	break;
        case E::BarcodeStandardUPCA:			e	= zbar::ZBAR_UPCA;		break;
        case E::BarcodeStandardEAN13:			e	= zbar::ZBAR_EAN13;		break;
        case E::BarcodeStandardISBN13:			e	= zbar::ZBAR_ISBN13;	break;
        case E::BarcodeStandardInterleaved25:	e	= zbar::ZBAR_I25;		break;
        case E::BarcodeStandardCode39:			e	= zbar::ZBAR_CODE39;	break;
        case E::BarcodeStandardPDF417:			e	= zbar::ZBAR_PDF417;	break;
        case E::BarcodeStandardQRCode:			e	= zbar::ZBAR_QRCODE;	break;
        case E::BarcodeStandardCode128:			e	= zbar::ZBAR_CODE128;	break;
        default:								e	= zbar::ZBAR_NONE;		break;
    }
    _imageScanner->set_config			(e, zbar::ZBAR_CFG_ENABLE, 1);
    // Set up expected lengths.
    _imageScanner->set_config			(e, zbar::ZBAR_CFG_MIN_LEN, _expectedLength);
    _imageScanner->set_config			(e, zbar::ZBAR_CFG_MAX_LEN, _expectedLength);
    // Reset density.
    _imageScanner->set_config			(zbar::ZBAR_NONE, zbar::ZBAR_CFG_X_DENSITY, 1);
    _imageScanner->set_config			(zbar::ZBAR_NONE, zbar::ZBAR_CFG_Y_DENSITY, 1);

    // Cache source informations.
    QImage const		*src			= source();
    quint32 const		w				= src->width();
    quint32 const		h				= src->height();
    quint32 const		srcBpl			= src->bytesPerLine();
    quint8 const		*srcBits		= src->bits();
    quint8 const		*dstBits		= 0;
    // Need of a copy of the image?
    if (srcBpl!=w) {
        quint8			*tmpBits		= new quint8[w*h];
        for (quint32 y=0; y<h; ++y)
            memcpy						(tmpBits+y*w, srcBits+y*srcBpl, w);
        dstBits							= tmpBits;
    }
    else
        dstBits							= srcBits;

    // Finally run scanner.
    zbar::Image				img         (w, h, "GREY", dstBits, w*h);
    _imageScanner->scan					(img);
    quint32                 i           = 0;
    for(zbar::Image::SymbolIterator s=img.symbol_begin(); s!=img.symbol_end(); ++s) {
        QVariantHash        code;
        code["TypeName"]                = QString::fromStdString(s->get_type_name());
        code["Type"]                    = s->get_type();
        code["Data"]                    = QString::fromStdString(s->get_data());
        setResult                       (QString::number(i), code);
        ++i;
    }
    img.set_data                        (0, 0);
    if (srcBpl!=w)
        delete[]						dstBits;
    #endif
    return								true;
}
