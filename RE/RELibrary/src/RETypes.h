/*! @file RETypes.h */

#ifndef RE_RETypes_h
#define RE_RETypes_h

#include <QtCore/QtGlobal>
#include <QtCore/QDebug>
#ifdef Q_OS_WIN
    #include <windows.h>
#endif

//! @brief Library Import / Export Definition.
#ifdef RELib
#	define RELibOpt Q_DECL_EXPORT
#else
#	define RELibOpt Q_DECL_IMPORT
#endif

//! @brief Logging Helpers.
#ifndef Dbg
#   define Dbg      qDebug()    << Q_FUNC_INFO << "#"
#   define Wrn      qWarning()  << Q_FUNC_INFO << "#"
#   define Crt      qCritical() << Q_FUNC_INFO << "#"
#endif

//! @brief Define RELibrary Calling Convention based on the platform's calling convention.
#ifndef RECConv
#   ifdef Q_OS_WIN
#       include <windows.h>
#       define RECConv WINAPI
#   endif
#   ifndef RECConv
#       define RECConv
#   endif
#endif

// Required stuff.
#include "ObjectsFactory.hpp"
#include "TypeWrapper.hpp"
#include "Matrix.hpp"
#include <QtCore/QList>
#include <QtCore/QHash>
#include <QtCore/QMutex>
#include <QtCore/QVector>
#include <QtCore/QBuffer>
#include <QtCore/QDateTime>
#include <QtCore/QMetaType>
#include <QtCore/QSemaphore>
#include <QtCore/QByteArray>
#include <QtCore/QVariantMap>
#include <QtCore/QStringList>
#include <QtCore/QMetaObject>
#include <QtGui/QImage>

#pragma pack(1)

//! @brief Enumeration Meta-type Streaming Registration Helper Macro.
#define	RegisterEnumStream(T)\
        friend QDataStream&	operator<<	(QDataStream& s, T const& t)	{ int i=t; return s << i; };\
        friend QDataStream&	operator>>	(QDataStream& s, T &t)			{ int i; s >> i; t=(T)i; return s; }

// Conditional forward declarations.
#ifndef RE_Engine_h
namespace RE { class Engine; }
#endif

/*! @brief Recognition Engine Namespace Definition.
 * This namespace holds the whole Recognition Engine logic. This includes:
 * - Data Types Definition,
 * - Filters,
 * - Annalyzers,
 * - Recognizers,
 * - The Client Binary format API exposition,
 * - The REPlug API exposition,
 * - A link to the licensing system (LM::Core),
 * - A scripting engine that is used for post-recognition processing.
 * - Various helpes to be used along the recognition workflow. */
namespace RE {
    // Forward declarations.
    static const char * const   SoftwareIdentifier = "fr.PierreQR.RecognitionSuite.RELibrary.Base";

    //! @brief Static Initialization Method (Types Initialization, Meta-Type Registration etc).
    RELibOpt void                   StaticInitializer					();
    //! @brief Recognition Engine Version Getter.
    RELibOpt char const*            Version								();
    //! @brief Backward-Compatible Client Binary Version Getter.
    RELibOpt char const*            ClientBinaryDataVersion				();
    //! @brief Converts a class or variable plain name to natural readable string.
    RELibOpt QString                ClassNameToNaturalString			(QString const &cn);
    //! @brief Image Signature Size Constant Getter.
    RELibOpt QSize                  DocumentTypesSignatureSize          ();
    //! @brief Application-Wide Invalid Variant Getter.
    RELibOpt QVariant               InvalidVariant                      ();
    //! @brief Convolution Matrix MetaType Identifier Getter.
    RELibOpt int                    CVMatricesMetatypeId                ();
    //! @brief Convolution Matrix MetaType Range First Identifier Getter.
    RELibOpt int                    FirstMatrixMetatypeId               ();
    //! @brief Convolution Matrix MetaType Range Last Identifier Getter.
    RELibOpt int                    LastMatrixMetatypeId                ();
    //! @brief Relative to Absolute Conversion Helper.
    RELibOpt QRect                  RelativeToAbsoluteRect				(QRectF const &r, QSize const &s);
    //! @brief Absolute to Relative Conversion Helper.
    RELibOpt QRectF                 AbsoluteToRelativeRect				(QRect const &r, QSizeF const &s);

    // ------------------------------------			Functions			------------------------------------ //
    //! @brief Native char* Delete-and-Copy Safeguard Helper.
    inline void					deleteAndCopy						(char *&dest, char const *source=0, bool deleteDest=true) {
        if (dest && deleteDest && dest!=source) {
            delete[]			dest;
            dest				= 0;
        }
        if (source && strlen(source)) {
            dest				= new char[strlen(source)+1];
            strcpy				(dest, source);
        }
    }
    //! @brief Native char* Delete-and-Copy Helper.
    inline void					deleteAndCopy						(char *&dest, QString const &source, bool deleteDest=true) {
        deleteAndCopy			(dest, source.toUtf8().constData(), deleteDest);
    }
    //! @brief Data Stream Version Reader Helper.
    inline quint32              readStreamVersion                   (QDataStream &s) {
        quint32         version;
        s               >> version;
        return version;
    }
    //! @brief Data Stream to QByteArray Template Conversion Helper.
    template<typename K>
    inline QByteArray			byteArrayFromStreamedValue			(K const &k) {
        QByteArray		ba;
        QBuffer			buff	(&ba);
        QDataStream		strm	(&buff);
        buff.open				(QIODevice::WriteOnly);
        strm					<< k;
        buff.close				();
        return					ba;
    }
    //! @brief QByteArray to Template Value Conversion Helper.
    template<typename K>
    inline K					valueFromStreamedByteArray			(QByteArray const &ba) {
        K					k;
        if (ba.count()) {
            QByteArray		copy	(ba);
            QBuffer			buff	(&copy);
            QDataStream		strm	(&buff);
            buff.open				(QIODevice::ReadOnly);
            strm					>> k;
            buff.close				();
        }
        return					k;
    }


    // ------------------------------------				Enums			------------------------------------ //
    //! @brief Range Object for Enumerators First and Last Type Definition.
    typedef QPair<int,int>	MetaTypesRange;
    //! @brief List of MetaTypesRange Type Definition.
    typedef QList<MetaTypesRange> MetaTypesRangeList;
    //! @brief QMetaObject* Based Enumeration Range Hashmap Type Definition.
    typedef QHash<QMetaObject const*,MetaTypesRangeList> MetaTypesRangeListHash;
    //! @brief Enumerations Encapsulation as a Fake QObject for Reflection Meta Access.
    class RELibOpt E {
    Q_GADGET
    public:
    Q_ENUMS	(First Agressivity BarcodeStandard PageSegmentation ReadingDirection DocumentValidity TransformMode Last)
        //! @brief First Enumeration Value Marker. Shouldn't be used directly.
        enum		First				{ FirstValue };
        //! @brief Agressivity Modes Definition.
        enum		Agressivity			{ AgressivityLowest = 0, AgressivityLow, AgressivityNormal, AgressivityHigh, AgressivityHighest };
        //! @brief Barcode Standard Modes Definition.
        enum		BarcodeStandard		{ BarcodeStandardUnknown = 0, BarcodeStandardEAN8, BarcodeStandardUPCE, BarcodeStandardISBN10, BarcodeStandardUPCA, BarcodeStandardEAN13, BarcodeStandardISBN13, BarcodeStandardInterleaved25, BarcodeStandardCode39, BarcodeStandardPDF417, BarcodeStandardQRCode, BarcodeStandardCode128 };
        //! @brief Page Segmentation Modes Definition.
        enum		PageSegmentation	{ PageSegmentationAutoOSD = 1, PageSegmentationAutoOnly, PageSegJementationAuto, PageSegmentationSingleColumn, PageSegmentationSingleBlockVertical, PageSegmentationSingleBlock, PageSegmentationSingleLine, PageSegmentationSingleWord, PageSegmentationSingleWordInCircle, PageSegmentationSingleChar };
        //! @brief Reading Directions Definition.
        enum		ReadingDirection	{ ReadingDirectionUnknown = 0, ReadingDirectionLeftToRight, ReadingDirectionRightToLeft, ReadingDirectionBottomToTop, ReadingDirectionTopToBottom };
        //! @brief Document Validities Definitions.
        enum		DocumentValidity	{ DocumentValidityValid = 0, DocumentValidityIncomplete, DocumentValidityInvalid };
        //! @brief Transformation Modes Definitions.
        enum        TransformMode       { TransformModeFast = 0, TransformModeSmooth };
        //! @brief Last Enumeration Value Marker. Shouldn't be used directly.
        enum		Last				{ LastValue };

        RegisterEnumStream	(RE::E::First)
        RegisterEnumStream	(RE::E::Agressivity)
        RegisterEnumStream	(RE::E::BarcodeStandard)
        RegisterEnumStream	(RE::E::PageSegmentation)
        RegisterEnumStream	(RE::E::ReadingDirection)
        RegisterEnumStream	(RE::E::DocumentValidity)
        RegisterEnumStream	(RE::E::TransformMode)
        RegisterEnumStream	(RE::E::Last)
    };
    //! @brief Enumeration's MetaObject Getter Helper.
    RELibOpt QMetaEnum              MetaForEnumType				(QMetaObject const *mo, QVariant::Type const &t);
    //! @brief Enumeration to Strong Type Conversion Helper.
    RELibOpt QVariant               VariantForEnumMetatypeId	(QVariant::Type mt, int v);
    //! @brief Strong Type to Weak Type Identifier Helper.
    RELibOpt int                    IntValueForEnumVariant		(QVariant const &v);


    // ------------------------------------     Runtime Contect         ------------------------------------ //
    /*! @brief Runtime Context Information Structure. */
    struct RuntimeContext {
        QString             name;
        RE::Engine const    *engine;
        explicit            RuntimeContext      (QString const &n, RE::Engine const *e) : name(n), engine(e) {}
    };


    // ------------------------------------				Types			------------------------------------ //
    #ifdef Q_MOC_RUN
    class RELibOpt T { Q_GADGET public:
    #else
    //! @brief Types Definitions.
    namespace T {
    #endif
        //! @brief Gray Value as a Standalone Strong Type Definition.
        DECLARE_TYPE_WRAPPER			(GrayValue, quint8)
        //! @brief Ratio as a MetaObject-Wrapped Native Type Definition.
        DECLARE_TYPE_WRAPPER			(Ratio, double)
        //! @brief Positive Ratio as a MetaObject-Wrapped Native Type Definition.
        DECLARE_INDIRECT_WRAPPER		(PositiveRatio, Ratio, double)
        //! @brief Negative Ratio as a MetaObject-Wrapped Native Type Definition.
        DECLARE_INDIRECT_WRAPPER		(NegativeRatio, Ratio, double)
        //! @brief Path as a MetaObject-Wrapped Native Type Definition.
        DECLARE_TYPE_WRAPPER			(Path, QString)
        //! @brief Folder Path as a MetaObject-Wrapped Native Type Definition.
        DECLARE_INDIRECT_WRAPPER		(FolderPath, Path, QString)
        //! @brief File Path as a MetaObject-Wrapped Native Type Definition.
        DECLARE_INDIRECT_WRAPPER		(FilePath, Path, QString)
        //! @brief List of Unsigned Ints Type Definition.
        typedef	QList<quint32>              UIntList;
        //! @brief List of Ints Type Definition.
        typedef	QVector<quint32>            UIntVector;
        //! @brief Hash of Unsigned Int to Unsigned Int Type Definition.
        typedef QHash<quint32, quint32>     UIntToUIntHash;
        //! @brief Map of Unsigned Int to Int Type Definition.
        typedef QMap<quint32, quint32>      UIntToUIntMap;
        //! @brief Hash of Unsigned Int to Double Type Definition.
        typedef	QHash<quint32, double>      UIntToDoubleHash;
        //! @brief Map of Unsigned Int to Double Type Definition.
        typedef QMap<quint32, double>       UIntToDoubleMap;
        //! @brief Hash of Unsigned Int to String Type Definition.
        typedef	QHash<quint32, QString>     UIntToStringHash;
        //! @brief Map of Unsigned Int to String Type Definition.
        typedef	QMap<quint32, QString>      UIntToStringMap;
        //! @brief Hash of Stringto String Type Definition.
        typedef	QHash<QString, QString>     StringToStringHash;
        //! @brief Map of Stringto String Type Definition.
        typedef	QMap<QString, QString>      StringToStringMap;
        //! @brief Hash of QVariants.
        typedef QHash<QString, QVariant>    StringToVariantHash;
        //! @brief Map of QVariants.
        typedef QMap<QString, QVariant>     StringToVariantMap;
        //! @brief Hash of Stringto Image Type Definition.
        typedef	QHash<QString, QImage>      StringToImageHash;
        //! @brief Map of Stringto Image Type Definition.
        typedef	QMap<QString, QImage>       StringToImageMap;
        //! @brief List of (Hashes of Unsigned Int to Double) Type Definition.
        typedef	QList<UIntToDoubleHash>     UIntToDoubleHashList;
        //! @brief List of (Maps of Unsigned Int to Double) Type Definition.
        typedef	QList<UIntToDoubleMap>      UIntToDoubleMapList;

        //! @brief Results block.
        typedef QHash<QString, QVariantHash> Results;
        //! @brief Convolution Matrix Type Definition.
        //! @see RE::Matrix
        typedef Matrix<double> CVMatrixElement;
        // The convolution matrix class itself.
        struct CVMatrix : public QList<CVMatrixElement> {
        public:
            // Default constructor.
            /**/                    CVMatrix				() : QList<CVMatrixElement>(), _mtId(0) {}
            virtual                 ~CVMatrix               () {}
            // Getter on original type.
            virtual int             metaTypeId				() const { return _mtId; }
            // QDataStream friend serialization stuff.
            friend QDataStream&		operator<<				(QDataStream& s, CVMatrix const &m)	{
                return      s       << (QList<CVMatrixElement> const&)m
                                    << m._mtId;
            }
            friend QDataStream&		operator>>				(QDataStream& s, CVMatrix &m)		{
                m.clear						();
                QList<CVMatrixElement>		matrix;
                int							mtId;
                s >> matrix >> mtId;
                m <<						matrix;
                m._mtId						= mtId;
                return s;
            }
        protected:
            int						_mtId;
        };
        typedef QList<CVMatrix>		CVMatrices;
        // Matrix system metatype markers.
        struct RELibOpt CVMatrixStart {
            inline friend QDataStream&	operator<<					(QDataStream& s, CVMatrixStart const&)      { return s; }
            inline friend QDataStream&	operator>>					(QDataStream& s, CVMatrixStart&)			{ return s; }
        };
        struct RELibOpt CVMatrixEnd {
            inline friend QDataStream&	operator<<					(QDataStream& s, CVMatrixEnd const&)        { return s; }
            inline friend QDataStream&	operator>>					(QDataStream& s, CVMatrixEnd&)              { return s; }
        };

        //! @brief Fill Color Type Definition.
        struct RELibOpt FillColor {
        public:
            // Members.
            bool				useSourceImage;
            GrayValue			color;
            /**/						FillColor					(bool us=false, quint8 c=255) : useSourceImage(us), color(c) {}
            // QDataStream friend serialization stuff.
            inline friend QDataStream&	operator<<					(QDataStream& s, FillColor const &fc)	{
                return      s       << (quint32)1
                                    << fc.useSourceImage << fc.color;
            }
            inline friend QDataStream&	operator>>					(QDataStream& s, FillColor &fc)			{
                readStreamVersion   (s);
                return      s       >> fc.useSourceImage >> fc.color;
            }
        };

        /*! @brief Zone Filter Data Definition Structure.
         * This structure is a persistence data container for RE::AbstractFilter based classes. It holds:
         * - The filter class,
         * - The filter binary location (Internal, Plugin, etc),
         * - All parameters for the filter to properly run. */
        struct RELibOpt ZoneFilterData {
            //! @brief List of Zone Filter Data Type Definition.
            typedef QList<ZoneFilterData> List;
            //! @brief Equality Operator Overload.
            inline bool					operator==					(ZoneFilterData const &zfd);
            //! @brief Inequality Operator Overload.
            inline bool					operator!=					(ZoneFilterData const &zfd);
            //! @brief To-Stream Operator Overload.
            inline friend QDataStream&	operator<<					(QDataStream& s, ZoneFilterData const &zfd)	{
                return      s       << (quint32)1
                                    << zfd.objectClassName << zfd.assemblyName << zfd.parameters;
            }
            //! @brief From-Stream Operator Overload.
            inline friend QDataStream&	operator>>					(QDataStream& s, ZoneFilterData &zfd)		{
                readStreamVersion   (s);
                return      s       >> zfd.objectClassName >> zfd.assemblyName >> zfd.parameters;
            }
            // Members.
            QString						objectClassName,
                                        assemblyName;
            QVariantMap					parameters;
        };

        /*! @brief Zone Recognizer Data Definition Structure.
         * This structure is a persistence data container for RE::AbstractRecognizer based classes. It holds:
         * - The recognizer class,
         * - The relative area on which to run the recognizer,
         * - The recognizer binary location (Internal, Plugin, etc),
         * - All parameters for the recognizer to properly run. */
        struct RELibOpt ZoneRecognizerData {
            //! @brief List of Zone Recognizer Data Type Definition.
            typedef QList<ZoneRecognizerData> List;
            //! @brief Equality Operator Overload.
            inline bool					operator==					(ZoneRecognizerData const &zrd);
            //! @brief Unequality Operator Overload.
            inline bool					operator!=					(ZoneRecognizerData const &zrd);
            //! @brief To-Stream Operator Overload.
            inline friend QDataStream&	operator<<					(QDataStream& s, ZoneRecognizerData const &zrd)	{
                return      s       << (quint32)1
                                    << zrd.objectClassName << zrd.assemblyName
                                    << zrd.relativeRect << zrd.parameters << zrd.filterStack;
            }
            //! @brief From-Stream Operator Overload.
            inline friend QDataStream&	operator>>					(QDataStream& s, ZoneRecognizerData &zrd)		{
                readStreamVersion   (s);
                return      s       >> zrd.objectClassName >> zrd.assemblyName
                                    >> zrd.relativeRect >> zrd.parameters >> zrd.filterStack;
            }
            // Members.
            QString						objectClassName,
                                        assemblyName;
            QRectF						relativeRect;
            QVariantMap					parameters;
            ZoneFilterData::List		filterStack;
        };

        /*! @brief Document Type Data Structure Definition.
         * This structure is a persistence data container for defining what a Document Type is made of. It allows the definition of:
         * - The Document Type internal code (Arbitrary, but must be unique per Client-Binary),
         * - A name (Human-readable, as defined in the RELibraryAdmin project) for the Document Type,
         * - A flag specifying wether the Document Type is ANN-trained or not,
         * - A flag telling wether the MICR pattern for the Document Type can uniquelly identify this type without risk amongst other Document Types,
         * - A flag indicating if a Validity Checking Script is present for this Document Type,
         * - A flag indicating if a Subtype Selection Script is present for this Document Type,
         * - A list of image-preprocessing filters (Unconditionnal for the whole image),
         * - A list of Recognizers that can be applied to this Document Type's images.
         * As these members are accessed very often, the developer is trusted to directly access the members of this object, and no getters / setters are exposed. This should stay the case unless the Document Type Data object is made available from outside the engine.
         * As this structure has streaming overloaded operators, those handle both:
         * - Filling up the structure from a stream of data, and handle version-migration related logic,
         * - Direct the structure data to a stream for persistence or network operations. */
        struct RELibOpt DocumentTypeData {
            // Types.
            typedef QList<DocumentTypeData> List;
            // QDataStream friend serialization stuff.
            inline friend QDataStream&	operator<<				(QDataStream& s, DocumentTypeData const &dtd)	{
                return      s       << (quint32)2
                                    << dtd.code << dtd.name
                                    << dtd.noAnnTraining
                                    << dtd.micrRegExp << dtd.micrIsUnique << dtd.badMicrRejection
                                    << dtd.preprocessingFilters << dtd.recognizers
                                    << dtd.hasDocumentValidityScript;
            }
            inline friend QDataStream&	operator>>				(QDataStream& s, DocumentTypeData &dtd)			{
                quint32     version = readStreamVersion(s);
                // As of version 2, the MICR regexp is stored after the noAnnTraining flag (Logic issue).
                // Also, the preprocessingFilters list was added to the stream, as well as the hasSubTypeSelectionScript flag.
                if (version==2)
                    return s        >> dtd.code >> dtd.name
                                    >> dtd.noAnnTraining
                                    >> dtd.micrRegExp >> dtd.micrIsUnique >> dtd.badMicrRejection
                                    >> dtd.preprocessingFilters >> dtd.recognizers
                                    >> dtd.hasDocumentValidityScript;
                else
                    return s        >> dtd.code >> dtd.name
                                    >> dtd.micrRegExp
                                    >> dtd.noAnnTraining
                                    >> dtd.micrIsUnique >> dtd.badMicrRejection
                                    >> dtd.recognizers
                                    >> dtd.hasDocumentValidityScript;
            }
            // Members.
            quint32						code;
            QString						name;
            bool						noAnnTraining;
            QString						micrRegExp;
            bool						micrIsUnique;
            bool						badMicrRejection;
            bool						hasDocumentValidityScript;
            bool                        hasSubtypeSelectionScript;
            ZoneFilterData::List        preprocessingFilters;
            ZoneRecognizerData::List	recognizers;
        };
        /*! @brief Client Binary Data Structure Definition.
         * This structure is a data persistence wrapper around all the data present within a Client Binary packet. It allows the definition of:
         * - A Version Identifier, usually the value of RE::ClientBinaryDataVersion() at the time of the generation,
         * - A Generator Version Identifier, usually the value of REA::Version() at the time of the generation,
         * - A Recognition Library Version, usually the value of RE::Version() at the time of the generation,
         * - A name (Human-readable, as defined when using the RELibraryAdmin generation wizard),
         * - A global script,
         * - A ANN threshold (As defined when using the RELibraryAdmin generation wizard),
         * - A ANN filling data block, used to populate the ANN at runtime,
         * - A list of Document Type Data definition objects (As specified when using the RELibraryAdmin generation wizard),
         * As these members are accessed very often, the developer is trusted to directly access the members of this object, and no
         * getters / setters are exposed. This should stay the case unless the Document Type Data object is made available
         * from outside the engine.
         * As this structure has streaming overloaded operators, those handle both:
         * - Filling up the structure from a stream of data, and handle version-migration related logic,
         * - Direct the structure data to a stream for persistence or network operations. */
        struct RELibOpt ClientBinaryData {
        public:
            // Clears the binary data object.
            void							clear				();
            // QDataStream friend serialization stuff.
            inline friend QDataStream&	operator<<				(QDataStream& s, ClientBinaryData const &cbd)	{
                return      s       << (quint32)1
                                    << cbd.version << cbd.generatorVersion << cbd.libraryVersion
                                    << cbd.name << cbd.copyright
                                    << cbd.script
                                    << cbd.annThreshold << cbd.annData
                                    << cbd.documentTypesData;
            }
            inline friend QDataStream&	operator>>				(QDataStream& s, ClientBinaryData &cbd)			{
                readStreamVersion   (s);
                return      s       >> cbd.version >> cbd.generatorVersion >> cbd.libraryVersion
                                    >> cbd.name >> cbd.copyright
                                    >> cbd.script
                                    >> cbd.annThreshold >> cbd.annData
                                    >> cbd.documentTypesData;
            }
            //! @brief Version String Member.
            QString						version;
            //! @brief Generator (E.g. RELibraryAdmin) Version String Member.
            QString						generatorVersion;
            //! @brief Library (E.g. RELibrary) Version String Member.
            QString						libraryVersion;
            //! @brief Human-Readable Name String Member.
            QString						name;
            //! @brief Human-Readable Copyright String Member.
            QString						copyright;
            //! @brief Global Script String Member.
            QString						script;
            //! @brief ANN Threshold Error Value Member.
            quint8						annThreshold;
            //! @brief ANN Data Binary Member.
            QByteArray					annData;
            //! @brief Document Types (Recognizers, etc) Member.
            DocumentTypeData::List		documentTypesData;
        };

        /*! @brief Recognition Document Structure Definition.
         * This structure is the base data container for any Recognition Document that is passed around the lifetime of a recognition cycle.
         * It allows to attach various pieces of information to documents:
         * - A Document Type Code once it has been guessed or passed from the API,
         * - A pointer to the Document Type Data once it's known,
         * - A timestamp indicating the moment of creation of this document,
         * - A map of strings containing the document informations (Such as MICR data, etc),
         * - A map of images containing the document various pictures (Front, Back, or more for specific applications),
         * - A list of acceptation reasons that gets filled up by the scripting block for this document once it's been recognized,
         * - A list of rejection reasons that gets filled up by the scripting block for this document once it's been recognized.
         * As these members are accessed very often, the developer is trusted to directly access the members of this object, and no getters / setters are exposed. This should stay the case unless the Document Type Data object is made available from outside the engine.
         * As this structure has streaming overloaded operators, those handle both:
         * - Filling up the structure from a stream of data, and handle version-migration related logic,
         * - Direct the structure data to a stream for persistence or network operations. */
        struct RELibOpt Document {
        public:
            // Constructor / Destructor.
            /**/						Document				(quint32 uid=0);
            /**/						Document				(RE::T::Document const& d);
            /**/						~Document				();
            // Members.
            QMutex                      mutex;                  // Locker.
            QSemaphore                  semaphore;              // Memory management counter.
            qint32						typeDataCode;			// Recognition type data code.
            RE::T::DocumentTypeData const
                                        *typeData;				// Holds recognition type data.
            quint32						uid;					// Unique identifier.
            bool						micrMatches;			// Set to true when recognition regexp matches.
            QDateTime					timestamp;				// Creation timestamp.
            RE::T::Results              results;                // Results map.
            //StringToStringMap			strings;				// Recognized strings.
            StringToImageMap			images;					// Acquired images.
            RE::E::DocumentValidity		validity;				// Computed validity.
            QStringList					acceptationReasons;		// Acceptation reasons upon validity checks.
            QStringList					rejectionReasons;		// Rejection reasons upon validity checks.
        };

        // Force metaobject declaration.
        extern RELibOpt const QMetaObject staticMetaObject;
    }

    // Results conversion to JSON format.
    RELibOpt QString        resultsToJsonString                 (RE::T::Results const &results);
    RELibOpt QString        variantToJson                       (QVariant const &v);
    RELibOpt QString        sanitizeJsonKey                     (QString const &key);
    RELibOpt QString        sanitizeJsonString                  (QString const &string);

    // ------------------------------------     Plugin Interface        ------------------------------------ //
    /*! @brief Plugin Information Definition Structure.
     * This structure's purpose is to keep track of what plugins expose to the engine, in term of their:
     * - Name,
     * - Version,
     * - Description (Human-Readable),
     * - File base name, as it was discovered by the plugin manager,
     * - Enumerator Ranges, so the engine can map native plugin types into strongly-typed types for building parameters lists.
     * When a Client Binary Data object is loaded in the engine, checks are made against plugins that might be required, based on
     * exported Client Binary filters, and recognizers binary locations.
     * @todo Dependency checks between plugins.
     * @see RE::AbstractPlugin, RE::PluginLoader, RE::PluginsManager */
    struct PluginBaseInfos {
        // Types.
        typedef QList<PluginBaseInfos> List;
        // Interface-useable members.
        QString					name,
                                version,
                                description;
        // Reserved.
        QString					fileBaseName;
        MetaTypesRangeListHash	enumeratorRanges;
        // Required for QList.
        inline bool				operator==		(PluginBaseInfos const &bi) { return name==bi.name && version==bi.version; }
    };
    //! @brief Plugin-Exported MetaObject List Type Definition.
    typedef QList<QMetaObject const*> PluginMetaObjectList;
}

#pragma pack()

// Special...
Q_DECLARE_METATYPE(void*)
Q_DECLARE_METATYPE(QRect)
Q_DECLARE_METATYPE(QRectF)

// Plugin base informations.
Q_DECLARE_TYPEINFO(RE::PluginBaseInfos, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::PluginBaseInfos)

// Enums.
Q_DECLARE_TYPEINFO(RE::E::First, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::E::First)
Q_DECLARE_TYPEINFO(RE::E::Agressivity, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::E::Agressivity)
Q_DECLARE_TYPEINFO(RE::E::BarcodeStandard, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::E::BarcodeStandard)
Q_DECLARE_TYPEINFO(RE::E::PageSegmentation, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::E::PageSegmentation)
Q_DECLARE_TYPEINFO(RE::E::ReadingDirection, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::E::ReadingDirection)
Q_DECLARE_TYPEINFO(RE::E::DocumentValidity, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::E::DocumentValidity)
Q_DECLARE_TYPEINFO(RE::E::TransformMode, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::E::TransformMode)
Q_DECLARE_TYPEINFO(RE::E::Last, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::E::Last)

// Basic types.
Q_DECLARE_METATYPE(RE::T::UIntList)
Q_DECLARE_METATYPE(RE::T::UIntVector)
Q_DECLARE_METATYPE(RE::T::UIntToUIntHash)
Q_DECLARE_METATYPE(RE::T::UIntToUIntMap)
Q_DECLARE_METATYPE(RE::T::UIntToDoubleHash)
Q_DECLARE_METATYPE(RE::T::UIntToDoubleMap)
Q_DECLARE_METATYPE(RE::T::UIntToStringHash)
Q_DECLARE_METATYPE(RE::T::UIntToStringMap)
Q_DECLARE_METATYPE(RE::T::StringToStringHash)
Q_DECLARE_METATYPE(RE::T::StringToStringMap)
Q_DECLARE_METATYPE(RE::T::StringToVariantHash)
Q_DECLARE_METATYPE(RE::T::StringToVariantMap)
Q_DECLARE_METATYPE(RE::T::StringToImageHash)
Q_DECLARE_METATYPE(RE::T::StringToImageMap)
Q_DECLARE_METATYPE(RE::T::UIntToDoubleHashList)
Q_DECLARE_METATYPE(RE::T::UIntToDoubleMapList)

Q_DECLARE_TYPEINFO(RE::T::GrayValue, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::T::GrayValue)
Q_DECLARE_TYPEINFO(RE::T::Ratio, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::T::Ratio)
Q_DECLARE_TYPEINFO(RE::T::PositiveRatio, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::T::PositiveRatio)
Q_DECLARE_TYPEINFO(RE::T::NegativeRatio, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::T::NegativeRatio)
Q_DECLARE_TYPEINFO(RE::T::Path, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::T::Path)
Q_DECLARE_TYPEINFO(RE::T::FolderPath, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::T::FolderPath)
Q_DECLARE_TYPEINFO(RE::T::FilePath, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::T::FilePath)

// Declare basic matrix types.
Q_DECLARE_METATYPE(RE::T::CVMatrixElement)
Q_DECLARE_METATYPE(RE::T::CVMatrix)
Q_DECLARE_METATYPE(RE::T::CVMatrices)

// Other classes / structs.
Q_DECLARE_TYPEINFO(RE::T::FillColor, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RE::T::FillColor)
Q_DECLARE_TYPEINFO(RE::T::ZoneFilterData, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::T::ZoneFilterData)
Q_DECLARE_TYPEINFO(RE::T::ZoneFilterData::List, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::T::ZoneFilterData::List)
Q_DECLARE_TYPEINFO(RE::T::ZoneRecognizerData, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::T::ZoneRecognizerData)
Q_DECLARE_TYPEINFO(RE::T::ZoneRecognizerData::List, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::T::ZoneRecognizerData::List)
Q_DECLARE_TYPEINFO(RE::T::ClientBinaryData, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RE::T::ClientBinaryData)

#endif
