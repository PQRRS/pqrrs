#ifndef RE_Resizer_h
#define RE_Resizer_h

// Base class.
#include "AbstractFilter.h"

namespace RE {

/*! @brief Resizer Filter Class.
 * This filter class aims to provide a simple and effective way to resize images. It relies on Qt's internal image processing methods
 * as it is robust and fast enough to be reliable for this task.
 * The behavior based on the parameters are as follows, given a width (w) and a height (h):
 * - If w and h are zero, nothing is done,
 * - If w or h is zero, then it will be computed according to the original image's ratio, so it will be preserved.
 * - If both w and h are non-zero, the aspect-ratio will be lost. */
class RELibOpt Resizer : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    /*! @brief Mark Offsets as Percentages. */
    Q_PROPERTY(bool     offsetInPercent READ offsetInPercent    WRITE setOffsetInPercent    RESET resetOffsetInPercent)
    //! @brief New Width Property, or Zero to Adapt to Height.
    Q_PROPERTY(quint32  width           READ width              WRITE setWidth              RESET resetWidth)
    //! @brief New Height Property, or Zero to Adapt to Width.
    Q_PROPERTY(quint32  height          READ height             WRITE setHeight             RESET resetHeight)
    /*! @brief Transformation Mode Property
     * @see RE::E::TransformMode */
    Q_PROPERTY (RE::E::TransformMode
                        mode            READ mode               WRITE setMode               RESET resetMode)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				Resizer						(QObject *p=0);

    // Accessors.
    bool const&             offsetInPercent             () const                    { return _offsetInPercent; }
    void                    setOffsetInPercent          (bool const &v)             { _offsetInPercent = v; }
    void                    resetOffsetInPercent        ()                          { _offsetInPercent = false; }
    quint32                 width						() const                    { return _size.width(); }
    void					setWidth					(quint32 v)                 { _size.setWidth(v); }
    void					resetWidth					()                          { _size.setWidth(0); }
    quint32                 height						() const                    { return _size.height(); }
    void					setHeight					(quint32 v)                 { _size.setHeight(v); }
    void					resetHeight					()                          { _size.setHeight(0); }
    RE::E::TransformMode    mode                        () const                    { return _mode; }
    void                    setMode                     (RE::E::TransformMode v)    { _mode = v; }
    void                    resetMode                   ()                          { _mode = RE::E::TransformModeSmooth; }

    inline static QImage	Apply						(quint32 w, quint32 h, QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        Resizer fltr;
        fltr.setWidth                   (w);
        fltr.setHeight                  (h);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }
protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply						();
    virtual QSize			minimumImageSize			() const;

private:
    bool                    _offsetInPercent;
    QSize					_size;
    RE::E::TransformMode    _mode;
};

}

#endif
