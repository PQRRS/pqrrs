#include "TiltCorrector.h"
#include "../PixelHelper.hpp"

RE::TiltCorrector::TiltCorrector (QObject *p)
: RE::AbstractFilter(p) {
    resetAngle          ();
    resetMode           ();
    resetFraming        ();
    resetLowBoundary    ();
    resetHighBoundary   ();
}

QImage RE::TiltCorrector::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize RE::TiltCorrector::minimumImageSize () const {
    return QSize(3, 3);
}

template<typename K>
QImage RE::TiltCorrector::algo () {
    // Rotate picture.
    QTransform              trans;
    trans.rotateRadians                 (-_angle);
    QImage                  altered     = source().transformed(trans, _mode==RE::E::TransformModeFast ? Qt::FastTransformation : Qt::SmoothTransformation);
    if (altered.format()==QImage::Format_ARGB32_Premultiplied) {
        altered                         = altered.convertToFormat(QImage::Format_ARGB32);
        PixelWriter<QRgb>   pw          (&altered);
        QRgb const          repl        = qRgba(_framing.red(), _framing.green(), _framing.blue(), 255);
        QRgb                pix;
        qint32              wOffset;
        for (qint32 y=0; y<pw.height; ++y) {
            wOffset                     = pw.valueOffset(y);
            for (qint32 x=0; x<pw.width; ++x) {
                pix                     = pw.value(x+wOffset);
                if (qAlpha(pix)!=255)
                    pw.setValue         (x+wOffset, repl);
            }
        }
    }
    // Return cropped result.
    return altered;
}
