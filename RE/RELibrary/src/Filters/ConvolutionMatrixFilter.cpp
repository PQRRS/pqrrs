#include "ConvolutionMatrixFilter.h"
#include "../Engine.h"
#include <QtCore/QDebug>
#define USE_MATH_DEFINES
#include <math.h>

RE::ConvolutionMatrixFilter::ConvolutionMatrixFilter (QObject *p)
: RE::AbstractFilter(p) {
    resetFillColor      ();
    resetMatrices       ();
}

// Apply filter.
QImage RE::ConvolutionMatrixFilter::doApply () {
    if (_matrices.isEmpty() || _matrices[0].isEmpty())
        return source();

    // Prepare a non-const source to be able do a permutation.
    QImage						src				= source().convertToFormat(QImage::Format_Indexed8, RE::Engine::grayscalePalette());
    src.setColorTable           (RE::Engine::grayscalePalette());
    src.setColorCount           (source().colorCount());

    // Prepare destination.
    QImage						dst				(src.size(), QImage::Format_Indexed8);
    dst.setColorTable							(RE::Engine::grayscalePalette());
    if (_fillColor.useSourceImage)				dst = src;
    else										dst.fill(_fillColor.color);

    int							mIsz			= 0;
    foreach (RE::T::CVMatrix matrix, _matrices) {
        mIsz									++;
        int						meIsz			= 0;
        foreach (RE::T::CVMatrixElement matrixElement, matrix) {
            meIsz								++;
            // Prepare borders computation.
            qint8				bordW		= (matrixElement.size().width()-1)/2;
            qint8				bordH		= (matrixElement.size().height()-1)/2;

            // Prepare native matrix to avoid Qt access overhead.
            quint8 const        mW			= matrixElement.size().width();
            quint8 const        mH			= matrixElement.size().height();
            double				mSum		= 0;
            quint32				mSize		= mW*mH;
            double const		*mNative	= matrixElement.data();
            for (quint32 i=0; i<mSize; ++i)
                mSum							+= mNative[i];

            // Prepare sum variables.
            double				sG;
            // Store pixel pointers and infos.
            quint8 const        *srcBits	= src.bits();
            quint32 const       srcBpl		= src.bytesPerLine();
            quint8              *dstBits	= dst.bits();
            qint32 const        right		= src.width()-bordW;
            qint32 const        bottom		= src.height()-bordH;
            // Normalization matrix.
            if ((quint32)mSum==0) {
                // Process each pixel, but omit matrix borders.
                qint32			yOffDst;
                qint32			yMOffPix, yMOff;
                for (qint32 y=bordH; y<bottom; ++y) {
                    yOffDst					= y*srcBpl;
                    for (qint32 x=bordW; x<right; ++x) {
                        // Iterate matrix.
                        sG					= 0;
                        for (qint32 my=0; my<mH; ++my) {
                            yMOffPix		= (y+my-bordH)*srcBpl;
                            yMOff			= my*mW;
                            for (qint32 mx=0; mx<mW; ++mx)
                                sG			+= srcBits[x+mx-bordW + yMOffPix] * mNative[mx+yMOff];
                        }
                        dstBits[x+yOffDst]	= qBound(0.0, sG+128.0, 255.0);
                    }
                }
            }
            // Coef. matrix.
            else {
                // Process each pixel, but omit matrix borders.
                qint32			yOffDst;
                qint32			yMOffPix, yMOff;
                for (qint32 y=bordH; y<bottom; ++y) {
                    yOffDst					= y*srcBpl;
                    for (qint32 x=bordW; x<right; ++x) {
                        // Iterate matrix.
                        sG					= 0;
                        for (qint32 my=0; my<mH; ++my) {
                            yMOffPix		= (y+my-bordH)*srcBpl;
                            yMOff			= my*mW;
                            for (qint32 mx=0; mx<mW; ++mx)
                                sG			+= srcBits[x+mx-bordW + yMOffPix] * mNative[mx+yMOff];
                        }
                        sG					/= mSum;
                        dstBits[x+yOffDst]	= qBound(0.0, sG, 255.0);
                    }
                }
            }
            // If the current element isn't the last of the last matrix, permute source and destination.
            if (mIsz<_matrices.count() || meIsz<matrix.count())
                src								= dst;
        }
    }
    // Might-set the debugging output.
    setDebuggingImage               (dst);
    return                          dst;
}
QSize RE::ConvolutionMatrixFilter::minimumImageSize () const {
    QSize	size			(1, 1);
    foreach (RE::T::CVMatrix matrix, matrices()) {
        foreach (RE::T::CVMatrixElement matrixElement, matrix) {
            size.setWidth	(qMax(size.width(), matrixElement.size().width()));
            size.setHeight	(qMax(size.height(), matrixElement.size().height()));
        }
    }
    return size;
}
