#ifndef RE_ColorsRemover_h
#define RE_ColorsRemover_h

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include <QtGui/QColor>

namespace RE {

/*! @brief Color Remover Filter Class.
 * @todo Documentation. */
class RELibOpt ColorsRemover : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    //! @brief Target (Keep) Color Property.
    Q_PROPERTY(QColor       		targetColor         READ targetColor        WRITE setTargetColor        RESET resetTargetColor)
    //! @brief Replacement Color Property.
    Q_PROPERTY(QColor               replacementColor    READ replacementColor   WRITE setReplacementColor   RESET resetReplacementColor)
    //! @brief Target Distance Threshold Property.
    Q_PROPERTY(quint16              deltaThreshold      READ deltaThreshold     WRITE setDeltaThreshold     RESET resetDeltaThreshold)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE						ColorsRemover           (QObject *p=0);

    // Accessors.
    QColor const&                   targetColor				() const					{ return _targetColor; }
    void							setTargetColor			(QColor const &v)           { _targetColor = v; }
    void                            resetTargetColor        ()                          { _targetColor = QColor::fromRgb(0x00,0x00,0x00); }
    QColor const&                   replacementColor        () const					{ return _replacementColor; }
    void							setReplacementColor     (QColor const &v)           { _replacementColor = v; }
    void                            resetReplacementColor   ()                          { _replacementColor = QColor::fromRgb(0xff,0xff,0xff); }
    quint16                         deltaThreshold			() const					{ return _deltaThreshold; }
    void							setDeltaThreshold       (quint16 v)                 { _deltaThreshold = v; }
    void                            resetDeltaThreshold     ()                          { _deltaThreshold = 64; }

    inline static QImage			Apply					(QColor const &tc, quint16 dt, QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        ColorsRemover fltr;
        fltr.setTargetColor             (tc);
        fltr.setDeltaThreshold          (dt);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage                  doApply                 ();
    virtual QSize                   minimumImageSize        () const;

private:
    template<typename K>
    QImage                          algo                    ();
    template<typename K>
    bool                            parallelizedByteAlgo    (QImage const *src, QImage *dst, qint32 yStart, qint32 yEnd);

private:
    QColor          				_targetColor;
    QColor                          _replacementColor;
    quint16                         _deltaThreshold;
};

}

#endif
