#ifndef RE_Cropper_h
#define RE_Cropper_h

// Base class.
#include "AbstractFilter.h"

namespace RE {

/*! @brief Image Brightness / Contrast Filter Class.
 * @todo Documentation. */
class RELibOpt Cropper : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    /*! @brief Mark Offsets as Percentages. */
    Q_PROPERTY(bool     offsetInPercent READ offsetInPercent    WRITE setOffsetInPercent    RESET resetOffsetInPercent)
    /*! @brief Margin from Left to be Suppressed. */
    Q_PROPERTY(quint32  leftOffset      READ leftOffset         WRITE setLeftOffset         RESET resetLeftOffset)
    /*! @brief Margin from Top to be Suppressed. */
    Q_PROPERTY(quint32  topOffset       READ topOffset          WRITE setTopOffset          RESET resetTopOffset)
    /*! @brief Margin from Right to be Suppressed. */
    Q_PROPERTY(quint32  rightOffset     READ rightOffset        WRITE setRightOffset        RESET resetRightOffset)
    /*! @brief Margin from Bottom to be Suppressed. */
    Q_PROPERTY(quint32  bottomOffset    READ bottomOffset       WRITE setBottomOffset       RESET resetBottomOffset)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				Cropper                     (QObject *p=0);

    // Accessors.
    bool const&         offsetInPercent                 () const            { return _offsetInPercent; }
    void                setOffsetInPercent              (bool const &v)     { _offsetInPercent = v; }
    void                resetOffsetInPercent            ()                  { _offsetInPercent = false; }
    quint32 const&      leftOffset                      () const            { return _leftOffset; }
    void                setLeftOffset                   (quint32 const &v)  { _leftOffset = v; }
    void                resetLeftOffset                 ()                  { _leftOffset = 0; }
    quint32 const&      topOffset                       () const            { return _topOffset; }
    void                setTopOffset                    (quint32 const &v)  { _topOffset = v; }
    void                resetTopOffset                  ()                  { _topOffset = 0; }
    quint32 const&      rightOffset                     () const            { return _rightOffset; }
    void                setRightOffset                  (quint32 const &v)  { _rightOffset = v; }
    void                resetRightOffset                ()                  { _rightOffset = 0; }
    quint32 const&      bottomOffset                    () const            { return _bottomOffset; }
    void                setBottomOffset                 (quint32 const &v)  { _bottomOffset = v; }
    void                resetBottomOffset               ()                  { _bottomOffset = 0; }

    // Static applier.
    inline static QImage	Apply						(bool oip, quint32 lo, quint32 to, quint32 ro, quint32 bo, QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        Cropper fltr;
        fltr.setOffsetInPercent         (oip);
        fltr.setLeftOffset              (lo);
        fltr.setTopOffset               (to);
        fltr.setRightOffset             (ro);
        fltr.setBottomOffset            (bo);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    bool            _offsetInPercent;
    quint32         _leftOffset;
    quint32         _topOffset;
    quint32         _rightOffset;
    quint32         _bottomOffset;
};

}

#endif
