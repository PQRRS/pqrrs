#include "ObjectExtractor.h"
#include "../PixelHelper.hpp"
#include <QtGui/QPainter>
#include <QtCore/QDebug>

RE::ObjectExtractor::ObjectExtractor (QObject *p)
: RE::AbstractFilter(p) {
    resetFilterStack    ();
    resetStep           ();
    resetLowerLimit     ();
    resetUpperLimit     ();
    resetFromRight      ();
    resetFromLeft       ();
    resetFromTop        ();
    resetFromBottom     ();
}

QImage RE::ObjectExtractor::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize RE::ObjectExtractor::minimumImageSize () const {
    return QSize(3, 3);
}

template<typename K>
QImage RE::ObjectExtractor::algo () {
    QImage const    &src                = source();
    // Processing takes place here.
    RE::RuntimeContext const
                    &rt                 = runtimeContext();
    QImage          tmp                 = rt.engine->filteredImage(_filterStack, rt.name, src, parallelizedCoresCount());
    // Step is greater than the image height / width. That's a problem.
    if (_locator.step()>=tmp.width() || _locator.step()>=tmp.height()) {
        Wrn                             << "Step is greater than the image dimensions, aborting detection.";
        return                          src;
    }
    // Now that the image has been contrasted, detect edges.
    _locator.setParallelizedCoresCount  (parallelizedCoresCount());
    _locator.setSource                  (&tmp);
    _locator.run                        ();
    _locator.resetSource                ();
    // Crop source image.
    QRectF          relBounds           = RE::AbsoluteToRelativeRect(_locator.boundaries(), tmp.size());
    QRect           absBounds           = RE::RelativeToAbsoluteRect(relBounds, src.size());
    if (debuggingEnabled())
        setDebuggingImage               (tmp);
    // Return cropped result.
    return src.copy(absBounds);
}
