#ifndef RE_MedianFilter_h
#define RE_MedianFilter_h

// Required stuff.
#include "AbstractFilter.h"

namespace RE {

/*! @brief Median Filter Class.
 * @todo Documentation. */
class RELibOpt MedianFilter : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    //! @brief Framing Fill Color Property.
    Q_PROPERTY	(RE::T::FillColor	fillColor			READ fillColor			WRITE setFillColor			RESET resetFillColor)
    //! @brief Horizontal Radius Property.
    Q_PROPERTY	(quint32            horizontalRadius	READ horizontalRadius	WRITE sethorizontalRadius	RESET resetHorizontalRadius)
    //! @brief Vertical Radius Property.
    Q_PROPERTY	(quint32            verticalRadius		READ verticalRadius		WRITE setVerticalRadius		RESET resetVerticalRadius)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				MedianFilter			(QObject *p=0);

    // Accessors.
    RE::T::FillColor		fillColor				() const                { return _fillColor; }
    void					setFillColor			(RE::T::FillColor v)	{ _fillColor = v; }
    void					resetFillColor			()						{ _fillColor = RE::T::FillColor(); }
    quint32					horizontalRadius		()	const				{ return _horizontalRadius; }
    void					sethorizontalRadius		(quint32 v)				{ _horizontalRadius = v; }
    void					resetHorizontalRadius	()						{ _horizontalRadius = 0; }
    quint32					verticalRadius			()	const				{ return _verticalRadius; }
    void					setVerticalRadius		(quint32 v)				{ _verticalRadius = v; }
    void					resetVerticalRadius		()						{ _verticalRadius = 0; }

    inline static QImage	Apply					(quint32 hRad, quint32 vRad, QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        MedianFilter fltr;
        fltr.sethorizontalRadius        (hRad);
        fltr.setVerticalRadius          (vRad);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    template<typename K>
    QImage                  algo                        ();
    template<typename K>
    bool                    parallelizedByteAlgo        (QImage const *src, QImage *dst, qint32 z, qint32 yStart, qint32 yEnd);

private:
    RE::T::FillColor		_fillColor;
    quint32					_horizontalRadius, _verticalRadius;
};

};

#endif
