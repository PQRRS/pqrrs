#include "LocalThresholder.h"
#include "../Analyzers/CumulativeHistogram.h"
#include "../Engine.h"
#include "../PixelHelper.hpp"
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#include <math.h>

RE::LocalThresholder::LocalThresholder (QObject *p)
    : AbstractFilter(p) {
    resetFillColor          ();
    resethorizontalRadius   ();
    resetVerticalRadius     ();
    resetDelta              ();
}
QImage RE::LocalThresholder::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize RE::LocalThresholder::minimumImageSize () const {
    return QSize(_horizontalRadius*2+1, _verticalRadius*2+1);
}

template<typename K> QImage RE::LocalThresholder::algo () {
    if (_horizontalRadius==0 && _verticalRadius==0)
        return                      source();
    // Prepare source pointers.
    QImage const            *src    = &source();
    PixelReader<K> const    pr      (src);
    // Prepare destination pointers.
    QImage                  dst     = src->copy();
    if (!_fillColor.useSourceImage) {
        quint8 const        &fc     = _fillColor.color;
        if (pr.isMultiplane)
            dst.fill                (qRgb(fc,fc,fc));
    }
    // Parallelized run!
    qint32 const            pcc     = parallelizedCoresCount();
    if (pcc==1)
        for (qint32 z=pr.firstColorPlane; z<=pr.lastColorPlane; ++z)
            parallelizedByteAlgo<K> (src, &dst, z, _verticalRadius, src->height()-_verticalRadius);
    else {
        double const        step    = ((double)src->height()-(_verticalRadius*2))/(double)pcc;
        QFutureSynchronizer<bool>
                            tasks;
        for (qint32 z=pr.firstColorPlane; z<=pr.lastColorPlane; ++z)
            if (pcc==1)
                parallelizedByteAlgo<K> (src, &dst, z, 0, pr.height);
            else {
                for (qint32 i=0; i<pcc; ++i) {
                    qint32      yStart  = _verticalRadius + step*i;
                    qint32      yEnd    = _verticalRadius + step*(i+1);
                    tasks.addFuture     (QtConcurrent::run<bool>(this, &RE::LocalThresholder::parallelizedByteAlgo<K>, src, &dst, z, yStart, yEnd));
                }
                tasks.waitForFinished       ();
            }
        }
    // Might-set the debugging output.
    setDebuggingImage               (dst);
    return                          dst;
}
template<typename K>
bool RE::LocalThresholder::parallelizedByteAlgo (QImage const *src, QImage *dst, qint32 z, qint32 yStart, qint32 yEnd) {
    // Prepare source pointers.
    PixelReader<K> const    pr      (src);
    PixelWriter<K>          pw      (dst);
    // Prepare some constant helpers.
    qint32 const    hRad            = _horizontalRadius;
    qint32 const    vRad            = _verticalRadius;
    qint32 const    arrSize         = (hRad*2+1)*(vRad*2+1);
    qint32 const    right           = pr.width-hRad-1;
    quint8          pix;
    qint32          mean;
    qint32          sum, rOffset, rMatOffset, wOffset;
    for (qint32 y=yStart; y<yEnd; ++y) {
        rOffset                 = pr.byteOffset(y, z);
        wOffset                 = pw.byteOffset(y, z);
        for (qint32 x=hRad; x<right; ++x) {
            sum                 = 0;
            for (qint32 mY=-vRad; mY<=vRad; ++mY) {
                rMatOffset      = pr.byteOffset(y+mY, z);
                for (qint32 mX=-hRad; mX<=hRad; ++mX)
                    sum         += pr.byte((x+mX)*pr.bpc+rMatOffset);
            }
            mean                = qBound(0, sum/arrSize+_delta, 255);
            pix                 = pr.byte(x*pr.bpc+rOffset);
            pw.setByte          (x*pw.bpc+wOffset, pix>mean ? 255 : 0);
        }
    }
    return true;
}
