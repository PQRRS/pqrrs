#include "HistogramEqualizer.h"
#include "../Analyzers/CumulativeHistogram.h"
#include "../Engine.h"
#include "../PixelHelper.hpp"
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#include <QtCore/QDebug>
#include <math.h>

RE::HistogramEqualizer::HistogramEqualizer (QObject *p)
: AbstractFilter(p) {
}

QImage RE::HistogramEqualizer::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize RE::HistogramEqualizer::minimumImageSize () const {
	return QSize(1, 1);
}

template<typename K>
QImage RE::HistogramEqualizer::algo () {
    // Prepare source pointers.
    QImage const            *src    = &source();
    PixelReader<K> const    pr      (src);
    // Prepare destination pointers.
    QImage                  dst     (src->size(), src->format());
    dst.setColorTable               (src->colorTable());
    dst.setColorCount               (src->colorCount());
    dst.fill                        (Qt::white);
    // Prepare common histogram object.
    RE::CumulativeHistogram ch;
    quint32 const           *chData;
    ch.setSource                    (src);
    // Parallelized run!
    qint32 const            pcc     = parallelizedCoresCount();
    double const            step    = (double)src->height()/(double)pcc;
    QList<quint8*>          eqhDatas;
    QFutureSynchronizer<bool>
                            tasks;
    for (qint32 z=pr.firstColorPlane; z<=pr.lastColorPlane; ++z) {
        // Compute current plane's histogram.
        ch.setPlane                 (z);
        ch.run					    ();
        chData                      = ch.cumulativeData().data();
        double const    coef        = 255.0/(pr.width*pr.height);
        quint8          *eqhData    = new quint8[256];
        eqhDatas                    << eqhData;
        for (quint16 i=0; i<256; ++i)
            eqhData[i]              = qBound(0.0, chData[i]*coef, 255.0);
        if (pcc==1) {
            ParallelizationParams p;
            p.src                   = src;
            p.dst                   = &dst;
            p.eqhData               = eqhData;
            p.z                     = z;
            p.yStart                = 0;
            p.yEnd                  = pr.height;
            parallelizedByteAlgo<K> (p);
        }
        else {
            for (qint32 i=0; i<pcc; ++i) {
                ParallelizationParams p;
                p.src               = src;
                p.dst               = &dst;
                p.eqhData           = eqhData;
                p.z                 = z;
                p.yStart            = step*i;
                p.yEnd              = step*(i+1);
                tasks.addFuture     (QtConcurrent::run<bool>(this, &RE::HistogramEqualizer::parallelizedByteAlgo<K>, p));
            }
        }
    }
    if (pcc>1)
        tasks.waitForFinished       ();
    // Once futures are done, delete old equalization data.
    foreach (quint8 *eqhData, eqhDatas)
        delete[]                    eqhData;
    // Might-set the debugging output.
    setDebuggingImage               (dst);
    return                          dst;
}
template<typename K>
bool RE::HistogramEqualizer::parallelizedByteAlgo (ParallelizationParams p) {
    // Prepare source pointers.
    PixelReader<K> const    pr      (p.src);
    PixelWriter<K>          pw      (p.dst);
    qint32          rOffset, wOffset;
    for (qint32 y=p.yStart; y<p.yEnd; ++y) {
        rOffset                 = pr.byteOffset(y, p.z);
        wOffset                 = pw.byteOffset(y, p.z);
        for (qint32 x=0; x<pr.width; ++x)
            pw.setByte          (x*pw.bpc+wOffset, p.eqhData[pr.byte(x*pr.bpc+rOffset)]);
    }
    return                      true;
}
