#include "MedianFilter.h"
#include "../Engine.h"
#include "../PixelHelper.hpp"
#include <QtCore/QDebug>
#include <QtCore/QFutureSynchronizer>
#include <QtCore/QtConcurrentRun>

RE::MedianFilter::MedianFilter (QObject *p)
: RE::AbstractFilter(p) {
    resetFillColor          ();
    resetHorizontalRadius   ();
    resetVerticalRadius     ();
}
QImage RE::MedianFilter::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize RE::MedianFilter::minimumImageSize () const {
	return QSize(_horizontalRadius*2+1, _verticalRadius*2+1);
}

template<typename K>
QImage RE::MedianFilter::algo () {
    if (_horizontalRadius==0 && _verticalRadius==0)
        return                      source();
    // Prepare source pointers.
    QImage const            *src    = &source();
    PixelReader<K> const    pr      (src);
    // Prepare destination pointers.
    QImage                  dst;
    if (_fillColor.useSourceImage)
        dst                         = src->copy();
    else {
        quint8 const        &fc     = _fillColor.color;
        dst                         = QImage(src->size(), src->format());
        dst.setColorTable           (src->colorTable());
        dst.setColorCount           (src->colorCount());
        if (pr.isMultiplane)
            dst.fill                (qRgb(fc,fc,fc));
        else
            dst.fill                (fc);
    }
    // Parallelized run!
    qint32 const            pcc     = parallelizedCoresCount();
    if (pcc==1)
        for (qint32 z=pr.firstColorPlane; z<=pr.lastColorPlane; ++z)
            parallelizedByteAlgo<K> (src, &dst, z, _verticalRadius, src->height()-_verticalRadius);
    else {
        double const        step    = ((double)src->height()-(_verticalRadius*2))/(double)pcc;
        QFutureSynchronizer<bool>
                            tasks;
        for (qint32 z=pr.firstColorPlane; z<=pr.lastColorPlane; ++z)
            for (qint32 i=0; i<pcc; ++i) {
                qint32      yStart  = _verticalRadius + step*i;
                qint32      yEnd    = _verticalRadius + step*(i+1);
                tasks.addFuture     (QtConcurrent::run<bool>(this, &RE::MedianFilter::parallelizedByteAlgo<K>, src, &dst, z, yStart, yEnd));
            }
        tasks.waitForFinished       ();
    }
    // Might-set the debugging output.
    setDebuggingImage               (dst);
    return                          dst;
}
template<typename K>
bool RE::MedianFilter::parallelizedByteAlgo (QImage const *src, QImage *dst, qint32 z, qint32 yStart, qint32 yEnd) {
    // Prepare source pointers.
    PixelReader<K> const    pr      (src);
    PixelWriter<K>          pw      (dst);
    // Prepare some constant helpers.
    qint16 const        hRad		= _horizontalRadius;
    qint16 const        vRad		= _verticalRadius;
    qint16 const        arrSize		= (hRad*2+1)*(vRad*2+1);
    qint16 const        middle		= arrSize/2;
    qint32              p;
    quint8              *pixelsData;
    QVector<quint8>     pixels;
    pixels.resize                   (arrSize);
    qint32              rMatOffset, wOffset;
    for (qint32 y=yStart; y<yEnd; ++y) {
        wOffset                     = pw.byteOffset(y, z);
        for (qint32 x=hRad; x<pr.width-hRad; ++x) {
            p                       = 0;
            pixelsData              = pixels.data();
            for (qint32 mY=-vRad; mY<=vRad; ++mY) {
                rMatOffset          = pr.byteOffset(y+mY, z);
                for (qint32 mX=-hRad; mX<=hRad; ++mX)
                    pixelsData[p++] = pr.byte((x+mX)*pr.bpc+rMatOffset);
            }
            qSort                   (pixels);
            pw.setByte              (x*pw.bpc+wOffset, pixels[middle]);
        }
    }
    return true;
}
