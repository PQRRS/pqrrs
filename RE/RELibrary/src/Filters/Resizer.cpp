#include "Resizer.h"
#include "../Engine.h"
#include <QtCore/QDebug>

RE::Resizer::Resizer (QObject *p)
: RE::AbstractFilter(p) {
    resetOffsetInPercent    ();
    resetWidth              ();
    resetHeight             ();
    resetMode               ();
}

QImage RE::Resizer::doApply () {
    qint32			w		= _size.width();
    qint32			h		= _size.height();
    if (!w && !h)			return source();
    QImage const	&src	= source();
    // Compute ratios.
    if (w==0)
        w                   = h*src.width()/src.height();
    else if (h==0)
        h                   = w*src.height()/src.width();
    if (!w || !h)
        return source();

    if (_offsetInPercent) {
        w                   = src.width()/100.0*w;
        h                   = src.height()/100.0*h;
    }

    QImage          dst     = src.scaled(w, h, Qt::IgnoreAspectRatio, _mode==RE::E::TransformModeFast ? Qt::FastTransformation : Qt::SmoothTransformation);
    if (forceGrayscale() || dst.isGrayscale())
        dst                 = RE::Engine::paletizeIfGrayscale(dst, true);
    else
        dst                 = dst.convertToFormat(QImage::Format_ARGB32);
    // Might-set the debugging output.
    setDebuggingImage       (dst);
    return                  dst;
}
QSize RE::Resizer::minimumImageSize () const {
	return QSize(2, 2);
}
