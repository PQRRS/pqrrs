#ifndef RE_OpticalTiltCorrector_h
#define RE_OpticalTiltCorrector_h

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include "HistogramStretcher.h"
namespace tesseract { class TessBaseAPI; }

namespace RE {

/*! @brief Optical Tilt Correction Filter Class.
 * This filter attempts to detect and deskew an image based on an OCR-heuristic method.
 * @see RE::RectangleLocator */
class RELibOpt OpticalTiltCorrector : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    Q_PROPERTY(RE::T::ZoneFilterData::List
                            filterStack         READ filterStack        WRITE setFilterStack        RESET resetFilterStack)
    Q_PROPERTY(bool         orientationOnly     READ orientationOnly    WRITE setOrientationOnly    RESET resetOrientationOnly)


public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE             OpticalTiltCorrector        (QObject *p=0);
    virtual                 ~OpticalTiltCorrector       ();
    // Accessors.
    RE::T::ZoneFilterData::List const&
                            filterStack                 () const                                { return _filterStack; }
    void                    setFilterStack              (RE::T::ZoneFilterData::List const &v)  { _filterStack = v; }
    void                    resetFilterStack            ()                                      { _filterStack.clear(); }
    bool const&             orientationOnly             () const                                { return _orientationOnly; }
    void                    setOrientationOnly          (bool const &v)                         { _orientationOnly = v; }
    void                    resetOrientationOnly        ()                                      { _orientationOnly = true; }

protected:
    virtual QSize			minimumImageSize            () const;
    QImage                  doApply                     ();
    void                    destroyApi                  ();

private:
    tesseract::TessBaseAPI          *_tessApi;
    RE::T::ZoneFilterData::List     _filterStack;
    bool                            _orientationOnly;
};

}

#endif
