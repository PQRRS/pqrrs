#include "Cropper.h"
#include "../RETypes.h"
#include "../Engine.h"
#include "../PixelHelper.hpp"
#include <QtCore/QRect>
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#define _USE_MATH_DEFINES
#include <math.h>

RE::Cropper::Cropper (QObject *p)
: RE::AbstractFilter(p) {
    resetOffsetInPercent    ();
    resetLeftOffset         ();
    resetTopOffset          ();
    resetRightOffset        ();
    resetBottomOffset       ();
}
QImage RE::Cropper::doApply () {
    QImage const        &src        = source();
    QRect               crop;
    if (_offsetInPercent) {
        if (100-_leftOffset-_rightOffset<=0 || 100-_topOffset-_bottomOffset<=0) {
            Wrn                     << "Impossible area width requested.";
            return source();
        }
        QRectF          tmp;
        tmp.setCoords               (_leftOffset, _topOffset, 100.0 - _rightOffset, 100 - _bottomOffset);
        crop                        = RE::RelativeToAbsoluteRect(tmp, src.size());
    }
    else {
        qint32 const    iW          = src.width();
        qint32 const    iH          = src.height();
        if (iW-_leftOffset-_rightOffset<=0 || iH-_topOffset-_bottomOffset<=0) {
            Wrn                     << "Impossible area width requested.";
            return source();
        }
        crop.setCoords              (_leftOffset, _topOffset, iW-_rightOffset, iH-_bottomOffset);
    }
    return                          src.copy(crop);
}
QSize RE::Cropper::minimumImageSize () const {
    return QSize(1, 1);
}
