#ifndef RE_ObjectExtractor_h
#define RE_ObjectExtractor_h

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include "HistogramStretcher.h"
#include "../Analyzers/RectangleLocator.h"
#include <QtGui/QColor>

namespace RE {

/*! @brief Object Extraction Filter Class.
 * This class mixes functionnality of various other Analyzers and Filters. It aims to provide an all-in-one
 * helper for cleaning up objects sitting on a large background.
 * Given an input image and a set of parameters, it will, in this order:
 * - Stretches the original image histogram to make edges more visible in a temporary image,
 * - Attempts to locate the object within the specified color boundaries,
 * - Cuts the edges of the original image.
 * @see RE::HistogramStretcher, RE::RectangleLocator
 */
class ObjectExtractor : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)

    //! @brief Preprocessing Stack.
    Q_PROPERTY(RE::T::ZoneFilterData::List
                                filterStack             READ filterStack    WRITE setFilterStack    RESET resetFilterStack)
    //! @see RE::RectangleLocator
    Q_PROPERTY(quint16          locatorStep             READ step           WRITE setStep           RESET resetStep         STORED false)
    //! @see RE::RectangleLocator
    Q_PROPERTY(QColor           locatorLowerLimit       READ lowerLimit     WRITE setLowerLimit     RESET resetLowerLimit   STORED false)
    //! @see RE::RectangleLocator
    Q_PROPERTY(QColor           locatorUpperLimit       READ upperLimit     WRITE setUpperLimit     RESET resetUpperLimit   STORED false)
    //! @see RE::RectangleLocator
    Q_PROPERTY(bool             locatorFromLeft         READ fromLeft       WRITE setFromLeft       RESET resetFromLeft     STORED false)
    //! @see RE::RectangleLocator
    Q_PROPERTY(bool             locatorFromRight        READ fromRight      WRITE setFromRight      RESET resetFromRight    STORED false)
    //! @see RE::RectangleLocator
    Q_PROPERTY(bool             locatorFromTop          READ fromTop        WRITE setFromTop        RESET resetFromTop      STORED false)
    //! @see RE::RectangleLocator
    Q_PROPERTY(bool             locatorFromBottom       READ fromBottom     WRITE setFromBottom     RESET resetFromBottom   STORED false)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE						ObjectExtractor         (QObject *p=0);

    // Preprocessing stack.
    RE::T::ZoneFilterData::List const&
                                    filterStack             () const                                { return _filterStack; }
    void                            setFilterStack          (RE::T::ZoneFilterData::List const &v)  { _filterStack = v; }
    void                            resetFilterStack        ()                                      { _filterStack = RE::T::ZoneFilterData::List(); }
    // Step accessors.
    quint16 const&                  step                    () const                            { return _locator.step(); }
    void                            setStep                 (quint16 const &v)                  { _locator.setStep(v); }
    void                            resetStep               ()                                  { _locator.resetStep(); }
    // Limits accessors.
    QColor const&                   lowerLimit              () const                            { return _locator.lowerLimit(); }
    void							setLowerLimit           (QColor const &v)                   { _locator.setLowerLimit(v); }
    void                            resetLowerLimit         ()                                  { _locator.resetLowerLimit(); }
    QColor const&                   upperLimit              () const                            { return _locator.upperLimit(); }
    void							setUpperLimit           (QColor const &v)                   { _locator.setUpperLimit(v); }
    void                            resetUpperLimit         ()                                  { _locator.resetUpperLimit(); }
    // Direction flags setters.
    bool                            fromLeft                () const                            { return _locator.fromLeft(); }
    void                            setFromLeft             (bool v)                            { _locator.setFromLeft(v); }
    void                            resetFromLeft           ()                                  { _locator.resetFromLeft(); }
    bool                            fromRight               () const                            { return _locator.fromRight(); }
    void                            setFromRight            (bool v)                            { _locator.setFromRight(v); }
    void                            resetFromRight          ()                                  { _locator.resetFromRight(); }
    bool                            fromTop                 () const                            { return _locator.fromTop(); }
    void                            setFromTop              (bool v)                            { _locator.setFromTop(v); }
    void                            resetFromTop            ()                                  { _locator.resetFromTop(); }
    bool                            fromBottom              () const                            { return _locator.fromBottom(); }
    void                            setFromBottom           (bool v)                            { _locator.setFromBottom(v); }
    void                            resetFromBottom         ()                                  { _locator.resetFromBottom(); }

    inline static QImage			Apply					(quint16 const &st, QColor const &ll, QColor const &ul, bool fl, bool fr, bool ft, bool fb, QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        ObjectExtractor fltr;
        fltr.setStep                    (st);
        fltr.setLowerLimit              (ll);
        fltr.setUpperLimit              (ul);
        fltr.setFromLeft                (fl);
        fltr.setFromRight               (fr);
        fltr.setFromTop                 (ft);
        fltr.setFromBottom              (fb);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage					doApply					();
    virtual QSize					minimumImageSize		() const;

    template<typename K>
    QImage                          algo                    ();

private:
    RE::T::ZoneFilterData::List     _filterStack;

    RE::RectangleLocator            _locator;
};

}

#endif
