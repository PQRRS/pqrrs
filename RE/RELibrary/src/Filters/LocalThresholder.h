#ifndef RE_LocalThresholder_h
#define RE_LocalThresholder_h

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include <QtGui/QColor>

namespace RE {

/*! @brief Local Thresholding Filter Class.
 * @todo Documentation. */
class RELibOpt LocalThresholder : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    //! @brief Framing Fill Color Property.
    Q_PROPERTY(RE::T::FillColor		fillColor			READ fillColor			WRITE setFillColor			RESET resetFillColor)
    //! @brief Horizontal Radius Property.
    Q_PROPERTY(quint32				horizontalRadius	READ horizontalRadius	WRITE sethorizontalRadius	RESET resethorizontalRadius)
    //! @brief Vertical Radiys Property.
    Q_PROPERTY(quint32				verticalRadius		READ verticalRadius		WRITE setVerticalRadius		RESET resetVerticalRadius)
    //! @brief Delta Threshold Property.
    Q_PROPERTY(qint32				delta				READ delta				WRITE setDelta				RESET resetDelta)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE			LocalThresholder		(QObject *p=0);

    // Accessors.
    T::FillColor		fillColor				() const                { return _fillColor; }
    void				setFillColor			(T::FillColor v)        { _fillColor = v; }
    void				resetFillColor			()						{ _fillColor = T::FillColor(); }
    quint32				horizontalRadius		()	const				{ return _horizontalRadius; }
    void				sethorizontalRadius		(quint32 v)				{ _horizontalRadius = v; }
    void				resethorizontalRadius	()						{ _horizontalRadius = 0; }
    quint32				verticalRadius			()	const				{ return _verticalRadius; }
    void				setVerticalRadius		(quint32 v)				{ _verticalRadius = v; }
    void				resetVerticalRadius		()						{ _verticalRadius = 0; }
    qint32 const&		delta					() const				{ return _delta; }
    void				setDelta				(qint32 const &v)		{ _delta = v; }
    void				resetDelta				()						{ _delta = 0; }

    inline static QImage	Apply				(T::FillColor const &fc, quint32 const &hr, quint32 const &vr, qint32 const &d, QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        LocalThresholder fltr;
        fltr.setFillColor               (fc);
        fltr.sethorizontalRadius        (hr);
        fltr.setVerticalRadius          (vr);
        fltr.setDelta                   (d);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage		doApply                 ();
    virtual QSize		minimumImageSize        () const;

private:
    template<typename K>
    QImage              algo                    ();
    template<typename K>
    bool                parallelizedByteAlgo    (QImage const *src, QImage *dst, qint32 z, qint32 yStart, qint32 yEnd);

private:
    T::FillColor		_fillColor;
    quint32				_horizontalRadius,
                        _verticalRadius;
    qint32				_delta;
};

}

#endif
