#include "OpticalTiltCorrector.h"
#include "TiltCorrector.h"
#include "../Engine.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <tesseract/baseapi.h>

struct OSBestResult {
    int             orientation_id;
    int             script_id;
    float           sconfidence;
    float           oconfidence;
};
struct OSResults {
    float           orientations[4];
    float           scripts_na[4][120];
    UNICHARSET      *unicharset;
    OSBestResult    osBestResult;
    OSResults       ()  { memset(this, 0, sizeof(OSResults)); }
};

RE::OpticalTiltCorrector::OpticalTiltCorrector (QObject *p)
: RE::AbstractFilter(p), _tessApi(0) {
    resetFilterStack        ();
    resetOrientationOnly    ();
}
RE::OpticalTiltCorrector::~OpticalTiltCorrector () {
    destroyApi                          ();
}
void RE::OpticalTiltCorrector::destroyApi () {
    if (_tessApi) {
        _tessApi->End                   ();
        Wrn                                 << "Skipping deletion.";
        // PQR delete                          _tessApi;
        _tessApi                        = 0;
    }
}

QImage RE::OpticalTiltCorrector::doApply () {
    QImage const    src                     = source();

    // No predicate?! Use default.
    if (!_tessApi) {
        Wrn                             << "Loading orientation detection predicate...";
        _tessApi                        = new tesseract::TessBaseAPI();
        if (_tessApi->Init("./", "fra", tesseract::OEM_DEFAULT))
            destroyApi                      ();
    }
    // Recognition cannot start.
    if (!_tessApi) {
        Crt                             << "Unable to initialize recognizer. Aborting recognition.";
        return                          src;
    }
    // Clear API and reset classifier.
    _tessApi->Clear                     ();
    // Change page segmentation to OSD only.
    _tessApi->SetPageSegMode            (tesseract::PSM_AUTO_OSD);

    // Processing takes place here.
    RE::RuntimeContext const
                    &rt                 = runtimeContext();
    QImage          tmp                 = rt.engine->filteredImage(_filterStack, rt.name, src, parallelizedCoresCount());
    if (debuggingEnabled())
        setDebuggingImage               (tmp);

    if (tmp.format()==QImage::Format_Indexed8) {
        tmp                             = RE::Engine::paletizeIfGrayscale(tmp, true);
        _tessApi->SetImage              (tmp.bits(), tmp.width(), tmp.height(), 1, tmp.bytesPerLine());
    }
    else {
        tmp                             = tmp.convertToFormat(QImage::Format_RGB32);
        _tessApi->SetImage              (tmp.bits(), tmp.width(), tmp.height(), tmp.depth()/8, tmp.bytesPerLine());
    }

    // Prepare orientation detection variables.
    tesseract::Orientation      orientation;
    tesseract::WritingDirection direction;
    tesseract::TextlineOrder    order;
    float                       angle;

    // Grab an iterator on the page, after detecting it's orientation.
    tesseract::PageIterator     *it     =  _tessApi->AnalyseLayout();
    it->Orientation                     (&orientation, &direction, &order, &angle);

    // Since layout analysis fails at 180 degrees, run an OSDetect pass and use this orientation.
    OSResults                   osRes;
    _tessApi->DetectOS                  (&osRes);
    orientation                         = (tesseract::Orientation)osRes.osBestResult.orientation_id;
    // All done with tess.
    Wrn                                 << "Orientation:" << orientation << "Order:" << order << "Direction:" << direction << "Angle:" << angle;

    if (_orientationOnly)
        angle                           = 0;
    switch (orientation) {
    case tesseract::ORIENTATION_PAGE_UP:
        angle                           += 0.0f;
        break;
    case tesseract::ORIENTATION_PAGE_RIGHT:
        angle                           += M_PI * 90.0f / 180.0f;
        break;
    case tesseract::ORIENTATION_PAGE_DOWN:
        angle                           += M_PI * 180.0f / 180.0f;
        break;
    case tesseract::ORIENTATION_PAGE_LEFT:
        angle                           += M_PI * 270.0f / 180.0f;
        break;
    }

    _tessApi->Clear                     ();

    if (angle==0.0f)
        return                          src;

    TiltCorrector               tc;
    tc.setAngle                         (angle);
    tc.setMode                          (RE::E::TransformModeSmooth);
    tc.setFraming                       (Qt::white);
    tc.setSource                        (src);
    return tc.apply();
}
QSize RE::OpticalTiltCorrector::minimumImageSize () const {
    return QSize(1, 1);
}
