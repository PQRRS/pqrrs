#ifndef RE_HistogramStretcher_h
#define RE_HistogramStretcher_h

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include <QtGui/QColor>

namespace RE {

/*! @brief Histogram Stretching Filter Class.
 * @todo Documentation. */
class RELibOpt HistogramStretcher : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    //! @brief Low Defaultable Threshold Property.
    Q_PROPERTY(RE::T::GrayValue		lowBoundary		READ lowBoundary	WRITE setLowBoundary    RESET resetLowBoundary)
    //! @brief High Defaultable Threshold Property.
    Q_PROPERTY(RE::T::GrayValue		highBoundary	READ highBoundary	WRITE setHighBoundary   RESET resetHighBoundary)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE						HistogramStretcher		(QObject *p=0);

    // Accessors.
    RE::T::GrayValue const&			lowBoundary				() const					{ return _lowBoundary; }
    void							setLowBoundary			(RE::T::GrayValue const &v)	{ _lowBoundary = v; }
    void                            resetLowBoundary        ()                          { _lowBoundary = 0; }
    RE::T::GrayValue const&			highBoundary			() const					{ return _highBoundary; }
    void							setHighBoundary			(RE::T::GrayValue const &v)	{ _highBoundary = v; }
    void                            resetHighBoundary       ()                          { _highBoundary = 255; }

    inline static QImage			Apply					(T::GrayValue const &lb, T::GrayValue const &hb, QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        HistogramStretcher fltr;
        fltr.setLowBoundary             (lb);
        fltr.setHighBoundary            (hb);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage                  doApply                 ();
    virtual QSize                   minimumImageSize        () const;

private:
    struct ParallelizationParams {
        QImage const                *src;
        QImage                      *dst;
        quint8                      lb;
        quint8                      hb;
        qint32                      z;
        qint32                      yStart;
        qint32                      yEnd;
    };
    template<typename K>
    QImage                          algo                    ();
    template<typename K>
    bool                            parallelizedByteAlgo    (ParallelizationParams p);

private:
    RE::T::GrayValue				_lowBoundary,
                                    _highBoundary;
};

}

#endif
