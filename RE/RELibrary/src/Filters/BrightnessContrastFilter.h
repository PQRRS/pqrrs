#ifndef RE_BrightnessContrastFilter_h
#define RE_BrightnessContrastFilter_h

// Base class.
#include "AbstractFilter.h"

namespace RE {

/*! @brief Image Brightness / Contrast Filter Class.
 * @todo Documentation. */
class RELibOpt BrightnessContrastFilter : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    //! @brief Brightness Factor Property.
    Q_PROPERTY(RE::T::Ratio	brightnessFactor	READ brightnessFactor	WRITE setBrightnessFactor	RESET resetBrightnessFactor)
    //! @brief Contrast Factor Property.
    Q_PROPERTY(RE::T::Ratio	contrastFactor		READ contrastFactor		WRITE setContrastFactor		RESET resetContrastFactor)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE				BrightnessContrastFilter	(QObject *p=0);

    // Accessors.
    T::Ratio				brightnessFactor			() const					{ return _brightnessFactor; }
    void					setBrightnessFactor			(T::Ratio const &v)			{ _brightnessFactor = v; }
    void					resetBrightnessFactor		()							{ _brightnessFactor = .0; }
    T::Ratio				contrastFactor				() const					{ return _contrastFactor; }
    void					setContrastFactor			(T::Ratio const &v)			{ _contrastFactor = v; }
    void					resetContrastFactor			()							{ _contrastFactor = .0; }

    // Static applier.
    inline static QImage	Apply						(double const &br, double const &ct, QImage const &src, quint32 const &pcc, bool const &fgs=false)	{
        BrightnessContrastFilter fltr;
        fltr.setBrightnessFactor        (br);
        fltr.setContrastFactor          (ct);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage			doApply                     ();
    virtual QSize			minimumImageSize            () const;

private:
    template<typename K>
    QImage                  algo                        ();
    template<typename K>
    bool                    parallelizedByteAlgo        (QImage const *src, QImage *dst, qint32 z, qint32 yStart, qint32 yEnd);

private:
    double					_brightnessFactor;
    double					_contrastFactor;

};

}

#endif
