#ifndef RE_RibbonRemover_h
#define RE_RibbonRemover_h

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include "../Filters/HistogramStretcher.h"
#include "../Analyzers/RectangleLocator.h"
#include <QtGui/QColor>

namespace RE {

class RELibOpt RibbonRemover : public AbstractFilter {
Q_OBJECT
Q_INTERFACES(RE::AbstractFilter)
// Stretcher properties.
Q_PROPERTY(RE::T::GrayValue		lowBoundary		READ lowBoundary	WRITE setLowBoundary    RESET resetLowBoundary)
Q_PROPERTY(RE::T::GrayValue		highBoundary	READ highBoundary	WRITE setHighBoundary   RESET resetHighBoundary)
// Locator properties.
Q_PROPERTY(quint16  step                        READ step                   WRITE setStep               RESET resetStep)
Q_PROPERTY(QColor   lowerLimit                  READ lowerLimit             WRITE setLowerLimit         RESET resetLowerLimit)
Q_PROPERTY(QColor   upperLimit                  READ upperLimit             WRITE setUpperLimit         RESET resetUpperLimit)
Q_PROPERTY(bool     fromLeft                    READ fromLeft               WRITE setFromLeft           RESET resetFromLeft)
Q_PROPERTY(bool     fromRight                   READ fromRight              WRITE setFromRight          RESET resetFromRight)
Q_PROPERTY(bool     fromTop                     READ fromTop                WRITE setFromTop            RESET resetFromTop)
Q_PROPERTY(bool     fromBottom                  READ fromBottom             WRITE setFromBottom         RESET resetFromBottom)

public:
    // Constructor.
    Q_INVOKABLE						RibbonRemover           (QObject *p=0);

    // Stretcher properties.
    RE::T::GrayValue const&			lowBoundary				() const                            { return _stretcher.lowBoundary(); }
    void							setLowBoundary			(RE::T::GrayValue const &v)         { _stretcher.setLowBoundary(v); }
    void                            resetLowBoundary        ()                                  { _stretcher.resetLowBoundary(); }
    RE::T::GrayValue const&			highBoundary			() const                            { return _stretcher.highBoundary(); }
    void							setHighBoundary			(RE::T::GrayValue const &v)         { _stretcher.setHighBoundary(v); }
    void                            resetHighBoundary       ()                                  { _stretcher.resetHighBoundary(); }
    // Step accessors.
    quint16 const&                  step                    () const                            { return _locator.step(); }
    void                            setStep                 (quint16 const &v)                  { _locator.setStep(v); }
    void                            resetStep               ()                                  { _locator.resetStep(); }
    // Limits accessors.
    QColor const&                   lowerLimit              () const                            { return _locator.lowerLimit(); }
    void							setLowerLimit           (QColor const &v)                   { _locator.setLowerLimit(v); }
    void                            resetLowerLimit         ()                                  { _locator.resetLowerLimit(); }
    QColor const&                   upperLimit              () const                            { return _locator.upperLimit(); }
    void							setUpperLimit           (QColor const &v)                   { _locator.setUpperLimit(v); }
    void                            resetUpperLimit         ()                                  { _locator.resetUpperLimit(); }
    // Direction flags setters.
    bool                            fromLeft                () const                            { return _locator.fromLeft(); }
    void                            setFromLeft             (bool v)                            { _locator.setFromLeft(v); }
    void                            resetFromLeft           ()                                  { _locator.resetFromLeft(); }
    bool                            fromRight               () const                            { return _locator.fromRight(); }
    void                            setFromRight            (bool v)                            { _locator.setFromRight(v); }
    void                            resetFromRight          ()                                  { _locator.resetFromRight(); }
    bool                            fromTop                 () const                            { return _locator.fromTop(); }
    void                            setFromTop              (bool v)                            { _locator.setFromTop(v); }
    void                            resetFromTop            ()                                  { _locator.resetFromTop(); }
    bool                            fromBottom              () const                            { return _locator.fromBottom(); }
    void                            setFromBottom           (bool v)                            { _locator.setFromBottom(v); }
    void                            resetFromBottom         ()                                  { _locator.resetFromBottom(); }

    inline static QImage			Apply					(quint16 const &st,
                                                             QColor const &ll, QColor const &ul,
                                                             bool fl, bool fr, bool ft, bool fb,
                                                             QImage const &src) {
        RibbonRemover eq;
        eq.setSource(src);
        eq.setStep(st);
        eq.setLowerLimit(ll); eq.setUpperLimit(ul);
        eq.setFromLeft(fl); eq.setFromRight(fr);
        eq.setFromTop(ft); eq.setFromBottom(fb);
        return eq.apply();
    }

protected:
    virtual QImage					doApply					();
    virtual QSize					minimumImageSize		() const;
    QImage                          apply                   ();

    template <class K>
    QImage                          algo                    ();

private:
    HistogramStretcher              _stretcher;
    RectangleLocator                _locator;
};

}

#endif
