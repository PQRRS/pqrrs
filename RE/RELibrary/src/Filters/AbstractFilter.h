#ifndef RE_AbstractFilter_h
#define RE_AbstractFilter_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include "../RETypes.h"
#include <QtGui/QImage>
// Forward declarations.
namespace RE { class Engine; }

namespace RE {

/*! @brief Base Image Filtering Class.
 * This is the base class for any image filter, as it makes the process of creating an image analyzis object simpler while normalizing IOs.
 * The main processing logic will be executed by calling RE::AbstractFilter::apply() from the outside, internally checking for problems with the image
 * and then calling the RE::AbstractFilter::doApply() method.
 * Also, as any filter / analyzer / recognizer accepts QImage as it's main input, it's common practice to
 * relocate complex computation in a templatized method (Eg. template<typename K> bool algo()), and then derive
 * processing for chunks of images when possible according to the parallelizedCoresCount property (Eg. in a template<typename K>
bool parallelizedByteAlgo (QImage const *src, qint32 z, qint32 yStart, qint32 yEnd) synchronized by QFuture-like helpers).
 * @see RE::BrightnessContrastFilter::apply(), RE::BrightnessContrastFilter::algo(), RE::BrightnessContrastFilter::parallelizedByteAlgo()
 * Those methods would be called on different threads based on the requested cores count. */
class RELibOpt AbstractFilter : public QObject {
public:
    Q_OBJECT
    /*! @brief Useable Parallelization Physical Cores Property.
     * As the image processing takes place in the inherited class, there is no guarantee that this property
     * will induce the same result on different analyzers, however using existing analyzer source code as a foundation
     * when creating a new one will ensure coherence is respected. */
    Q_PROPERTY(quint32  parallelizedCoresCount  READ parallelizedCoresCount WRITE setParallelizedCoresCount RESET resetParallelizedCoresCount)
    //! @brief Source Image Property.
    Q_PROPERTY(QImage   source                  READ source                 WRITE setSource                 RESET resetSource)
    //! @brief Grayscale Image Preconversion Property.
    Q_PROPERTY(bool     forceGrayscale          READ forceGrayscale         WRITE setForceGrayscale         RESET resetForceGrayscale)
    // Debugging output generation properties.
    Q_PROPERTY(bool     debuggingEnabled        READ debuggingEnabled       WRITE setDebuggingEnabled       RESET resetDebuggingEnabled)
    Q_PROPERTY(QImage   debuggingImage          READ debuggingImage)

protected:
    //! @brief Default Constructor.
    Q_INVOKABLE             AbstractFilter              (QObject *p=0);

public:
    // Engine setter / getter.
    RE::RuntimeContext const&
                            runtimeContext              () const                { return _runtimeContext; }
    void                    setRuntimeContext           (RE::RuntimeContext const &v) { _runtimeContext = v; }
    // Accessors.
    quint32 const&          parallelizedCoresCount      () const            { return _parallelizedCoresCount; }
    void                    setParallelizedCoresCount   (quint32 const &v)  { _parallelizedCoresCount = v; }
    void                    resetParallelizedCoresCount ()                  { _parallelizedCoresCount = 1; }
    virtual QImage const&	source                      () const			{ return _source; }
    virtual void			setSource                   (QImage const &v)	{ _source = v; }
    virtual void			resetSource                 ()					{ _source = _debuggingImage = QImage(); }
    virtual bool const&     forceGrayscale              () const            { return _forceGrayscale; }
    virtual void            setForceGrayscale           (bool const &v)     { _forceGrayscale = v; }
    virtual void            resetForceGrayscale         ()                  { _forceGrayscale = false; }
    // Debug output accessors.
    virtual bool const&     debuggingEnabled            () const            { return _debuggingEnabled; }
    virtual void            setDebuggingEnabled         (bool const &v)     { _debuggingEnabled = v; }
    virtual void            resetDebuggingEnabled       ()                  { _debuggingEnabled = false; }
    virtual QImage const&   debuggingImage              () const            { return _debuggingImage; }
    // Run the filter.
    virtual QImage			apply                       ();

protected:
    // Debugging image setter (Only callable by subclasses to avoid API confusion).
    virtual void            setDebuggingImage           (QImage const &v)   { _debuggingImage = _debuggingEnabled ? v : QImage(); }
    // Implementable methods.
    virtual QImage			doApply                     () = 0;
    virtual QSize			minimumImageSize            () const = 0;

private:
    RE::RuntimeContext      _runtimeContext;
    quint32                 _parallelizedCoresCount;
    QImage					_source;
    bool                    _forceGrayscale;
    bool                    _debuggingEnabled;
    QImage                  _debuggingImage;
};

}

Q_DECLARE_INTERFACE(RE::AbstractFilter, "fr.PierreQR.RecognitionSuite.RELibrary.AbstractRecognizer/1.0")

#endif
