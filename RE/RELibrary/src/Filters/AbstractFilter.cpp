#include "AbstractFilter.h"
#include "../Engine.h"
#include <QtCore/QDebug>

RE::AbstractFilter::AbstractFilter (QObject *p)
: QObject(p),
_runtimeContext("", 0) {
    resetParallelizedCoresCount ();
    resetSource                 ();
    resetForceGrayscale         ();
    resetDebuggingEnabled       ();
}

QImage RE::AbstractFilter::apply () {
	if (_source.isNull()) {
		Crt << "Invalid source image!";
		return _source;
	}
	// Source is invalid... Just return.
    QSize const imgSize = _source.size();
    QSize const minSize = minimumImageSize();
	// Check that the requested working area is within minimum filter range.
	if (imgSize.width()<minSize.width() || imgSize.height()<minSize.height()) {
        Crt << "Bad area - Requested:" << imgSize.width() << "x" << imgSize.height() << ", Required:" << minSize.width() << "x" << minSize.height() << "!";
		return		_source;
	}
    else if (_forceGrayscale)
        _source             = RE::Engine::paletizeIfGrayscale(_source, true);
    return doApply          ();
}
