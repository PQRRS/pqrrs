#ifndef RE_TiltCorrector_h
#define RE_TiltCorrector_h

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include "HistogramStretcher.h"
// Forward declarations.
namespace RE { class Engine; }

namespace RE {

/*! @brief Object Tilt Correction Filter Class.
 * This object attempts to determine a viable angle on which the object in the image is tilted.
 * Also, the filter will try to suppress borders induced by image rotation.
 * The filter behaves as follow:
 * - Stretches the image original histogram into a temporary image,
 * - Iterates from top-left corner of the image to find an edge all along the border, with increments of 20 pixels horizontally every time.
 * - Makes an average of all found angles.
 * - Rotates the image.
 * - Tries to replace the induced border with the color specified in the framing property.
 * @see RE::RectangleLocator */
class RELibOpt TiltCorrector : public RE::AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    /*! @brief Rotation Angle Property (Rad.). */
    Q_PROPERTY(double               angle                   READ angle          WRITE setAngle          RESET resetAngle)
    /*! @brief Rotation Quality Mode.
     * @see RE::E::TransformMode */
    Q_PROPERTY(RE::E::TransformMode mode                    READ mode           WRITE setMode           RESET resetMode)
    //! Framing Induction Color Property.
    Q_PROPERTY(QColor               framing                 READ framing        WRITE setFraming        RESET resetFraming)
    //! Pre-Processing Histogram Stretcher Low Boundary Property.
    Q_PROPERTY(RE::T::GrayValue     stretchingLowBoundary   READ lowBoundary	WRITE setLowBoundary    RESET resetLowBoundary  STORED false)
    //! Pre-Processing Histogram Stretcher High Boundary Property.
    Q_PROPERTY(RE::T::GrayValue     stretchingHighBoundary  READ highBoundary	WRITE setHighBoundary   RESET resetHighBoundary STORED false)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE						TiltCorrector           (QObject *p=0);

    // Rotation accessors.
    double const&                   angle                   () const                            { return _angle; }
    void                            setAngle                (double const &v)                   { _angle = v; }
    void                            resetAngle              ()                                  { _angle = 0.0; }
    RE::E::TransformMode            mode                    () const                            { return _mode; }
    void                            setMode                 (RE::E::TransformMode v)            { _mode = v; }
    void                            resetMode               ()                                  { _mode = RE::E::TransformModeSmooth; }
    QColor const&                   framing                 () const                            { return _framing; }
    void                            setFraming              (QColor const &v)                   { _framing = v; }
    void                            resetFraming            ()                                  { _framing = Qt::white; }
    RE::T::GrayValue const&			lowBoundary				() const                            { return _stretcher.lowBoundary(); }
    void							setLowBoundary			(RE::T::GrayValue const &v)         { _stretcher.setLowBoundary(v); }
    void                            resetLowBoundary        ()                                  { _stretcher.resetLowBoundary(); }
    RE::T::GrayValue const&			highBoundary			() const                            { return _stretcher.highBoundary(); }
    void							setHighBoundary			(RE::T::GrayValue const &v)         { _stretcher.setHighBoundary(v); }
    void                            resetHighBoundary       ()                                  { _stretcher.resetHighBoundary(); }

    inline static QImage			Apply					(double const &a, RE::E::TransformMode const &m, QColor const &f,
                                                             RE::T::GrayValue const &lb, RE::T::GrayValue const &hb,
                                                             QImage const &src, quint32 const &pcc, bool fgs=false) {
        TiltCorrector fltr;
        fltr.setAngle                   (a);
        fltr.setMode                    (m);
        fltr.setFraming                 (f);
        fltr.setLowBoundary             (lb);
        fltr.setHighBoundary            (hb);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage					doApply					();
    virtual QSize					minimumImageSize		() const;

    template<typename K>
    QImage                          algo                    ();

private:
    double                          _angle;
    RE::E::TransformMode            _mode;
    QColor                          _framing;
    RE::HistogramStretcher          _stretcher;
};

}

#endif
