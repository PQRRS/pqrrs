#ifndef RE_HistogramEqualizer_h
#define RE_HistogramEqualizer_h

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include <QtGui/QColor>

namespace RE {

/*! @brief Histogram Equalizer Filter Class.
 * @todo Documentation. */
class RELibOpt HistogramEqualizer : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE						HistogramEqualizer      (QObject *p=0);

    inline static QImage			Apply					(QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        HistogramEqualizer  fltr;
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage                  doApply                 ();
    virtual QSize                   minimumImageSize        () const;

private:
    struct ParallelizationParams {
        QImage const                *src;
        QImage                      *dst;
        quint8 const                *eqhData;
        qint32                      z;
        qint32                      yStart;
        qint32                      yEnd;
    };
    template<typename K>
    QImage                          algo                    ();
    template<typename K>
    bool                            parallelizedByteAlgo    (ParallelizationParams p);
};

}

#endif
