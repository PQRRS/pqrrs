#include "ColorsRemover.h"
#include "../Engine.h"
#include "../PixelHelper.hpp"
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#include <math.h>

RE::ColorsRemover::ColorsRemover (QObject *p)
: AbstractFilter(p) {
    resetTargetColor        ();
    resetReplacementColor   ();
    resetDeltaThreshold     ();
}

QImage RE::ColorsRemover::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize RE::ColorsRemover::minimumImageSize () const {
    return QSize(1, 1);
}

template<typename K> QImage RE::ColorsRemover::algo () {
    // Prepare source pointers.
    QImage const            *src    = &source();
    PixelReader<K> const    pr      (src);
    // Prepare some helper constants.
    K const                 repl    = pr.isMultiplane ? _replacementColor.rgba() : qGray(_replacementColor.rgba());
    // Prepare destination pointers.
    QImage                  dst     (src->size(), src->format());
    dst.setColorTable               (src->colorTable());
    dst.setColorCount               (src->colorCount());
    if (pr.isMultiplane)
        dst.fill                    (repl);
    else
        dst.fill                    (repl);
    // Parallelized run!
    qint32 const            pcc     = parallelizedCoresCount();
    if (pcc==1)
        parallelizedByteAlgo<K>     (src, &dst, 0, src->height());
    else {
        QFutureSynchronizer<bool>
                            tasks;
        double const        step    = (double)src->height()/(double)pcc;
        for (qint32 i=0; i<pcc; ++i)
            tasks.addFuture         (QtConcurrent::run<bool>(this, &RE::ColorsRemover::parallelizedByteAlgo<K>, src, &dst, step*i, step*(i+1)));
        tasks.waitForFinished       ();
    }
    // Might-set the debugging output.
    setDebuggingImage               (dst);
    return                          dst;
}
template<typename K> bool RE::ColorsRemover::parallelizedByteAlgo (QImage const *src, QImage *dst, qint32 yStart, qint32 yEnd) {
    // Prepare source pointers.
    PixelReader<K> const    pr      (src);
    PixelWriter<K>          pw      (dst);
    // Prepare some helper constants.
    K const                 target  = pr.isMultiplane ? _targetColor.rgba() : qGray(_targetColor.rgba());
    qint32                  rOffset, wOffset;
    for (qint32 y=yStart; y<yEnd; ++y) {
        rOffset                     = pr.valueOffset(y);
        wOffset                     = pw.valueOffset(y);
        for (qint32 x=0; x<pr.width; ++x) {
            // Split source pixel components.
            K const         &pixel      = pr.value(x+rOffset);
            if (PixelReader<K>::colorDistance(pixel, target)<=_deltaThreshold)
               pw.setValue              (x+wOffset,  pixel);
        }
    }
    return true;
}
