#include "BrightnessContrastFilter.h"
#include "../RETypes.h"
#include "../Engine.h"
#include "../PixelHelper.hpp"
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#define _USE_MATH_DEFINES
#include <math.h>

RE::BrightnessContrastFilter::BrightnessContrastFilter (QObject *p)
: RE::AbstractFilter(p) {
    resetBrightnessFactor   ();
    resetContrastFactor     ();
}
QImage RE::BrightnessContrastFilter::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize RE::BrightnessContrastFilter::minimumImageSize () const {
	return QSize(1, 1);
}

template<typename K> QImage RE::BrightnessContrastFilter::algo () {
    if (_brightnessFactor==0.0f && _contrastFactor==0.0f)
        return                      source();
    // Prepare source pointers.
    QImage const            *src    = &source();
    PixelReader<K> const    pr      (src);
    // Prepare destination pointers.
    QImage                  dst     (src->size(), src->format());
    dst.setColorTable               (src->colorTable());
    dst.setColorCount               (src->colorCount());
    dst.fill                        (Qt::white);
    // Parallelized run!
    qint32 const            pcc     = parallelizedCoresCount();
    if (pcc==1)
        for (qint32 z=pr.firstColorPlane; z<=pr.lastColorPlane; ++z)
            parallelizedByteAlgo<K> (src, &dst, z, 0, src->height());
    else {
        QFutureSynchronizer<bool>
                            tasks;
        double const        step    = (double)src->height()/(double)pcc;
        for (qint32 z=pr.firstColorPlane; z<=pr.lastColorPlane; ++z)
            for (qint32 i=0; i<pcc; ++i)
                tasks.addFuture     (QtConcurrent::run<bool>(this, &RE::BrightnessContrastFilter::parallelizedByteAlgo<K>, src, &dst, z, step*i, step*(i+1)));
        tasks.waitForFinished       ();
    }
    // Might-set the debugging output.
    setDebuggingImage               (dst);
    return                          dst;
}
template<typename K>
bool RE::BrightnessContrastFilter::parallelizedByteAlgo (QImage const *src, QImage *dst, qint32 z, qint32 yStart, qint32 yEnd) {
    // Prepare source pointers.
    PixelReader<K> const    pr      (src);
    PixelWriter<K>          pw      (dst);
    // Prepare some constant helpers.
    double const            cbCoef  = 1.0+_brightnessFactor;
    double const            ccCoef  = tan((1.0+_contrastFactor)*M_PI/4.0);
    double                  pixel;
    qint32                  rOffset, wOffset;
    for (qint32 y=yStart; y<yEnd; ++y) {
        rOffset                 = pr.byteOffset(y, z);
        wOffset                 = pw.byteOffset(y, z);
        for (qint32 x=0; x<pr.width; ++x) {
            pixel				= pr.byte(x*pr.bpc+rOffset);
            pixel				= _brightnessFactor<0.0 ? pixel*cbCoef : pixel + (255-pixel)*_brightnessFactor;
            pw.setByte          (x*pw.bpc+wOffset, qBound(0.0, (pixel-127.0) * ccCoef + 127.0, 255.0));
        }
    }
    return true;
}
