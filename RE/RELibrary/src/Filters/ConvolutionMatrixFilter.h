#ifndef RE_ConvolutionMatrixFilter_hpp
#define RE_ConvolutionMatrixFilter_hpp

// Base class.
#include "AbstractFilter.h"
// Required stuff.
#include "../RETypes.h"
#include "../Matrix.hpp"
#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtCore/QDataStream>
#include <QtGui/QColor>

namespace RE {

/*! @brief Convolution Matrix Filter Class.
 * @todo Documentation. */
class RELibOpt ConvolutionMatrixFilter : public AbstractFilter {
public:
    Q_OBJECT
    Q_INTERFACES(RE::AbstractFilter)
    //! @brief Framing Fill Color Property.
    Q_PROPERTY	(RE::T::FillColor	fillColor	READ fillColor	WRITE setFillColor	RESET resetFillColor)
    //! @brief Matrices Property.
    Q_PROPERTY	(RE::T::CVMatrices  matrices	READ matrices	WRITE setMatrices	RESET resetMatrices)

public:
    //! @see RE::AbstractFilter::AbstractFilter()
    Q_INVOKABLE					ConvolutionMatrixFilter	(QObject *p=0);

    // Accessors.
    RE::T::FillColor const		fillColor				() const                        { return _fillColor; }
    void						setFillColor			(RE::T::FillColor v)            { _fillColor = v; }
    void						resetFillColor			()                              { _fillColor = RE::T::FillColor(); }
    RE::T::CVMatrices			const&	matrices		()	const                       { return _matrices; }
    void						setMatrices				(RE::T::CVMatrices const &v)	{ _matrices = v; }
    void						resetMatrices			()                              { _matrices.clear(); }

    // Static applier with array of matrices.
    inline static QImage		Apply					(RE::T::CVMatrices const &m, QImage const &src, bool const &fgs=false) {
        ConvolutionMatrixFilter fltr;
        fltr.setMatrices        (m);
        fltr.setSource          (src);
        fltr.setForceGrayscale  (fgs);
        return fltr.apply       ();
    }
    // Static applier with single of matrix.
    inline static QImage		Apply					(RE::T::CVMatrix const &m, QImage const &src, quint32 const &pcc, bool const &fgs=false) {
        RE::T::CVMatrices ms;
        ms                      << m;
        ConvolutionMatrixFilter fltr;
        fltr.setMatrices                (ms);
        fltr.setSource                  (src);
        fltr.setParallelizedCoresCount  (pcc);
        fltr.setForceGrayscale          (fgs);
        return fltr.apply               ();
    }
    // Static applier with two of matrices.
    inline static QImage		Apply					(RE::T::CVMatrix const &m1, RE::T::CVMatrix const &m2, QImage const &src, bool const &fgs=false) {
        RE::T::CVMatrices ms;
        ms                      << m1
                                << m2;
        return Apply            (ms, src, fgs);
    }
    // Static applier with two of matrices.
    inline static QImage		Apply					(RE::T::CVMatrix const &m1, RE::T::CVMatrix const &m2, RE::T::CVMatrix const &m3, QImage const &src, bool const &fgs=false) {
        RE::T::CVMatrices ms;
        ms                      << m1
                                << m2
                                << m3;
        return Apply(           ms, src, fgs);
    }

protected:
    //! @see RE::AbstractFilter::doApply()
    virtual QImage				doApply					();
    virtual QSize				minimumImageSize		() const;

private:
    RE::T::FillColor			_fillColor;
    RE::T::CVMatrices			_matrices;

public:
    #define P99(...) __VA_ARGS__
    #define DECLARE_SINGLE_CVM(name, width, height, data)				\
    class name : public RE::T::CVMatrix {								\
    public:																\
        name () : RE::T::CVMatrix() {									\
            double d1[] = data;											\
            *this << RE::T::CVMatrixElement(QSize(width, height), d1);	\
            _mtId	= qMetaTypeId<RE::ConvolutionMatrixFilter::name>();	\
        }																\
        virtual ~name () {}                                             \
    }
    #define DECLARE_DOUBLE_CVM(name, width, height, data)				\
    class name : public RE::T::CVMatrix {								\
    public:																\
        name () : RE::T::CVMatrix() {									\
            double d1[] = data;											\
            *this << RE::T::CVMatrixElement(QSize(width, height), d1)	\
                  << RE::T::CVMatrixElement(QSize(height, width), d1);	\
            _mtId	= qMetaTypeId<RE::ConvolutionMatrixFilter::name>();	\
        }																\
        virtual ~name() {}                                              \
    }
    // Declare all matrices.
    DECLARE_SINGLE_CVM(HomemadeSpecial, 3, 3, P99({
        -1, -2, -1,
        +0, +3, +0,
        +1, +2, +1 }));

    DECLARE_SINGLE_CVM(Laplacian1_9x1, 3, 3, P99({
        +0, -1, +0,
        -1, +4, -1,
        +0, -1, +0 }));
    DECLARE_SINGLE_CVM(Laplacian2_9x1, 3, 3, P99({
        -1, -1, -1,
        -1, +8, -1,
        -1, -1, -1 }));
    DECLARE_SINGLE_CVM(Laplacian_25x1, 5, 5, P99({
        +0, +0,  -1, +0, +0,
        +1, -1,  -2, -1, +0,
        -1, -2, +17, -2, -1,
        +1, -1,  -2, -1, +0,
        +1, +0,  -1, +0, +0 }));

    DECLARE_SINGLE_CVM(SobelHorizontal_9x1, 3, 3, P99({
        -2, -4, -2,
        +0, +0, +0,
        +2, +4, +2 }));
    DECLARE_SINGLE_CVM(SobelVertical_9x1, 3, 3, P99({
        -2, +0, +2,
        -4, +0, +4,
        -2, +0, +2 }));

    DECLARE_SINGLE_CVM(LineDetectionHorizontal_9x1, 3, 3, P99({
        -1, -1, -1,
        +2, +2, +2,
        -1, -1, -1 }));
    DECLARE_SINGLE_CVM(LineDetectionHorizontal_15x1, 5, 3, P99({
        -1, -1, -1, -1, -1,
        +2, +2, +2, +2, +2,
        -1, -1, -1, -1, -1 }));
    DECLARE_SINGLE_CVM(LineDetectionVertical_9x1, 3, 3, P99({
        -1, +2, -1,
        -1, +2, -1,
        -1, +2, -1 }));
    DECLARE_SINGLE_CVM(LineDetectionVertical_15x1, 3, 5, P99({
        -1, +2, -1,
        -1, +2, -1,
        -1, +2, -1,
        -1, +2, -1,
        -1, +2, -1 }));

    DECLARE_SINGLE_CVM(PointSpread_9x1, 3, 3, P99({
        -0.627, +0.352, -0.627,
        +0.352, +2.923, +0.352,
        -0.627, +0.352, -0.627 }));

    DECLARE_SINGLE_CVM(Sharpen1_9x1, 3, 3, P99({
        +0,  -2,  +0,
        -2,  +11, -2,
        +0,  -2,  +0 }));
    DECLARE_SINGLE_CVM(Sharpen2_9x1, 3, 3, P99({
        +0.00, -0.25, +0.00,
        -0.25, +2.00, -0.25,
        +0.00, -0.25, +0.00 }));
    DECLARE_SINGLE_CVM(Sharpen3_9x1, 3, 3, P99({
        -0.25, -0.25, -0.25,
        -0.25, +3.00, -0.25,
        -0.25, -0.25, -0.25 }));
    DECLARE_SINGLE_CVM(SharpenHighPass_9x1, 3, 3, P99({
        -1, -1, -1,
        -1, +9, -1,
        -1, -1, -1 }));
    DECLARE_SINGLE_CVM(SharpenHighPass_25x1, 5, 5, P99({
        +0, -1,  -1, -1, +0,
        -1, +2,  -4, +2, -1,
        -1, -4, +13, -4, -1,
        -1, +2,  -4, +2, -1,
        +0, -1,  -1, -1, +0 }));

    DECLARE_DOUBLE_CVM(SmoothArithmeticMean_3x2, 3, 1, P99({
        1.0/3.0, 1.0/3.0, 1.0/3.0 }));
    //DECLARE_DOUBLE_CVM(SmoothArithmeticMean_4x2, 4, 1, P99({
    //    1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0 }));
    DECLARE_DOUBLE_CVM(SmoothArithmeticMean_4x2, 3, 1, P99({
            1.0/3.0, 1.0/3.0, 1.0/3.0 }));
    DECLARE_SINGLE_CVM(Smoothing_9x1, 3, 3, P99({
        +1, +2, +1,
        +2, +4, +2,
        +1, +2, +1 }));
    DECLARE_DOUBLE_CVM(Smoothing_5x2, 5, 1, P99({
        +1, +2,  +6, +2, +1 }));

    DECLARE_SINGLE_CVM(Contrast_9x1, 3, 3, P99({
        +0, -1, +0,
        -1, +5, -1,
        +0, -1, +0 }));

    DECLARE_SINGLE_CVM(Emboss_9x1, 3, 3, P99({
        +2, +0, +0,
        +0, -1, +0,
        +0, +0, -1 }));
    DECLARE_SINGLE_CVM(EmbossSubtle_9x1, 3, 3, P99({
        +1, +1, -1,
        +1, +3, -1,
        +1, -1, -1 }));

    DECLARE_SINGLE_CVM(HorizontalBlur_3x1, 3, 1, P99({
        .25, .5, .25 }));
    DECLARE_SINGLE_CVM(VerticalBlur_3x1, 1, 3, P99({
        .25, .5, .25 }));
    DECLARE_DOUBLE_CVM(Blur_3x2, 3, 1, P99({
        .25, .5, .25 }));
    DECLARE_SINGLE_CVM(Blur_25x1, 5, 5, P99({
        +2,  +4,  +5,  +4, +2,
        +4,  +9, +12,  +9, +4,
        +5, +12, +15, +12, +5,
        +4,  +9, +12,  +9, +4,
        +2,  +4,  +5,  +4, +2 }));
    DECLARE_DOUBLE_CVM(Blur_7x2, 7, 1, P99({
        +1, +6, +15, +18, +15, +6, +1 }));

    DECLARE_SINGLE_CVM(EdgesDetection1_9x1, 3, 3, P99({
        +1, +1, +1,
        +1, -7, +1,
        +1, +1, +1}));
    DECLARE_SINGLE_CVM(EdgesDetection2_9x1, 3, 3, P99({
        -5, +0, +0,
        +0, +0, +0,
        +0, +0, +5}));
};

}

// Begin with start marker.
Q_DECLARE_METATYPE(RE::T::CVMatrixStart)
// Register each ready-to-use matrix.
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::HomemadeSpecial)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Laplacian1_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Laplacian2_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Laplacian_25x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::SobelHorizontal_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::SobelVertical_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::LineDetectionHorizontal_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::LineDetectionHorizontal_15x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::LineDetectionVertical_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::LineDetectionVertical_15x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::PointSpread_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Sharpen1_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Sharpen2_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Sharpen3_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::SharpenHighPass_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::SharpenHighPass_25x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::SmoothArithmeticMean_3x2)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::SmoothArithmeticMean_4x2)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Smoothing_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Smoothing_5x2)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Contrast_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Emboss_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::EmbossSubtle_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::HorizontalBlur_3x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::VerticalBlur_3x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Blur_3x2)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Blur_25x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::Blur_7x2)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::EdgesDetection1_9x1)
Q_DECLARE_METATYPE(RE::ConvolutionMatrixFilter::EdgesDetection2_9x1)
// Finish with end marker.
Q_DECLARE_METATYPE(RE::T::CVMatrixEnd)

#endif
