#include "HistogramStretcher.h"
#include "../Engine.h"
#include "../PixelHelper.hpp"
#include "../Analyzers/BasicHistogram.h"
#include <QtCore/QDebug>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureSynchronizer>
#include <math.h>

RE::HistogramStretcher::HistogramStretcher (QObject *p)
: AbstractFilter(p) {
    resetLowBoundary    ();
    resetHighBoundary   ();
}

QImage RE::HistogramStretcher::doApply () {
    QImage::Format  fmt     = source().format();
    switch (fmt) {
        case QImage::Format_Indexed8:
            return algo<quint8>();
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
            return algo<quint32>();
        default:
            Crt         << "Bad image format:" << fmt << ". Please report this message to the product's support group, providing your source project and picture when available.";
            return      source();
    }
}
QSize RE::HistogramStretcher::minimumImageSize () const {
	return QSize(1, 1);
}

template<typename K>
QImage RE::HistogramStretcher::algo () {
    // Prepare source pointers.
    QImage const            *src    = &source();
    PixelReader<K> const    pr      (src);
    // Prepare destination pointers.
    QImage                  dst     (src->size(), src->format());
    dst.setColorTable               (src->colorTable());
    dst.setColorCount               (src->colorCount());
    dst.fill                        (Qt::white);
    // Prepare common histogram object.
    BasicHistogram          bh;
    quint32 const           *bhData;
    bh.setSource                    (src);
    // Parallelized run!
    qint32 const            pcc     = parallelizedCoresCount();
    double const            step    = (double)src->height()/(double)pcc;
    QFutureSynchronizer<bool>
                            tasks;
    for (qint32 z=pr.firstColorPlane; z<=pr.lastColorPlane; ++z) {
        // Compute current plane's histogram.
        bh.setPlane                 (z);
        bh.run                      ();
        bhData                      = bh.data().constData();
        // Prepare lower bound.
        qint16				lb;
        for (lb=_lowBoundary.value(); lb<256; ++lb)
            if (bhData[lb]!=0)
                break;
        // Prepare higher bound.
        qint16				hb      = _highBoundary.value();
        for (hb=_highBoundary.value(); hb>0 && hb>lb; --hb)
            if (bhData[hb]!=0)
                break;
        // Ensure bounds.
        if (lb>=hb) {
            for (qint32 i=0; i<256; ++i)
                if (bhData[i]!=0) {
                    lb                  = i;
                    break;
                }
            for (qint32 i=255; i>=0; --i)
                if (bhData[i]!=0) {
                    hb                  = i;
                    break;
                }
        }
        if (pcc==1) {
            ParallelizationParams p;
            p.src                   = src;
            p.dst                   = &dst;
            p.lb                    = lb;
            p.hb                    = hb;
            p.z                     = z;
            p.yStart                = 0;
            p.yEnd                  = pr.height;
            parallelizedByteAlgo<K> (p);
        }
        else {
            // Spawn workers.
            for (qint32 i=0; i<pcc; ++i) {
                ParallelizationParams p;
                p.src               = src;
                p.dst               = &dst;
                p.lb                = lb;
                p.hb                = hb;
                p.z                 = z;
                p.yStart            = step*i;
                p.yEnd              = step*(i+1);
                tasks.addFuture     (QtConcurrent::run<bool>(this, &RE::HistogramStretcher::parallelizedByteAlgo<K>, p));
            }
        }
    }
    bool                    allOk   = true;
    if (pcc>1)
        tasks.waitForFinished       ();
    for (quint16 i=0; i<tasks.futures().count(); ++i) {
        if (tasks.futures().at(i).result()==false) {
            allOk                   = false;
            break;
        }
    }
    if (!allOk)
        dst                         = *src;
    // Might-set the debugging output.
    setDebuggingImage               (dst);
    return                          dst;
}
template<typename K>
bool RE::HistogramStretcher::parallelizedByteAlgo (ParallelizationParams p) {
    // Prepare source pointers.
    PixelReader<K> const    pr      (p.src);
    PixelWriter<K>          pw      (p.dst);

    // Prepare some constant helpers.
    qint32                  rOffset, wOffset;
    // Compute normalization constant.
    double				n       = 255.0 / (double)(p.hb-p.lb);
    for (qint32 y=p.yStart; y<p.yEnd; ++y) {
        rOffset                 = pr.byteOffset(y, p.z);
        wOffset                 = pw.byteOffset(y, p.z);
        for (qint32 x=0; x<pr.width; ++x) {
            pw.setByte          (x*pw.bpc+wOffset, qBound(0.0, (pr.byte(x*pr.bpc+rOffset)-p.lb)*n, 255.0));
        }
    }
    return true;
}
