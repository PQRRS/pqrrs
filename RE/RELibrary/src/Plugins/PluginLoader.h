#ifndef RE_PluginLoader_h
#define RE_PluginLoader_h

// Base class.
#include <QtCore/QPluginLoader>
// Other stuff.
#include "../RETypes.h"
#include <QtCore/QList>
#include <QtCore/QStringList>
namespace RE {
	class AbstractPlugin;
}

namespace RE {

/*! @brief The PluginLoader Class.
 * @todo Documentation, Use case.
 */
class RELibOpt PluginLoader : public QPluginLoader {
public:
    Q_OBJECT
    Q_PROPERTY(RE::PluginBaseInfos	baseInfos	READ baseInfos)

public:
	// To allow instanciation via the plugins manager...
	friend class							PluginsManager;
	// Types.
	typedef QList<PluginLoader*>			PointerList;

protected:
	// Constructors.
    /*! @brief Default Constructor.
     * @todo Documentation.
     */
                                            PluginLoader		(QString const &fileName, QObject *parent=0);
	// Destructors.
	virtual									~PluginLoader		();
	
	// Load / Unload.
	bool									load				();
	bool									unload				();

public:
	// Get contained instance.
	AbstractPlugin*							instance			();
	// Accessors.
	RE::PluginBaseInfos const&				baseInfos			() const;
	// Metainfo accessors.
	PluginMetaObjectList const&				filters				() const;
	PluginMetaObjectList const&				analyzers			() const;
	PluginMetaObjectList const&				recognizers			() const;

private:
	// Base infos.
	QString									_fileBaseName;
    PluginBaseInfos                         _baseInfos;
	// Members.
	PluginMetaObjectList					_filters,
											_analyzers,
											_recognizers;
	QStringList								_providedClassNames;
};

}

#endif
