#include "PluginLoader.h"
#include "AbstractPlugin.h"
#include <QtCore/QMetaObject>
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>

// Constructors.
RE::PluginLoader::PluginLoader (QString const &fn, QObject *p)
: QPluginLoader(fn, p), _fileBaseName(QFileInfo(fn).baseName()) {
}
RE::PluginLoader::~PluginLoader () {
	if (isLoaded())
		Wrn						<< "Destructing plugin" << _fileBaseName << "while still loaded!";
}

// Load the plugin.
bool RE::PluginLoader::load () {
	// Already loaded?
	if (isLoaded())				return true;
	// Try to load the library...
	else if (!QPluginLoader::load()) {
		Wrn						<< "Cannot load plugin library" << _fileBaseName << ":" << errorString() << "!";
		return					false;
	}
	// See if plugin contains a AbstractPlugin instance...
	AbstractPlugin*	plg			= instance();
	if (!plg) {
		Wrn						<< "Cannot instanciate plugin" << _fileBaseName << "!";
		return					false;
	}
	Dbg							<< "Plugin" << _fileBaseName << "has been successfully loaded.";
	// Cache metaobject lists.
	_filters					= plg->filters();
	_analyzers					= plg->analyzers();
	_recognizers				= plg->recognizers();
	// Store plugin informations
	Dbg							<< "Loading plugin objects...";
	if (_baseInfos.name.isEmpty()) {
		PluginBaseInfos
                    bi              = plg->baseInfos();
        _baseInfos.name             = bi.name;
        _baseInfos.version          = bi.version;
        _baseInfos.description      = bi.description;
        _baseInfos.enumeratorRanges = bi.enumeratorRanges;
		foreach (QMetaObject const *mo, _filters) {
			Dbg					<< "Filter" << mo->className() << "is provided.";
			_providedClassNames	<< mo->className();
		}
		foreach (QMetaObject const *mo, _analyzers) {
			Dbg					<< "Analyzer" << mo->className() << "is provided.";
			_providedClassNames	<< mo->className();
		}
		foreach (QMetaObject const *mo, _recognizers) {
			Dbg					<< "Recognizer" << mo->className() << "is provided.";
			_providedClassNames	<< mo->className();
		}
	}
	return true;
}
RE::AbstractPlugin* RE::PluginLoader::instance () {
	if (!isLoaded()) {
		Wrn							<< "The plugin" << _fileBaseName << "isn't loaded!";
		return 0;
	}
	RE::AbstractPlugin	*toRet=qobject_cast<RE::AbstractPlugin*>(QPluginLoader::instance());
	if (!toRet)
		Wrn							<< "The root object in library" << _fileBaseName << "failed to cast as a RE::AbstractPlugin object!";
	return							toRet;
}
// Unload the plugin.
bool RE::PluginLoader::unload () {
	// Already unloaded?
	if (!isLoaded())			return true;
	// Reset metainfos.
	_filters.clear				();
	_analyzers.clear			();
	_recognizers.clear			();
    // TODO Try to unload the plugin...
    bool        ok              = true; // PQR QPluginLoader::unload();
    if (!ok)
        Wrn                     << "Unable to unload plugin" << _baseInfos.name << ":" << errorString() << "!";
    return                      ok;
}

// Accessors.
RE::PluginBaseInfos const& RE::PluginLoader::baseInfos () const {
	return _baseInfos;
}
// Metainfo accessors.
RE::PluginMetaObjectList const& RE::PluginLoader::filters () const {
	return _filters;
}
RE::PluginMetaObjectList const& RE::PluginLoader::analyzers () const {
	return _analyzers;
}
RE::PluginMetaObjectList const& RE::PluginLoader::recognizers () const {
	return _recognizers;
}
