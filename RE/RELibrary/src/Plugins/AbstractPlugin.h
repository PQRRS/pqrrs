#ifndef RE_AbstractPlugin_h
#define RE_AbstractPlugin_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include "../RETypes.h"

namespace RE {

/*! @brief Abstract Plugin Class Definition
 * This class defines what needs to be ulfilled by a plugin for it to be properly used by the engine.
 * The expected requirements are implementations of all the pure virtual methods (baseInfos(), filters(), analyzers() and recognizers().
 * @see RE::PluginBaseInfos, RE::PluginMetaObjectList */
class RELibOpt AbstractPlugin : public QObject {
Q_OBJECT
protected:
    //! @brief Default Constructor.
                                    AbstractPlugin	(QObject *p=0);
public:
    /*! @brief Base Plugin Information Object Getter.
     * This method is in charge of returning a Plugin Base Information Object (Descriptor), so the engine can identify this plugin
     * uniquelly and use its exported objects.
     * @see RE::PluginBaseInfos
     * @return A PluginBaseInfos object filled with the plugin's informations. */
	virtual PluginBaseInfos			baseInfos		() const	= 0;
    /*! @brief Filters Exporting Method.
     * This method allows a plugin to export any number of filters.
     * @return A PluginMetaObjectList containing all available filters MetaObjects. */
	virtual PluginMetaObjectList	filters			() const	= 0;
    /*! @brief Filters Exporting Method.
     * This method allows a plugin to export any number of analyzers.
     * @return A PluginMetaObjectList containing all available filters MetaObjects. */
    virtual PluginMetaObjectList	analyzers		() const	= 0;
    /*! @brief Filters Exporting Method.
     * This method allows a plugin to export any number of recognizers.
     * @return A PluginMetaObjectList containing all available filters MetaObjects. */
    virtual PluginMetaObjectList	recognizers		() const	= 0;
};

}

// Plugin interface.
Q_DECLARE_INTERFACE(RE::AbstractPlugin, "fr.PierreQR.RecognitionSuite.RELibrary.AbstractPlugin/1.0")

#endif
