#include "PluginsManager.h"
#include "AbstractPlugin.h"
#include "PluginLoader.h"
#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

RE::PluginsManager::PluginsManager (QObject *p)
:QObject(p) {
    // Loop for each found plugin.
    QStringList					fltrs;
    #if defined(Q_OS_WIN)
    fltrs                                               << "REPlug*.dll";
    #elif defined(Q_OS_MACX)
    fltrs                                               << "libREPlug*.dylib";
    #elif defined(Q_OS_UNIX)
    fltrs                                               << "libREPlug*.so";
    #endif
    QStringList                 paths                   = QCoreApplication::libraryPaths();
    // PQR TODO
    paths                                               << "/opt/PQRRS/plugins";
    foreach (QString const &path, paths) {
        QDir                            d               (path + (path.endsWith("/plugins") ? "/RELibrary" : ""));
        if (!d.exists())                                continue;
        Dbg                                             << "Discovering plugins in" << d.absolutePath() << "...";
        foreach (QString const &fi, d.entryList(fltrs, QDir::Files)) {
            Dbg                                         << "Loading plugin" << fi << "from" << d.path() << "...";
            loadFile                                    (d.absoluteFilePath(fi));
        }
    }
}
RE::PluginsManager::~PluginsManager () {
    for (Cache::const_iterator i=_cacheByFileName.constBegin(); i!=_cacheByFileName.constEnd(); ++i) {
        unload      (i.value().first);
        delete      i.value().second;
    }
}

RE::PluginBaseInfos::List RE::PluginsManager::loadedPlugins () const {
    PluginBaseInfos::List bis;
    for (Cache::const_iterator i=_cacheByFileName.constBegin(); i!=_cacheByFileName.constEnd(); ++i) {
        bis						<< i.value().first;
    }
    return						bis;
}
RE::AbstractPlugin* RE::PluginsManager::rootInstance (PluginBaseInfos const &bi) const {
    PluginLoader	*plgLdr		= _cacheByName.value(bi.name).second;
    return	plgLdr ? plgLdr->instance() : 0;
}

RE::PluginLoader* RE::PluginsManager::loadFile (QString const &fn) {
    QString			bn			= QFileInfo(fn).baseName();
    if (_cacheByFileName.contains(bn)) {
        Dbg                                     << "Plugin" << bn << "is in cache, not reloading.";
        return					_cacheByFileName[bn].second;
    }
    PluginLoader	*plgLdr		= new PluginLoader(fn);
    if (!plgLdr->load()) {
        Wrn                                     << "Loading of plugin" << bn << "failed!";
        delete					plgLdr;
        return					0;
    }
    PluginBaseInfos
            const	&bi			= plgLdr->baseInfos();
    _cacheByName[bi.name]		= qMakePair(bi, plgLdr);
    _cacheByFileName[bn]		= qMakePair(bi, plgLdr);
    return						plgLdr;
}
bool RE::PluginsManager::unload (PluginBaseInfos const &bi) {
    if (!_cacheByName.contains(bi.name))
        return					true;
    PluginLoader		*plgLdr	= _cacheByName.value(bi.name).second;
    if (!plgLdr->isLoaded())	return true;
    Dbg							<< "Unloading" << bi.name << "...";
    if (!plgLdr->unload()) {
        Wrn						<< "Cannot unload plugin" << bi.name << "!";
        return					false;
    }
    return						true;
}
