#ifndef RE_PluginsManager_h
#define RE_PluginsManager_h

// Base class.
#include <QtCore/QObject>
// Other stuff.
#include "../RETypes.h"
#include "PluginLoader.h"
#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtCore/QString>
namespace RE {
    class AbstractPlugin;
}

namespace RE {

/*!
 * @brief Plugins Manager Class.
 * @todo Documentation, Use case. */
class RELibOpt PluginsManager : public QObject {
Q_OBJECT
public:
    // Constructors.
    /*! @brief Constructor.
     * @todo Documentation.
     */
                                PluginsManager		(QObject *p=0);
    // Destructor.
    virtual						~PluginsManager		();
    // Query methods.
    PluginBaseInfos::List		loadedPlugins		() const;
    RE::AbstractPlugin*			rootInstance		(PluginBaseInfos const &bi) const;

protected:
    // Loading methods.
    bool						load				(PluginBaseInfos const &bi);
    bool						unload				(PluginBaseInfos const &bi);
    PluginLoader*				loadFile			(QString const &fileName);

private:
    // Types.
    typedef QMap< QString,QPair <RE::PluginBaseInfos,RE::PluginLoader*> > Cache;
    // Global plugins cache.
    Cache                       _cacheByName;
    Cache                       _cacheByFileName;
};

}

#endif
