#ifndef RE_Matrix_h
#define RE_Matrix_h

// Required stuff.
#include <QtCore/QList>
#include <QtCore/QVector>
#include <QtCore/QSize>

/*!
 * @brief Matrix Template Class.
 * The matrix class wraps any template type into a matrix-like object.
 * It augments the data with an automatic reference counter so the developer doesn't have
 * to worry about passing or handling large datasets by reference or by value.
 */
template<typename T> class Matrix {
public:
    typedef QList	< Matrix<T> >			List;
    typedef	QVector	< Matrix<T> >			Vector;

    /*! @brief Default Contstructor.
     * @param size is an optional QSize reference from which the matrix dimensions will be set.
     * @param data is an optional data pointer typed to T from which to copy the data from to the internal matrix data object. */
                        Matrix					(QSize const &size=QSize(1,1), T const *data=0) : _data(0), _referenceCount(0) { setData(size, data); }
   /*! @brief Copy Constructor.
    * When the copy-constructor is called, the internal data pointer is not deeply copied.
    * It is important to underatand that the underlying data is referenced from the built matrix object
    * to the copied one, and the reference counter is incremented. Resulting objects share the data and reference counter pointers.
    * @param orig is the copied object. */
                        Matrix					(const Matrix<T> &orig) : _size(orig._size), _data(orig._data), _referenceCount(orig._referenceCount) { (*_referenceCount)++; }
    /*! @brief Destructor
     * When destructing a Matrix object, the reference counter is decremented. The underlying data is destructed when the reference
     * counter reaches zero. */
                            ~Matrix				() { dereference(); }

    //! @brief To-Stream Operator Overload.
    friend QDataStream&	operator<<				(QDataStream& s, Matrix<T> const &m) {
        QSize const	&sz			= m._size;
        s						<< sz;
        quint32		count		= sz.width()*sz.height();
        for (quint32 i=0; i<count; ++i)
            s					<< m._data[i];
        return s;
    }
    //! @brief From-Stream Operator Overload.
    friend QDataStream&	operator>>				(QDataStream& s, Matrix<T> &m) {
        QSize		sz;
        s						>> sz;
        quint32		count		= sz.width()*sz.height();
        T			*dt			= new double[count];
        for (quint32 i=0; i<count; ++i)
            s					>> dt[i];
        m.setData				(sz, dt);
        delete[]				dt;
        return s;
    }

    //! @brief Copy Operator Overload.
    Matrix<T>&			operator=				(Matrix<T> const &orig)	{
        if (this!=&orig) {
            dereference			();
            _size				= orig._size;
            _data				= orig._data;
            _referenceCount		= orig._referenceCount;
            (*_referenceCount)	++;
        }
        return *this;
    }
    //! @brief Equality Operator Overload.
    bool				operator==				(const Matrix<T> &m) const {
        if (_size!=m.size())
            return false;
        quint32		count		= _size.width()*_size.height();
        for (quint32 i=0; i<count; ++i)
            if (_data[i]!=m._data[i])
                return false;
        return true;
    }
    //! @brief Inequality Operator Overload.
    bool				operator!=				(const Matrix<T> &m) const {
        return !*this==m;
    }

    //! @brief Size Getter.
    QSize const&			size				() const    { return _size; }
    //! @brief Underlying Constant Memory Accessor.
    T const*				data				() const    { return _data; }
    //! @brief Underlying Memory Accessor.
    T*						data				()          { return _data; }

    //! @brief Data Setter.
    void					setData					(QSize const &size, T const *data=0) {
        dereference			();
        _size				= size;
        quint32		count	= _size.width()*_size.height();
        _data				= new T[count];
        _referenceCount		= new quint32;
        (*_referenceCount)	= 1;
        if (data)			memcpy(_data, data, count*sizeof(T));
        else				memset(_data, 0, count*sizeof(T));
    }

protected:
    //! @brief Reference Counter Decrementing Helper, Frees Underlying Data at Zero.
    void					dereference			() {
        if (_data && _referenceCount) {
            (*_referenceCount)	--;
            if (!(*_referenceCount)) {
                delete[]		_data;
                _data			= 0;
                delete			_referenceCount;
                _referenceCount	= 0;
            }
        }
    }

private:
    // Members.
    QSize				_size;
    T*					_data;
    mutable quint32*	_referenceCount;
};

#endif
