/*! @file RELibrary.h
 * @brief RELibrary C API Header File.
 * @todo Documentation.
*/

#ifndef RE_RELibrary_h
#define RE_RELibrary_h

#include "RETypes.h"

#ifdef Q_OS_WIN
#include <windows.h>
extern "C" {
    RELibOpt    qint32      WINAPI      DllMain                         (HINSTANCE hInstance, DWORD dwReason, LPVOID);
}
#endif

/*! @defgroup RELibCAPI RELib C-API
 * This subset of functions are part of the RELibrary C API. They expose most of the core functionality
 * of RELibrary, and allows developers to use the RE::Engine from within their application.
 * A typical usage of the C API would look like the following pseudo-code:
 * - Statically initialize Recognition Library with REStaticInitializer() .
 * - Assert that the initialized version holds the expected versions numbers with REVersion() as well as REClientBinaryVersion() .
 * - Then {
 *   - Grab a handle to a new Recognition Engine with RECreateEngine(), and make sure that the handle is not null.
 *   - Load a Client Binary into the Recognition Engine with RELoadEngineClientBinary() .
 *   - Then {
 *     - Instanciate a Recognition Document with RECreateDocument() .
 *     - Attach strings and images to the document using REGetDocument* and RESetDocument* functions.
 *     - Guess the Recognition Document type with REGuessDocumentType(), and extract data with RERecognizeDocumentData().
 *     - Iterate through results, read images and strings.
 *     - For any image read with REGetDocumentImageForKey(), it is important to free up the memory by calling REReleaseMemory()!
 *     - Free the Recognition Document with REDeleteDocument().
 *   }
 *   - Delete the Recognition Engine with REDeleteEngine() .
 * } */

/*! @ingroup RELibCAPI
 * @{ */
#ifdef __cplusplus
extern "C" {
#endif

typedef struct SAStruct {
    int         count;
    char        **elements;
} SAStruct;

/*! @brief Recognition Engine Version Getter.
 * Allows the retrieval of the current RELibrary binary version as a string.
 * @return A char* describing the version. */
RELibOpt    char const* RECConv     REVersion                       ();
/*! @brief Recognition Engine Client Binary Version Getter.
 * Allows the retrieval of the minimum required version of a Client Binary package for RELibrary to use.
 * @return A char* describing the required Client Binary version. */
RELibOpt    char const* RECConv     REClientBinaryVersion           ();

/*! @brief Recognition Engine Static Initializer.
 * Globally initializes the RELibrary process, and has to be called only one time before doing anything else. */
RELibOpt    void        RECConv     REStaticInitializer             ();

RELibOpt    quint32     RECConv     REReleaseString                 (char *stringMemoryHandle);
RELibOpt    quint32     RECConv     REReleaseStringArray            (SAStruct array);
/*! @brief Recognition Document Image / String Memory Freeing Method.
 * Deletes memory previously allocated by the REGetDocumentImageForKey() function.
 * @param[in] imageMemoryHandle The memory block allocated by REGetDocumentImageForKey().
 * @return 0 when an error has occured, 1 if the operation was successfull. */
RELibOpt    quint32     RECConv     REReleaseImage                  (char *imageMemoryHandle);

/*! @brief Recognition Engine Instanciator.
 * Instanciates a new Recognition Engine object, and returns a handle allowing to use it later.
 * @param[in] licenseFilename A string containing the full path of the license file (Eg. "/home/me/.PQRRSLicense.lic").
 * @return A handle to the newly allocated Recognition Engine object, or 0 if an error occured. */
RELibOpt	void*       RECConv     RECreateEngine                  (char const *licenseFilename);
/*! @brief Recognition Engine Client Binary Loader.
 * Loads a Client Binary data file into a Recognition Engine instance, and returns the operation result.
 * @param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function.
 * @param[in] clientBinaryPath A string containing the absolute or relative path of the Client Binary file to be loaded into the Recognition Engine.
 * @return 0 when an error has occured, 1 if loading was successfull. */
RELibOpt    quint32     RECConv     RELoadEngineClientBinary        (void *reHandle, char const *clientBinaryPath);
/*! @brief Recognition Engine Destruction.
 * Releases a previously allocated Recognition Engine.
 * @param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function.
 * @return 0 when an error has occured, 1 if loading was successfull. */
RELibOpt	quint32     RECConv     REDeleteEngine                  (void *reHandle);

/*! @brief Recognition Document Instanciator.
 * Creates a Recognition Document to be used later with an instance of a Recognition Engine.
 * A Recognition Document can have a type as well attachements (Pictures, strings, etc). Once a recognition document has been
 * created, and at least one picture has been attached to it, it is possible to either let the Recognition Engine try to
 * guess it's type or define it programmatically. Once the Recognition Document's type has been defined, the data recognition
 * itself can then occur using the RERecognizeDocumentData() function.
 * @return A void* pointer to the newly created Recognition Document instance, or 0 if an error occured. */
RELibOpt    void*       RECConv     RECreateDocument                ();
/*! @brief Recognition Document Destructor.
 * Releases a Recognition Document instance as well as any resource it relates.
 * After type detection and recognition has been performed on a document, it needs to be released so any memory allocation
 * related to that object is freed.
 * Please note that any image memory allocated with the REGetDocumentImageForKey() will not be automatically be freed, the
 * REReleaseMemory() function needs to be called for this memory to be freed!
 * @param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function.
 * @return 0 when an error has occured, 1 if loading was successfull. */
RELibOpt    quint32     RECConv     REDeleteDocument                (void *rdHandle);

/*! @brief All Document Results Getter (JSON). */
RELibOpt    quint32     RECConv     REGetDocumentJsonResults        (void const *rdHandle, char **out);
/*! @brief Single Recognition Zone Results Getter (JSON). */
RELibOpt    quint32     RECConv     REGetDocumentJsonResultForKey   (void const *rdHandle, char const *catKey, char **out);

/*! @brief Recognition Document String Category Keys Getter. */
RELibOpt    SAStruct    RECConv     REGetDocumentCategoryKeys       (void const *rdHandle);
/*! @brief Recognition Document String Keys Getter. */
RELibOpt    SAStruct    RECConv     REGetDocumentStringKeys         (void const *rdHandle, char const *catKey);
/*! @brief Recognition Document String Getter by Key. */
RELibOpt    quint32     RECConv     REGetDocumentStringForKey       (void const *rdHandle, char const *catKey, char const *szKey, char **out);
/*! @brief Recognition Document String Setter by Key.
 * Attaches a string to a Recognition Document based on a given key.
 * @param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the key and value need to be retrieved.
 * @param[in] catKey The category key to be used..
 * @param[in] key The key for which the string will be attached to the document.
 * @param[in] value The string itself.
 * @return 0 when an error has occured, 1 if the operation was successfull. */
RELibOpt    quint32     RECConv     RESetDocumentStringForKey       (void *rdHandle, char const *catKey, char const *key, char const *value);

/*! @brief Recognition Document Image Keys Getter. */
RELibOpt    SAStruct    RECConv     REGetDocumentImageKeys          (void const *rdHandle);
/*! @brief Recognition Document Image Getter by Key.
 * Retrieves a Recognition Document's image for a given key.
 * @param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the image needs to be retrieved.
 * @param[in] key The key for which the image will be retrieved.
 * @param[in] format Can be of'jpg', 'png', 'gif', 'bmp'.
 * @param[in] quality Not yet documented.
 * @param[out] outLength Is set with the size of the data that has been written in the output buffer.
 * @param[out] outImageMemoryHandle A pointer to a pointer for the image buffer to be allocated and filled with data.
 * @return 0 when an error has occured, any other value will be the length of the allocated outImageMemoryHandle. */
RELibOpt    quint32     RECConv     REGetDocumentImageForKey        (void const *rdHandle, char const *key, char const *format, unsigned int quality, unsigned long long &outLength, char **outImageMemoryHandle);
/*! @brief Recognition Document Image Setter by Key.
 * Attaches an image to a Recognition Document based on a given key.
 * @param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the image needs to be set.
 * @param[in] key The key that needs to be used for the image to be set.
 * @param[in] data The image data to be processed and attached to the Recognition Document./
 * @param[in] dataLength The length of the data to be read.
 * @return 0 when an error has occured, 1 if the operation was successfull. */
RELibOpt    quint32     RECConv     RESetDocumentImageForKey        (void *rdHandle, char const *key, char const *data, unsigned int dataLength);

/*! @brief Recognition Document Type Guessing Method.
 * Guesses a Recognition Document's type based on it's attached strings and images.
 * @param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function.
 * @param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and on which the guess will be taken.
 * @param[in] parallelizedCoresCount An unsigned int telling the engine how many CPU cores to use when filtering the image. If unsure, use 1. If you want to use the ideal CPU count, pass 0.
 * @return 0 when an error has occured, 1 if the operation was successfull. */
RELibOpt    quint32     RECConv     REGuessDocumentType             (void const *reHandle, void *rdHandle, unsigned int parallelizedCoresCount);
/*! @brief Recognition Document Type Getter.
 * Retrieves a Recognition Document's type index. This call is mostly usefull after using the REGuessDocumentType() function.
 * @param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the type index needs to be known.
 * @return 0 when an error has occured or if the Recognition Document's type is unknown, any other value for it's type. */
RELibOpt    quint32     RECConv     REGetDocumentType               (void const *rdHandle);
/*! @brief Recognition Document Type Setter.
 * Forces a Recognition Document's type by it's index. This function's main purpose is to bypass the neural network
 * based document type recognition process when the document type is known.
 * @param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function. The client binary data loaded into this engine will be used for setting the document type index.
 * @param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the type index needs to be known.
 * @param[in] typeCode The document type code to be set for the given document handle. This document type code has to exist in the client binary documents definition logic.
 * @return 0 when an error has occured or if the Recognition Document's type is unknown, any other value for it's type. */
RELibOpt    quint32     RECConv     RESetDocumentType               (void const *reHandle, void *rdHandle, unsigned int typeCode);
/*! @brief Recognition Document Recognition Runner.
 * Starts the recognition on a Recognition Document. Once the recognition has been achieved, the results will be made available in the Recognition Document's strings and images.
 * @param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function.
 * @param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the recognition process needs to be done.
 * @param[in] parallelizedCoresCount An unsigned int telling the engine how many CPU cores to use when filtering the image. If unsure, use 1. If you want to use the ideal CPU count, pass 0.
 * @param[in] threadId A thread identifier from the caller's context, or zero is no multi-threading is required.
 * @return 0 when an error has occured, 1 if the operation was successfull. */
RELibOpt    quint32     RECConv     RERecognizeDocumentData         (void const *reHandle, void *rdHandle, unsigned int parallelizedCoresCount, unsigned int threadId);

#ifdef __cplusplus
}
#endif
/*! @} */

#endif
