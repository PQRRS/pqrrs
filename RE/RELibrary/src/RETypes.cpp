#include "RETypes.h"
#include "Engine.h"
#include "../../Versioning/RELibraryVersion.h"
#include "../../Versioning/REClientBinaryVersion.h"
// Required stuff.
#include <LMLibrary/LMTypes>
#include "Filters/ConvolutionMatrixFilter.h"
#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QCoreApplication>

RE::T::Document::Document (quint32 id)
: semaphore(INT_MAX), typeDataCode(-1), typeData(0), uid(id), micrMatches(false),
timestamp(QDateTime::currentDateTime()), validity(RE::E::DocumentValidityIncomplete) {
    results["Global"]["UUID"]   = QString("%1%2")
                                    .arg(timestamp.toString("yyyyMMdd-hhmmsszzz"))
                                    .arg(uid, 3, 10, QChar('0'));
}
RE::T::Document::Document (RE::T::Document const &d)
: semaphore(INT_MAX), typeDataCode(d.typeDataCode), typeData(d.typeData), uid(d.uid), micrMatches(d.micrMatches),
timestamp(d.timestamp),
results(d.results), images(d.images), validity(d.validity), acceptationReasons(d.acceptationReasons), rejectionReasons(d.rejectionReasons) {
}
RE::T::Document::~Document () {
    if (semaphore.available()<INT_MAX)
        Crt						<< "A document is being deleted while having a retain count of" << INT_MAX-semaphore.available() << "!";
}

// Results conversion to JSON format.
QString RE::resultsToJsonString (RE::T::Results const &results) {
    QStringList     blocks;
    for (RE::T::Results::const_iterator rI=results.constBegin(); rI!=results.constEnd(); ++rI) {
        blocks              << QString("%1: %2").arg(sanitizeJsonKey(rI.key())).arg(variantToJson(QVariant(rI.value())));
    }
    return "{"+blocks.join(",")+"}";
}
QString RE::variantToJson (QVariant const &v) {
    qint32          ut      = v.userType();
    // String.
    switch (ut) {
    case QMetaType::QString:
        return  sanitizeJsonString(v.toString());
    // Boolean.
    case QMetaType::Bool:
        return v.toBool() ? "Y" : "N";
    case QMetaType::Float:
    case QMetaType::Double:
    case QMetaType::UInt:
    case QMetaType::Int:
    case QMetaType::UChar:
    case QMetaType::Char:
        return  v.toString();
    // Data array.
    case QMetaType::QByteArray:
        return "0x" + v.toByteArray().toHex();
    // Lists.
    case QMetaType::QVariantList: {
        QVariantList const  &vl  = v.toList();
        QStringList items;
        foreach (QVariant const &v, vl)
            items           << variantToJson(v);
        return QString("[%1]").arg(items.join(","));
    }
     // Associative objects.
    case QMetaType::QVariantHash: {
        QVariantHash const &vh  = v.toHash();
        QStringList items;
        for (QVariantHash::const_iterator it=vh.constBegin(); it!=vh.constEnd(); ++it)
            items           << QString("%1:%2").arg(sanitizeJsonKey(it.key())).arg(variantToJson(it.value()));
        return QString("{%1}").arg(items.join(","));
    }
    case QMetaType::QVariantMap: {
        QVariantMap const &vh  = v.toMap();
        QStringList items;
        for (QVariantMap::const_iterator it=vh.constBegin(); it!=vh.constEnd(); ++it)
            items           << QString("%1: %2").arg(sanitizeJsonKey(it.key())).arg(variantToJson(it.value()));
        return QString("{%1}").arg(items.join(","));
    }
    // Lines.
    case QMetaType::QLine: {
        QLine       line    = v.toLine();
        return QString("{\"X1\":%1,\"Y1\":%2,\"X2\":%3,\"Y1\":%4}").arg(line.x1()).arg(line.y1()).arg(line.x2()).arg(line.y2());
    }
    case QMetaType::QLineF: {
        QLineF      line    = v.toLine();
        return QString("{\"X1\":%1,\"Y1\":%2,\"X2\":%3,\"Y1\":%4}").arg(line.x1()).arg(line.y1()).arg(line.x2()).arg(line.y2());
    }
    // Rectangles.
    case QMetaType::QRect: {
        QRect       rect    = v.toRect();
        return QString("{\"Left\":%1,\"Top\":%2,\"Width\":%3,\"Height\":%4}").arg(rect.left()).arg(rect.top()).arg(rect.width()).arg(rect.height());
    }
    case QMetaType::QRectF: {
        QRect       rect    = v.toRect();
        return QString("{\"Left\":%1,\"Top\":%2,\"Width\":%3,\"Height\":%4}").arg(rect.left()).arg(rect.top()).arg(rect.width()).arg(rect.height());
    }
    // Unknown variant?!
    default:
        Wrn     << "Unknown result variant type" << ut << "!";
        return  sanitizeJsonString(v.toString());
    }
}
QString RE::sanitizeJsonKey (const QString &v) {
    return sanitizeJsonString(v);
}
QString RE::sanitizeJsonString (const QString &v) {
    QString         res, str        = v;
    str.replace                     ("\\", "\\\\");
    // Escape unicode characters.
    const ushort*   unc             = str.utf16();
    unsigned int i = 0;
    while (unc[i]) {
        if (unc[i]<128)
            res.append              (unc[i]);
        else {
            QString hex             = QString::number(unc[i], 16).rightJustified(4, QLatin1Char('0'));
            res.append              (QLatin1String ("\\u") ).append(hex);
        }
        ++i;
    }
    str                             = res;
    str.replace(QLatin1String("\""), QLatin1String("\\\"") );
    str.replace(QLatin1String("\b"), QLatin1String("\\b") );
    str.replace(QLatin1String("\f"), QLatin1String("\\f") );
    str.replace(QLatin1String("\n"), QLatin1String("\\n") );
    str.replace(QLatin1String("\r"), QLatin1String("\\r") );
    str.replace(QLatin1String("\t"), QLatin1String("\\t") );

    return QString("\"%1\"").arg(str);
}

bool RE::T::ZoneFilterData::operator== (ZoneFilterData const &v) {
    if (objectClassName!=v.objectClassName || parameters.count()!=v.parameters.count())
        return false;
    // Compare parameters.
    for (QVariantMap::const_iterator i=parameters.constBegin(); i!=parameters.constEnd(); ++i) {
        QString const	&key	= i.key();
        QVariant const	&val	= i.value();
        if (!v.parameters.contains(key) || val!=v.parameters[key])
            return false;
    }
    return true;
}
bool RE::T::ZoneFilterData::operator!= (ZoneFilterData const &v) {
    return !operator==(v);
}

// Zone Recognizer Data implementation.
bool RE::T::ZoneRecognizerData::operator== (ZoneRecognizerData const &v) {
    // Compare names, parameters count, and filter stack count.
    if (objectClassName!=v.objectClassName || parameters.count()!=v.parameters.count() || filterStack.count()!=v.filterStack.count())
        return false;
    // Compare parameters.
    for (QVariantMap::const_iterator i=parameters.constBegin(); i!=parameters.constEnd(); ++i) {
        QString const	&key	= i.key();
        QVariant const	&val	= i.value();
        if (!v.parameters.contains(key) || val!=v.parameters[key])
            return false;
    }
    // Compare filters stack.
    int					count	= filterStack.count();
    for (int i=0; i<count; ++i)
        if (filterStack[i]!=v.filterStack[i])
            return false;
    return true;
}
bool RE::T::ZoneRecognizerData::operator!= (ZoneRecognizerData const &v) {
    return !operator==(v);
}

void RE::T::ClientBinaryData::clear () {
    version.clear					();
    generatorVersion.clear			();
    libraryVersion.clear			();
    name.clear						();
    copyright.clear					();
    annData.clear					();
    documentTypesData.clear			();
}

void RE::StaticInitializer () {
    LM::StaticInitializer   ();

    static bool	initDone	= false;
    if (initDone)			return;
    initDone				= true;

    // Enums.
    #define EnumFullRegistration(name)	qRegisterMetaType<name>(#name); qRegisterMetaTypeStreamOperators<int>(#name)
    EnumFullRegistration	(RE::E::First);
    EnumFullRegistration	(RE::E::Agressivity);
    EnumFullRegistration	(RE::E::BarcodeStandard);
    EnumFullRegistration	(RE::E::PageSegmentation);
    EnumFullRegistration	(RE::E::ReadingDirection);
    EnumFullRegistration	(RE::E::DocumentValidity);
    EnumFullRegistration        (RE::E::TransformMode);
    EnumFullRegistration	(RE::E::Last);
    #undef EnumFullRegistration

    // All other types.
    #define TypeFullRegistration(name)	qRegisterMetaType<name>(#name); qRegisterMetaTypeStreamOperators<name>(#name)
    // Basic types.
    TypeFullRegistration	(RE::T::GrayValue);
    // Ratios.
    TypeFullRegistration	(RE::T::Ratio);
    TypeFullRegistration	(RE::T::PositiveRatio);
    TypeFullRegistration	(RE::T::NegativeRatio);
    // Paths.
    TypeFullRegistration	(RE::T::Path);
    TypeFullRegistration	(RE::T::FolderPath);
    TypeFullRegistration	(RE::T::FilePath);
    // List / Vectors / Hashes.
    TypeFullRegistration	(RE::T::UIntList);
    TypeFullRegistration	(RE::T::UIntVector);
    TypeFullRegistration	(RE::T::UIntToDoubleHash);
    TypeFullRegistration	(RE::T::UIntToStringHash);
    TypeFullRegistration	(RE::T::StringToStringHash);
    TypeFullRegistration	(RE::T::StringToImageHash);
    TypeFullRegistration	(RE::T::UIntToDoubleHashList);
    // Classes.
    TypeFullRegistration	(RE::T::FillColor);
    // Matrix-related types.
    TypeFullRegistration	(RE::T::CVMatrixElement);
    TypeFullRegistration	(RE::T::CVMatrix);
    TypeFullRegistration	(RE::T::CVMatrices);
    // Other classes / structs.
    TypeFullRegistration    (RE::T::FillColor);
    TypeFullRegistration    (RE::T::ZoneFilterData);
    TypeFullRegistration    (RE::T::ZoneFilterData::List);
    TypeFullRegistration    (RE::T::ZoneRecognizerData);
    TypeFullRegistration    (RE::T::ZoneRecognizerData::List);
    TypeFullRegistration    (RE::T::ClientBinaryData);
    // Ready-to-use matrices.
    TypeFullRegistration	(RE::T::CVMatrixStart);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::HomemadeSpecial);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Laplacian1_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Laplacian2_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Laplacian_25x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::SobelHorizontal_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::SobelVertical_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::LineDetectionHorizontal_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::LineDetectionHorizontal_15x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::LineDetectionVertical_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::LineDetectionVertical_15x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::PointSpread_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Sharpen2_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Sharpen3_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::SharpenHighPass_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::SharpenHighPass_25x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::SmoothArithmeticMean_3x2);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::SmoothArithmeticMean_4x2);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Smoothing_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Smoothing_5x2);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Contrast_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Emboss_9x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::HorizontalBlur_3x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::VerticalBlur_3x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Blur_3x2);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Blur_25x1);
    TypeFullRegistration	(RE::ConvolutionMatrixFilter::Blur_7x2);
    TypeFullRegistration	(RE::T::CVMatrixEnd);
    #undef TypeFullRegistration
}
char const* RE::Version () {
    return const_cast<char*>(STRINGIZE(RELibraryVersion));
}
char const* RE::ClientBinaryDataVersion () {
    return const_cast<char*>(STRINGIZE(REClientBinaryVersion));
}

// Absolute <-> Relative rect conversions.
QRect RE::RelativeToAbsoluteRect (QRectF const &r, QSize const &s) {
    double	xFactor	= s.width()/100.0f;
    double	yFactor	= s.height()/100.0f;
    return QRect(r.x()*xFactor, r.y()*yFactor, r.width()*xFactor, r.height()*yFactor);
}
QRectF RE::AbsoluteToRelativeRect (QRect const &r, QSizeF const &s) {
    double	xFactor	= 100.0f/s.width();
    double	yFactor	= 100.0f/s.height();
    return QRectF(r.x()*xFactor, r.y()*yFactor, r.width()*xFactor, r.height()*yFactor);
}

QString RE::ClassNameToNaturalString (QString const &cn) {
    QString         cleanRight;
    int             lIdx        = cn.lastIndexOf(':')-1;
    QString         left        = lIdx>=0 ? "[" + cn.left(lIdx) + "]" : "";
    QString         right       = cn.mid(lIdx+2).replace('_', ' ');
    right[0]					= right[0].toUpper();
    bool			pIsLow		= false;
    bool			pIsNum		= false;
    bool			pIsSym		= false;
    for (int i=0; i<right.length(); ++i) {
        QChar		c			= right[i];
        bool		isLow		= c.isLower();
        bool		isNum		= c.isNumber();
        bool		isSym		= c.isSymbol();
        bool		change		= i!=0 && ((pIsLow && !isLow) || isNum!=pIsNum || isSym!=pIsSym);
        if (change)
            cleanRight          += ' ';
        cleanRight              += c;
        pIsLow					= isLow;
        pIsNum					= isNum;
        pIsSym					= isSym;
    }
    if (left.isEmpty())
        return cleanRight;
    else if (right.isEmpty())
        return left;
    else
        return left+' '+cleanRight;
}

// Document signature size.
QSize RE::DocumentTypesSignatureSize () { return QSize(8, 10); }

// Matrix metatype id getters.
int RE::CVMatricesMetatypeId () { return qMetaTypeId<RE::T::CVMatrices>(); }
int RE::FirstMatrixMetatypeId () { return qMetaTypeId<RE::T::CVMatrixStart>()+1; }
int RE::LastMatrixMetatypeId () { return qMetaTypeId<RE::T::CVMatrixEnd>(); }

QMetaEnum RE::MetaForEnumType (QMetaObject const *mo, QVariant::Type const &t) {
    QString const	&mtName		= QMetaType::typeName(t);
    QString			mtNameArr	= mtName.mid(mtName.lastIndexOf(':')+1);
    return mo->enumerator(mo->indexOfEnumerator(mtNameArr.toUtf8().constData()));
}
QVariant RE::VariantForEnumMetatypeId (QVariant::Type mtid, int value) {
    return QVariant(mtid, (void*)&value);
}
int RE::IntValueForEnumVariant (QVariant const &v) {
    return *(int const*)v.constData();
}
