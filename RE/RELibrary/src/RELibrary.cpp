#include "RELibrary.h"
#include "RETypes.h"
#include "Engine.h"
#include <LMLibrary/Core>
#include <QtCore/QtGlobal>
#include <QtCore/QDir>
#include <QtCore/QBuffer>
#include <QtCore/QByteArray>
#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>
#include <QtGui/QImage>
#include <QtGui/QImageWriter>

#define engineFromHandle    ((RE::Engine*)reHndl)
#define documentFromHandle  ((RE::T::Document*)rdHndl)
#define variantFromHandle   ((QVariant*)nHndl)

using namespace RE;

static bool         initDone    = false;
#ifdef Q_OS_WIN
#include "../ThirdParty/win32/include/QtMfcApp/QMfcApp"
static HINSTANCE    hInst        = 0;
// Entry point.
BOOL WINAPI DllMain (HINSTANCE hInstance, DWORD, LPVOID) {
    hInst = hInstance;
    return true;
}
#endif

RELibOpt char const* RECConv REVersion () {
    return RE::Version();
}
RELibOpt char const* RECConv REClientBinaryVersion () {
    return RE::ClientBinaryDataVersion();
}

RELibOpt quint32 RECConv REReleaseString (char *stringMemoryHandle) {
    if (!stringMemoryHandle)
        return                  false;
    delete[]                    stringMemoryHandle;
    return                      true;
}
RELibOpt quint32 RECConv REReleaseStringArray (SAStruct array) {
    if (!array.elements)
        return  false;
    for (int i=0; i<array.count; ++i) {
        delete[]array.elements[i];
    }
    delete[]    array.elements;
    return      true;
}
RELibOpt quint32 RECConv REReleaseImage (char *mem) {
    // Check memory validity.
    if (!mem)
        return  false;
    delete[]    mem;
    return      true;
}


RELibOpt void RECConv REStaticInitializer () {
    if (initDone)        return;
    initDone            = true;
    if (!qApp) {
        bool    ok      = false;
        #ifdef Q_OS_WIN
            ok          = QMfcApp::pluginInstance(hInst);
        #else
            ok          = qApp!=0;
        #endif
        if (!ok) {
            int argc    = 0;
            new QCoreApplication(argc, NULL);
        }
    }
    // Remove default Qt plugins path.
    //foreach (QString const &p, QCoreApplication::libraryPaths())
    //    if (p.startsWith("C:/Qt"))
    //        QCoreApplication::removeLibraryPath(p);
    // Finally call RELibrary static initializer.
    RE::StaticInitializer();
}

RELibOpt void* RECConv RECreateEngine (char const *licenseFilename) {
    LM::Core            *lm  = new LM::Core();
    lm->loadFromFile         (licenseFilename);
    return                   new RE::Engine(lm);
}
RELibOpt quint32 RECConv RELoadEngineClientBinary (void *reHndl, char const *cbPath) {
    return engineFromHandle->loadClientBinaryData(cbPath) ? 1 : 0;
}
RELibOpt quint32 RECConv REDeleteEngine (void *reHndl) {
    if (!reHndl)    return  false;
    delete          engineFromHandle->licensingManagerCore();
    delete          engineFromHandle;
    return          true;
}

RELibOpt void* RECConv RECreateDocument () {
    return              new RE::T::Document();
}
RELibOpt quint32 RECConv REDeleteDocument (void *rdHndl) {
    if (!rdHndl)    return false;
    delete          documentFromHandle;
    return          true;
}

RELibOpt quint32 RECConv REGetDocumentJsonResults (void const *rdHndl, char **out) {
    if (!rdHndl)                    return false;
    QString const   &results        = RE::resultsToJsonString(documentFromHandle->results);
    *out                            = new char[results.length()+1];
    strcpy                          (*out, results.toUtf8().constData());
    return                          true;
}
RELibOpt quint32 RECConv REGetDocumentJsonResultForKey (void const *rdHndl, char const *catKey, char **out) {
    if (!documentFromHandle->results.contains(catKey)) {
        Wrn                             << "Category key" << catKey << "is invalid!";
        return                          false;
    }
    QString const   &result         = RE::variantToJson(documentFromHandle->results[catKey]);
    *out                            = new char[result.length()+1];
    strcpy                          (*out, result.toUtf8().constData());
    return                          true;
}

RELibOpt SAStruct RECConv REGetDocumentCategoryKeys (void const *rdHndl) {
    SAStruct                array;
    array.count                         = 0;
    array.elements                      = 0;

    QStringList const       &keys       = documentFromHandle->results.keys();
    array.count                         = keys.count();
    array.elements                      = new char*[array.count];
    for (int i=0; i<array.count; ++i) {
        QString const       &key        = keys[i];
        array.elements[i]               = new char[key.count()+1];
        strcpy                          (array.elements[i], key.toUtf8().constData());
    }
    return array;
}
RELibOpt SAStruct RECConv REGetDocumentStringKeys (void const *rdHndl, char const *catKey) {
    SAStruct                array;
    array.count                         = 0;
    array.elements                      = 0;

    if (!rdHndl || !catKey)
        return                          array;
    else if (!documentFromHandle->results.contains(catKey)) {
        Wrn                             << "Category key" << catKey << "is invalid!";
        return                          array;
    }

    QStringList const       &keys       = documentFromHandle->results[catKey].keys();
    array.count                         = keys.count();
    array.elements                      = new char*[array.count];
    for (int i=0; i<array.count; ++i) {
        QString const       &key        = keys[i];
        array.elements[i]               = new char[key.count()+1];
        strcpy                          (array.elements[i], key.toUtf8().constData());
    }
    return array;
}
RELibOpt quint32 RECConv REGetDocumentStringForKey (void const *rdHndl, char const *catKey, char const *szKey, char **out) {
    if (!rdHndl || !catKey)
        return                          false;
    else if (!documentFromHandle->results.contains(catKey)) {
        Wrn                             << "Category key" << catKey << "is invalid!";
        return                          false;
    }
    else if (!documentFromHandle->results[catKey].contains(szKey)) {
        Wrn                             << "String key" << szKey << "is invalid!";
        return                          false;
    }

    QString const           &val        = documentFromHandle->results[catKey][szKey].toString();
    *out                                = new char[val.length()+1];
    strcpy                              (*out, val.toUtf8().constData());
    return                              true;
}

RELibOpt quint32 RECConv RESetDocumentStringForKey (void *rdHndl, char const *catKey, char const *key, char const *val) {
    // Check document, key and value validity.
    if (!rdHndl || !catKey || !key || !val || !strlen(catKey) || !strlen(key))
        return                          false;
    documentFromHandle->results[catKey][key]    = val;
    return                              true;
}

RELibOpt SAStruct RECConv REGetDocumentImageKeys (void const *rdHndl) {
    SAStruct                array;
    array.count                         = 0;
    array.elements                      = 0;

    QStringList const       &keys       = documentFromHandle->images.keys();
    array.count                         = keys.count();
    array.elements                      = new char*[array.count];
    for (int i=0; i<array.count; ++i) {
        QString const       &key        = keys[i];
        array.elements[i]               = new char[key.count()+1];
        strcpy                          (array.elements[i], key.toUtf8().constData());
    }
    return array;
}
RELibOpt quint32 RECConv REGetDocumentImageForKey (void const *rdHndl, char const *key, char const *fmt, quint32 quality, quint64 &outLength, quint8 **outBuffer) {
    // Check document validity and make sure that given key exists.
    if (!rdHndl || !key || !documentFromHandle->images.contains(key))
        return              false;
    // Retrieve the string at given key.
    QByteArray          ba;
    { // Memguard.
        QBuffer         bf  (&ba);
        bf.open             (QIODevice::WriteOnly);
        QImageWriter    iw  (&bf, fmt);
        iw.setQuality       (quality);
        iw.write            (documentFromHandle->images[key]);
        bf.close            ();
    }
    outLength               = ba.length();
    *outBuffer              = new quint8[outLength];
    memcpy                  (*outBuffer, ba.data(), outLength);
    return                  true;
}
RELibOpt quint32 RECConv RESetDocumentImageForKey (void *rdHndl, char const *key, char const *data, quint32 len) {
    // Check document, key, image and length validity.
    if (!rdHndl || !key || !data || !len)
        return                      false;
    // Try to load image, and check it's validity.
    QImage      img;
    if (!img.loadFromData((uchar*)data, len))
        return                      false;
    // Assign image to document.
    documentFromHandle->images[key] = img;
    return                          true;
}

RELibOpt quint32 RECConv REGuessDocumentType (void const *reHndl, void *rdHndl, quint32 pcc) {
    return reHndl && engineFromHandle->guessDocumentType(documentFromHandle, pcc);
}
RELibOpt quint32 RECConv RESetDocumentType (void const *reHndl, void *rdHndl, quint32 typeCode) {
    if (!reHndl || !rdHndl)
        return                              false;
    foreach (RE::T::DocumentTypeData const &dt, engineFromHandle->clientBinaryData().documentTypesData) {
        if (dt.code!=typeCode)
            continue;
        documentFromHandle->typeData        = &dt;
        documentFromHandle->typeDataCode    = typeCode;
    }
    return                                  false;
}
RELibOpt quint32 RECConv REGetDocumentType (void const *rdHndl) {
    return rdHndl && documentFromHandle->typeData ? documentFromHandle->typeData->code : 0;
}
RELibOpt quint32 RECConv RERecognizeDocumentData (void const *reHndl, void *rdHndl, quint32 pcc, quint32 threadId) {
    return reHndl && engineFromHandle->recognizeDocumentData(documentFromHandle, pcc, threadId);
}
