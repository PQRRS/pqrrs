#include "Engine.h"
// Licensing.
#include <LMLibrary/LMTypes>
#include <LMLibrary/Core>
// Pixel helper.
#include "PixelHelper.hpp"
// Plugins management.
#include "Plugins/AbstractPlugin.h"
#include "Plugins/PluginLoader.h"
#include "Plugins/PluginsManager.h"
// Image analysis.
#include "Analyzers/BasicHistogram.h"
#include "Analyzers/CumulativeHistogram.h"
#include "Analyzers/RectangleLocator.h"
// Recognizers.
#include "Analyzers/Recognizers/BarcodeReader.h"
#include "Analyzers/Recognizers/DataMatrixReader.h"
#include "Analyzers/Recognizers/DummyRecognizer.h"
#include "Analyzers/Recognizers/OpticalCharacterRecognizer.h"
#include "Analyzers/Recognizers/RawOpticalCharacterRecognizer.h"
// Image filtering.
#include "Filters/AbstractFilter.h"
#include "Filters/BrightnessContrastFilter.h"
#include "Filters/ColorsRemover.h"
#include "Filters/Cropper.h"
#include "Filters/ConvolutionMatrixFilter.h"
#include "Filters/HistogramEqualizer.h"
#include "Filters/HistogramStretcher.h"
#include "Filters/LocalThresholder.h"
#include "Filters/MedianFilter.h"
#include "Filters/ObjectExtractor.h"
#include "Filters/Resizer.h"
#include "Filters/OpticalTiltCorrector.h"
#include "Filters/TiltCorrector.h"
// Scriptting types.
#include "ScriptingWrappers.h"
// Fann required stuff.
#include "Fann/floatfann.h"
// Other various stuff.
#include <QtCore/QFile>
#include <QtCore/QThread>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QCoreApplication>
#include <QtScript/QScriptEngine>

// Constructors / Destructor.
RE::Engine::Engine (LM::Core *lm, QObject *p)
: QObject(p), _lm(lm), _licensed(false), _pluginsManager(0), _scriptEngine(0), _scriptObject(0), _clientBinaryAnn(0) {
    Dbg										<< "Constructing with version" << RE::Version() << "...";

    // Register filters factory instanciators.
    _filtersFactory.registerClass			<RE::BrightnessContrastFilter>();
    _filtersFactory.registerClass           <RE::ColorsRemover>();
    _filtersFactory.registerClass			<RE::ConvolutionMatrixFilter>();
    _filtersFactory.registerClass			<RE::Cropper>();
    _filtersFactory.registerClass			<RE::HistogramEqualizer>();
    _filtersFactory.registerClass			<RE::HistogramStretcher>();
    _filtersFactory.registerClass			<RE::LocalThresholder>();
    _filtersFactory.registerClass			<RE::MedianFilter>();
    _filtersFactory.registerClass			<RE::ObjectExtractor>();
    _filtersFactory.registerClass           <RE::OpticalTiltCorrector>();
    _filtersFactory.registerClass			<RE::Resizer>();
    _filtersFactory.registerClass			<RE::TiltCorrector>();
    // Register recognizers factory instanciators.
    _recognizersFactory.registerClass		<RE::BarcodeReader>();
    _recognizersFactory.registerClass		<RE::DataMatrixReader>();
    _recognizersFactory.registerClass		<RE::OpticalCharacterRecognizer>();
    _recognizersFactory.registerClass		<RE::RawOpticalCharacterRecognizer>();
    _recognizersFactory.registerClass		<RE::DummyRecognizer>();

    // Set up licensing.
    _obfCheesecake                           = new quint8[16];
    _hostUuid                                = LM::Core::hostUuid();
    _licensed                                = _lm->isVerified() && LM::Core::LMObfuscatedCheesecakeForData(_lm, RE::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    DeclareCheesecake                       (RE::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    if (_licensed) {
        // Register internal enums range.
        QMetaObject const		*mo         = &RE::E::staticMetaObject;
        _enumeratorRanges[mo]               << MetaTypesRange(qMetaTypeId<RE::E::First>()+CKZ01, qMetaTypeId<RE::E::Last>()+CKZ02);
        // Make sure plugins are known to the system.
        _pluginsManager                     = new PluginsManager(this);
        parsePlugins                        ();
    }
}
RE::Engine::~Engine () {
    // PQR Leak: Delete client binary data objects...
    if (_clientBinaryAnn)
        fann_destroy                        (_clientBinaryAnn);
    // Delete scripting related objects...
    if (_scriptObject)
        delete								_scriptObject;
    if (_scriptEngine)
        delete								_scriptEngine;
    // Delete cached recognizers / filters.
    qDeleteAll								(_instanciatedRecognizers);
    qDeleteAll								(_instanciatedFilters);
    // Delete plugins manager.
    delete									_pluginsManager;
    // PQR Leak: get rid of cheesecake data.
    delete[] _obfCheesecake;
    _obfCheesecake                          = 0;
}

// Licensing related accessors.
bool RE::Engine::licensed () const {
    return _licensed;
}

// Plugins management.
RE::PluginBaseInfos::List RE::Engine::loadedPlugins () const {
    return _pluginsManager->loadedPlugins();
}
QHash<QString,QString> const& RE::Engine::objectsLocations () const {
    return _objectsLocations;
}

// Enum meta helper functions.
RE::MetaTypesRangeListHash const& RE::Engine::enumeratorRanges () const {
    return _enumeratorRanges;
}
QMetaObject const* RE::Engine::containingMetaObjectForEnumType (int t) const {
    QMetaObject const					*mo		= 0;
    // Loop for each metaobject / range list pairs.
    for (MetaTypesRangeListHash::const_iterator i=_enumeratorRanges.constBegin(); !mo && i!=_enumeratorRanges.constEnd(); ++i) {
        // Loop for each range.
        MetaTypesRangeList const	&rs		= i.value();
        foreach (MetaTypesRange const &r, rs) {
            // Range matches, use this metaobject!
            if (t>r.first && t<r.second) {
                mo								= i.key();
                break;
            }
        }
    }
    return mo;
}

// Parameters builder.
QVariantMap RE::Engine::defaultObjectParameters (QObject *o) const {
    QMetaObject const			*mo			= o->metaObject();
    QVariantMap					params;
    QString                     superClass  = mo->superClass()->className();
    if (superClass=="RE::AbstractFilter")
        params["forceGrayscale"]            = false;
    else if (superClass=="RE::AbstractRecognizer") {
        params["name"]						= "";
        params["makeChecksums"]             = false;
        params["useFullImage"]				= false;
        params["keepFilteredImage"]         = false;
    }
    for (int i=mo->propertyOffset(); i<mo->propertyCount(); ++i) {
        QMetaProperty const		&mp			= mo->property(i);
        if (!mp.isWritable())				continue;
        char const				*pName		= mp.name();
        if (mp.isResettable()) {
            if (!mp.reset(o))
                Wrn							<< "Unable to reset property" << pName
                                            << "for object of type" << mo->className() << ".";
            params[pName]					= o->property(pName);
        }
        else {
            Wrn                             << "Property" << pName << "of object type" << mo->className() << "isn't resetable!";
            params[pName]					= QVariant(mp.userType(), (void*)0);
        }
    }
    return params;
}

// Filters factory related.
RE::ObjectsFactory const& RE::Engine::filtersFactory () const {
    return _filtersFactory;
}
RE::T::ZoneFilterData RE::Engine::makeZoneFilterData (QString const &cn) const {
    Dbg << "Making a new Zone Filter Data object of class" << cn << "...";
    T::ZoneFilterData			zfd;
    if (_licensed) {
        zfd.objectClassName                 = cn;
        zfd.assemblyName					= _objectsLocations.contains(cn) ? _objectsLocations[cn] : "";
        zfd.parameters						= _filtersParameters[cn];
    }
    else
        Crt                                 << "Unlicensed, inducing random crash...";
    return									zfd;
}
RE::AbstractFilter* RE::Engine::imageFilterFromData (RE::T::ZoneFilterData const &dt, QString cacheHash) const {
    if (!_licensed) {
        Crt                                 << "Unlicensed, inducing random crash...";
        return                              0;
    }
    QString							aifName	= dt.objectClassName;
    cacheHash                               = (cacheHash.length() ? cacheHash : "0") + ":" + aifName;
    AbstractFilter                  *aif    = 0;
    { // Lock.
        QMutexLocker                ml      (&_filterCacheLock);
        if (!_instanciatedFilters.contains(cacheHash)) {
            Dbg << "Instanciating Image Filter of class" << aifName << "from Zone Filter Data...";
            if (aifName.isEmpty()) {
                Crt                             << "An empty filter name was encountered.";
                return                          0;
            }
            aif                                 = (AbstractFilter*)_filtersFactory.instanciateClass(aifName);
            if (!aif) {
                Crt								<< "Filter of type" << aifName << "could not be instanciated.";
                return							0;
            }
            // Generate runtime context for this filter.
            aif->setRuntimeContext              (RE::RuntimeContext(cacheHash, this));
            // Finally cache.
            Dbg                                 << "Caching" << aifName << "in" << cacheHash << "...";
            _instanciatedFilters[cacheHash]     = aif;
        }
        else {
            Dbg                                 << "Recycling instanciated Image Filter of class" << aifName << "in" << cacheHash << "...";
            aif                                 = _instanciatedFilters[cacheHash];
        }
    }
    // Reset every single property in the object.
    QMetaObject const       &smo        = aif->staticMetaObject;
    quint32 const           &pc         = smo.propertyCount();
    for (quint32 i=0; i<pc; ++i) {
        QMetaProperty const &mp         = smo.property(i);
        if (mp.isResettable())
            mp.reset                    (aif);
    }
    // Set up parameters.
    QVariantMap const				&ps		= dt.parameters;
    for (QVariantMap::const_iterator i=ps.constBegin(); i!=ps.constEnd(); ++i)
        aif->setProperty					(i.key().toUtf8().constData(), i.value());
    return aif;
}

// Recognizers factory related.
RE::ObjectsFactory const& RE::Engine::recognizersFactory () const {
    return _recognizersFactory;
}
RE::T::ZoneRecognizerData RE::Engine::makeZoneRecognizerData (QString const &cn, QRectF rr) const {
    Dbg << "Making a new Zone Recognizer Data object of class" << cn << "on rect" << rr << "...";
    T::ZoneRecognizerData			zrd;
    if (_licensed) {
        zrd.objectClassName					= cn;
        zrd.assemblyName					= _objectsLocations.contains(cn) ? _objectsLocations[cn] : "";
        zrd.relativeRect					= rr;
        zrd.parameters						= _recognizersParameters[cn];
    }
    else
        Crt                                 << "Unlicensed, inducing random crash...";
    return									zrd;
}
RE::AbstractRecognizer* RE::Engine::imageRecognizerFromData (RE::T::ZoneRecognizerData const &zrd, QString cacheHash) const {
    if (!_licensed) {
        Crt                                 << "Unlicensed, inducing random crash...";
        return                              0;
    }
    QString const               &airName    = zrd.objectClassName;
    QVariantMap const			&ps 		= zrd.parameters;
    AbstractRecognizer          *air        = 0;
    cacheHash                               = (cacheHash.length() ? cacheHash : "0") + ":" + airName;
    if (airName=="RE::OpticalCharacterRecognizer" || airName=="RE::RawOpticalCharacterRecognizer")
        cacheHash                           += QString("[%1:%2]").arg(ps["predicate"].toString()).arg(ps["enableDictionary"].toString());
    { // Lock.
        QMutexLocker                    ml      (&_recognizerCacheLock);
        if (!_instanciatedRecognizers.contains(cacheHash)) {
            Dbg << "Instanciating Image Recognizer of class" << airName << "from Zone Recognizer Data...";
            if (airName.isEmpty()) {
                Crt                             << "An empty recognizer name was encountered.";
                return                          0;
            }
            air                                 = (AbstractRecognizer*)_recognizersFactory.instanciateClass(airName);
            if (!air) {
                Crt								<< "Recognizer of type" << airName << "could not be instanciated.";
                return							0;
            }
            // Generate runtime context for this recognizer.
            air->setRuntimeContext              (RE::RuntimeContext(cacheHash, this));
            // Finally cache.
            Dbg                                 << "Caching" << airName << "in" << cacheHash << "...";
           _instanciatedRecognizers[cacheHash]  = air;
        }
        else {
            Dbg                                 << "Recycling instanciated Image Recognizer of class" << airName << "in" << cacheHash << "...";
            air                                 = _instanciatedRecognizers[cacheHash];
        }
    }
    // Reset every single property in the object.
    QMetaObject const       &smo        = air->staticMetaObject;
    quint32 const           &pc         = smo.propertyCount();
    for (quint32 i=0; i<pc; ++i) {
        QMetaProperty const &mp         = smo.property(i);
        if (mp.isResettable())
            mp.reset                    (air);
    }
    // Set up parameters.
    for (QVariantMap::const_iterator i=ps.constBegin(); i!=ps.constEnd(); ++i)
        air->setProperty					(i.key().toUtf8().constData(), i.value());
    return air;
}
QImage RE::Engine::filteredImage (RE::T::ZoneFilterData::List const &zrfs, QString const &cacheHash, QImage const &img, quint32 const &pcc) const {
    QList<QImage> const     &imgs           = filteredImages(zrfs, cacheHash, img, pcc, false);
    return imgs.isEmpty() ? QImage() : imgs[0];
}
QList<QImage> RE::Engine::filteredImages (RE::T::ZoneFilterData::List const &zrfs, QString const &cacheHash, QImage const &img, quint32 const &pcc, bool debug) const {
    QImage                          fImg    = paletizeIfGrayscale(img, img.isGrayscale());
    QImage                          fdImg   = fImg;
    quint32                         i       = 0;
    foreach (T::ZoneFilterData const &zfd, zrfs) {
        AbstractFilter				*aif	= imageFilterFromData(zfd, cacheHash);
        if (!aif) {
            Crt								<< "Filter won't be able to run because it was not instanciated!";
            continue;
        }
        aif->setDebuggingEnabled            (debug);
        aif->setSource						(fImg);
        aif->setParallelizedCoresCount      (pcc);
        fImg								= aif->apply();
        fdImg                               = debug ? aif->debuggingImage() : fImg;
        aif->resetSource                    ();
        i                                   ++;
    }
    return QList<QImage>() << fImg << fdImg;
}
QImage RE::Engine::filteredRecognizerImage (RE::T::ZoneRecognizerData const &zrd, QString const &cacheHash, QImage const &img, quint32 const &pcc) const {
    QImage							fImg	= zrd.parameters["useFullImage"].toBool() ? img : img.copy(relativeRectToAbsolute(zrd.relativeRect, img.size()));
    return filteredImage                    (zrd.filterStack, cacheHash, fImg, pcc);
}
RE::T::StringToVariantHash RE::Engine::recognizerResults (RE::T::ZoneRecognizerData const &zrd, QString const &cacheHash, QImage const &filteredImage, quint32 const &pcc) const {
    if (!_licensed) {
        Crt                             << "Unlicensed, inducing random crash...";
        return                          RE::T::StringToVariantHash();
    }
    // Run the recognizer on the filtered image.
    AbstractRecognizer			*air	= imageRecognizerFromData(zrd, cacheHash);
    if (!air)							return RE::T::StringToVariantHash();
    air->setSource						(&filteredImage);
    air->setParallelizedCoresCount      (pcc);
    RE::T::StringToVariantHash  res     = air->run() ? air->results() : RE::T::StringToVariantHash();
    air->resetSource                    ();
    return res;
}

// Helpers for geometry conversions.
QRect RE::Engine::relativeRectToAbsolute (QRectF const &relR, QSize const &sz) {
    // Cache image size ratios.
    double const	ratW	= sz.width()/100.0;
    double const	ratH	= sz.height()/100.0;
    return QRect(relR.x()*ratW, relR.y()*ratH, relR.width()*ratW, relR.height()*ratH);;
}

// Client binary loader / getter.
bool RE::Engine::loadClientBinaryData (QString const &path) {
    if (!_licensed) {
        Crt                                 << "Unlicensed, inducing random crash...";
        return                              false;
    }
    // The binary being loaded is the same as the one already loaded...
    if (_clientBinaryPath==path)			return true;
    // A client binary was already loaded...
    else if (!_clientBinaryPath.isEmpty()) {
        // Get rid of its ANN.
        if (_clientBinaryAnn) {
            fann_destroy                    (_clientBinaryAnn);
            _clientBinaryAnn				= 0;
        }
        { // Lock.
            QMutexLocker        ml          (&_scriptLock);
            // Get rid of scripting engine and script objects.
            _scriptFunctionsByType.clear		();
            if (_scriptObject) {
                delete							_scriptObject;
                _scriptObject					= 0;
            }
            if (_scriptEngine) {
                delete							_scriptEngine;
                _scriptEngine					= 0;
            }
        }
    }
    // Cache binary data path being loaded so it won't happends twice in a row.
    _clientBinaryData.clear					();
    _clientBinaryPath						= path;

    // No new binary data... Just return.
    if (_clientBinaryPath.isEmpty())		return true;

    // Load new client binary.
    QFile						fCb			(_clientBinaryPath);
    if (!fCb.exists()) {
        Wrn                                 << "Unable to load" << _clientBinaryPath << ": the file doesn't exist!";
        _clientBinaryPath.clear             ();
        _clientBinaryData.clear             ();
        return                              false;
    }
    QDataStream					fS			(&fCb);
    fCb.open								(QIODevice::ReadOnly);
    fS										>> _clientBinaryData;
    fCb.close								();
    // Bad reading from file?
    if (_clientBinaryData.version.isEmpty()) {
        Wrn                                 << "Unable to load" << _clientBinaryPath << ": invalid file format!";
        _clientBinaryPath.clear             ();
        _clientBinaryData.clear             ();
        return                              false;
    }

    // Check client binary data version.
    else if (!isClientBinaryDataCompatible(_clientBinaryData)) {
        _clientBinaryPath.clear			();
        _clientBinaryData.clear			();
        return                          false;
    }
    // Binary data contains script?
    if (_clientBinaryData.script.length()) {
        { // Lock.
            QMutexLocker            ml          (&_scriptLock);
            // No scripting engine yet?
            if (!_scriptEngine)
                _scriptEngine                   = new QScriptEngine();
            // If any script object was instanciated, release it.
            if (_scriptObject) {
                delete                          _scriptObject;
                _scriptObject                   = 0;
            }
            _scriptObject                       = new QScriptValue(_scriptEngine->evaluate(_clientBinaryData.script));
            if (_scriptEngine->hasUncaughtException()) {
                Wrn								<< "Script parsing failed:" << _scriptEngine->uncaughtException().toString() << "at line" << _scriptEngine->uncaughtExceptionLineNumber() << ".";
                delete							_scriptObject;
                _scriptObject					= 0;
            }
        }
    }
    // If there is ann data, restore it.
    if (!_clientBinaryData.annData.isEmpty())
        _clientBinaryAnn					= binaryToAnn(_clientBinaryData.annData);
    _clientBinaryData.annData.clear			();

    return									true;
}
RE::T::ClientBinaryData const& RE::Engine::clientBinaryData () const {
    return _clientBinaryData;
}
bool RE::Engine::isClientBinaryDataCompatible (RE::T::ClientBinaryData const &cbd) {
    // Check for version numbers.
    { // Memguard.
        QStringList				lV			= cbd.version.split('.');
        QStringList				rV			= QString(RE::ClientBinaryDataVersion()).split('.');
        if (lV.count()!=3 || rV.count()!=3 || lV[0].toUInt()>rV[0].toUInt() || lV[1].toUInt()>rV[1].toUInt() || lV[2].toUInt()>rV[2].toUInt()) {
            Crt								<< "Incompatible Client Binary version (Current" << RE::ClientBinaryDataVersion() << ", exported in Client Binary" << cbd.version << ")!";
            return							false;
        }
    }
    { // Memguard.
        QStringList				lV			= cbd.libraryVersion.split('.');
        QStringList				rV			= QString(RE::Version()).split('.');
        if (lV.count()!=3 || rV.count()!=3 || lV[0].toUInt()>rV[0].toUInt() || lV[1].toUInt()>rV[1].toUInt() || lV[2].toUInt()>rV[2].toUInt()) {
            Crt								<< "Incompatible Library Binary version (Current" << RE::Version() << ", required by Client Binary" << cbd.libraryVersion << ")!";
            return							false;
        }
    }
    return true;
}


// Document recognition methods.
RE::T::DocumentTypeData const* RE::Engine::documentTypeDataForCode (qint32 code) const {
    if (code==-1)			return 0;
    foreach (RE::T::DocumentTypeData const &td, _clientBinaryData.documentTypesData)
        if (td.code==(quint32)code)
            return &td;
    return 0;
}
bool RE::Engine::guessDocumentType (RE::T::Document *doc, quint32 const &pcc) const {
    if (!_licensed) {
        Crt                                             << "Unlicensed, inducing random crash...";
        return                                          false;
    }
    // No client data loaded...
    else if (_clientBinaryPath.isEmpty()) {
        Wrn												<< "No Client Binary present. Aborting!";
        doc->typeData									= 0;
        return                                          false;
    }

    // Store document MICR.
    QString                 micr                        = doc->results["General"]["MICR"].toString();
    // There is no ANN data, it means that only one type is being handled.
    if (!_clientBinaryAnn) {
        doc->typeData									= &_clientBinaryData.documentTypesData.first();
        doc->micrMatches								= QRegExp(QString("^%1$").arg(doc->typeData->micrRegExp)).exactMatch(micr);
        Dbg												<< "No ANN in Client Binary. Using default type" << doc->typeData->code << " (" << doc->typeData->name << ").";
        doc->results["Type"]["Code"]                    = QString::number(doc->typeData->code);
        doc->results["Type"]["Name"]                    = doc->typeData->name;
        doc->results["Type"]["Score"]                   = "0";
        doc->results["Type"]["RecognitionMethod"]       = "Forced";
        return                                          true;
    }

    // Document has MICR, try to determine its type.
    qint32					dtCount						= _clientBinaryData.documentTypesData.count();
    if (!micr.isEmpty()) {
        for (qint32 i=0; i<dtCount; ++i) {
            RE::T::DocumentTypeData const	&dtd		= _clientBinaryData.documentTypesData[i];
            if (QRegExp(QString("^%1$").arg(dtd.micrRegExp)).exactMatch(micr)) {
                if (!dtd.micrIsUnique) {
                    Dbg									<< "MICR pattern not unique, forcing ANN type recognition.";
                    break;
                }
                Dbg                                         << "Choosed type" << dtd.code << "(" << dtd.name << ")from MICR RegExp.";
                doc->typeData                               = &dtd;
                doc->micrMatches                            = true;
                doc->results["Type"]["Code"]                = QString::number(dtd.code);
                doc->results["Type"]["Name"]                = dtd.name;
                doc->results["Type"]["Score"]               = "100.0";
                doc->results["Type"]["RecognitionMethod"]   = "MICR";
                return                                  true;
            }
        }
    }

    // Retrieve document image.
    QImage						img			= doc->images.contains("Front") ? doc->images["Front"] : QImage();
    // No image... Fail.
    if (img.isNull()) {
        doc->typeData						= 0;
        return                              false;
    }
    // Compute type's signature.
    QByteArray					baSig		= signatureForImage(img, pcc);
    quint8 const				*sig		= (quint8 const*)baSig.constData();

    { //Lock.
        // Prepare ANN inputs.
        fann_type					*inputs     = new fann_type[_clientBinaryAnn->num_input];
        for (quint32 si=0; si<_clientBinaryAnn->num_input; si++)
            inputs[si]							= 1.0/255.0*sig[si];
        // Retrieve output types probabilities.
        fann_type					*outputs	= fann_run(_clientBinaryAnn, inputs);
        fann_type					max			= 0.0;
        qint32						oIdx		= -1;
        // Determine highest probability index.
        for (quint32 dtdIdx=0, i=0; i<_clientBinaryAnn->num_output; ++i,++dtdIdx) {
            if (_clientBinaryData.documentTypesData[dtdIdx].noAnnTraining)
                dtdIdx++;
            fann_type				o			= outputs[i];
            if (max<o) {
                oIdx							= dtdIdx;
                max								= o;
            }
        }
        delete[]                                (inputs);

        // Get document info for found type.
        T::DocumentTypeData const	&dtd		= _clientBinaryData.documentTypesData[oIdx];
        quint8						score		= qRound(max*100.0);
        // Score is too low.
        if (score<_clientBinaryData.annThreshold) {
            Wrn									<< "ANN found type" << dtd.name << "with an unacceptable score of" << score << "(Threshold at" << _clientBinaryData.annThreshold << ")!";
            doc->typeData						= 0;
            return                              false;
        }
        else if (dtd.badMicrRejection && dtd.micrIsUnique)
            return                              false;

        Dbg										<< "ANN found type" << dtd.name << "with a score of" << score << ".";
        doc->typeData							= &dtd;
        doc->micrMatches						= QRegExp(QString("^%1$").arg(dtd.micrRegExp)).exactMatch(micr);
        doc->results["Type"]["Code"]                = QString::number(dtd.code);
        doc->results["Type"]["Name"]                = dtd.name;
        doc->results["Type"]["RecognitionMethod"]   = "ANN";
        doc->results["Type"]["Score"]               = QString::number(score);
        return                                  true;
    }
}
bool RE::Engine::recognizeDocumentData (RE::T::Document *doc, quint32 const &pcc, quint32 const &threadId) const {
    if (!_licensed) {
        Crt                             << "Unlicensed, inducing random crash...";
        return                          false;
    }
    // Document has an unknown type, there is nothing we can do... For now.
    else if (!doc->typeData) {
        Wrn								<< "Document Type isn't known, recognition cannot start!";
        return                          false;
    }
    // No attached front pictures... Can't do.
    else if (!doc->images.contains("Front")) {
        Wrn								<< "Document has no recognizable attached picture, aborting recognition!";
        return                          false;
    }

    // Retrieve document image and preprocess it.
    QString                 cacheHash   = QString("Preprocessing::%1").arg(QString::number(threadId));
    QImage const			&imgFront	= filteredImage(doc->typeData->preprocessingFilters, cacheHash, doc->images["Front"], pcc);

    /*
    // Make big hash!
    QCryptographicHash      hasher      (QCryptographicHash::Sha1);
    qint32                  realWidth   = imgFront.width() * (imgFront.format()==QImage::Format_Indexed8 ? 1 : 4);
    quint32                 height      = imgFront.height();
    for (quint32 y=0; y<height; ++y) {
        hasher.addData              ((char const*)imgFront.scanLine(y), realWidth);
    }
    Wrn << "BIG HASH IS" << QString(hasher.result().toHex());
    */

    // Get document info for found type.
    Dbg									<< "Starting recognition for type" << doc->typeData->name << "...";

    // Run recognition loop for each recognition data.
    foreach (T::ZoneRecognizerData const &zrd, doc->typeData->recognizers) {
        QString const       &zrdName    = zrd.parameters["name"].toString();
        cacheHash                       = QString::number(threadId);
        QImage const        &fImg       = filteredRecognizerImage(zrd, cacheHash, imgFront, pcc);
        if (zrd.parameters["keepFilteredImage"].toBool())
            doc->images[zrdName]        = fImg;
        // Only one result in recognition results.
        doc->results[zrdName]           = recognizerResults(zrd, cacheHash, fImg, pcc);
        /*
        RE::T::StringToVariantHash
                            rs          = recognizerResults(zrd, cacheHash, fImg, pcc);
        if (rs.count()==1)
            doc->strings[zrdName]       = rs.values().first();
        // Attach each result.
        else {
            for (AbstractRecognizer::Results::const_iterator i=rs.constBegin(); i!=rs.constEnd(); ++i)
                doc->strings[QString("%1.%2").arg(zrdName).arg(i.key())]	= i.value();
        }
        */
    }
    determineDocumentValidity           (doc);
    return true;
}
bool RE::Engine::determineDocumentValidity (RE::T::Document *doc) const {
    if (!_licensed) {
        Crt                             << "Unlicensed, inducing random crash...";
        return                          false;
    }
    // Document has no type data. We can't run scripts.
    else if (!doc->typeData) {
        doc->rejectionReasons			<< tr("The document's type is invalid.");
        doc->validity					= RE::E::DocumentValidityInvalid;
        return                          false;
    }
    // Document has no test script. It is valid.
    else if (!doc->typeData->hasDocumentValidityScript) {
        doc->validity					= RE::E::DocumentValidityValid;
        return                          true;
    }
    // Retrieve document type code.
    quint32				tCode			= doc->typeData->code;
    // Cache script for later use.
    if (!_scriptFunctionsByType.contains(tCode))
        _scriptFunctionsByType[tCode]	= _scriptObject->property(QString("type%1Check").arg(tCode));
    /* PQR TODO
    // Prepare mapping wrapping objects.
    SWStringToStringMap	*swStrings		= new SWStringToStringMap(&doc->results);
    SWStringList		*swAccRsns		= new SWStringList(&doc->acceptationReasons);
    SWStringList		*swRejRsns		= new SWStringList(&doc->rejectionReasons);
    QScriptValue        val;
    { // Lock.
        QMutexLocker        ml          (&_scriptLock);
        // Prepare script arguments.
        QScriptValueList	args			= QScriptValueList()
                                                << _scriptEngine->newQObject(swStrings)
                                                << _scriptEngine->newQObject(swAccRsns)
                                                << _scriptEngine->newQObject(swRejRsns);
        // Finally invoke script.
        val                                 = _scriptFunctionsByType[tCode].call(*_scriptObject, args);
        if (_scriptEngine->hasUncaughtException()) {
            doc->validity                   = RE::E::DocumentValidityInvalid;
            Wrn                                 << "Script parsing failed:" << _scriptEngine->uncaughtException().toString() << "at line" << _scriptEngine->uncaughtExceptionLineNumber() << ".";
            _scriptEngine->clearExceptions	();
        }
    }
    // All good, store validity.
    doc->validity                       = (RE::E::DocumentValidity)val.toUInt16();
    // Get rid of the mapping objects.
    delete                              swStrings;
    delete                              swAccRsns;
    delete                              swRejRsns;
    return                              true;
    */
    doc->validity                       = RE::E::DocumentValidityValid;
    return                              true;
}
// ANN serialisation / deserialisation.
QByteArray RE::Engine::annToBinary (fann *ann) {
    if (!ann) {
        Wrn						<< "Invalid ANN to binarize!";
        return					QByteArray();
    }
    // Data stream.
    QByteArray			ad;
    QBuffer				buf		(&ad);
    buf.open					(QIODevice::WriteOnly);
    QDataStream			ds		(&buf);

    // First save unstreamable types.
    ds					<< (qint32)(ann->last_layer-ann->first_layer)
                        << (qint32)ann->network_type
                        << (qint32)ann->training_algorithm
                        << (qint32)ann->train_error_function
                        << (qint32)ann->train_stop_function;
    // Proceed with easy types.
    ds                  << ann->learning_rate
                        << ann->connection_rate
                        << ann->learning_momentum
                        << ann->cascade_output_change_fraction
                        << ann->quickprop_decay
                        << ann->quickprop_mu
                        << ann->rprop_increase_factor
                        << ann->rprop_decrease_factor
                        << ann->rprop_delta_min
                        << ann->rprop_delta_max
                        << ann->rprop_delta_zero
                        << ann->cascade_output_stagnation_epochs
                        << ann->cascade_candidate_change_fraction
                        << ann->cascade_candidate_stagnation_epochs
                        << ann->cascade_max_out_epochs
                        << ann->cascade_min_out_epochs
                        << ann->cascade_max_cand_epochs
                        << ann->cascade_min_cand_epochs
                        << ann->cascade_num_candidate_groups
                        << ann->bit_fail_limit
                        << ann->cascade_candidate_limit
                        << ann->cascade_weight_multiplier;
    // Arrays.
    ds                  << ann->cascade_activation_functions_count;
    for (quint32 i=0; i<ann->cascade_activation_functions_count; i++)
        ds				<< (qint32)ann->cascade_activation_functions[i];

    ds                  << ann->cascade_activation_steepnesses_count;
    for (quint32 i=0; i<ann->cascade_activation_steepnesses_count; i++)
        ds				<< ann->cascade_activation_steepnesses[i];

    for (fann_layer *layIt=ann->first_layer; layIt!=ann->last_layer; layIt++)
        ds				<< (qint32)(layIt->last_neuron-layIt->first_neuron);

    ds                  << (quint32)(ann->scale_mean_in ? 1 : 0);
    if (ann->scale_mean_in) {
        for (quint32 i=0; i<ann->num_input; i++)
            ds          << ann->scale_mean_in[i]
                        << ann->scale_deviation_in[i]
                        << ann->scale_new_min_in[i]
                        << ann->scale_factor_in[i];
        for (quint32 i=0; i<ann->num_output; i++)
            ds          << ann->scale_mean_out[i]
                        << ann->scale_deviation_out[i]
                        << ann->scale_new_min_out[i]
                        << ann->scale_factor_out[i];
    }

    for(fann_layer *layIt=ann->first_layer; layIt!=ann->last_layer; layIt++)
        for (fann_neuron *nrnIt=layIt->first_neuron; nrnIt!=layIt->last_neuron; nrnIt++)
            ds          << (qint32)nrnIt->last_con-nrnIt->first_con
                        << (qint32)nrnIt->activation_function
                        << (float)nrnIt->activation_steepness;

    for (quint32 i=0; i<ann->total_connections; i++)
        ds              << (qint32)(ann->connections[i]-ann->first_layer->first_neuron)
                        << ann->weights[i];

    // Finally return result.
    return ad;
}
fann* RE::Engine::binaryToAnn (QByteArray &ad) {
    if (!ad.count()) {
        Wrn                     << "Invalid ANN binary data!";
        return                  0;
    }
    fann				*ann	= 0;
    // Data stream.
    QBuffer				buf		(&ad);
    buf.open					(QIODevice::ReadOnly);
    QDataStream			ds		(&buf);
    // Work vars.
    qint32              tmp;

    // First load unstreamable types.
    ds							>> tmp;
    ann                         = fann_allocate_structure(tmp);
    ds                          >> tmp;
    ann->network_type           = (fann_nettype_enum)tmp;
    ds                          >> tmp;
    ann->training_algorithm     = (fann_train_enum)tmp;
    ds                          >> tmp;
    ann->train_error_function   = (fann_errorfunc_enum)tmp;
    ds                          >> tmp;
    ann->train_stop_function    = (fann_stopfunc_enum)tmp;
    // Proceed with easy types.
    ds                          >> ann->learning_rate
                                >> ann->connection_rate
                                >> ann->learning_momentum
                                >> ann->cascade_output_change_fraction
                                >> ann->quickprop_decay
                                >> ann->quickprop_mu
                                >> ann->rprop_increase_factor
                                >> ann->rprop_decrease_factor
                                >> ann->rprop_delta_min
                                >> ann->rprop_delta_max
                                >> ann->rprop_delta_zero
                                >> ann->cascade_output_stagnation_epochs
                                >> ann->cascade_candidate_change_fraction
                                >> ann->cascade_candidate_stagnation_epochs
                                >> ann->cascade_max_out_epochs
                                >> ann->cascade_min_out_epochs
                                >> ann->cascade_max_cand_epochs
                                >> ann->cascade_min_cand_epochs
                                >> ann->cascade_num_candidate_groups
                                >> ann->bit_fail_limit
                                >> ann->cascade_candidate_limit
                                >> ann->cascade_weight_multiplier;
    // Arrays.
    ds                          >> ann->cascade_activation_functions_count;
    ann->cascade_activation_functions	= (fann_activationfunc_enum*)realloc(ann->cascade_activation_functions, ann->cascade_activation_functions_count*sizeof(fann_activationfunc_enum));
    for (quint32 i=0; i<ann->cascade_activation_functions_count; i++) {
        ds						>> tmp;
        ann->cascade_activation_functions[i]
                                = (fann_activationfunc_enum)tmp;
    }

    ds                          >> ann->cascade_activation_steepnesses_count;
    ann->cascade_activation_steepnesses	= (fann_type*)realloc(ann->cascade_activation_steepnesses, ann->cascade_activation_steepnesses_count*sizeof(fann_type));
    for(quint32 i=0; i<ann->cascade_activation_steepnesses_count; i++)
        ds						>> ann->cascade_activation_steepnesses[i];

    for(fann_layer *layIt=ann->first_layer; layIt!=ann->last_layer; layIt++) {
        ds						>> tmp;
        layIt->first_neuron     = 0;
        layIt->last_neuron      = layIt->first_neuron+tmp;
        ann->total_neurons		+= tmp;
    }
    ann->num_input				= ann->first_layer->last_neuron-ann->first_layer->first_neuron-1;
    ann->num_output				= ((ann->last_layer-1)->last_neuron-(ann->last_layer-1)->first_neuron);
    if(ann->network_type==FANN_NETTYPE_LAYER)
        ann->num_output--;

    ds                          >> tmp;
    if (tmp) {
        for (quint32 i=0; i<ann->num_input; i++)
            ds                  >> ann->scale_deviation_in[i]
                                >> ann->scale_new_min_in[i]
                                >> ann->scale_factor_in[i];
        for (quint32 i=0; i<ann->num_output; i++)
            ds                  >> ann->scale_mean_out[i]
                                >> ann->scale_deviation_out[i]
                                >> ann->scale_new_min_out[i]
                                >> ann->scale_factor_out[i];
    }
    fann_allocate_neurons		(ann);

    for (fann_neuron *nrnIt=ann->first_layer->first_neuron; nrnIt!=(ann->last_layer-1)->last_neuron; nrnIt++) {
        qint32                  tmp2;
        float                   tmp3;
        ds                      >> tmp
                                >> tmp2
                                >> tmp3;
        nrnIt->activation_function
                                = (fann_activationfunc_enum)tmp2;
        nrnIt->activation_steepness
                                = (float)tmp3;
        nrnIt->first_con        = ann->total_connections;
        ann->total_connections  += tmp;
        nrnIt->last_con         = ann->total_connections;
    }
    fann_allocate_connections	(ann);

    for (quint32 i=0; i<ann->total_connections; i++) {
        ds                      >> tmp
                                >> ann->weights[i];
        ann->connections[i]     = ann->first_layer->first_neuron+tmp;
    }

    // Finally return result.
    return						ann;
}

QByteArray RE::Engine::signatureForImage (QImage const &img, quint32 const &pcc) const {
    if (img.isNull()) {
        Crt << "Invalid image to compute signature from!";
        return QByteArray();
    }
    quint32 const	sigW		= DocumentTypesSignatureSize().width();
    quint32 const	sigH		= DocumentTypesSignatureSize().height();

    QImage          edited      = img;
    edited                      = ObjectExtractor::Apply            (10, QColor::fromRgb(0x00,0x00,0x00), QColor::fromRgb(0xaa,0xaa,0xaa), true, true, true, true, edited, pcc, true);

    quint32 const	bordW		= edited.width()/100.0*5.0;
    quint32 const	bordH		= edited.height()/100.0*5.0;
    edited						= edited.copy                       (QRect(bordW, bordH, edited.width()-2*bordW, edited.height()-2*bordH)).scaledToHeight(200, Qt::SmoothTransformation);
    edited						= BrightnessContrastFilter::Apply	(.1, .3, edited, pcc, false);
    edited						= HistogramStretcher::Apply			(75, 180, edited, pcc, false);
    edited                      = ConvolutionMatrixFilter::Apply    (RE::ConvolutionMatrixFilter::Smoothing_9x1(), edited, pcc, false);
    edited						= HistogramEqualizer::Apply			(edited, pcc, false);
    edited						= edited.scaled						(sigW+2, sigH+2, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    edited						= ConvolutionMatrixFilter::Apply	(RE::ConvolutionMatrixFilter::HomemadeSpecial(), edited, pcc, false);
    edited                      = RE::Engine::paletizeIfGrayscale   (edited.copy(1, 1, sigW, sigH), false);
    QByteArray      sig;
    PixelReader<quint8> const
                    pr          (&edited);
    for (qint32 y=0; y<pr.height; ++y) {
        if (y%2==0)
            for (quint32 x=0; x<sigW; ++x)
                sig.append(pr.byte(x, y, 0));
        else
            for (quint32 x=0; x<sigW; ++x)
                sig.append(pr.byte(sigW-1-x, y, 0));
    }
    return sig;
}

QVector<QRgb> const&  RE::Engine::grayscalePalette () {
    // Create indexed palette.
    static QVector<QRgb> pal;
    if (pal.size()==0) {
        pal.reserve             (256);
        for (quint16 i=0; i<256; ++i)
            pal					<< qRgb(i,i,i);
    }
    return pal;
}
QImage RE::Engine::paletizeIfGrayscale (QImage const &img, bool force) {
    if (img.format()==QImage::Format_Indexed8)
        return              img;
    else if (force) {
        QImage      ret     = img.convertToFormat(QImage::Format_Indexed8, grayscalePalette());
        ret.setColorTable   (RE::Engine::grayscalePalette());
        ret.setColorCount   (256);
        return              ret;
    }
    else
        return              img;
}


// Plugins stuff.
RE::PluginsManager& RE::Engine::pluginsManager () {
    return				*_pluginsManager;
}
void RE::Engine::parsePlugins () {
    Dbg											<< "Loading plugins...";
    // Cache available plugins list.
    foreach (PluginBaseInfos const &bi, _pluginsManager->loadedPlugins()) {
        // Try to get a handle on the plugin's root object.
        AbstractPlugin 		*plg				= _pluginsManager->rootInstance(bi);
        // Handle failure...
        if (!plg) {
            Crt									<< "Plugin" << bi.name << "appears to be loaded but is not providing a root instance!";
            continue;
        }
        // Register each filter with the factory.
        foreach (QMetaObject const *mo, plg->filters()) {
            Dbg									<< "Caching information on" << mo->className() << "...";
            _filtersFactory.registerClass		(mo);
            _objectsLocations[mo->className()]	= plg->baseInfos().name;
        }
        // Register each recognizers with the factory.
        foreach (QMetaObject const *mo, plg->recognizers()) {
            Dbg									<< "Caching information on" << mo->className() << "...";
            _recognizersFactory.registerClass	(mo);
            _objectsLocations[mo->className()]	= plg->baseInfos().name;
        }
        // Add any enumerations that this plugin may expose.
        for (MetaTypesRangeListHash::const_iterator i=bi.enumeratorRanges.constBegin(); i!=bi.enumeratorRanges.constEnd(); ++i)
            _enumeratorRanges[i.key()]             << i.value();
    }

    // Initialize parameters for instantiable filters.
    Dbg										<< "Caching Image Filters parameters for faster instanciation...";
    ObjectsFactory::CacheHash const	&rf		= _filtersFactory.registeredTypes();
    for (ObjectsFactory::CacheHash::const_iterator i=rf.constBegin(); i!=rf.constEnd(); ++i) {
        QString const				&n		= i.key();
        if (_filtersParameters.contains(n))	continue;
        RE::AbstractFilter			*aif	= (RE::AbstractFilter*)_filtersFactory.instanciateClass(n);
        _filtersParameters[n]				= defaultObjectParameters(aif);
        // PQR Leak: use delete instead of destroy.
        delete aif;
        //QMetaType::destroy					(QMetaType::type(n.toUtf8().constData()), aif);
    }
    // Initialize parameters for instantiable recognizers.
    Dbg										<< "Caching Image Recognizers parameters for faster instanciation...";
    ObjectsFactory::CacheHash const	&rr		= _recognizersFactory.registeredTypes();
    for (ObjectsFactory::CacheHash::const_iterator i=rr.constBegin(); i!=rr.constEnd(); ++i) {
        QString const				&n		= i.key();
        if (_recognizersParameters.contains(n))	continue;
        RE::AbstractRecognizer		*air	= (RE::AbstractRecognizer*)_recognizersFactory.instanciateClass(n);
        _recognizersParameters[n]			= defaultObjectParameters(air);
        // PQR Leak: use delete instead of destroy.
        delete air;
        // QMetaType::destroy					(QMetaType::type(n.toUtf8().constData()), air);
    }
}
