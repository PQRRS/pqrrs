# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   RELibrary
TEMPLATE        =   lib
CONFIG          +=  shared
QT              +=  script gui

include         (../../LMLibrary.pri)

target.path     =   $${INSTALLDIR}/lib
INSTALLS        +=  target

VERSION             =   $${RELibraryVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)
message             (REClientBinary is at version $${REClientBinaryVersion}.)
checkTouchVersion   ("REClientBinary", $${REClientBinaryVersion})

DEFINES             +=  RELib FANN_NO_DLL GRAPHICS_DISABLED USE_STD_NAMESPACE

unix|linux: {
    INCLUDEPATH     +=  /opt/PQRRS/include
    LIBS            +=  -L/opt/PQRRS/lib -ltesseract
    LIBS            +=  -lzbar -ldmtx
}
win32 {
    # Windows-specific precompilation options and libs.
    DEFINES         +=  __MSW32__ WIN32 NDEBUG
    LIBS            +=  -luser32 -lkernel32
    # Tesseract includes and libs.
    INCLUDEPATH     +=  $${BASEDIR}/Dependencies/tesseract-3.02/include
    LIBS            +=  -L"$${BASEDIR}/Dependencies/tesseract-3.02/$$ARCH/vc11/lib" -llibtesseract302
    # ZBar includes and libs.
    contains(ARCH, "x86") {
        INCLUDEPATH +=  $${BASEDIR}/Dependencies/zbar-0.10/include
        LIBS        +=  -L"$${BASEDIR}/Dependencies/zbar-0.10/$$ARCH/vc11/lib" -llibzbar-0
    }
    # LibDMTX includes and libs.
    INCLUDEPATH     +=  $${BASEDIR}/Dependencies/libdmtx-0.7.4/include
    LIBS            +=  -L"$${BASEDIR}/Dependencies/libdmtx-0.7.4/$$ARCH/vc11/lib" -llibdmtx
    # Other things.
    HEADERS         +=  ThirdParty/win32/include/QtMfcApp/qmfcapp.h
    SOURCES         +=  ThirdParty/win32/include/QtMfcApp/qmfcapp.cpp
}

HEADERS         +=  src/Analyzers/AbstractAnalyzer.h \
                    src/Analyzers/BasicHistogram.h \
                    src/Analyzers/CumulativeHistogram.h \
                    src/Analyzers/Recognizers/AbstractRecognizer.h \
                    src/Analyzers/Recognizers/BarcodeReader.h \
                    src/Analyzers/Recognizers/DataMatrixReader.h \
                    src/Analyzers/Recognizers/DummyRecognizer.h \
                    src/Analyzers/Recognizers/OpticalCharacterRecognizer.h \
                    src/Analyzers/Recognizers/RawOpticalCharacterRecognizer.h \
                    src/Analyzers/RectangleLocator.h \
                    src/Engine.h \
                    src/Fann/floatfann.h \
                    src/Filters/AbstractFilter.h \
                    src/Filters/BrightnessContrastFilter.h \
                    src/Filters/ColorsRemover.h \
                    src/Filters/ConvolutionMatrixFilter.h \
                    src/Filters/Cropper.h \
                    src/Filters/HistogramEqualizer.h \
                    src/Filters/HistogramStretcher.h \
                    src/Filters/LocalThresholder.h \
                    src/Filters/MedianFilter.h \
                    src/Filters/ObjectExtractor.h \
                    src/Filters/OpticalTiltCorrector.h \
                    src/Filters/Resizer.h \
                    src/Filters/TiltCorrector.h \
                    src/Matrix.hpp \
                    src/ObjectsFactory.hpp \
                    src/PixelHelper.hpp \
                    src/Plugins/AbstractPlugin.h \
                    src/Plugins/PluginLoader.h \
                    src/Plugins/PluginsManager.h \
                    src/RELibrary.h \
                    src/RETypes.h \
                    src/ScriptingWrappers.h \
                    src/TypeWrapper.hpp

SOURCES         +=  src/Analyzers/AbstractAnalyzer.cpp \
                    src/Analyzers/BasicHistogram.cpp \
                    src/Analyzers/CumulativeHistogram.cpp \
                    src/Analyzers/Recognizers/AbstractRecognizer.cpp \
                    src/Analyzers/Recognizers/BarcodeReader.cpp \
                    src/Analyzers/Recognizers/DataMatrixReader.cpp \
                    src/Analyzers/Recognizers/DummyRecognizer.cpp \
                    src/Analyzers/Recognizers/OpticalCharacterRecognizer.cpp \
                    src/Analyzers/Recognizers/RawOpticalCharacterRecognizer.cpp \
                    src/Analyzers/RectangleLocator.cpp \
                    src/Engine.cpp \
                    src/Fann/floatfann.c \
                    src/Filters/AbstractFilter.cpp \
                    src/Filters/BrightnessContrastFilter.cpp \
                    src/Filters/ColorsRemover.cpp \
                    src/Filters/ConvolutionMatrixFilter.cpp \
                    src/Filters/Cropper.cpp \
                    src/Filters/HistogramEqualizer.cpp \
                    src/Filters/HistogramStretcher.cpp \
                    src/Filters/LocalThresholder.cpp \
                    src/Filters/MedianFilter.cpp \
                    src/Filters/ObjectExtractor.cpp \
                    src/Filters/OpticalTiltCorrector.cpp \
                    src/Filters/Resizer.cpp \
                    src/Filters/TiltCorrector.cpp \
                    src/Plugins/AbstractPlugin.cpp \
                    src/Plugins/PluginLoader.cpp \
                    src/Plugins/PluginsManager.cpp \
                    src/RELibrary.cpp \
                    src/RETypes.cpp
