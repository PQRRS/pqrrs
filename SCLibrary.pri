INCLUDEPATH             +=  $${BASEDIR}/SC/SCLibrary/include
DEPENDPATH              +=  $${BASEDIR}/SC/SCLibrary
LIBS                    +=  -L"$${MAINBINDIR}"
unix|linux:LIBS         +=  -lSCLibrary
win32:LIBS              +=  -lSCLibrary$${SCLibraryMajVersion}
DEFINES                 +=  SCLibCppStyle

win32:PRE_TARGETDEPS    +=  $${MAINBINDIR}/SCLibrary$${RELibraryMajVersion}.dll
linux:PRE_TARGETDEPS    +=  $${MAINBINDIR}/libSCLibrary.so
macx:PRE_TARGETDEPS     +=  $${MAINBINDIR}/libSCLibrary.dylib
