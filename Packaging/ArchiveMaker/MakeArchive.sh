#!/bin/bash

DISTNAME=`lsb_release -si`
DISTVERS=`lsb_release -sr`
DISTARCH=`uname -m | sed 's/x86_//;s/i[3-6]86/32/'`

BASENAME=$(basename "$1")
FILENAME="${BASENAME%.*}"
TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
ARCHIVENAME="${DISTNAME}${DISTVERS}_x${DISTARCH}_${FILENAME}_${TIMESTAMP}.tar.gz"

echo "Creating ${ARCHIVENAME}..."
tar czXfv "Exclusions.txt" "${ARCHIVENAME}" `cat $1`
