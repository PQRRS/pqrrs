#!/bin/sh

echo "--------- Begining to copy files -----------"

PKGPFX="packages/fr.PierreQR"
echo "Prefix is now $PKGPFX"

COMPRESS="/c/Program Files/7-Zip/7z.exe"
function archiveHelper() {
    echo "    $1"
    OLDDIR=`pwd`
    cd $2
    "$COMPRESS" a -bd -y ../data/data.7z * > /dev/null
    cd $OLDDIR
}

PKGBASE=RecognitionSuite.RELibrary.Base
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/RELibrary*.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.RELibrary.Plugins.REPlugA2iA
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/REPlugA2iA*.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.RELibrary.Plugins.REPlugFujiTools
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/REPlugFujiTools*.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.RELibrary.Plugins.REPlugOpenCV
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/REPlugOpenCV*.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.RELibrary.API
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH/API/lib
mkdir -p $PKGPATH/API/include
cp ../../RE/RELibrary/src/RELibrary.h $PKGPATH/API/include/
cp ../../RE/RELibrary/src/RETypes.h $PKGPATH/API/include/
archiveHelper $PKGBASE $PKGPATH


PKGBASE=RecognitionSuite.SCLibrary.Base
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/SCLibrary*.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.SCLibrary.API
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH/API/lib
mkdir -p $PKGPATH/API/include
cp ../../SC/SCLibrary/src/SCLibrary.h $PKGPATH/API/include/
cp ../../SC/SCLibrary/src/SCTypes.h $PKGPATH/API/include/
archiveHelper $PKGBASE $PKGPATH


PKGBASE=RecognitionSuite.Gui.RSUniversalUI.Base
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH/Images
cp ../../Release/RSUniversalUI.exe $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.Gui.RSUniversalUI.Themes.JdcMidiPy
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH/Themes
cp -R ../../GUIs/RSUniversalUI/src/Resources/Themes/JdcMidiPy.rsuuitheme $PKGPATH/Themes/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.Gui.RSUniversalUI.Themes.Capsys
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH/Themes
cp -R ../../GUIs/RSUniversalUI/src/Resources/Themes/Capsys.rsuuitheme $PKGPATH/Themes/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.Gui.SCLibraryTester
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/SCLibraryTester.exe $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

echo "    Updating version numbers"
OLDDIR=`pwd`
cd ../../Release
./VersionsUpdater
cd $OLDDIR
