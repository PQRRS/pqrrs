#!/bin/sh

echo "----- Begining to update repositories ------"

# Helper function.
REPOGEN="c:/opt/QtSDK/QtIFW/1.3/bin/repogen"
function packageHelperIncluding {
    echo "   Updating $CURPKG"
    CURPKG=$1
    INCPKGS=$2
    $REPOGEN \
        --update \
        --config config/$CURPKG.xml \
        --packages packages \
        --include $INCPKGS \
        ./QtIfwRepo/$CURPKG
}
function packageHelperExcluding {
    echo "   Updating $CURPKG"
    CURPKG=$1
    EXCPKGS=$2
    $REPOGEN \
        --update \
        --config config/$CURPKG.xml \
        --packages packages \
        --exclude $EXCPKGS \
        ./QtIfwRepo/$CURPKG
}

# ------------------------------------------------------------ #

# PQR
packageHelperExcluding "PQR" "None"
