#!/bin/sh

echo "------ Begining to copy dependencies -------"

PKGPFX="packages/fr.PierreQR"
echo "Prefix is now $PKGPFX"

COMPRESS="/c/Program Files/7-Zip/7z.exe"
function archiveHelper() {
    echo "    $1"
    OLDDIR=`pwd`
    cd $2
    "$COMPRESS" a -bd -y ../data/data.7z * > /dev/null
    cd $OLDDIR
}

PKGBASE=Qt.Core
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/QtCore4.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=Qt.Network
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/QtNetwork4.dll  $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=Qt.Script
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/QtScript4.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=Qt.Gui
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/QtGui4.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=Qt.Sql
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/QtSql4.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=Qt.Xml
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/QtXml4.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=Qt.Plugins.ImageFormats
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp -R ../../Release/imageformats $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=Qt.Plugins.IconEngines
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp -R ../../Release/iconengines $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=Qt.Plugins.SqlDrivers
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp -R ../../Release/sqldrivers $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.RELibrary.Dependencies
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/libiconv-2.dll $PKGPATH/
cp ../../Release/liblept168.dll $PKGPATH/
cp ../../Release/libtesseract302.dll $PKGPATH/
cp ../../Release/libzbar-0.dll $PKGPATH/
cp ../../Release/libdmtx.dll $PKGPATH/
cp -R ../../Release/tessdata $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.RELibrary.Plugins.REPlugA2iA.Dependencies
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/A2iACheckReader.dll $PKGPATH/
archiveHelper $PKGBASE $PKGPATH

PKGBASE=RecognitionSuite.RELibrary.Plugins.REPlugOpenCV.Dependencies
PKGPATH=$PKGPFX.$PKGBASE/files
mkdir -p $PKGPATH
cp ../../Release/opencv_core247.dll $PKGPATH/
cp ../../Release/opencv_imgproc247.dll  $PKGPATH/
archiveHelper $PKGBASE $PKGPATH
