#!/bin/sh

echo "----- Begining to build installers... ------"

BINARYCREATOR="c:/opt/QtSDK/QtIFW/1.3/bin/binarycreator"
INSTALLERBASE="c:/opt/QtSDK/QtIFW/1.3/bin/installerbase"
function installerHelper() {
    PKGNAME=$1
    EXCLPKG=$2
    echo "    Building ${PKGNAME} with config at config/${PKGNAME}.xml..."
    $BINARYCREATOR \
        --verbose \
        --template $INSTALLERBASE \
        --config config/${PKGNAME}.xml \
        --ignore-translations \
        --packages packages/ \
        --online-only \
        "${PKGNAME} Setup"
}

# Build PQR installer.
installerHelper "PQR" "None"
