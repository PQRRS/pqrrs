// Required stuff.
#include "UI/DlgMain.h"
#include <LMLibrary/Core>
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <QtCore/QDir>
#include <QtCore/QTranslator>
#include <QtGui/QStyleFactory>
#include <QtGui/QApplication>
#include <QtGui/QMessageBox>

int main (int argc, char *argv[]) {
    QApplication    a           (argc, argv);
    a.setQuitOnLastWindowClosed (true);
    a.setStyle                  (QStyleFactory::create("Plastique"));

    // Initialize licensing.
    //LM::Core        *lm         = &LM::Core::Instance();
    LM::Core        *lm         = new LM::Core(&a);
    lm->setParent               (&a);
    lm->loadFromFile(QString("%1/.PQRRSLicense.lic").arg(QDir::homePath()));

    // Instanciate stuff.
    RE::StaticInitializer       ();

    // Install translator.
    QTranslator     trans;
    QString         locale      = QLocale::system().name().left(2).toUpper();
    if (trans.load(QString(":/Translations/RELibraryAdmin_%1.qm").arg(locale)))
        qApp->installTranslator (&trans);

    // Initialize things, and display main window.
    POCSHC::DlgMain	w           (lm);
    QMessageBox     *d          = 0;
    // License ok.
    if (w.engine()->licensed())
        w.show                  ();
    // License wrong.
    else {
        d                       = new QMessageBox(QMessageBox::Critical, QObject::tr("Licensing Issue"), lm->lastMessage(), QMessageBox::Ok);
        QObject::connect        (d, SIGNAL(buttonClicked(QAbstractButton*)), &a, SLOT(quit()));
        d->show                 ();
    }

    int             ret			= a.exec();
    if (d)                      delete d;
    return                      ret;
}
