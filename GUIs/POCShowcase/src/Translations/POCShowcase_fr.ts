<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>QObject</name>
    <message>
        <source>Identity Card</source>
        <translation>Carte d&apos;Identité</translation>
    </message>
    <message>
        <source>Bank Identifier Code</source>
        <translation>Relevé d&apos;Identité Bancaire</translation>
    </message>
    <message>
        <source>Address Proof</source>
        <translation>Preuve d&apos;Adresse</translation>
    </message>
    <message>
        <source>Bank Check</source>
        <translation>Chèque Bancaire</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <source>Invalid</source>
        <translation>Invalide</translation>
    </message>
    <message>
        <source>Incomplete</source>
        <translation>Incomplet</translation>
    </message>
    <message>
        <source>Valid</source>
        <translation>Valide</translation>
    </message>
</context>
<context>
    <name>SHC::DocumentsModel</name>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Results</source>
        <translation>Resultats</translation>
    </message>
</context>
<context>
    <name>SHC::ResultsModel</name>
    <message>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>SHC::WndMain</name>
    <message>
        <source>Documents</source>
        <translation>Documents</translation>
    </message>
    <message>
        <source>Results</source>
        <translation>Resultats</translation>
    </message>
    <message>
        <source>Console</source>
        <translation>Console</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Nettoyer</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Sender</source>
        <translation>Origine</translation>
    </message>
    <message>
        <source>Method</source>
        <translation>Méthode</translation>
    </message>
    <message>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <source>Incorrect Type Detected</source>
        <translation>Type Incorrect Détecté</translation>
    </message>
    <message>
        <source>The document doesn&apos;t match the expected type (Expected: %1, Found: %2). Would you like to force the recognition as if it was a %3?</source>
        <translation>Le document ne correspond pas au type attendu (Attendu: %1, Trouvé: %2). Voullez-vous forcer la reconnaissance en tant que %3?</translation>
    </message>
    <message>
        <source>The bottom line is incorrectly formatted.</source>
        <translation type="obsolete">La ligne basse n&apos;est pas formattée correctement.</translation>
    </message>
    <message>
        <source>The document matches it&apos;s expected type, and the extracted data is valid.</source>
        <translation>Le type de document est correct, mais les données extraites sont invalides.</translation>
    </message>
    <message>
        <source>Recognition Demo</source>
        <translation>Démo Reconnaissance</translation>
    </message>
    <message>
        <source>This document didn&apos;t pass the security verification tests.</source>
        <translation>Ce document n&apos;a pas passé les tests de vérification.</translation>
    </message>
</context>
</TS>
