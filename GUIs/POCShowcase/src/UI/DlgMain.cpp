#include "DlgMain.h"
#include <LMLibrary/Core>
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include "../EditDistance.h"
#include <QtCore/QUrl>
#include <QtCore/QDebug>
#include <QtGui/QPixmap>
#include <QtCore/QThread>
#include <QtGui/QDropEvent>
#include <QtGui/QMessageBox>
#include <QtGui/QDragEnterEvent>

POCSHC::DlgMain::DlgMain(LM::Core *lm, QWidget *parent)
: QDialog(parent), _ui(new Ui::DlgMain) {
    _ui->setupUi                                    (this);
    setAcceptDrops                                  (true);

    if (1) {
        // Update window flags.
        setWindowFlags                              (Qt::Widget|Qt::FramelessWindowHint);
    }
    else {
        // Update window flags.
        setWindowFlags                              (Qt::Widget|Qt::FramelessWindowHint|Qt::X11BypassWindowManagerHint);
        // Make translucent background.
        QPalette            pal;
        pal.setColor                                (QPalette::Base, Qt::transparent);
        setPalette                                  (pal);
        setAttribute                                (Qt::WA_TranslucentBackground);
    }

    // Cache recognition engine.
    _re                                             = new RE::Engine(lm);
    _re->loadClientBinaryData                       (":/Resources/Required/POCShowcase.recb");

    _ui->lblAcceptationReasons->setVisible          (false);
    _ui->lblRejectionReasons->setVisible            (false);
}

void POCSHC::DlgMain::dragEnterEvent (QDragEnterEvent *e) {
    e->acceptProposedAction ();
}
void POCSHC::DlgMain::dropEvent (QDropEvent *e) {
    setImageFilename            (e->mimeData()->urls().first().toLocalFile());
}

void POCSHC::DlgMain::setImageFilename (QString const &fn) {
    QImage                  img     (fn);
    if (img.isNull()) {
        Crt                         << "The specified file isn't a recognized image format.";
        return;
    }

    // Create recognition document.
    RE::T::Document         d;
    // Assign image to document.
    d.images["Front"]               = img;
    // Force type code to document.
    quint32                 tc      = 1;
    d.typeDataCode                  = 1;
    // Prepare storable result.
    QMap<QString,QString>  res;
    _re->guessDocumentType          (&d, QThread::idealThreadCount());
    if (d.typeData->code==tc)
        _re->recognizeDocumentData  (&d, QThread::idealThreadCount());

    for (RE::T::StringToStringMap::const_iterator i=d.strings.constBegin(); i!=d.strings.constEnd(); ++i) {
        QString             k       = i.key();
        QString             v       = i.value();
        if (k=="Serial" && v.length()>12)
                v               = v.left(12);
        if (k=="Sex" || k=="Serial" || k=="MRZLine")
            v                   = v.replace(QRegExp("[-:;.]"), "");
        if (k=="BirthDate")
            v                   = v.replace(QRegExp("[-:; ]"), "");
        if (k=="LastName" || k=="FirstNames")
            v                   = v.replace(QRegExp("[:;.]"), "");
        if (k=="Sex")
            v                   = v.replace(' ', "");
        res[k]                  = v;
    }

    if (d.typeData->code!=tc) {
        if (QMessageBox::question(  this,
                                    tr("Incorrect Type Detected"),
                                    tr("The document doesn't match the expected type. Would you like to force the recognition anyway?"),
                                    QMessageBox::Yes,
                                    QMessageBox::No)==QMessageBox::No)
            return;
        d.typeDataCode                  = tc;
        foreach (RE::T::DocumentTypeData const &dt, _re->clientBinaryData().documentTypesData) {
            if ((qint32)dt.code==d.typeDataCode) {
                d.typeData              = &dt;
                break;
            }
        }
    }

    QString     last            = QString(res["LastName"]).trimmed().replace('0', "O");
    QString     first           = QString(res["FirstNames"]).trimmed().replace('0', "O");
    QString     birthdate       = QString(res["BirthDate"]).trimmed().remove(QRegExp("[-`~!@#$%^&*()_—+=|:;<>«»,.?/{}\'\"\\[\\]\\\\]"));
    QString     sex             = QString(res["Sex"]).trimmed().remove(QRegExp("[-`~!@#$%^&*()_—+=|:;<>«»,.?/{}\'\"\\[\\]\\\\]"));
    QString     serial          = QString(res["Serial"]).trimmed();
    QString     mrz             = QString(res["MRZLine"]).trimmed();
    // Advanced first / last cleanup.
    while (last.contains("  "))
        last                    = last.replace("  ", " ");
    while (first.contains("  "))
        first                   = first.replace("  ", " ");
    last                        = last.replace(", ", "<<").replace(",", "<<").replace(" - ", "-").replace("-", "<").replace(" ", "<");
    first                       = first.replace(", ", "<<").replace(",", "<<").replace(" - ", "-").replace("-", "<").replace(" ", "<");
    // Advanced mrz cleanup.
    QString     tmp;
    QStringList mrzs            = mrz.split("\n");
    tmp                         = mrzs[0];
    QString     mrz1            = tmp.left(30).replace("0", "O") + tmp.right(6).replace("O", "0");
    tmp                         = mrzs[1];
    QString     mrz2            = tmp.left(13).replace("O", "0") + tmp.mid(13, 14).replace("0", "O") + tmp.right(9).replace("O", "0");
    // Compute first distance.
    // IDFRAGOMES<FERREIRA<<<<<<<<<<<
    // IDFRAGOMES<FERREIRA<<<<<<<<<<<604118
    QString     top             = QString("IDFRA%1").arg(last.left(25));
    if (top.length()<30)
        top                     += QString("<").repeated(36-6-top.length());
    quint32     d1              = EditDistance().CalEditDistance(
                    top.toUtf8().constData(),
                    mrz1.left(30).toUtf8().constData(),
                    30
                );
    // Compute second distance.
    // 030260400626<ESLIE<<<<<<<<<800108<F
    // 03O2604006263ESLIE<<<<<<<<<8001081F5
    QString     bot             = serial + QString::number(serial.toInt()%3) + first.left(14);
    QString     botFin          = QString("%1%2%3<%4")
            .arg(birthdate.mid(6, 2))
            .arg(birthdate.mid(2, 2))
            .arg(birthdate.mid(0, 2))
            .arg(sex);
    bot                         += QString("<").repeated(36-bot.length()-9) + botFin;
    quint32     d2              = EditDistance().CalEditDistance(
                    bot.toUtf8().constData(),
                    mrz2.left(35).toUtf8().constData(),
                    35
                )-2;
    quint32     dist            = d1+d2;
    if (dist>12)
        res["RejectionReason"]  = tr("This document didn't pass the security verification tests.");
    else
        res["AcceptationReason"]= tr("The document matches it's expected type, and the extracted data is valid.");

    bool rej = res.contains("RejectionReason");
    _ui->lblAcceptationReasons->setVisible(!rej);
    _ui->lblRejectionReasons->setVisible(rej);

    _ui->lblAcceptationReasons->setText(res["AcceptationReason"]);
    _ui->lblRejectionReasons->setText(res["RejectionReason"]);

    _ui->txtLastName->setText       (res["LastName"].replace('0', "O"));
    _ui->txtFirstNames->setText     (res["FirstNames"]);
    _ui->txtIdNumber->setText       (mrz2.left(12));
    _ui->txtBirthDate->setText      (mrz2.mid(31,2) + "/" + mrz2.mid(29,2) + "/" + mrz2.mid(27,2));
    _ui->txtSex->setText            (mrz2.mid(34,1)=="M" ? tr("Male") : tr("Female"));
    _ui->txtMrzLine->setPlainText   (mrz1 + "\n" + mrz2);

    _ui->wgtDocumentPicture->setImage   (d.images["Front"]);
    _ui->wgtPhoto->setImage             (d.images["Photo"]);
    _ui->wgtSignature->setImage         (d.images["Signature"]);
}

POCSHC::DlgMain::~DlgMain() {
    delete          _ui;
}
