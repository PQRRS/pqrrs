#ifndef POCSHC_DlgMain_h
#define POCSHC_DlgMain_h

#include "ui_DlgMain.h"
#include <QtGui/QDialog>
#include <QtGui/QItemSelection>

// Forward declarations.
namespace LM {
    class Core;
}
namespace RE {
    class Engine;
}
namespace POCSHC {
    class ResultsModel;
}

namespace POCSHC {
    class Core;
}

namespace POCSHC {

class DlgMain : public QDialog {
Q_OBJECT
public:
    explicit            DlgMain                             (LM::Core *lm, QWidget *p=0);
    virtual             ~DlgMain                            ();
    // Accessors.
    RE::Engine*         engine                              () const                { return _re; }
    void                dragEnterEvent                      (QDragEnterEvent *e);
    void                dropEvent                           (QDropEvent *e);

public slots:
    // Other actions.
    void                setImageFilename                    (QString const &fn);

private:
    // UI Object.
    POCSHC::Ui::DlgMain *_ui;
    // Recognition engine.
    RE::Engine          *_re;
};

}

#endif
