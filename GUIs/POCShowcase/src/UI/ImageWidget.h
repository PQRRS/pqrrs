#ifndef POSSHC_ImageWidget_h
#define POSSHC_ImageWidget_h

// Base class.
#include <QtGui/QWidget>
// Required stuff.
#include <QtGui/QImage>
#include <QtGui/QPainter>
#include <QtGui/QStyleOption>
#include <QtGui/QApplication>
#include <QtCore/QDebug>

namespace POCSHC {

class ImageWidget : public QWidget {
Q_OBJECT
public:
    /**/			ImageWidget			(QWidget *p=0) : QWidget(p) {}
    void			setImage			(QImage const &img) {
        _image			= img;
        update			();
    };
protected:
    void			paintEvent			(QPaintEvent*) {
        if (_image.isNull())		return;
        QStyleOption			opt;
        opt.init					(this);
        QPainter				p	(this);
        style()->drawPrimitive		(QStyle::PE_Widget, &opt, &p, this);
        p.drawImage                 (0, 0, _image.scaled(width(), height()));
        p.end						();
    };
private:
    QImage			_image;
};

}

#endif
