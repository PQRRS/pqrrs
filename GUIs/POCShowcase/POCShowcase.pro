# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   POCShowcase
TEMPLATE        =   app
QT              +=  gui

include         (../../LMLibrary.pri)
include         (../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@executable_path/../../../libRELibrary.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}"
}

target.path     =   $${INSTALLDIR}/bin
INSTALLS        +=  target

VERSION             =   $${POCShowcaseVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

TRANSLATIONS    +=  src/Translations/POCShowcase_fr.ts
RESOURCES       +=  src/POCShowcase.qrc
FORMS           +=  src/Forms/DlgMain.ui
HEADERS         +=  src/UI/DlgMain.h \
                    src/EditDistance.h \
                    src/UI/StyledWidgets.h \
                    src/UI/ImageWidget.h
SOURCES         +=  src/main.cpp \
                    src/UI/DlgMain.cpp \
                    src/EditDistance.cpp
