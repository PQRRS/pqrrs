# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   RSUniversalUI
TEMPLATE        =   app
QT              +=  gui sql network
BIN_TARGET      =   "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}" && \

include         (../../LMLibrary.pri)
include         (../../RELibrary.pri)
include         (../../SCLibrary.pri)
macx: {
    QMAKE_POST_LINK +=  install_name_tool \
                            -change \
                                "libLMLibrary.0.dylib" \
                                "@executable_path/../../../libLMLibrary.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}" && \
                        install_name_tool \
                            -change \
                                "libRELibrary.0.dylib" \
                                "@executable_path/../../../libRELibrary.dylib" \
                                "\"$${DESTDIR}\"/$${TARGET}.app/Contents/MacOS/$${TARGET}" && \
                        install_name_tool \
                            -change \
                                "libSCLibrary.0.dylib" \
                                "@executable_path/../../../libSCLibrary.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}"
}

target.path     =   $${INSTALLDIR}/bin
INSTALLS        +=  target

VERSION             =   $${RSUniversalUIVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

TRANSLATIONS    +=  src/Translations/RSUniversalUI_fr.ts
RESOURCES       +=  src/RSUniversalUI.qrc
HEADERS         +=  ThirdParty/QtSingleApplication/src/QtLockedFile \
                    ThirdParty/QtSingleApplication/src/QtSingleApplication \
                    ThirdParty/QtSingleApplication/src/qtsingleapplication.h \
                    ThirdParty/QtSingleApplication/src/qtlockedfile.h \
                    ThirdParty/QtSingleApplication/src/qtlocalpeer.h \
                    ThirdParty/QtSingleApplication/src/qtsinglecoreapplication.h \
                    src/Core.h \
                    src/RSUUITypes.h \
                    src/Plugins/PluginsManager.h \
                    src/Plugins/PluginLoader.h \
                    src/Plugins/AbstractPlugin.h \
                    src/Models/DocumentsModel.h \
                    src/Models/DocumentTypesModel.h \
                    src/Models/IssuersModel.h \
                    src/Models/SlipsModel.h \
                    src/UI/ImageWidget.h \
                    src/UI/ItemDelegates.h \
                    src/UI/StyledWidgets.h \
                    src/UI/Dialogs/DlgDocumentFix.h \
                    src/UI/Dialogs/DlgMain.h \
                    src/UI/Dialogs/DlgSettings.h \
                    src/UI/Dialogs/DlgSlips.h \
                    src/Models/SettingsModel.h \
                    src/Utilities/ThemeManager.h \
                    src/Utilities/DocumentUtility.h \
                    src/Utilities/DatabaseManager.h \
                    src/Workers/ThreadedWorker.h
SOURCES         +=  ThirdParty/QtSingleApplication/src/qtsingleapplication.cpp \
                    ThirdParty/QtSingleApplication/src/qtlockedfile_win.cpp \
                    ThirdParty/QtSingleApplication/src/qtlockedfile.cpp \
                    ThirdParty/QtSingleApplication/src/qtlocalpeer.cpp \
                    ThirdParty/QtSingleApplication/src/qtsinglecoreapplication.cpp \
                    src/main.cpp \
                    src/Core.cpp \
                    src/RSUUITypes.cpp \
                    src/Plugins/PluginsManager.cpp \
                    src/Plugins/PluginLoader.cpp \
                    src/Plugins/AbstractPlugin.cpp \
                    src/Models/DocumentsModel.cpp \
                    src/Models/DocumentTypesModel.cpp \
                    src/Models/IssuersModel.cpp \
                    src/Models/SlipsModel.cpp \
                    src/UI/ItemDelegates.cpp \
                    src/UI/Dialogs/DlgDocumentFix.cpp \
                    src/UI/Dialogs/DlgMain.cpp \
                    src/UI/Dialogs/DlgSettings.cpp \
                    src/UI/Dialogs/DlgSlips.cpp \
                    src/Models/SettingsModel.cpp \
                    src/Utilities/ThemeManager.cpp \
                    src/Utilities/DocumentUtility.cpp \
                    src/Utilities/DatabaseManager.cpp
FORMS           +=  src/Forms/DlgDocumentFix.ui \
                    src/Forms/DlgMain.ui \
                    src/Forms/DlgSettings.ui \
                    src/Forms/DlgSlips.ui

