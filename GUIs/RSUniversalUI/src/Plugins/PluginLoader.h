#ifndef RSUUI_PluginLoader_h
#define RSUUI_PluginLoader_h

// Base class.
#include <QtCore/QPluginLoader>
// Other stuff.
#include "../RSUUITypes.h"
#include <QtCore/QList>
#include <QtCore/QStringList>
namespace RSUUI {
	class AbstractPlugin;
}

namespace RSUUI {

class PluginLoader : public QPluginLoader {
public:
    Q_OBJECT
    Q_PROPERTY(RSUUI::PluginBaseInfos	baseInfos	READ baseInfos)

public:
	// To allow instanciation via the plugins manager...
	friend class							PluginsManager;
	// Types.
	typedef QList<PluginLoader*>			PointerList;

protected:
	// Constructors.
	/**/									PluginLoader		(QString const &fileName, QObject *parent=0);
	// Destructors.
	virtual									~PluginLoader		();
	
	// Load / Unload.
	bool									load				();
	bool									unload				();

public:
	// Get contained instance.
	AbstractPlugin*							instance			();
	// Accessors.
    RSUUI::PluginBaseInfos const&			baseInfos			() const;
	// Metainfo accessors.

private:
	// Base infos.
	QString									_fileBaseName;
    RSUUI::PluginBaseInfos					_baseInfos;
};

}

#endif
