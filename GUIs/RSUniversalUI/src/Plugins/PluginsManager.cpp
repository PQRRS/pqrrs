#include "PluginsManager.h"
#include "AbstractPlugin.h"
#include "PluginLoader.h"
#include <QtCore/QDir>
#include <QtCore/QDebug>

RSUUI::PluginsManager::Cache RSUUI::PluginsManager::_cacheByName;
RSUUI::PluginsManager::Cache RSUUI::PluginsManager::_cacheByFileName;

RSUUI::PluginsManager::PluginsManager (QString const &path, QObject *p)
:QObject(p) {
    // Instanciate directory object.
    QDir				d		(path);
    // Loop for each found plugin.
    QStringList					fltrs;
    #if defined(Q_OS_WIN)
    fltrs						<< "REPlug*.dll";
    #elif defined(Q_OS_MACX)
    fltrs						<< "libREPlug*.dylib";
    #elif defined(Q_OS_UNIX)
    fltrs						<< "libREPlug*.so";
    #endif
    foreach (QString const &fi, d.entryList(fltrs, QDir::Files)) {
        Dbg						<< "Discovered plugin file" << fi << ".";
        delete PluginsManager::loadFile(d.absoluteFilePath(fi));
    }
}
RSUUI::PluginsManager::~PluginsManager () {
    for (Cache::const_iterator i=_cacheByFileName.constBegin(); i!=_cacheByFileName.constEnd(); ++i)
        unload					(i.value().first);
}

RSUUI::PluginBaseInfos::List RSUUI::PluginsManager::loadedPlugins () const {
    PluginBaseInfos::List bis;
    for (Cache::const_iterator i=_cacheByFileName.constBegin(); i!=_cacheByFileName.constEnd(); ++i)
        bis						<< i.value().first;
    return						bis;
}
RSUUI::AbstractPlugin* RSUUI::PluginsManager::rootInstance (PluginBaseInfos const &bi) const {
    PluginLoader	*plgLdr		= _cacheByName.value(bi.name).second;
    return	plgLdr ? plgLdr->instance() : 0;
}

RSUUI::PluginLoader* RSUUI::PluginsManager::loadFile (QString const &fn) {
    QString			bn			= QFileInfo(fn).baseName();
    if (_cacheByFileName.contains(bn)) {
        Dbg						<< "Plugin" << bn << "is in cache, not reloading.";
        return					_cacheByFileName[bn].second;
    }
    PluginLoader	*plgLdr		= new PluginLoader(fn);
    if (!plgLdr->load()) {
        Wrn						<< "Loading of plugin" << bn << "failed!";
        delete					plgLdr;
        return					0;
    }
    PluginBaseInfos
            const	&bi			= plgLdr->baseInfos();
    _cacheByName[bi.name]		= qMakePair(bi, plgLdr);
    _cacheByFileName[bn]		= qMakePair(bi, plgLdr);
    return						plgLdr;
}
bool RSUUI::PluginsManager::unload (PluginBaseInfos const &bi) {
    if (!_cacheByName.contains(bi.name))
        return					true;
    PluginLoader		*plgLdr	= _cacheByName.value(bi.name).second;
    if (!plgLdr->isLoaded())	return true;
    Dbg							<< "Unloading" << bi.name << "...";
    if (!plgLdr->unload()) {
        Wrn						<< "Cannot unload plugin" << bi.name << "!";
        return					false;
    }
    return						true;
}
