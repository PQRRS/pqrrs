#ifndef RSUUI_PluginsManager_h
#define RSUUI_PluginsManager_h

// Base class.
#include <QtCore/QObject>
// Other stuff.
#include "../RSUUITypes.h"
#include "PluginLoader.h"
#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtCore/QString>
namespace RSUUI {
    class AbstractPlugin;
}

namespace RSUUI {

class PluginsManager : public QObject {
Q_OBJECT
public:
    // Constructors.
    /**/						PluginsManager		(QString const &path, QObject *p=0);
    // Destructor.
    virtual						~PluginsManager		();
    // Query methods.
    PluginBaseInfos::List		loadedPlugins		() const;
    RSUUI::AbstractPlugin*		rootInstance		(PluginBaseInfos const &bi) const;

protected:
    // Loading methods.
    bool						load				(PluginBaseInfos const &bi);
    bool						unload				(PluginBaseInfos const &bi);
    PluginLoader*				loadFile			(QString const &fileName);

private:
    // Types.
    typedef QMap< QString,QPair <PluginBaseInfos,PluginLoader*> > Cache;
    // Global plugins cache.
    static Cache				_cacheByName;
    static Cache				_cacheByFileName;
};

}

#endif
