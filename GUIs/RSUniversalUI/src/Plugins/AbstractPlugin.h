#ifndef RSUUI_AbstractPlugin_h
#define RSUUI_AbstractPlugin_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include "../RSUUITypes.h"

namespace RSUUI {

class AbstractPlugin : public QObject {
Q_OBJECT
protected:
	// Constructors.
	/**/							AbstractPlugin	(QObject *p=0);
public:
	// Plugin information strings.
	virtual PluginBaseInfos			baseInfos		() const	= 0;
	// Plugin functionality.
};

}

// Plugin interface.
Q_DECLARE_INTERFACE(RSUUI::AbstractPlugin, "fr.PierreQR.RecognitionSuite.RELibrary.AbstractPlugin/1.0")

#endif
