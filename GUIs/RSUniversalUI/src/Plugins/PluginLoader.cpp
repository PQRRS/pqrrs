#include "PluginLoader.h"
#include "AbstractPlugin.h"
#include <QtCore/QMetaObject>
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>

// Constructors.
RSUUI::PluginLoader::PluginLoader (QString const &fn, QObject *p)
: QPluginLoader(fn, p), _fileBaseName(QFileInfo(fn).baseName()) {
}
RSUUI::PluginLoader::~PluginLoader () {
	if (isLoaded())
		Wrn						<< "Destructing plugin" << _fileBaseName << "while still loaded!";
}

// Load the plugin.
bool RSUUI::PluginLoader::load () {
    // Already loaded?
	if (isLoaded())				return true;
	// Try to load the library...
	else if (!QPluginLoader::load()) {
		Wrn						<< "Cannot load plugin library" << _fileBaseName << ":" << errorString() << "!";
		return					false;
	}
	// See if plugin contains a AbstractPlugin instance...
	AbstractPlugin*	plg			= instance();
	if (!plg) {
		Wrn						<< "Cannot instanciate plugin" << _fileBaseName << "!";
		return					false;
	}
	Dbg							<< "Plugin" << _fileBaseName << "has been successfully loaded.";
	// Store plugin informations
	Dbg							<< "Loading plugin objects...";
	if (_baseInfos.name.isEmpty()) {
		PluginBaseInfos
					bi			= plg->baseInfos();
		_baseInfos.name			= bi.name;
		_baseInfos.version		= bi.version;
		_baseInfos.description	= bi.description;
	}
	return true;
}
RSUUI::AbstractPlugin* RSUUI::PluginLoader::instance () {
	if (!isLoaded()) {
		Wrn							<< "The plugin" << _fileBaseName << "isn't loaded!";
		return 0;
	}
    RSUUI::AbstractPlugin	*toRet=qobject_cast<RSUUI::AbstractPlugin*>(QPluginLoader::instance());
	if (!toRet)
        Wrn							<< "The root object in library" << _fileBaseName << "failed to cast as a RSUUI::AbstractPlugin object!";
	return							toRet;
}
// Unload the plugin.
bool RSUUI::PluginLoader::unload () {
    // Already unloaded?
	if (!isLoaded())			return true;
	// Try to unload the plugin...
	if (QPluginLoader::unload())
		return					true;
	Wrn							<< "Unable to unload plugin" << _baseInfos.name << ":" << errorString() << "!";
	return						false;
}

// Accessors.
RSUUI::PluginBaseInfos const& RSUUI::PluginLoader::baseInfos () const {
	return _baseInfos;
}
