#include "ItemDelegates.h"
#include "../Core.h"
#include "../Models/DocumentsModel.h"
#include <QtCore/QDate>
#include <QtCore/QFileInfo>
#include <QApplication>
#include <QtGui/QImage>
#include <QtGui/QSpinBox>
#include <QtGui/QPainter>
#include <QtGui/QStyleOption>
#include <QtGui/QFontMetrics>

RSUUI::ReadOnly::ReadOnly (QObject *p) : QStyledItemDelegate(p) {}
QWidget* RSUUI::ReadOnly::createEditor (QWidget*, const QStyleOptionViewItem&, const QModelIndex&) const {
    return 0;
}

// Fancy date delegate.
RSUUI::FancyDate::FancyDate (QObject *p) : ReadOnly(p) {
}
void RSUUI::FancyDate::paint (QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const {
    if (!i.isValid())					return QStyledItemDelegate::paint(p, o, i);
    QDateTime				dt;
    dt.setTime_t						(i.data().toInt());
    QString					txt			= RSUUI::Core::Instance().humanReadableDateTime(dt);
    // Save painter.
    p->save								();
    // Draw default BG.
    QStyleOptionViewItemV4	o4			= o;
    initStyleOption						(&o4, i);
    QStyle					*s			= o4.widget ? o4.widget->style() : QApplication::style();
    s->drawPrimitive					(QStyle::PE_PanelItemViewItem, &o4, p, o4.widget);
    // Prepare text pen.
    QPalette::ColorRole		cr			= o.state & QStyle::State_Active ? (o.state & QStyle::State_Selected ? QPalette::HighlightedText : QPalette::WindowText) : QPalette::WindowText;
    s->drawItemText						(p, o.rect, Qt::AlignLeft|Qt::AlignVCenter, o.palette, true, txt, cr);
    // Restore painter.
    p->restore							();
}
QSize RSUUI::FancyDate::sizeHint (const QStyleOptionViewItem &o, const QModelIndex &i) const {
    QFontMetrics			fm			(o.font);
    QDateTime				dt;
    dt.setTime_t						(i.data().toInt());
    return					QSize		(fm.width(RSUUI::Core::Instance().humanReadableDateTime(dt)), fm.height());
}

// Fancy currency delegate.
RSUUI::FancyCurrency::FancyCurrency (QObject *p) : ReadOnly(p) {
}
void RSUUI::FancyCurrency::paint (QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const {
    if (!i.isValid())					return QStyledItemDelegate::paint(p, o, i);
    QString					txt			= QLocale().toCurrencyString(i.data().toFloat());
    // Save painter.
    p->save								();
    // Draw default BG.
    QStyleOptionViewItemV4	o4			= o;
    initStyleOption						(&o4, i);
    QStyle					*s			= o4.widget ? o4.widget->style() : QApplication::style();
    s->drawPrimitive					(QStyle::PE_PanelItemViewItem, &o4, p, o4.widget);
    // Prepare text pen.
    QPalette::ColorRole		cr			= o.state & QStyle::State_Active ? (o.state & QStyle::State_Selected ? QPalette::HighlightedText : QPalette::WindowText) : QPalette::WindowText;
    s->drawItemText						(p, o.rect, Qt::AlignLeft|Qt::AlignVCenter, o.palette, true, txt, cr);
    // Restore painter.
    p->restore							();
}
QSize RSUUI::FancyCurrency::sizeHint (const QStyleOptionViewItem &o, const QModelIndex &i) const {
    QFontMetrics			fm			(o.font);
    return					QSize		(fm.width(QLocale().toCurrencyString(i.data().toFloat())), fm.height());
}

// Fancy slip key delegate.
RSUUI::SlipKeyDelegate::SlipKeyDelegate (QObject *p) : ReadOnly(p) {
}
void RSUUI::SlipKeyDelegate::paint (QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const {
    if (!i.isValid())					return QStyledItemDelegate::paint(p, o, i);
    quint32					k			= i.data().toInt();
    QString					txt			= QString("%1%2").arg(k ? (k==1?"TRV":"BRD") : "TMP").arg(k ? k : i.row());
    // Save painter.
    p->save								();
    // Draw default BG.
    QStyleOptionViewItemV4	o4			= o;
    initStyleOption						(&o4, i);
    QStyle					*s			= o4.widget ? o4.widget->style() : QApplication::style();
    s->drawPrimitive					(QStyle::PE_PanelItemViewItem, &o4, p, o4.widget);
    // Draw pixmap.
    QPixmap					pm			= i.data(Qt::DecorationRole).value<QPixmap>();
    quint32					pmWidth		= pm.width();
    quint32					pmHeight	= pm.height();
    p->drawPixmap						(QRect(o.rect.left()+5, o.rect.top()+(o.rect.height()-pmHeight)/2, pmWidth, pmHeight), pm);
    // Prepare text pen.
    QRect					r			(o.rect);
    r.setX								(r.x()+26);
    QPalette::ColorRole		cr			= o.state & QStyle::State_Active ? (o.state & QStyle::State_Selected ? QPalette::HighlightedText : QPalette::WindowText) : QPalette::WindowText;
    s->drawItemText						(p, r, Qt::AlignLeft|Qt::AlignVCenter, o.palette, true, txt, cr);
    // Restore painter.
    p->restore();
}
QSize RSUUI::SlipKeyDelegate::sizeHint (const QStyleOptionViewItem &o, const QModelIndex &i) const {
    QFontMetrics			fm			(o.font);
    // Compute text size.
    quint32					k			= i.data().toInt();
    QString					txt			= QString("%1%2").arg(k ? (k==1?"TRV":"BRD") : "TMP").arg(k ? k : i.row());
    // Return text size composed with icon.
    return								QSize(fm.width(txt)+26, 24);
}

// Fancy document key delegate.
RSUUI::DocumentKeyDelegate::DocumentKeyDelegate (QObject *p) : ReadOnly(p) {
}
void RSUUI::DocumentKeyDelegate::paint (QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const {
    if (!i.isValid())					return QStyledItemDelegate::paint(p, o, i);
    quint32					k			= i.data().toInt();
    QString					txt			= QString("%1%2").arg(k ? "DOC" : "TMP").arg(k ? k : i.row());
    // Save painter.
    p->save								();
    // Draw default BG.
    QStyleOptionViewItemV4	o4			= o;
    initStyleOption						(&o4, i);
    QStyle					*s			= o4.widget ? o4.widget->style() : QApplication::style();
    s->drawPrimitive					(QStyle::PE_PanelItemViewItem, &o4, p, o4.widget);
    // Draw pixmap.
    QPixmap					pm			= i.data(Qt::DecorationRole).value<QPixmap>();
    quint32					pmWidth		= pm.width();
    quint32					pmHeight	= pm.height();
    p->drawPixmap						(QRect(o.rect.left()+5, o.rect.top()+(o.rect.height()-pmHeight)/2, pmWidth, pmHeight), pm);
    // Prepare text pen.
    QRect					r			(o.rect);
    r.setX								(r.x()+26);
    QPalette::ColorRole		cr			= o.state & QStyle::State_Active ? (o.state & QStyle::State_Selected ? QPalette::HighlightedText : QPalette::WindowText) : QPalette::WindowText;
    s->drawItemText						(p, r, Qt::AlignLeft|Qt::AlignVCenter, o.palette, true, txt, cr);
    // Restore painter.
    p->restore();
}
QSize RSUUI::DocumentKeyDelegate::sizeHint (const QStyleOptionViewItem &o, const QModelIndex &i) const {
    QFontMetrics			fm			(o.font);
    // Compute text size.
    quint32					k			= i.data().toInt();
    QString					txt			= QString("%1%2").arg(k ? "DOC" : "TMP").arg(k ? k : i.row());
    // Return text size composed with icon.
    return								QSize(fm.width(txt)+26, 24);
}
