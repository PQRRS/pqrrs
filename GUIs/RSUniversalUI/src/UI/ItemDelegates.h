#ifndef REA_ItemDelegates_h
#define REA_ItemDelegates_h

// Base classes and required stuff.
#include <QtGui/QStyledItemDelegate>

namespace RSUUI {

class ReadOnly : public QStyledItemDelegate {
Q_OBJECT
public:
	/**/			ReadOnly				(QObject *p);
	QWidget*		createEditor			(QWidget *p, const QStyleOptionViewItem &option, const QModelIndex &i) const;
};

class FancyDate : public ReadOnly {
Q_OBJECT
public:
	/**/			FancyDate				(QObject *p);
	void			paint					(QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const;
	QSize			sizeHint				(const QStyleOptionViewItem &o, const QModelIndex &i) const;
};

class FancyCurrency : public ReadOnly {
Q_OBJECT
public:
	/**/			FancyCurrency			(QObject *p);
	void			paint					(QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const;
	QSize			sizeHint				(const QStyleOptionViewItem &o, const QModelIndex &i) const;
};

class SlipKeyDelegate : public ReadOnly {
Q_OBJECT
public:
	/**/			SlipKeyDelegate			(QObject *p);
	void			paint					(QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const;
	QSize			sizeHint				(const QStyleOptionViewItem &o, const QModelIndex &i) const;
};

class DocumentKeyDelegate : public ReadOnly {
Q_OBJECT
public:
	/**/			DocumentKeyDelegate		(QObject *p);
	void			paint					(QPainter *p, const QStyleOptionViewItem &o, const QModelIndex &i) const;
	QSize			sizeHint				(const QStyleOptionViewItem &o, const QModelIndex &i) const;
};

}

#endif
