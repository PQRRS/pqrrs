#ifndef RSUUI_DlgDocumentFix_h
#define RSUUI_DlgDocumentFix_h

// Base class and UI.
#include <QtGui/QDialog>
#include "ui_DlgDocumentFix.h"
// Required stuff.
namespace RE {
	namespace T {
        struct Document;
    }
}
namespace RSUUI {
	class Core;
	class DocumentTypesModel;
	class DocumentsModel;
}

namespace RSUUI {

class DlgDocumentFix : public QDialog {
public:
    Q_OBJECT
    Q_PROPERTY	(quint32	documentRow	READ documentRow	WRITE setDocumentRow)

public:
	// Constructors / Destructor.
	/**/					DlgDocumentFix							(QWidget *p=0);
	/**/					~DlgDocumentFix							();
	// Accessors.
	quint32					documentRow								() const;
	void					setDocumentRow							(quint32 const &v);
	RE::T::Document const&	document								() const;

protected slots:
	void					resetDocument							();
	void					documentTypeIndexChanged				();
	void					on_btnVerifySerial_clicked				();
	void					updateUi								();

private:
	Ui::DlgDocumentFix		_ui;
	Core					*_core;
	quint32					_documentRow;
	RE::T::Document			*_document;
};

}

#endif
