#include "DlgSettings.h"
#include "../../RSUUITypes.h"
#include "../../Core.h"
#include "../../Utilities/ThemeManager.h"
#include "../../Models/SettingsModel.h"
#include <SCLibrary/SCTypes>
#include <RELibrary/RETypes>
#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtGui/QStringListModel>

RSUUI::DlgSettings::DlgSettings (QWidget *p)
: QDialog(p), _inInit(true), _core(&RSUUI::Core::Instance()) {
    _ui.setupUi								(this);
    _core->prepareSkin						(*this);
    // Update version fields.
    _ui.txtRSUUIVersion->setText			(RSUUI::Version());
    _ui.txtSCLibraryVersion->setText		(SC::Version());
    _ui.txtRELibraryVersion->setText		(RE::Version());

    // Load values.
    SettingsModel           *s              = _core->settings();
    _ui.txtCorpName->setText				(s->valueForKey("Preferences::CorpName"));
    _ui.txtCorpAddress->setText             (s->valueForKey("Preferences::CorpAddress"));
    _ui.txtCorpPhone->setText               (s->valueForKey("Preferences::CorpPhone"));
    _ui.txtCorpRegNumber->setText           (s->valueForKey("Preferences::CorpRegNumber"));
    _ui.txtCrtMembershipCode->setText		(s->valueForKey("Preferences::CorpCrtMembershipCode"));
    _ui.cmbCrtPaymentMode->setCurrentIndex	(s->valueForKey("Preferences::CorpCrtPaymentMode").toInt());
    _ui.txtAncvMembershipCode->setText		(s->valueForKey("Preferences::CorpAncvMembershipCode"));
    _ui.chkFrontEndorsement->setChecked     (s->valueForKey("Preferences::FrontEndorsement").toUInt());

    // Instanciate theme browser model.
    QStringList         themes              = QStringList() << tr("Default") << _core->themeManager()->availableThemes();
    _ui.cmbTheme->setModel                  (new QStringListModel(themes, this));
    QString const       theme               = _core->themeManager()->currentTheme();
   _ui.cmbTheme->setCurrentIndex            (theme.isEmpty() || !themes.contains(theme) ? 0 : themes.indexOf(theme));
   _ui.lblTheme->setVisible                 (themes.count()>1);
   _ui.cmbTheme->setVisible                 (themes.count()>1);
}
RSUUI::DlgSettings::~DlgSettings () {
    SettingsModel           *s              = _core->settings();
    _core->themeManager()->setCurrentTheme                      (_ui.cmbTheme->currentIndex()==0 ? "" : _ui.cmbTheme->currentText());
    s->setValueForKey   ("Preferences::CorpName",               _ui.txtCorpName->text());
    s->setValueForKey   ("Preferences::CorpAddress",            _ui.txtCorpAddress->text());
    s->setValueForKey   ("Preferences::CorpPhone",              _ui.txtCorpPhone->text());
    s->setValueForKey   ("Preferences::CorpRegNumber",          _ui.txtCorpRegNumber->text());
    s->setValueForKey   ("Preferences::CorpCrtMembershipCode",	_ui.txtCrtMembershipCode->text());
    s->setValueForKey   ("Preferences::CorpCrtPaymentMode",		QString::number(_ui.cmbCrtPaymentMode->currentIndex()));
    s->setValueForKey   ("Preferences::CorpAncvMembershipCode",	_ui.txtAncvMembershipCode->text());
    s->setValueForKey   ("Preferences::FrontEndorsement",       _ui.chkFrontEndorsement->isChecked() ? "1" : "0");
}

void RSUUI::DlgSettings::setVisible (bool v) {
    QDialog::setVisible		(v);
    emit	visibilityChanged	(v);
}
