#include "DlgDocumentFix.h"
// Required stuff.
#include "../../Core.h"
#include "../../Utilities/DocumentUtility.h"
#include "../../Models/DocumentTypesModel.h"
#include "../../Models/DocumentsModel.h"
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <QtCore/QDebug>

RSUUI::DlgDocumentFix::DlgDocumentFix (QWidget *p)
: QDialog(p), _core(&Core::Instance()), _document(0) {
    _ui.setupUi								(this);
    _core->prepareSkin						(*this);
    setModal								(true);
    // Initialize types combo.
    _ui.cmbDocumentType->setModel			(_core->documentTypes());
    _ui.cmbDocumentType->setModelColumn		(DocumentTypesModel::Name);
    // Prepare document object.
    _document								= new RE::T::Document();
    // Connect stuff.
    connect(_ui.txtDocumentSerial, SIGNAL(textChanged(QString)), this, SLOT(updateUi()));
    connect(_ui.cmbDocumentType, SIGNAL(currentIndexChanged(int)), this, SLOT(documentTypeIndexChanged()));
}
RSUUI::DlgDocumentFix::~DlgDocumentFix () {
    delete			_document;
}

quint32 RSUUI::DlgDocumentFix::documentRow () const {
    return			_documentRow;
}
void RSUUI::DlgDocumentFix::setDocumentRow (quint32 const &v) {
    resetDocument							();
    _documentRow							= v;
    _ui.cmbDocumentType->setCurrentIndex	(0);
    _ui.txtDocumentSerial->setText			("");
    _ui.txtSerialValidity->setText			("");
    updateUi								();
}
RE::T::Document const& RSUUI::DlgDocumentFix::document () const {
    return			*_document;
}

void RSUUI::DlgDocumentFix::resetDocument () {
    // Clear any reason.
    _document->strings.clear				();
    _document->acceptationReasons.clear		();
    _document->rejectionReasons.clear		();
    _document->typeData						= 0;
    _document->typeDataCode					= -1;
    _document->validity						= RE::E::DocumentValidityInvalid;
}
void RSUUI::DlgDocumentFix::documentTypeIndexChanged () {
    updateUi								();
}
void RSUUI::DlgDocumentFix::on_btnVerifySerial_clicked () {
    _core->recognitionEngine				();
    // Cache document utility.
    DocumentUtility	*docUtil				= _core->documentUtility();
    // Retrieve recognition type code from selected document type.
    quint32			dtRow					= _ui.cmbDocumentType->currentIndex();
    qint32			typeRCode				= _core->documentTypes()->index(dtRow, DocumentTypesModel::RecognitionCode).data().toInt();
    // Grab the currently running recognition engine.
    RE::Engine		*_re					= _core->recognitionEngine();
    // Reset type data, type code, and serial.
    resetDocument							();
    _document->strings["Type.Code"]			= QString::number(typeRCode);
    _document->strings["Serial"]			= _ui.txtDocumentSerial->text();
    _document->typeData						= _re->documentTypeDataForCode(typeRCode);
    _document->typeDataCode					= _document->typeData ? _document->typeData->code : -1;
    _re->determineDocumentValidity			(_document);
    docUtil->postValidate					(_document);
    docUtil->translateReasons				(_document);
    // Prepare status text and color.
    QString			txtClr, serStat;
    // Build status text and color.
    if (_document->validity==RE::E::DocumentValidityValid) {
        txtClr								= "#afa";
        serStat								= _document->acceptationReasons.join(" ");
    }
    else if (_document->validity==RE::E::DocumentValidityInvalid) {
        txtClr								= "#f88";
        serStat								= _document->rejectionReasons.join(" ");
    }
    else {
        txtClr								= "#ff9";
        serStat								= _document->rejectionReasons.join(" ");
    }
    _ui.txtSerialValidity->setStyleSheet	(QString("color: %1;").arg(txtClr));
    _ui.txtSerialValidity->setText			(serStat);
    updateUi								();
    // 040429583280085018800002
    // <01234567890<01234567890<01234567890421:
}

void RSUUI::DlgDocumentFix::updateUi () {
    bool			serEmpty			= _ui.txtDocumentSerial->text().isEmpty();
    bool			staEmpty			= _ui.txtSerialValidity->text().isEmpty();
    bool			serValid			= _document && _document->validity==RE::E::DocumentValidityValid;
    quint32         dtKey               = _core->documentTypes()->index(_ui.cmbDocumentType->currentIndex(), DocumentTypesModel::Key).data().toInt();
    bool			dtValid				= dtKey!=RSUUI::UnknownDocumentTypeKey;
    _ui.cmbDocumentType->setEnabled		(!serValid);
    _ui.btnVerifySerial->setEnabled		(dtValid && !serValid && !serEmpty);
    _ui.txtDocumentSerial->setEnabled	(dtValid && !serValid);
    _ui.txtSerialValidity->setVisible	(!staEmpty);
    _ui.btnApply->setEnabled			(serValid);
    // Document image zone.
    QString			uuid				= _core->documents()->index(_documentRow, DocumentsModel::ExtractedUUID).data().toString();
    QPalette		pal					= _ui.wgtDocumentPicture->palette();
    _ui.wgtDocumentPicture->setImage	(QImage(QString("Images/%1.jpg").arg(uuid)));
}
