#ifndef RSUUI_DlgSlips_h
#define RSUUI_DlgSlips_h

// Base class and private UI.
#include <QtGui/QDialog>
#include "ui_DlgSlips.h"
#include "../../RSUUITypes.h"
// Required stuff.
namespace RSUUI {
    class Core;
    class SlipsModel;
    class DocumentsModel;
}
class QMenu;

namespace RSUUI {

class DlgSlips : public QDialog {
Q_OBJECT

public:
    /**/					DlgSlips							(QWidget *parent=0);
    /**/					~DlgSlips							();

protected:
    void					startSlipProcessing					(RSUUI::E::ScanOption opts, bool count, bool endorse);
    void					continueSlipProcessing				();

public slots:
    // Core events.
    void					scannerStateChanged                 (RSUUI::T::ScannerHelper const&);
    void					scanningStarted						(RSUUI::T::ScannerHelper const&);
    void					scanningFinished					(RSUUI::T::ScannerHelper const&);
    void					documentScanned                     ();
    // Filters.
    void					applySlipsFilters					();
    void					applyDocumentsFilters				();
private slots:
    // Buttons.
    void					sendSlipMenuMarkSent				();
    void					sendSlipMenuCheckMarkSent			();
    void					sendSlipMenuCheckEndorseMarkSent	();
    void                    exportSlipPdf                       ();
    void                    exportSlipText                      ();
    void					on_btnSlipDeclareDispute_clicked	();
    void					on_btnSlipDeclarePaid_clicked		();
    void					on_btnSlipsDelete_clicked			();
    void					on_btnDocumentsDelete_clicked		();
    // Selection watchers.
    void					slipsSelectionChanged				();
    void					documentsSelectionChanged			();
    void					updateUi							();

private:
    Ui::DlgSlips			_ui;
    Core					*_core;
    QModelIndex				_selectedSlip;
    bool					_isProcessingSlip;
    bool					_count;
    bool					_endorse;
    RSUUI::E::ScanOptions	_slipCheckMode;
    quint32					_docsCounter;
    QMenu					*_mnuSendSlip;
    QMenu                   *_mnuExportSlip;

signals:
    void					startScanning						(RSUUI::E::ScanOptions);
    void					sendSlip							(QModelIndex, bool);
    void					disputeSlip							(QModelIndex);
    void					paySlip								(QModelIndex, QString);
    void					deleteSlips							(QModelIndexList);
    void					deleteDocuments						(QModelIndexList, QModelIndex);
};

}

#endif
