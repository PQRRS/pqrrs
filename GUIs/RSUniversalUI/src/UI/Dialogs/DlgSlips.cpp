#include "DlgSlips.h"
#include <RELibrary/RETypes>
#include "../../Core.h"
#include "../../Models/SettingsModel.h"
#include "../../Models/SlipsModel.h"
#include "../../Models/DocumentsModel.h"
#include "../ItemDelegates.h"
#include <QtCore/QUrl>
#include <QtCore/QDateTime>
#include <QtGui/QMenu>
#include <QtGui/QPrinter>
#include <QtGui/QMessageBox>
#include <QtGui/QFileDialog>
#include <QtGui/QTextDocument>

RSUUI::DlgSlips::DlgSlips (QWidget *p)
: QDialog(p), _core(&Core::Instance()), _isProcessingSlip(false), _count(true), _endorse(true) {
    _ui.setupUi										(this);
    _core->prepareSkin								(*this);
    // Hide document image area.
    _ui.wgtDocumentPicture->setVisible				(false);

    // Set main spliter factors.
    _ui.spltrContents->setStretchFactor				(0, 2);
    _ui.spltrContents->setStretchFactor				(1, 3);

    // Set up filters initial values.
    _ui.dtFilterStart->setDate						(QDate::currentDate());
    _ui.dtFilterEnd->setDate						(QDate::currentDate());

    // Set up models.
    _ui.tblVwSlips->setModel						(_core->slips());
    // Disable edition.
    _ui.tblVwSlips->setEditTriggers					(QAbstractItemView::NoEditTriggers);
    // Configure columns for documents table.
    _ui.tblVwSlips->setColumnHidden					(SlipsModel::Status,				true);
    _ui.tblVwSlips->setColumnHidden					(SlipsModel::Comments,				true);
    // Set columns width for slips table.
    _ui.tblVwSlips->setColumnWidth					(SlipsModel::Key,					85);
    _ui.tblVwSlips->setColumnWidth					(SlipsModel::CreatedOn,				85);
    _ui.tblVwSlips->setColumnWidth					(SlipsModel::DocumentCount,			55);
    _ui.tblVwSlips->setColumnWidth					(SlipsModel::Total,					55);
    // Set delegates for documents table.
    _ui.tblVwSlips->setItemDelegateForColumn		(SlipsModel::Key,					new SlipKeyDelegate(_ui.tblVwSlips));
    _ui.tblVwSlips->setItemDelegateForColumn		(SlipsModel::CreatedOn,				new FancyDate(_ui.tblVwSlips));
    _ui.tblVwSlips->setItemDelegateForColumn		(SlipsModel::Total,					new FancyCurrency(_ui.tblVwDocuments));
    // Finally set up sorting.
    _ui.tblVwSlips->sortByColumn					(SlipsModel::Key,					Qt::DescendingOrder);

    // Set up models.
    _ui.tblVwDocuments->setModel					(_core->documents());
    // Disable edition.
    _ui.tblVwDocuments->setEditTriggers				(QAbstractItemView::NoEditTriggers);
    // Configure columns for documents table.
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::Slip,				true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::PointOfSale,		true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::CreatedOn,			true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::Data,				true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::ExtractedUUID,		true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::ExtractedValidity,	true);
    // Configure columns widths for documents table.
    _ui.tblVwDocuments->setColumnWidth				(DocumentsModel::Key,				85);
    _ui.tblVwDocuments->setColumnWidth				(DocumentsModel::DocumentType,		145);
    _ui.tblVwDocuments->setColumnWidth				(DocumentsModel::ExtractedAmount,	55);
    _ui.tblVwDocuments->setColumnWidth				(DocumentsModel::ExtractedSerial,	145);
    // Set delegates for documents table.
    _ui.tblVwDocuments->setItemDelegateForColumn	(DocumentsModel::Key,				new DocumentKeyDelegate(_ui.tblVwDocuments));
    _ui.tblVwDocuments->setItemDelegateForColumn	(DocumentsModel::ExtractedAmount,	new FancyCurrency(_ui.tblVwDocuments));

    // Filters.
    connect(_ui.chkFilterStartEnable, SIGNAL(stateChanged(int)), this, SLOT(applySlipsFilters()));
    connect(_ui.chkFilterEndEnable, SIGNAL(stateChanged(int)), this, SLOT(applySlipsFilters()));
    connect(_ui.dtFilterStart, SIGNAL(dateChanged(QDate)), this, SLOT(applySlipsFilters()));
    connect(_ui.dtFilterEnd, SIGNAL(dateChanged(QDate)), this, SLOT(applySlipsFilters()));
    connect(_ui.chkSlipFilterOpen, SIGNAL(stateChanged(int)), this, SLOT(applySlipsFilters()));
    connect(_ui.chkSlipFilterSent, SIGNAL(stateChanged(int)), this, SLOT(applySlipsFilters()));
    connect(_ui.chkSlipFilterDisputed, SIGNAL(stateChanged(int)), this, SLOT(applySlipsFilters()));
    connect(_ui.chkSlipFilterPaid, SIGNAL(stateChanged(int)), this, SLOT(applySlipsFilters()));
    // Connect slips selection to filtering.
    connect(_ui.tblVwSlips->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)), this, SLOT(slipsSelectionChanged()));
    connect(_ui.tblVwDocuments->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)), this, SLOT(documentsSelectionChanged()));

    // Create Send Slip button menu.
    _mnuSendSlip									= new QMenu(this);
    _mnuSendSlip->addAction							(QIcon(":/Resources/Images/Icons/Set/accept"),
                                                        tr("Check, Endorse then Mark as Sent (Recommended)"),
                                                        this, SLOT(sendSlipMenuCheckEndorseMarkSent()));
    _mnuSendSlip->addAction							(QIcon(":/Resources/Images/Icons/Set/caution"),
                                                        tr("Check then Mark as Sent (No endorsement will be performed)"),
                                                        this, SLOT(sendSlipMenuCheckMarkSent()));
    _mnuSendSlip->addAction							(QIcon(":/Resources/Images/Icons/Set/exclamation"),
                                                        tr("Mark as Sent (No verification will be performed at all)"),
                                                        this, SLOT(sendSlipMenuMarkSent()));
    _ui.btnSlipDeclareSent->setMenu					(_mnuSendSlip);

    // Create Export Slip button menu.
    _mnuExportSlip                                  = new QMenu(this);
    _mnuExportSlip->addAction                       (QIcon(":/Resources/Images/Icons/Set/document"),
                                                        tr("Export as PDF"),
                                                        this, SLOT(exportSlipPdf()));
    _mnuExportSlip->addAction                       (QIcon(":/Resources/Images/Icons/Set/document"),
                                                        tr("Export as Text"),
                                                         this, SLOT(exportSlipText()));
    _ui.btnSlipsExport->setMenu                     (_mnuExportSlip);

    // Force refiltering.
    applySlipsFilters								();
    slipsSelectionChanged							();
}
RSUUI::DlgSlips::~DlgSlips () {
}

void RSUUI::DlgSlips::startSlipProcessing (RSUUI::E::ScanOption opts, bool count, bool endorse) {
    _isProcessingSlip				= true;
    _docsCounter					= 0;
    _slipCheckMode					= opts;
    _count							= count;
    _endorse						= endorse;
    updateUi						();

    // Ask the user to place the documents reversed in the scanner.
    QStringList				title;
    if (_count)				title	<< tr("Check");
    if (_endorse)			title	<< tr("Endorse");
    QMessageBox::information		(
                                        this,
                                        tr("Slip Processing (%1)").arg(title.join(tr(" and "))),
                                        tr("Please insert this slip's documents in the scanner, picture facing your left hand, and press \"Ok\" once ready."),
                                        QMessageBox::Ok
                                    );
    emit startScanning(_slipCheckMode);
}
void RSUUI::DlgSlips::continueSlipProcessing () {
    // Ask the user for any other document, or finish if none.
    QStringList				title;
    if (_count)				title	<< tr("Check");
    if (_endorse)			title	<< tr("Endorse");
    QMessageBox				msg		(
                                        QMessageBox::Question,
                                        tr("Slip Processing (%1)").arg(title.join(tr(" and "))),
                                        tr("If there are documents remaining for this slip, please insert them in the scanner, picture facing your left hand, and press \"Go\" once ready. Otherwise, press \"Finish\"."),
                                        QMessageBox::NoButton,
                                        this
                                    );
    QAbstractButton			*btnC	= msg.addButton(tr("Go"), QMessageBox::AcceptRole);
                                      msg.addButton(tr("Finish"), QMessageBox::RejectRole);
    msg.exec						();
    // User clocked check again, start scanning.
    if (msg.clickedButton()==btnC)
        return emit startScanning(_slipCheckMode);
    // Make sure checked document count is right.
    _isProcessingSlip				= false;
    updateUi						();
    if (_count) {
        quint32				sdCount	= _core->slips()->index(_selectedSlip.row(), SlipsModel::DocumentCount).data(Qt::DisplayRole).toInt();
        // Document checking problem... Ask the user if he wants to close the slip anyways.
        bool                intPass = _docsCounter==sdCount;
        if (!intPass && QMessageBox::No==QMessageBox::warning(
                this,
                tr("Slip Integrity Problem"),
                tr("It appears that the checking cycle has found %1 %2 instead of %3. Do you want to mark this slip as sent despite this integrity problem?")
                    .arg(_docsCounter)
                    .arg(_docsCounter==1 ? tr("documents") : tr("document"))
                    .arg(sdCount),
                QMessageBox::No,
                QMessageBox::Yes))
            return;
        // All good!
        emit sendSlip               (_selectedSlip, intPass);
    }
}

// Core events.
void RSUUI::DlgSlips::scannerStateChanged (RSUUI::T::ScannerHelper const&) {
}
void RSUUI::DlgSlips::scanningStarted (RSUUI::T::ScannerHelper const&) {
}
void RSUUI::DlgSlips::scanningFinished (RSUUI::T::ScannerHelper const&) {
    if (!_isProcessingSlip)
        return;
    continueSlipProcessing			();
}
void RSUUI::DlgSlips::documentScanned () {
    _docsCounter++;
}

// Filters.
void RSUUI::DlgSlips::applySlipsFilters () {
    SlipsModel			*slips		= _core->slips();
    // Prepare status codes for querying.
    QList<quint32>		status;
    if (_ui.chkSlipFilterOpen->isChecked())		status	<< RSUUI::E::SlipOpen;
    if (_ui.chkSlipFilterSent->isChecked())		status	<< RSUUI::E::SlipSent;
    if (_ui.chkSlipFilterDisputed->isChecked())	status	<< RSUUI::E::SlipDisputed;
    if (_ui.chkSlipFilterPaid->isChecked())		status	<< RSUUI::E::SlipPaid;
    // Set slips model criteria.
    slips->setCriteria	(_ui.chkFilterStartEnable->isChecked() ? _ui.dtFilterStart->dateTime() : QDateTime(),
                            _ui.chkFilterEndEnable->isChecked() ? _ui.dtFilterEnd->dateTime() : QDateTime(),
                            status,
                            true);
}
void RSUUI::DlgSlips::applyDocumentsFilters () {
    quint32					slipId	= _selectedSlip.isValid() ? _core->slips()->index(_selectedSlip.row(), SlipsModel::Key).data().toInt() : 0;
    QList<quint32>			valids	= QList<quint32>()
                                        << RE::E::DocumentValidityValid
                                        << RE::E::DocumentValidityIncomplete
                                        << RE::E::DocumentValidityInvalid;
    _core->documents()->setCriteria	(QDateTime(), QDateTime(), 0, valids, slipId);
}
// Send slip button actions.
void RSUUI::DlgSlips::sendSlipMenuMarkSent () {
    if (QMessageBox::No==QMessageBox::warning(
            this,
            tr("Slip Modification"),
            tr("You have choosen to mark this slip as sent without performing any check and without endorsing the documents. Be aware that the gathering organism may refuse the documents as they will not be endorsed, and that the slip declared contents may differ from reallity. Are you sure you want to do this?"),
            QMessageBox::Yes | QMessageBox::No))
        return;
    emit sendSlip(_selectedSlip, false);
}
void RSUUI::DlgSlips::sendSlipMenuCheckMarkSent () {
    startSlipProcessing				(E::ScanOptionNone, true, false);
}
void RSUUI::DlgSlips::sendSlipMenuCheckEndorseMarkSent () {
    startSlipProcessing				(E::ScanOptionEndorseBack, true, true);
}
// Export slip button actions.
void RSUUI::DlgSlips::exportSlipPdf () {
    // No slip selected.
    if (!_selectedSlip.isValid())           return;
    // Cache slips model.
    SlipsModel                  *slips      = _core->slips();
    quint32 const               slipRow     = _selectedSlip.row();
    // Get slip ID and creation timestamp.
    quint32 const               slipKey     = slips->index(slipRow, SlipsModel::Key).data().toInt();
    QString const               slipId      = QString("%1%2")
                                                .arg(slipKey ? (slipKey==1?"TRV":"BRD") : "TMP")
                                                .arg(slipKey);
    QDateTime                   slipDt;
    slipDt.setTime_t                       (slips->index(slipRow, SlipsModel::CreatedOn).data().toInt());

    SettingsModel               *s          = _core->settings();
    QString                     fn          = QString("%1/%2.pdf")
                                                .arg(QFileInfo(s->valueForKey("DlgSlips::SlipPdfExportPath", QDir::homePath())).path())
                                                .arg(slipId);
    fn                                      = QFileDialog::getSaveFileName(this, tr("Slip Export Path"), fn, tr("Portable Document Format (*.pdf)"));
    if (!fn.length())
        return;
    s->setValueForKey                       ("DlgSlips::SlipPdfExportPath", fn);

    QFile                       fHtml       (":/Resources/HTML/PrintSlipSummary.html");
    fHtml.open                              (QIODevice::ReadOnly|QIODevice::Text);
    QString const               html        = QString::fromUtf8(fHtml.readAll())
                                                .replace("%SlipIdentifier%",        slipId)
                                                .replace("%CurrentDate%",           QDate::currentDate().toString(Qt::DefaultLocaleLongDate))
                                                .replace("%CorpName%",              s->valueForKey("Preferences::CorpName"))
                                                .replace("%CorpRegNumber%",         s->valueForKey("Preferences::CorpRegNumber"))
                                                .replace("%CorpAddress%",           s->valueForKey("Preferences::CorpAddress"))
                                                .replace("%CorpPhone%",             s->valueForKey("Preferences::CorpPhone"))
                                                .replace("%CorpCrtMembershipCode%", s->valueForKey("Preferences::CorpCrtMembershipCode"))
                                                .replace("%GeneratedSlipTitle%",  _core->documents()->htmlSlipTitle())
                                                .replace("%GeneratedSlipDetails%",  _core->documents()->htmlSlipDetails());
    fHtml.close                             ();
    QPrinter                    prnt;
    prnt.setOutputFormat                    (QPrinter::PdfFormat);
    prnt.setOutputFileName                  (fn);

    QTextDocument               doc;
    // Set up stylesheet on slip summary widget.
    QFile                       fStyle		(":/Resources/Stylesheets/PrintSlipSummary.css");
    fStyle.open                             (QIODevice::ReadOnly|QIODevice::Text);
    QString                     css			(fStyle.readAll());
    fStyle.close                            ();
    doc.addResource                         (QTextDocument::StyleSheetResource, QUrl("SlipSummary.css"), css);
    // Finally set up the HTML and print to PDF.
    doc.setHtml                             (html);
    doc.print                               (&prnt);
}
void RSUUI::DlgSlips::exportSlipText () {
}
// Other buttons.
void RSUUI::DlgSlips::on_btnSlipDeclareDispute_clicked () {
    if (QMessageBox::No==QMessageBox::question(
            this,
            tr("Slip Modification"),
            tr("Declaring a dispute on a slip will freeze the sleep until further information can be added. Do you want to continue?"),
            QMessageBox::Yes | QMessageBox::No))
        return;
    emit disputeSlip(_selectedSlip);
}
void RSUUI::DlgSlips::on_btnSlipDeclarePaid_clicked () {
    QMessageBox				msg		(
                                        QMessageBox::Question,
                                        tr("Slip Modification"),
                                        tr("Would you like to declare this slip as paid?"),
                                        QMessageBox::NoButton,
                                        this
                                    );
    QAbstractButton			*btnNo	= msg.addButton(tr("No"), QMessageBox::RejectRole);
    QAbstractButton			*btnSav	= msg.addButton(tr("Yes, but save picures"), QMessageBox::AcceptRole);
                                      msg.addButton(tr("Yes"), QMessageBox::DestructiveRole);
    msg.exec						();
    if (msg.clickedButton()==btnNo)
        return;
    QString					pp;
    if (msg.clickedButton()==btnSav) {
        SettingsModel       *s      = _core->settings();
        pp							= QFileDialog::getExistingDirectory (this, tr("Document Pictures Backup Folder"), s->valueForKey("DlgSlips::PicturesBackupPath", QDir::homePath()));
        if (pp.isEmpty())
            return;
        s->setValueForKey   		("DlgSlips::PicturesBackupPath", pp);
    }
    emit paySlip(_selectedSlip, pp);
}
void RSUUI::DlgSlips::on_btnSlipsDelete_clicked () {
    if (!_selectedSlip.isValid())
        return;
    else if (QMessageBox::Yes==QMessageBox::question(
            this,
            tr("Slip Deletion"),
            tr("Deleting a slip will also delete any document it contains. Would you like to continue?"),
            QMessageBox::Yes | QMessageBox::No))
    emit					deleteSlips(QModelIndexList()<<_selectedSlip);
}
void RSUUI::DlgSlips::on_btnDocumentsDelete_clicked () {
    // Retrieve document row.
    QModelIndexList const	&idxs		= _ui.tblVwDocuments->selectionModel()->selectedRows();
    if (!idxs.count())
        return;
    else if (QMessageBox::Yes==QMessageBox::question(
            this,
            tr("Document Deletion"),
            tr("Do you really want to delete this document as well as any pictures attached to it?"),
            QMessageBox::Yes | QMessageBox::No))
        emit				deleteDocuments(idxs, _selectedSlip);
}
// Selection watchers.
void RSUUI::DlgSlips::slipsSelectionChanged () {
    QModelIndexList const	&sIdxs	= _ui.tblVwSlips->selectionModel()->selectedRows();
    _selectedSlip					= sIdxs.count()==1 ? sIdxs.first() : QModelIndex();
    applyDocumentsFilters			();
    updateUi						();
}
// Selection watchers.
void RSUUI::DlgSlips::documentsSelectionChanged () {
    updateUi						();
}
void RSUUI::DlgSlips::updateUi () {
    // Cache models.
    SlipsModel				*slips			= _core->slips();
    // Cache slip selection informations.
    bool					oneSlip			= _selectedSlip.isValid();
    E::SlipStatus			slpStatus		= (E::SlipStatus)(oneSlip ? slips->index(_selectedSlip.row(), SlipsModel::Status).data().toInt() : 0);
    // Update slips actions.
    _ui.wgtMainButtonsBar->setEnabled		(!_isProcessingSlip);
    _ui.wgtSlipButtonsBar->setEnabled		(!_isProcessingSlip);
    _ui.wgtDocumentsButtonsBar->setEnabled	(!_isProcessingSlip);
    _ui.tblVwSlips->setEnabled				(!_isProcessingSlip);
    _ui.tblVwDocuments->setEnabled			(!_isProcessingSlip);
    _ui.btnSlipsExport->setEnabled          (oneSlip);
    _ui.btnSlipsDelete->setEnabled			(oneSlip && (slpStatus==E::SlipOpen || slpStatus==E::SlipPaid));
    _ui.btnSlipDeclareSent->setEnabled		(oneSlip && slpStatus==E::SlipOpen);
    _ui.btnSlipDeclareDispute->setEnabled	(oneSlip && slpStatus==E::SlipSent);
    _ui.btnSlipDeclarePaid->setEnabled		(oneSlip && (slpStatus==E::SlipSent || slpStatus==E::SlipDisputed));
    // Slip comments zone.
    _ui.txtSlipComments->setVisible			(oneSlip);
    _ui.txtSlipComments->setPlainText		(oneSlip ? slips->index(_selectedSlip.row(), SlipsModel::Comments).data().toString() : QString());

    // Cache document selection informations.
    QModelIndexList const	&docsIdxs		= _ui.tblVwDocuments->selectionModel()->selectedRows();
    quint32					docsCount		= docsIdxs.count();
    bool					oneDoc			= docsIdxs.count()==1;
    // Update documents actions.
    _ui.btnDocumentsDelete->setEnabled		(docsCount && oneSlip && slpStatus==E::SlipOpen);
    // Document image zone.
    _ui.wgtDocumentPicture->setVisible	(oneDoc);
    if (oneDoc) {
        QString const			&uuid	= _core->documents()->index(docsIdxs.first().row(), DocumentsModel::ExtractedUUID).data().toString();
        QPalette				pal		= _ui.wgtDocumentPicture->palette();
        _ui.wgtDocumentPicture->setImage
                                (QImage(QString("Images/%1.jpg").arg(uuid)));
    }
}
