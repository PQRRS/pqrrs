#ifndef RSUUI_DlgMain_h
#define RSUUI_DlgMain_h

// Base class and private UI.
#include <QtGui/QDialog>
#include "ui_DlgMain.h"
// Required stuff.
#include "../../RSUUITypes.h"
#include <QtCore/QModelIndex>
namespace RSUUI {
	class Core;
	class IssuersModel;
	class DocumentTypesModel;
	class SlipsModel;
	class DocumentsModel;
	class DlgDocumentFix;
}
class QTextDocument;

namespace RSUUI {

class DlgMain : public QDialog {
Q_OBJECT

public:
	/**/						DlgMain										(QWidget *parent=0);
	/**/						~DlgMain									();

public slots:
	// Reimplemented slots.
	void						setVisible									(bool v);
	// Filters.
	void						on_dtFilterStart_dateChanged				(QDate const&);
	void						on_dtFilterEnd_dateChanged					(QDate const&);
	void						on_cmbBxFilterIssuer_currentIndexChanged	(int);
	void						applyDocumentsFilters						();

public slots:
	// Core events.
    void						scannerStateChanged                         (RSUUI::T::ScannerHelper const&);
    void						scanningStarted								(RSUUI::T::ScannerHelper const&);
    void						scanningFinished							(RSUUI::T::ScannerHelper const&);
    void						documentScanned                             ();

protected slots:
	// Documents actions.
	void						on_btnDocumentsDelete_clicked				();
	void						on_btnDocumentFix_clicked					();
	// Main actions.
	void						on_btnInitScanner_clicked					();
	void						on_btnFreeScanner_clicked					();
	void						on_btnScanStart_clicked						();
	void						on_btnSlipCreate_clicked					();
	// Selection watchers.
	void						documentsSelectionChanged					();
	// Document Fix dialog events.
	void						documentFixDialogFinished					(int);
	// UI updater.
	void						updateUi									();
	void						updateHtmlSummary							();

private:
	Ui::DlgMain					_ui;
	QTextDocument				*_slipSummaryDoc;
	bool						_inInit;
	Core						*_core;
	bool						_isScanning;
	DlgDocumentFix				*_dlgDocFix;

signals:
	void						visibilityChanged					(bool v);
	void						initializeScanner					();
	void						freeScanner							();
    void						startScanning                       (RSUUI::E::ScanOptions);
	void						deleteDocuments						(QModelIndexList);
	void						createSlipWithDocuments				();
	void						showSlipsHistory					();
};

}

#endif
