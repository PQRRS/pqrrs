#ifndef RSUUI_DlgSettings_h
#define RSUUI_DlgSettings_h

// Base class and private UI.
#include <QtGui/QDialog>
#include "ui_DlgSettings.h"
// Required stuff.
namespace RSUUI {
	class Core;
}

namespace RSUUI {

class DlgSettings : public QDialog {
Q_OBJECT

public:
	/**/						DlgSettings									(QWidget *parent=0);
	/**/						~DlgSettings								();

public slots:
	// Reimplemented slots.
	void						setVisible									(bool v);

protected:
	void						save										();

private:
	Ui::DlgSettings				_ui;
	bool						_inInit;
	Core						*_core;

signals:
    void						visibilityChanged                           (bool v);
};

}

#endif
