#include "DlgMain.h"
#include "../../Core.h"
#include "../../Utilities/DocumentUtility.h"
#include "../../Models/IssuersModel.h"
#include "../../Models/DocumentTypesModel.h"
#include "../../Models/SlipsModel.h"
#include "../../Models/DocumentsModel.h"
#include "../ItemDelegates.h"
#include <RELibrary/RETypes>
#include "DlgDocumentFix.h"
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtGui/QDialog>
#include <QtGui/QMessageBox>
#include <QtGui/QTextDocument>

RSUUI::DlgMain::DlgMain (QWidget *p)
: QDialog(p), _slipSummaryDoc(0), _inInit(true), _core(&Core::Instance()),
_isScanning(false), _dlgDocFix(0) {
    _ui.setupUi										(this);
    _core->prepareSkin								(*this);

    // Set up filters initial values.
    _ui.dtFilterStart->setDate						(QDate::currentDate());
    _ui.dtFilterEnd->setDate						(QDate::currentDate());
    _ui.cmbBxFilterIssuer->setModel					(_core->issuers());
    _ui.cmbBxFilterIssuer->setModelColumn			(IssuersModel::Name);

    // Set up table model.
    _ui.tblVwDocuments->setModel					(_core->documents());
    // Disable edition.
    _ui.tblVwDocuments->setEditTriggers				(QAbstractItemView::NoEditTriggers);
    // Configure columns.
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::Slip,				true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::PointOfSale,		true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::CreatedOn,			true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::Data,				true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::ExtractedUUID,		true);
    _ui.tblVwDocuments->setColumnHidden				(DocumentsModel::ExtractedValidity,	true);
    // Configure columns widths.
    _ui.tblVwDocuments->setColumnWidth				(DocumentsModel::Key,				85);
    _ui.tblVwDocuments->setColumnWidth				(DocumentsModel::DocumentType,		150);
    _ui.tblVwDocuments->setColumnWidth				(DocumentsModel::ExtractedAmount,	70);
    _ui.tblVwDocuments->setColumnWidth				(DocumentsModel::ExtractedSerial,	120);
    // Set up delegates.
    _ui.tblVwDocuments->setItemDelegateForColumn	(DocumentsModel::Key,				new DocumentKeyDelegate(_ui.tblVwDocuments));
    _ui.tblVwDocuments->setItemDelegateForColumn	(DocumentsModel::CreatedOn,			new FancyDate(_ui.tblVwDocuments));
    _ui.tblVwDocuments->setItemDelegateForColumn	(DocumentsModel::ExtractedAmount,	new FancyCurrency(_ui.tblVwDocuments));

    // Buttons.
    connect(_ui.btnSlipsHistory, SIGNAL(clicked()), this, SIGNAL(showSlipsHistory()));
    connect(_ui.btnClose, SIGNAL(clicked()), this, SLOT(accept()));
    // Filters.
    connect(_ui.chkFilterDateRangeEnable, SIGNAL(stateChanged(int)), this, SLOT(applyDocumentsFilters()));
    connect(_ui.chkFilterIssuerEnable, SIGNAL(stateChanged(int)), this, SLOT(applyDocumentsFilters()));
    connect(_ui.chkFilterValidityValid, SIGNAL(stateChanged(int)), this, SLOT(applyDocumentsFilters()));
    connect(_ui.chkFilterValidityIncomplete, SIGNAL(stateChanged(int)), this, SLOT(applyDocumentsFilters()));
    connect(_ui.chkFilterValidityInvalid, SIGNAL(stateChanged(int)), this, SLOT(applyDocumentsFilters()));
    // Selection watchers.
    connect(_ui.tblVwDocuments->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)), this, SLOT(documentsSelectionChanged()));

    // Force re-filtering.
    _inInit											= false;
    applyDocumentsFilters							();
    updateUi										();
}
RSUUI::DlgMain::~DlgMain () {
    if (_dlgDocFix)			documentFixDialogFinished(QDialog::Rejected);
}
void RSUUI::DlgMain::setVisible (bool v) {
    QDialog::setVisible		(v);
    emit	visibilityChanged	(v);
}

// Filters actions.
void RSUUI::DlgMain::on_dtFilterStart_dateChanged (QDate const&) {
    if (_inInit)
        return;
    else if (!_ui.chkFilterDateRangeEnable->isChecked())
        _ui.chkFilterDateRangeEnable->setChecked		(true);
    else
        applyDocumentsFilters							();
}
void RSUUI::DlgMain::on_dtFilterEnd_dateChanged (QDate const&) {
    if (_inInit)
        return;
    else if (!_ui.chkFilterDateRangeEnable->isChecked())
        _ui.chkFilterDateRangeEnable->setChecked		(true);
    else
        applyDocumentsFilters							();
}
void RSUUI::DlgMain::on_cmbBxFilterIssuer_currentIndexChanged (int) {
    if (_inInit)
        return;
    else if (!_ui.chkFilterIssuerEnable->isChecked())
        _ui.chkFilterIssuerEnable->setChecked			(true);
    else
        applyDocumentsFilters							();
}
void RSUUI::DlgMain::applyDocumentsFilters () {
    if (_inInit)				return;
    // Prepare document validity codes for querying.
    QList<quint32>		validity;
    if (_ui.chkFilterValidityValid->isChecked())		validity	<< RE::E::DocumentValidityValid;
    if (_ui.chkFilterValidityIncomplete->isChecked())	validity	<< RE::E::DocumentValidityIncomplete;
    if (_ui.chkFilterValidityInvalid->isChecked())		validity	<< RE::E::DocumentValidityInvalid;
    // Set documents model criteria.
    _core->documents()->setCriteria	(
                                _ui.chkFilterDateRangeEnable->isChecked() ? _ui.dtFilterStart->dateTime() : QDateTime(),
                                _ui.chkFilterDateRangeEnable->isChecked() ? _ui.dtFilterEnd->dateTime() : QDateTime(),
                                _ui.chkFilterIssuerEnable->isChecked() ? _core->issuers()->index( _ui.cmbBxFilterIssuer->currentIndex(), IssuersModel::Key).data().toInt() : 0,
                                validity,
                                WorkingSlipKey);
    updateHtmlSummary		();
}

// Core events.
void RSUUI::DlgMain::scannerStateChanged (RSUUI::T::ScannerHelper const&) {
    updateUi				();
}
void RSUUI::DlgMain::scanningStarted (RSUUI::T::ScannerHelper const&) {
}
void RSUUI::DlgMain::scanningFinished (RSUUI::T::ScannerHelper const&) {
    _isScanning				= false;
    updateUi				();
    updateHtmlSummary		();
}
void RSUUI::DlgMain::documentScanned () {
}
// Documents actions.
void RSUUI::DlgMain::on_btnDocumentsDelete_clicked () {
    QModelIndexList const	&idxs	= _ui.tblVwDocuments->selectionModel()->selectedRows();
    if (!idxs.count())				return;
    emit		deleteDocuments(idxs);
    updateHtmlSummary				();
}
void RSUUI::DlgMain::on_btnDocumentFix_clicked () {
    QModelIndexList	sel				= _ui.tblVwDocuments->selectionModel()->selectedRows();
    if (sel.count()!=1)				return;
    quint32			docRow			= sel.first().row();
    if (!_dlgDocFix) {
        _dlgDocFix					= new DlgDocumentFix();
        connect						(_dlgDocFix, SIGNAL(finished(int)), this, SLOT(documentFixDialogFinished(int)), Qt::QueuedConnection);
    }
    _dlgDocFix->setDocumentRow		(docRow);
    _dlgDocFix->show				();
}
// Main actions.
void RSUUI::DlgMain::on_btnInitScanner_clicked () {
    emit		initializeScanner();
}
void RSUUI::DlgMain::on_btnFreeScanner_clicked () {
    emit		freeScanner();
}
void RSUUI::DlgMain::on_btnScanStart_clicked () {
    _inInit										= true;
    _isScanning									= true;
    _ui.chkFilterDateRangeEnable->setChecked	(false);
    _ui.chkFilterIssuerEnable->setChecked		(false);
    _ui.chkFilterValidityValid->setChecked		(true);
    _ui.chkFilterValidityIncomplete->setChecked	(true);
    _ui.chkFilterValidityInvalid->setChecked	(true);
    _inInit										= false;
    applyDocumentsFilters						();
    updateUi									();
    emit startScanning(E::ScanOptionAnalysis);
}

void RSUUI::DlgMain::on_btnSlipCreate_clicked () {
    // Make sure all documents are loaded for this query.
    DocumentsModel		*docs		= _core->documents();
    while (docs->canFetchMore())
        docs->fetchMore();
    // No document in filter...
    if (!docs->rowCount())			return;
    // Make sure all documents are valid.
    for (qint32 row=0; row<docs->rowCount(); ++row) {
        RE::E::DocumentValidity
                        val			= (RE::E::DocumentValidity)docs->index(row, DocumentsModel::ExtractedValidity).data().toInt();
        if (val!=RE::E::DocumentValidityValid) {
            QMessageBox::warning(
                this,
                tr("Warning"),
                tr("One or more documents are not valid. Please either delete or correct the invalid documents and try again."),
                QMessageBox::Ok);
            return;
        }
    }
    // Make a list of filtered document ID.
    QStringList			docIds;
    for (qint32 row=0; row<docs->rowCount(); ++row)
        docIds						<< docs->index(row, DocumentsModel::Key).data().toString();
    // Retrieve issuers count for these documents.
    QString				sq			= QString(
                                        "SELECT COUNT(DISTINCT(DocumentTypes.issuer_id))"\
                                        "	FROM DocumentTypes,Documents"\
                                        "	WHERE Documents.documentType_id=DocumentTypes.id AND Documents.id IN (%1)"\
                                        "		AND (%2);")
                                        .arg(docIds.join(","))
                                        .arg(docs->filter());
    QSqlQuery			qi			(sq);
    // Cannot check for issuers count.
    if (!qi.next()) {
        QMessageBox::critical(
            this,
            tr("Error"),
            tr("An unknown error occured while checking for filtered documents issuers: %1").arg(qi.lastError().text()),
            QMessageBox::Ok);
        return;
    }
    // More than one issuer.
    else if (qi.value(0).toInt()!=1) {
        QMessageBox::warning(
            this,
            tr("Warning"),
            tr("Filtered documents seem to be from multiple issuers. Please refine the filters until the resulting documents belong to only one issuer."),
            QMessageBox::Ok);
        return;
    }
    // Confirm action.
    else if (QMessageBox::No==QMessageBox::question(
            this,
            tr("Slip Creation"),
            tr("You are about to create a new slip from all the filtered documents. Once done, it will be impossible to add any other documents to the created slip.\nWould you like to continue?"),
            QMessageBox::Yes | QMessageBox::No))
        return;

    // Look if there is a slip document in the list.
    qi.seek                         (0);
    for (qint32 row=0; row<docs->rowCount(); ++row) {
        quint32         dtId        = docs->documentTypeKeyForRow(row);
        if (_core->physicalSlipKeys().contains(dtId)) {
            // TODO!
        }
    }
    // Make core create slip.
    emit			createSlipWithDocuments();
}
// Selection watchers.
void RSUUI::DlgMain::documentsSelectionChanged () {
    updateUi						();
}
// Document Fix dialog events.
void RSUUI::DlgMain::documentFixDialogFinished (int r) {
    if (r!=QDialog::Accepted)				return;
    _core->documents()->updateWithDocument	(_ui.tblVwDocuments->selectionModel()->selectedRows().first().row(), &_dlgDocFix->document());
    _core->documents()->submitAll			();
    _dlgDocFix->deleteLater					();
    _dlgDocFix								= 0;
}

void RSUUI::DlgMain::updateUi () {
    // Cache models.
    DocumentsModel				*docs		= _core->documents();
    // Cache some values.
    bool						scanerReady	= _core->scannerHelper().state==E::ScannerStateIdle;
    bool						scanerBusy	= _core->scannerHelper().state==E::ScannerStateBusy;
    QModelIndexList const		&idxs		= _ui.tblVwDocuments->selectionModel()->selectedRows();
    quint32						selCnt		= idxs.count();
    bool						oneDoc		= selCnt==1;
    bool						fixDoc		= false;
    if (oneDoc) {
        RE::E::DocumentValidity	val			= (RE::E::DocumentValidity)docs->index(idxs.first().row(), DocumentsModel::ExtractedValidity).data().toInt();
        fixDoc								= val==RE::E::DocumentValidityIncomplete;
    }
    // Set up UI.
    _ui.wgtMainButtonsBar->setEnabled		(!_isScanning && !scanerBusy);
    _ui.btnInitScanner->setVisible			(!scanerReady && !scanerBusy);
    _ui.btnFreeScanner->setVisible			(scanerReady || scanerBusy);
    _ui.btnScanStart->setEnabled			(scanerReady);
    _ui.btnDocumentsDelete->setEnabled		(selCnt);
    _ui.btnDocumentFix->setEnabled			(fixDoc);

    // Prepare status text and color.
    QString						docStat,
                                txtClr;
    if (!selCnt) {
        txtClr								= "#aaa";
        docStat								= tr("No document in selection");
    }
    else if (selCnt==1) {
        quint32					row			= idxs.first().row();
        QByteArray				ba			= docs->index(row, DocumentsModel::Data).data().toByteArray();
        RE::T::Document			doc			= RE::valueFromStreamedByteArray<RE::T::Document>(ba);
        // Build status text and color.
        RE::E::DocumentValidity
                                val			= (RE::E::DocumentValidity)docs->index(row, DocumentsModel::ExtractedValidity).data().toInt();
        if (val==RE::E::DocumentValidityValid) {
            txtClr							= "#afa";
            docStat							= doc.acceptationReasons.join(" ");
        }
        else if (val==RE::E::DocumentValidityInvalid) {
            txtClr							= "#f66";
            docStat							= doc.rejectionReasons.join(" ");
        }
        else {
            txtClr							= "#ff4";
            docStat							= doc.rejectionReasons.join(" ");
        }
    }
    else {
        txtClr								= "#aaa";
        docStat								= tr("Multiple documents in selection");
    }
    _ui.txtDocumentsStatus->setStyleSheet	(QString("color: %1;").arg(txtClr));
    _ui.txtDocumentsStatus->setText			(docStat);
}

void RSUUI::DlgMain::updateHtmlSummary () {
    // Prepare HTML document.
    if (!_slipSummaryDoc) {
        _slipSummaryDoc						= new QTextDocument();
        // Set up stylesheet on slip summary widget.
        QFile					fStyle		(":/Resources/Stylesheets/SlipSummary.css");
        fStyle.open							(QIODevice::ReadOnly|QIODevice::Text);
        QString					css			(fStyle.readAll());
        fStyle.close						();
        _slipSummaryDoc->addResource		(QTextDocument::StyleSheetResource, QUrl("SlipSummary.css"), css);
        // Set document.
        _ui.txtSlipSummary->setDocument		(_slipSummaryDoc);
    }

    QFile                       fHtml       (":/Resources/HTML/WorkingSlipSummary.html");
    fHtml.open                              (QIODevice::ReadOnly|QIODevice::Text);
    QString                     html        = QString::fromUtf8(fHtml.readAll())
                                                .replace("%GeneratedSlipTitle%", _core->documents()->htmlSlipTitle())
                                                .replace("%GeneratedSlipDetails%", _core->documents()->htmlSlipDetails());
    fHtml.close                             ();
    _slipSummaryDoc->setHtml				(html);
}
