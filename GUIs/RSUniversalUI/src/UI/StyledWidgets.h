#ifndef RSUUI_StyledWidgets_h
#define RSUUI_StyledWidgets_h

// Base classes.
#include <QtGui/QFrame>
#include <QtGui/QPushButton>

namespace RSUUI {

class WgtButtonsBar : public QFrame {
Q_OBJECT
public:
	/**/	WgtButtonsBar			(QWidget *p=0, Qt::WFlags f=0) : QFrame(p, f) {};
};
class WgtMainButtonsBar : public WgtButtonsBar {
Q_OBJECT
public:
	/**/	WgtMainButtonsBar		(QWidget *p=0, Qt::WFlags f=0) : WgtButtonsBar(p, f) {};
};
class WgtSecondaryButtonsBar : public WgtButtonsBar {
Q_OBJECT
public:
	/**/	WgtSecondaryButtonsBar	(QWidget *p=0, Qt::WFlags f=0) : WgtButtonsBar(p, f) {};
};

class BtnNormal : public QPushButton {
Q_OBJECT
public:
	/**/	BtnNormal				(QWidget *p=0) : QPushButton(p) {};
};
class BtnClose : public BtnNormal {
Q_OBJECT
public:
	/**/	BtnClose				(QWidget *p=0) : BtnNormal(p) {};
};
class BtnAccept : public BtnNormal {
Q_OBJECT
public:
	/**/	BtnAccept				(QWidget *p=0) : BtnNormal(p) {};
};
class BtnReject : public BtnNormal {
Q_OBJECT
public:
	/**/	BtnReject				(QWidget *p=0) : BtnNormal(p) {};
};

}

#endif

