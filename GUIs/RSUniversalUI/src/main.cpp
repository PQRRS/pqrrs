#include "Core.h"
#include <LMLibrary/Core>
#include <QtGui/QFont>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QLocale>
#include <QtCore/QProcess>
#include <QtCore/QTextCodec>
#include <QtCore/QTranslator>
#include <QtGui/QFontDatabase>

#include "../ThirdParty/QtSingleApplication/src/QtSingleApplication"

#include <iostream>
int main (int argc, char *argv[]) {
    // Version requested.
    if (argc==2 && QString(argv[1])=="--version") {
        std::cout						<< RSUUI::Version();
        return							0;
    }

    // Make the app.
    QtSingleApplication         *a      =  new QtSingleApplication("fr.PierreQR.RecognitionSuite.Gui.RSUniversalUI", argc, argv);
    QtSingleApplication::setOrganizationName    ("Pierre qui Roule EURL");
    QtSingleApplication::setOrganizationDomain  ("fr.PierreQR");
    QtSingleApplication::setApplicationName     ("RS Universal UI");
    a->setQuitOnLastWindowClosed                (false);
    if (a->isRunning()) {
        a->sendMessage                  ("WakeUp");
        return                          0;
    }

    // Initialize everything.
    RSUUI::StaticInitializer                ();

    // Set translation encoding.
    QLocale						loc		= QLocale::system();
    QLocale::setDefault					(loc);
    // Install translation.
    QTranslator					trans;
    { // Memguard.
        // Install translator.
        bool					loadOk	= trans.load(":/Translations/RSUniversalUI.qm");
        if (loadOk)
            a->installTranslator        (&trans);
    }

    // Read fonts and make them available.
    { // Memguard.
        QStringList				fList;
        fList							<< "Calculator.ttf";
        foreach (QString const &fName, fList) {
            QFile				res		(":/Resources/Fonts/"+fName);
            if (!res.open(QIODevice::ReadOnly)) {
                Dbg						<< "Cannot load font" << fName << "from resources.";
                continue;
            }
            else if (QFontDatabase::addApplicationFontFromData(res.readAll())==-1) {
                Dbg						<< "Cannot add font" << fName << "to application.";
                continue;
            }
        }
    }
    QObject::connect(a, SIGNAL(messageReceived(QString const&)), &RSUUI::Core::Instance(), SLOT(singleApplicationMessage(QString const&)));
    int							ret     	= a->exec();
    RSUUI::Core::Destroy                    ();
    return                                  ret;
}
