#ifndef RSUUI_RSUUITypes_h
#define RSUUI_RSUUITypes_h

// Required stuff.
#include <QtCore/QObject>
#include <QtCore/QVariant>
class QMutex;

#ifndef Dbg
#   define Dbg      qDebug()    << Q_FUNC_INFO << "#"
#   define Wrn      qWarning()  << Q_FUNC_INFO << "#"
#   define Crt      qCritical() << Q_FUNC_INFO << "#"
#endif

#pragma pack(1)

#define	RegisterEnumStream(T)\
        friend QDataStream&	operator<<	(QDataStream& s, T const& t)	{ int i=t; return s << i; };\
        friend QDataStream&	operator>>	(QDataStream& s, T &t)			{ int i; s >> i; t=(T)i; return s; }

namespace SC {
    struct DeviceConfig;
    struct Config;
    class AScannerController;
}

namespace RSUUI {
    static const char * const   SoftwareIdentifier = "fr.PierreQR.RecognitionSuite.Gui.RSUniversalUI";

    // ------------------------------------			Constants			------------------------------------ //
    extern qint32 const             UnrecognizedDocumentTypeCode;
    extern quint32 const            OrpheanIssuerKey;
    extern quint32 const            UnknownDocumentTypeKey;
    extern quint32 const            WorkingSlipKey;


    // ------------------------------------				Enums			------------------------------------ //
    // Enum ranges containers.
    typedef QPair<int,int>	MetaTypesRange;
    typedef QList<MetaTypesRange> MetaTypesRangeList;
    typedef QHash<QMetaObject const*,MetaTypesRangeList> MetaTypesRangeListHash;
    // Enums.
    class E {
    Q_GADGET
    public:
    Q_ENUMS	(First SlipStatus ScannerState ScanOption Last)
    Q_FLAGS	(ScanOptions)
        enum        First				{ FirstValue };
        enum        SlipStatus			{ SlipOpen = 0, SlipSent, SlipDisputed, SlipPaid };
        enum        ScannerState		{ ScannerStateFree = 0, ScannerStateBusy, ScannerStateIdle };
        enum    	ScanOption			{ ScanOptionNone = 0, ScanOptionAnalysis = 1, ScanOptionEndorseFront = 2, ScanOptionEndorseBack = 4 };
        enum        Last				{ LastValue };
        // Expose enums and flags.
        Q_DECLARE_FLAGS		(ScanOptions, ScanOption)
        RegisterEnumStream	(RSUUI::E::First)
        RegisterEnumStream	(RSUUI::E::SlipStatus)
        RegisterEnumStream	(RSUUI::E::ScannerState)
        RegisterEnumStream	(RSUUI::E::ScanOption)
        RegisterEnumStream	(RSUUI::E::Last)
    };
    // Enum helper methods.
    QMetaEnum	MetaForEnumType				(QMetaObject const *mo, QVariant::Type const &t);
    QVariant	VariantForEnumMetatypeId	(QVariant::Type mt, int v);
    int			IntValueForEnumVariant		(QVariant const &v);


    // ------------------------------------				Types			------------------------------------ //
    #ifdef Q_MOC_RUN
    class  T { Q_GADGET public:
    #else
    namespace T {
    #endif

        class ScannerHelper {
        public:
            /**/					ScannerHelper				();
            // Members.
            SC::DeviceConfig		*deviceConfig;
            SC::Config				*config;
            SC::AScannerController	*controller;
            QMutex					*mutex;
            RSUUI::E::ScannerState	state;
            RSUUI::E::ScanOptions	options;
        };

        // Force metaobject declaration.
        extern const QMetaObject staticMetaObject;
    }

    // ------------------------------------		Plugin Interface		------------------------------------ //
    struct PluginBaseInfos {
        // Types.
        typedef QList<PluginBaseInfos> List;
        // Interface-useable members.
        QString					name,
                                version,
                                description;
        // Reserved.
        QString					fileBaseName;
        MetaTypesRangeListHash	enumeratorRanges;
        // Required for QList.
        inline bool				operator==		(PluginBaseInfos const &bi) { return name==bi.name && version==bi.version; }
    };
    typedef QList<QMetaObject const*> PluginMetaObjectList;

    // -------------------------------------		Other stuff			------------------------------------- //
    // Complete types registration for streaming and register metatypes.
    void			StaticInitializer					();
    char const*     Version								();
}

#pragma pack()

// Plugin base informations.
Q_DECLARE_TYPEINFO(RSUUI::PluginBaseInfos, Q_COMPLEX_TYPE);
Q_DECLARE_METATYPE(RSUUI::PluginBaseInfos)

// Enums.
Q_DECLARE_TYPEINFO(RSUUI::E::First, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RSUUI::E::First)
Q_DECLARE_TYPEINFO(RSUUI::E::SlipStatus, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RSUUI::E::SlipStatus)
Q_DECLARE_TYPEINFO(RSUUI::E::ScannerState, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RSUUI::E::ScannerState)
Q_DECLARE_TYPEINFO(RSUUI::E::ScanOption, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RSUUI::E::ScanOption)
Q_DECLARE_TYPEINFO(RSUUI::E::Last, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(RSUUI::E::Last)
// Flags.
Q_DECLARE_OPERATORS_FOR_FLAGS(RSUUI::E::ScanOptions)
// Classes.
Q_DECLARE_TYPEINFO(RSUUI::T::ScannerHelper, Q_MOVABLE_TYPE);
Q_DECLARE_METATYPE(RSUUI::T::ScannerHelper)

#endif
