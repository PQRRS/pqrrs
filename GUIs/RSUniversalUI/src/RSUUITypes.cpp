#include "RSUUITypes.h"
#include "../../Versioning/RSUniversalUIVersion.h"
// Required stuff.
#include <SCLibrary/SCTypes>
#include <RELibrary/RETypes>
#include "Core.h"
#include "Utilities/ThemeManager.h"
#include <QtCore/QDebug>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>

qint32 const	RSUUI::UnrecognizedDocumentTypeCode	= -1;
quint32 const	RSUUI::OrpheanIssuerKey				= 1;
quint32 const	RSUUI::UnknownDocumentTypeKey		= 1;
quint32 const	RSUUI::WorkingSlipKey				= 1;

RSUUI::T::ScannerHelper::ScannerHelper ()
: deviceConfig(), config(),
controller(0), mutex(), state(E::ScannerStateFree), options(E::ScanOptionAnalysis) {
}

void RSUUI::StaticInitializer () {
    // Make sure SC / RE static init is done.
    SC::StaticInitializer   ();

    static bool	initDone	= false;
    if (initDone)			return;
    initDone				= true;

    // Enums.
    #define EnumFullRegistration(name)	qRegisterMetaType<name>(#name); qRegisterMetaTypeStreamOperators<int>(#name)
    EnumFullRegistration	(RSUUI::E::First);
    EnumFullRegistration	(RSUUI::E::SlipStatus);
    EnumFullRegistration	(RSUUI::E::ScannerState);
    EnumFullRegistration	(RSUUI::E::ScanOption);
    EnumFullRegistration	(RSUUI::E::Last);
    #undef EnumFullRegistration
    // All other types.
    #define TypeFullRegistration(name)	qRegisterMetaType<name>(#name); qRegisterMetaTypeStreamOperators<name>(#name)
    #undef TypeFullRegistration
    qRegisterMetaType					<RSUUI::T::ScannerHelper>("RSUUI::T::ScannerHelper");
    // Make sure at least one RSUUI::Core is present.
    RSUUI::Core::Instance               ();
}
char const* RSUUI::Version () {
    return const_cast<char*>(STRINGIZE(RSUniversalUIVersion));
}

QMetaEnum RSUUI::MetaForEnumType (QMetaObject const *mo, QVariant::Type const &t) {
    QString const	&mtName		= QMetaType::typeName(t);
    QString			mtNameArr	= mtName.mid(mtName.lastIndexOf(':')+1);
    return mo->enumerator(mo->indexOfEnumerator(mtNameArr.toUtf8().constData()));
}
QVariant RSUUI::VariantForEnumMetatypeId (QVariant::Type mtid, int value) {
    return QVariant(mtid, (void*)&value);
}
int RSUUI::IntValueForEnumVariant (QVariant const &v) {
    return *(int const*)v.constData();
}
