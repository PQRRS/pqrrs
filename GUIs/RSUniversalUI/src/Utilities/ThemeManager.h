#ifndef RSUUI_ThemeManager_h
#define RSUUI_ThemeManager_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include <QtCore/QDir>

namespace RSUUI {

class ThemeManager : public QObject {
Q_OBJECT

public:
    // Constructors / Destructor.
    /**/                ThemeManager            (QObject *p=0);
    // Getters / Setters.
    QDir                themesPath              () const;
    QStringList         availableThemes         () const;
    QString             currentTheme            () const;
    void                setCurrentTheme         (QString const &n);
    QString             stylesheetPathForName   (QString const &n) const;
};

}

#endif
