#include "DatabaseManager.h"
// Required stuff.
#include <RELibrary/RETypes>
#include "../Core.h"
#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

RSUUI::DatabaseManager::DatabaseManager(QObject *p)
: QObject(p), _database(QSqlDatabase::addDatabase("QSQLITE")) {
}

QDir RSUUI::DatabaseManager::databasesPath () const {
    return QDir(QString("%1/Databases/").arg(QDir::currentPath()));
}

QStringList RSUUI::DatabaseManager::availableDatabases () const {
    return databasesPath().entryList(QStringList() << "*.rsuuidb", QDir::Files, QDir::Name);
}

QString RSUUI::DatabaseManager::currentDatabaseName () const {
    return                  QSettings().value("Preferences::Database").toString();
}
void RSUUI::DatabaseManager::setCurrentDatabaseName (QString const &n) {
    QSettings().setValue("Preferences::Database", n);
    // Close DB if open.
    if (_database.isOpen())
        _database.close                 ();

    // Store full database path.
    QString const       sDb             = QString("%1/%2").arg(databasesPath().path()).arg(n);
    // Check wether the database needs creation or not.
    QFile const         fDb             (sDb);
    bool const      	createDb		= !fDb.exists() || fDb.size()==0;

    // Initialize database.
    _database.setDatabaseName			(sDb);
    if (!_database.open()) {
        Wrn								<< "The database failed to open.";
        return;
    }

    // Create database if it's not yet done.
    if (createDb) {
        Dbg								<< "A default database needs to be created for" << n << ".";
        QFile			f				(":/Resources/SqlScripts/Create.sql");
        f.open							(QIODevice::ReadOnly);
        QString			l;
        while (!f.atEnd()) {
            QString		t				= f.readLine().simplified();
            if (t.startsWith("--"))		continue;
            l							+= " "+t;
            if (l.isEmpty() || !l.endsWith(';'))
                continue;
            QSqlError	e				= QSqlQuery(l).lastError();
            if (e.type()!=QSqlError::NoError) {
                Crt						<< "Query" << l << "failed with error" << e.text() << "!";
                _database.close			();
                return;
            }
            l.clear						();
        }
        f.close							();
    }
}
