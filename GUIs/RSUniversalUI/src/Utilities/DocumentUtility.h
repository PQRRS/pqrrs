#ifndef RSUUI_DocumentUtility_h
#define RSUUI_DocumentUtility_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include <QtSql/QSqlRecord>

namespace RE {
    namespace T {
        struct Document;
    }
}
namespace RSUUI {
    class Core;
}

namespace RSUUI {

class DocumentUtility : public QObject {
Q_OBJECT
public:
    /**/						DocumentUtility			(Core *c);
    void						postValidate			(RE::T::Document *doc) const;
    QSqlRecord					documentRecordForSerial	(QString const &serial) const;
    void						translateReasons		(RE::T::Document *doc) const;

private:
    Core						*_core;
};

}

#endif
