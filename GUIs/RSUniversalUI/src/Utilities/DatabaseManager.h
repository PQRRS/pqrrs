#ifndef RSUUI_DatabaseManager_h
#define RSUUI_DatabaseManager_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include <QtCore/QDir>
#include <QtSql/QSqlDatabase>

namespace RSUUI {

class DatabaseManager : public QObject {
Q_OBJECT
public:
    // Constructors / Destructor.
    /**/                DatabaseManager         (QObject *p=0);
    // Getters / Setters.
    QDir                databasesPath           () const;
    QStringList         availableDatabases      () const;
    QString             currentDatabaseName     () const;
    void                setCurrentDatabaseName  (QString const &n);
    QSqlDatabase&       database                () { return _database; }

private:
    QSqlDatabase        _database;
};

}

#endif
