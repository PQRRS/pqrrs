#include "ThemeManager.h"
// Required stuff.
#include "../Core.h"
#include "../Models/SettingsModel.h"

RSUUI::ThemeManager::ThemeManager(QObject *p)
: QObject(p) {
}

QDir RSUUI::ThemeManager::themesPath () const {
    return QDir(QString("%1/Themes/").arg(QDir::currentPath()));
}

QStringList RSUUI::ThemeManager::availableThemes () const {
    return themesPath().entryList(QStringList() << "*.rsuuitheme", QDir::Dirs, QDir::Name);
}

QString RSUUI::ThemeManager::currentTheme () const {
    SettingsModel   *s      = RSUUI::Core::Instance().settings();
    QString         theme   = s->valueForKey("Preferences::Theme");
    return themesPath().cd(QString("%1").arg(theme)) ? theme : QString();
}
void RSUUI::ThemeManager::setCurrentTheme (QString const &n) {
    RSUUI::Core::Instance().settings()->setValueForKey("Preferences::Theme", n);
}


QString RSUUI::ThemeManager::stylesheetPathForName (QString const &n) const {
    QString         theme       = currentTheme();
    QString const   intName     = QString(":/Resources/Stylesheets/%1.css").arg(n);
    if (theme.isEmpty())        return intName;
    QString         extName     = QString("%1/%2/%3.css").arg(themesPath().path()).arg(theme).arg(n);
    return QFile(extName).exists() ? extName : intName;
}
