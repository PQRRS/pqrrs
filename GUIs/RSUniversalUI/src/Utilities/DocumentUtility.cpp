#include "DocumentUtility.h"
// Required stuff.
#include <RELibrary/RETypes>
#include "../Core.h"
#include <QtCore/QDebug>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>

RSUUI::DocumentUtility::DocumentUtility (Core *c)
: QObject(c), _core(c) {
}

void RSUUI::DocumentUtility::postValidate (RE::T::Document *doc) const {
    // Invalid doc.
    if (doc->validity!=RE::E::DocumentValidityValid)
        return;
    // It has no serial number!
    else if (!doc->strings.contains("Serial") || doc->strings["Serial"].isEmpty()) {
        doc->validity					= RE::E::DocumentValidityIncomplete;
        doc->rejectionReasons			= QStringList() << tr("This document has no serial number.");
    }
    // Document is a physical slip.
    else if (_core->physicalSlipKeys().contains(_core->documentTypeKeyForRCode(doc->typeData->code))) {
        doc->validity                   = RE::E::DocumentValidityValid;
        doc->acceptationReasons         = QStringList() << tr("This document is a physical slip.");
    }
    // It has no amount!
    else if (!doc->strings.contains("Amount")) {
        doc->validity					= RE::E::DocumentValidityIncomplete;
        doc->rejectionReasons			= QStringList() << tr("This document has no amount.");
    }
    // It is already in database!
    QSqlRecord			rec				= documentRecordForSerial(doc->strings["Serial"]);
    if (!rec.isEmpty()) {
        QString			docCode			= QString("DOC%1").arg(rec.value(0).toInt());
        quint32			slipKey			= rec.value(1).toInt();
        QString			slipCode		= QString("%1%2").arg(slipKey==1?"TRV":"BRD").arg(slipKey);
        // Retrieve date.
        QDateTime		dt;
        dt.setTime_t					(rec.value(2).toInt());
        // Finally reject.
        doc->validity					= RE::E::DocumentValidityInvalid;
        doc->rejectionReasons			= QStringList()
                                            << tr("This document has already been registered as %1 in %2, created on %3.")
                                                .arg(docCode)
                                                .arg(slipCode)
                                                .arg(dt.date().toString(Qt::DefaultLocaleShortDate));
    }
}
QSqlRecord RSUUI::DocumentUtility::documentRecordForSerial (QString const &serial) const {
    QSqlQuery			q;
    q.prepare					("SELECT id,slip_id,createdOn FROM Documents WHERE extractedSerial=:serial;");
    q.bindValue					(":serial",		serial);
    q.exec						();
    return						q.next() ? q.record() : QSqlRecord();
}
void RSUUI::DocumentUtility::translateReasons (RE::T::Document *doc) const {
    // Recognition package: CRT.
    Q_UNUSED(QT_TR_NOOP("The issuer code does not match."));
    // Recognition package: Serial.
    Q_UNUSED(QT_TR_NOOP("A serial has been found, but it is not properly formatted."));
    Q_UNUSED(QT_TR_NOOP("A serial has been found, but it seems corrupted."));
    Q_UNUSED(QT_TR_NOOP("A serial has been found, but its checksum is incorrect."));
    Q_UNUSED(QT_TR_NOOP("The serial shows an outdated value."));
    Q_UNUSED(QT_TR_NOOP("The serial is valid."));

    // Translate acceptation reasons.
    QStringList		tARs;
    foreach (QString const &ar, doc->acceptationReasons)
        tARs					<< tr(ar.toUtf8());
    doc->acceptationReasons		= tARs;
    // Translate rejection reasons.
    QStringList		tRRs;
    foreach (QString const &rr, doc->rejectionReasons)
        tRRs					<< tr(rr.toUtf8());
    doc->rejectionReasons		= tRRs;
}
