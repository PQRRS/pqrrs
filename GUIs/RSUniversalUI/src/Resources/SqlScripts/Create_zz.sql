-- Settings table.
CREATE TABLE IF NOT EXISTS
    Settings (
        key						TEXT,
        value					TEXT
    );
CREATE UNIQUE INDEX			idxSettingsKey			ON Settings(key);

-- Issuers table.
CREATE TABLE
	Issuers (
		id					INTEGER PRIMARY KEY		AUTOINCREMENT,
		name				TEXT					DEFAULT NULL
	);
CREATE UNIQUE INDEX			idxIssuersName			ON Issuers(name);


-- Document types table.
CREATE TABLE
	DocumentTypes (
		id					INTEGER PRIMARY KEY		AUTOINCREMENT,
		issuer_id			INTEGER,
		recognitionCode		INTEGER,
        isPhysicalSlip      INTEGER,
		name				TEXT					DEFAULT NULL,
		FOREIGN KEY			(issuer_id)				REFERENCES	Issuers			(id)	ON DELETE CASCADE
	);
CREATE UNIQUE INDEX			idxDocumentTypeName		ON DocumentTypes(name);
CREATE UNIQUE INDEX			idxDocumentTypeRCode	ON DocumentTypes(recognitionCode);
CREATE INDEX				idxDocumentTypeIssuer	ON DocumentTypes(issuer_id);


-- Slips table.
CREATE TABLE
	Slips (
		id					INTEGER PRIMARY KEY		AUTOINCREMENT,
		createdOn			INTEGER					DEFAULT (strftime('%s','now')),
		status				INTEGER					DEFAULT 0,
		comments			TEXT					DEFAULT NULL
	);
CREATE INDEX				idxSlipCreatedOn		ON Slips(createdOn);
CREATE INDEX				idxSlipStatus			ON Slips(status);
CREATE INDEX				idxSlipComments			ON Slips(comments);


-- Documents table.
CREATE TABLE
	Documents (
		id					INTEGER PRIMARY KEY AUTOINCREMENT,
		documentType_id		INTEGER,
		slip_id				INTEGER					DEFAULT 1,
		pointOfSale			INTEGER,
		createdOn			INTEGER					DEFAULT (strftime('%s','now')),
		extractedUuid		TEXT					DEFAULT NULL,
		extractedAmount		REAL					DEFAULT NULL,
		extractedSerial		TEXT					DEFAULT NULL,
		extractedValidity	INTEGER					DEFAULT 0,
		data				BLOB,
		FOREIGN KEY			(documentType_id)		REFERENCES	DocumentTypes	(id)	ON DELETE CASCADE,
		FOREIGN KEY			(slip_id)				REFERENCES	Slips			(id)	ON DELETE CASCADE
	);
CREATE INDEX				idxDocumentDocumentType	ON Documents(documentType_id);
CREATE INDEX				idxDocumentTypeSlip		ON Documents(slip_id);
CREATE INDEX				idxDocumentPointOfSale	ON Documents(pointOfSale);
CREATE INDEX				idxDocumentCreatedOn	ON Documents(createdOn);
CREATE INDEX				idxDocumentSerial		ON Documents(extractedSerial);
CREATE INDEX				idxDocumentValidity		ON Documents(extractedValidity);


-- Issuers default data.
INSERT INTO
	Issuers			(id,	name)
    VALUES			(NULL,	"Other");
INSERT INTO
	Issuers			(id,	name)
	VALUES			(NULL,	"CRT");
INSERT INTO
	Issuers			(id,	name)
	VALUES			(NULL,	"ANCV");

-- No Type.
INSERT INTO
    DocumentTypes	(id,	issuer_id,	recognitionCode,    isPhysicalSlip,	name)
    VALUES			(NULL,	1,			-1,					0,              "Unknown");
-- CRT types.
INSERT INTO
    DocumentTypes	(id,	issuer_id,	recognitionCode,	isPhysicalSlip, name)
    VALUES			(NULL,	2,			1,					0,              "Cheque Dejeuner");
INSERT INTO
    DocumentTypes	(id,	issuer_id,	recognitionCode,	isPhysicalSlip, name)
    VALUES			(NULL,	2,			2,					0,              "Ticket Restaurant");
INSERT INTO
    DocumentTypes	(id,	issuer_id,	recognitionCode,	isPhysicalSlip, name)
    VALUES			(NULL,	2,			3,					0,              "Cheque de Table");
INSERT INTO
    DocumentTypes	(id,	issuer_id,	recognitionCode,	isPhysicalSlip, name)
    VALUES			(NULL,	2,			4,					0,              "Cheque Restaurant");
INSERT INTO
    DocumentTypes	(id,	issuer_id,	recognitionCode,	isPhysicalSlip, name)
    VALUES			(NULL,	2,			7,					1,              "CRT Slip");
-- ANCV Types.
INSERT INTO
    DocumentTypes	(id,	issuer_id,	recognitionCode,	isPhysicalSlip, name)
    VALUES			(NULL,	3,			5,					0,              "Cheque Vacances");

-- Default slips data.
INSERT INTO
	Slips			(id)
	VALUES			(NULL);
