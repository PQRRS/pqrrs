#ifndef RSUUI_Core_h
#define RSUUI_Core_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include "RSUUITypes.h"
#include <SCLibrary/SCTypes>
#include <QtCore/QDateTime>
#include <QtCore/QModelIndexList>
#include <QtGui/QSystemTrayIcon>
// Forward declarations.
namespace LM {
    class Core;
}
namespace RSUUI {
    // Plugins management.
    class PluginsManager;
    // Various classes.
    class ThreadedWorker;
    class DatabaseManager;
    class DocumentUtility;
    class ThemeManager;
    // Models.
    class SettingsModel;
    class IssuersModel;
    class DocumentTypesModel;
    class SlipsModel;
    class DocumentsModel;
    // Dialogs.
    class DlgMain;
    class DlgSettings;
    class DlgSlips;
}
namespace SC {
    class AScannerController;
    struct DeviceConfig;
    struct Config;
}
namespace RE {
    class Engine;
    namespace T {
        struct Document;
    }
}
class QMenu;
class QAction;

namespace RSUUI {

class Core : public QObject {
Q_OBJECT

protected:
    /**/								Core							(QObject *parent=0);
    /**/								~Core							();

public:
    // Singleton getters.
    inline static Core&                 Instance						()                      { return _Instance ? *_Instance : *new Core(); }
    inline static void                  Destroy							()                      { if (_Instance) { delete _Instance; _Instance = 0; } }

    // Loaded plugins info getter.
    PluginBaseInfos::List				loadedPlugins					() const;
    QHash<QString,QString> const&		objectsLocations				() const;

    // Various getters.
    inline T::ScannerHelper const&		scannerHelper					() const                { return _scHlpr; }
    inline RE::Engine*					recognitionEngine				() const                { return _re; }
    inline DocumentUtility*				documentUtility					() const                { return _docUtility; }
    inline ThemeManager*                themeManager                    () const                { return _themeManager; }
    // Getters on database and models.
    inline SettingsModel*               settings                        () const                { return _settings; }
    inline IssuersModel*				issuers							() const                { return _issuers; }
    inline DocumentTypesModel*			documentTypes					() const                { return _documentTypes; }
    inline SlipsModel*					slips							() const                { return _slips; }
    inline DocumentsModel*				documents						() const                { return _documents; }
    // Cached document types data getters.
    inline quint32                      documentTypeKeyForRCode         (qint32 rCode) const    { return _dtKeysByRCodes[rCode]; }
    inline QString                      documentTypeNameForKey          (quint32 key) const     { return _dtNameByKey[key]; }
    inline QList<quint32> const&        physicalSlipKeys                () const                { return _physicalSlipKeys; }
    // Print strings getters.
    QString                             frontPrintString                () const;
    QString								rearPrintString					() const;

    // Static SCLibrary callbacks.
    static quint32 SCCBCConv            staticCbProcessingStarted		(RSUUI::Core *_this);
    static quint32 SCCBCConv            staticCbProcessingFinished		(RSUUI::Core *_this);
    static quint32 SCCBCConv            staticCbNewDocument				(RSUUI::Core *_this, quint32 docId, quint32 code, char const *name, RE::T::Document *doc);
    static quint32 SCCBCConv            staticCbDocumentRecognitionDone (RSUUI::Core *_this, quint32 docId, quint32 stringCount, char const *timestamp, RE::T::Document *doc);
    quint32								cbProcessingStarted				();
    quint32								cbProcessingFinished			();
    quint32								cbNewDocument					(RE::T::Document *doc);
    quint32								cbDocumentRecognitionDone		(RE::T::Document *doc);

    // Skin ease methods.
    void								prepareSkin						(QWidget &dlg);
    // Human-readable formaters.
    QString								humanReadableDateTime			(QDateTime const &dt);
    // Hardware hash getter.
    bool                                checkLicense                    () const;

protected:
    // Plugins stuff.
    PluginsManager&						pluginsManager					();
    void								parsePlugins					();

private slots:
    // Single-application message handler.
    void                                singleApplicationMessage        (QString const&);
    // System tray related slots.
    void								iconActivated					(QSystemTrayIcon::ActivationReason r);
    void								toggleMainWindow				();
    void								quitActionTriggered				();
    // Scanner controller slots.
    void								ensureScannerController			();
    void								initializeScanner				();
    void								freeScanner						();
    void								changeScannerState				(RSUUI::E::ScannerState);
    void								startScanning                   (RSUUI::E::ScanOptions opts);
    // Documents / Slips handling slots.
    void								deleteDocuments					(QModelIndexList const &idxs, QModelIndex const &slipIdx=QModelIndex());
    void								createSlipWithDocuments			();
    void								markSlipSent					(QModelIndex const &idx, bool integrityOk);
    void								markSlipDisputed				(QModelIndex const &idx);
    void								markSlipPaid					(QModelIndex const &idx, QString const &picPath);
    void								changeSlipStatus				(QModelIndex const &idx, RSUUI::E::SlipStatus ss, QString const &msg);
    void								deleteSlips						(QModelIndexList const &idxs);
    // Dialogs handling.
    void                                licenseMismatchDetected         ();
    void								presentSlipsDialog              (int row=0);
    void								slipsDialogFinished				(int r);
    void								toggleSettingsWindow			();
    void								settingsDialogFinished			(int r);

private:
    // Static instance.
    static Core							*_Instance;
    // Licensing sutuff.
    LM::Core                            *_lm;
    // Plugins management.
    PluginsManager						*_pluginsManager;
    // RELibrary stuff.
    RE::Engine                          *_re;
    // SCLibrary stuff.
    RSUUI::T::ScannerHelper				_scHlpr;
    // Database, models, and their proxy counterparts
    SettingsModel                       *_settings;
    IssuersModel						*_issuers;
    DocumentTypesModel					*_documentTypes;
    SlipsModel							*_slips;
    DocumentsModel						*_documents;
    // Cached document types names and keys.
    QHash<qint32,quint32>               _dtKeysByRCodes;
    QHash<qint32,QString>               _dtNameByKey;
    QList<quint32>                      _physicalSlipKeys;
    // Document utility.
    ThreadedWorker                      *_threadedWorker;
    DatabaseManager                     *_dbManager;
    DocumentUtility						*_docUtility;
    ThemeManager                        *_themeManager;
    // System tray icon and it's menu.
    QSystemTrayIcon						*_systrayIcon;
    QMenu								*_mnuSystray;
    QAction								*_actSystrayMainWin;
    QAction								*_actSystraySettingsWin;
    // Dialogs.
    DlgMain								*_dlgMain;
    DlgSettings							*_dlgSettings;
    DlgSlips							*_dlgSlips;

signals:
    void                                licenseMismatch                 () const;
    void								status							(QString msg) const;
    void								scannerStateChanged				(RSUUI::T::ScannerHelper const&) const;
    void								scanningStarted					(RSUUI::T::ScannerHelper const&) const;
    void								scanningFinished				(RSUUI::T::ScannerHelper const&) const;
    void								documentScanned					() const;
};

}

#endif
