#include "IssuersModel.h"
// Required stuff.
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlIndex>

RSUUI::IssuersModel::IssuersModel (QObject *p, QSqlDatabase const &db)
: QSqlTableModel(p, db) {
    // Setup model table and edition strategy.
    setTable				("Issuers");
    setEditStrategy			(QSqlTableModel::OnManualSubmit);
    // Setup headers.
    setHeaderData			(Key,					Qt::Horizontal, tr("Key"));
    setHeaderData			(Name,					Qt::Horizontal, tr("Name"));
}

Qt::ItemFlags RSUUI::IssuersModel::flags (const QModelIndex &i) const {
    return QSqlTableModel::flags(i) | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
void RSUUI::IssuersModel::triggerUpdate (QModelIndex const &idx) {
    emit dataChanged(idx, idx);
}

int RSUUI::IssuersModel::columnCount (const QModelIndex&) const {
    return Count;
}
QVariant RSUUI::IssuersModel::data (const QModelIndex &i, int r) const {
    if (!i.isValid())
        return QVariant();
    else
        return QSqlTableModel::data(i, r);
}
