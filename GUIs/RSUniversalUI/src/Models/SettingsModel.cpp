#include "SettingsModel.h"
// Required stuff.
#include "../Core.h"
#include <RELibrary/RETypes>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>

RSUUI::SettingsModel::SettingsModel (QObject *p, QSqlDatabase const &db)
: QSqlTableModel(p, db) {
    // Setup model table and edition strategy.
    setTable				("Settings");
    setEditStrategy			(QSqlTableModel::OnFieldChange);
    // Setup headers.
    setHeaderData			(Key,					Qt::Horizontal, tr("Key"));
    setHeaderData			(Value,					Qt::Horizontal, tr("Value"));
}

int RSUUI::SettingsModel::columnCount (const QModelIndex&) const {
    return Count;
}
QString RSUUI::SettingsModel::valueForKey (QString const &key, QString const &dv) const {
    QSqlQuery		q;
    q.prepare				("SELECT value FROM Settings WHERE key=:key;");
    q.bindValue				(":key",	key);
    q.exec					();
    return q.next() ? q.value(0).toString() : dv;
}
void RSUUI::SettingsModel::setValueForKey (QString const &key, QString const &value) {
    QSqlQuery		q;
    q.prepare				("INSERT OR REPLACE INTO Settings (key,value) VALUES (:key, :value);");
    q.bindValue				(":key",	key);
    q.bindValue				(":value",	value);
    if (q.exec()) {
        select();
        return;
    }
}
