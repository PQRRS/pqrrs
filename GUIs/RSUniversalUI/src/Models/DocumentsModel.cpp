#include "DocumentsModel.h"
// Required stuff.
#include "../Core.h"
#include "../Utilities/DocumentUtility.h"
#include <RELibrary/RETypes>
#include <QtCore/QFile>
#include <QtCore/QLocale>
#include <QtCore/QDateTime>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <limits>

RSUUI::DocumentsModel::DocumentsModel (Core *c, QSqlDatabase const &db)
: QSqlRelationalTableModel(c, db), _core(c) {
    // Setup model table and edition strategy.
    setTable				("Documents");
    setEditStrategy			(OnManualSubmit);

    // Setup relationships.
    setRelation				(DocumentType,			QSqlRelation("DocumentTypes",	"id",	"name"));
    setRelation				(Slip,					QSqlRelation("Slips",			"id",	"createdOn"));

    // Setup headers.
    setHeaderData			(Key,					Qt::Horizontal, tr("Key"));
    setHeaderData			(DocumentType,			Qt::Horizontal, tr("Type"));
    setHeaderData			(Slip,					Qt::Horizontal, tr("Slip"));
    setHeaderData			(PointOfSale,			Qt::Horizontal,	tr("POS"));
    setHeaderData			(CreatedOn,				Qt::Horizontal, tr("Date"));
    setHeaderData			(ExtractedUUID,			Qt::Horizontal, tr("UUID"));
    setHeaderData			(ExtractedAmount,		Qt::Horizontal, tr("Amnt."));
    setHeaderData			(ExtractedSerial,		Qt::Horizontal, tr("Serial"));
    setHeaderData			(ExtractedValidity,		Qt::Horizontal, tr("Valid."));
    setHeaderData			(Data,					Qt::Horizontal, tr("Data"));
}
// Filtering methods.
void RSUUI::DocumentsModel::setCriteria (QDateTime const &startDate, QDateTime const &endDate, quint32 issuerId, QList<quint32> validity, quint32 slipId) {
    if (!validity.count())
        return		setFilter("0=1");

    QStringList		filter;
    filter					<< QString("Documents.slip_id=%1").arg(slipId);
    if (issuerId) {
        QSqlQuery		qi	(QString("SELECT id FROM DocumentTypes WHERE issuer_id=%1;").arg(issuerId));
        QStringList		dtIds;
        while (qi.next())
            dtIds			<< qi.value(0).toString();
        filter				<< QString("documentType_id IN (%1)").arg(dtIds.join(","));
    }
    if (startDate.isValid())
        filter				<< QString("Documents.createdOn>=%1").arg(startDate.toTime_t());
    if (endDate.isValid())
        filter				<< QString("Documents.createdOn<=%1").arg(endDate.addDays(1).toTime_t());
    // Filter on validity.
    QStringList	validityFilter;
    foreach (quint32 const &s, validity)
        validityFilter		<< QString::number(s);
    filter					<< QString("Documents.extractedValidity IN (%1)").arg(validityFilter.join(","));
    setFilter				(filter.join(" AND "));
}
// Force triggering an update at a given index.
void RSUUI::DocumentsModel::triggerUpdate (QModelIndex const &idx) {
    emit dataChanged(idx, idx);
}

// Creates a record from a document.
#include <QDebug>
RE::E::DocumentValidity RSUUI::DocumentsModel::createWithDocument (RE::T::Document const *oDoc) {
    RE::T::Document		doc			(*oDoc);
    // First, check if the document type exists.
    qint32				typeKey		= RSUUI::UnknownDocumentTypeKey;
    if (doc.strings.contains("Type.Code")) {
        qint32			typeRCode	= doc.strings["Type.Code"].toInt();
        bool            isPhySlip;
        if (typeRCode!=RSUUI::UnrecognizedDocumentTypeCode) {
            typeKey                 = _core->documentTypeKeyForRCode(typeRCode);
            if (typeKey)
                isPhySlip           = _core->physicalSlipKeys().contains(typeKey);
            else
                typeKey             = RSUUI::UnknownDocumentTypeKey;
        }
        Q_UNUSED(isPhySlip);
    }
    // Run post-validation and translate acceptation / rejection reasons.
    _core->documentUtility()->postValidate(&doc);
    _core->documentUtility()->translateReasons(&doc);
    // Create new document record in model.
    QString const			&uuid	= doc.strings["UUID"];
    QSqlRecord				rec		= record();
    rec.setNull						(Key);
    rec.setValue					(DocumentType,		typeKey);
    rec.setValue					(Slip,				RSUUI::WorkingSlipKey);
    rec.setValue					(PointOfSale,		0);
    rec.setValue					(CreatedOn,			doc.timestamp.toTime_t());
    rec.setValue					(ExtractedUUID,		uuid);
    rec.setValue					(ExtractedAmount,	doc.strings.contains("Amount") ? doc.strings["Amount"].toFloat() : 0.0);
    rec.setValue					(ExtractedSerial,	doc.strings.contains("Serial") ? doc.strings["Serial"] : "");
    rec.setValue					(ExtractedValidity,	doc.validity);
    rec.setValue					(Data,				RE::byteArrayFromStreamedValue(doc));
    // Insert record into model table.
    insertRecord					(-1, rec);
    return doc.validity;
}
void RSUUI::DocumentsModel::updateWithDocument (quint32 row, RE::T::Document const *oDoc) {
    RE::T::Document		doc			(*oDoc);

    // First, check if the document type exists.
    qint32				typeKey		= RSUUI::UnknownDocumentTypeKey;
    if (doc.strings.contains("Type.Code")) {
        qint32			typeRCode	= doc.strings["Type.Code"].toInt();
        if (typeRCode!=RSUUI::UnrecognizedDocumentTypeCode) {
            typeKey                 = _core->documentTypeKeyForRCode(typeRCode);
            if (!typeKey)
                typeKey             = RSUUI::UnknownDocumentTypeKey;
        }
    }
    // Update data.
    blockSignals                    (true);
    setData							(index(row, DocumentType),		typeKey);
    setData							(index(row, ExtractedAmount),	doc.strings.contains("Amount") ? doc.strings["Amount"].toFloat() : 0.0);
    setData							(index(row, ExtractedSerial),	doc.strings.contains("Serial") ? doc.strings["Serial"] : "");
    setData							(index(row, ExtractedValidity),	doc.validity);
    setData							(index(row, Data),				RE::byteArrayFromStreamedValue(doc));
    blockSignals                    (false);
    emit dataChanged(index(row,DocumentType), index(row,Data));
}

// Remove a document image.
void RSUUI::DocumentsModel::deleteImageAtRow (quint32 row) {
    QString const		&uuid			= index(row, ExtractedUUID).data().toString();
    QFile::remove						(QString("Images/%1.jpg").arg(uuid));
}
// Remove a document and delete it's image.
void RSUUI::DocumentsModel::removeRowAndDeleteImage (quint32 row) {
    deleteImageAtRow                    (row);
    removeRow                           (row);
}

// Ease querying...
quint32 RSUUI::DocumentsModel::documentCount (bool excludeSlips) const {
    // Prepare conditions stringlist.
    QStringList     conds   = QStringList()
                                << "Documents.documentType_id=DocumentTypes.id"
                                << "(" + filter() + ")";
    // Add physical slip exclusion to list.
    if (excludeSlips)
        conds                   << "DocumentTypes.isPhysicalSlip=0";
    // Join everything to a clean SQL conditions string.
    QString         szConds = conds.join(" AND ");

    // Query.
    QSqlQuery		q		(QString(
                                "SELECT COUNT(Documents.id)"\
                                "	FROM Documents,DocumentTypes"\
                                "	WHERE %1;"
                            ).arg(szConds));
    return			q.next()
                        ? q.value(0).toInt()
                        : 0;
}
float RSUUI::DocumentsModel::total (bool excludeSlips) const {
    // Prepare conditions stringlist.
    QStringList     conds   = QStringList()
                                << "Documents.documentType_id=DocumentTypes.id"
                                << "(" + filter() + ")";
    // Add physical slip exclusion to list.
    if (excludeSlips)
        conds                   << "DocumentTypes.isPhysicalSlip=0";
    // Join everything to a clean SQL conditions string.
    QString         szConds = conds.join(" AND ");

    // Query.
    QSqlQuery		q		(QString(
                                "SELECT SUM(Documents.extractedAmount)"\
                                "	FROM Documents,DocumentTypes"\
                                "	WHERE %1;"
                            ).arg(szConds));
    return			q.next() ? q.value(0).toFloat() : 0.0;
}
QHash<quint32,QString> RSUUI::DocumentsModel::distinctIssuers (bool excludeSlips) const {
    // Prepare conditions stringlist.
    QStringList     conds   = QStringList()
                                << "Documents.documentType_id=DocumentTypes.id"
                                << "DocumentTypes.issuer_id=Issuers.id"
                                << "(" + filter() + ")";
    // Add physical slip exclusion to list.
    if (excludeSlips)
        conds                   << "isPhysicalSlip=0";
    // Join everything to a clean SQL conditions string.
    QString         szConds = conds.join(" AND ");

    // Query.
    QSqlQuery		q		(QString(
                                "SELECT DISTINCT Issuers.id,Issuers.name"\
                                "	FROM Issuers,DocumentTypes,Documents"\
                                "	WHERE %1;"
                            ).arg(szConds));
    QHash<quint32,QString>
                    toRet;
    while (q.next())
        toRet[q.value(0).toInt()] = q.value(1).toString();
    return toRet;
}

// Make an HTML summary of displayed documents.
QString RSUUI::DocumentsModel::htmlSlipTitle (bool excludeSlips) const {
    // Prepare locale.
    QLocale const			loc;
    // Make base HTML string.
    quint32					docCnt		= documentCount(excludeSlips);
    return                              QString("<h3>%1 %2 - %3</h3>")
                                            .arg(docCnt)
                                            .arg(docCnt==1 ? tr("Document") : tr("Documents"))
                                            .arg(loc.toCurrencyString(total(excludeSlips)));
}
QString RSUUI::DocumentsModel::htmlSlipDetails (bool excludeSlips) const {
    // Prepare locale.
    QLocale const			loc;

    QStringList				htmlDetails;
    QHash<quint32,QString>	issuers		= distinctIssuers();
    foreach (quint32 issuerId, issuers.keys()) {
        // Prepare conditions stringlist.
        QStringList     conds   = QStringList()
                                    << "Documents.documentType_id=DocumentTypes.id"
                                    << QString("DocumentTypes.issuer_id=%1").arg(issuerId)
                                    << "(" + filter() + ")";
        // Add physical slip exclusion to list.
        if (excludeSlips)
            conds                   << "isPhysicalSlip=0";
        // Join everything to a clean SQL conditions string.
        QString         szConds = conds.join(" AND ");

        // Query.
        QSqlQuery			qg			(QString(
                                            "SELECT DocumentTypes.name,DocumentTypes.isPhysicalSlip,COUNT(Documents.id),SUM(Documents.extractedAmount)"\
                                            "	FROM DocumentTypes,Documents"\
                                            "	WHERE  %1"\
                                            "	GROUP BY DocumentTypes.id;"
                                        ).arg(szConds));
        QStringList			htmlList;
        quint32             grpCnt      = 0;
        float				grpTot		= 0.0;
        while (qg.next()) {
            QString			docName		= qg.value(0).toString();
            bool            isPhySlip   = qg.value(1).toInt();
            quint32			docsCnt		= qg.value(2).toInt();
            float			docsTot		= qg.value(3).toFloat();
            grpCnt                      += docsCnt;
            grpTot						+= docsTot;
            htmlList					<< QString("<tr><td class='label'>%1 %2</td><td class='amount'>%3</td></tr>")
                                            .arg(docsCnt)
                                            .arg(docName)
                                            .arg(isPhySlip ? "" : loc.toCurrencyString(docsTot));
        }
        // Concatenate into details.
        htmlDetails						<< QString("<tr><td colspan=2><hr /></td></tr><tr class='title'><td>%1 %2</td><td class='amount'>%3</td></tr>%4")
                                           .arg(grpCnt)
                                           .arg(issuers[issuerId])
                                           .arg(loc.toCurrencyString(grpTot))
                                           .arg(htmlList.join(""));
    }

    // Make base HTML string.
    return                              QString("<table width=100%>%2</table>")
                                            .arg(htmlDetails.join(""));
}

// Reimplemented methods.
Qt::ItemFlags RSUUI::DocumentsModel::flags (const QModelIndex &i) const {
    return QSqlRelationalTableModel::flags(i) | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
int RSUUI::DocumentsModel::columnCount (const QModelIndex&) const {
    return Count;
}
QVariant RSUUI::DocumentsModel::data (const QModelIndex &i, int r) const {
    if (r==Qt::DecorationRole) {
        if (i.column()==Key) {
            switch ((RE::E::DocumentValidity)index(i.row(), ExtractedValidity).data(Qt::DisplayRole).toInt()) {
                case RE::E::DocumentValidityValid:
                    return QPixmap(":/Resources/Images/Icons/Set/accept.png");
                case RE::E::DocumentValidityIncomplete:
                    return QPixmap(":/Resources/Images/Icons/Set/caution.gif");
                case RE::E::DocumentValidityInvalid:
                    return QPixmap(":/Resources/Images/Icons/Set/exclamation.gif");
            }
        }
    }
    return QSqlRelationalTableModel::data(i, r);
}
quint32 RSUUI::DocumentsModel::documentTypeKeyForRow (quint32 row) const {
    QSqlQuery       q;
    q.prepare                   ("SELECT documentType_id FROM Documents WHERE id=:id;");
    q.bindValue                 (":id", index(row, DocumentsModel::Key).data().toInt());
    q.exec                      ();
    q.next                      ();
    return q.value(0).toInt();
}

void RSUUI::DocumentsModel::moveRowsToSlipWithKey (quint32 slipId) {
    relationModel(Slip)->select	();
    quint32		rCount	= rowCount();
    blockSignals            (true);
    for (quint32 row=0; row<rCount; ++row)
        setData(index(row, Slip), slipId);
    blockSignals            (false);
    emit dataChanged(index(0,Slip), index(rCount-1, Slip));
}
