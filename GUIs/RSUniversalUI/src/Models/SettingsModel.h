#ifndef RSUUI_SettingsModel_h
#define RSUUI_SettingsModel_h

// Base class.
#include <QtSql/QSqlTableModel>
// Required stuff.
#include <QtCore/QVariant>

namespace RSUUI {

class SettingsModel : public QSqlTableModel {
Q_OBJECT
public:
	// Columns indices helpers.
	enum { Key, Value /**/, Count };
	// Constructor.
    /**/				SettingsModel       		(QObject *p, QSqlDatabase const &db);
	// Reimplemented methods.
	int					columnCount					(QModelIndex const &i) const;
	// K/V stuff.
    QString				valueForKey					(QString const &key, QString const &defaultValue=QString()) const;
	void				setValueForKey				(QString const &key, QString const &value);
};

}

#endif
