#include "SlipsModel.h"
// Required stuff.
#include "../Core.h"
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlIndex>

RSUUI::SlipsModel::SlipsModel (QObject *p, QSqlDatabase const &db)
: QSqlTableModel(p, db) {
    // Setup model table and edition strategy.
    setTable				("Slips");
    setEditStrategy			(QSqlTableModel::OnManualSubmit);
    // Setup headers.
    setHeaderData			(Key,					Qt::Horizontal, tr("Key"));
    setHeaderData			(CreatedOn,				Qt::Horizontal, tr("Date"));
    setHeaderData			(Status,				Qt::Horizontal, tr("Status"));
    setHeaderData			(Comments,				Qt::Horizontal, tr("Comments"));
    setHeaderData			(DocumentCount,			Qt::Horizontal, tr("Docs."));
    setHeaderData			(Total,					Qt::Horizontal, tr("Total"));
}
// Filter data.
void RSUUI::SlipsModel::setCriteria (QDateTime const &startDate, QDateTime const &endDate, QList<quint32> status, bool excludeWorkingSlip) {
    if (!status.count())
        return		setFilter("0=1");

    QStringList		filter;
    // Filter slips by ID.
    if (excludeWorkingSlip)
        filter				<< QString("id!=%1").arg(RSUUI::WorkingSlipKey);
    // Filter by date.
    if (startDate.isValid())
        filter				<< QString("createdOn>=%1").arg(startDate.toTime_t());
    if (endDate.isValid())
        filter				<< QString("createdOn<=%1").arg(endDate.addDays(1).toTime_t());
    // Filter on status.
    QStringList	statusFilter;
    foreach (quint32 const &s, status)
        statusFilter		<< QString::number(s);
    filter					<< QString("status IN (%1)").arg(statusFilter.join(","));
    setFilter				(filter.join(" AND "));
}

Qt::ItemFlags RSUUI::SlipsModel::flags (const QModelIndex &i) const {
    return QSqlTableModel::flags(i) | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
void RSUUI::SlipsModel::triggerUpdate (QModelIndex const &idx) {
    emit dataChanged(idx, idx);
}

int RSUUI::SlipsModel::columnCount (const QModelIndex&) const {
    return Count;
}
QVariant RSUUI::SlipsModel::data (const QModelIndex &i, int r) const {
    if (!i.isValid())
        return QVariant();

    if (r==Qt::DisplayRole) {
        switch (i.column()) {
            case DocumentCount: {
                QSqlQuery		q	(QString("SELECT COUNT(id) FROM Documents WHERE slip_id=%1").arg(index(i.row(), Key).data().toInt()));
                if (!q.next())		return 0;
                return				q.value(0);
            }
            case Total: {
                QSqlQuery		q	(QString("SELECT SUM(extractedAmount) FROM Documents WHERE slip_id=%1").arg(index(i.row(), Key).data().toInt()));
                if (!q.next())		return 0;
                return				q.value(0);
            }
        }
    }
    else if (r==Qt::DecorationRole) {
        if (i.column()==Key) {
            switch ((RSUUI::E::SlipStatus)index(i.row(), Status).data().toInt()) {
                case RSUUI::E::SlipOpen:
                    return QPixmap(":/Resources/Images/Icons/Set/document-edit.gif");
                case RSUUI::E::SlipSent:
                    return QPixmap(":/Resources/Images/Icons/Set/mail-front.gif");
                case RSUUI::E::SlipDisputed:
                    return QPixmap(":/Resources/Images/Icons/Set/exclamation.gif");
                case RSUUI::E::SlipPaid:
                    return QPixmap(":/Resources/Images/Icons/Set/accept.png");
            }
        }
    }
    return QSqlTableModel::data(i, r);
}
