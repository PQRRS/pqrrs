#ifndef RSUUI_DocumentTypesModel_h
#define RSUUI_DocumentTypesModel_h

// Base class.
#include <QtSql/QSqlRelationalTableModel>
// Required stuff.
#include <QtCore/QVariant>

namespace RSUUI {

class DocumentTypesModel : public QSqlRelationalTableModel {
Q_OBJECT
public:
    // Columns indices helpers.
    enum                    { Key = 0, Issuer, RecognitionCode, IsPhysicalSlip, Name, /*|*/ Count };
    // Constructor.
    /**/                    DocumentTypesModel			(QObject *p, QSqlDatabase const &db);
    // Reimplemented methods.
    Qt::ItemFlags           flags						(QModelIndex const&i) const;
    void                    triggerUpdate				(QModelIndex const &idx);
    static int              sampleImagesCount			(QString const &filter);
    static int              trainableSampleImagesCount	(QString const &filter);
    int                     columnCount					(QModelIndex const &i) const;
    QVariant                data						(QModelIndex const &i, int r=Qt::DisplayRole) const;
};

}

#endif
