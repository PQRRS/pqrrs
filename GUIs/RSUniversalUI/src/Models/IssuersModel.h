#ifndef RSUUI_IssuersModel_h
#define RSUUI_IssuersModel_h

// Base class.
#include <QtSql/QSqlTableModel>
// Required stuff.
#include <QtCore/QVariant>

namespace RSUUI {

class IssuersModel : public QSqlTableModel {
Q_OBJECT
public:
    // Columns indices helpers.
    enum                    { Key = 0, Name, /*|*/ Count };
    // Constructor.
    /**/                    IssuersModel				(QObject *p, QSqlDatabase const &db);
    // Reimplemented methods.
    Qt::ItemFlags           flags						(QModelIndex const&i) const;
    void                    triggerUpdate				(QModelIndex const &idx);
    static int              sampleImagesCount			(QString const &filter);
    static int              trainableSampleImagesCount	(QString const &filter);
    int                     columnCount					(QModelIndex const &i) const;
    QVariant                data						(QModelIndex const &i, int r=Qt::DisplayRole) const;
};

}

#endif
