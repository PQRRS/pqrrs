#ifndef RSUUI_SlipsModel_h
#define RSUUI_SlipsModel_h

// Base class.
#include <QtSql/QSqlTableModel>
// Required stuff.
#include <QtCore/QVariant>

namespace RSUUI {

class SlipsModel : public QSqlTableModel {
Q_OBJECT
public:
    // Columns indices helpers.
    enum                    { Key = 0, CreatedOn, Status, Comments, /*|*/ DocumentCount, Total, Count };
    // Constructor.
    /**/                    SlipsModel					(QObject *p, QSqlDatabase const &db);
    // Filtering methods.
    void                    setCriteria					(QDateTime const &startDate, QDateTime const &endDate, QList<quint32> status, bool excludeWorkingSlip);
    // Reimplemented methods.
    Qt::ItemFlags           flags						(QModelIndex const&i) const;
    void                    triggerUpdate				(QModelIndex const &idx);
    static int              sampleImagesCount			(QString const &filter);
    static int              trainableSampleImagesCount	(QString const &filter);
    int                     columnCount					(QModelIndex const &i) const;
    QVariant                data						(QModelIndex const &i, int r=Qt::DisplayRole) const;
};

};

#endif
