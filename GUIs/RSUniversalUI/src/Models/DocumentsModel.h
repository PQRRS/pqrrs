#ifndef RSUUI_DocumentsModel_h
#define RSUUI_DocumentsModel_h

// Base class.
#include <QtSql/QSqlRelationalTableModel>
// Required stuff.
#include <RELibrary/RETypes>
#include <QtCore/QVariant>

namespace RE {
    namespace T {
        struct Document;
    }
}
namespace RSUUI {
    class Core;
}

namespace RSUUI {

class DocumentsModel : public QSqlRelationalTableModel {

Q_OBJECT
public:
    // Columns indices helpers.
    enum					{ Key = 0, DocumentType, Slip, PointOfSale, CreatedOn, ExtractedUUID, ExtractedAmount, ExtractedSerial, ExtractedValidity, Data, /**/ Count };
    // Constructor.
    /**/					DocumentsModel				(Core *c, QSqlDatabase const &db);

    // Filtering methods.
    void					setCriteria					(QDateTime const &startDate, QDateTime const &endDate, quint32 issuerId, QList<quint32> validity, quint32 slipId);
    // Force triggering an update at a given index.
    void					triggerUpdate				(QModelIndex const &idx);

    // Removes a document from the model and deletes it's associated image.
    RE::E::DocumentValidity createWithDocument			(RE::T::Document const *d);
    void					updateWithDocument			(quint32 row, RE::T::Document const *d);
    void                    deleteImageAtRow            (quint32 row);
    void					removeRowAndDeleteImage		(quint32 row);

    // Ease querying...
    quint32					documentCount				(bool excludeSlips=true) const;
    float					total						(bool excludeSlips=true) const;
    QHash<quint32,QString>	distinctIssuers				(bool excludeSlips=true) const;

    // Make an HTML summary of displayed documents.
    QString					htmlSlipTitle        		(bool excludeSlips=true) const;
    QString					htmlSlipDetails     		(bool excludeSlips=true) const;

    // Reimplemented methods.
    Qt::ItemFlags			flags						(QModelIndex const&i) const;
    int						columnCount					(QModelIndex const &i) const;
    QVariant				data						(QModelIndex const &i, int r=Qt::DisplayRole) const;
    quint32                 documentTypeKeyForRow       (quint32 row) const;
    void					moveRowsToSlipWithKey		(quint32 slipId);

private:
    Core					*_core;
};

}

#endif
