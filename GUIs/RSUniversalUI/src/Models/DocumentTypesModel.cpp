#include "DocumentTypesModel.h"

RSUUI::DocumentTypesModel::DocumentTypesModel (QObject *p, QSqlDatabase const &db)
: QSqlRelationalTableModel(p, db) {
    // Setup model table and edition strategy.
    setTable				("DocumentTypes");
    setEditStrategy			(QSqlTableModel::OnManualSubmit);

    // Setup headers.
    setHeaderData			(Key,				Qt::Horizontal, tr("Key"));
    setHeaderData			(Issuer,			Qt::Horizontal, tr("Issuer"));
    setHeaderData			(RecognitionCode,	Qt::Horizontal,	tr("RCode"));
    setHeaderData			(RecognitionCode,	Qt::Horizontal,	tr("Slip?"));
    setHeaderData			(Name,				Qt::Horizontal, tr("Name"));
}

Qt::ItemFlags RSUUI::DocumentTypesModel::flags (const QModelIndex &i) const {
    return QSqlTableModel::flags(i) | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
void RSUUI::DocumentTypesModel::triggerUpdate (QModelIndex const &idx) {
    emit dataChanged(idx, idx);
}

int RSUUI::DocumentTypesModel::columnCount (const QModelIndex&) const {
    return Count;
}
QVariant RSUUI::DocumentTypesModel::data (const QModelIndex &i, int r) const {
    if (!i.isValid())
        return QVariant();
    else
        return QSqlTableModel::data(i, r);
}
