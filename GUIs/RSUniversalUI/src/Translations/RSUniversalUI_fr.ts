<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>DlgDocumentFix</name>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Fix</source>
        <translation>Corriger</translation>
    </message>
    <message>
        <source>Fix Document</source>
        <translation>Corriger un Titre</translation>
    </message>
</context>
<context>
    <name>DlgMain</name>
    <message>
        <source>Start
Scan</source>
        <translation>Démarrer
Scan</translation>
    </message>
    <message>
        <source>Slips
History</source>
        <translation>Journal
Bordereaux</translation>
    </message>
    <message>
        <source>Issuer:</source>
        <translation>Émetteur:</translation>
    </message>
    <message>
        <source>Delete Document</source>
        <translation>Effacer Titre</translation>
    </message>
    <message>
        <source>Fix Document</source>
        <translation>Corriger Titre</translation>
    </message>
    <message>
        <source>Create Slip with Documents</source>
        <translation>Créer Bordereau avec Titres</translation>
    </message>
    <message>
        <source>Start Scanning</source>
        <translation>Démarrer Scan</translation>
    </message>
    <message>
        <source>View Slips History</source>
        <translation>Void Historique des Bordereaux</translation>
    </message>
    <message>
        <source>Range:</source>
        <translation>Plage:</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Initialize
Scanner</source>
        <translation>Initialiser
Scanner</translation>
    </message>
    <message>
        <source>Free
Scanner</source>
        <translation>Libérer
Scanner</translation>
    </message>
    <message>
        <source>Scan</source>
        <translation>Scanner</translation>
    </message>
</context>
<context>
    <name>DlgSettings</name>
    <message>
        <source>Corporate Name</source>
        <translation type="obsolete">Raison Sociale</translation>
    </message>
    <message>
        <source>Corporate Name for Printing</source>
        <translation type="obsolete">Raison Sociale pour Impression</translation>
    </message>
    <message>
        <source>CRT Membership Code</source>
        <translation>Numéro d&apos;Affilié CRT</translation>
    </message>
    <message>
        <source>CRT Payment Mode</source>
        <translation>Mode de Paiement CRT</translation>
    </message>
    <message>
        <source>7 Days</source>
        <translation>7 Jours</translation>
    </message>
    <message>
        <source>7 Days Gathered</source>
        <translation>7 Jours en Centre de Collecte</translation>
    </message>
    <message>
        <source>21 Days</source>
        <translation>21 Jours</translation>
    </message>
    <message>
        <source>21 Days Gathered</source>
        <translation>21 Jours en Centre de Collecte</translation>
    </message>
    <message>
        <source>Express</source>
        <translation>Express</translation>
    </message>
    <message>
        <source>ANCV Membership Code</source>
        <translation>Numéro d&apos;Affilié ANCV</translation>
    </message>
    <message>
        <source>RSUUI Version</source>
        <translation>Version de RSUUI</translation>
    </message>
    <message>
        <source>SCLibrary Version</source>
        <translation>Version de SCLibrary</translation>
    </message>
    <message>
        <source>RELibrary Version</source>
        <translation>Version de RELibrary</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Software</source>
        <translation>Logiciel</translation>
    </message>
    <message>
        <source>ANCV</source>
        <translation>ANCV</translation>
    </message>
    <message>
        <source>CRT</source>
        <translation>CRT</translation>
    </message>
    <message>
        <source>Company</source>
        <translation>Société</translation>
    </message>
    <message>
        <source>Automatic
Filling</source>
        <translation type="obsolete">Remplissage
Automatique</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <source>Registration Number</source>
        <translation>Numéro de SIRET</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Versions</source>
        <translation>Versions</translation>
    </message>
    <message>
        <source>Front Endorsement</source>
        <translation>Endossement Recto</translation>
    </message>
    <message>
        <source>License Key</source>
        <translation type="obsolete">Clef de License</translation>
    </message>
</context>
<context>
    <name>DlgSlips</name>
    <message>
        <source>From:</source>
        <translation>De:</translation>
    </message>
    <message>
        <source>To:</source>
        <translation>A:</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ouvert</translation>
    </message>
    <message>
        <source>Sent</source>
        <translation>Envoyé</translation>
    </message>
    <message>
        <source>Disputed</source>
        <translation>Litigieux</translation>
    </message>
    <message>
        <source>Paid</source>
        <translation>Payé</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Declare Sent</source>
        <translation>Déclarer Envoyé</translation>
    </message>
    <message>
        <source>Declare Disputed</source>
        <translation>Déclarer Litige</translation>
    </message>
    <message>
        <source>Declare Paid</source>
        <translation>Déclarer Payé</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Slips</source>
        <translation>Bordereaux</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Licensing Issue</source>
        <translation>Problème de License</translation>
    </message>
</context>
<context>
    <name>RSUUI::Core</name>
    <message>
        <source>System Tray Availability</source>
        <translation>Disponibilité Zone de Notification</translation>
    </message>
    <message>
        <source>Management Window</source>
        <translation type="obsolete">Fenêtre de Gestion</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <source>Today</source>
        <translation>Aujourd&apos;hui</translation>
    </message>
    <message>
        <source>Yesterday</source>
        <translation>Hier</translation>
    </message>
    <message>
        <source>Sent on %1</source>
        <translation>Envoyé le %1</translation>
    </message>
    <message>
        <source>Disputed on %1</source>
        <translation>Litigieux le %1</translation>
    </message>
    <message>
        <source>Paid on %1</source>
        <translation>Payé le %1</translation>
    </message>
    <message>
        <source>A system tray cannot be found for this operating system right now. It may become available later, in which case RSUniversal UI will populate it with an icon.</source>
        <translation>Aucune zone de Notification n&apos;a pu etre trouvee sur ce systeme. Elle peut toutefois devenir disponible plus tard, au quel cas RSUniversal UI  s&apos;y placera.</translation>
    </message>
    <message>
        <source>PQR RSUniversal UI</source>
        <translation>PQR RSUniversal UI</translation>
    </message>
    <message>
        <source>PQR RSUniversal UI is now running. Double-click this icon to show the main window.</source>
        <translation>PQR RSUniversal UI est maintenant en cours d&apos;execution. Double-cliquez-cet icone pour voir la fenetre principale.</translation>
    </message>
    <message>
        <source>
Deleted documents on %1 (%2)</source>
        <translation>Suppression titres le %1 (%2)</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Scan</source>
        <translation>Scanner</translation>
    </message>
    <message>
        <source>Database Selection</source>
        <translation>Sélection de la Base de Donnée</translation>
    </message>
    <message>
        <source>You have several databases. Which one would you like to open?</source>
        <translation>Vous disposez de multiples bases de données. Laquelle souhaitez-vous utiliser?</translation>
    </message>
    <message>
        <source>Sent on %1 despite an integrity problem.</source>
        <translation>Envoyé le %1 malgré un problème d&apos;intégrité.</translation>
    </message>
    <message>
        <source>Unlicensed Version</source>
        <translation>Version sans License</translation>
    </message>
    <message>
        <source>Firstfr.PierreQR.RSUniversalUISecond%1Third%2English</source>
        <translation type="obsolete">Firstfr.PierreQR.RSUniversalUISecond%1Third%2French</translation>
    </message>
    <message>
        <source>%1-%2-%3-%4</source>
        <translation type="obsolete">%1-%2-%3-%4</translation>
    </message>
    <message>
        <source>Licensing Issue</source>
        <translation>Problème de License</translation>
    </message>
    <message>
        <source>There was an internal problem due to an invalid license key. Please ensure your license key has been entered correctly.</source>
        <translation>Un probleme interne s&apos;est produit suite a une clef de license invalide. Merci de vérifier que votre clef de license ait été saisie correctement.</translation>
    </message>
    <message>
        <source>%1%2</source>
        <translation type="obsolete">%1%2</translation>
    </message>
    <message>
        <source>Firstfr.PierreQR.RSUniversalUISecond%1Third%2</source>
        <translation type="obsolete">Firstfr.PierreQR.RSUniversalUISecond%1Third%2</translation>
    </message>
</context>
<context>
    <name>RSUUI::DlgMain</name>
    <message>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>One or more documents are not valid. Please either delete or correct the invalid documents and try again.</source>
        <translation>Un ou plusieurs titres sont invalides. Merci de bien vouloir les supprimer ou les corriger puis ré-essayez.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>An unknown error occured while checking for filtered documents issuers.</source>
        <translation type="obsolete">Une erreur inconnue est survenue lors de la vérification des titres filtrés.</translation>
    </message>
    <message>
        <source>Filtered documents seem to be from multiple issuers. Please refine the filters until the resulting documents belong to only one issuer.</source>
        <translation>Il semble que les titres filtrés soient issus de plusieurs émetteurs. Merci d&apos;affiner vos critères jusqu&apos;à ce que les titres n&apos;appartiennent qu&apos;à un unique émetteur.</translation>
    </message>
    <message>
        <source>Slip Creation</source>
        <translation>Création Bordereau</translation>
    </message>
    <message>
        <source>You are about to create a new slip from all the filtered documents. Once done, it will be impossible to add any other documents to the created slip.
Would you like to continue?</source>
        <translation>Vous êtes sur le point de créer un bordereau à partir des titres filtrés. Une fois cela fait, il vous serrât impossible d&apos;y ajouter d&apos;autres titres. Voulez continuer?</translation>
    </message>
    <message>
        <source>No document in selection</source>
        <translation>Aucun titre dans la séléction</translation>
    </message>
    <message>
        <source>Multiple documents in selection</source>
        <translation>Plusieurs titres dans la sélection</translation>
    </message>
    <message>
        <source>An unknown error occured while checking for filtered documents issuers: %1</source>
        <translation>Une erreur inconnue est survenue durant la vérification des émétteurs: %1</translation>
    </message>
</context>
<context>
    <name>RSUUI::DlgSettings</name>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
</context>
<context>
    <name>RSUUI::DlgSlips</name>
    <message>
        <source>Slip Modification</source>
        <translation>Modification Bordereau</translation>
    </message>
    <message>
        <source>Once a slip is declared as sent, it is impossible to delete it or to change any of it&apos;s content. Do you want to continue?</source>
        <translation type="obsolete">Une fois un bordereau déclaré tel qu&apos;envoyé, il devient impossible de l&apos;effacer, ou d&apos;en modifier son contenu. Voulez-vous continuer?</translation>
    </message>
    <message>
        <source>Declaring a dispute on a slip will freeze the sleep until further information can be added. Do you want to continue?</source>
        <translation>Déclarer un litige entraînera le gel du bordereau jusqu&apos;à la résolution du litige. Voulez-vous continuer?</translation>
    </message>
    <message>
        <source>Slip Deletion</source>
        <translation>Suppression Bordereau</translation>
    </message>
    <message>
        <source>Once deleted, a slip and the document and images it may contain will be forever lost. Do you want to proceed with deletion?</source>
        <translation type="obsolete">Une fois supprimé, ,un bordereau ainsi que les documents et images qu&apos;il contient sont perdus à tout jamais. Voulez-vous continuer?</translation>
    </message>
    <message>
        <source>Document Deletion</source>
        <translation>Suppression Document</translation>
    </message>
    <message>
        <source>Do you really want to delete this document as well as any pictures attached to it?</source>
        <translation>Voulez-vous réellement supprimer le document ainsi que toute image lui étant attachée?</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>Yes, but save picures</source>
        <translation>Oui, mais sauvegarder les images</translation>
    </message>
    <message>
        <source>Document Pictures Backup Folder</source>
        <translation>Dossier de Sauvegarde des Images des Titres</translation>
    </message>
    <message>
        <source>Once declared as paid, a slip is forever closed, and any document picture gets deleted. Doing so will also avoid you from declaring a dispute later for this slip. Do you want to continue?</source>
        <translation type="obsolete">Une fois payé, un bordereau est fermé pour toujours, et toute image de titre est effacée, vous empêchera aussi de déclarer un futur litige pour ce bordereau. Voulez-vous continuer?</translation>
    </message>
    <message>
        <source>Slip Integrity Check</source>
        <translation type="obsolete">Verification d&apos;Intégrité du Bordereau</translation>
    </message>
    <message>
        <source>Please insert this slip&apos;s documents in the scanner, picture facing your left hand, and press &quot;Check&quot; once ready.</source>
        <translation type="obsolete">Merci de bien vouloir insérer les titres de ce bordereau dans le scanner, l&apos;image vers votre main gauche, puis appuyez sur &quot;Vérifier&quot;.</translation>
    </message>
    <message>
        <source>If there are documents remaining for this slip&apos;s check, please insert them in the scanner, picture facing your left hand, and press &quot;Check&quot; once ready. Otherwise, press &quot;Finish&quot;.</source>
        <translation type="obsolete">S&apos;il vous reste des titres pour la vérification de ce bordereau, merci de bien vouloir les placer dans le scanner, image vers votre main gauche, puis appuyez sur &quot;Vérifier&quot;. Autrement, appuyez sur &quot;Terminer&quot;.</translation>
    </message>
    <message>
        <source>Check</source>
        <translation>Vérifier</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation>Terminer</translation>
    </message>
    <message>
        <source>Slip Integrity Problem</source>
        <translation>Problème d&apos;Intégrité du Bordereau</translation>
    </message>
    <message>
        <source>It appears that the checking cycle has found %1 %2 instead of %3. Do you want to mark this slip as sent despite this integrity problem?</source>
        <translation>Il semble que le cycle de vérification ait trouvé %1 %2 au lieu de %3. Voullez-vous tout de même marquer ce bordereau comme envoyé malgré le ce probleme  d&apos;intégrité?</translation>
    </message>
    <message>
        <source>documents</source>
        <translation>titres</translation>
    </message>
    <message>
        <source>document</source>
        <translation>titre</translation>
    </message>
    <message>
        <source>To be able to send a slip, you first have to check it&apos;s integrity. While doing so, you may endorse the documents as well. What do you want to do?</source>
        <translation type="obsolete">Pour pouvoir envoyer un bordereau, vous devez en vérifier l&apos;intégrité. Ce faisant, vous pouvez aussi endosser les titres. Que voulez-vous faire?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <source>Check and Endorse</source>
        <translation type="obsolete">Vérifier et Endosser</translation>
    </message>
    <message>
        <source>Check, Endorse then Mark as Sent (Recommended)</source>
        <translation>Vérifier, Endosser puis marquer comme Envoyé (Recommandé)</translation>
    </message>
    <message>
        <source>Check then Mark as Sent (No endorsement will be performed)</source>
        <translation>Vérifier puis marquer comme Envoyé (Aucun endossement ne sera efféctué)</translation>
    </message>
    <message>
        <source>Mark as Sent (No verification will be performed at all)</source>
        <translation>Marquer comme Envoyé (Aucune vérification ni endossement ne sera effectué)</translation>
    </message>
    <message>
        <source>Endorse</source>
        <translation>Endosser</translation>
    </message>
    <message>
        <source>Slip Processing (%1)</source>
        <translation>Traitement de Bordereau (%1)</translation>
    </message>
    <message>
        <source> and </source>
        <translation> et </translation>
    </message>
    <message>
        <source>Please insert this slip&apos;s documents in the scanner, picture facing your left hand, and press &quot;Ok&quot; once ready.</source>
        <translation>Merci de bien vouloir insérer les titres de ce bordereau dans le scanner, l&apos;image vers votre main gauche, puis appuyez sur &quot;Ok&quot;.</translation>
    </message>
    <message>
        <source>If there are documents remaining for this slip, please insert them in the scanner, picture facing your left hand, and press &quot;Go&quot; once ready. Otherwise, press &quot;Finish&quot;.</source>
        <translation>S&apos;il vous reste des titres pour ce bordereau, merci de bien vouloir les placer dans le scanner, image vers votre main gauche, puis appuyez sur &quot;Go&quot;. Autrement, appuyez sur &quot;Terminer&quot;.</translation>
    </message>
    <message>
        <source>Go</source>
        <translation>Go</translation>
    </message>
    <message>
        <source>You have choosen to mark this slip as sent without performing any check and without endorsing the documents. Be aware that the gathering organism may refuse the documents as they will not be endorsed, and that the slip declared contents may differ from reallity. Are you sure you want to do this?</source>
        <translation>Vous avez choisi de marquer ce bordereau comme envoyé sans en vérifier son contenu, et sans endossement. Rappellez-vous que l&apos;organisme de collecte peut vous refuser les document s&apos;ils ne sont pas endossés, et que le contenu déclaré peut différer du contenu réel pour ce bordereau. Voulez-vous continuer malgré tout?</translation>
    </message>
    <message>
        <source>Would you like to declare this slip as paid?</source>
        <translation>Voulez-vous déclarer ce bordereau comme payé?</translation>
    </message>
    <message>
        <source>Deleting a slip will also delete any document it contains. Would you like to continue?</source>
        <translation>La suppression d&apos;un bordereau entrainera l&apos;effacement des titres qu&apos;il peut contenir. Voulez-vous continuer?</translation>
    </message>
    <message>
        <source>Export as PDF</source>
        <translation>Exporter en PDF</translation>
    </message>
    <message>
        <source>Export as Text</source>
        <translation>Exporter en Texte</translation>
    </message>
    <message>
        <source>Slip Export Path</source>
        <translation>Chemin de l&apos;Export du Bordereau</translation>
    </message>
    <message>
        <source>Portable Document Format (*.pdf)</source>
        <translation>Portable Document Format (*.pdf)</translation>
    </message>
</context>
<context>
    <name>RSUUI::DocumentTypesModel</name>
    <message>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <source>Issuer</source>
        <translation>Émetteur</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>RCode</source>
        <translation>RCode</translation>
    </message>
    <message>
        <source>Slip?</source>
        <translation>Bordereau?</translation>
    </message>
</context>
<context>
    <name>RSUUI::DocumentUtility</name>
    <message>
        <source>This document has no amount.</source>
        <translation>Ce titre ne comporte pas de montant.</translation>
    </message>
    <message>
        <source>This document has no serial number.</source>
        <translation>Ce titre n&apos;a pas d&apos;identifiant.</translation>
    </message>
    <message>
        <source>The serial is valid.</source>
        <translation>L&apos;identifiant est valide.</translation>
    </message>
    <message>
        <source>The serial shows an outdated value.</source>
        <translation>L&apos;identifiant dénote un titre périmé.</translation>
    </message>
    <message>
        <source>This document has already been registered as %1 in %2, created on %3.</source>
        <translation>Titre déja en base comme %1 dans %2, créé le %3.</translation>
    </message>
    <message>
        <source>The issuer code does not match.</source>
        <translation>Le code émetteur ne correspond pas.</translation>
    </message>
    <message>
        <source>A serial has been found, but it is not properly formatted.</source>
        <translation>Un identifiant a été trouvé, mais son formattage est incorrect.</translation>
    </message>
    <message>
        <source>A serial has been found, but it seems corrupted.</source>
        <translation>Un identifiant a été trouvé, mais il semble corrompu.</translation>
    </message>
    <message>
        <source>A serial has been found, but its checksum is incorrect.</source>
        <translation>Un identifiant a été trouvé, mais sa clé de contrôle est incorrecte.</translation>
    </message>
    <message>
        <source>This document is a physical slip.</source>
        <translation>Ce titre est un bordereau.</translation>
    </message>
</context>
<context>
    <name>RSUUI::DocumentsModel</name>
    <message>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Slip</source>
        <translation>Bordereau</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <source>Serial</source>
        <translation>Identifiant</translation>
    </message>
    <message>
        <source>Valid.</source>
        <translation>Valid.</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Documents</source>
        <translation>Titres</translation>
    </message>
    <message>
        <source>POS</source>
        <translation>PdV</translation>
    </message>
    <message>
        <source>&lt;li&gt;%1 %2&lt;span class=&apos;amount&apos;&gt;%3&lt;/span&gt;&lt;/li&gt;</source>
        <translation type="obsolete">&lt;li&gt;%1 %2&lt;span class=&apos;amount&apos;&gt;%3&lt;/span&gt;&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;%1 %2&lt;span class=&apos;amount&apos;&gt;%3&lt;/span&gt;&lt;/h3&gt;&lt;ul&gt;%4&lt;/ul&gt;</source>
        <translation type="obsolete">&lt;h3&gt;%1 %2&lt;span class=&apos;amount&apos;&gt;%3&lt;/span&gt;&lt;/h3&gt;&lt;ul&gt;%4&lt;/ul&gt;</translation>
    </message>
    <message>
        <source>&lt;h2&gt;%1 %2&lt;span class=&apos;amount&apos;&gt;%3&lt;/span&gt;&lt;/h2&gt;%4</source>
        <translation type="obsolete">&lt;h2&gt;%1 %2&lt;span class=&apos;amount&apos;&gt;%3&lt;/span&gt;&lt;/h2&gt;%4</translation>
    </message>
    <message>
        <source>Amnt.</source>
        <translation>Mont.</translation>
    </message>
</context>
<context>
    <name>RSUUI::IssuersModel</name>
    <message>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>RSUUI::SettingsModel</name>
    <message>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>RSUUI::SlipsModel</name>
    <message>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <source>Docs.</source>
        <translation>Tit.</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>Total</translation>
    </message>
</context>
</TS>
