#ifndef RSUUI_ThreadedWorker_h
#define RSUUI_ThreadedWorker_h

// Base class.
#include <QtCore/QThread>
// Required stuff.
#include <QtCore/QFile>
#include <QtCore/QSemaphore>
#include <QtGui/QImage>

namespace RSUUI {

class ThreadedWorker : public QThread {
Q_OBJECT

public:
    // Constructors.
    /**/				ThreadedWorker      	(QObject *p=0) : QThread(p), _mutex(INT_MAX) {}
    // Queue operators.
    void				scheduleFileDeletion    (QString fn) {
        _mutex.acquire();
        emit fileDeletionScheduled(fn);
    }
    void                scheduleImageStorage    (QImage img, QString uuid) {
        if (img.isNull() || uuid.isEmpty())
            return;
        _mutex.acquire();
        emit imageStorageScheduled(img, uuid);
    }
    // Status getter.
    bool				hasPendingTasks 		() const { return _mutex.available()!=INT_MAX; }

protected:
    // Main run loop, actually just polls signal to slot.
    void				run						() {
        connect             (this, SIGNAL(fileDeletionScheduled(QString)), this, SLOT(deleteFile(QString)), Qt::QueuedConnection);
        connect             (this, SIGNAL(imageStorageScheduled(QImage, QString)), this, SLOT(storeImage(QImage, QString)), Qt::QueuedConnection);
        exec                ();
    }
protected slots:
    void                deleteFile              (QString fn) {
        QFile::remove       (fn);
        _mutex.release      ();
    }
    void                storeImage              (QImage img, QString uuid) {
        img.scaledToHeight  (210, Qt::SmoothTransformation).save(QString("Images/%1.jpg").arg(uuid), "JPEG", 35);
        _mutex.release      ();
    }

private:
    QSemaphore          _mutex;

signals:
    void                fileDeletionScheduled   (QString);
    void                imageStorageScheduled   (QImage, QString);
};

}

#endif
