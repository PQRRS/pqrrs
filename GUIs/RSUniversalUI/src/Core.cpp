#include "Core.h"
// Licensing stuff.
#include <LMLibrary/LMTypes>
#include <LMLibrary/Core>
// SC / RE Stuff.
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <SCLibrary/SCTypes>
#include <SCLibrary/SCLibrary>
#include <SCLibrary/Controllers/AScannerController>
// Plugins management.
#include "Plugins/AbstractPlugin.h"
#include "Plugins/PluginLoader.h"
#include "Plugins/PluginsManager.h"
// Various classes.
#include "Utilities/DatabaseManager.h"
#include "Utilities/DocumentUtility.h"
#include "Utilities/ThemeManager.h"
#include "Workers/ThreadedWorker.h"
// Models.
#include "Models/SettingsModel.h"
#include "Models/IssuersModel.h"
#include "Models/DocumentTypesModel.h"
#include "Models/SlipsModel.h"
#include "Models/DocumentsModel.h"
// UI Classes.
#include "UI/Dialogs/DlgMain.h"
#include "UI/Dialogs/DlgSettings.h"
#include "UI/Dialogs/DlgSlips.h"
// Qt stuff.
#include <QtCore/QFile>
#include <QtCore/QMutexLocker>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QCryptographicHash>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#include <QtGui/QMenu>
#include <QtGui/QAction>
#include <QtGui/QMessageBox>
#include <QtGui/QApplication>
#include <QtGui/QInputDialog>
#include <QtGui/QApplication>
// Other.
#include <time.h>

RSUUI::Core*	RSUUI::Core::_Instance				= 0;

class MSleepHelper : public QThread { public: static void msleep(int ms) { QThread::msleep(ms); } };

RSUUI::Core::Core (QObject *p)
: QObject(p), _lm(new LM::Core(this)), _pluginsManager(0), _re(0),
_settings(0), _issuers(0), _documentTypes(0), _slips(0), _documents(0),
_threadedWorker(0), _dbManager(0), _docUtility(0), _themeManager(0),
_systrayIcon(0), _mnuSystray(0), _actSystrayMainWin(0), _actSystraySettingsWin(0),
_dlgMain(0), _dlgSettings(0), _dlgSlips(0) {
    _lm                                 = new LM::Core(this);
    _lm->loadFromFile                   (QString("%1/.PQRRSLicense.lic").arg(QDir::homePath()).toUtf8().constData());
    if (!checkLicense())
        return;
    _Instance							= this;
    // Make sure recognition file exists.
    //if (!QFile("./RSUniversalUI.recb").exists())
    //    QFile(":/Resources/Required/RSUniversalUI.recb").copy("./RSUniversalUI.recb");
    // Read configuration file.
    //if (!QFile("./RSUniversalUI.scconf").exists())
    //    QFile(":/Resources/Required/RSUniversalUI.scconf").copy("./RSUniversalUI.scconf");

    // Create SCLibrary configuration objects.
    _scHlpr.deviceConfig				= new SC::DeviceConfig;
    _scHlpr.config						= new SC::Config;
    _scHlpr.mutex						= new QMutex();
    // Read configuration file.
    QFile			fConf				(":/RSUniversalUI.scconf");
    QDataStream		sConf				(&fConf);
    fConf.open							(QIODevice::ReadOnly);
    sConf								>> *_scHlpr.deviceConfig >> *_scHlpr.config;
    fConf.close							();
    // Set up locale.
    SC::deleteAndCopy					(_scHlpr.deviceConfig->locale, QLocale().name());
    SC::deleteAndCopy					(_scHlpr.config->recognitionDataPath, ":/RSUniversalUI.recb");
    // Set up callbacks.
    _scHlpr.config->userObject			= this;
    _scHlpr.config->onProcessingStarted			= (SC::Config::ProcessingStartedCallback)&staticCbProcessingStarted;
    _scHlpr.config->onProcessingFinished		= (SC::Config::ProcessingFinishedCallback)&staticCbProcessingFinished;
    _scHlpr.config->onNewDocument				= (SC::Config::NewDocumentCallback)&staticCbNewDocument;
    _scHlpr.config->onDocumentRecognitionDone	= (SC::Config::DocumentRecognitionDoneCallback)&staticCbDocumentRecognitionDone;

    // Instanciate the recognition engine.
    _re									= new RE::Engine(_lm, this);
    _re->loadClientBinaryData			(_scHlpr.config->recognitionDataPath);

    // System tray available on this system
    if (!QSystemTrayIcon::isSystemTrayAvailable())
        QMessageBox::warning((QWidget*)QApplication::desktop(), tr("System Tray Availability"), tr("A system tray cannot be found for this operating system right now. It may become available later, in which case RSUniversal UI will populate it with an icon."));

    // Make tray menu.
    _mnuSystray							= new QMenu((QWidget*)QApplication::desktop());
    // Prepare main window menu action.
    _actSystrayMainWin					= _mnuSystray->addAction(QIcon(":/Resources/Images/Icons/Set/printer"), tr("Scan"), this, SLOT(toggleMainWindow()));
    _actSystrayMainWin->setCheckable	(true);
    // Separator.
    _mnuSystray->addSeparator			();
    // Settings window.
    _actSystraySettingsWin				= _mnuSystray->addAction(QIcon(":/Resources/Images/Icons/Set/clipboard"), tr("Settings"), this, SLOT(toggleSettingsWindow()));
    _actSystraySettingsWin->setCheckable(true);
    // Finally add quit action.
    _mnuSystray->addAction				(QIcon(":/Resources/Images/Icons/Set/icon_cancel"), tr("Quit"), this, SLOT(quitActionTriggered()));

    // Make tray icon.
    _systrayIcon						= new QSystemTrayIcon(this);
    connect								(_systrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    _systrayIcon->setIcon				(QIcon(":/Resources/Images/Icons/Systray.ico"));
    _systrayIcon->setContextMenu		(_mnuSystray);
    _systrayIcon->show					();
    // Display initial running message.
    _systrayIcon->showMessage			(tr("PQR RSUniversal UI"),
                                            tr("PQR RSUniversal UI is now running. Double-click this icon to show the main window."),
                                            QSystemTrayIcon::Information,
                                            100);

    // Check wether creating the database is required or not.
    QDir::current().mkdir               ("Databases");
    QDir::current().mkdir               ("Images");

    // Various classes.
    _threadedWorker                     = new ThreadedWorker    ();
    _dbManager                          = new DatabaseManager   (this);
    _docUtility							= new DocumentUtility   (this);
    _themeManager                       = new ThemeManager      (this);

    QStringList const       dbs         = _dbManager->availableDatabases();
    quint32 const           lastDbIdx   = dbs.indexOf(_dbManager->currentDatabaseName());
    QString                 dbn         = dbs.count() ? dbs.first() : "Default.rsuuidb";
    if (dbs.count()>1) {
        dbn                             = QInputDialog::getItem(
                                            (QWidget*)QApplication::desktop(),
                                            tr("Database Selection"),
                                            tr("You have several databases. Which one would you like to open?"),
                                            dbs,
                                            lastDbIdx,
                                            false);
    }

    _dbManager->setCurrentDatabaseName  (dbn);

    { // Cache document types names.
        _dtKeysByRCodes.clear           ();
        _dtNameByKey.clear              ();
        _physicalSlipKeys.clear         ();
        QSqlQuery           q;
        q.prepare                       ("SELECT id,recognitionCode,isPhysicalSlip,name FROM DocumentTypes;");
        q.exec                          ();
        while (q.next()) {
            quint32         key         = q.value(0).toInt();
            qint32          rCode       = q.value(1).toInt();
            bool            isPhySlip   = q.value(2).toBool();
            QString         name        = q.value(3).toString();
            _dtKeysByRCodes[rCode]      = key;
            _dtNameByKey[key]           = name;
            if (isPhySlip)
                _physicalSlipKeys       << key;
        }
    }

    // Models.
    _settings                           = new SettingsModel     (this, _dbManager->database());
    _issuers							= new IssuersModel      (this, _dbManager->database());
    _documentTypes						= new DocumentTypesModel(this, _dbManager->database());
    _slips								= new SlipsModel        (this, _dbManager->database());
    _documents							= new DocumentsModel    (this, _dbManager->database());
    // Force models selection.
    _issuers->select 					();
    _documentTypes->select				();
    _slips->select						();
    _documents->select					();

    // Set up global stylesheet.
    QFile					fStyle		(_themeManager->stylesheetPathForName("Application"));
    fStyle.open							(QIODevice::ReadOnly);
    QString					sStyle		= QLatin1String(fStyle.readAll());
    fStyle.close						();
    qApp->setStyleSheet                 (sStyle);

    // Connect licensing problem slot.
    connect(this, SIGNAL(licenseMismatch()), this, SLOT(licenseMismatchDetected()));

    // Make sure plugins are known to the system.
    _pluginsManager							= new PluginsManager(qApp->applicationDirPath(), this);
    parsePlugins							();

    // Finish startup.
    _threadedWorker->start              ();
    toggleMainWindow					();
}

RSUUI::Core::~Core () {
    // Wait for the threaded worker to exit.
    while (_threadedWorker->hasPendingTasks())
        MSleepHelper::msleep    (50);
    _threadedWorker->terminate  ();
    // Release scanner controller.
    changeScannerState			(RSUUI::E::ScannerStateFree);
    delete						_scHlpr.deviceConfig;
    delete						_scHlpr.config;
    // Delete RELibrary objects as well.
    if (_re)
        delete					_re;
    // Get rid of systray.
    _systrayIcon->hide			();
    delete						_systrayIcon;
    // Delete every and each model...
    delete						_documents;
    delete						_slips;
    delete						_documentTypes;
    delete						_issuers;
    // Delete helper classes.
    delete                      _threadedWorker;
    delete                      _dbManager;
    delete                      _docUtility;
    delete                      _themeManager;
    // Delete plugins manager.
    delete						_pluginsManager;
}

// Plugins management.
RSUUI::PluginBaseInfos::List RSUUI::Core::loadedPlugins () const {
    return _pluginsManager->loadedPlugins();
}

QString RSUUI::Core::frontPrintString () const {
    // Check license...
    return checkLicense()
            ? QString("%1 %2 %3")
                .arg(_settings->valueForKey("Preferences::CrtMembershipCode"))
                .arg(_settings->valueForKey("Preferences::CorpName"))
                .arg(QDate::currentDate().toString(Qt::DefaultLocaleShortDate))
            : tr("Unlicensed Version");
}
QString RSUUI::Core::rearPrintString () const {
    return checkLicense()
            ? QString("%1 - %2\n%3 - %4")
                .arg(_settings->valueForKey("Preferences::CorpName"))
                .arg(_settings->valueForKey("Preferences::CorpAddress"))
                .arg(_settings->valueForKey("Preferences::CorpPhone"))
                .arg(_settings->valueForKey("Preferences::CorpRegNumber"))
            : tr("Unlicensed Version");
}

// Static SCLibrary callbacks.
quint32 SCCBCConv RSUUI::Core::staticCbProcessingStarted (RSUUI::Core *_this) {
    return							_this->cbProcessingStarted();
}
quint32 SCCBCConv RSUUI::Core::staticCbProcessingFinished (RSUUI::Core *_this) {
    return							_this->cbProcessingFinished();
}
quint32 RSUUI::Core::staticCbNewDocument (RSUUI::Core *_this, quint32, quint32, char const*, RE::T::Document *doc) {
    return							_this->cbNewDocument(doc);
}
quint32 SCCBCConv RSUUI::Core::staticCbDocumentRecognitionDone (RSUUI::Core *_this, quint32, quint32, char const*, RE::T::Document *doc) {
    return							_this->cbDocumentRecognitionDone(doc);
}
// Regular SCLibrary callbacks.
quint32 RSUUI::Core::cbProcessingStarted () {
    return							0;
}
quint32 RSUUI::Core::cbProcessingFinished () {
    // Submit to database.
    _scHlpr.mutex->unlock			();
    _documents->submitAll           ();
    emit		scanningFinished		(_scHlpr);
    return							0;
}
quint32 RSUUI::Core::cbNewDocument (RE::T::Document*) {
    emit		documentScanned		();
    return							2;
}
quint32 RSUUI::Core::cbDocumentRecognitionDone (RE::T::Document *doc) {
    if (_scHlpr.options & E::ScanOptionAnalysis) {
        // Async saving of the image.
        if (doc->images.contains("Front"))
            _threadedWorker->scheduleImageStorage
                                        (doc->images["Front"], doc->strings["UUID"]);
        // Store document, check for it's validity.
        RE::E::DocumentValidity
                            val         = _documents->createWithDocument(doc);
        return val==RE::E::DocumentValidityValid ? 2 : 0;
    }
    return 2;
}

void RSUUI::Core::prepareSkin (QWidget &wgt) {
    if (1) {
        // Update window flags.
        wgt.setWindowFlags				(Qt::Widget|Qt::FramelessWindowHint);
    }
    else {
        // Update window flags.
        wgt.setWindowFlags				(Qt::Widget|Qt::FramelessWindowHint|Qt::X11BypassWindowManagerHint);
        // Make translucent background.
        QPalette			pal;
        pal.setColor					(QPalette::Base, Qt::transparent);
        wgt.setPalette					(pal);
        wgt.setAttribute				(Qt::WA_TranslucentBackground);
    }
    // Apply stylesheet.
    QFile					fStyle		(_themeManager->stylesheetPathForName(wgt.objectName()));
    fStyle.open							(QIODevice::ReadOnly);
    QString					sStyle		= QLatin1String(fStyle.readAll());
    fStyle.close						();
    wgt.setStyleSheet					(sStyle);
}
QString RSUUI::Core::humanReadableDateTime (QDateTime const &dt) {
    QDate const				&today		= QDate::currentDate();
    QDate const				&d			= dt.date();
    return d==today ? tr("Today") : (d==today.addDays(-1) ? tr("Yesterday") : d.toString(Qt::DefaultLocaleShortDate));
}
// License checker.
bool RSUUI::Core::checkLicense () const {
    if (_lm->isVerified() && LM::Core::LMObfuscatedCheesecakeForData(_lm, RSUUI::SoftwareIdentifier, LM::Core::hostUuid(), 0))
        return                  true;
    QMessageBox         *d   = new QMessageBox(QMessageBox::Critical, QObject::tr("Licensing Issue"), _lm->lastMessage(), QMessageBox::Ok);
    QObject::connect        (d, SIGNAL(buttonClicked(QAbstractButton*)), QApplication::instance(), SLOT(quit()));
    d->show                 ();
    emit                    licenseMismatch();
    return                  false;
}

// Plugins stuff.
RSUUI::PluginsManager& RSUUI::Core::pluginsManager () {
    return				*_pluginsManager;
}
void RSUUI::Core::parsePlugins () {
    Dbg											<< "Loading plugins...";
    // Cache available plugins list.
    foreach (PluginBaseInfos const &bi, _pluginsManager->loadedPlugins()) {
        // Try to get a handle on the plugin's root object.
        AbstractPlugin 		*plg				= _pluginsManager->rootInstance(bi);
        // Handle failure...
        if (!plg) {
            Crt									<< "Plugin" << bi.name << "appears to be loaded but is not providing a root instance!";
            continue;
        }
    }
}


// Single-application message handler.
void RSUUI::Core::singleApplicationMessage (QString const &msg) {
    if (msg=="WakeUp" && (!_dlgMain || !_dlgMain->isVisible()))
        toggleMainWindow			();
}

// System tray related slots.
void RSUUI::Core::iconActivated (QSystemTrayIcon::ActivationReason r) {
    switch (r) {
        case QSystemTrayIcon::DoubleClick:
            toggleMainWindow();
            break;
        case QSystemTrayIcon::Context:
        case QSystemTrayIcon::Trigger:
        case QSystemTrayIcon::MiddleClick:
        default:
            break;
    }
}
void RSUUI::Core::toggleMainWindow () {
    if (!_dlgMain) {
        _dlgMain						= new DlgMain();
        // Core -> DlgMain actions.
        connect(this, SIGNAL(scannerStateChanged(RSUUI::T::ScannerHelper const&)), _dlgMain, SLOT(scannerStateChanged(RSUUI::T::ScannerHelper const&)));
        connect(this, SIGNAL(scanningStarted(RSUUI::T::ScannerHelper const&)), _dlgMain, SLOT(scanningStarted(RSUUI::T::ScannerHelper const&)));
        connect(this, SIGNAL(scanningFinished(RSUUI::T::ScannerHelper const&)), _dlgMain, SLOT(scanningFinished(RSUUI::T::ScannerHelper const&)));
        connect(this, SIGNAL(documentScanned()), _dlgMain, SLOT(documentScanned()));
        // DlgMain -> Core actions.
        connect(_dlgMain, SIGNAL(visibilityChanged(bool)), _actSystrayMainWin, SLOT(setChecked(bool)));
        connect(_dlgMain, SIGNAL(initializeScanner()), this, SLOT(initializeScanner()));
        connect(_dlgMain, SIGNAL(freeScanner()), this, SLOT(freeScanner()));
        connect(_dlgMain, SIGNAL(startScanning(RSUUI::E::ScanOptions)), this, SLOT(startScanning(RSUUI::E::ScanOptions)));
        connect(_dlgMain, SIGNAL(deleteDocuments(QModelIndexList)), this, SLOT(deleteDocuments(QModelIndexList)));
        connect(_dlgMain, SIGNAL(createSlipWithDocuments()), this, SLOT(createSlipWithDocuments()));
        connect(_dlgMain, SIGNAL(showSlipsHistory()), this, SLOT(presentSlipsDialog()));
    }
    _dlgMain->setVisible				(!_dlgMain->isVisible());
}
void RSUUI::Core::quitActionTriggered () {
    QApplication::quit					();
}

// Scanner controller slots.
void RSUUI::Core::ensureScannerController () {
    if (_scHlpr.controller)
        return;
    _scHlpr.controller					= (SC::AScannerController*)SCInstanciate(QString("%1/.PQRRSLicense.lic").arg(QDir::homePath()).toUtf8().constData(), _scHlpr.deviceConfig);
    _scHlpr.controller->setRecognitionEngine	(recognitionEngine());
}
void RSUUI::Core::initializeScanner () {
    _scHlpr.state						= RSUUI::E::ScannerStateBusy;
    emit								scannerStateChanged(_scHlpr);
    ensureScannerController				();
    QtConcurrent::run<void>             (this, &RSUUI::Core::changeScannerState, RSUUI::E::ScannerStateIdle);
}
void RSUUI::Core::freeScanner () {
    _scHlpr.state						= RSUUI::E::ScannerStateBusy;
    emit								scannerStateChanged(_scHlpr);
    QtConcurrent::run<void>             (this, &RSUUI::Core::changeScannerState, RSUUI::E::ScannerStateFree);
}
void RSUUI::Core::changeScannerState (RSUUI::E::ScannerState ss) {
    // Requested state is ready.
    if (ss==RSUUI::E::ScannerStateIdle) {
        // Make sure a scanner controller exists.
        ensureScannerController				();
        // Try to init scanner.
        if (_scHlpr.controller->initialize()) {
            _scHlpr.state                   = RSUUI::E::ScannerStateIdle;
            // Check license...
            if (!checkLicense())
                return changeScannerState   (RSUUI::E::ScannerStateFree);
        }
        // Scanner init failed.
        else
            return changeScannerState       (RSUUI::E::ScannerStateFree);
    }
    // Requested state is free.
    else if (ss==RSUUI::E::ScannerStateFree && _scHlpr.controller) {
        _scHlpr.controller->stopProcessing  ();
        while (_scHlpr.controller->isRunning()) {
            _scHlpr.controller->exit        ();
            _scHlpr.controller->terminate   ();
            MSleepHelper::msleep            (50);
        }
        delete								_scHlpr.controller;
        _scHlpr.controller					= 0;
        _scHlpr.state						= E::ScannerStateFree;
    }
    // Nothing to be done.
    else
        return;
    emit scannerStateChanged                (_scHlpr);
}

void RSUUI::Core::startScanning (RSUUI::E::ScanOptions opts) {
    // Scanner already busy.
    if (!_scHlpr.mutex->tryLock())
        return;
    else if ((opts & E::ScanOptionAnalysis) &&
             _settings->valueForKey("Preferences::FrontEndorsement").toUInt())
    opts                                    |= E::ScanOptionEndorseFront;
    // Check license...
    if (!checkLicense())                    return;
    // Notify.
    _scHlpr.options							= opts;
    _scHlpr.config->enableMicr				= (opts & E::ScanOptionAnalysis);
    _scHlpr.config->sides					= (opts & E::ScanOptionAnalysis) ? SC::E::DocumentSideFront : SC::E::DocumentSideNone;
    _scHlpr.config->preferedResolution		= (opts & E::ScanOptionAnalysis) ? 300 : 0;
    _scHlpr.config->enableRejectionTests	= (opts & E::ScanOptionAnalysis);
    _scHlpr.config->enableRecognition		= (opts & E::ScanOptionAnalysis);
    if (opts & E::ScanOptionEndorseFront)
        SC::deleteAndCopy                   (_scHlpr.config->printData, frontPrintString());
    else if (opts & E::ScanOptionEndorseBack)
        SC::deleteAndCopy                   (_scHlpr.config->printData, rearPrintString());
    else
        SC::deleteAndCopy                   (_scHlpr.config->printData, 0);
    // Finally configure scanner controller.
    ensureScannerController					();
    if (_scHlpr.controller->configure(*_scHlpr.config) &&
            _scHlpr.controller->startProcessing())
        emit scanningStarted(_scHlpr);
    else
        emit scanningFinished(_scHlpr);
}

// Documents / Slips handling slots.
void RSUUI::Core::deleteDocuments (QModelIndexList const &idxs, QModelIndex const &slipIdx) {
    // A parent slip has been specified.
    if (slipIdx.isValid()) {
        QModelIndex const	&comIdx			= _slips->index(slipIdx.row(), SlipsModel::Comments);
        QString				comments		= _slips->data(comIdx).toString();
        QStringList			serials;
        foreach (QModelIndex const &idx, idxs)
            serials							<< _documents->index(idx.row(), DocumentsModel::ExtractedSerial).data().toString();
        comments							+= QString(tr("\nDeleted documents on %1 (%2)"))
                                                .arg(QDate::currentDate().toString(Qt::DefaultLocaleShortDate))
                                                .arg(serials.join(", "));
        _slips->setData						(comIdx, comments);
    }
    // Delete documents.
    foreach (QModelIndex const &idx, idxs) {
        QString const		&uuid           = _documents->index(idx.row(), DocumentsModel::ExtractedUUID).data().toString();
        _threadedWorker->scheduleFileDeletion
                                            (QString("Images/%1.jpg").arg(uuid));
        _documents->removeRow               (idx.row());
    }
    _documents->submitAll					();
    // If a slip was changed, save it.
    if (slipIdx.isValid())
        _slips->submitAll					();
}
void RSUUI::Core::createSlipWithDocuments () {
    // No document in filter...
    if (!_documents->rowCount())		return;
    // Create empty slip.
    QSqlQuery			q;
    q.prepare							("INSERT INTO Slips (createdOn, status, comments) VALUES (strftime('%s','now'), :status, :comments);");
    q.bindValue							(":status",		0);
    q.bindValue							(":comments",	"");
    q.exec								();
    quint32				slipId			= q.lastInsertId().toUInt();
    // Move documents to empty slip.
    _documents->moveRowsToSlipWithKey	(slipId);
    _documents->submitAll				();
}
void RSUUI::Core::markSlipSent (QModelIndex const &idx, bool integrityOk) {
    E::SlipStatus		cs			= (E::SlipStatus)_slips->index(idx.row(), SlipsModel::Status).data().toInt();
    if (cs!=RSUUI::E::SlipOpen)
        return;
    QString             msg;
    if (integrityOk)
        msg                         = tr("Sent on %1").arg(QDate::currentDate().toString(Qt::DefaultLocaleShortDate));
    else
        msg                         = tr("Sent on %1 despite an integrity problem.").arg(QDate::currentDate().toString(Qt::DefaultLocaleShortDate));
    changeSlipStatus				(idx, E::SlipSent, msg);
}
void RSUUI::Core::markSlipDisputed (QModelIndex const &idx) {
    E::SlipStatus		cs			= (E::SlipStatus)_slips->index(idx.row(), SlipsModel::Status).data().toInt();
    if (cs!=E::SlipSent)
        return;
    changeSlipStatus					(idx, E::SlipDisputed, tr("Disputed on %1").arg(QDate::currentDate().toString(Qt::DefaultLocaleShortDate)));
}
void RSUUI::Core::markSlipPaid (QModelIndex const &idx, QString const &picsPath) {
    E::SlipStatus		cs			= (E::SlipStatus)_slips->index(idx.row(), SlipsModel::Status).data().toInt();
    if (cs!=E::SlipSent && cs!=E::SlipDisputed)
        return;
    // Delete any picture associated to contained documents.
    while (_documents->canFetchMore())
        _documents->fetchMore	();
    for (qint32 docRow=0; docRow<_documents->rowCount(); ++docRow) {
        QString		uuid				= _documents->index(docRow, DocumentsModel::ExtractedUUID).data().toString();
        if (uuid.isEmpty())				continue;
        QFile		pic					(QString("Images/%1.jpg").arg(uuid));
        if (!picsPath.isEmpty())
            pic.copy					(QString("%1/%2.jpg").arg(picsPath).arg(uuid));
        QFile::remove					(pic.fileName());
    }
    changeSlipStatus					(idx, E::SlipPaid, tr("Paid on %1").arg(QDate::currentDate().toString(Qt::DefaultLocaleShortDate)));
}
void RSUUI::Core::changeSlipStatus (QModelIndex const &idx, RSUUI::E::SlipStatus ss, QString const &msg) {
    // Get status and comment indexes.
    QModelIndex const   	&sIdx		= _slips->index(idx.row(), SlipsModel::Status);
    QModelIndex const		&cIdx		= _slips->index(idx.row(), SlipsModel::Comments);
    // Retrieve comment.
    QString					cmt			= _slips->data(cIdx).toString();
    // Comment already has something, go to new line.
    cmt									+= (cmt.isEmpty()?"":"\n") + msg;
    // Update data.
    _slips->setData						(sIdx, ss);
    _slips->setData						(cIdx, cmt);
    _slips->submitAll					();
}
void RSUUI::Core::deleteSlips (QModelIndexList const &idxs) {
    // Make sure each document from the slip is loaded.
    while (_documents->canFetchMore())
        _documents->fetchMore();
    // Delete each document from this slip.
    for (qint32 row=_documents->rowCount()-1; row>=0; --row)
        _documents->removeRowAndDeleteImage (row);
    _documents->submitAll					();
    // Now, delete the slip.
    foreach (QModelIndex const &idx, idxs)
        if (_slips->index(idx.row(), SlipsModel::Key).data().toInt()!=1)
            _slips->removeRow               (idx.row());
    _slips->submitAll						();
}

// License problem slot.
void RSUUI::Core::licenseMismatchDetected () {
    QMessageBox::warning(
                    QApplication::activeWindow(),
                    tr("Licensing Issue"),
                    tr("There was an internal problem due to an invalid license key. Please ensure your license key has been entered correctly."));
}
// Dialogs handling.
void RSUUI::Core::presentSlipsDialog (int) {
    // Instanciate slips dialog if required.
    if (!_dlgSlips) {
        _dlgSlips					= new DlgSlips();
        // Core -> DlgSlips.
        connect(this, SIGNAL(scannerStateChanged(RSUUI::T::ScannerHelper const&)), _dlgSlips, SLOT(scannerStateChanged(RSUUI::T::ScannerHelper const&)));
        connect(this, SIGNAL(scanningStarted(RSUUI::T::ScannerHelper const&)), _dlgSlips, SLOT(scanningStarted(RSUUI::T::ScannerHelper const&)));
        connect(this, SIGNAL(scanningFinished(RSUUI::T::ScannerHelper const&)), _dlgSlips, SLOT(scanningFinished(RSUUI::T::ScannerHelper const&)));
        connect(this, SIGNAL(documentScanned()), _dlgSlips, SLOT(documentScanned()));
        // DlgSlips -> Core.
        connect(_dlgSlips, SIGNAL(startScanning(RSUUI::E::ScanOptions)), this, SLOT(startScanning(RSUUI::E::ScanOptions)));
        connect(_dlgSlips, SIGNAL(sendSlip(QModelIndex,bool)), this, SLOT(markSlipSent(QModelIndex,bool)));
        connect(_dlgSlips, SIGNAL(disputeSlip(QModelIndex)), this, SLOT(markSlipDisputed(QModelIndex)));
        connect(_dlgSlips, SIGNAL(paySlip(QModelIndex, QString)), this, SLOT(markSlipPaid(QModelIndex, QString)));
        connect(_dlgSlips, SIGNAL(deleteSlips(QModelIndexList)), this, SLOT(deleteSlips(QModelIndexList)));
        connect(_dlgSlips, SIGNAL(deleteDocuments(QModelIndexList, QModelIndex)), this, SLOT(deleteDocuments(QModelIndexList, QModelIndex)));
        connect(_dlgSlips, SIGNAL(finished(int)), this, SLOT(slipsDialogFinished(int)));
    }
    // Show slips dialog.
    _dlgSlips->show					();
    // If a main dialog was displayed, hide it and schedule it's showing upon slips dialog's closing.
    if (_dlgMain && _dlgMain->isVisible()) {
        _dlgMain->hide				();
        connect(_dlgSlips, SIGNAL(finished(int)), _dlgMain, SLOT(show()));
    }
}
void RSUUI::Core::slipsDialogFinished (int) {
    // If a main dialog exists, re-apply filters.
    if (_dlgMain)
        _dlgMain->applyDocumentsFilters	();
    _dlgSlips->deleteLater				();
    _dlgSlips							= 0;
}
void RSUUI::Core::toggleSettingsWindow () {
    // Instanciate settings dialog if required.
    if (!_dlgSettings) {
        _dlgSettings				= new DlgSettings();
        // DlgSettings -> Core.
        connect(_dlgSettings, SIGNAL(visibilityChanged(bool)), _actSystraySettingsWin, SLOT(setChecked(bool)));
        connect(_dlgSettings, SIGNAL(finished(int)), this, SLOT(settingsDialogFinished(int)));
    }
    // Show settings dialog.
    _dlgSettings->setVisible		(!_dlgSettings->isVisible());
    // If a main dialog was displayed, hide it and schedule it's showing upon settings dialog's closing.
    if (_dlgMain && _dlgMain->isVisible()) {
        _dlgMain->hide				();
        connect(_dlgSettings, SIGNAL(finished(int)), _dlgMain, SLOT(show()));
    }
}
void RSUUI::Core::settingsDialogFinished (int) {
    // If a main dialog exists, re-apply filters.
    if (_dlgMain)
        _dlgMain->applyDocumentsFilters	();
    _dlgSettings->deleteLater			();
    _dlgSettings						= 0;
}
