# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   Showcase
TEMPLATE        =   app
QT              +=  gui

include         (../../LMLibrary.pri)
include         (../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@executable_path/../../../libRELibrary.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}"
}

target.path     =   $${INSTALLDIR}/bin
INSTALLS        +=  target

VERSION             =   $${ShowcaseVersion}
message             ($${TARGET} is at version $${ShowcaseVersion}.)
checkTouchVersion   ($$TARGET, $$VERSION)

TRANSLATIONS    +=  src/Translations/Showcase_fr.ts
RESOURCES       +=  src/Showcase.qrc
FORMS           +=  src/Forms/WndMain.ui
HEADERS         +=  src/SHCTypes.h \
                    src/Core.h \
                    src/UI/WndMain.h \
                    src/Models/DocumentsModel.h \
                    src/Models/ResultsModel.h \
                    src/EditDistance.h
SOURCES         +=  src/main.cpp \
                    src/SHCTypes.cpp \
                    src/Core.cpp \
                    src/UI/WndMain.cpp \
                    src/Models/DocumentsModel.cpp \
                    src/Models/ResultsModel.cpp \
                    src/EditDistance.cpp
