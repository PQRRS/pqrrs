#include "Core.h"
// Required stuff.
#include "Models/DocumentsModel.h"
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <QtCore/QDir>
#include <QtCore/QThread>
#include <QtCore/QDebug>

class MSleepHelper : public QThread { public: static void msleep(int ms) { QThread::msleep(ms); } };

SHC::Core::Core (LM::Core *lm, QObject *p)
: QObject(p), _lm(lm), _re(0), _documentsModel(0) {
    // Instanciate the recognition engine.
    _re									= new RE::Engine(lm, this);
    _re->loadClientBinaryData           (":/Resources/Required/Showcase.recb");
    // Initialize models.
    _documentsModel                     = new SHC::DocumentsModel(this);
}

SHC::Core::~Core () {
    // Delete RELibrary objects as well.
    if (_re)
        delete					_re;
}
