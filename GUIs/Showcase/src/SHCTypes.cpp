#include "SHCTypes.h"
#include "../../Versioning/ShowcaseVersion.h"
// Required stuff.
#include <RELibrary/RETypes>
#include "Core.h"
#include <QtCore/QDebug>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>

void SHC::StaticInitializer () {
    static bool	initDone	= false;
    if (initDone)			return;
    initDone				= true;
    // Enums.
    #define EnumFullRegistration(name)	qRegisterMetaType<name>(#name); qRegisterMetaTypeStreamOperators<int>(#name)
    EnumFullRegistration	(SHC::E::First);
    EnumFullRegistration	(SHC::E::DocumentType);
    EnumFullRegistration	(SHC::E::DocumentStatus);
    EnumFullRegistration	(SHC::E::Last);
    #undef EnumFullRegistration
}
char const* SHC::Version () {
    return const_cast<char*>(STRINGIZE(ShowcaseVersion));
}

# include <ctype.h>
quint32 SHC::Levenshtein (char const  *w1, char const *w2) {
    quint32     l1      = strlen(w1);
    quint32     l2      = strlen(w2);

    while (l1>0 && l2>0 && w1[0]==w2[0])
        w1++, w2++, l1--, l2--;
    if (!l1)            return l2;
    else if (!l2)       return l1;

    quint32     *v      = new quint32[l2+1];
    for (quint32 j=0; j<l2+1; j++)
        v[j]            = j;

    quint32     cur,
                nxt;
    for (quint32 i=0; i<l1; i++) {
        cur                 = i+1;
        for (quint32 j=0; j<l2; j++) {
            quint32 c   = !(w1[i]==w2[j] || (i && j && w1[i-1]==w2[j] && w1[i]==w2[j-1]));
            nxt         = qMin(qMin(v[j+1]+1, cur+1), v[j]+c);
            v[j]        = cur;
            cur         = nxt;
        }
        v[l2]           = nxt;
    }
    delete[]            v;
    return              nxt;
}


QMetaEnum SHC::MetaForEnumType (QMetaObject const *mo, QVariant::Type const &t) {
    QString const	&mtName		= QMetaType::typeName(t);
    QString			mtNameArr	= mtName.mid(mtName.lastIndexOf(':')+1);
    return mo->enumerator(mo->indexOfEnumerator(mtNameArr.toUtf8().constData()));
}
QVariant SHC::VariantForEnumMetatypeId (QVariant::Type mtid, int value) {
    return QVariant(mtid, (void*)&value);
}
int SHC::IntValueForEnumVariant (QVariant const &v) {
    return *(int const*)v.constData();
}

QString SHC::typeToString (SHC::E::DocumentType dt) {
    switch (dt) {
        case SHC::E::IdentityCard:          return QObject::tr("Identity Card");
        case SHC::E::BankIdentifierCode:    return QObject::tr("Bank Identifier Code");
        case SHC::E::AddressProof:          return QObject::tr("Address Proof");
        case SHC::E::BankCheck:             return QObject::tr("Bank Check");
        default:                            return QObject::tr("Unknown");
    }
}
QString SHC::statusToString (SHC::E::DocumentStatus ds) {
    switch (ds) {
        case SHC::E::Invalid:               return QObject::tr("Invalid");
        case SHC::E::Incomplete:            return QObject::tr("Incomplete");
        case SHC::E::Valid:                 return QObject::tr("Valid");
        default:                            return QObject::tr("Unknown");
    }
}
