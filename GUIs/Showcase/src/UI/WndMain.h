#ifndef SHC_WndMain_h
#define SHC_WndMain_h

#include "ui_WndMain.h"
#include <QtGui/QMainWindow>
#include <QtGui/QItemSelection>

// Forward declarations.
class QDataWidgetMapper;
class QStandardItemModel;
namespace LM {
    class Core;
}
namespace SHC {
    class Core;
    class ResultsModel;
}

namespace SHC {

class WndMain : public QMainWindow {
Q_OBJECT
public:
    /**/                WndMain                             (SHC::Core *core, QWidget *p=0);
    virtual             ~WndMain                            ();
    void                dragEnterEvent                      (QDragEnterEvent *e);
    void                dropEvent                           (QDropEvent *e);

protected:
    // Debug messages handling.
    static void         staticMessageHandler                (QtMsgType type, const char *msg);

protected slots:
    // UI updating method.
    void                updateUi                            ();
    // Console actions.
    void                on_btnClearConsole_clicked			();
    // Documents actions.
    void                documentTable_selectionChanged      (QItemSelection const&, QItemSelection const&);
    void                resultsTable_selectionChanged       (QItemSelection const&, QItemSelection const&);
    // Other actions.
    void                setImageFilename                    (QString const &fn);

private:
    // UI Object.
    SHC::Ui::WndMain    *_ui;
    // For console output.
    static WndMain		*_instance;
    QStandardItemModel  *_consoleModel;
    // Other stuff.
    SHC::Core           *_core;
    SHC::ResultsModel   *_resultsModel;
    QModelIndex         _docSel,
                        _resSel;
};

}

#endif
