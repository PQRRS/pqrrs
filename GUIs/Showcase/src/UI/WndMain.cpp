#include "WndMain.h"
#include "ui_WndMain.h"
#include "../Core.h"
#include "../Models/ResultsModel.h"
#include "../EditDistance.h"
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <QtCore/QUrl>
#include <QtCore/QDebug>
#include <QtCore/QThread>
#include <QtGui/QMessageBox>
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>
#include <QtGui/QDataWidgetMapper>
#include <QtGui/QApplication>

SHC::WndMain*		SHC::WndMain::_instance		= 0;

SHC::WndMain::WndMain(SHC::Core *c, QWidget *parent)
: QMainWindow(parent), _ui(new Ui::WndMain), _consoleModel(new QStandardItemModel(0, 3, this)), _core(c), _resultsModel(0) {
    _instance                                       = this;
    _ui->setupUi                                    (this);
    // Create output model.
    _ui->tblVwOutput->setModel                      (_consoleModel);
    on_btnClearConsole_clicked                      ();
    qInstallMsgHandler                              (SHC::WndMain::staticMessageHandler);

    // Adjust splitters.
    _ui->spltrDocuments->setStretchFactor           (0, 1);
    _ui->spltrDocuments->setStretchFactor           (1, 2);
    _ui->spltrDocumentResults->setStretchFactor     (0, 1);
    _ui->spltrDocumentResults->setStretchFactor     (1, 2);

    SHC::DocumentsModel     *dtMdl                  = _core->documentsModel();
    // Setup documents table model.
    _ui->tblVwDocuments->setModel                   (dtMdl);
    // Setup table columns visibility.
    _ui->tblVwDocuments->setColumnHidden            (SHC::DocumentsModel::Type,         false);
    _ui->tblVwDocuments->setColumnHidden            (SHC::DocumentsModel::Status,       false);
    _ui->tblVwDocuments->setColumnHidden            (SHC::DocumentsModel::Results,      true);
    // Setup table columns size.
    _ui->tblVwDocuments->setColumnWidth             (SHC::DocumentsModel::Type,         130);
    _ui->tblVwDocuments->setColumnWidth             (SHC::DocumentsModel::Status,       _ui->tblVwDocuments->width()-140);
    _ui->tblVwDocuments->setColumnWidth             (SHC::DocumentsModel::Results,      0);

    // Setup results table model.
    _resultsModel                                   = new SHC::ResultsModel(this);
    _ui->lstVwDocumentResults->setModel             (_resultsModel);
    _ui->lstVwDocumentResults->setModelColumn       (SHC::ResultsModel::Key);

    // Handle tables selection changes.
    connect(_ui->tblVwDocuments->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
            this, SLOT(documentTable_selectionChanged(QItemSelection, QItemSelection)));
    connect(_ui->lstVwDocumentResults->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
            this, SLOT(resultsTable_selectionChanged(QItemSelection, QItemSelection)));

    setAcceptDrops                                  (true);

    updateUi                                        ();

    // CHQ CNI100 CNI300 RIB A5
    // C:\Showcase\Showcase.exe
    QStringList             args    = QApplication::arguments();
    if (args.count())
        args.pop_front              ();
    Dbg                             << args;
    if (args.count()>=2) {
        E::DocumentType     type    = (SHC::E::DocumentType)0;
        if (args[1]=="CNI100" || args[1]=="CNI300")
            type                    = SHC::E::IdentityCard;
        else if (args[1]=="CHQ")
            type                    = SHC::E::BankCheck;
        QModelIndex         idx;
        for (int row=0; row<dtMdl->rowCount(QModelIndex()); ++row) {
            idx                     = dtMdl->index(row, SHC::DocumentsModel::Type);
            if (idx.data(Qt::EditRole).toInt()==type)
                break;
        }
        _ui->tblVwDocuments->selectionModel()->setCurrentIndex(idx, QItemSelectionModel::Clear | QItemSelectionModel::Select | QItemSelectionModel::Rows);
        setImageFilename        (args[0]);
    }
}

void SHC::WndMain::dragEnterEvent (QDragEnterEvent *e) {
    if (_docSel.isValid())
        e->acceptProposedAction ();
}
void SHC::WndMain::dropEvent (QDropEvent *e) {
    setImageFilename            (e->mimeData()->urls().first().toLocalFile());
}

void SHC::WndMain::staticMessageHandler (QtMsgType t, const char *m) {
    // No MainWindow yet instanciated.
    if (!_instance)
        return;
    else if (_instance->_consoleModel) {
        // Prepare color and icon.
        QColor					clr;
        QIcon					icn;
        switch (t) {
            case QtDebugMsg:	clr.setRgb(0x88, 0xff, 0x88);   icn = QIcon(":/Resources/Images/Icons/Set/checkmark.gif");      break;
            case QtWarningMsg:	clr.setRgb(0xff, 0xcc, 0x88);   icn = QIcon(":/Resources/Images/Icons/Set/caution.gif");        break;
            case QtCriticalMsg:	clr.setRgb(0xff, 0x88, 0x88);   icn = QIcon(":/Resources/Images/Icons/Set/exclamation.gif");    break;
            case QtFatalMsg:	clr.setRgb(0xff, 0x55, 0x55);   icn = QIcon(":/Resources/Images/Icons/Set/stop.gif");           break;
        }

        // Reformat message.
        QString					message		(m);
        // Split message to extract method / message.
        QString					sender, method;
        if (message.contains(" # ")) {
            QStringList			msgs		= message.split(" # ");
            QStringList			origin		= msgs.takeAt(0).split("::");
            method							= origin.takeLast();
            sender							= origin.join("::");
            message							= msgs.join(" # ");
        } else
            sender = method					= "Unknown";

        // Cache model.
        QStandardItemModel		*mdl		= _instance->_consoleModel;
        // Create items row.
        quint32					rowCount	= mdl->rowCount();
        QList<QStandardItem*>	row;
        row									<< new QStandardItem(icn,"") << new QStandardItem(sender) << new QStandardItem(method) << new QStandardItem(message);
        mdl->appendRow						(row);
        mdl->setData						(mdl->index(rowCount, 1), clr, Qt::ForegroundRole);
        mdl->setData						(mdl->index(rowCount, 2), clr, Qt::ForegroundRole);
        mdl->setData						(mdl->index(rowCount, 3), clr, Qt::ForegroundRole);
        QMetaObject::invokeMethod			(_instance->_ui->tblVwOutput, "scrollToBottom", Qt::QueuedConnection);
    }
    if (t==QtFatalMsg)
        abort();
}

void SHC::WndMain::updateUi () {
    bool        docSel                      = _docSel.isValid();
    bool        resSel                      = docSel && _resSel.isValid();
    _ui->lstVwDocumentResults->setEnabled   (docSel);
    _ui->txtDocumentResult->setEnabled      (resSel);
}

void SHC::WndMain::on_btnClearConsole_clicked () {
    _consoleModel->clear					();
    // Update headers.
    _consoleModel->setHorizontalHeaderLabels	(QStringList() << tr("#") << tr("Sender") << tr("Method") << tr("Message"));
}

void SHC::WndMain::setImageFilename (QString const &fn) {
    QImage                  img     (fn);
    if (img.isNull()) {
        Crt                         << "The specified file isn't a recognized image format.";
        return;
    }

    // Cache documents model.
    SHC::DocumentsModel     *mdl    = _core->documentsModel();
    // Cache recognition engine.
    RE::Engine              *re     = _core->recognitionEngine();

    // Create recognition document.
    RE::T::Document         d;
    // Assign image to document.
    d.images["Front"]               = img;
    // Force type code to document.
    quint32                 tc      = mdl->index(_docSel.row(), SHC::DocumentsModel::Type).data(Qt::EditRole).toInt();
    if (tc==2)                      tc=4;
    d.typeDataCode                  = tc;
    foreach (RE::T::DocumentTypeData const &dt, _core->recognitionEngine()->clientBinaryData().documentTypesData) {
        if ((qint32)dt.code==d.typeDataCode) {
            d.typeData              = &dt;
            break;
        }
    }

    // Prepare storable result.
    QMap<QString,QVariant>  res;

    re->guessDocumentType           (&d, QThread::idealThreadCount());
    if (d.typeData->code==tc)
        re->recognizeDocumentData       (&d, QThread::idealThreadCount());

    for (RE::T::StringToStringMap::const_iterator i=d.strings.constBegin(); i!=d.strings.constEnd(); ++i) {
        QString             k       = i.key();
        QString             v       = i.value();
        if (tc==SHC::E::IdentityCard) {
            if (k=="Serial" && v.length()>12)
                    v               = v.left(12);
            if (k=="LastName" || k=="FirstNames" || k=="BirthDate" || k=="MRZLine")
                v                   = v.replace(QRegExp("[-:;.]"), "");
        }
        res[k]                      = v;
    }

    if (d.typeData->code!=tc) {
        if (QMessageBox::question(  this,
                                    tr("Incorrect Type Detected"),
                                    tr("The document doesn't match the expected type (Expected: %1, Found: %2). Would you like to force the recognition as if it was a %3?")
                                        .arg(SHC::typeToString((SHC::E::DocumentType)tc))
                                        .arg(SHC::typeToString((SHC::E::DocumentType)d.typeData->code))
                                        .arg(SHC::typeToString((SHC::E::DocumentType)tc)),
                                    QMessageBox::Yes,
                                    QMessageBox::No)==QMessageBox::No)
            return;
        d.typeDataCode                  = tc;
        foreach (RE::T::DocumentTypeData const &dt, _core->recognitionEngine()->clientBinaryData().documentTypesData) {
            if ((qint32)dt.code==d.typeDataCode) {
                d.typeData              = &dt;
                break;
            }
        }
    }
    else {
        mdl->setData                (mdl->index(_docSel.row(), SHC::DocumentsModel::Status), SHC::E::Incomplete);
        switch (tc) {
        case SHC::E::IdentityCard: {
            // IDFRAMASSIN<<<<<<<<<<<<<<<<<<<<<<<<<
            // 000991300S487FABRICE<<ANDRE7408063M0
            QString     last            = res["LastName"].toString().replace(QRegExp("[- ]"), "<").replace('0', "O");
            QString     first           = res["FirstNames"].toString().replace('0', "O");
            QString     serial          = res["Serial"].toString();
            QString     mrz             = res["MRZLine"].toString();
            // Compute first distance.
            QString     top             = QString("IDFRA%1")
                                            .arg(last.left(25).replace("-", "<"));
            if (top.length()<30)
                top                     += QString("<").repeated(30-top.length());
            quint32     d1              = EditDistance().CalEditDistance(
                            top.toUtf8().constData(),
                            mrz.left(30).toUtf8().constData(),
                            35
                        );
            // Compute second distance.
            QString     bot             = QString("%1<%2")
                                            .arg(serial)
                                            .arg(first.remove(' ').replace(",", "<<").left(14));
            if (bot.length()<30)
                bot                     += QString("<").repeated(30-top.length());
            quint32     d2              = EditDistance().CalEditDistance(
                            bot.toUtf8().constData(),
                            mrz.mid(37, 25).toUtf8().constData(),
                            35
                        );
            quint32     dist            = d1+d2;

            if (dist>20)
                res["RejectionReason"]  = tr("This document didn't pass the security verification tests.");
            else {
                mdl->setData            (mdl->index(_docSel.row(), SHC::DocumentsModel::Status), SHC::E::Valid);
                res["AcceptationReason"]= tr("The document matches it's expected type, and the extracted data is valid.");
            }
            break;
        }
        case SHC::E::BankCheck:
        case SHC::E::BankIdentifierCode:
            mdl->setData                (mdl->index(_docSel.row(), SHC::DocumentsModel::Status), SHC::E::Valid);
            res["AcceptationReason"]    = tr("The document matches it's expected type, and the extracted data is valid.");
            break;
        case SHC::E::AddressProof:
        default:
            mdl->setData                (mdl->index(_docSel.row(), SHC::DocumentsModel::Status), SHC::E::Valid);
            res["AcceptationReason"]    = tr("The document matches it's expected type, and the extracted data is valid.");
            break;
        }
    }

    mdl->setData                    (mdl->index(_docSel.row(), SHC::DocumentsModel::Results), res);
    _resultsModel->resetWithMap     (res);}

void SHC::WndMain::documentTable_selectionChanged (QItemSelection const &sel, QItemSelection const&) {
    _docSel                                 = sel.first().topLeft();
    QMap<QString,QVariant>  res             = _core->documentsModel()->index(_docSel.row(), SHC::DocumentsModel::Results).data().toMap();
    _resultsModel->resetWithMap             (res);
    _ui->txtDocumentResult->setPlainText    ("");
    _resSel                                 = QModelIndex();
    updateUi                                ();
}
void SHC::WndMain::resultsTable_selectionChanged (QItemSelection const &sel, QItemSelection const&) {
    _resSel                                 = sel.first().topLeft();
    _ui->txtDocumentResult->setPlainText    (_resultsModel->index(_resSel.row(), SHC::ResultsModel::Value).data().toString());
    updateUi                                ();
}

SHC::WndMain::~WndMain() {
    delete          _resultsModel;
    delete          _ui;
    delete          _consoleModel;
}
