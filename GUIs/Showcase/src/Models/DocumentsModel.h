#ifndef SHC_DocumentsModel_h
#define SHC_DocumentsModel_h

// Base class.
#include <QtGui/QStandardItemModel>
// Required stuff.
#include "../SHCTypes.h"
#include <QtCore/QVariant>

namespace SHC {

class DocumentsModel : public QStandardItemModel {
Q_OBJECT
public:
    // Columns indices helpers.
    enum { Type = 0, Status, Results, Count };

    // Constructor.
    /**/				DocumentsModel  			(QObject *p);
    // Reimplemented methods.
    Qt::ItemFlags		flags						(QModelIndex const&i) const;
    int                 rowCount                    (QModelIndex const &i) const;
    int					columnCount					(QModelIndex const &i) const;
    QVariant			data						(QModelIndex const &i, int r=Qt::DisplayRole) const;
    bool				setData						(QModelIndex const &i, QVariant const &v, int r=Qt::EditRole);
};

}

#endif
