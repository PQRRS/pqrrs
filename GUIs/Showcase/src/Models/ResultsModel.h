#ifndef SHC_ResultsModel_h
#define SHC_ResultsModel_h

// Base class.
#include <QtCore/QAbstractItemModel>
// Required stuff.
#include "../SHCTypes.h"
#include <QtCore/QVariant>

namespace SHC {

class ResultsModel : public QAbstractItemModel {
Q_OBJECT
public:
    // Columns indices helpers.
    enum { Key = 0, Value, Count };

    // Constructor.
    /**/                    ResultsModel                (QObject *p);
    // Reimplemented methods.
    QVariant                headerData                  (int s, Qt::Orientation o, int r) const;
    QModelIndex             index                       (int r, int c, QModelIndex const &p = QModelIndex()) const;
    QModelIndex             parent                      (QModelIndex const &i) const;
    Qt::ItemFlags           flags						(QModelIndex const &i) const;
    int                     rowCount                    (QModelIndex const &i) const;
    int                     columnCount					(QModelIndex const &i) const;
    QVariant                data						(QModelIndex const &i, int r=Qt::DisplayRole) const;
    bool                	setData						(QModelIndex const &i, QVariant const &v, int r=Qt::EditRole);
    void                    resetWithMap                (QMap<QString,QVariant> &m);

private:
    QMap<QString,QVariant>  _results;
};

}

#endif
