#include "ResultsModel.h"
// Required stuff.
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlIndex>

SHC::ResultsModel::ResultsModel (QObject *p)
: QAbstractItemModel(p) {
}

void SHC::ResultsModel::resetWithMap (QMap<QString, QVariant> &m) {
    _results            = m;
    beginResetModel     ();
    endResetModel       ();
}

QVariant SHC::ResultsModel::headerData (int s, Qt::Orientation o, int) const {
    if (o==Qt::Horizontal) {
        if (s==Key)
            return tr("Key");
        else if (s==Value)
            return tr("Value");
    }
    return QVariant();
}
QModelIndex SHC::ResultsModel::index (int r, int c, QModelIndex const&) const {
    return createIndex(r, c, 0);
}
QModelIndex SHC::ResultsModel::parent (QModelIndex const&) const {
    return QModelIndex();
}
Qt::ItemFlags SHC::ResultsModel::flags (const QModelIndex &i) const {
    Qt::ItemFlags			flags			= QAbstractItemModel::flags(i) | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    return flags;
}
int SHC::ResultsModel::columnCount (const QModelIndex&) const {
    return Count;
}
int SHC::ResultsModel::rowCount (const QModelIndex&) const {
    return _results.count();
}
QVariant SHC::ResultsModel::data (const QModelIndex &i, int r) const {
    if (r==Qt::DisplayRole) {
        if (i.column()==Key)
            return _results.keys()[i.row()];
        else if (i.column()==Value)
            return _results[_results.keys()[i.row()]].toString();
    }
    return QVariant();
}
bool SHC::ResultsModel::setData (QModelIndex const &i, QVariant const &v, int r) {
    return QAbstractItemModel::setData(i, v, r);
}

