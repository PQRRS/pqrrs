#include "DocumentsModel.h"
// Required stuff.
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>
#include <QtCore/QModelIndex>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlIndex>

SHC::DocumentsModel::DocumentsModel (QObject *p)
: QStandardItemModel(4, 4, p) {
    // Setup headers.
    setHeaderData			(Type,					Qt::Horizontal, tr("Type"));
    setHeaderData			(Status,                Qt::Horizontal, tr("Status"));
    setHeaderData			(Results,               Qt::Horizontal, tr("Results"));
    // Set all status to invalid.
    for (quint8 i=0; i<4; ++i) {
        setData             (index(i, Type),        i+1,                Qt::EditRole);
        setData             (index(i, Status),      SHC::E::Invalid,    Qt::EditRole);
    }
}

Qt::ItemFlags SHC::DocumentsModel::flags (const QModelIndex &i) const {
    Qt::ItemFlags			flags			= QStandardItemModel::flags(i) | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    return flags;
}
int SHC::DocumentsModel::columnCount (const QModelIndex&) const {
    return Count;
}
int SHC::DocumentsModel::rowCount (const QModelIndex&) const {
    return 4;
}
QVariant SHC::DocumentsModel::data (const QModelIndex &i, int r) const {
    if (r==Qt::DisplayRole) {
        if (i.column()==Type)
            return SHC::typeToString((SHC::E::DocumentType)i.data(Qt::EditRole).toInt());
        else if (i.column()==Status)
            return SHC::statusToString((SHC::E::DocumentStatus)i.data(Qt::EditRole).toInt());
    }
    return QStandardItemModel::data(i, r);
}
bool SHC::DocumentsModel::setData (QModelIndex const &i, QVariant const &v, int r) {
    return QStandardItemModel::setData(i, v, r);
}

