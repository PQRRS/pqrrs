#ifndef SHC_Core_h
#define SHC_Core_h

// Base class.
#include <QtCore/QObject>
// Required stuff.
#include "SHCTypes.h"
#include "Models/DocumentsModel.h"
#include <QtCore/QDateTime>
#include <QtCore/QModelIndexList>
#include <QtGui/QSystemTrayIcon>
#include <QtGui/QStandardItemModel>

// Forward declarations.
namespace LM {
    class Core;
}
namespace RE {
    class Engine;
    namespace T {
        struct Document;
    }
}

namespace SHC {

class Core : public QObject {
Q_OBJECT

public:
    /**/								Core							(LM::Core *lm, QObject *p=0);
    /**/								~Core							();
    // Models getters.
    SHC::DocumentsModel*                documentsModel                  ()                      { return _documentsModel; }

    // Various getters.
    inline RE::Engine*					recognitionEngine				() const                { return _re; }

private:
    // Licensing stuff.
    LM::Core                            *_lm;
    // RELibrary stuff.
    RE::Engine                          *_re;
    SHC::DocumentsModel                 *_documentsModel;

};

}

#endif
