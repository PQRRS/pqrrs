#ifndef SHC_SHCTypes_h
#define SHC_SHCTypes_h

// Required stuff.
#include <QtCore/QObject>
#include <QtCore/QVariant>
class QMutex;

#ifndef Dbg
#   define Dbg      qDebug()    << Q_FUNC_INFO << "#"
#   define Wrn      qWarning()  << Q_FUNC_INFO << "#"
#   define Crt      qCritical() << Q_FUNC_INFO << "#"
#endif

#pragma pack(1)

#define	RegisterEnumStream(T)\
        friend QDataStream&	operator<<	(QDataStream& s, T const& t)	{ int i=t; return s << i; };\
        friend QDataStream&	operator>>	(QDataStream& s, T &t)			{ int i; s >> i; t=(T)i; return s; }

namespace SHC {
    // ------------------------------------			Constants			------------------------------------ //


    // ------------------------------------				Enums			------------------------------------ //
    // Enum ranges containers.
    typedef QPair<int,int>	MetaTypesRange;
    typedef QList<MetaTypesRange> MetaTypesRangeList;
    typedef QHash<QMetaObject const*,MetaTypesRangeList> MetaTypesRangeListHash;
    // Enums.
    class E {
    Q_GADGET
    public:
    Q_ENUMS	(First DocumentType DocumentStatus Last)
    Q_FLAGS	(ScanOptions)
        enum        First				{ FirstValue };
        enum        DocumentType        { IdentityCard = 1, BankIdentifierCode, AddressProof, BankCheck };
        enum        DocumentStatus      { Invalid = 0, Incomplete, Valid };
        enum        Last				{ LastValue };
        // Expose enums and flags.
        RegisterEnumStream	(SHC::E::First)
        RegisterEnumStream	(SHC::E::DocumentType)
        RegisterEnumStream	(SHC::E::DocumentStatus)
        RegisterEnumStream	(SHC::E::Last)
    };
    // Enum helper methods.
    QMetaEnum	MetaForEnumType				(QMetaObject const *mo, QVariant::Type const &t);
    QVariant	VariantForEnumMetatypeId	(QVariant::Type mt, int v);
    int			IntValueForEnumVariant		(QVariant const &v);
    // Enum to string.
    QString     typeToString                (SHC::E::DocumentType dt);
    QString     statusToString              (SHC::E::DocumentStatus ds);

    // ------------------------------------				Types			------------------------------------ //
    #ifdef Q_MOC_RUN
    class  T { Q_GADGET public:
    #else
    namespace T {
    #endif
        // Force metaobject declaration.
        extern const QMetaObject staticMetaObject;
    }

    // -------------------------------------		Other stuff			------------------------------------- //
    // Complete types registration for streaming and register metatypes.
    void			StaticInitializer					();
    char const*     Version								();
    quint32         Levenshtein                         (char const  *w1, char const *w2);
}

#pragma pack()

// Enums.
Q_DECLARE_TYPEINFO(SHC::E::First, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(SHC::E::First)
Q_DECLARE_TYPEINFO(SHC::E::DocumentType, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(SHC::E::DocumentType)
Q_DECLARE_TYPEINFO(SHC::E::DocumentStatus, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(SHC::E::DocumentStatus)
Q_DECLARE_TYPEINFO(SHC::E::Last, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(SHC::E::Last)
// Flags.
// Classes.

#endif
