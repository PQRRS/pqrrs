// Required stuff.
#include "Core.h"
#include "UI/WndMain.h"
#include <LMLibrary/Core>
#include <RELibrary/RETypes>
#include <QtCore/QDir>
#include <QtCore/QLocale>
#include <QtCore/QTranslator>
#include <QtGui/QApplication>
#include <QtGui/QStyleFactory>

int main (int argc, char *argv[]) {
    QApplication	a			(argc, argv);
    a.setQuitOnLastWindowClosed	(true);
    a.setStyle					(QStyleFactory::create("Plastique"));

    // Setup licensing.
    LM::Core        *lm         = new LM::Core(&a);
    lm->loadFromFile            (QString("%1/.PQRRSLicense.lic").arg(QDir::homePath()));
    // Initialize RELibrary.
    RE::StaticInitializer       ();

    // Install translator.
    QTranslator		trans;
    QString			locale		= QLocale::system().name().left(2).toUpper();
    bool			loadOk		= trans.load(QString(":/Translations/Showcase_%1.qm").arg(locale));
    if (loadOk)		qApp->installTranslator(&trans);

    SHC::Core       *c          = new SHC::Core(lm, &a);

    // Initialize things, and display main window.
    SHC::WndMain	w           (c);
    w.show                      ();
    // Run the application.
    int				ret			= a.exec();
    delete                      c;
    // Finally return application result.
    return			ret;
}
