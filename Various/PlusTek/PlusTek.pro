# Base settings.
include             (../../Common.pri)
DESTDIR             =   $$MAINBINDIR
TARGET              =   PlusTek
TEMPLATE            =   app
QT                  +=  gui

target.path         =   $${INSTALLDIR}/bin
INSTALLS            +=  target

VERSION             =   $${PlusTekVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

SOURCES             +=  src/main.cpp
