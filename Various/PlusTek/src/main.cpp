#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCore/QProcess>
#include <QtCore/QFileInfo>
#include <QtGui/QApplication>
#include <QtGui/QMessageBox>
#include <QtGui/QDesktopWidget>

int main (int iCount, char* args[]) {
    QApplication        app     (iCount, args);
    QString             path    = "C:/home/Administrator/Documents/PQRRS/PQRRS/Release";
    //QString             path    = "C:/Showcase";
    QString             an      = QFileInfo(args[0]).fileName();
    QStringList         pArgs;
    pArgs                       << args[1] << an.mid(0, an.length()-4);
    QProcess            *sc     = new QProcess(&app);
    sc->setWorkingDirectory     (path);
    QObject::connect            (sc, SIGNAL(finished(int)), &app, SLOT(quit()));
    sc->start                   (QString("%1/Showcase.exe").arg(path), pArgs);
    return                      app.exec();
}
