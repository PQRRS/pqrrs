#include <RELibrary/RETypes>
#include <SCLibrary/SCTypes>
#include <RELibrary/Plugins/AbstractPlugin>
#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCore/QProcess>
#include <QtCore/QPluginLoader>
#include <QtCore/QCoreApplication>
#include <QtXml/QDomNode>
#include <QtXml/QDomDocument>

int main(int, char*[]) {
    RE::T::StringToStringHash		vrs;
    vrs["fr.PierreQR.RecognitionSuite.RELibrary.Base"]  = QString(RE::Version());
    vrs["fr.PierreQR.RecognitionSuite.SCLibrary.Base"]  = QString(SC::Version());
    vrs["fr.PierreQR.RecognitionSuite.RELibrary.API"]   = vrs["fr.PierreQR.RecognitionSuite.RELibrary.Base"];
    vrs["fr.PierreQR.RecognitionSuite.SCLibrary.API"]   = vrs["fr.PierreQR.RecognitionSuite.SCLibrary.Base"];
    { // Guard.
        QProcess					proc;
        #ifdef Q_OS_DARWIN
        proc.start							("SCLibraryTester.app", QStringList() << "--version");
        #else
        proc.start							("./SCLibraryTester", QStringList() << "--version");
        #endif
        proc.waitForFinished				();
        vrs["fr.PierreQR.RecognitionSuite.Gui.SCLibraryTester"]
                                            = QString(proc.readAllStandardOutput().constData());
    }
    { // Guard.
        QProcess					proc;
        #ifdef Q_OS_DARWIN
        proc.start							("RSUniversalUI.app", QStringList() << "--version");
        #else
        proc.start							("./RSUniversalUI", QStringList() << "--version");
        #endif
        proc.waitForFinished				();
        vrs["fr.PierreQR.RecognitionSuite.Gui.RSUniversalUI.Base"]
                                            = QString(proc.readAllStandardOutput().constData());
    }

    // Check for plugins versions.
    QDir							d		("./");
    // Loop for each found plugin.
    QStringList								fltrs;
    #if defined(Q_OS_WIN)
    fltrs						<< "REPlug*.dll";
    #elif defined(Q_OS_DARWIN)
    fltrs						<< "libREPlug*.dylib";
    #elif defined(Q_OS_UNIX)
    fltrs						<< "libREPlug*.so";
    #endif
    foreach (QString const &fi, d.entryList(fltrs, QDir::Files)) {
        QPluginLoader				plgLdr	(fi);
        plgLdr.load						();
        RE::AbstractPlugin			*plg	= (RE::AbstractPlugin*)plgLdr.instance();
        if (!plg) {
            qWarning()                      << QString("Cannot find an instanciable plugin in %1.").arg(fi);
            continue;
        }
        QString						pkg		= QString("fr.PierreQR.RecognitionSuite.RELibrary.Plugins.%1").arg(plg->baseInfos().name);
        vrs[pkg]							= QString(plg->baseInfos().version);
        delete								plg;
        plgLdr.unload						();
    }

    for (RE::T::StringToStringHash::const_iterator i=vrs.constBegin(); i!=vrs.constEnd(); ++i) {
        QString						pkg		= i.key();
        QString						fn		= QString("../Packaging/QtIfwInstaller/packages/%1/meta/package.xml").arg(pkg);
        QString                     vNew	= i.value();
        if (!vNew.length()) {
            qWarning()                      << "Invalid version from" << pkg << "!";
            continue;
        }
        QFile						fPkg	(fn);
        if (!fPkg.exists()) {
            qWarning()                      << pkg << "package not found!";
            continue;
        }
        QDomDocument				doc;
        fPkg.open							(QIODevice::ReadOnly|QIODevice::Text);
        doc.setContent						(&fPkg);
        fPkg.close							();
        QDomElement					root	= doc.documentElement();
        QDomElement					child	= root.firstChild().toElement();
        while (!child.isNull()) {
            if (child.tagName()=="Version") {
                QString				vOld	= child.text();
                qWarning()                  << i.key() << "is at" << vOld;
                if (vOld!=vNew) {
                    child.appendChild		(doc.createTextNode(vNew));
                    QDomNodeList	toDel	= child.childNodes();
                    if (toDel.count())
                        child.removeChild	(toDel.at(0));
                    qWarning()              << "    Updated to" << vNew << ".";
                }
            }
            child							= child.nextSibling().toElement();
        }
        fPkg.open							(QIODevice::Truncate|QIODevice::WriteOnly);
        QTextStream                 sPkg    (&fPkg);
        doc.save							(sPkg, 0);
        fPkg.close							();
    }
    return 0;
}
