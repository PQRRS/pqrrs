# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   VersionsUpdater
TEMPLATE        =   app
QT              +=  xml
CONFIG          +=  console

include         (../../LMLibrary.pri)
include         (../../RELibrary.pri)
include         (../../SCLibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                        install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/../../../libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}" && \
                        install_name_tool \
                            -change \
                                "libSCLibrary.$${SCLibraryMajVersion}.dylib" \
                                "@loader_path/../../../libSCLibrary.$${SCLibraryMajVersion}.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}"
}

target.path     =   $${INSTALLDIR}/bin
INSTALLS        +=  target

VERSION             =   $${VersionUpdaterVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

SOURCES         +=  src/main.cpp
