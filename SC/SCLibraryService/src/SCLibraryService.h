#ifndef SCLibraryService_h
#define SCLibraryService_h

// Base classes.
#include <QtCore/QObject>
#include "../ThirdParty/QtService/QtServiceBase"
// Required stuff.
#include <SCLibrary/SCTypes>
#include <QtCore/QCoreApplication>
// Forward declarations.
namespace SCS {
    class SCLibraryServiceWorker;
}

namespace SCS {

class SCLibraryService : public QObject, public QtService<QCoreApplication> {
Q_OBJECT

public:
	// Constructor / Destructor.
	/**/					SCLibraryService		(int argc, char **argv);
	/**/					~SCLibraryService		();

protected:
	virtual void			start					();
	virtual void			stop					();

public:
	static QString const	serviceName;
private:
	SCLibraryServiceWorker	*_worker;
};

}

#endif
