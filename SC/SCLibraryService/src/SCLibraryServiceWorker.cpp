#include "SCLibraryServiceWorker.h"
#include <SCLibrary/SCLibrary>
#include <SCLibrary/SCTypes>
#include <QtCore/QFile>
#include <QtCore/QDebug>

SCS::SCLibraryServiceWorker::SCLibraryServiceWorker (QObject *p)
: QThread(p), _hasToRun(true), _sc(0) {
}

void SCS::SCLibraryServiceWorker::run () {
	// Read configuration.
	QFile			fConf				("c:\\SCLibraryService.scconf");
	QDataStream		sConf				(&fConf);
	fConf.open							(QIODevice::ReadOnly);
	sConf								>> _deviceConfig
										>> _config;
	fConf.close							();
	_config.userObject					= this;
	
	// Read overiddes.
	QSettings		s					("c://SCLibraryServiceOverrides.inf", QSettings::IniFormat);
	SC::DeviceConfig::readOverrides		(_deviceConfig, s);
	SC::Config::readOverrides			(_config, s);
	
	// Make sure callbacks are set to null.
	_config.onProcessingStarted			= 0;
	_config.onNewDocument				= 0;
	_config.onDocumentRecognitionDone	= 0;
	_config.onProcessingFinished		= (SC::Config::ProcessingFinishedCallback)processingFinished;
	
	// Instanciate controller forever or unitl asked elsewise.
	while (_hasToRun) {
		// Instanciate a controller.
        if (!(_sc=(SC::AScannerController*)SCInstanciate(&_deviceConfig)))
			break;
		// Initialize the controller.
		if (SCInitialize(_sc)) {
			// Configure controller.
			msleep					(500);
            if (_sc && SCConfigure(_sc, &_config)) {
				// Run until asked elsewise.
				while (_hasToRun) {
					msleep			(500);
					QMutexLocker	ml	(&_waitMutex);
					if (!_sc || !SCStartProcessing(_sc))
						break;
					// Wait for the operation to either complete or to fail.
					_waitCondition.wait	(&_waitMutex);
					// Check for errors.
					if (!_sc || SCErrorCode(_sc, true))
						break;
				}
			}
		}
		// Release controller.
		SCStopProcessing			(_sc);
		SCDelete					(_sc);
		_sc							= 0;
		if (_hasToRun)				msleep(10000);
	}
}

int SCS::SCLibraryServiceWorker::processingFinished (SCS::SCLibraryServiceWorker *_this) {
	_this->_waitCondition.wakeAll		();
	return								0;
}

void SCS::SCLibraryServiceWorker::stop () {
	_hasToRun							= false;
	_waitCondition.wakeAll				();
}
