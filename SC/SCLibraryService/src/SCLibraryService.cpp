#include "SCLibraryService.h"
#include "SCLibraryServiceWorker.h"
#include <QtCore/QWaitCondition>

QString	const SCS::SCLibraryService::serviceName	("Anikop Service");

SCS::SCLibraryService::SCLibraryService (int argc, char **argv)
: QObject(), QtService<QCoreApplication>(argc, argv, serviceName) {
	setServiceDescription			("Anikop Scanner Controller Service.");
	setServiceFlags					(QtServiceBase::Default);
	setStartupType					(QtServiceController::ManualStartup);
	createApplication				(argc, argv);
    _worker							= new SCLibraryServiceWorker(this);
}
SCS::SCLibraryService::~SCLibraryService () {
	stop							();
}

void SCS::SCLibraryService::start () {
	_worker->start					();
}
void SCS::SCLibraryService::stop () {
    // Helper class to allow calling msleep.
    class MSleepHelper : public QThread { public: static void msleep(int ms) { QThread::msleep(ms); } };
    // Stop worker, wait for termination.
    _worker->stop					();
    while (_worker->isRunning())
        MSleepHelper::msleep(125);
}
