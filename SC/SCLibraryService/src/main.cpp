#include "SCLibraryService.h"
#include "../ThirdParty/QtService/QtServiceBase"
#include <LMLibrary/LMTypes>
#include <SCLibrary/SCTypes>
#include <QtCore/QDir>
#include <QtCore/QMutex>
#include <QtCore/QDebug>
#include <QtCore/QThread>
#include <QtCore/QWaitCondition>
#include <QtCore/QCoreApplication>

int main (int argc, char *argv[]) {
    // Helper functions...
    if (argc==2) {
        QCoreApplication	a					(argc, argv);
        // Helper class to allow calling msleep.
        class MSleepHelper : public QThread { public: static void msleep(int ms) { QThread::msleep(ms); } };
        // Parse arguments.
        QString				arg					(argv[1]);
        if (arg=="-i" || arg=="--install")		return QtServiceController::install(argv[0], "", "");
        else if (arg=="-s" || arg=="--start")	return QtServiceController(SCS::SCLibraryService::serviceName).start();
        else if (arg=="-t" || arg=="--stop")	return QtServiceController(SCS::SCLibraryService::serviceName).stop();
        else if (arg=="-r" || arg=="--restart") {
            QtServiceController	svc				(SCS::SCLibraryService::serviceName);
            svc.stop							();
            while (svc.isRunning())
                MSleepHelper::msleep            (125);
            return								svc.start();
        }
        else if (arg=="-u" || arg=="--uninstall") {
            QtServiceController	svc				(SCS::SCLibraryService::serviceName);
            svc.stop							();
            return								svc.uninstall();
        }
        qDebug()								<< "Invalid option:" << arg << "!";
        return									false;
    }

    // Run all required initialization.
    SC::StaticInitializer						();

    // Run service.
    SCS::SCLibraryService	s                   (argc, argv);
    // Remove default Qt plugins path.
    foreach (QString const &p, qApp->libraryPaths())
        if (p.startsWith("C:/Qt"))				qApp->removeLibraryPath(p);
    return										s.exec();
}
