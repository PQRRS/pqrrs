#ifndef SCLibraryServiceWorker_h
#define SCLibraryServiceWorker_h

// Base class.
#include <QtCore/QThread>
// Required stuff.
#include <SCLibrary/SCTypes>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>

namespace SC { class AScannerController; }

namespace SCS {

class SCLibraryServiceWorker : public QThread {
Q_OBJECT

public:
	// Constructor.
							SCLibraryServiceWorker	(QObject *p=0);
	// Start / stop stuff.
	void					run						();
	void					stop					();

	static int __stdcall	processingFinished		(SCS::SCLibraryServiceWorker *userObject);

private:
	// Run loop flag.
	bool					_hasToRun;
	QMutex					_waitMutex;
	QWaitCondition			_waitCondition;
	// Configuration.
	SC::DeviceConfig		_deviceConfig;
	SC::Config				_config;
	// Instanciated controller.
	SC::AScannerController	*_sc;
};

}

#endif
