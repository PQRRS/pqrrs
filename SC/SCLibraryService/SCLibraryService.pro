# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   SCLibraryService
TEMPLATE        =   app
QT              +=  core gui

# PQR other libraries...
INCLUDEPATH     +=  $${BASEDIR}/LM/LMLibrary/include \
                    $${BASEDIR}/SC/SCLibrary/include
DEPENDPATH      +=  $${BASEDIR}/LM/LMLibrary \
                    $${BASEDIR}/SC/SCLibrary
LIBS            +=  -L"$${MAINBINDIR}" \
                    -lLMLibrary \
                    -lSCLibrary
DEFINES         +=  LMLibCppStyle \
                    SCLibCppStyle

# MacOSX specific.
macx: {
    QMAKE_POST_LINK +=  install_name_tool \
                            -change \
                                "libLMLibrary.1.dylib" \
                                "@executable_path/libLMLibrary.1.dylib" \
                                "$${DESTDIR}/$${TARGET}" && \
                        install_name_tool \
                            -change \
                                "libSCLibrary.1.dylib" \
                                "@executable_path/libSCLibrary.1.dylib" \
                                "$${DESTDIR}/$${TARGET}"
}

HEADERS         +=  src/SCLibraryService.h \
                    src/SCLibraryServiceWorker.h \
                    ThirdParty/QtService/qtservice.h \
                    ThirdParty/QtService/qtservice_p.h

SOURCES         +=  src/main.cpp \
                    src/SCLibraryService.cpp \
                    src/SCLibraryServiceWorker.cpp \
                    ThirdParty/QtService/qtservice.cpp
