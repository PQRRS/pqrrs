# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   SCLibraryTester
TEMPLATE        =   app
QT              +=  gui

include         (../../LMLibrary.pri)
include         (../../RELibrary.pri)
include         (../../SCLibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                            install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/../../../libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}" && \
                        install_name_tool \
                            -change \
                                "libSCLibrary.$${SCLibraryMajVersion}.dylib" \
                                "@loader_path/../../../libSCLibrary.$${SCLibraryMajVersion}.dylib" \
                                "$${DESTDIR}/$${TARGET}.app/Contents/MacOS/$${TARGET}"
}

target.path     =   $${INSTALLDIR}/bin
INSTALLS        +=  target

VERSION             =   $${SCLibraryTesterVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

TRANSLATIONS    +=  src/Translations/SCLibraryTester_fr.ts

RESOURCES       +=  src/SCLibraryTester.qrc

FORMS           +=  src/Forms/WndTester.ui

HEADERS         +=  src/UI/WndTester.h

SOURCES         +=  src/main.cpp \
                    src/UI/WndTester.cpp
