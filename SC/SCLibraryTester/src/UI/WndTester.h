#ifndef WndTester_h
#define WndTester_h

// Base class and private UI.
#include <QtGui/QDialog>
#include "ui_WndTester.h"
// Required stuff.
#include <SCLibrary/SCTypes>

class QCloseEvent;
namespace SC {
    struct DeviceConfig;
    struct Config;
    class AScannerController;
}

namespace SCLTester {

class WndTester : public QDialog {
Q_OBJECT

public:
    // Constructor / Destructor.
                                    WndTester								(QWidget *parent=0, Qt::WFlags flags=0);
    static const char*  			Version									();

protected:
    void							closeEvent								(QCloseEvent *e);

protected slots:
    void							on_btnDataPath_clicked					();
    void							on_btnDataPathReset_clicked				();
    void							on_btnInit_clicked						();
    void							on_btnRecognitionDataPath_clicked		();
    void							on_btnRecognitionDataPathReset_clicked	();
    void							on_btnPrintImageFilename_clicked		();
    void							on_btnProcessing_clicked				();
    void							on_btnImportSettings_clicked			();
    void							on_btnExportSettings_clicked			();
    void							close									();

public slots:
    Q_INVOKABLE void				updateUi								();
    Q_INVOKABLE void				output									(QString const&);
    bool                            setRecognitionDataPath                  (QString const&);
    void							releaseSCInstance						();

private:
    static quint32 SCCBCConv cbOnProcessingStarted          (WndTester *uo);
    static quint32 SCCBCConv cbOnProcessingFinished         (WndTester *uo);
    static quint32 SCCBCConv cbOnNewDocument                (WndTester *uo, quint32 dId, quint32 code, char const *name, void *doc);
    static quint32 SCCBCConv cbOnDocumentRecognitionDone    (WndTester *uo, quint32 dId, quint32 stringCount, char const *timestamp, void *doc);

    // Main UI.
    Ui::WndTester					_ui;
    // Instanciated controller and configuration.
    SC::DeviceConfig				*_deviceConfig;
    SC::Config						*_config;
    void                            *_sc;
    // Processing flag.
    bool							_processing;
};

}

#endif
