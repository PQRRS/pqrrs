#include "WndTester.h"
#include "../../Versioning/SCLibraryTesterVersion.h"
// Required stuff.
#include <LMLibrary/LMTypes>
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include <SCLibrary/SCTypes>
#include <SCLibrary/SCLibrary>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtCore/QLibrary>
#include <QtCore/QSettings>
#include <QtCore/QByteArray>
#include <QtCore/QDataStream>
#include <QtCore/QtConcurrentRun>
#include <QtGui/QFileDialog>
#include <QtGui/QCloseEvent>
#include <QtGui/QMessageBox>

SCLTester::WndTester::WndTester (QWidget *parent, Qt::WFlags flags)
: QDialog(parent, flags),
_deviceConfig(0), _config(0), _sc(0), _processing(false) {
    _ui.setupUi								(this);
    _ui.txtVersionSCLibraryTester->setText	(Version());

    _ui.txtVersionRELibrary->setText        (RE::Version());
    _ui.txtVersionSCLibrary->setText        (SC::Version());

    // Initialize required structures.
    _deviceConfig							= new SC::DeviceConfig();
    _config									= new SC::Config();

    updateUi								();
}
const char* SCLTester::WndTester::Version () {
    return const_cast<char* const>(STRINGIZE(SCLibraryTesterVersion));
}

void SCLTester::WndTester::closeEvent (QCloseEvent*) {
    releaseSCInstance			();
    delete						_deviceConfig;
    delete						_config;
}

quint32 SCCBCConv SCLTester::WndTester::cbOnProcessingStarted (WndTester *_this) {
    QMetaObject::invokeMethod   (_this, "output", Q_ARG(QString, tr("Processing has started.")));
    return 0;
}
quint32 SCCBCConv SCLTester::WndTester::cbOnProcessingFinished (WndTester *_this) {
    _this->_processing          = false;
    QMetaObject::invokeMethod   (_this, "updateUi");
    QMetaObject::invokeMethod   (_this, "output", Q_ARG(QString, tr("Processing has ended.")));
    return 0;
}
quint32 SCCBCConv SCLTester::WndTester::cbOnNewDocument (WndTester *_this, quint32, quint32, char const*, void*) {
    return _this->_ui.cmbFirstCallback->currentIndex();
}
quint32 SCCBCConv SCLTester::WndTester::cbOnDocumentRecognitionDone (WndTester *_this, quint32, quint32, char const*, void*) {
    return _this->_ui.cmbSecondCallback->currentIndex();
}

void SCLTester::WndTester::on_btnDataPath_clicked () {
    QString	szPath	= QFileDialog::getExistingDirectory(this, tr("Destination Data Path"), _ui.txtDataPath->text(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (szPath.length())
        _ui.txtDataPath->setText(szPath);
}
void SCLTester::WndTester::on_btnDataPathReset_clicked () {
    _ui.txtDataPath->setText("");
}

void SCLTester::WndTester::on_btnInit_clicked () {
    // Not yet initialized.
    if (!_sc) {
        // Device configuration.
        _deviceConfig->enableLogging            = _ui.chkLogging->isChecked();
        quint32					devId           = _ui.cmbInitDevice->currentIndex()==_ui.cmbInitDevice->count()-1 ? SC::E::DeviceIdentifierDummy : _ui.cmbInitDevice->currentIndex();
        _deviceConfig->deviceId                 = (SC::E::DeviceIdentifier)devId;
        SC::deleteAndCopy                       (_deviceConfig->dataPath, _ui.txtDataPath->text());
        SC::deleteAndCopy                       (_deviceConfig->locale, _ui.txtLocale->text());

        _sc                                     = SCInstanciate(QString("%1/.PQRRSLicense.lic").arg(QDir::homePath()).toUtf8().constData(), _deviceConfig);
        if (!_sc) {
            output                              (tr("Failed to instanciate scanner controller."));
            return                              updateUi();
        }

        // Initialize controller.
        if (!SCInitialize(_sc)) {
            SC::E::ErrorCode	err             = (SC::E::ErrorCode)SCErrorCode(_sc, true);
            output                              (tr("Failed to initialize scanner controller: %1.").arg((quint32)err));
            releaseSCInstance                   ();
            return                              updateUi();
        }
        output                                  (tr("Scanner controller successfully initialized."));
    }

    // Free instance.
    else
        releaseSCInstance();
    updateUi();
}

void SCLTester::WndTester::on_btnRecognitionDataPath_clicked () {
    QString				path		= QFileDialog::getOpenFileName(this, tr("Recognition Data File"), _ui.txtRecognitionDataPath->text(), QString("%1 (*.recb)").arg(tr("RE Client Binaries")));
    if (path.length()) {
        setRecognitionDataPath          (path);
    }
}
void SCLTester::WndTester::on_btnRecognitionDataPathReset_clicked () {
    _ui.txtRecognitionDataPath->setText("");
}
bool SCLTester::WndTester::setRecognitionDataPath (QString const &path) {
    _ui.txtRecognitionDataPath->setText(path);
    // No new binary data... Just return.
    if (path.isEmpty())
        return true;

    // Load new client binary.
    RE::T::ClientBinaryData     cbd;
    QFile						fCb			(path);
    QDataStream					fS			(&fCb);
    fCb.open								(QIODevice::ReadOnly);
    fS										>> cbd;
    fCb.close								();
    if (!RE::Engine::isClientBinaryDataCompatible(cbd)) {
        QMessageBox::warning                (this, tr("Warning"), tr("The Client Binary Data is incompatible with at least one component. Please re-generate it."), QMessageBox::Ok, QMessageBox::NoButton);
        return                              false;
    }

    return									true;
}

void SCLTester::WndTester::on_btnPrintImageFilename_clicked () {
    QString				path		= QFileDialog::getOpenFileName(this, tr("Print Image Filename"), _ui.txtPrintImageFilename->text());
    if (path.length())				_ui.txtPrintImageFilename->setText(path);
}

void SCLTester::WndTester::on_btnProcessing_clicked () {
    // MICR options.
    _config->enableMicr						= _ui.chkMicr->isChecked();
    // Image options.
    _config->sides							= (SC::E::DocumentSide)_ui.cmbSides->currentIndex();
    _config->reversedDocument				= _ui.chkReversedDocument->isChecked();
    _config->preferedResolution				= (_ui.cmbResolution->currentIndex()+1)*100;
    _config->enableColor					= _ui.chkColor->isChecked();
    _config->enableImagesSaving				= _ui.chkImagesSave->isChecked();
    // Misc options.
    _config->dropFeedEnabled				= _ui.chkDropFeedEnabled->isChecked();
    _config->machineUid						= _ui.spnMachineUid->value();
    _config->enableDocUidReset				= _ui.chkResetDocUid->isChecked();
    _config->enableRejectionTests			= _ui.chkRejectionTests->isChecked();
    _config->enableDoubleFeedingDetection	= _ui.chkDoubleFeeding->isChecked();
    _config->enableRecognition				= _ui.chkRecognitionEnabled->isChecked();
    _config->enableProprietaryRecognition	= _ui.chkProprietaryRecognitionEnabled->isChecked();
    SC::deleteAndCopy						(_config->recognitionDataPath, _ui.txtRecognitionDataPath->text());
    _config->enableResultFilesSplit			= _ui.chkSplitResultFiles->isChecked();
    _config->maximumDocumentCount			= _ui.spnMaxDocuments->value();
    // Print data.
    SC::deleteAndCopy						(_config->printData, _ui.txtPrintData->toPlainText().replace("\r\n", "\r"));
    SC::deleteAndCopy						(_config->printImageFilename, _ui.txtPrintImageFilename->text().replace("\r\n", "\r"));
    _config->printInBold					= _ui.chkPrintInBold->isChecked();
    _config->printOffset					= _ui.spnPrintOffset->value();
    _config->printImageOffset				= _ui.spnPrintImageOffset->value();
    // Callbacks.
    _config->userObject						= this;
    _config->onProcessingStarted			= (SC::Config::ProcessingStartedCallback)&cbOnProcessingStarted;
    _config->onProcessingFinished			= (SC::Config::ProcessingFinishedCallback)&cbOnProcessingFinished;
    _config->onNewDocument					= (SC::Config::NewDocumentCallback)&cbOnNewDocument;
    _config->onDocumentRecognitionDone		= (SC::Config::DocumentRecognitionDoneCallback)&cbOnDocumentRecognitionDone;

    if (!SCConfigure(_sc, _config)) {
        SC::E::ErrorCode	err				= (SC::E::ErrorCode)SCErrorCode(_sc, true);
        output								(tr("Failed to configure scanner controller: %1.").arg((quint32)err));
        return								updateUi();
    }
    output									(tr("Scanner controller successfully configured."));

    if (!SCStartProcessing(_sc)) {
        output								("Failed to start processing.");
        SC::E::ErrorCode	err				= (SC::E::ErrorCode)SCErrorCode(_sc, true);
        output								(tr("Failed to start processing with code: %1").arg((quint32)err));
        return;
    }
    _processing								= true;
    updateUi								();
}

void SCLTester::WndTester::on_btnImportSettings_clicked () {
    QSettings			s					("Pierre qui Roule EURL", "SCLibrary");
    QString				path				= s.value("WndMain::SettingsLastPath", QDir::homePath()).toString();
    path									= QFileDialog::getOpenFileName(this, tr("Open Config File..."), path, QString("%1 (*.scconf)").arg(tr("SC Config Files")));
    if (path.isEmpty())						return;
    s.setValue								("WndMain::SettingsLastPath", QFileInfo(path).dir().path());

    // Read file.
    { // Read file.
        QFile		fConf					(path);
        QDataStream	sConf					(&fConf);
        fConf.open							(QIODevice::ReadOnly);
        int				cbIdx1, cbIdx2;
        sConf								>> *_deviceConfig >> *_config >> cbIdx1 >> cbIdx2;
        fConf.close							();
        _ui.cmbFirstCallback->setCurrentIndex(cbIdx1);
        _ui.cmbSecondCallback->setCurrentIndex(cbIdx2);
    }

    // Device.
    _ui.chkLogging->setChecked				(_deviceConfig->enableLogging);
    quint32		devId						= _deviceConfig->deviceId!=255 ? _deviceConfig->deviceId : _ui.cmbInitDevice->count()-1;
    _ui.cmbInitDevice->setCurrentIndex		(devId);
    _ui.txtDataPath->setText				(_deviceConfig->dataPath);
    _ui.txtLocale->setText					(_deviceConfig->locale);
    // MICR.
    _ui.chkMicr->setChecked					(_config->enableMicr);
    // Images.
    _ui.cmbSides->setCurrentIndex			(_config->sides);
    _ui.chkReversedDocument->setChecked		(_config->reversedDocument);
    _ui.cmbResolution->setCurrentIndex		((_config->preferedResolution/100)-1);
    _ui.chkColor->setChecked				(_config->enableColor);
    _ui.chkImagesSave->setChecked			(_config->enableImagesSaving);
    // Misc.
    _ui.chkDropFeedEnabled->setChecked		(_config->dropFeedEnabled);
    _ui.spnMachineUid->setValue				(_config->machineUid);
    _ui.chkResetDocUid->setChecked			(_config->enableDocUidReset);
    _ui.chkRejectionTests->setChecked		(_config->enableRejectionTests);
    _ui.chkDoubleFeeding->setChecked		(_config->enableDoubleFeedingDetection);
    _ui.chkRecognitionEnabled->setChecked	(_config->enableRecognition);
    _ui.chkProprietaryRecognitionEnabled->setChecked(
                                            _config->enableProprietaryRecognition);
    setRecognitionDataPath                  (_config->recognitionDataPath);
    _ui.chkSplitResultFiles->setChecked		(_config->enableResultFilesSplit);
    _ui.spnMaxDocuments->setValue			(_config->maximumDocumentCount);
    // Print data.
    _ui.txtPrintData->setPlainText			(_config->printData);
    _ui.txtPrintImageFilename->setText		(_config->printImageFilename);
    _ui.chkPrintInBold->setChecked			(_config->printInBold);
    _ui.spnPrintOffset->setValue			(_config->printOffset);
    _ui.spnPrintImageOffset->setValue		(_config->printImageOffset);
}
void SCLTester::WndTester::on_btnExportSettings_clicked () {
    QSettings			s;
    QString				path				= s.value("WndMain::SettingsLastPath", QDir::homePath()).toString();
    path									= QFileDialog::getSaveFileName(this, tr("Save Config As..."), path, QString("%1 (*.scconf)").arg(tr("SC Config Files")));
    if (path.isEmpty())						return;
    s.setValue								("WndMain::SettingsLastPath", QFileInfo(path).dir().path());

    // Device configuration.
    _deviceConfig->enableLogging			= _ui.chkLogging->isChecked();
    quint32		devId						= _ui.cmbInitDevice->currentIndex()==_ui.cmbInitDevice->count()-1 ? SC::E::DeviceIdentifierDummy : _ui.cmbInitDevice->currentIndex();
    _deviceConfig->deviceId					= (SC::E::DeviceIdentifier)devId;
    SC::deleteAndCopy						(_deviceConfig->dataPath, _ui.txtDataPath->text());
    SC::deleteAndCopy						(_deviceConfig->locale, _ui.txtLocale->text());

    // MICR.
    _config->enableMicr						= _ui.chkMicr->isChecked();
    // Images.
    _config->sides							= (SC::E::DocumentSide)_ui.cmbSides->currentIndex();
    _config->reversedDocument				= _ui.chkReversedDocument->isChecked();
    _config->preferedResolution				= (_ui.cmbResolution->currentIndex()+1)*100;
    _config->enableColor					= _ui.chkColor->isChecked();
    _config->enableImagesSaving				= _ui.chkImagesSave->isChecked();
    // Misc.
    _config->dropFeedEnabled				= _ui.chkDropFeedEnabled->isChecked();
    _config->machineUid						= _ui.spnMachineUid->value();
    _config->enableDocUidReset				= _ui.chkResetDocUid->isChecked();
    _config->enableRejectionTests			= _ui.chkRejectionTests->isChecked();
    _config->enableDoubleFeedingDetection	= _ui.chkDoubleFeeding->isChecked();
    _config->enableRecognition				= _ui.chkRecognitionEnabled->isChecked();
    _config->enableProprietaryRecognition	= _ui.chkProprietaryRecognitionEnabled->isChecked();
    SC::deleteAndCopy						(_config->recognitionDataPath, _ui.txtRecognitionDataPath->text());
    _config->enableResultFilesSplit			= _ui.chkSplitResultFiles->isChecked();
    _config->maximumDocumentCount			= _ui.spnMaxDocuments->value();
    // Print data.
    SC::deleteAndCopy						(_config->printData, _ui.txtPrintData->toPlainText().replace("\r\n", "\r"));
    // Print image.
    SC::deleteAndCopy						(_config->printImageFilename, _ui.txtPrintImageFilename->text().replace("\r\n", "\r"));
    // Print params.
    _config->printInBold					= _ui.chkPrintInBold->isChecked();
    _config->printOffset					= _ui.spnPrintOffset->value();
    _config->printImageOffset				= _ui.spnPrintImageOffset->value();
    // Callbacks.
    _config->userObject						= 0;
    _config->onProcessingStarted			= 0;
    _config->onProcessingFinished			= 0;
    _config->onNewDocument					= 0;
    _config->onDocumentRecognitionDone		= 0;

    // Write file.
    QFile		fConf						(path);
    QDataStream	sConf						(&fConf);
    fConf.open								(QIODevice::WriteOnly);
    sConf									<< *_deviceConfig << *_config << _ui.cmbFirstCallback->currentIndex() << _ui.cmbSecondCallback->currentIndex();
    fConf.close();
}

void SCLTester::WndTester::updateUi () {
    _ui.cmbInitDevice->setEnabled		(true);
    _ui.btnInit->setText				(_sc ? tr("Free"):tr("Initialize"));
    _ui.btnInit->setEnabled				(!_processing);
    _ui.btnProcessing->setEnabled		(_sc && !_processing);
    this->setEnabled					(!_processing);
}

void SCLTester::WndTester::output (QString const &msg) {
    QListWidget	*list				= _ui.lstOutput;
    list->addItem					(msg);
    list->scrollToItem				(list->item(list->count()-1));
}

void SCLTester::WndTester::releaseSCInstance () {
    if (!_sc)						return;
    SCDelete                        (_sc);
    _sc								= 0;
    output							(tr("Scanner controller instance freed."));
    return							updateUi();
}

void SCLTester::WndTester::close () {
    if (_sc)						on_btnInit_clicked();
    QDialog::close					();
}
