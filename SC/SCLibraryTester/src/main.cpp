//#include <SCLibrary/SCTypes>
#include "UI/WndTester.h"
#include <LMLibrary/Core>
#include <QtCore/QDir>
#include <QtCore/QTranslator>
#include <QtCore/QCoreApplication>
#include <QtGui/QStyleFactory>
#include <QtGui/QApplication>

#include <iostream>
int main (int argc, char *argv[]) {
    // Version requested.
    if (argc==2 && QString(argv[1])=="--version") {
        std::cout						<< SCLTester::WndTester::Version();
        return							0;
    }
    // Make application...
    QApplication            *a          = qApp;
    if (!qApp)
        a                               = new QApplication(argc, argv);
    QApplication::setOrganizationName   ("Pierre qui Roule EURL");
    QApplication::setOrganizationDomain ("fr.PierreQR");
    QApplication::setApplicationName    ("SC Library Tester");

    // Tweaks.
    a->setQuitOnLastWindowClosed		(true);
    a->setStyle                         (QStyleFactory::create("Plastique"));

    // Setup licensing.
    LM::Core                *lm         = new LM::Core(a);
    lm->loadFromFile                    (QString("%1/.PQRRSLicense.lic").arg(QDir::homePath()));

    // Initialize libraries...
    SC::StaticInitializer				();

    // Install translator.
    QTranslator				t;
    if (t.load(":/Translations/SCLibraryTester.qm"))
        a->installTranslator            (&t);

    // Prepare main window.
    SCLTester::WndTester	w;
    w.show();
    // Run the application.
    int						ret			= a->exec();
    // Finally return application result.
    return					ret;
}
