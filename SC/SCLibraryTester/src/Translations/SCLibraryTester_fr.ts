<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>SCLTester::WndTester</name>
    <message>
        <source>SCLibrary version %1</source>
        <translation type="obsolete">SCLibrary version %1</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="47"/>
        <source>Processing has started.</source>
        <translation>Le scan a demarré.</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="53"/>
        <source>Processing has ended.</source>
        <translation>Le scan a terminé.</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="64"/>
        <source>Destination Data Path</source>
        <translation>Chemin de Destination</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="84"/>
        <source>Failed to instanciate scanner controller.</source>
        <translation>Echec de l&apos;instanciation du controlleur de scanner.</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="91"/>
        <source>Failed to initialize scanner controller: %1.</source>
        <translation>Echec de l&apos;initialisation du controlleur de scanner: %1.</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="95"/>
        <source>Scanner controller successfully initialized.</source>
        <translation>Controlleur de scanner initialisé avec succes.</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="105"/>
        <source>Recognition Data File</source>
        <translation>Fichier de reconaissance</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="105"/>
        <source>RE Client Binaries</source>
        <translation>Binaires Client RE</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="127"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="127"/>
        <source>The Client Binary Data is incompatible with at least one component. Please re-generate it.</source>
        <translation>Les versions requises spécifiées dans le Binaires Client ne correspondent pas a au moins un composant. Veuillez le régénérer.</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="135"/>
        <source>Print Image Filename</source>
        <translation>Fichier d&apos;image à imprimer</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="174"/>
        <source>Failed to configure scanner controller: %1.</source>
        <translation>Echec de la configuration du controleur de scanner: %1.</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="177"/>
        <source>Scanner controller successfully configured.</source>
        <translation>Controlleur de scanner configué  avec succes.</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="182"/>
        <source>Failed to start processing with code: %1</source>
        <translation>Echec du démarrage du scan avec erreur: %1</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="192"/>
        <source>Open Config File...</source>
        <translation>Ouvrir fichier de config...</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="192"/>
        <location filename="../UI/WndTester.cpp" line="244"/>
        <source>SC Config Files</source>
        <translation>Fichiers de configuration SC</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="244"/>
        <source>Save Config As...</source>
        <translation>Enregistrer la config sous...</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="299"/>
        <source>Free</source>
        <translation>Libérer</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="299"/>
        <source>Initialize</source>
        <translation>Initialiser</translation>
    </message>
    <message>
        <location filename="../UI/WndTester.cpp" line="315"/>
        <source>Scanner controller instance freed.</source>
        <translation>Controleur de scanner libéré.</translation>
    </message>
</context>
<context>
    <name>WndTester</name>
    <message>
        <location filename="../Forms/WndTester.ui" line="14"/>
        <source>SCLibrary Tester</source>
        <translation>SCLibrary Tester</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="33"/>
        <source>Device Configuration</source>
        <translation>Configuration Machine</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="57"/>
        <source>Logging</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="74"/>
        <source>Device Identifier</source>
        <translation>Identifiant Machine</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="91"/>
        <source>Canon CR55</source>
        <translation>Canon CR55</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="96"/>
        <source>Canon CR180</source>
        <translation>Canon CR180</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="101"/>
        <source>Canon CR190</source>
        <translation>Canon CR190</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="106"/>
        <source>Epson TMS1000</source>
        <translation>Epson TMS1000</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="111"/>
        <source>Panini Vision X</source>
        <translation>Panini VISION X</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="116"/>
        <source>Panini IDeal</source>
        <translation>Panini IDeal</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="136"/>
        <source>Dummy</source>
        <translation>Dummy</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="144"/>
        <source>Data path</source>
        <translation>Chemin des Données</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="obsolete">Parcourir</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="899"/>
        <source>Initialize</source>
        <translation>Initialiser</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="182"/>
        <source>Locale</source>
        <translation>Locale</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="121"/>
        <source>Epson TMS2000</source>
        <translation>Epson TMS2000</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="126"/>
        <source>Panini Vision S</source>
        <translation>Panini Vision S</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="131"/>
        <source>Panini wIDeal</source>
        <translation>Panini wIDeal</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="166"/>
        <location filename="../Forms/WndTester.ui" line="510"/>
        <location filename="../Forms/WndTester.ui" line="602"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="173"/>
        <location filename="../Forms/WndTester.ui" line="517"/>
        <source>RAZ</source>
        <translation>RAZ</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="192"/>
        <source>EN</source>
        <translation>FR</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="200"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="224"/>
        <source>Read MICR</source>
        <translation>Lire MICR</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="248"/>
        <source>Drop Feed Mode</source>
        <translation>Alimentation Auto</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="269"/>
        <source>Sides to scan</source>
        <translation>Côtés à scanner</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="303"/>
        <source>Reversed Document</source>
        <translation>Documents à l&apos;Envers</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="320"/>
        <source>Closest Resolution</source>
        <translation>Résolution la plus Proche</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="349"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="366"/>
        <source>Save Images</source>
        <translation>Enregistrer les images</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="390"/>
        <source>Machine UID</source>
        <translation>UID Machine</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="407"/>
        <source>Reset Doc UID</source>
        <translation>Reset du Doc. UID</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="420"/>
        <source>Do Rejection Tests</source>
        <translation>Faire les Tests de Rejet</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="437"/>
        <source>Double-Feeding Detection</source>
        <translation>Detection Double Epaisseur</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="454"/>
        <source>Recognition</source>
        <translation>Reconnaissance</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="471"/>
        <source>Proprietary Recognition</source>
        <translation>Reconnaissance Propriétaire</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="488"/>
        <source>Recognition Data File Path</source>
        <translation>Chemin du Fichier de Reconaissance</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="526"/>
        <source>Split Result Files</source>
        <translation>Decouper les Fichiers de Résultat</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="543"/>
        <source>Max. Docs.</source>
        <translation>Max. Docs.</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="563"/>
        <source>Data to print</source>
        <translation>Ligne à Imprimer</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="580"/>
        <source>Image to print</source>
        <translation>Image à Imprimer</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="611"/>
        <source>Print in Bold</source>
        <translation>Impression en Gras</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="628"/>
        <source>Print Offset</source>
        <translation>Décallage de l&apos;Impression</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="654"/>
        <source>PrintImage Offset</source>
        <translation>Décallage de l&apos;Impression d&apos;Image</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="765"/>
        <source>Versions</source>
        <translation>Versions</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="780"/>
        <source>SCLibrary Tester:</source>
        <translation>SCLibrary Tester:</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="794"/>
        <source>RELibrary:</source>
        <translation>RELibrary:</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="808"/>
        <source>SCLibrary:</source>
        <translation>SCLibrary:</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="906"/>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="331"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="336"/>
        <source>200</source>
        <translation>200</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="341"/>
        <source>300</source>
        <translation>300</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="280"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="285"/>
        <source>Front</source>
        <translation>Devant</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="290"/>
        <source>Rear</source>
        <translation>Derrière</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="295"/>
        <source>Both</source>
        <translation>Les Deux</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="681"/>
        <source>Callbacks</source>
        <translation>Retours</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="696"/>
        <source>First Callback Return</source>
        <translation>Premier Retour</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="728"/>
        <source>Second Callback Return</source>
        <translation>Deuxième Retour</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="710"/>
        <location filename="../Forms/WndTester.ui" line="742"/>
        <source>Reject</source>
        <translation>Rejeter</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="715"/>
        <location filename="../Forms/WndTester.ui" line="747"/>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="720"/>
        <location filename="../Forms/WndTester.ui" line="752"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <source>Load / Save</source>
        <translation type="obsolete">Charger / Enregistrer</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="854"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../Forms/WndTester.ui" line="873"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
</context>
</TS>
