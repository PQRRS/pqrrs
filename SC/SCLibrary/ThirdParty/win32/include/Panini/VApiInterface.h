// Vision API interface ver. 3.5.0

#ifndef VAPI_INTERFACE_INCLUDED
#define VAPI_INTERFACE_INCLUDED

/*********************************************************************
* BASE DATA TYPES DEFINITIONS
*********************************************************************/
typedef DWORD     VAPI_DEVICE_ID;
typedef DWORD     ERR_CODE;
typedef ERR_CODE  VAPI_RET_TYPE;

#ifndef TRUE
  #define TRUE    (1)
#endif
#ifndef FALSE
  #define FALSE   (0)
#endif


/*********************************************************************
* SOME FUNCTION TAGS
*********************************************************************/

// Tag for VISION API function declaration
#define VISION_API_DECL           __declspec(dllimport)

// VISION API function tag
#define VISION_API                WINAPI

/////////////////////////////////////
// Device Parameters definition
/////////////////////////////////////

// FEEDING MODE
#define HOPPER_FEED 0
#define DROP_FEED   1

// MICR FONTS
#define MICR_FONT_CMC7        (0)
#define MICR_FONT_E13B        (1)
#define MICR_FONT_AUTO        (2)
#define MICR_FONT_E13B_OCR    (3)
#define MICR_FONT_CMC7_OCR    (4)
#define MICR_FONT_AUTO_OCR    (5)

// SPACES
#define SPACES_NONE 0
#define SPACES_ONE  1
#define SPACES_ALL  2

// PRINTER ENABLE
#define PRINTER_DISABLE               0x00000000
#define PRINTER_ENABLE_LEADING_EDGE   0x00000001
#define PRINTER_ENABLE_TRAILING_EDGE  0x00000003

// PRINTER QUALITY FLAGS
#define PRINTER_AGP_QUALITY_NORMAL    0x00000000
#define PRINTER_AGP_QUALITY_HIGHQ     0x00000100
#define PRINTER_AGP_QUALITY_DRAFT     0x00000200

// PRINTER SMART JET
#define PRINTER_ENABLE_SMART_JET      0x00010000

// VIRTUAL PRINTER
#define PRINTER_VIRTUAL_ENABLE        0x01000000

// IMAGE FORMAT
#define FORMAT_NONE                 0
#define FORMAT_GIV                  1
#define FORMAT_JPEG                 2
#define FORMAT_BMP                  3
#define FORMAT_NATIVE_BMP           4
#define FORMAT_BMP_COLOR            5
#define FORMAT_JPEG_COLOR           6
#define FORMAT_GIV_DROP_OUT_RED     7
#define FORMAT_GIV_DROP_OUT_GREEN   8
#define FORMAT_GIV_DROP_OUT_BLUE    9
#define FORMAT_BMP_TRUE_COLOR       10
#define FORMAT_JPEG_TRUE_COLOR      11
#define FORMAT_GIV_DOR_TRUE_COLOR   12
#define FORMAT_GIV_DOG_TRUE_COLOR   13
#define FORMAT_GIV_DOB_TRUE_COLOR   14
#define FORMAT_GIV_DROP_OUT_IR      15

#define FRANKED_IMAGE               0x00010000
#define COMP_IMAGE_FOR_DECISION     0x00020000

// IMAGE PAGING
#define PAGING_ONLY_SINGLE  0
#define PAGING_ONLY_MULTI   1
#define PAGING_SINGLE_MULTI 2

// RESOLUTION
#define RESOLUTION_100 100
#define RESOLUTION_200 200
#define RESOLUTION_300 300

typedef struct _ImageProperties
{
  UINT Format;      // Compression Format
  UINT Paging;      // Single-pagefile or multi page TIFF file
  UINT Resolution;  // Resolution (dpi)
  UINT ColorDepth;  // 16, 64 or 256 gray levels
  UINT Threshold;   // JPG quality Factor or G4 threshold
}
IMAGE_PROPERTIES;

///////////////////
// SNIPPETS
///////////////////

#define SNIPPET_ENABLE                0x00000001
#define SNIPPET_ENABLE_FOR_DECISION    (0x00000100|SNIPPET_ENABLE)
#define SNIPPET_DISABLE               0x00000000

// Orientation
#define SNIPPET_ORIENTATION_NORMAL                    0    
#define SNIPPET_ORIENTATION_CCW_90_DEG_ROT            1    
#define SNIPPET_ORIENTATION_UPSIDE_DOWN               2
#define SNIPPET_ORIENTATION_CW_90_DEG_ROT             3
#define SNIPPET_ORIENTATION_VERTICAL_MIRROR           4
#define SNIPPET_ORIENTATION_CCW_90_DEG_ROT_VERT_MIR   5
#define SNIPPET_ORIENTATION_UPSIDE_DOWN_VERT_MIRROR   6
#define SNIPPET_ORIENTATION_CW_90_DEG_ROT_VERT_MIR    7

// Color
#define SNIPPET_COLOR_GREY_LEVEL    0
#define SNIPPET_COLOR_BLACK_WHITE   1
#define SNIPPET_COLOR_COLOR         2

typedef struct _Snippet
{
  UINT Xposition;   // X position of the Snippet.
                    // Referred to the LOWER-RIGHT corner for the FRONT side
                    // Referred to the LOWER-LEFT corner for the REAR side
  UINT Yposition;   // Y position of the Snippet
                    // Referred to the BOTTOM of the document
  UINT Width;       // Snippet Width
  UINT Height;      // Snippet Height
  UINT Orientation; // Snippet Orientation
  UINT Color;       // Snippet Color
  UINT Compression; // Compression Format (Not Yet Implemented)
  BOOL Millimeters; // Units 
} Snippet;

typedef struct _SnippetProperties
{
  BOOL Enable;        // Snippet Enabling
  BOOL Front;         // TRUE if the Snippet is on the Front
  Snippet Properties; // Snippet Properties Struct (see Above)
} SNIPPET_PROPERTIES;

typedef struct _DeviceParameters {
  // MICR
  // MICR Enabling
  BOOL bMICREnable;
  // MICR Font(0=CMC7  1=E13B  2=AUTO, Default: E13B)
  UINT nMICRFont;
  // MICR Waveform File Saving Enabling
  BOOL bMICRSaveSamples;
  // Spaces management (0=NONE  1=SINGLE  2=ALL, Default: NONE)
  UINT nMICRSpaces;

  // Micr+ Library Parameters
  // Reject Symbol Character (Default: '?')
  char cRejectSymbol;
  // Reserved, MUST BE 0
  UINT nReserved;
  // Reserved, MUST BE FALSE
  BOOL bReserved;

  // IMAGE
  // First Front Image Properties
  IMAGE_PROPERTIES  ImagePropertiesFront1;
  // Second Front Image Properties
  IMAGE_PROPERTIES  ImagePropertiesFront2;
  // First Rear Image Properties
  IMAGE_PROPERTIES  ImagePropertiesRear1;
  // Second Rear Image Properties
  IMAGE_PROPERTIES  ImagePropertiesRear2;

  // SNIPPETS
  SNIPPET_PROPERTIES SnippetProperties[10];

  // PRINTER
  // Ink-Jet Enabling
  BOOL bPrintEnable;

  // WORKING MODE
  // One Document feeding option
  BOOL bOneDoc;

  // Feeding options (HOPPER FEED = 0, DROP FEED = 1)
  UINT nFeedingMode;

} DeviceParameters;

typedef struct _tagDevices
{
  char SerialNumber[11];      // Serial number 53xxxxx

  BYTE FeederLimit;           // Valid values are 0(FF), 30(SF), 1(1F)

  BYTE MaxDPM;                // Valid values are 30, 60 and 90

  BOOL MicrE13B;              // TRUE means device available
  BOOL MicrCMC7;              // TRUE means device available
  BOOL MicrAUTO;              // TRUE means device available

  BOOL Inkjet;                // TRUE means device available
  BOOL InkjetMultiline;       // TRUE means device available
  BOOL InkjetGraphic;         // TRUE means device available
  BYTE InkjetLines;           // Valid valus are 0 (Unlimited), 1,2 and 3 lines.

  BOOL ImageFront;            // TRUE means device available
  BOOL ImageFrontClr;         // TRUE means device available
  BOOL ImageFrontDropOut;     // TRUE means device available
  BOOL ImageRear;             // TRUE means device available
  BOOL ImageRearClr;          // TRUE means device available
  BOOL ImageRearDropOut;      // TRUE means device available

  BOOL OcrAB;                 // TRUE means device available
  BOOL OcrMicr;               // TRUE means device available
  BOOL OcrBarcode1D;          // TRUE means device available
  BOOL OcrBarcode2D;          // TRUE means device available
}
DEVICES, *PDEVICES;

// DEVICE STATES
#define DeviceShutDown              0
#define DeviceStartingUp            1
#define DeviceShuttingDown          2
#define DeviceChangeParameters      3
#define DeviceChangingParameters    4
#define DeviceOnLine                5
#define DeviceOffLine               6
#define DeviceStandBy               7
#define DeviceFeeding               8
#define DeviceExceptionInProgress   9
#define DeviceLocked                10

//#############################################################################
// User defined messages parameters send from BridgeThread
//#############################################################################

#define WMPAR_SORTER_CONNECTED                0
#define WMPAR_SORTER_DISCONNECTED             1
#define WMPAR_SORTER_NEW_DOCUMENT             2
#define WMPAR_SORTER_MICR_AVAILABLE           3
#define WMPAR_SORTER_SET_ITEM_OUTPUT          4
#define WMPAR_SORTER_DOCUMENT_INSIDE_POCKET   5
#define WMPAR_SORTER_EXCEPTION                6
#define WMPAR_IMAGES_READY                    7
#define WMPAR_SNIPPETS_READY                  8
#define WMPAR_DOC_COMPLETED                   9
#define WMPAR_STANDBY                         10
#define WMPAR_MAGCARD                         11
#define WMPAR_SET_DOWNSTREAM_OPTIONS          12
#define WMPAR_COMPRESSED_IMG_FOR_DECISION     13

//*****************************************************************************
// Function:  GetApiRelease
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:     char* sVersion - pointer to API version destination string
//                  BYTE MaxLen - Max string len
//
//   Return Value:  TRUE - the function terminates correctly
//                  FALSE - there has been some errors 
//
//   Description:
//     Get string with API release.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetApiRelease( char* sVersion, BYTE MaxLen );


//*****************************************************************************
// Function:  GetDriverRelease
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:     char* sVersion  - pointer to Driver version destination string
//                  BYTE MaxLen     - Max string len
//
//   Return Value:  TRUE - the function terminates correctly
//                  FALSE - there has been some errors 
//
//   Description:
//     Get string with Driver release.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDriverRelease( char* sVersion, BYTE MaxLen );


//*****************************************************************************
// Function:  GetFwVersion
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters, DeviceOnLine, DeviceOffLine
//
//   State Transition:  None
//
//   Arguments:    DWORD DeviceID - Device ID
//                 char* sVersion - pointer to fw version destination variable
//                 BYTE MaxLen    - Max string len
//
//   Return Value: TRUE  - the function terminates correctly
//                 FALSE - there has been some errors
//
//   Description:
//     Returns the version of the firmware
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetFwVersion( DWORD DeviceID, char* sVersion, BYTE MaxLen );


//#############################################################################
// ERRORS MANAGEMENT
//#############################################################################
#define ERROR_MIN_STR_LEN 256

//*****************************************************************************
// Function:  GetApiError, GetApiErrorString
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:        DWORD *pdwError      - pointer to the returned error
//                     char *sErrorString   - pointer to destination string
//                     int MaxLen           - string max length (at least ERROR_MIN_STR_LEN)
//
//   Return Value: TRUE  - the function terminates correctly
//                 FALSE - there has been some errors
//
//   Description:
//     Return the Error Code or Error Code string for the last function failed. Clear last error.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetApiError( DWORD * pdwError );
VISION_API_DECL BOOL VISION_API GetApiErrorString( char *sErrorString, int MaxLen );


//*****************************************************************************
// Function:  GetUsbError, GetUsbErrorString
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:        DWORD DeviceID       - device ID
//                     DWORD *pdwError      - pointer to the returned error
//                     char *sErrorString   - pointer to destination string
//                     int MaxLen           - string max length (at least ERROR_MIN_STR_LEN)
//
//   Return Value: TRUE  - the function terminates correctly
//                 FALSE - there has been some errors
//
//   Description:
//     Return the Error Code or Error Code string for the last USB failure. Clear USB error.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetUsbError( DWORD DeviceID, DWORD *pdwError );
VISION_API_DECL BOOL VISION_API GetUsbErrorString( DWORD DeviceID, char *sErrorString, int MaxLen );


//*****************************************************************************
// Function:  GetDeviceError, GetDeviceErrorString
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:        DWORD DeviceID       - device ID
//                     DWORD *pdwError      - pointer to the returned sorter error
//                     char *sErrorString   - pointer to destination string
//                     int MaxLen           - string max length (at least ERROR_MIN_STR_LEN)
//
//   Return Value: TRUE  - the function terminates correctly
//                 FALSE - there has been some errors
//
//   Description:
//     Return the Error Code or Error Code string for the last device failure. Clear device error.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDeviceError( DWORD DeviceID, DWORD *pdwError );
VISION_API_DECL BOOL VISION_API GetDeviceErrorString( DWORD DeviceID, char *sErrorString, int MaxLen );


//*****************************************************************************
// Function:  GetSorterError, GetSorterErrorString
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID       - device ID
//                 DWORD *pdwError      - pointer to the returned sorter error
//                 char *sErrorString   - pointer to destination string
//                 int MaxLen           - string max length (at least ERROR_MIN_STR_LEN)
//
//   Return Value: TRUE  - the function terminates correctly
//                 FALSE - there has been some errors
//
//   Description:
//     Get sorter's error: Byte 3 (MSB) - Error Class
//                         Byte 2       - Peripheral
//                         Byte 1       - Error Code
//                         Byte 0 (LSB) - Jam Point
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetSorterError( DWORD DeviceID, DWORD *pdwError );
VISION_API_DECL BOOL VISION_API GetSorterErrorString( DWORD DeviceID, char *sErrorString, int MaxLen );


//*****************************************************************************
// Function:  GetSerialNumber
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:    DWORD DeviceID - Device ID
//                 char *sVersion - pointer to serial number destination variable
//                 BYTE MaxLen    - Max string len (at least 11 bytes)
//
//   Return Value: TRUE  - the function terminates correctly
//                 FALSE - there has been some errors
//
//   Description:
//     Returns the serial number of the device
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetSerialNumber( DWORD DeviceID, char *sSerialNumber, BYTE MaxLen );


//*****************************************************************************
// Function:  ReadCryptedIDCard
//
//   Method Type:      Synchronous
//
//   Valid in States:  ChangeParameters
//
//   State Transition: None
//
//   Arguments:    DWORD DeviceID - Device ID
//                 BYTE *pKey - The key data or the key file (The buffer must be equal or greater than 128 bytes)
//                 BYTE *pLen - The Key len (Input: buffer length (at least 128 bytes), output: bytes copied to the buffer )
//
//
//   Return Value: TRUE  - the function terminates correctly
//                 FALSE - there has been some errors
//
//   Description:
//     Read the crypted ID Card
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API ReadCryptedIDCard( DWORD DeviceID, BYTE *pIDCard, BYTE *pLen );

VISION_API_DECL BOOL VISION_API UpgradeIDCard( DWORD DeviceID, BYTE *pIDCardUpgrade, DWORD Len );


//*****************************************************************************
// Function:  GetIDCardDescription
//
//   Method Type:      Synchronous
//
//   Valid in States:  Offline
//
//   State Transition: None
//
//   Arguments:    DWORD DeviceID - Device ID
//                 BYTE **pDescription - A pointer to a funtion-created buffer that receive the description
//                        data. Delete the pointer after the usage with VirtualFree(...)
//                 BYTE *pLen - The description len (Input: buffer length, output: bytes copied to the buffer )
//                 DEVICES *Devices - A DEVICES structure reporting device enabled on the machine
//
//
//   Return Value: TRUE  - the function terminates correctly
//                 FALSE - there has been some errors
//
//   Description:
//     Read the ID Card description
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetIDCardDescription( DWORD DeviceID, char **pDescription, int *pLen, DEVICES *Devices );


VISION_API_DECL BOOL VISION_API SetMaxDPM( DWORD DeviceID, int Dpm );

VISION_API_DECL BOOL VISION_API GetMaxDPM( DWORD DeviceID, int *Dpm );

//*****************************************************************************
// Function:  GetAvailablePockets
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID    - Device ID
//              UCHAR *pPockets   - pointer to destination variable
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//   Description:
//     Get the available physical pockets.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetAvailablePockets( DWORD DeviceID, UCHAR *pPockets );



//*****************************************************************************
// Function:  SetMaxPocket
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID    - Device ID
//              UCHAR Pocket      - Pocket number (1,2)
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//   Description:
//     Set the available physical pocket.
//     This permits to temporary downgrade a 2 pocket Vision X to a 1 pocket device.
//     This function doesn't update the IDCARD.
//     It's for Demo purposes.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API SetMaxPocket( DWORD DeviceID, BYTE Pocket );


//*****************************************************************************
// Function:  SetHandDropDly
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID    - Device ID
//              DWORD Dly         - Delay in ms
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//   Description:
//     When the feeder is set in Hand-Drop mode this function
//     set the feeding dly between the moment when a document
//     is detected in the feeder and feeding process start.
//     Valid values for dly are from 100 to 60000 ms.
//     The internal default value is 100.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API SetHandDropDly( DWORD DeviceID, DWORD Dly );


//*****************************************************************************
// Function:  SetAGPLines
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID    - Device ID
//              BYTE  Lines       - AGP lines
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//   Description:
//    This funtion can temporary downgrade the AGP printer lines.
//    It's for Demo purposes. Valid values are defined below.
//-----------------------------------------------------------------------------
#define AGP_UNLIMITED_LINES  0
#define AGP_2_LINES          2
#define AGP_1_LINE           1
VISION_API_DECL BOOL VISION_API SetAGPLines( DWORD DeviceID, BYTE Lines );


//*****************************************************************************
// Function:  SetFeederLimit
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID    - Device ID
//              BYTE  Limit       - Feeder limitation
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//   Description:
//    This funtion can temporary downgrade the feeder.
//    It's for Demo purposes. Valid values are defined below.
//-----------------------------------------------------------------------------
#define FEEDER_FULL       0
#define FEEDER_HALF       50
#define FEEDER_SMALL      30
#define FEEDER_ONE        1
VISION_API_DECL BOOL VISION_API SetFeederLimit( DWORD DeviceID, BYTE Limit );

//#############################################################################
// PARAMETER SET/GET
//#############################################################################


//*****************************************************************************
// Function:  SetSorterParameter
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters, DeviceOffLine, DeviceOnLine
//
//   State Transition: -
//
//   Arguments:     DWORD DeviceID      - device ID
//                  WORD  ParamId       - parameter ID
//                  WORD  ParamValue    - parameter's value
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Set sorter parameter.
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API SetSorterParameter( DWORD DeviceID, WORD ParamID, WORD ParamValue );


//*****************************************************************************
// Function:  GetSorterParameter
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters, DeviceOffLine, DeviceOnLine
//
//   State Transition: -
//
//   Arguments:     DWORD DeviceID            - device ID
//                  WORD ParamID              - parameter ID
//                  AParameter *pParamData - destination pointer structure
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Get sorter's parameter.
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
typedef struct _AParameter
{
     char sDescription[35];        // Significato del parametro
     char sUnit[5];                // Unit� di misura
     USHORT usValue;               // Valore attuale del parametro
     USHORT usDefault;             // Valore di default del parametro
     USHORT usMin;                 // Minimo
     USHORT usMax;                 // Massimo
} AParameter;

VISION_API_DECL BOOL VISION_API GetSorterParameter( DWORD DeviceID, WORD ParamID,  AParameter *pParamData );


//*****************************************************************************
// Function:  SetDeviceParameters
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: -
//
//   Arguments:     DWORD DeviceID                  - device ID
//                  DeviceParameters  DeviceParam   - structure containing Device parameters
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Set Device parameters.
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API SetDeviceParameters( DWORD DeviceID, DeviceParameters  DeviceParam );


//*****************************************************************************
// Function:  SetDeviceParameters
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID                  - device ID
//                 DeviceParameters  *DeviceParam  - structure receiving Device parameters
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Get actual Device parameters.
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDeviceParameters( DWORD DeviceID, DeviceParameters  *DeviceParam );


//#############################################################################
//#############################################################################
// DEVICE STATES CONTROL FUNCTIONS
//#############################################################################
//#############################################################################


//#############################################################################
// GET DEVICE STATE
//#############################################################################


//*****************************************************************************
// Function:  GetDeviceState, GetDeviceStateString
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:        DWORD DeviceID             - Device ID
//                     DWORD *pDeviceState        - pointer to state destination variable
//                     char  *sDeviceStateString  - pointer to string state destination variable
//                     int MaxLen                 - Max string len
//
//   Return Value:     TRUE  - if the function terminates correctly
//                     FALSE - if there has been some errors
//
//   Description:
//     Returns the current state of the API Device as a DWORD or as a string.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDeviceState( DWORD DeviceID, DWORD *pdwDeviceState );
VISION_API_DECL BOOL VISION_API GetDeviceStateString( DWORD DeviceID, char *sDeviceStateString, int MaxLen );


//#############################################################################
// STATE CHANGE
//#############################################################################


//*****************************************************************************
// Function:  StartUp
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceShutDown
//
//   State Transition: DeviceStartingUp, DeviceChangeParameters
//
//   Arguments:        HWND Handle        - handle to the application's destination window
//                     UINT SorterMessage - user message defined inside application
//
//   Return Value:     DeviceID - should be used on all future commands if the function 
//                                terminates correctly.
//                     0        - if there has been some errors. It is possible to read the 
//                                error code by using the SorterGetLastError() function
//
//   Description: 
//     Opens a communication channel with the sorter and defines.
//     The dll starts an internal thread that manages the connection. 
//     When the status of either the sorter or the connection changes, it sends 
//     the corresponding message to application's window.
//-----------------------------------------------------------------------------
VISION_API_DECL DWORD VISION_API StartUp( HWND Handle, UINT SorterMessage );


//*****************************************************************************
// Function:  ShutDown
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters, DeviceOnLine, DeviceStandBy
//
//   State Transition: DeviceShutDown
//
//   Arguments:        DWORD DeviceID - identifier returned by StartUp function
//
//   Return Value: TRUE  - if the function terminates correctly
//                 FALSE - if there has been some errors. It is possible to read the error 
//                         code by using the SorterGetLastError() function
//
//   Description:
//     Closes the interface channel for the device identified by the parameter DeviceID.
//     Frees all memory and terminates the polling thread.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API ShutDown( DWORD DeviceID );


//*****************************************************************************
// Function:  OnLine
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: DeviceOnLine
//
//   Arguments:        DWORD DeviceID - identifier returned by StartUp function
//
//   Return Value: TRUE  - if the function terminates correctly
//                 FALSE - if there has been some errors. It is possible to read the error 
//                         code by using the SorterGetLastError() function
//
//   Description:
//     Change the state to DeviceOnLine for the device identified by the parameter DeviceID.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API OnLine( DWORD DeviceID );


//*****************************************************************************
// Function:  ChangeParameters
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOffLine, DeviceOnLine
//
//   State Transition: DeviceChangeParameters
//
//   Arguments:        DWORD DeviceID - identifier returned by StartUp function
//
//   Return Value: TRUE  - if the function terminates correctly
//                 FALSE - if there has been some errors. It is possible to read the error 
//                         code by using the SorterGetLastError() function
//
//   Description:
//     Change the state to DeviceChangeParameters for the device identified by the parameter DeviceID.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API ChangeParameters( DWORD DeviceID );


//#############################################################################
//#############################################################################
// ON-LINE FUNCTIONS
//#############################################################################
//#############################################################################

//*****************************************************************************
// Function:  IsFeederEmpty
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOnline
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID    - Device ID
//              BOOL  pFlag       - A pointer to a BOOL flag
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//   Description:
//    This funtion return the status of the feeder sensor in order
//    to detect if the feeder is empty.
//    If pFlag is returned TRUE it means the feeder doesn't contain documents.
//    Otherwise one or more docs are present in the feeder.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API IsFeederEmpty( DWORD DeviceID, BOOL *pFlag );


//*****************************************************************************
// Function:  IsTrackFree
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOnline
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID    - Device ID
//              BOOL  pFlag       - A pointer to a BOOL flag
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//   Description:
//    This funtion return the status of the track.
//    If all the sensor are free it means that track is free and a new job is
//    allowed
//    If pFlag is returned TRUE it means the is free.
//    Otherwise one or more sensor are busy due to a document in the track.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API IsTrackFree( DWORD DeviceID, BOOL *pFlag );


//#############################################################################
// FEEDING
//#############################################################################


//*****************************************************************************
// Function:  StartFeeding
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOnLine
//
//   State Transition: DeviceFeeding
//
//   Arguments:     DWORD DeviceID - device ID
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API StartFeeding( DWORD DeviceID );



//*****************************************************************************
// Function:  StopFeeding
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding
//
//   State Transition: DeviceOnLine
//
//   Arguments:     DWORD DeviceID - device ID
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Stop document feeding.
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API StopFeeding( DWORD DeviceID );



//*****************************************************************************
// Function:  FreeTrack
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOnLine
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID       - device ID
//                  BYTE ucPocket        - destination pocket
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Free Track command. 
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API FreeTrack( DWORD DeviceID, BYTE Pocket );


//*****************************************************************************
// Function:  SetPocket
//
//   Method Type:      Asynchronous
//
//   Valid in States:  DeviceFeeding (SetItemOutput event)
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID      - device ID
//                  DWORD DocID         - document identification number
//                  BYTE Pocket         - destination pocket
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Set document destination pocket.
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API SetPocket( DWORD DeviceID, DWORD DocID, BYTE Pocket );


//*****************************************************************************
// Function:  GetDocumentLength
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID        - identifier returned by the SorterApiStartUp function
//              DWORD DocID           - document ID
//              DWORD *pdwDocLength   - pointer to destination variable
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors. It is possible to read the error 
//                         code by using the SorterGetLastError() function
//   Description:
//     Get the document length expressed in mm.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDocumentLength( DWORD DeviceID, DWORD DocID, DWORD *pdwDocLength );


//*****************************************************************************
// Function:  GetMicrCodeline
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding (during MicrAvailable event)
//
//   State Transition: None
//
//   Arguments:   DWORD DeviceID      - device ID
//                char* sMicrResult   - destination data buffer
//                DWORD MaxLen        - destination data buffer length
//
//   Return Value: TRUE  - read done
//                 FALSE - read failure
//
//   Description:
//     Read MICR codeline from device.
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetMicrCodeline( DWORD DeviceID, char* sMicrResult, DWORD MaxLen );


//*****************************************************************************
// Function:  GetDocumentType
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID       - identifier returned by the SorterApiStartUp function
//              DWORD DocID          - document ID
//              DWORD *DocType       - pointer to the destination variable
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//
//   Description:
//     Get the document type
//     This function is supported only by the I-Deal device
//-----------------------------------------------------------------------------
#define DOC_TYPE_CHEQUE   0
#define DOC_TYPE_CARD     1
VISION_API_DECL BOOL VISION_API GetDocumentType( DWORD DeviceID, DWORD DocID, DWORD *DocType );


//*****************************************************************************
// Function:  EnableFranking
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding (during the WMPAR_SET_DOWNSTREAM_OPTIONS message)
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID              - identifier returned by the SorterApiStartUp function
//              DWORD TrailingEdgeAlignment - Leading or trailing edge alignment (FALSE: leading edge, TRUE: trailing edge)
//              DWORD StartPosition         - Distance in mm from the alignment edge
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//
//   Description:
//     Enable the franking operation
//     This function is supported only by the I-Deal device
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API EnableFranking( DWORD DeviceID, DWORD TrailingEdgeAlignment, DWORD StartPosition );


//*****************************************************************************
// Function:  GetMagCardResult
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOnLine (during MagCard event)
//
//   State Transition: None
//
//   Arguments:   DWORD DeviceID      - device ID
//                PCHAR sResult       - destination data buffer (user allocated)
//                DWORD BufferLen     - destination data buffer length
//
//   Return Value: TRUE  - successful
//                 FALSE - failed
//
//   Description:
//     Get the MagCard result during the MagCard message.
//     The destination buffer have to be large enough to contain the result.
//     If the buffer length is not enough the function fails and doesn't return
//     any data. We suggest to allocate a buffer of MAGCARD_MIN_BUFFER_LEN bytes.
//-----------------------------------------------------------------------------
#define MAGCARD_ERROR_NONE      0
#define MAGCARD_ERROR_BASE      MAGCARD_ERROR_NONE
#define MAGCARD_ERROR_PARITY    (MAGCARD_ERROR_BASE+1)
#define MAGCARD_ERROR_STANDARD  (MAGCARD_ERROR_BASE+2)
#define MAGCARD_ERROR_DIRECTION (MAGCARD_ERROR_BASE+3)

#define MAGCARD_MIN_BUFFER_LEN  (512)
VISION_API_DECL BOOL VISION_API GetMagCardResult( DWORD DeviceID, PCHAR sResult, DWORD BufferLen );


//*****************************************************************************
// Function:  GetOCRCodeline
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding (during SnippetReady event)
//
//   State Transition: None
//
//   Arguments:   DWORD DeviceID      - device ID
//                BYTE* pBmp          - bitmap buffer pointer
//                char* sDestString   - destination data buffer
//                DWORD StringLen     - destination data buffer length
//                int Font            - OCR Font
//                int Threshold       - OCR Reject Threshold
//
//   Return Value: TRUE  - read done
//                 FALSE - read failure
//
//   Description:
//     Read OCR codeline or Barcode from device.
//     If error set the ErrorCode field with a proper value.
//-----------------------------------------------------------------------------
// OCR Engine 1 fonts
#define OCR1_OCRA           0x0001  // OCR-A Euro limited
#define OCR1_OCRB           0x0002  // OCR-B Euro limited
#define OCR1_OCRAB          0x0003  // OCR-A and OCR-B Euro limited
#define OCR1_OCRBUK         0x0004  // OCR-B extended for UK banking
#define OCR1_E13BO          0x0005  // E13B optical
#define OCR1_E13BOXOCRBUK   0x0006  // E13B + OCRB for UK, switched on 'X'
#define OCR1_OCRAALNUM      0x0007  // OCR A alphanumeric
#define OCR1_OCRBALNUM      0x0008  // OCR B alphanumeric
#define OCR1_OCRB1403       0x0009  // OCRB 1403M
#define OCR1_OCRAN          0x000A  // OCRA numeric for use with OCRB1403

#define OCR1_BC128          0x0010  // Barcode 1D Code 128
#define OCR1_BC39           0x0011  // Barcode 1D Code 39
#define OCR1_BC2OF5         0x0012  // Barcode 1D Interleaved 2 of 5
#define OCR1_BCUPCA         0x0013  // Barcode 1D UPCA
#define OCR1_BCEAN13        0x0014  // Barcode 1D EAN13
#define OCR1_BCUPCE         0x0015  // Barcode 1D UPCE
#define OCR1_BCEAN8         0x0016  // Barcode 1D EAN8

#define OCR1_BCPDF417       0x0017  // Barcode 2D PDF417

#define OCR1_CMC7O          0x0018  // CMC7 Optical

// VisionS
#define OCR1_BCCODABAR      0x0019  // Barcode 1D CODA BAR
#define OCR1_BCUCCEAN128    0x001A  // Barcode 1D UCCEAN_128
#define OCR1_BCAUTO         0x001B  // Barcode 1D Auto Recognition


VISION_API_DECL BOOL VISION_API GetOCRCodeline(DWORD DeviceID, BYTE* pBmp, char* sOCRResult, DWORD StringLen, int Font, int Threshold);


//*****************************************************************************
// Function:  SendPrinterData
//
//   Valid in States:  DeviceFeeding (during SetItemOutput event)
//                     DeviceOnLine (before StartFeeding)
//
//   State Transition: None
//
//   Arguments:    DWORD DeviceID     - device ID
//                 DWORD Head         - head (for Vision X always 0)
//                 LOGFONT Font       - Font descriptor
//                 char *sText        - String
//                 DWORD TextPosition - Text position in mm
//                 char *sImgPath     - Path or memory pointer to a B/W bitmap image
//                                      The memory pointer is a DIB
//                 DWORD ImgPosition  - Image position in mm
//                 DWORD ImgSrcType   - Source of the image (Memory pointer or file)
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//     Create the inkjet buffer with the specified font and send it through bulk output pipe.
//     If error set the ErrorCode field with a proper value.
//
//     This function must be called, if Printer is enbaled, before Start for the first
//     document and during NewDocument event for the next document.For 30/60 Dpm can be called 
//     in the SetItemOutput, instead of the New Document. No other combination
//     are permitted.
//
//     Notice: For the VX AGP Printer, with 2 lines enabled, is possible to set
//             the vertcal position. If the Text is empty (NULL or zero length string) the 
//             highest bytes of the dwImgOffset parameters can be used to set the vertical
//             position in pixels. The range is form 0 to 49.
//             THIS OPTION IS AVAILABLE ONLY FOR THE 2 LINES AGP PRINTER WHEN NO TEXT
//             IS PRINTED.
//             For the 2 lines printer the BMP height must be 50 pixels.
//             For the 4 lines printer the BMP height must be 100 pixels.
//             Example:
//             DWORD dwImgPos = 10;   // Horizontal position in mm
//             DWORD dwVertPos = 20;  // Vertical position in pixels
//             dwImgPos |= (dwVertPos<<16);
//             SendPrinterData( DevID, 0, lf, "", 0, "Stamp.bmp", dwImgPos, PRT_SRC_FILE );
//-----------------------------------------------------------------------------
#define PRT_SRC_FILE    0
#define PRT_SRC_MEM_PTR  1

VISION_API_DECL BOOL VISION_API SendPrinterData( DWORD DeviceID, DWORD Head, LOGFONTA Font, char *sText, DWORD TextPosition, char *sImgPath, DWORD ImgPosition, DWORD ImgSrcType);

/*********************************************************************
* STRUCT FOR IMAGES. You receive it during WMPAR_IMAGES_READY
* or WMPAR_SNIPPETS_READY
*********************************************************************/
typedef struct _CompressedImage
{
  BYTE *pBuffer;
  int BufferLen;
}
CompressedImage;

typedef struct _ImagesStruct
{
  DWORD DocNumber;
  CompressedImage *Images;
  BOOL SnippetFront;
}
ImagesStruct;


//*****************************************************************************
// Function:  FreeImagesBuffer
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:     ImagesStruct *pImageStruct - pointer to the structure received from API during
//                                              ImageReady event
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//        Free the compressed images buffers
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API FreeImagesBuffer(ImagesStruct *pImageStruct);


//*****************************************************************************
// Function:  FreeSnippetBuffer
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:     ImagesStruct *pImageStruct - pointer to the structure received from API during
//                                              ImageReady event
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//        Free the snippets buffers
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API FreeSnippetBuffer(ImagesStruct *pImageStruct);


//*****************************************************************************
// Function:  SetImageAdjustment
//
//   Valid in States: DeviceOnLine
//                    DeviceChangeParameters
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID     - device ID
//                  int   Contrast     - Range is -100 to 100
//                  int   Brightness   - Range is -100 to 100
//                  BOOL  Front        - TRUE is Front , FALSE is Rear
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//   Set the image color adjustment parameters.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API SetImageAdjustment(DWORD DeviceID, int Contrast, int Brightness, BOOL Front);  


//*****************************************************************************
// Function:  Rs232SetBaud
//
//   Valid in States: DeviceChangeParameters
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID     - device ID
//                 UINT  Baudrate     - Baudarate ( up to 57600)
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//   Set the VX serial port baudrate. VisionX always uses 8 data bit, 1 stop bit
//   and no parity.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API Rs232SetBaud( DWORD DeviceID, UINT Baudrate );


//*****************************************************************************
// Function:  Rs232Write
//
//   Valid in States: DeviceOnLine
//                    DeviceFeeding
//
//   State Transition: None
//
//   Arguments:    DWORD DeviceID     - device ID
//                 BYTE  *pBuffer     - Buffer Data
//                 BYTE  Len          - Buffer Length (1 to 255)
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//   Send pBuffer data through the VX serial port
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API Rs232Write( DWORD DeviceID, BYTE* pBuffer, BYTE Len );


//*****************************************************************************
// Function:  Rs232GetLen
//
//   Valid in States: DeviceOnLine
//                    DeviceFeeding
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID     - device ID
//                  BYTE  *pLen        - Bytes returned (0 to 255)
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//   Read the bytes received through the Vision X serial port
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API Rs232GetLen( DWORD DeviceID, BYTE* pLen );


//*****************************************************************************
// Function:  Rs232Read
//
//   Valid in States: DeviceOnLine
//                    DeviceFeeding
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID     - device ID
//                 BYTE  *pBuffer     - Buffer Data
//                 BYTE  Len          - Buffer Length (1 to 255).
//                                      Usually <= Rs232GetLen returned value.
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//   Read in pBuffer the data received through the Vision X serial port
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API Rs232Read( DWORD DeviceID, BYTE* pBuffer, BYTE Len );


//*****************************************************************************
// Function:  ReadPrinterDropsCounter
//
//   Valid in States: DeviceOnLine
//
//   State Transition: None
//
//   Arguments:    DWORD DeviceID           - device ID
//                 DWORD  *pdwDropsCounter  - Destination drops counter
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//    Read the printer drops counter. This value is used to estimate the inkjet
//    cartridge consumption.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API ReadPrinterDropsCounter( DWORD DeviceID, DWORD* pdwDropsCounter );


//*****************************************************************************
// Function:  ResetPrinterDropsCounter
//
//   Valid in States: DeviceOnLine
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID     - device ID
//                                      Usually <= Rs232GetLen returned value.
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//    Reset the drops counter. To be used during cartridge replacement
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API ResetPrinterDropsCounter( DWORD DeviceID );


//*****************************************************************************
// Function:  GetHistoricalCounter
//
//   Valid in States: DeviceOnLine
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID      - Device ID
//                  DWORD CounterID     - The counter ID
//                  DWORD *pValue       - Receive the current counter value
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//     This function returns the historical counters value such as processed docs,
//     franked documents...
//-----------------------------------------------------------------------------

#define COUNTER_BASE           (0)

#define COUNTER_DOCS            (COUNTER_BASE      )
#define COUNTER_CARDS           (COUNTER_DOCS     +1)
#define COUNTER_FRANKING        (COUNTER_CARDS    +1)

#define COUNTER_FIRST           (COUNTER_DOCS)
#define COUNTER_LAST            (COUNTER_FRANKING)
#define COUNTER_VALUES          ((COUNTER_LAST-COUNTER_FIRST)+1)

VISION_API_DECL BOOL VISION_API GetHistoricalCounter( DWORD DeviceID, DWORD CounterID, DWORD *pValue );


//*****************************************************************************
// Function:  ResetHistoricalCounter
//
//   Valid in States: DeviceOnLine
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID      - Device ID
//                  DWORD CounterID     - The counter ID
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//     This function reset the historical counters value regarding consumable parts
//     such as franking roller.
//     Available counters ID are:
//     - COUNTER_FRANKING
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API ResetHistoricalCounter( DWORD DeviceID, DWORD CounterID );


//*****************************************************************************
// Function:  GetPrinterCartridgeInfo
//
//   Valid in States: DeviceOnLine
//
//   State Transition: None
//
//   Arguments:     DWORD DeviceID      - Device ID
//                  BOOL  *pInserted    - Receive the cartridge status.
//                                        TRUE means cartridge inserted in the nest
//                                        FALSE means cartridge non present
//                  DWORD *pCartridgeID - Receive the cartridge ID number
//                                        This value is valid only for the AGP printer.
//                                        When an AGP is inserted this parameter will receive
//                                        a number greater than 0.
//                                      
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//    This function is available for Vision X Plus device.
//    Read the printer cartridge info. The "Inserted" flag signal the presence
//    of the cartridge in the nest. This flag is valid for the single line
//    an the AGP printer.
//    The "CartridgeID" receive the Identification number of a an AGP printer
//    This value can be used to detect a cartridge replacement.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetPrinterCartridgeInfo( DWORD DeviceID, BOOL *pInserted, DWORD *pCartridgeID );


//*****************************************************************************
// Function:  GetDeviceFeature
//
//  Valid in States:  DeviceChangeParameters
//                    DeviceOnLine
//
//  State Transition: None
//
//  Arguments:    DWORD DeviceID      - Device ID
//                DWORD FeatureID     - Select the feature ID
//                DWORD *lpReturnedBuffer - A pointer to the user allocated buffer
//                DWORD BufferSize    - The buffer data size
//
//  Return Value: TRUE  - done
//                FALSE - failed
//
//  Description:
//    Get the device features, capabilities and options
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDeviceFeature( DWORD DeviceID, DWORD FeatureID, LPVOID lpReturnedBuffer, DWORD BufferSize );

// Device features ID definitions
#define DEVICE_FEATURE_BASE                   (0)

#define DEVICE_FEATURE_DEVICE_TYPE              (DEVICE_FEATURE_BASE)
#define DEVICE_FEATURE_SN                       (DEVICE_FEATURE_DEVICE_TYPE+1)
#define DEVICE_FEATURE_SN_IDCARD                (DEVICE_FEATURE_SN+1)
#define DEVICE_FEATURE_EXTERNAL_IDCARD          (DEVICE_FEATURE_SN_IDCARD+1)
#define DEVICE_FEATURE_SN_EXTERNAL_IDCARD       (DEVICE_FEATURE_EXTERNAL_IDCARD+1)
#define DEVICE_FEATURE_DPM                      (DEVICE_FEATURE_SN_EXTERNAL_IDCARD+1)
#define DEVICE_FEATURE_FEEDER_LIMIT             (DEVICE_FEATURE_DPM+1)
#define DEVICE_FEATURE_FEEDER_SD                (DEVICE_FEATURE_FEEDER_LIMIT+1)
#define DEVICE_FEATURE_MICR_E13B                (DEVICE_FEATURE_FEEDER_SD+1)
#define DEVICE_FEATURE_MICR_CMC7                (DEVICE_FEATURE_MICR_E13B+1)
#define DEVICE_FEATURE_MICR_AUTO                (DEVICE_FEATURE_MICR_CMC7+1)
#define DEVICE_FEATURE_MICR_OCR_E13B            (DEVICE_FEATURE_MICR_AUTO+1)
#define DEVICE_FEATURE_MICR_OCR_CMC7            (DEVICE_FEATURE_MICR_OCR_E13B+1)
#define DEVICE_FEATURE_PRINTER_TYPE             (DEVICE_FEATURE_MICR_OCR_CMC7+1)
#define DEVICE_FEATURE_PRINTER_LINES            (DEVICE_FEATURE_PRINTER_TYPE+1)
#define DEVICE_FEATURE_PRINTER_BITMAP           (DEVICE_FEATURE_PRINTER_LINES+1)
#define DEVICE_FEATURE_PRINTER_BITMAP_HEIGHT    (DEVICE_FEATURE_PRINTER_BITMAP+1)

#define DEVICE_FEATURE_IMAGE_FRONT              (DEVICE_FEATURE_PRINTER_BITMAP_HEIGHT+1)
#define DEVICE_FEATURE_IMAGE_FRONT_FAST_COLOR   (DEVICE_FEATURE_IMAGE_FRONT+1)
#define DEVICE_FEATURE_IMAGE_FRONT_TRUE_COLOR   (DEVICE_FEATURE_IMAGE_FRONT_FAST_COLOR+1)
#define DEVICE_FEATURE_IMAGE_FRONT_FAST_DROPOUT (DEVICE_FEATURE_IMAGE_FRONT_TRUE_COLOR+1)
#define DEVICE_FEATURE_IMAGE_FRONT_DPI          (DEVICE_FEATURE_IMAGE_FRONT_FAST_DROPOUT+1)
#define DEVICE_FEATURE_IMAGE_FRONT_FAST_DROPOUT_IR  (DEVICE_FEATURE_IMAGE_FRONT_DPI+1)

#define DEVICE_FEATURE_IMAGE_REAR               (DEVICE_FEATURE_IMAGE_FRONT_FAST_DROPOUT_IR+1)
#define DEVICE_FEATURE_IMAGE_REAR_FAST_COLOR    (DEVICE_FEATURE_IMAGE_REAR+1)
#define DEVICE_FEATURE_IMAGE_REAR_TRUE_COLOR    (DEVICE_FEATURE_IMAGE_REAR_FAST_COLOR+1)
#define DEVICE_FEATURE_IMAGE_REAR_FAST_DROPOUT  (DEVICE_FEATURE_IMAGE_REAR_TRUE_COLOR+1)
#define DEVICE_FEATURE_IMAGE_REAR_DPI           (DEVICE_FEATURE_IMAGE_REAR_FAST_DROPOUT+1)
#define DEVICE_FEATURE_IMAGE_REAR_FAST_DROPOUT_IR   (DEVICE_FEATURE_IMAGE_REAR_DPI+1)

#define DEVICE_FEATURE_OCR_AB                   (DEVICE_FEATURE_IMAGE_REAR_FAST_DROPOUT_IR+1)
#define DEVICE_FEATURE_OCR_MICR                 (DEVICE_FEATURE_OCR_AB+1)
#define DEVICE_FEATURE_OCR_BARCODE_1D           (DEVICE_FEATURE_OCR_MICR+1)
#define DEVICE_FEATURE_OCR_BARCODE_2D           (DEVICE_FEATURE_OCR_BARCODE_1D+1)
#define DEVICE_FEATURE_IQA                      (DEVICE_FEATURE_OCR_BARCODE_2D+1)

#define DEVICE_FEATURE_POCKETS                  (DEVICE_FEATURE_IQA+1)

#define DEVICE_FEATURE_ROHS_COMPLIANT           (DEVICE_FEATURE_POCKETS+1)

#define DEVICE_FEATURE_SMART_JET                (DEVICE_FEATURE_ROHS_COMPLIANT+1)

#define DEVICE_FEATURE_MAGCARD                  (DEVICE_FEATURE_SMART_JET+1)

#define DEVICE_FEATURE_OCR_ENGINE               (DEVICE_FEATURE_MAGCARD+1)
#define DEVICE_FEATURE_BARCODE_1D_ENGINE        (DEVICE_FEATURE_OCR_ENGINE+1)
#define DEVICE_FEATURE_BARCODE_2D_ENGINE        (DEVICE_FEATURE_BARCODE_1D_ENGINE+1)
#define DEVICE_FEATURE_MICR_OCR_ENGINE          (DEVICE_FEATURE_BARCODE_2D_ENGINE+1)

#define DEVICE_FEATURE_ULTRASONIC_DFD           (DEVICE_FEATURE_MICR_OCR_ENGINE+1)

#define DEVICE_FEATURE_CARDS                    (DEVICE_FEATURE_ULTRASONIC_DFD+1)

#define DEVICE_FEATURE_FRANKING                 (DEVICE_FEATURE_CARDS+1)

#define DEVICE_FEATURE_FRANKING_LENGTH          (DEVICE_FEATURE_FRANKING+1)

#define DEVICE_FEATURE_LAST                     (DEVICE_FEATURE_FRANKING_LENGTH)

// Device types definitions (DEVICE_FEATURE_DEVICE_TYPE)
#define DEVICE_MYVISIONX  0
#define DEVICE_VISIONX    1
#define DEVICE_VISIONS    2
#define DEVICE_IDEAL      3

// Device SN definitions
#define DEVICE_SN_BUFFER_LEN  11

// Device Printer types definitions (DEVICE_FEATURE_PRINTER_TYPE)
#define DEVICE_PRINTER_DISABLED     0
#define DEVICE_PRINTER_SINGLE_LINE  1
#define DEVICE_PRINTER_AGP          2

// Device features boolean
#define DEVICE_FEATURE_NO   0
#define DEVICE_FEATURE_YES  1


//*****************************************************************************
// Function:  DisableDownStreamImage
//
//   Valid in States:  DeviceFeeding (during SetItemOutput event)
//                     DeviceOnLine (before StartFeeding)
//
//   State Transition: None
//
//   Arguments:    DWORD DeviceID     - Device ID
//                 DWORD DocID        - Document ID
//
//   Return Value: TRUE  - done
//                 FALSE - failure
//
//   Description:
//     This function disable the downstream image capture
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API DisableDownStreamImage( DWORD DeviceID, DWORD DocID );


//*****************************************************************************
// Function:  GetFullPocketStatus
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOnline, DeviceOffline, DeviceFeeding
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID  - Device ID
//              DWORD *pStatus  - pointer to destination buffer.
//                                Must be a 2 DWORD length array.
//              DWORD bWarning  - Select the warning or the error detection.
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//   Description:
//     Get the status of the Full Pocket detection. LSb of the 1st DWORD is
//     Pocket 1. MSb is Pocket 32.
//     LSb of the 2nb DWORD is Pocket 33: Msb is Pocket 64.
//     1 means Full Pocket detected.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetFullPocketStatus( DWORD DeviceID, DWORD *pStatus, BOOL bWarning );


//*****************************************************************************
// Function:  GetDoubleFeedingResult
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding
//
//   State Transition: None
//
//   Arguments: DWORD DeviceID       - Device ID
//              DWORD dwDocId        - Document ID
//              DWORD *pbDetected    - Pointer to destination variable
//
//   Return Value: - TRUE  if the function terminates correctly
//                 - FALSE if there has been some errors.
//
//   Description:
//     Get the double feeding detection status for the document.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDoubleFeedingResult( DWORD DeviceID, DWORD dwDocID, BOOL *pbDetected );


//*****************************************************************************
// Function:  SetFeedingMode
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID       - device ID
//                 UCHAR Mode           - Feeding Mode (START/STOP or FLOW MODE)
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Set the Feeding Mode.
//-----------------------------------------------------------------------------

// FEEDING MODE
#define FEEDING_STARTSTOP 0
#define FEEDING_FLOWMODE  1

VISION_API_DECL BOOL VISION_API SetFeedingMode( DWORD DeviceID, BYTE Mode );



//##########################################################################################
// VIRTUAL ENDORSEMENT FUNCTIONS
//##########################################################################################

//*****************************************************************************
// Function:  GetVirtualEndorsementAreaProperty
//
//  Valid in States: DeviceChangeParameters
//                   DeviceOnLine
//
//  State Transition: None
//
//  Arguments:    DWORD DeviceID      - Device ID
//                DWORD AreaID        - Select the Area (see the defines)
//                DWORD ParamID       - Select the Param ID (see the defines)
//                LPVOID pData        - the pointer to a user allocated buffer that receives the data
//
//  Return Value: TRUE  - done
//                FALSE - failure
//
//  Description:
//    Get an area property values. LPVOID represents different data types
//    depending on the PropID (DWORD, LOGFONT, COLORREF, RECT)
//-----------------------------------------------------------------------------
__declspec(dllexport) BOOL WINAPI GetVirtualEndorsementAreaProperty( DWORD DeviceID, DWORD AreaID, DWORD PropID, LPVOID pData );


//*****************************************************************************
// Function:  SetVirtualEndorsementAreaProperty
//
//  Valid in States: DeviceChangeParameters
//                   DeviceOnLine
//
//  State Transition: None
//
//  Arguments:    DWORD DeviceID      - Device ID
//                DWORD AreaID        - Select the Area (see the defines)
//                DWORD ParamID       - Select the Param ID (see the defines)
//                LPVOID pData        - the pointer to a user allocated buffer that contains the data
//
//  Return Value: TRUE  - done
//                FALSE - failure
//
//  Description:
//    Set an area property values. LPVOID represents different data types
//    depending on the PropID (DWORD, LOGFONT, COLORREF, RECT)
//-----------------------------------------------------------------------------
__declspec(dllexport) BOOL WINAPI SetVirtualEndorsementAreaProperty( DWORD DeviceID, DWORD AreaID, DWORD PropID, LPVOID pData );


//*****************************************************************************
// Function:  SetVirtualEndorsement
//
//  Valid in States: DeviceFeeding
//                   DeviceOnLine
//
//  State Transition: None
//
//  Arguments:    DWORD DeviceID      - Device ID
//                DWORD AreaID        - Select the Area (see the defines)
//                char  *sText        - the pointer to a user allocated string that contains the text
//                char  *sBmpPath     - the pointer to a user allocated string that contains the bitmap path
//                                      or the pointer to a user allocated buffer that contains a DIB
//                DWORD BmpSrcType    - sets the sBmpPath type (File path)
//
//  Return Value: TRUE  - done
//                FALSE - failure
//
//  Description:
//    Set the data to "virtual" print on a document. The programmer can call
//    this function multiple times to set printing data on different areas.
//    This function has to be called when the VirtualEndorsement is enabled.
//-----------------------------------------------------------------------------
__declspec(dllexport) BOOL WINAPI SetVirtualEndorsement( DWORD DeviceID, DWORD AreaID, char *sText, char *sBmpPath, DWORD BmpSrcType );


//
// Virtual endorsement areas ID definitions
//
#define ENDORSEMENT_AREAS         (8)

#define ENDORSEMENT_AREA_BASE     (0)
#define ENDORSEMENT_AREA_PAYEE    (ENDORSEMENT_AREA_BASE)
#define ENDORSEMENT_AREA_BOFD     (ENDORSEMENT_AREA_PAYEE   + 1)
#define ENDORSEMENT_AREA_TRANSIT  (ENDORSEMENT_AREA_BOFD    + 1)
#define ENDORSEMENT_AREA_CUSTOM1  (ENDORSEMENT_AREA_TRANSIT + 1)
#define ENDORSEMENT_AREA_CUSTOM2  (ENDORSEMENT_AREA_CUSTOM1 + 1)
#define ENDORSEMENT_AREA_CUSTOM3  (ENDORSEMENT_AREA_CUSTOM2 + 1)
#define ENDORSEMENT_AREA_CUSTOM4  (ENDORSEMENT_AREA_CUSTOM3 + 1)
#define ENDORSEMENT_AREA_CUSTOM5  (ENDORSEMENT_AREA_CUSTOM4 + 1)

#define ENDORSEMENT_AREA_LAST     (ENDORSEMENT_AREA_CUSTOM5)

//
// Virtual endorsement params ID definitions
//
#define ENDORSEMENT_PARAM_BASE      (0)

#define ENDORSEMENT_PARAM_RECT      (ENDORSEMENT_PARAM_BASE)
#define ENDORSEMENT_PARAM_UNIT      (ENDORSEMENT_PARAM_RECT   + 1)
#define ENDORSEMENT_PARAM_FONT      (ENDORSEMENT_PARAM_UNIT   + 1)
#define ENDORSEMENT_PARAM_COLOR     (ENDORSEMENT_PARAM_FONT   + 1)
#define ENDORSEMENT_PARAM_FLAGS     (ENDORSEMENT_PARAM_COLOR  + 1)
#define ENDORSEMENT_PARAM_ROTATION  (ENDORSEMENT_PARAM_FLAGS  + 1)

#define ENDORSEMENT_PARAM_LAST      (ENDORSEMENT_PARAM_ROTATION)
#define ENDORSEMENT_PARAMS          (6)

//
// Virtual endorsement area units definitions
//
#define ENDORSEMENT_UNITS         (2)

#define ENDORSEMENT_UNIT_MM       (0)
#define ENDORSEMENT_UNIT_MILS     (1)

#define ENDORSEMENT_UNIT_LAST     (ENDORSEMENT_UNIT_MILS)

//
// Virtual endorsement area flags definitions
// - Text alignment
// - Word wrap
#define ENDORSEMENT_FLAG_LEFT       (0x00000001)
#define ENDORSEMENT_FLAG_CENTER     (0x00000002)
#define ENDORSEMENT_FLAG_RIGHT      (0x00000004)
#define ENDORSEMENT_FLAG_WORD_WRAP  (0x00000008)

#define ENDORSEMENT_FLAGS_MASK      (~(ENDORSEMENT_FLAG_LEFT|ENDORSEMENT_FLAG_CENTER|ENDORSEMENT_FLAG_RIGHT|ENDORSEMENT_FLAG_WORD_WRAP))


//
// Virtual endorsement area text rotation angle definitions (counterclockwise)
//
#define ENDORSEMENT_ROTATE_0    (0)
#define ENDORSEMENT_ROTATE_90   (1)
#define ENDORSEMENT_ROTATE_180  (2)
#define ENDORSEMENT_ROTATE_270  (3)

#define ENDORSEMENT_ROTATE_LAST (ENDORSEMENT_ROTATE_270)

//
// Virtual endorsement bmp type definitions
//
#define ENDORSEMENT_BMP_FILE    (0)

#define ENDORSEMENT_BMP_LAST    (ENDORSEMENT_BMP_FILE)


//*****************************************************************************
// Function:  SetIQAParameter
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceChangeParameters
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID       - Device ID
//                 DWORD ImageNumber    - 1st Front image or 2nd Front image or 1st Rear Image or 2nd Rear image
//                 DWORD Test           - Test type to be performed on an image
//                 DWORD Parameter      - Select the IQA test parameter
//                 int   Value          - Input of the parameter value
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Set an IQA parameter value of a certain image
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API SetIQAParameter( DWORD DeviceID, UINT ImageNumber, DWORD Test, DWORD Parameter, int Value );

//*****************************************************************************
// Function:  GetIQAResults
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID       - Device ID
//                 DWORD DocID          - Document ID
//                 DWORD ImageNumber    - 1st Front image or 2nd Front image or 1st Rear Image or 2nd Rear image
//                 DWORD Test           - Test result selector
//                 int  *Value          - Receive the test result value
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//     Return the result of an IQA test
//     Each test returns one the following results:
//     -	0 : the condition has not been tested;
//     -	1 : the condition is tested, and the defect is present;
//     -	2 : the condition is tested, and the defect is not present.
//
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetIQAResults( DWORD DeviceID, DWORD DocID, UINT ImageNumber, DWORD Test, int* Value );

//*****************************************************************************
// Function:  GetIQAValues
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceFeeding
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID       - Device ID
//                 DWORD DocID          - Document ID
//                 DWORD ImageNumber    - 1st Front image or 2nd Front image or 1st Rear Image or 2nd Rear image
//                 DWORD Parameter      - Value selector
//                 int  *Value          - Receive the test value
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//    Get the result value of an IQA parameter
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetIQAValues( DWORD DeviceID, DWORD DocID, UINT ImageNumber, DWORD Parameter, int* Value );

//*****************************************************************************
// Function:  GetIQALastError
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID       - Device ID
//                 int*  Error          - IQA error code
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//    Get last IQA error
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetIQALastError( DWORD DeviceID, int *Error );

//*****************************************************************************
// Function:  GetIQALastErrorString
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID             - device ID
//                 char* ErrorString          - error string
//                 DWORD MaxErrorStringLen    - error string length
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//    Get last IQA error description.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetIQALastErrorString( DWORD DeviceID, char *ErrorString, DWORD MaxErrorStringLen );

//*****************************************************************************
// Function:  GetDocsInTrack
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOnLine
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID       - device ID
//                 DWORD* pdwNum        - number of docs in track
//                 DWORD* pdwFirstID    - first doc's ID
//                 DWORD* pdwFirstID    - last doc's ID
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//    Get the number of docs in the track, the first and the last doc's ID.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDocsInTrack( DWORD DeviceID, DWORD *pdwNum, DWORD *pdwFirstID, DWORD *pdwLastID );

//*****************************************************************************
// Function:  GetDocInfo
//
//   Method Type:      Synchronous
//
//   Valid in States:  DeviceOnLine
//
//   State Transition: -
//
//   Arguments:    DWORD DeviceID   - device ID
//                 DWORD dwDocID    - doc's ID
//                 BYTE* pData      - doc's data
//
//   Return Value: TRUE  - command done
//                 FALSE - command failure
//
//   Description:
//    Get doc dwDocID data.
//-----------------------------------------------------------------------------
VISION_API_DECL BOOL VISION_API GetDocInfo( DWORD DeviceID, DWORD dwDocID, BYTE *pData );

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// VISION API "INTERFACE LAYER" FUNCTIONS
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//*****************************************************************************
// Function:  VApiGetVersion
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:     char* sVersion - pointer to API version destination string
//                  BYTE MaxLen - Max string len
//
//   Return Value:  TRUE - the function terminates correctly
//                  FALSE - there has been some errors 
//
//   Description:
//     Get string with API release.
//-----------------------------------------------------------------------------
VISION_API_DECL VAPI_RET_TYPE VISION_API VApiGetVersion( char* sVersion, DWORD MaxLen );


//*****************************************************************************
// Function:  VApiSetDeviceEngine
//
//   Method Type:      Synchronous
//
//   Valid in States:  The device must be shutdown
//
//   State Transition: None
//
//   Arguments:     DWORD EngineSelector - Device engine selector
//
//   Return Value:  VAPI_ERR_NONE if successful otherwise the error code
//
//   Description:
//   This function select the device engine
//-----------------------------------------------------------------------------
#define VAPI_ENGINE_BASE    (0)
#define VAPI_ENGINE_VX      (VAPI_ENGINE_BASE)  // Vision X device engine selector
#define VAPI_ENGINE_VS      (VAPI_ENGINE_VX+1)  // Vision S device engine selector
#define VAPI_ENGINE_ID      (VAPI_ENGINE_VS+1)  // I-Deal device engine selector

#define VAPI_ENGINE_MAX     (VAPI_ENGINE_ID)

VISION_API_DECL VAPI_RET_TYPE VISION_API VApiSetDeviceEngine( DWORD EngineSelector );

//*****************************************************************************
// Function:  VApiGetError
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:        char *sErrorString   - pointer to destination string
//                     int MaxLen           - string max length (at least ERROR_MIN_STR_LEN)
//
//   Return Value: The last error code
//
//   Description:
//     Return the last Error Code. Clear last error.
//-----------------------------------------------------------------------------
VISION_API_DECL VAPI_RET_TYPE VISION_API VApiGetError( void );

//*****************************************************************************
// Function:  VApiGetError, VApiGetErrorString
//
//   Method Type:      Synchronous
//
//   Valid in States:  All
//
//   State Transition: None
//
//   Arguments:        ERR_CODE errorCode   - Error code returned by VApiGetError
//                     char *sErrorString   - pointer to destination string
//                     int MaxLen           - string max length (at least ERROR_MIN_STR_LEN)
//
//   Return Value: VAPI_ERR_NONE if successful otherwise the error code
//
//   Description:
//     Return the Error Code string description.
//-----------------------------------------------------------------------------
VISION_API_DECL VAPI_RET_TYPE VISION_API VApiGetErrorString( ERR_CODE ErrorCode, char *sErrorString, int MaxLen );


#define RET_OK 0 //NO ERROR

//
// VISION API LAYER ERROR CODES - returned by the VApiGetError()
//
#define VAPI_ERR_NONE                       RET_OK// no error
#define VAPI_ERR_BASE                       VAPI_ERR_NONE
#define VAPI_ERR_ENGINE_LOAD_FAILED         (VAPI_ERR_BASE+1)  // Device Engine load failed
#define VAPI_ERR_FUNCTION_NOT_SUPPORTED     (VAPI_ERR_BASE+2)  // The Device does not support the interface function
#define VAPI_ERR_INVALID_PARAM              (VAPI_ERR_BASE+3)  // Invalid parameter detected
#define VAPI_ERR_INTERNAL                   (VAPI_ERR_BASE+4)  // An internal error occured

#define _VAPI_ERR_LENGTH                    (5)

//
// API LAYER ERROR CODES - returned by the GetApiError(...) method
//
#define API_ERR_NONE                       0   // no error
#define API_ERR_DEVICE_CREATE              1   // Device create failure
#define API_ERR_BRIDGE_THREAD              2   // failure creating or resuming Bridge Tread during API starting-up or shutting-down
#define API_ERR_POLLING_THREAD             3   // failure creating or resuming Polling Tread during API starting-up or shutting-down
#define API_ERR_COMPRESSION_THREAD         4   // Failure Creating or Resuming Compression Thread during API starting-up or Shutting-down
#define API_ERR_OCR_MANAGER                5   // Generic OCR error 
#define API_ERR_OCR_NOT_ENABLED            6   // The font is not available
#define API_ERR_BUFF_ALLOC                 7   // buffers allocation failure
#define API_ERR_NO_USB_PORT_AVAILABLE      8   // no USB port available starting-up the API (StartUp() method)
#define API_ERR_LOG                        9   // error writting function call to Log file
#define API_ERR_INVALID_DEV_ID             10  // API method called with an invalid ID
#define API_ERR_INVALID_DEV_OBJ            11  // invalid Device Object corrisponding the ID supplied by the method
#define API_ERR_INVALID_PARAM              12  // invalid parameter passed to API method
#define API_ERR_INVALID_STATE              13  // method cannot be called inside the current API state
#define API_ERR_DEVICE                     14  // error caused calling a Device Object specific method.
                                               // More information about the error calling GetDeviceError(...)
#define API_ERR_USB                        15  // error caused calling a Device Object method that involve a USB tranfer.
                                               // More information about the error calling GetUSBError(...)
#define API_ERR_FPGA_PROGRAMMING           16  // FPGA programming error
#define API_ERR_E2PROM_WRITE               17  // E2PROM write error
#define API_ERR_E2PROM_READ                18  // E2PROM read error
#define API_ERR_SET_TIME_LEDS              19  // Set time LEDs error
#define API_ERR_SET_GAINS                  20  // Set gains error
#define API_ERR_SET_OFFSETS                21  // Set offsets error
#define API_ERR_SERIAL_PORT                22  // error opening or clossing serial port
#define API_ERR_SEND_SERIAL_COMMAND        23  // send command through serial port
#define API_ERR_NOT_AVAILABLE_DEVICE       24  // not available device enabled

#define API_ERR_TRANSFER_THREAD            25  // Failure Creating or Resuming Transfer thread during API starting-up or Shutting-down
#define API_ERR_INTERNAL                   26 // API Internal error
#define API_ERR_IQA                        27  // IQA Internal error

#define _API_ERR_LENGTH                    28


//
// DEVICE ERROR CODES - returned by the GetDeviceError(...) method
//
#define DEVICE_ERR_NONE                            0  // no error
#define DEVICE_ERR_UNKNOWN_SORTER_ERROR            1  // unknown sorter error

#define DEVICE_ERR_USB                             2  // USB error
#define DEVICE_ERR_USB_RX_CRC                      3  // CRC error reading USB message from sorter

#define DEVICE_ERR_WAIT_FEED_DONE_TIMEOUT          4  // wait feed done timeout
#define DEVICE_ERR_WAIT_LAST_DOC_POCKETED_TIMEOUT  5  // wait last document pocketed timeout
#define DEVICE_ERR_READ_SORTER_STATUS              6  // read sorter status failure

#define DEVICE_ERR_SORTER_ERROR_PENDING            7  // Sorter error pending: photocells callibration failure or jam document

#define DEVICE_ERR_COMMUNICATION_FAILURE           8  // Communication failure reported by sorter
#define DEVICE_ERR_FEED_FAILURE                    9  // Feed failure

#define DEVICE_ERR_FULL_POCKET                     10 // Full pocket
#define DEVICE_ERR_SAFETY                          11 // Safety: cover opened
#define DEVICE_ERR_GET_LAST_DOC_POCKETED_ID        12 // Failure reading last document pocketed ID
#define DEVICE_ERR_FEED_DOCUMENT                   13 // Failure sending feed document command
#define DEVICE_ERR_TRACK_NOT_CLEAR                 14 // Feeding command failure due to a document in the track
#define DEVICE_ERR_SET_POCKET                      15 // Set pocket command failure
#define DEVICE_ERR_GET_LAST_DOC_ID                 16 // Failure reading last document ID

#define DEVICE_ERR_READ_MICR_SIZE                  17 // Failure reading MICR waveform size
#define DEVICE_ERR_READ_MICR_WAVEFORM_CHUNK        18 // Failure reading MICR waveform chunk
#define DEVICE_ERR_DIFFERENT_MICR_SIZE             19 // Different returned MICR size
#define DEVICE_ERR_MICR_BUFFER_OVERFLOW            20 // MICR buffer overflow
#define DEVICE_ERR_MICR_READING                    21 // MICR reading failure

#define DEVICE_ERR_ACQUISITION_FAILED              22 // Not able to acquire Image
#define DEVICE_ERR_COMPRESSION_ERR                 23 // Error in Compression Thread
#define DEVICE_ERR_READ_ERROR                      24 // Read sorter error failure
#define DEVICE_ERR_CREATE_COMPENSATION_TABLES      25 // Failure creating compensation tables
#define DEVICE_ERR_ID_CARD                         26 // Failure decoding the ID Card
#define DEVICE_ERR_DEVICE_ENABLING                 27 // Application is enabling not available device
#define DEVICE_ERR_LOADING_OCR                     28 // Failure loading OCR engine
#define DEVICE_ERR_CONNECTION_TIMEOUT              29 // USB connection attempt timeout
#define DEVICE_ERR_FEEDER_EMPTY                    30 // Feeder empty
#define DEVICE_ERR_FREE_TRACK_FAILED               31 // Free track command is failed
#define DEVICE_ERR_SOFTWARE_OVERLOAD               32 // Software overload
#define DEVICE_ERR_STANDBY                         33 // The device is in Standby status
#define DEVICE_ERR_NOT_COMPATIBLE_FW               34 // The firmware is not compatible with 
#define DEVICE_ERR_INVALID_SEND_PRINTER            35 // Invalid SendPrinterData call

#define DEVICE_ERR_UNSUPPORTED_ID_CARD             36 // The IDCard contain unsupported informations

#define DEVICE_ERR_INVALID_PARAMETERS              37 // Application is setting wrong device parameters

#define DEVICE_ERR_FEEDER_LIMIT                    38 // The feeder has reached the docs limitation

#define DEVICE_ERR_WAITING_FEEDER_EMPTY            39 // The device is waiting for feeder empty because
                                                      // docs limitation has reached

#define DEVICE_ERR_FEEDER_LIMIT_RESET              40 // The feeder is ready to work after limitation
#define DEVICE_ERR_NOT_AVAILABLE_MAX_DPM           41 //
#define DEVICE_ERR_NOT_CALIBRATED_IMAGE            42
#define DEVICE_ERR_UPGRADE_UNSUCCESSFUL            43 // Errore con Upgrade IDCard
#define DEVICE_ERR_NO_PREREQUISITE_FOR_UPGRADE     44

#define DEVICE_ERR_BUTTON_STOP                     47 // 
#define DEVICE_ERR_BUTTON_JAM                      48 // 

#define DEVICE_ERR_INVALID_STATE                   49 // 

#define DEVICE_ERR_DEFAULT_POCKET                  50 //

#define DEVICE_ERR_US_DFD_SELF_TEST                51 //  Ultrasonic DFD Self-Test failed

#define DEVICE_ERR_MISSING_FRANKING                52 //  The app hasn't call the EnableFrankign function when the Franked image is enabled

#define DEVICE_ERR_FRANKING_FAILED                 53 //  The device has detected a problem franking the document

#define _DEVICE_ERR_LENGTH                         54


//
// USB ERROR CODES - returned by the GetUsbError(...) function
//
#define USB_ERR_NONE                    0   // no error
#define USB_ERR_INVALID_DEVICE_NAME     1  // invalid USB device name. Used inside SetDeviceName(...)
#define USB_ERR_OPEN_DRIVER             2  // error opening driver
#define USB_ERR_CLOSE_DRIVER            3  // error closing driver
#define USB_ERR_GET_PIPE_INFO           4  // get pipe info failure
#define USB_ERR_GET_DEVICE_DESCRIPTOR   5  // get device descriptor failure
#define USB_ERR_DEVICE_IO_CONTROL       6  // DeviceIoControl call failure
#define USB_ERR_WRITE_BULK              7  // write bulk pipe failure
#define USB_ERR_READ_BULK               8  // read bulk pipe failure
#define USB_ERR_WRITE_CONTROL           9  // write control failure
#define USB_ERR_READ_CONTROL            10 // read control failure
#define USB_ERR_GET_LINK_STATE          11 // get link state failure
#define USB_ERR_SEND_LINK_MESSAGE       12 // send link message failure
#define USB_ERR_RECEIVE_LINK_MESSAGE    13 // receive link message
#define USB_ERR_PROGRAM_FPGA            14 // program FPGA
#define USB_ERR_CHECK_FPGA              15 // check FPGA programming done
#define USB_ERR_WRITE_E2PROM            16 // write E2PROM
#define USB_ERR_READ_E2PROM             17 // read E2PROM
#define USB_ERR_SET_TIME_LEDS           18 // set time LEDs
#define USB_ERR_SET_GAINS               19 // set gains
#define USB_ERR_SET_OFFSETS             20 // set offsets
#define USB_ERR_WRITE_DMA_BULK          21 // write DMA bulk pipe failure
#define USB_ERR_READ_DMA_BULK_PARAM     22 // read DMA bulk pipe - parameter
#define USB_ERR_READ_DMA_BULK_SETUP     23 // read DMA bulk pipe - setup
#define USB_ERR_READ_DMA_BULK_PHASE1    24 // read DMA bulk pipe - phase 1
#define USB_ERR_READ_DMA_BULK_PHASE2    25 // read DMA bulk pipe - phase 2
#define USB_ERR_READ_DMA_BULK_CLEAR     26 // read DMA bulk pipe - clear
#define USB_ERR_READ_ID_CARD            27 // read ID CARD EEPROM
#define USB_ERR_WRITE_ID_CARD           28 // write ID CARD EEPROM
#define USB_ERR_SET_DPM                 29 // set DPM

#define USB_INVALID_PARAM               30 // Invalid function parameters
#define USB_INVALID_DEVICE              31 // Invalid USB device handle
#define USB_SYSTEM_ERROR                32 // System error

#define _USB_ERR_LENGTH                 33

//
// SORTER ERROR CODES
//

// ERROR CLASSES
#define ERR_GENERAL        0x00   // General error
#define ERR_COMM           0x01   // Error on Communication with PC
#define ERR_INIT           0x02   // HW initialization Error
#define ERR_PERIPHERAL     0x03   // Devices error.
#define ERR_TRANSPORT      0x04   // Error during the transport of the paper

// Pheripherals codes
#define GENERAL            0x00
#define COMMUNICATION      0x01
#define READER_MODULE      0x02
#define ENCODER_MODULE     0x03
#define SORTER_MODULE      0x04
#define MICR_DEVICE        0x05
#define IMAGE              0x06
#define PRINTER1           0x07
#define PRINTER2           0x08
#define ENCODING_DEVICE    0x09
#define IMAGE2             0x0A
#define DROP_STATION       0x0B
#define OCR                0x0C
#define BARCODE            0x0D
#define MOTOR              0x0E

#define ERR_NONE           0x00   // No errors

// Error codes related to ERR_COMM
#define ERR_CHECKSUM      0x01    // checksum verification failure
#define ERR_UNKNOWN_CMD   0x02
#define ERR_INVALID_CMD   0x03
#define ERR_DOC_ID        0x04    // The Doc ID of sent already exist
#define ERR_POCKET        0x05    // Invalid destination pocket
#define ERR_SOURCE        0x06    // Invalid feeding source
#define ERR_PREV_POCKET   0x07    // Missing pocket for previous document on feed request
#define ERR_PARAM         0x08    // Error reading/writing a parameter

// Error codes related to ERR_INIT
#define ERR_INIT_PHOTOS   0x01    // Photocells initialization error

// Error codes related to ERR_TRANSPORT
#define ERR_DOC_LOST        0x01    // Docs never reach the photo or document too short
#define ERR_DOC_LENGTH      0x02    // Too long document
#define ERR_DOC_FEED        0x03    // Missing document in the feeder or feed failure
#define ERR_FREE_TRACK      0x04    // Free track not completed
#define ERR_DOC_DEST        0x05    // Doc without destination pocket
#define ERR_FULL_POCKET     0x06    // The pocket is full
#define ERR_OPEN_POCKET     0x07    // Problem opening the pocket
#define ERR_DOUBLE_FEEDING  0x08    // Double-feeding detected
#define ERR_DEFAULT_POCKET  0x09    // The document is in the default pocket
#define ERR_BUSY_PHOTO      0x0A    // Photo busy starting job
#define ERR_PRINTER         0x0B    // The printer device has detected a problem
#define ERR_IMAGE           0x0C    // The image device has detected a problem
#define ERR_BUFFER_BUSY     0x0D    // Il device ha rilevato un errore di sovrascrittura dei dati
#define ERR_BUTTON_STOP     0x0E    // Job stopped by user button
#define ERR_BUTTON_JAM      0x0F    // Jam by user button
#define ERR_DOC_IN_ADVANCE  0x10    // Document in advance
#define ERR_DOC_SHORT       0x11    // Too short document
#define ERR_FRANKING        0x12    // Franking failed

// Error codes related to ERR_PERIPHERAL
#define ERR_OPENED_COVER    0x01    // The cover are opened

#endif // of VAPI_INTERFACE_INCLUDED
