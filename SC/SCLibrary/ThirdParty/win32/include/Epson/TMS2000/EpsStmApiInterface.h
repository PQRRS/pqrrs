/******************************************************************************/
/* Copyright (C) SEIKO EPSON CORPORATION 2011. All rights reserved.           */
/******************************************************************************/
/*                                                                            */
/* EpsStmApiInterface.h                                                       */
/*                                                                            */
/* All interface definition of Status API                                     */
/*                                                                            */
/* Modification history                                                       */
/* -------------------------------------------------------------------------- */
/* 2011-07-12   New                                                           */
/*                                                                            */
/******************************************************************************/

#if !defined(_EPSSTMAPIINTERFACE_H_2000)
#define _EPSSTMAPIINTERFACE_H_2000

#include "MultiFunction.h"

#ifndef __STDCALL__
#if defined(WIN32)
#define __STDCALL__ __stdcall
#elif defined(__GNUC__)
#define __STDCALL__ __attribute__((stdcall))
#endif
#endif


typedef int (__STDCALL__ *DLL_BiOpenMonPrinter)                           (int, LPSTR);
typedef int (__STDCALL__ *DLL_BiCloseMonPrinter)                          (int);
typedef int (__STDCALL__ *DLL_BiStartEndorsementSetStatusBackFunction)    (int, int(__STDCALL__ *)(unsigned long, char*));
typedef int (__STDCALL__ *DLL_BiStartEndorsementSetStatusBackWnd)         (int, HWND, unsigned long*);
typedef int (__STDCALL__ *DLL_BiSetStatusBackFunction)                    (int, int(__STDCALL__ *)(DWORD));
typedef int (__STDCALL__ *DLL_BiSetStatusBackFunctionEx)                  (int, int(__STDCALL__ *)(DWORD, LPSTR));
typedef int (__STDCALL__ *DLL_BiSetStatusBackWnd)                         (int, long, LPDWORD);
typedef int (__STDCALL__ *DLL_BiSetStatusBackWndEx)                       (int, HWND, LPDWORD);
typedef int (__STDCALL__ *DLL_BiEndEndorsementSetStatusBackFunction)      (int, int(__STDCALL__ *)(unsigned long, char*));
typedef int (__STDCALL__ *DLL_BiEndEndorsementSetStatusBackWnd)           (int, HWND, unsigned long*);
typedef int (__STDCALL__ *DLL_BiCancelStatusBack)                         (int);
typedef int (__STDCALL__ *DLL_BiStartEndorsementCancelStatusBack)         (int);
typedef int (__STDCALL__ *DLL_BiEndEndorsementCancelStatusBack)           (int);
typedef int (__STDCALL__ *DLL_BiSCNMICRSetStatusBackFunction)             (int, int(__STDCALL__ *)(DWORD, WORD, WORD, LPSTR));
typedef int (__STDCALL__ *DLL_BiSCNMICRSetStatusBackWnd)                  (int, long, LPDWORD, LPWORD, LPWORD);
typedef int (__STDCALL__ *DLL_BiSCNMICRSetStatusBackWndEx)                (int, HWND, LPDWORD, LPWORD, LPWORD);
typedef int (__STDCALL__ *DLL_BiSCNMICRCancelStatusBack)                  (int);
typedef int (__STDCALL__ *DLL_BiSetInkStatusBackFunction)                 (int, int(__STDCALL__ *)(WORD));
typedef int (__STDCALL__ *DLL_BiSetInkStatusBackFunctionEx)               (int, int(__STDCALL__ *)(WORD, LPSTR));
typedef int (__STDCALL__ *DLL_BiSetInkStatusBackWnd)                      (int, long, LPDWORD);
typedef int (__STDCALL__ *DLL_BiSetInkStatusBackWndEx)                    (int, HWND, LPDWORD);
typedef int (__STDCALL__ *DLL_BiCancelInkStatusBack)                      (int);
typedef int (__STDCALL__ *DLL_BiCancelError)                              (int);
typedef int (__STDCALL__ *DLL_BiResetCounter)                             (int, WORD);
typedef int (__STDCALL__ *DLL_BiResetPrinter)                             (int);
typedef int (__STDCALL__ *DLL_BiSCNMICRCancelFunction)                    (int, WORD);
typedef int (__STDCALL__ *DLL_BiSCNMICRFunction)                          (int, LPVOID, WORD);
typedef int (__STDCALL__ *DLL_BiSCNMICRFunctionContinuously)              (int, LPVOID, WORD);
typedef int (__STDCALL__ *DLL_BiSCNMICRFunctionPostPrint)                 (int, LPVOID, WORD);
typedef int (__STDCALL__ *DLL_BiSCNDeleteCroppingArea)                    (int, BYTE);
typedef int (__STDCALL__ *DLL_BiDecodeBarcode)                            (int, LPCSTR, LPMF_BARCODE);
typedef int (__STDCALL__ *DLL_BiDecodeBarcodeMemory)                      (int, LPBYTE, DWORD, LPMF_BARCODE);
typedef int (__STDCALL__ *DLL_BiGetBarcodeData)                           (int, DWORD, LPMF_BARCODE);
typedef int (__STDCALL__ *DLL_BiGetOcrABText)                             (int, DWORD, BYTE, LPCSTR, LPMF_OCR_AB);
typedef int (__STDCALL__ *DLL_BiInsertValidation)                         (int, DWORD);
typedef int (__STDCALL__ *DLL_BiBufferedPrint)                            (int, DWORD);
typedef int (__STDCALL__ *DLL_BiPrintBarCode)                             (int, LPBYTE, DWORD, DWORD, DWORD);
typedef int (__STDCALL__ *DLL_BiPrintImage)                               (int, LPSTR);
typedef int (__STDCALL__ *DLL_BiPrintMemoryImage)                         (int, LPBYTE, DWORD);
typedef int (__STDCALL__ *DLL_BiPrintText)                                (int, LPSTR, MF_DECORATE);
typedef int (__STDCALL__ *DLL_BiRemoveValidation)                         (int, DWORD);
typedef int (__STDCALL__ *DLL_BiPrintCutSheet)                            (int, void*, unsigned short);
typedef int (__STDCALL__ *DLL_BiUpdateEndorseText)                        (int, LPSTR[3], DWORD[3], WORD[3],WORD[3]);
typedef int (__STDCALL__ *DLL_BiAutoCutRollPaper)                         (int, int);
typedef int (__STDCALL__ *DLL_BiPrintMultipleToneImage)                   (int, char, char);
typedef int (__STDCALL__ *DLL_BiClearTemplatePrintData)                   (int, unsigned long);
typedef int (__STDCALL__ *DLL_BiTemplatePrint)                            (int, int);
typedef int (__STDCALL__ *DLL_BiInkHeadCleaning)                          (int);
typedef int (__STDCALL__ *DLL_BiMICRCleaning)                             (int);
typedef int (__STDCALL__ *DLL_BiOpenDrawer)                               (int, BYTE, BYTE);
typedef int (__STDCALL__ *DLL_BiSetTransactionNumber)                     (int, DWORD);
typedef int (__STDCALL__ *DLL_BiSetTransactionNumberWithIncremental)      (int, DWORD, DWORD);
typedef int (__STDCALL__ *DLL_BiSetWaterfallMode)                         (int, BYTE);
typedef int (__STDCALL__ *DLL_BiSetBehaviorToScnResult)                   (int, BYTE, BYTE, BYTE);
typedef int (__STDCALL__ *DLL_BiLoadAPISettings)                          (int, char*);
typedef int (__STDCALL__ *DLL_BiSetMonInterval)                           (int, WORD, WORD);
typedef int (__STDCALL__ *DLL_BiSCNSelectScanUnit)                        (int, BYTE);
typedef int (__STDCALL__ *DLL_BiESCNEnable)                               (BYTE);
typedef int (__STDCALL__ *DLL_BiSCNSelectScanFace)                        (int, BYTE);
typedef int (__STDCALL__ *DLL_BiSCNSetImageFormat)                        (int, BYTE);
typedef int (__STDCALL__ *DLL_BiSCNSetImageQuality)                       (int, BYTE, char, BYTE, BYTE);
typedef int (__STDCALL__ *DLL_BiSCNSetImageTypeOption)                    (int, unsigned long);
typedef int (__STDCALL__ *DLL_BiSCNSetScanArea)                           (int, BYTE, BYTE, BYTE, BYTE);
typedef int (__STDCALL__ *DLL_BiSelectErrorEjectAtContinuously)           (int, DWORD, DWORD);
typedef int (__STDCALL__ *DLL_BiSetNumberOfDocuments)                     (int, BYTE);
typedef int (__STDCALL__ *DLL_BiSetOcrABAreaOrigin)                       (int, BYTE);
typedef int (__STDCALL__ *DLL_BiSCNSelectScanImage)                       (int, unsigned char);
typedef int (__STDCALL__ *DLL_BiSCNSetCroppingArea)                       (int, BYTE, BYTE, BYTE, BYTE, BYTE);
typedef int (__STDCALL__ *DLL_BiESCNDefineCropArea)                       (int, BYTE, WORD, WORD, WORD, WORD);
typedef int (__STDCALL__ *DLL_BiESCNClearImage)                           (int, BYTE, DWORD, LPSTR, LPSTR);
typedef int (__STDCALL__ *DLL_BiESCNStoreImage)                           (int, DWORD, LPSTR, LPSTR, BYTE);
typedef int (__STDCALL__ *DLL_BiESCNSetAutoSize)                          (int, BYTE);
typedef int (__STDCALL__ *DLL_BiESCNSetCutSize)                           (int, WORD);
typedef int (__STDCALL__ *DLL_BiESCNSetDeSkew)                            (int, WORD);
typedef int (__STDCALL__ *DLL_BiESCNSetDocumentSize)                      (int, WORD, WORD);
typedef int (__STDCALL__ *DLL_BiESCNSetRotate)                            (int, BYTE);
typedef int (__STDCALL__ *DLL_BiSCNSetImageAdjustment)                    (int, int, int, unsigned long);
typedef int (__STDCALL__ *DLL_BiSetPrintStation)                          (int, WORD);
typedef int (__STDCALL__ *DLL_BiSetEndorseDirection)                      (int, BYTE);
typedef int (__STDCALL__ *DLL_BiSetPrintAlignment)                        (int, DWORD);
typedef int (__STDCALL__ *DLL_BiSetPrintControl)                          (int, BYTE, BYTE);
typedef int (__STDCALL__ *DLL_BiSetPrintPosition)                         (int, WORD, WORD);
typedef int (__STDCALL__ *DLL_BiSetPrintSize)                             (int, WORD, WORD);
typedef int (__STDCALL__ *DLL_BiSetPrintCutSheetSettings)                 (int, unsigned char, unsigned char);
typedef int (__STDCALL__ *DLL_BiSetPrintImageMethod)                      (int, int);
typedef int (__STDCALL__ *DLL_BiLoadTemplatePrintArea)                    (int, char*, int*);
typedef int (__STDCALL__ *DLL_BiSetTemplatePrintArea)                     (int, int, char*, LPPRINTAREAINFO, unsigned long);
typedef int (__STDCALL__ *DLL_BiMICRSelectDataHandling)                   (int, BYTE, BYTE, BYTE);
typedef int (__STDCALL__ *DLL_BiMICRClearSpaces)                          (int, BYTE);
typedef int (__STDCALL__ *DLL_BiSetPaperThickness)                        (int, WORD);
typedef int (__STDCALL__ *DLL_BiRingBuzzer)                               (int, BYTE, BYTE, WORD, WORD);
typedef int (__STDCALL__ *DLL_BiSelectJamDetect)                          (int, DWORD);
typedef int (__STDCALL__ *DLL_BiGetCounter)                               (int, WORD, LPDWORD);
typedef int (__STDCALL__ *DLL_BiGetOfflineCode)                           (int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiGetOfflineCodeByIndex)                    (int, int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiGetType)                                  (int, LPBYTE, LPBYTE, LPBYTE, LPBYTE);
typedef int (__STDCALL__ *DLL_BiGetTransactionNumber)                     (int, LPDWORD);
typedef int (__STDCALL__ *DLL_BiGetVersion)                               (int, int, LPVERSION_INFO);
typedef int (__STDCALL__ *DLL_BiGetRealStatus)                            (int, LPDWORD);
typedef int (__STDCALL__ *DLL_BiGetStatus)                                (int, LPDWORD);
typedef int (__STDCALL__ *DLL_BiSCNGetClumpStatus)                        (int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiSCNGetImageFormat)                        (int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiSCNGetImageQuality)                       (int, LPBYTE, char*, LPBYTE, LPBYTE);
typedef int (__STDCALL__ *DLL_BiSCNGetImageTypeOption)                    (int, unsigned long*);
typedef int (__STDCALL__ *DLL_BiSCNGetScanArea)                           (int, LPBYTE, LPBYTE, LPBYTE, LPBYTE);
typedef int (__STDCALL__ *DLL_BiESCNGetMaxCropAreas)                      (int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiESCNGetRemainingImages)                   (int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiESCNGetAutoSize)                          (int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiESCNGetCutSize)                           (int, LPWORD);
typedef int (__STDCALL__ *DLL_BiESCNGetDeSkew)                            (int, LPWORD);
typedef int (__STDCALL__ *DLL_BiESCNGetDocumentSize)                      (int, LPWORD, LPWORD);
typedef int (__STDCALL__ *DLL_BiESCNGetRotate)                            (int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiConfirmBufferedData)                      (int, LPDWORD);
typedef int (__STDCALL__ *DLL_BiESCNRetrieveImage)                        (int, DWORD, LPSTR, LPSTR, LPDWORD, LPBYTE);
typedef int (__STDCALL__ *DLL_BiGetIqaResult)                             (int, DWORD, LPMF_IQA_RESULT);
typedef int (__STDCALL__ *DLL_BiGetScanImage)                             (int, DWORD, LPMF_SCAN);
typedef int (__STDCALL__ *DLL_BiGetScanImageSize)                         (int, DWORD, LPMF_SCAN);
typedef int (__STDCALL__ *DLL_BiGetPrnCapability)                         (int, BYTE, LPBYTE, LPBYTE);
typedef int (__STDCALL__ *DLL_BiGetInkStatus)                             (int, LPWORD);
typedef int (__STDCALL__ *DLL_BiGetPrintStation)                          (int, LPWORD);
typedef int (__STDCALL__ *DLL_BiGetPrintAlignment)                        (int, LPDWORD);
typedef int (__STDCALL__ *DLL_BiGetPrintControl)                          (int, LPBYTE, LPBYTE);
typedef int (__STDCALL__ *DLL_BiGetPrintPosition)                         (int, LPWORD, LPWORD);
typedef int (__STDCALL__ *DLL_BiGetPrintSize)                             (int, LPWORD, LPWORD);
typedef int (__STDCALL__ *DLL_BiGetPrintCutSheetSettings)                 (int, unsigned char*, unsigned char*);
typedef int (__STDCALL__ *DLL_BiSCNGetCroppingArea)                       (int, LPWORD, LPBYTE);
typedef int (__STDCALL__ *DLL_BiGetPrintImageMethod)                      (int, int*);
typedef int (__STDCALL__ *DLL_BiSCNGetImageAdjustment)                    (int, int*, int*, unsigned long*);
typedef int (__STDCALL__ *DLL_BiMICRGetStatus)                            (int, LPBYTE);
typedef int (__STDCALL__ *DLL_BiGetMicrText)                              (int, DWORD, LPMF_MICR);
typedef int (__STDCALL__ *DLL_BiSetConfigure)                             (int, unsigned long, unsigned long);
typedef int (__STDCALL__ *DLL_BiSCNPrintText)                             (int, unsigned long, LPCSTR, MF_DECORATE*);
typedef int (__STDCALL__ *DLL_BiSCNPrintMemoryImage)                      (int, unsigned long, LPBYTE, DWORD);

#endif  /* !defined(_EPSSTMAPIINTERFACE_H) */
