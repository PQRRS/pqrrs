/******************************************************************************/
/* Copyright (C) SEIKO EPSON CORPORATION 2011. All rights reserved.           */
/******************************************************************************/
/*                                                                            */
/* MultiFunction.h                                                            */
/*                                                                            */
/* Definition of the constants and also data type.                            */
/*                                                                            */
/* Modification history                                                       */
/* -------------------------------------------------------------------------- */
/* 2011-07-12   New                                                           */
/*                                                                            */
/******************************************************************************/

#if !defined(TMS2000_MULTI_FUNCTION_H)
#define TMS2000_MULTI_FUNCTION_H


#if defined(WIN32)

#elif defined(__GNUC__)

#endif


/******************************************************************************/
/******************************************************************************/
/* ERROR CODE                                                                 */
/******************************************************************************/
/******************************************************************************/
#define     SUCCESS                         0           /* Success                                                            */

#define     ERR_UNKNOWN                     -9999       /* Unknown Error                                                      */

#define     ERR_TYPE                        -10         /* nType error                                                        */
#define     ERR_OPENED                      -20         /* Already opened                                                     */
#define     ERR_NO_PRINTER                  -30         /* There is not printer driver                                        */
#define     ERR_NO_TARGET                   -40         /* Printer out of object                                              */
#define     ERR_NO_MEMORY                   -50         /* No memory                                                          */
#define     ERR_HANDLE                      -60         /* Invalid handle                                                     */
#define     ERR_TIMEOUT                     -70         /* Time out                                                           */
#define     ERR_ACCESS                      -80         /* cannot read/write                                                  */
#define     ERR_PARAM                       -90         /* param error                                                        */
#define     ERR_NOT_SUPPORT                 -100        /* not support                                                        */
#define     ERR_OFFLINE                     -110        /* now offline                                                        */
#define     ERR_NOT_EPSON                   -120        /* not epson printer                                                  */
#define     ERR_WITHOUT_CB                  -130        /* without callback function                                          */
#define     ERR_BUFFER_OVER_FLOW            -140        /* Read buffer over flow                                              */
#define     ERR_REGISTRY                    -150        /* Registry error                                                     */
#define     ERR_ENABLE                      -160        /* BiOpenMonPrinter() is already called                               */
#define     ERR_DISK_FULL                   -170        /* Capacity of a disk is insufficient                                 */
#define     ERR_NO_IMAGE                    -180        /* No image data                                                      */
#define     ERR_ENTRY_OVER                  -190        /* Registration number-of-cases over                                  */
#define     ERR_CROPAREAID                  -200        /* No specific CropAreaID                                             */
#define     ERR_EXIST                       -210        /* Already the same thing                                             */
#define     ERR_NOT_FOUND                   -220        /* Not found                                                          */
#define     ERR_IMAGE_FILEOPEN              -230        /* Open failure                                                       */
#define     ERR_IMAGE_UNKNOWNFORMAT         -240        /* Unknown format                                                     */
#define     ERR_IMAGE_FAILED                -250        /* Image creation failure                                             */
#define     ERR_WORKAREA_NO_MEMORY          -260        /* No memory for WORKAREA                                             */
#define     ERR_WORKAREA_UNKNOWNFORMAT      -270        /* Image creation failure because of format injustice                 */
#define     ERR_WORKAREA_FAILED             -280        /* WORKAREA creation failure                                          */
#define     ERR_IMAGE_FILEREAD              -290        /* Image file read error                                              */
#define     ERR_PAPERINSERT_TIMEOUT         -300        /* Image paper insert error of timeout                                */
#define     ERR_EXEC_FUNCTION               -310        /* Other API is running                                               */
#define     ERR_EXEC_MICR                   -320        /* Now reading MICR                                                   */
#define     ERR_EXEC_SCAN                   -330        /* Now scanning image                                                 */
#define     ERR_SS_NOT_EXIST                -340        /* Status service not started                                         */
#define     ERR_SPL_NOT_EXIST               -350        /* Spooler service not started                                        */
#define     ERR_SPL_PAUSED                  -370        /* Spooler service paused                                             */
#define     ERR_RESET                       -400        /* Now printer is resetting                                           */
#define     ERR_THREAD                      -420        /* Failure to the start of Thread                                     */
#define     ERR_ABORT                       -430        /* Because BiSCNMICRCancelFunction was called, processing was stopped */
#define     ERR_MICR                        -440        /* Error occurred during MICR processing                              */
#define     ERR_SCAN                        -450        /* Error occurred during the image scan                               */
#define     ERR_LINE_OVERFLOW               -460        /* A line overflow has occurred during transaction printing           */
#define     ERR_NOT_EXEC                    -470        /* Processing is not executed                                         */
#define     ERR_DATA_INVALID                -480        
#define     ERR_SIZE                        -1000       /* Size excess error                                                  */
#define     ERR_PAPER_PILED                 -1010       /* Paper pilling error                                                */
#define     ERR_PAPER_JAM                   -1020       /* Paper jam has occurred                                             */
#define     ERR_COVER_OPEN                  -1030       /* Cover open error                                                   */
#define     ERR_MICR_NODATA                 -1040       /* MICR data is not existing                                          */
#define     ERR_MICR_BADDATA                -1050       /* MICR data is not able to recognize                                 */
#define     ERR_MICR_PARSE                  -1060       /* MICR data can not be parsed                                        */
#define     ERR_MICR_NOISE                  -1070       /* Noise error has occurred during MICR reading                       */
#define     ERR_SCN_COMPRESS                -1080       /* Scan image data compressing error                                  */
#define     ERR_PAPER_EXIST                 -1090       /* Because there is a paper on the path, API can not be execute       */
#define     ERR_PAPER_INSERT                -1100       /* Paper insertion error                                              */
#define     ERR_LESS_CHECKS                 -1110       
#define     ERR_SCN_IQA                     -1120       /* IQA not pass                                                       */
#define     ERR_BARCODE_NODATA              -1130       /* Barcode data is not existing                                       */
#define     ERR_UNLOCKED                    -1140       /* Port Unlocked                                                      */
#define     ERR_NET_CONNECTED               -1150       /* Network connection fails                                           */
#define     ERR_MSRW_NODATA                 -1160       /* MSR data is not existing                                           */
#define     ERR_IMAGE_FILEREMOVE            -1170       /* Remove failure                                                     */
#define     ERR_PRINT_DATA_LENGTH_EXCEED    -1180		/* Printing data length exceeds the paper size                        */
#define     ERR_PRINT_DATA_UNRECEIVE        -1190       /* Printing data is not received                                      */
#define     ERR_EXEC_SCAN_CHECK_CONTINUOUS  -2000       /* Now scanning check continuous mode                                 */
#define     ERR_EXEC_SCAN_CHECK_ONEBYONE    -2010       /* Now scanning check one by one mode                                 */
#define     ERR_EXEC_SCAN_IDCARD            -2020       /* Now scanning ID card                                               */
#define     ERR_EXEC_PRINT_ROLLPAPER        -2030       /* Now printing text, image or barcode on roll paper                  */
#define     ERR_EXEC_PRINT_VALIDATION       -2040       /* Now printing text, image or barcode on validation                  */


/******************************************************************************/
/******************************************************************************/
/* ASB Bit                                                                    */
/******************************************************************************/
/******************************************************************************/
#define     ASB_NO_RESPONSE                             0x00000001  /* No response                            */
#define     ASB_PRINT_SUCCESS                           0x00000002  /* Finish to print                        */
#define     ASB_DRAWER_KICK                             0x00000004  /* Drawer kick-out connector pin3 is HIGH */
#define     ASB_OFF_LINE                                0x00000008  /* Off-line                               */
#define     ASB_MAIN_POCKET_NEAR_FULL                   0x00000010  /* Main Pocket Near Full              */
#define     ASB_COVER_OPEN                              0x00000020  /* Cover is open                          */
#define     ASB_PAPER_FEED                              0x00000040  /* Paper is now feeding by PF FW          */
#define     ASB_SUB_POCKET_NEAR_FULL                    0x00000080  /* Sub Pocket Near Full               */
#define     ASB_WAIT_PEPRT_EJECT                        0x00000100  /* Waiting for slip remove                */
#define     ASB_PANEL_SWITCH                            0x00000200  /* Panel switch                           */
#define     ASB_MECHANICAL_ERR                          0x00000400  /* Mechanical error                       */
#define     ASB_AUTOCUTTER_ERR                          0x00000800  /* Auto cutter error                      */
#define     ASB_UNRECOVER_ERR                           0x00002000  /* Unrecoverable error                    */
#define     ASB_AUTORECOVER_ERR                         0x00004000  /* Auto-Recoverable error                 */
#define     ASB_NOT_CARD_INSERT                         0x00010000  /* Not card insert                        */
#define     ASB_RECEIPT_NEAR_END                        0x00020000  /* Receipt paper roll near-end            */
#define     ASB_MAIN_NEAR_FULL                          0x00020000  /* Main Pocket not Near Full              */
#define     ASB_EJECT_SENSOR_NO_PAPER                   0x00040000  /* Eject sensor no paper                  */
#define     ASB_RECEIPT_END                             0x00080000  /* Receipt paper roll end                 */
#define     ASB_SUB_NEAR_FULL                           0x00080000  /* Sub Pocket not Near Full               */
#define     ASB_PAPER_INTERMEDIATE                      0x00200000  /* Not paper in Paper intermediate sensor */
#define     ASB_SLIP_TOF                                0x00200000  /* SLIP TOF                               */
#define     ASB_ASF_PAPER                               0x00400000  /* No paper ASF                           */
#define     ASB_SLIP_SELECTED                           0x01000000  /* Slip is not selected                   */
#define     ASB_PRINT_SLIP                              0x02000000  /* Cannot print on slip                   */
#define     ASB_STAMP_EXIST                             0x02000000  /* Stamp not Exist                        */
#define     ASB_VALIDATION_SELECTED                     0x04000000  /* Validation is not selected             */
#define     ASB_PRINT_VALIDATION                        0x08000000  /* Cannot print on validation             */
#define     ASB_WAIT_INSERT                             0x20000000  /* Wait insert                            */
#define     ASB_SLIP_PAPER_SIZE                         0x40000000  /* Not paper in Slip Paper size sensor    */
#define     ASB_VALIDATION_NO_PAPER                     0x40000000  /* Validation no paper                    */
#define     ASB_FRANKING_SENSOR                         0x40000000  /* Not paper in Franking sensor           */

#define     INK_ASB_NEAR_END                            0x0001      /* Ink Low                                */
#define     INK_ASB_END                                 0x0002      /* Replace Ink Cartridge                  */
#define     INK_ASB_NO_CARTRIDGE                        0x0004      /* Cartridge is not present               */
#define     INK_ASB_NO_CARTRIDGE2                       0x0008      /* Cartridge2 is not present              */
#define     INK_ASB_CLEANING                            0x0020      /* Being cleaned                          */
#define     INK_ASB_NEAR_END2                           0x0100      /* Ink Low2                               */
#define     INK_ASB_END2                                0x0200      /* Replace Ink Cartridge2                 */


/******************************************************************************/
/******************************************************************************/
/* Function Parameter                                                         */
/******************************************************************************/
/******************************************************************************/
/*========================================================*/
/* BiOpenMonPrinter()                                     */
/*========================================================*/
/*------------------------------------*/
/* Open Type                          */
/*------------------------------------*/
#define     TYPE_PORT                       1           /* use port name           */
#define     TYPE_PRINTER                    2           /* use printer driver name */

/*========================================================*/
/* BiMICRSelectDataHandling()                             */
/*========================================================*/
/*------------------------------------*/
/* Error Select                       */
/*------------------------------------*/
#define     ES_STOP_ALL                     0x00
#define     ES_CONTINUE_ALL                 0x01
#define     ES_CONTINUE_DOUBLEFEED          0x02
#define     ES_CONTINUE_NODATA              0x04
#define     ES_CONTINUE_BADDATA             0x08
#define     ES_CONTINUE_NOISE               0x10

/*========================================================*/
/* BiSCNSetImageQuality()                                 */
/*========================================================*/
/*------------------------------------*/
/* Scale Option                       */
/*------------------------------------*/
#define     EPS_BI_SCN_1BIT                 1           /* Monochrome */
#define     EPS_BI_SCN_8BIT                 8           /* Gray scale */
#define     EPS_BI_SCN_24BIT                24          /* RGB 24bit  */
/*------------------------------------*/
/* Color Option                       */
/*------------------------------------*/
#define     EPS_BI_SCN_MONOCHROME           48          /* Monochrome */
#define     EPS_BI_SCN_COLOR                49          /* Color      */
/*------------------------------------*/
/* Extensive Option                   */
/*------------------------------------*/
#define     EPS_BI_SCN_MANUAL               49          /* Manual                                           */
#define     EPS_BI_SCN_SHARP                50          /* Sharpness                                        */
#define     EPS_BI_SCN_SHARP_CUSTOM         51          /* Custom sharpness                                 */
#define     EPS_BI_SCN_SHARP_CUSTOM2        52          /* Combination of sharpness and custom sharpness    */
#define     EPS_BI_SCN_SHARP_CUSTOM3        53          /* Sharpness(eliminate checkered patterns)          */

/*========================================================*/
/* BiSCNSetImageFormat()                                  */
/*========================================================*/
/*------------------------------------*/
/* Format Option                      */
/*------------------------------------*/
#define     EPS_BI_SCN_TIFF                 1           /* TIFF format(CCITT Group 4)   */
#define     EPS_BI_SCN_RASTER               2           /* Raster Image                 */
#define     EPS_BI_SCN_BITMAP               3           /* Bitmap                       */
#define     EPS_BI_SCN_TIFF256              4           /* TIFF format(Gray scale)      */
#define     EPS_BI_SCN_JPEGHIGH             5           /* Jpeg format(High compressed) */
#define     EPS_BI_SCN_JPEGNORMAL           6           /* Jpeg format(Normal)          */
#define     EPS_BI_SCN_JPEGLOW              7           /* Jpeg format(Low compressed)  */
#define     EPS_BI_SCN_JTIFF                8           /* TIFF format(Jpeg compressed) */

/*========================================================*/
/* BiSCNSelectScanUnit()                                  */
/*========================================================*/
#define     EPS_BI_SCN_UNIT_CHECKPAPER      48          /* Checkpaper scanner unit */
#define     EPS_BI_SCN_UNIT_CARD            49          /* Card scanner unit       */

/*========================================================*/
/* BiSCNSelectScanFace()                                  */
/*========================================================*/
#define     MF_SCAN_FACE_FRONT              0
#define     MF_SCAN_FACE_BACK               1

/*========================================================*/
/* BiGetOcrABText()                                       */
/*========================================================*/
#define     OCR_SOURCE_TRANSACTION_NUMBER   0
#define     OCR_SOURCE_IMAGE_FILE           1

/*========================================================*/
/* BiSetPrintStation()                                    */
/*========================================================*/
/*------------------------------------*/
/* Printing Station                   */
/*------------------------------------*/
#define     MF_ST_ROLLPAPER                 0x0001                  /* Roll paper printing                   */
#define     MF_ST_VALIDATION                0x0002                  /* Validation printing                   */
#define     MF_ST_E_ENDORSEMENT             0x0004                  /* Electric endorse printing             */
#define     MF_ST_E_ENDORSEMENT_BACK        MF_ST_E_ENDORSEMENT     /* Electric endorse printing(back side)  */
#define     MF_ST_E_ENDORSEMENT_FRONT       0x0008                  /* Electric endorse printing(front side) */
#define     MF_ST_PHYSICAL_ENDORSEMENT      0x0010                  
#define     MF_ST_FEEDER                    0x0020                  

/*========================================================*/
/* BiPrintBarCode()                                       */
/*========================================================*/
/*------------------------------------*/
/* Symbol                             */
/*------------------------------------*/
#define     CODE_UPCA                                    101
#define     CODE_UPCE                                    102
#define     CODE_EAN8                                    103
#define     CODE_EAN13                                   104
#define     CODE_ITF                                     106
#define     CODE_CODABAR                                 107
#define     CODE_CODE39                                  108
#define     CODE_CODE93                                  109
#define     CODE_CODE128                                 110
#define     CODE_GS1_128                                 111
#define     CODE_GS1_DATABAR_OMINIDIRECTIONAL            112
#define     CODE_GS1_DATABAR_TRUNCATED                   113
#define     CODE_GS1_DATABAR_LIMITED                     114
#define     CODE_GS1_DATABAR_EXPANDED                    115
#define     CODE_PDF417                                  116
#define     CODE_QRCODE                                  117
#define     CODE_MAXICODE                                118
#define     CODE_GS1_DATABAR_STACKED                     119
#define     CODE_GS1_DATABAR_STACKED_OMINIDIRECTIONAL    120
#define     CODE_GS1_DATABAR_EXPANDED_STACKED            121
#define     CODE_COMPOSIT                                0x10000000
/*------------------------------------*/
/* Text Position                      */
/*------------------------------------*/
#define     HRI_NONE                        -11
#define     HRI_ABOVE                       -12
#define     HRI_BELOW                       -13

/*========================================================*/
/* BiSetPrintAlignment()                                  */
/*========================================================*/
#define     MF_PRINT_ALIGNMENT_LEFT         -1
#define     MF_PRINT_ALIGNMENT_CENTER       -2
#define     MF_PRINT_ALIGNMENT_RIGHT        -3

/*========================================================*/
/* BiBufferedPrint()                                      */
/*========================================================*/
#define     MF_PRT_BUFFERING                1
#define     MF_PRT_EXEC                     2
#define     MF_PRT_CLEAR                    3

/*========================================================*/
/* BiSelectJamDetect()                                    */
/*========================================================*/
#define     EPS_BI_JAM_DETECT_USE_UPPER_LEFT_SENSOR         0
#define     EPS_BI_JAM_DETECT_NOT_USE_UPPER_LEFT_SENSOR     1

/*========================================================*/
/* BiRingBuzzer()                                         */
/*========================================================*/
#define     MF_BUZZER_TONE_HIGH             0
#define     MF_BUZZER_TONE_MIDDLE           2
#define     MF_BUZZER_TONE_LOW              1

/*========================================================*/
/* BiSCNSetBehaviorToScnResult()                          */
/*========================================================*/
/*------------------------------------*/
/* Next Check                         */
/*------------------------------------*/
#define     MF_PROCESS_CONTINUE_OVERLAP     1
#define     MF_PROCESS_CONTINUE_NOOVERLAP   2
#define     MF_PROCESS_CONTINUE_CANCEL      3

/*========================================================*/
/* BiGetVersion()                                         */
/*========================================================*/
/*------------------------------------*/
/* Driver Type                        */
/*------------------------------------*/
#define     DRIVER_TYPE_J9000               1
#define     DRIVER_TYPE_S1000               2
#define     DRIVER_TYPE_S9000               4
#define     DRIVER_TYPE_S2000               5
/*------------------------------------*/
/* Type                               */
/*------------------------------------*/
#define     VERSION_TYPE_DRIVER             0x00000001
#define     VERSION_TYPE_USB                0x00000002
#define     VERSION_TYPE_MICR               0x00000003
#define     VERSION_TYPE_MICR_E13B          0x00000003
#define     VERSION_TYPE_MICR_CMC7          0x00010003
#define     VERSION_TYPE_OCR                0x00000004
#define     VERSION_TYPE_IMAGE              0x00000005
#define     VERSION_TYPE_IQA                0x00000006
#define     VERSION_TYPE_BARCODE            0x00000007
#define     VERSION_TYPE_PH                 0x00000011
#define     VERSION_TYPE_MICR_PARSING       0x00000012
#define     VERSION_TYPE_BMPTORASTER        0x00000014

/*========================================================*/
/* BiMICRClearSpaces()                                    */
/*========================================================*/
#define     CLEAR_SPACE_DISABLE             0
#define     CLEAR_SPACE_ENABLE              1

/*========================================================*/
/* BiSetOcrABAreaOrigin()                                 */
/*========================================================*/
#define     OCR_ORIGIN_TOP_LEFT             0
#define     OCR_ORIGIN_BOTTOM_LEFT          1
#define     OCR_ORIGIN_TOP_RIGHT            2
#define     OCR_ORIGIN_BOTTOM_RIGHT         3

/*========================================================*/
/* BiSetEndorseDirection()                                */
/*========================================================*/
#define     EENDORSE_DIRECTION_LEFTRIGHT    1
#define     EENDORSE_DIRECTION_TOPBOTTOM    2
#define     EENDORSE_DIRECTION_RIGHTLEFT    3
#define     EENDORSE_DIRECTION_BOTTOMTOP    4

/*========================================================*/
/* BiSCNSetImageTypeOption()                              */
/*========================================================*/
#define     EPS_BI_SCN_OPTION_COLOR                     0
#define     EPS_BI_SCN_OPTION_IR                        1
#define     EPS_BI_SCN_OPTION_DROPOUT_RED               2
#define     EPS_BI_SCN_OPTION_DROPOUT_GREEN             3
#define     EPS_BI_SCN_OPTION_DROPOUT_BLUE              4
#define     EPS_BI_SCN_OPTION_COLOR_ENHANCEMENT_RED     5
#define     EPS_BI_SCN_OPTION_COLOR_ENHANCEMENT_GREEN   6
#define     EPS_BI_SCN_OPTION_COLOR_ENHANCEMENT_BLUE    7
#define     EPS_BI_SCN_OPTION_COLOR_IR                  8
#define     EPS_BI_SCN_OPTION_GRAYSCALE                 9
#define     EPS_BI_SCN_OPTION_GRAYSCALE_IR              10
#define     EPS_BI_SCN_OPTION_DROPOUT_RED_IR            11
#define     EPS_BI_SCN_OPTION_DROPOUT_GREEN_IR          12
#define     EPS_BI_SCN_OPTION_DROPOUT_BLUE_IR           13

/*========================================================*/
/* BiSCNSetImageAdjustment()                              */
/*========================================================*/
/*------------------------------------*/
/* Brightness                         */
/*------------------------------------*/
#define     EPS_BI_SCN_BRIGHTNESS_NONE      0
#define     EPS_BI_SCN_BRIGHTNESS_DEFAULT   EPS_BI_SCN_BRIGHTNESS_NONE
/*------------------------------------*/
/* Contrast                           */
/*------------------------------------*/
#define     EPS_BI_SCN_CONTRAST_NONE        0
#define     EPS_BI_SCN_CONTRAST_DEFAULT     EPS_BI_SCN_CONTRAST_NONE
/*------------------------------------*/
/* Gamma                              */
/*------------------------------------*/
#define     EPS_BI_SCN_GAMMA_NONE           0
#define     EPS_BI_SCN_GAMMA_10             10
#define     EPS_BI_SCN_GAMMA_18             18
#define     EPS_BI_SCN_GAMMA_22             22
#define     EPS_BI_SCN_GAMMA_DEFAULT        EPS_BI_SCN_GAMMA_NONE

/*========================================================*/
/* BiSCNMICRCancelFunction                                */
/*========================================================*/
#define     MF_EJECT_DISCHARGE              0x0000
#define     MF_EJECT_RELEASE                0x0001

/*========================================================*/
/* BiSetWaterfallMode()                                   */
/*========================================================*/
#define     WATERFALL_MODE_DISABLE          0
#define     WATERFALL_MODE_STANDARD         1
#define     WATERFALL_MODE_INHERIT_POCKET   2

/*========================================================*/
/* BiSelectErrorEjectAtContinuously()                     */
/*========================================================*/
/*------------------------------------*/
/* Error                              */
/*------------------------------------*/
#define     EPS_BI_ERROREJECT_ERROR_ALL              ES_CONTINUE_ALL
#define     EPS_BI_ERROREJECT_ERROR_DOUBLEFEED       ES_CONTINUE_DOUBLEFEED
#define     EPS_BI_ERROREJECT_ERROR_NODATA           ES_CONTINUE_NODATA
#define     EPS_BI_ERROREJECT_ERROR_BADDATA          ES_CONTINUE_BADDATA
#define     EPS_BI_ERROREJECT_ERROR_NOISE            ES_CONTINUE_NOISE
/*------------------------------------*/
/* Setting                            */
/*------------------------------------*/
#define     EPS_BI_ERROREJECT_SETTING_DISCHARGE      MF_EJECT_DISCHARGE
#define     EPS_BI_ERROREJECT_SETTING_RELEASE        MF_EJECT_RELEASE

/*========================================================*/
/* BiAutoCutRollPaper()                                   */
/*========================================================*/
#define     AUTOCUT_CUT                   0
#define     AUTOCUT_FEEDANDCUT            1

/*========================================================*/
/* BiSetPrintImageMethod()                                */
/*========================================================*/
#define     PRINTIMAGEMETHOD_BW           0
#define     PRINTIMAGEMETHOD_MULTITONE    1

/*========================================================*/
/* BiOpenDrawer()                                         */
/*========================================================*/
/*------------------------------------*/
/* Drawer Number                      */
/*------------------------------------*/
#define     EPS_BI_DRAWER_1               1
/*------------------------------------*/
/* Start Time                         */
/*------------------------------------*/
#define     EPS_BI_PLUSE_100              1
#define     EPS_BI_PLUSE_200              2
#define     EPS_BI_PLUSE_300              3
#define     EPS_BI_PLUSE_400              4
#define     EPS_BI_PLUSE_500              5
#define     EPS_BI_PLUSE_600              6
#define     EPS_BI_PLUSE_700              7
#define     EPS_BI_PLUSE_800              8

/*========================================================*/
/* BiTemplatePrint()                                      */
/*========================================================*/
#define     TEMPLATEPRINT_BUFFERING         0
#define     TEMPLATEPRINT_EXEC              1
#define     TEMPLATEPRINT_CLEAR             2
#define     TEMPLATEPRINT_EXEC_NOCLEAR      3

/*========================================================*/
/* BiSetTemplatePrintArea()                               */
/* BiClearTemplatePrintArea()                             */
/*========================================================*/
#define     TRANS_NO_SPECIFIED              0x40000000

/*========================================================*/
/* BiSetPrintCutSheetSettings()                           */
/*========================================================*/
#define     BI_PRINTCUTSHEET_SCAN         0
#define     BI_PRINTCUTSHEET_NOSCAN       1

/*========================================================*/
/* BiSetPrintArea()                                       */
/*========================================================*/
#define     SELECTPRINTAREA_AREANAME      0
#define     SELECTPRINTAREA_DIRECT        1

/*========================================================*/
/* BiSCNSelectScanImage()                                 */
/*========================================================*/
#define     MF_SCAN_IMAGE_VISIBLE         0
#define     MF_SCAN_IMAGE_INFRARED        1

/*========================================================*/
/* BiESCNEnable()                                         */
/*========================================================*/
/*------------------------------------*/
/* Saved Method                       */
/*------------------------------------*/
#define     CROP_STORE_MEMORY               0           /* Saved memory */
#define     CROP_STORE_FILE                 1           /* Saved file   */

/*========================================================*/
/* BiESCNSetAutoSize()                                    */
/*========================================================*/
/*------------------------------------*/
/* Enable/Disable Autosize            */
/*------------------------------------*/
#define     CROP_AUTOSIZE_DISABLE           0           /* Disabled auto size  */
#define     CROP_AUTOSIZE_ENABLE            1           /* Enabled auto size */

/*========================================================*/
/* BiESCNSetRotate()                                      */
/*========================================================*/
/*------------------------------------*/
/* Enable/Disable Rotate              */
/*------------------------------------*/
#define     CROP_ROTATE_DISABLE             0                            /* Enabled rotate */
#define     CROP_ROTATE_ENABLE              1                            /* Disabled rotate */

/*========================================================*/
/* BiESCNGetDeSkew(), BiESCNSetDeSkew()                   */
/*========================================================*/
#define     DESKEW_ALL                      0
#define     DESKEW_DISABLE                  65535

/*========================================================*/
/* BiESCNDefineCropArea()                                 */
/*========================================================*/
/*------------------------------------*/
/* CropArea Setting                   */
/*------------------------------------*/
#define     CROP_AREA_RESET_ALL             0           /* Delete all crop area  */
#define     CROP_AREA_ENTIRE_IMAGE          1           /* CropArea entire image */
#define     CROP_AREA_RIGHT                 65535       /* End X Point           */
#define     CROP_AREA_BOTTOM                65535       /* End Y Point           */

/*========================================================*/
/* BiESCNClearImage()                                     */
/*========================================================*/
/*------------------------------------*/
/* Delete Method                      */
/*------------------------------------*/
#define     CROP_CLEAR_ALL_IMAGE            0           /* Delete all image   */
#define     CROP_CLEAR_BY_FILEINDEX         1           /* Clear by fileIndex */
#define     CROP_CLEAR_BY_FILEID            2           /* Clear by fileId    */
#define     CROP_CLEAR_BY_IMAGETAGDATA      4           /* by imageTagData    */

/*========================================================*/
/* BiSCNMICRFunction                                      */
/*========================================================*/
/*------------------------------------*/
/* Message ID                         */
/*------------------------------------*/
#define     WM_MF_DONE                      (WM_USER +1)
#define     WM_MF_PROGRESS                  (WM_USER +2)

/*------------------------------------*/
/* Param                              */
/*------------------------------------*/
#define     MF_MACRO_GETUNITID(wParam)      LOBYTE(HIWORD(wParam))
#define     MF_MACRO_GETPHASE(wParam)       LOBYTE(LOWORD(wParam))
#define     MF_MACRO_GETFACE(lParam)        LOBYTE(HIWORD(lParam))
#define     MF_MACRO_PERCENT(lParam)        LOBYTE(LOWORD(lParam))

#define     MF_PHASE_INIT                   0
#define     MF_PHASE_MICR                   1
#define     MF_PHASE_SCAN                   2
#define     MF_PHASE_PRINT                  3
#define     MF_PHASE_EXIT                   4
#define     MF_PHASE_BARCODE                5

#define     MF_PROGRESS_START               0
#define     MF_PROGRESS_DONE                100
#define     MF_PROGRESS_WAIT_PAPER          101
#define     MF_PROGRESS_CLUMP_PAPER         102
#define     MF_PROGRESS_PAPER_PILED         103

/*========================================================*/
/* BiSCNMICRSetStatusBackFunction                         */
/*========================================================*/
/*------------------------------------*/
/* wMainStatus                        */
/*------------------------------------*/
#define     MF_FUNCTION_START               200
#define     MF_CHECKPAPER_PROCESS_START     201
#define     MF_DATARECEIVE_START            202
#define     MF_DATARECEIVE_DONE             203
#define     MF_CHECKPAPER_PROCESS_DONE      204
#define     MF_FUNCTION_DONE                205
#define     MF_DATARECEIVE_MICR_DONE        206
#define     MF_ERROR_OCCURED                299

/*========================================================*/
/* BiSCNMICRFunction                                      */
/* BiSCNMICRFunctionContinuously                          */
/* BiSCNMICRFunctionPostPrint                             */
/* BiPrintCutSheet                                        */
/*========================================================*/
/*------------------------------------*/
/* wFunction                          */
/*------------------------------------*/
#define     MF_EXEC                                 0x0000
#define     MF_CONTINUE                             0x0001
#define     MF_MICR_RETRANS                         0x0002
#define     MF_SCAN_FRONT_RETRANS                   0x0003
#define     MF_SCAN_BACK_RETRANS                    (MF_SCAN_FRONT_RETRANS + 1)

#define     MF_SET_BASE_PARAM                       0x0010
#define     MF_SET_MICR_PARAM                       0x0011
#define     MF_SET_SCAN_FRONT_PARAM                 0x0012
#define     MF_SET_SCAN_PARAM                       0x0012
#define     MF_SET_SCAN_BACK_PARAM                  (MF_SET_SCAN_FRONT_PARAM + 1)
#define     MF_SET_PRINT_PARAM                      0x0014
#define     MF_SET_PROCESS_PARAM                    0x0015
#define     MF_SET_IQA_PARAM                        0x0016
#define     MF_SET_BARCODE_FRONT_PARAM              0x0017
#define     MF_SET_BARCODE_PARAM                    0x0017
#define     MF_SET_BARCODE_BACK_PARAM               (MF_SET_BARCODE_FRONT_PARAM + 1)

#define     MF_CLEAR_BASE_PARAM                     0x0020
#define     MF_CLEAR_MICR_PARAM                     0x0021
#define     MF_CLEAR_SCAN_FRONT_PARAM               0x0022
#define     MF_CLEAR_SCAN_PARAM                     0x0022
#define     MF_CLEAR_SCAN_BACK_PARAM                (MF_CLEAR_SCAN_FRONT_PARAM + 1)
#define     MF_CLEAR_PRINT_PARAM                    0x0024
#define     MF_CLEAR_PROCESS_PARAM                  0x0025
#define     MF_CLEAR_IQA_PARAM                      0x0026
#define     MF_CLEAR_BARCODE_FRONT_PARAM            0x0027
#define     MF_CLEAR_BARCODE_PARAM                  0x0027
#define     MF_CLEAR_BARCODE_BACK_PARAM             (MF_CLEAR_BARCODE_FRONT_PARAM + 1)

#define     MF_GET_BASE_DEFAULT                     0x0030
#define     MF_GET_MICR_DEFAULT                     0x0031
#define     MF_GET_SCAN_DEFAULT                     0x0032
#define     MF_GET_SCAN_FRONT_DEFAULT               0x0032
#define     MF_GET_SCAN_BACK_DEFAULT                (MF_GET_SCAN_FRONT_DEFAULT + 1)
#define     MF_GET_PRINT_DEFAULT                    0x0034
#define     MF_GET_PROCESS_DEFAULT                  0x0035
#define     MF_GET_IQA_DEFAULT                      0x0036
#define     MF_GET_BARCODE_DEFAULT                  0x0037
#define     MF_GET_BARCODE_FRONT_DEFAULT            0x0037
#define     MF_GET_BARCODE_BACK_DEFAULT             (MF_GET_BARCODE_FRONT_DEFAULT + 1)

#define     MF_SET_SCAN_PRINTAREA_PARAM             MF_SET_SCAN_BACK_PARAM
#define     MF_SET_SCAN_NOPRINTAREA_PARAM           MF_SET_SCAN_FRONT_PARAM
#define     MF_CLEAR_SCAN_PRINTAREA_PARAM           MF_CLEAR_SCAN_BACK_PARAM
#define     MF_CLEAR_SCAN_NOPRINTAREA_PARAM         MF_CLEAR_SCAN_FRONT_PARAM
#define     MF_GET_SCAN_PRINTAREA_DEFAULT           MF_GET_SCAN_BACK_DEFAULT
#define     MF_GET_SCAN_NOPRINTAREA_DEFAULT         MF_GET_SCAN_FRONT_DEFAULT

/*========================================================*/
/* BiLoadAPISettings()                                    */
/*========================================================*/
/*------------------------------------*/
/* Default                            */
/*------------------------------------*/
#define APISETTINGS_DEFAULT "APISETTINGSDEFAULT"

/*========================================================*/
/* BiSetConfigure()                                       */
/*========================================================*/
/*------------------------------------*/
/* Confing ID                         */
/*------------------------------------*/
#define     MF_CONFIG_ID_2ND_RECEIPT              0
#define     MF_CONFIG_ID_SCAN_LENGTH              1
#define     MF_CONFIG_ID_MICR_RECOG               2
#define     MF_CONFIG_ID_ENDORSE_PRINT_POSITION   3
/*------------------------------------*/
/* Config Type (2nd Reciept)          */
/*------------------------------------*/
#define     MF_CONFIG_TYPE_2ND_RECEIPT_DISABLE    0
#define     MF_CONFIG_TYPE_2ND_RECEIPT_ENABLE     1
/*------------------------------------*/
/* Config Type (Scan Length)          */
/*------------------------------------*/
#define     MF_CONFIG_TYPE_SCAN_LENGTH_NORMAL     0
#define     MF_CONFIG_TYPE_SCAN_LENGTH_EXPAND     1
/*------------------------------------*/
/* Config Type (MICR Recog)          */
/*------------------------------------*/
#define     MF_CONFIG_TYPE_MICR_RECOG_ANSI        0
#define     MF_CONFIG_TYPE_MICR_RECOG_CCCC        1
/*------------------------------------*/
/* Config Type (Endorse Print Position )*/
/*------------------------------------*/
#define     MF_CONFIG_TYPE_ENDORSE_PRINT_POSITION_NORMAL        0
#define     MF_CONFIG_TYPE_ENDORSE_PRINT_POSITION_PAYEE_ENDORSE        1


/******************************************************************************/
/******************************************************************************/
/* MF Fuction Structure                                                       */
/******************************************************************************/
/******************************************************************************/
#if defined(WIN32)
#pragma pack(push, 1)   /* set structure packing to 1 */
#endif

/*========================================================*/
/* Base parameters                                        */
/*========================================================*/
/*------------------------------------*/
/* struct version(for iVersion)       */
/*------------------------------------*/
#define     MF_FIRST_VERSION                0x0100
#define     MF_BASE_VERSION01               0x0101
#define     MF_BASE_VERSION                 MF_FIRST_VERSION
/*------------------------------------*/
/* Event notice type(for dwNotifyType)*/
/*------------------------------------*/
#define     MF_BASE_MESSAGE_NO_MESSAGE      1
#define     MF_BASE_MESSAGE_EVENT           2
#define     MF_BASE_MESSAGE_HWND            3
#define     MF_BASE_MESSAGE_BUTTON_CLICK    4
#define     MF_BASE_MESSAGE_NOTIFY_MAX      MF_BASE_MESSAGE_BUTTON_CLICK
/*------------------------------------*/
/* Paper insertion timeout            */
/* (for dwTimeout)                    */
/*------------------------------------*/
#define     MF_BASE_TIMEOUT_MAX             300     /* 5 min    */
#define     MF_BASE_TIMEOUT_DEFAULT         0       /* INFINITE */
/*------------------------------------*/
/* NV memory use type                 */
/* (for bUseNVMemory)                 */
/*------------------------------------*/
#define     MF_BASE_NVMEMORY_NOT_USE        0
#define     MF_BASE_NVMEMORY_USE            1
/*------------------------------------*/
/* Paper eject type                   */
/* (for wErrorEject and wSuccessEject)*/
/*------------------------------------*/
#define     MF_EXIT_ERROR_DISCHARGE             MF_EJECT_DISCHARGE
#define     MF_EXIT_ERROR_RELEASE               MF_EJECT_RELEASE
#define     MF_EXIT_SUCCESS_DISCHARGE           MF_EJECT_DISCHARGE
#define     MF_EXIT_SUCCESS_RELEASE             MF_EJECT_RELEASE
#define     MF_EXIT_ERROR_CONTINUE_DISCHARGE    0x0010
#define     MF_EXIT_ERROR_CONTINUE_RELEASE      0x0011
/*------------------------------------*/
/* Buzzer factor type(for bBuzzerHz and bBuzzerCount)  */
/*------------------------------------*/
#define     MF_BUZZER_TYPE_SUCCESS          0
#define     MF_BUZZER_TYPE_ERROR            1
#define     MF_BUZZER_TYPE_WFEED            2
#define     MF_BUZZER_TYPE_MAX              3
/*------------------------------------*/
/* Buzzer type(for bBuzzerHz)         */
/*------------------------------------*/
#define     MF_BUZZER_HZ_4000               0
#define     MF_BUZZER_HZ_440                1
#define     MF_BUZZER_HZ_880                2
/*------------------------------------*/
/* Buzzer count(for bBuzzerCount)     */
/*------------------------------------*/
#define     MF_BUZZER_DISABLE               0
#define     MF_BUZZER_COUNT_MAX             3
/*------------------------------------*/
/* Base Structure(Version 0x0101)     */
/*------------------------------------*/
typedef struct {
/* Base Section */
    int             iSize;
    int             iVersion;
    int             iRet;
    DWORD           dwNotifyType;
    DWORD           dwTimeout;
    union {
        LPHANDLE        lphNotifyEvent;
        HWND            hNotifyWnd;
    } uNotifyHandle;
    HWND            hProgressWnd;
    WORD            wErrorEject;
    BYTE            bBuzzerHz[MF_BUZZER_TYPE_MAX];
    BYTE            bBuzzerCount[MF_BUZZER_TYPE_MAX];
    BYTE            bUseNVMemory;
    char            cPortName[256];
    WORD            wSuccessEject;
} MF_BASE01, *LPMF_BASE01;
/*------------------------------------*/
/* Base Structure(OLD:Version 0x0100) */
/*------------------------------------*/
typedef struct {
/* Base Section */
    int             iSize;
    int             iVersion;
    int             iRet;
    DWORD           dwNotifyType;
    DWORD           dwTimeout;
    union {
        LPHANDLE        lphNotifyEvent;
        HWND            hNotifyWnd;
    } uNotifyHandle;
    HWND            hProgressWnd;
    WORD            wErrorEject;
    BYTE            bBuzzerHz[MF_BUZZER_TYPE_MAX];
    BYTE            bBuzzerCount[MF_BUZZER_TYPE_MAX];
    BYTE            bUseNVMemory;
    char            cPortName[256];
} MF_BASE, *LPMF_BASE;

/*========================================================*/
/* Scan parameters                                        */
/*========================================================*/
/*------------------------------------*/
/* Structure version                  */
/*------------------------------------*/
#define     MF_SCAN_VERSION                 0x0100
/*------------------------------------*/
/* Scan resolution(for sResolution)   */
/*------------------------------------*/
#define     MF_SCAN_DPI_DEFAULT             0
#define     MF_SCAN_DPI_100                 100
#define     MF_SCAN_DPI_120                 120
#define     MF_SCAN_DPI_200                 200
#define     MF_SCAN_DPI_240                 240
#define     MF_SCAN_DPI_300                 300
#define     MF_SCAN_DPI_600                 600
/*------------------------------------*/
/* Scan structure                     */
/*------------------------------------*/
typedef struct{
/* Scan Section */
    int             iSize;
    int             iVersion;
    int             iRet;
    WORD            wImageID;
    short           sResolution;
    BYTE            bAddInfoDataSize;
    LPBYTE          pAddInfoData;
    BYTE            bStatus;
    BYTE            bDetail;
    DWORD           dwXSize;
    DWORD           dwYSize;
    DWORD           dwScanSize;
    LPBYTE          lpbScanData;
} MF_SCAN, *LPMF_SCAN;

/*========================================================*/
/* MICR parameters                                        */
/*========================================================*/
/*------------------------------------*/
/* Structure version(for iVersion)    */
/*------------------------------------*/
#define     MF_MICR_VERSION                 0x0100
#define     MF_MICR01_VERSION               0x0101
#define     MF_MICR_VERSION01               0x0101
/*------------------------------------*/
/* MICR type(for bMicOcrSelect)       */
/*------------------------------------*/
#define     MF_MICR_USE_MICR                0x00000001
#define     MF_MICR_USE_OCR                 0x00000002
/*------------------------------------*/
/* Font(for bFont)                    */
/*------------------------------------*/
#define     MF_MICR_FONT_E13B               0
#define     MF_MICR_FONT_CMC7               1

#define     MF_MICR_CHAR_MAX                81
#define     MF_MICR_CHAR_LEN                80
/*------------------------------------*/
/* Reliability structure              */
/*------------------------------------*/
typedef struct{
    CHAR        cRecogChar;         /* Recognized character */
    LONG        lPercentage;        /* Reliability(%)       */
} MF_OCR_RELIABILITY;
/*------------------------------------*/
/* Reliability information structure  */
/*------------------------------------*/
typedef struct{
    LONG                lPosition;          /* Position(0 is left edge)     */
    MF_OCR_RELIABILITY  stFirstSelect;      /* First recognition candidate  */
    MF_OCR_RELIABILITY  stSecondSelect;     /* Second recognition candidate */
} MF_OCR_RELIABLE_INFO;
/*------------------------------------*/
/* MICR/OCR structure(Version 0x0101) */
/*------------------------------------*/
typedef struct{
/* MIC_OCR Section */
    int                     iSize;
    int                     iVersion;
    int                     iRet;
    BYTE                    bFont;
    BYTE                    bMicOcrSelect;
    BOOL                    blParsing;
    BYTE                    bStatus;
    BYTE                    bDetail;
    CHAR                    szMicrStr[MF_MICR_CHAR_MAX];
    MF_OCR_RELIABLE_INFO    stOcrReliableInfo[MF_MICR_CHAR_MAX];
    CHAR                    szAccountNumber[MF_MICR_CHAR_MAX];
    CHAR                    szAmount[MF_MICR_CHAR_MAX];
    CHAR                    szBankNumber[MF_MICR_CHAR_MAX];
    CHAR                    szSerialNumber[MF_MICR_CHAR_MAX];
    CHAR                    szEPC[MF_MICR_CHAR_MAX];
    CHAR                    szTransitNumber[MF_MICR_CHAR_MAX];
    long                    lCheckType;
    long                    lCountryCode;
    CHAR                    szOnUSField[MF_MICR_CHAR_MAX];
    CHAR                    szAuxiliatyOnUsField[MF_MICR_CHAR_MAX];
} MF_MICR01, *LPMF_MICR01;
/*------------------------------------*/
/* MICR/OCR structure(OLD:Version 0x0100)*/
/*------------------------------------*/
typedef struct{
/* MIC_OCR Section */
    int                     iSize;
    int                     iVersion;
    int                     iRet;
    BYTE                    bFont;
    BYTE                    bMicOcrSelect;
    BOOL                    blParsing;
    BYTE                    bStatus;
    BYTE                    bDetail;
    CHAR                    szMicrStr[MF_MICR_CHAR_MAX];
    MF_OCR_RELIABLE_INFO    stOcrReliableInfo[MF_MICR_CHAR_MAX];
    CHAR                    szAccountNumber[MF_MICR_CHAR_MAX];
    CHAR                    szAmount[MF_MICR_CHAR_MAX];
    CHAR                    szBankNumber[MF_MICR_CHAR_MAX];
    CHAR                    szSerialNumber[MF_MICR_CHAR_MAX];
    CHAR                    szEPC[MF_MICR_CHAR_MAX];
    CHAR                    szTransitNumber[MF_MICR_CHAR_MAX];
    long                    lCheckType;
    long                    lCountryCode;
} MF_MICR, *LPMF_MICR;

/*========================================================*/
/* OCR_AB parameters                                      */
/*========================================================*/
/*------------------------------------*/
/* Structure version(for iVersion)    */
/*------------------------------------*/
#define     MF_OCR_AB_VERSION               0x0100
/*------------------------------------*/
/* Ocr Type                           */
/*------------------------------------*/
#define     MF_OCR_FONT_OCRA_NUM                1
#define     MF_OCR_FONT_OCRA_ALPHA              2
#define     MF_OCR_FONT_OCRA_ALPHANUM           3
#define     MF_OCR_FONT_OCRA_ALPHANUM_WOOH      7
#define     MF_OCR_FONT_OCRA_ALPHANUM_WOZERO    11
#define     MF_OCR_FONT_OCRB_NUM                17
#define     MF_OCR_FONT_OCRB_ALPHA              18
#define     MF_OCR_FONT_OCRB_ALPHANUM           19
#define     MF_OCR_FONT_OCRB_ALPHANUM_WOOH      23
#define     MF_OCR_FONT_OCRB_ALPHANUM_WOZERO    27
#define     MF_OCR_FONT_OCRA_SYMNUM             33
#define     MF_OCR_FONT_OCRB_SYMNUM             49
/*------------------------------------*/
/* Direction                          */
/*------------------------------------*/
#define     MF_OCR_LEFTRIGHT                1
#define     MF_OCR_TOPBOTTOM                2
#define     MF_OCR_RIGHTLEFT                3
#define     MF_OCR_BOTTOMTOP                4
/*------------------------------------*/
/* Area(for bEndX, bEndY)             */
/*------------------------------------*/
#define     OCR_AREA_RIGHT                  65535
#define     OCR_AREA_BOTTOM                 65535
#define     OCR_AREA_TOP                    65535
#define     OCR_AREA_LEFT                   65535
/*------------------------------------*/
/* SpeceHandling(for bSpeceHandling)  */
/*------------------------------------*/
#define     OCR_SPACE_ENABLE                1
#define     OCR_SPACE_DISABLE               0

#define     MF_OCR_AB_CHAR_MAX              128
/*------------------------------------*/
/* OCR AB structure                   */
/*------------------------------------*/
typedef struct{
    int                     iSize;
    int                     iVersion;
    int                     iRet;
    BYTE                    bOcrType;
    BYTE                    bDirection;
    WORD                    wStartX;
    WORD                    wStartY;
    WORD                    wEndX;
    WORD                    wEndY;
    BYTE                    bSpaceHandling;
    CHAR                    szOcrStr[MF_OCR_AB_CHAR_MAX];
    MF_OCR_RELIABLE_INFO    stOcrReliableInfo[MF_OCR_AB_CHAR_MAX];
} MF_OCR_AB, *LPMF_OCR_AB;

/*========================================================*/
/* Process parameters                                     */
/*========================================================*/
/*------------------------------------*/
/* Structure version(for iVersion)    */
/*------------------------------------*/
#define     MF_PROCESS_VERSION              0x0100
#define     MF_PROCESS01_VERSION            0x0101
#define     MF_PROCESS_VERSION01            0x0101
/*------------------------------------*/
/* Activation mode(For bActivationMode) */
/*------------------------------------*/
#define     MF_ACTIVATE_MODE_CONFIRMATION   0
#define     MF_ACTIVATE_MODE_HIGH_SPEED     1
/*------------------------------------*/
/* Paper type(For bPaperType)         */
/*------------------------------------*/
#define     MF_PAPER_TYPE_OTHER             0
#define     MF_PAPER_TYPE_CHECK             1
/*------------------------------------*/
/* Stamp status(For bSuccessStamp, bPaperMisInsertionStamp, bNoiseStamp */
/*              , bDoubleFeedStamp, bBaddataStamp, bNodataStamp)        */
/*------------------------------------*/
#define     MF_STAMP_DISABLE                0
#define     MF_STAMP_ENABLE                 1
/*------------------------------------*/
/* Error detect(For bPaperMisInsertionErrorSelect, bNoiseErrorSelect               */
/*              , bDoubleFeedErrorSelect, bBaddataErrorSelect, bNodataErrorSelect) */
/*------------------------------------*/
#define     MF_ERROR_SELECT_NODETECT        0
#define     MF_ERROR_SELECT_DETECT          1
/*------------------------------------*/
/* Eject select(For bPaperMisInsertionErrorEject, bNoiseErrorEject              */
/*              , bDoubleFeedErrorEject, bBaddataErrorEject, bNodataErrorEject) */
/*------------------------------------*/
#define     MF_EJECT_MAIN_POCKET            0x22
#define     MF_EJECT_SUB_POCKET             0x24
#define     MF_EJECT_NOEJECT                0x28
/*------------------------------------*/
/* Cancel status(For bPaperMisInsertionCancel, bNoiseCancel, bDoubleFeedCancel */
/*              , bBaddataCancel, bNodataCancel)                               */
/*------------------------------------*/
#define     MF_CANCEL_DISABLE               0
#define     MF_CANCEL_ENABLE                1
/*------------------------------------*/
/* Nearfull parmit(For bNearFullSelect) */
/*------------------------------------*/
#define     MF_NEARFULL_NOT_PERMIT          0x00
#define     MF_NEARFULL_MAIN_PERMIT         0x01
#define     MF_NEARFULL_SUB_PERMIT          0x02
#define     MF_NEARFULL_PERMIT              0x03
/*------------------------------------*/
/* Result partial(For bResultPartialData) */
/*------------------------------------*/
#define     MF_RESULT_NONE                  0
#define     MF_RESULT_PARTIAL               1
/*------------------------------------*/
/* Endorse Print Mode(For bEndorsePrintMode) */
/*------------------------------------*/
#define     MF_ENDORSEPRINT_MODE_HIGHSPEED      0
#define     MF_ENDORSEPRINT_MODE_DATAWAITING    1
/*------------------------------------*/
/* Process structure(Version 0x0101)  */
/*------------------------------------*/
typedef struct{
/* Process Section */
    int             iSize;
    int             iVersion;
    BYTE            bActivationMode;
    BYTE            bPaperType;
    DWORD           dwStartWaitTime;
    BYTE            bSuccessStamp;
    BYTE            bPaperMisInsertionErrorSelect;
    BYTE            bPaperMisInsertionErrorEject;
    BYTE            bPaperMisInsertionStamp;
    BYTE            bPaperMisInsertionCancel;
    BYTE            bNoiseErrorSelect;
    BYTE            bNoiseErrorEject;
    BYTE            bNoiseStamp;
    BYTE            bNoiseCancel;
    BYTE            bDoubleFeedErrorSelect;
    BYTE            bDoubleFeedErrorEject;
    BYTE            bDoubleFeedStamp;
    BYTE            bDoubleFeedCancel;
    BYTE            bBaddataErrorSelect;
    BYTE            bBaddataCount;
    BYTE            bBaddataErrorEject;
    BYTE            bBaddataStamp;
    BYTE            bBaddataCancel;
    BYTE            bNodataErrorSelect;
    BYTE            bNodataErrorEject;
    BYTE            bNodataStamp;
    BYTE            bNodataCancel;
    BYTE            bNearFullSelect;
    BYTE            bResultPartialData;
    BYTE            bEndorsePrintMode;
    BYTE            bPrnDataLenExceedErrorEject;
    BYTE            bPrnDataLenExceedCancel;
    BYTE            bPrnDataUnreceiveErrorEject;
    BYTE            bPrnDataUnreceiveCancel;

} MF_PROCESS01, *LPMF_PROCESS01;
/*------------------------------------*/
/* Process structure(OLD:Version 0x0100)*/
/*------------------------------------*/
typedef struct{
/* Process Section */
    int             iSize;
    int             iVersion;
    BYTE            bActivationMode;
    BYTE            bPaperType;
    DWORD           dwStartWaitTime;
    BYTE            bSuccessStamp;
    BYTE            bPaperMisInsertionErrorSelect;
    BYTE            bPaperMisInsertionErrorEject;
    BYTE            bPaperMisInsertionStamp;
    BYTE            bPaperMisInsertionCancel;
    BYTE            bNoiseErrorSelect;
    BYTE            bNoiseErrorEject;
    BYTE            bNoiseStamp;
    BYTE            bNoiseCancel;
    BYTE            bDoubleFeedErrorSelect;
    BYTE            bDoubleFeedErrorEject;
    BYTE            bDoubleFeedStamp;
    BYTE            bDoubleFeedCancel;
    BYTE            bBaddataErrorSelect;
    BYTE            bBaddataCount;
    BYTE            bBaddataErrorEject;
    BYTE            bBaddataStamp;
    BYTE            bBaddataCancel;
    BYTE            bNodataErrorSelect;
    BYTE            bNodataErrorEject;
    BYTE            bNodataStamp;
    BYTE            bNodataCancel;
    BYTE            bNearFullSelect;
    BYTE            bResultPartialData;
} MF_PROCESS, *LPMF_PROCESS;

/*========================================================*/
/* Print parameters                                       */
/*========================================================*/
/*------------------------------------*/
/* Structure version(for iVersion)    */
/*------------------------------------*/
#define     MF_PRINT_VERSION01              0x0101
#define     MF_PRINT_VERSION                0x0100
/*------------------------------------*/
/* Line number(for lpString,dwAttribute,wFont and wfontSize) */
/*------------------------------------*/
#define     MF_PRINT_LINE_1                 0
#define     MF_PRINT_LINE_2                 1
#define     MF_PRINT_LINE_3                 2
#define     MF_PRINT_LINE_MAX               MF_PRINT_LINE_3
/*------------------------------------*/
/* Charactor attribute(for dwAttribute) */
/*------------------------------------*/
#define     MF_PRINT_NO_ATTRIBUTE           0x00000000
#define     MF_PRINT_BOLD                   0x00000001
#define     MF_PRINT_UNDERLINE_1            0x00000010
#define     MF_PRINT_UNDERLINE_2            0x00000020
#define     MF_PRINT_BLACK                  0x00000100
#define     MF_PRINT_1ST_COLOR              0x00000100
#define     MF_PRINT_COLOR                  0x00000200
#define     MF_PRINT_2ND_COLOR              0x00000200
#define     MF_PRINT_MIXED                  0x00000300
#define     MF_PRINT_REVERSEVIDEO           0x00001000
/*------------------------------------*/
/* Font type(for wFont)               */
/*------------------------------------*/
#define     MF_PRINT_FONT_A                 1
#define     MF_PRINT_FONT_B                 2
#define     MF_PRINT_SYSTEMFONT             3
/*------------------------------------*/
/* Font size(for wFontSize)           */
/*------------------------------------*/
#define     MF_PRINT_FONT_W1_H1             0x0011
#define     MF_PRINT_FONT_W1_H2             0x0012
#define     MF_PRINT_FONT_W2_H1             0x0021
#define     MF_PRINT_FONT_W2_H2             0x0022
/*------------------------------------*/
/* Printing speed(for bSpeed)         */
/*------------------------------------*/
#define     MF_PRINT_SPEED_NORMAL           0
#define     MF_PRINT_SPEED_HIGH             1
#define     MF_PRINT_SPEED_ECONOMY          2
/*------------------------------------*/
/* PrintDirection(for bDirection)     */
/*------------------------------------*/
#define     MF_PRINT_DIRECTION_DOUBLE       0
#define     MF_PRINT_DIRECTION_SINGLE       1
#define     MF_PRINT_DERECTION_DOUBLE       0
#define     MF_PRINT_DERECTION_SINGLE       1
/*------------------------------------*/
/* Endorse type                       */
/*------------------------------------*/
#define     MF_PRINT_TYPE_ENDORSE_ONLY                  0
#define     MF_PRINT_TYPE_ENDORSE_NORMAL                1
#define     MF_PRINT_TYPE_ELECTRIC_ENDORSE_ONLY         2
#define     MF_PRINT_TYPE_ENDORSE_EXTEND                3
#define     MF_PRINT_TYPE_ELECTRIC_ENDORSE_EXTEND       4
/*------------------------------------*/
/* print structure(version 0x0101)    */
/*------------------------------------*/
typedef struct{
/* Print Section */
    int             iSize;
    int             iVersion;
    int             iRet;
    BOOL            blDummy;
    LPSTR           lpString[3];
    DWORD           dwAttribute[3];
    WORD            wFont[3];
    WORD            wFontSize[3];
    BYTE            bSpeed;
    BOOL            bDirection;
    DWORD           dwEndorseType;
} MF_PRINT01, *LPMF_PRINT01;
/*------------------------------------*/
/* print structure(OLD:version 0x0100)*/
/*------------------------------------*/
typedef struct{
/* Print Section */
    int             iSize;
    int             iVersion;
    int             iRet;
    BOOL            blElectricEndorse;
    LPSTR           lpString[3];
    DWORD           dwAttribute[3];
    WORD            wFont[3];
    WORD            wFontSize[3];
    BYTE            bSpeed;
    BOOL            bDirection;
} MF_PRINT, *LPMF_PRINT;

/*========================================================*/
/* IQA parameters                                         */
/*========================================================*/
/*------------------------------------*/
/* Structure version(for iVersion)    */
/*------------------------------------*/
#define     MF_IQA_VERSION                  0x0100
/*------------------------------------*/
/* IQA test status(For bUndersize,bOversize,bMincompressed,bMaxcompressed,    */
/*                     bFront_rear,bToolight,bToodark,bStreaks,bNoise,bFocus, */
/*                     bCorners,bEdges,bFraming,bSkew,bCarbon,bPiggyback)     */
/*------------------------------------*/
#define     MF_IQA_TEST_DISABLE             0
#define     MF_IQA_TEST_ENABLE              1
/*------------------------------------*/
/* IQA structure                      */
/*------------------------------------*/
typedef struct{
/* IQA Section */
    int             iSize;
    int             iVersion;
    BYTE            bErrorSelect;
    BYTE            bErrorEject;
    BYTE            bStamp;
    BYTE            bCancel;

    BYTE            bImageFormat;
    BYTE            bColorDepth;
    CHAR            bThreshold;
    BYTE            bColor;
    BYTE            bExOption;
    short           sResolution;

    BYTE            bUndersize;
    BYTE            bOversize;
    BYTE            bMincompressed;
    BYTE            bMaxcompressed;
    BYTE            bFront_rear;
    BYTE            bToolight;
    BYTE            bToodark;
    BYTE            bStreaks;
    BYTE            bNoise;
    BYTE            bFocus;
    BYTE            bCorners;
    BYTE            bEdges;
    BYTE            bFraming;
    BYTE            bSkew;
    BYTE            bCarbon;
    BYTE            bPiggyback;
} MF_IQA, *LPMF_IQA;

/*========================================================*/
/* IQA result parameters                                  */
/*========================================================*/
/*------------------------------------*/
/* Structure version(for iVersion)    */
/*------------------------------------*/
#define     MF_IQARESULT_VERSION            0x0100
/*------------------------------------*/
/* IQA result value                   */
/*------------------------------------*/
#define     IQARESULT_NOT_TESTED            0
#define     IQARESULT_PASS                  1
#define     IQARESULT_NOT_PASS              2
/*------------------------------------*/
/* UndersizeImage structure           */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iWidth;             /* width in tenths of inches */
    int             iHeight;            /* height in tenths of inches */
} IQARESULT_UNDERSIZE_IMAGE, *LPIQARESULT_UNDERSIZE_IMAGE;
/*------------------------------------*/
/* OversizeImage structure            */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iWidth;             /* width in tenths of inches */
    int             iHeight;            /* height in tenths of inches */
} IQARESULT_OVERSIZE_IMAGE, *LPIQARESULT_OVERSIZE_IMAGE;
/*------------------------------------*/
/* MinCompressedImageSize structure   */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iSize;              /* compressed image size in bytes */
} IQARESULT_MIN_COMPRESSED_IMAGE_SIZE, *LPIQARESULT_MIN_COMPRESSED_IMAGE_SIZE;
/*------------------------------------*/
/* MaxCompressedImageSize structure   */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iSize;              /* compressed image size in bytes */
} IQARESULT_MAX_COMPRESSED_IMAGE_SIZE, *LPIQARESULT_MAX_COMPRESSED_IMAGE_SIZE;
/*------------------------------------*/
/* FrontRearImageMismatch structure   */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iAbsWidthDiff;      /* absolute value of width difference between front and rear in tenths of inches */
    int             iAbsHeightDiff;     /* absolute value of height difference between front and rear in tenths of inches */
} IQARESULT_FRONT_REAR_IMAGE_MISMATCH, *LPIQARESULT_FRONT_REAR_IMAGE_MISMATCH;
/*------------------------------------*/
/* ImageTooLight structure            */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iBlackPixels;       /* percenage of black pixels in the image in units of 0.1 percent */
    int             iBrightness;        /* percent image brightness in units of 0.1 percent */
    int             iContrast;          /* percent image contrast in units of 0.1 percent */
} IQARESULT_IMAGE_TOO_LIGHT, *LPIQARESULT_IMAGE_TOO_LIGHT;
/*------------------------------------*/
/* ImageTooDark structure             */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iBlackPixels;       /* percenage of black pixels in the image in units of 0.1 percent */
    int             iBrightness;        /* percent image brightness in units of 0.1 percent */
} IQARESULT_IMAGE_TOO_DARK, *LPIQARESULT_IMAGE_TOO_DARK;
/*------------------------------------*/
/* HorizontalStreaksPresent structure */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iStreakCount;       /* Number of black streaks present in bitonal images */
    int             iStreakHeight;      /* Height of the largest horizontal black streak */
} IQARESULT_HORIZONTAL_STREAKS_PRESENT, *LPIQARESULT_HORIZONTAL_STREAKS_PRESENT;
/*------------------------------------*/
/* ExcessiveSpotNoise structure       */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iCount;             /* average number of spot per square inch of the image defined for bi-tonal only */
} IQARESULT_EXCESSIVE_SPOT_NOISE, *LPIQARESULT_EXCESSIVE_SPOT_NOISE;
/*------------------------------------*/
/* ImageOutOfFocus structure          */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iImageFocusScore;   /* (max video gradient) / (grey level dynamic range) * (pixel pitch) */
} IQARESULT_IMAGE_OUT_OF_FOCUS, *LPIQARESULT_IMAGE_OUT_OF_FOCUS;
/*------------------------------------*/
/* FoldedTornDocCorners structure     */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iTopLeftWidth;
    int             iTopLeftHeight;
    int             iTopRightWidth;
    int             iTopRightHeight;
    int             iBottomLeftWidth;
    int             iBottomLeftHeight;
    int             iBottomRightWidth;
    int             iBottomRightHeight;
} IQARESULT_FOLDED_TORN_DOC_CORNERS, *LPIQARESULT_FOLDED_TORN_DOC_CORNERS;
/*------------------------------------*/
/* FoldedTornDocEdges structure       */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iTopWidth;
    int             iTopHeight;
    int             iLeftWidth;
    int             iLeftHeight;
    int             iRightWidth;
    int             iRightHeight;
    int             iBottomWidth;
    int             iBottomHeight;
} IQARESULT_FOLDED_TORN_DOC_EDGES, *LPIQARESULT_FOLDED_TORN_DOC_EDGES;
/*------------------------------------*/
/* DocFramingError structure          */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iTop;
    int             iLeft;
    int             iRight;
    int             iBottom;
} IQARESULT_DOC_FRAMING_ERROR, *LPIQARESULT_DOC_FRAMING_ERROR;
/*------------------------------------*/
/* ExcessiveDocSkew structure         */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iAngle;
    int             iRange;
} IQARESULT_EXCESSIVE_DOC_SKEW, *LPIQARESULT_EXCESSIVE_DOC_SKEW;
/*------------------------------------*/
/* CarbonStripDetection structure     */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
    int             iStripHeight;
} IQARESULT_CARBON_STRIP_DETECTION, *LPIQARESULT_CARBON_STRIP_DETECTION;
/*------------------------------------*/
/* Piggyback structure                */
/*------------------------------------*/
typedef struct {
    BYTE            bResult;
} IQARESULT_PIGGYBACK, *LPIQARESULT_PIGGYBACK;
/*------------------------------------*/
/* IQA result structure               */
/*------------------------------------*/
typedef struct {
    int                                     iSize;
    int                                     iVersion;
    int                                     iRet;

    IQARESULT_UNDERSIZE_IMAGE               stUnderSize;
    IQARESULT_OVERSIZE_IMAGE                stOverSize;
    IQARESULT_MIN_COMPRESSED_IMAGE_SIZE     stMinCompressedImageSize;
    IQARESULT_MAX_COMPRESSED_IMAGE_SIZE     stMaxCompressedImageSize;
    IQARESULT_FRONT_REAR_IMAGE_MISMATCH     stFrontRearImageMismatch;
    IQARESULT_IMAGE_TOO_LIGHT               stImageTooLight;
    IQARESULT_IMAGE_TOO_DARK                stImageTooDark;
    IQARESULT_HORIZONTAL_STREAKS_PRESENT    stHorizontalStreaksPresent;
    IQARESULT_EXCESSIVE_SPOT_NOISE          stExcessiveSpotNoise;
    IQARESULT_IMAGE_OUT_OF_FOCUS            stImageOutOfFocus;
    IQARESULT_FOLDED_TORN_DOC_CORNERS       stFoldedTornDocCorners;
    IQARESULT_FOLDED_TORN_DOC_EDGES         stFoldedTornDocEdges;
    IQARESULT_DOC_FRAMING_ERROR             stDocFramingError;
    IQARESULT_EXCESSIVE_DOC_SKEW            stExcessiveDocSkew;
    IQARESULT_CARBON_STRIP_DETECTION        stCarbonStripDetection;
    IQARESULT_PIGGYBACK                     stPiggyBack;
} MF_IQA_RESULT, *LPMF_IQA_RESULT;

/*========================================================*/
/* Barcode parameters                                     */
/*========================================================*/
/*------------------------------------*/
/* Structure version(for iVersion)    */
/*------------------------------------*/
#define     MF_BARCODE_VERSION              0x0100
/*------------------------------------*/
/* Barcode symbol                     */
/*------------------------------------*/
#define     MF_BARCODE_SYMBOL_CODABAR       (0x00000001)
#define     MF_BARCODE_SYMBOL_CODE128       (0x00000002)
#define     MF_BARCODE_SYMBOL_CODE39        (0x00000004)
#define     MF_BARCODE_SYMBOL_ITF           (0x00000008)
#define     MF_BARCODE_SYMBOL_EAN_JAN       (0x00000010)
#define     MF_BARCODE_SYMBOL_UPC_A         (0x00000020)
#define     MF_BARCODE_SYMBOL_UPC_E         (0x00000040)
/*------------------------------------*/
/* Barcode direction                  */
/*------------------------------------*/
#define     MF_BARCODE_DIRECTION_ALL        0
#define     MF_BARCODE_DIRECTION_LEFTRIGHT  1
#define     MF_BARCODE_DIRECTION_TOPBOTTOM  2
#define     MF_BARCODE_DIRECTION_RIGHTLEFT  3
#define     MF_BARCODE_DIRECTION_BOTTOMTOP  4
/*------------------------------------*/
/* Barcode area origin                */
/*------------------------------------*/
#define     MF_BARCODE_ORIGIN_TOP_LEFT      0
#define     MF_BARCODE_ORIGIN_BOTTOM_LEFT   1
#define     MF_BARCODE_ORIGIN_TOP_RIGHT     2
#define     MF_BARCODE_ORIGIN_BOTTOM_RIGHT  3
/*------------------------------------*/
/* Barcode color                      */
/*------------------------------------*/
#define     MF_BARCODE_TARGET_COLOR_GRAY    0
#define     MF_BARCODE_TARGET_COLOR_RED     1
#define     MF_BARCODE_TARGET_COLOR_GREEN   2
#define     MF_BARCODE_TARGET_COLOR_BLUE    3
#define     MF_BARCODE_TARGET_COLOR_UV      4
/*------------------------------------*/
/* Area(for wEndX, wEndY)             */
/*------------------------------------*/
#define     MF_BARCODE_AREA_RIGHT           65535
#define     MF_BARCODE_AREA_BOTTOM          65535
/*------------------------------------*/
/* Barcode data                       */
/*------------------------------------*/
typedef struct {
    DWORD           dwSymbol;
    BYTE            bDirection;
    WORD            wStartX;
    WORD            wStartY;
    WORD            wEndX;
    WORD            wEndY;
    DWORD           dwDataSize;     /* data length          */
    LPVOID          pData;          /* barcode data(binary) */
} BARCODE_DATA, *LPBARCODE_DATA;
/*------------------------------------*/
/* Barcode decode information         */
/*------------------------------------*/
typedef struct {
    int     iRet;
    BYTE            bStatus;
    BYTE            bDetail;
    DWORD           dwSymbolMask;
    BYTE            bDirection;
    BYTE            bOrigin;
    WORD            wStartX;
    WORD            wStartY;
    WORD            wEndX;
    WORD            wEndY;
} BARCODE_INFO, *LPBARCODE_INFO;
/*------------------------------------*/
/* Barcode structure                  */
/*------------------------------------*/
typedef struct{
/* Barcode Section */
    int             iSize;
    int             iVersion;
    int             iRet;

    BYTE            bErrorSelect;
    BYTE            bErrorEject;
    BYTE            bStamp;
    BYTE            bCancel;

    DWORD           dwTargetColor;
    short           sResolution;
    DWORD           dwInfoMode;
    BARCODE_INFO    stInfo[5];
    BYTE            bDataCount;     /* array size for lpData */
    LPVOID          lpData;         /* array of BARCODE_DATA */
} MF_BARCODE, *LPMF_BARCODE;

/*========================================================*/
/* VERSION_INFO parameters                                */
/*========================================================*/
#define     VERSION_CHAR_MAX                64
/*------------------------------------*/
/* Version info structure             */
/*------------------------------------*/
typedef struct{
    CHAR         lpszDescription[VERSION_CHAR_MAX];
    CHAR         lpszVersion[VERSION_CHAR_MAX];
} VERSION_INFO, *LPVERSION_INFO;

/*========================================================*/
/* DECORATE parameters                                    */
/*========================================================*/
/*------------------------------------*/
/* Decorate structure                 */
/*------------------------------------*/
typedef struct {
    DWORD           dwAttribute;
    WORD            wFont;
    LPSTR           szFontName;
    WORD            wFontSize;
} MF_DECORATE, *LPMF_DECORATE;

/*========================================================*/
/* Print Area Info parameters                             */
/*========================================================*/
/*------------------------------------*/
/* Measure                            */
/*------------------------------------*/
#define     MEASURE_MM               0
#define     MEASURE_INCH_01          1
/*------------------------------------*/
/* Rotate                             */
/*------------------------------------*/
#define     IMAGEROTATE_0            0
#define     IMAGEROTATE_90           90
#define     IMAGEROTATE_180          180
#define     IMAGEROTATE_270          270
/*------------------------------------*/
/* Print Area Info structure          */
/*------------------------------------*/
typedef struct{
    char            szAreaName[128];
    int             nMeasure;
    int             nOriginX;
    int             nOriginY;
    int             nWidth;
    int             nHeight;
    int             nRotate;
}PRINTAREAINFO, *LPPRINTAREAINFO;

#if defined(WIN32)
#pragma pack(pop)    /* pop structure packing back to previous state */
#endif

#endif  /* !defined(_MULTI_FUNCTION_H) */
