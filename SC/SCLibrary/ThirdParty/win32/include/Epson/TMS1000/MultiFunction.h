/////////////////////////////////////////////////////////////////////
//
// MultiFunction.h
//
// Definition of the constants and also data type.
//
// Modification history
// ------------------------------------------------------------------
// 2006-01-13	New
// 2007-01-16	Modified
// 2007-05-21	Modified
/////////////////////////////////////////////////////////////////////

#if !defined(TMS1000_MULTI_FUNCTION_H)
#define TMS1000_MULTI_FUNCTION_H

#include <windows.h>

#pragma pack(push, 1)

// Message ID
#define WM_MF_DONE						(WM_USER +1)
#define WM_MF_PROGRESS					(WM_USER +2)

// wParam
#define MF_MACRO_GETUNITID(wParam)		LOBYTE(HIWORD(wParam))
#define MF_MACRO_GETPHASE(wParam)		LOBYTE(LOWORD(wParam))
#define MF_MACRO_GETFACE(lParam)		LOBYTE(HIWORD(lParam))
#define MF_MACRO_PERCENT(lParam)		LOBYTE(LOWORD(lParam))

#define MF_PHASE_INIT					0
#define MF_PHASE_MICR					1
#define MF_PHASE_SCAN					2
#define MF_PHASE_PRINT					3
#define MF_PHASE_EXIT					4

#define MF_PROGRESS_START				0
#define MF_PROGRESS_DONE				100
#define MF_PROGRESS_WAIT_PAPER			101
#define MF_PROGRESS_CLUMP_PAPER			102
#define MF_PROGRESS_PAPER_PILED			103

// BiSCNMICRSetStatusBackFunction or BiSCNMICRSetStatusBackWnd
// wMainStatus
#define MF_FUNCTION_START				200
#define MF_CHECKPAPER_PROCESS_START		201
#define MF_DATARECEIVE_START			202
#define MF_DATARECEIVE_DONE				203
#define MF_CHECKPAPER_PROCESS_DONE		204
#define MF_FUNCTION_DONE				205
#define MF_ERROR_OCCURED				299

// Function define
#define MF_EXEC							0x0000
#define MF_CONTINUE						0x0001
#define MF_MICR_RETRANS					0x0002
#define MF_SCAN_FRONT_RETRANS			0x0003
#define MF_SCAN_BACK_RETRANS			(MF_SCAN_FRONT_RETRANS + 1)

#define MF_SET_BASE_PARAM				0x0010
#define MF_SET_MICR_PARAM				0x0011
#define MF_SET_SCAN_FRONT_PARAM			0x0012
#define MF_SET_SCAN_PARAM				0x0012
#define MF_SET_SCAN_BACK_PARAM			(MF_SET_SCAN_FRONT_PARAM + 1)
#define MF_SET_PRINT_PARAM				0x0014
#define MF_SET_PROCESS_PARAM			0x0015		// (TM-S1000)

#define MF_CLEAR_BASE_PARAM				0x0020
#define MF_CLEAR_MICR_PARAM				0x0021
#define MF_CLEAR_SCAN_FRONT_PARAM		0x0022
#define MF_CLEAR_SCAN_PARAM				0x0022
#define MF_CLEAR_SCAN_BACK_PARAM		(MF_CLEAR_SCAN_FRONT_PARAM + 1)
#define MF_CLEAR_PRINT_PARAM			0x0024
#define MF_CLEAR_PROCESS_PARAM			0x0025		// (TM-S1000)

#define	MF_GET_BASE_DEFAULT				0x0030
#define	MF_GET_MICR_DEFAULT				0x0031
#define	MF_GET_SCAN_DEFAULT				0x0032
#define	MF_GET_SCAN_FRONT_DEFAULT		0x0032
#define	MF_GET_SCAN_BACK_DEFAULT		(MF_GET_SCAN_FRONT_DEFAULT + 1)
#define	MF_GET_PRINT_DEFAULT			0x0034
#define MF_GET_PROCESS_DEFAULT			0x0035		// (TM-S1000)

/**********************/
/* Base parameters    */
/**********************/
// struct version(for iVersion)
#define MF_FIRST_VERSION				0x0100
#define MF_BASE_VERSION01				0x0101
#define MF_BASE_VERSION					0x0100

// Event notice type(for dwNotifyType)
#define MF_BASE_MESSAGE_NO_MESSAGE		1
#define MF_BASE_MESSAGE_EVENT			2
#define MF_BASE_MESSAGE_HWND			3
#define MF_BASE_MESSAGE_BUTTON_CLICK	4
#define MF_BASE_MESSAGE_NOTIFY_MAX		MF_BASE_MESSAGE_BUTTON_CLICK

// Paper insertion timeout(for dwTimeout)
#define MF_BASE_TIMEOUT_MAX				300		// 5 min
#define MF_BASE_TIMEOUT_DEFAULT			0		// INFINITE

// NV memory use type(for bUseNVMemory)
#define MF_BASE_NVMEMORY_NOT_USE		0
#define MF_BASE_NVMEMORY_USE			1

// Paper eject type(for wErrorEject and wSuccessEject)
#define	MF_EJECT_DISCHARGE			0x0000
#define	MF_EJECT_RELEASE			0x0001
#define	MF_EXIT_ERROR_DISCHARGE		MF_EJECT_DISCHARGE
#define	MF_EXIT_ERROR_RELEASE		MF_EJECT_RELEASE
#define	MF_EXIT_SUCCESS_DISCHARGE	MF_EJECT_DISCHARGE
#define	MF_EXIT_SUCCESS_RELEASE		MF_EJECT_RELEASE
#define	MF_EXIT_ERROR_CONTINUE_DISCHARGE	0x0010	
#define	MF_EXIT_ERROR_CONTINUE_RELEASE		0x0011	

// Buzzer factor type(for bBuzzerHz and bBuzzerCount)
#define	MF_BUZZER_TYPE_SUCCESS		0
#define	MF_BUZZER_TYPE_ERROR		1
#define	MF_BUZZER_TYPE_WFEED		2
#define	MF_BUZZER_TYPE_MAX			3

// Buzzer type(for bBuzzerHz)
#define	MF_BUZZER_HZ_4000			0
#define	MF_BUZZER_HZ_440			1
#define	MF_BUZZER_HZ_880			2

// Buzzer count(for bBuzzerCount)
#define	MF_BUZZER_DISABLE			0
#define	MF_BUZZER_COUNT_MAX			3

// Base Structure (Version 0x0101)
typedef struct {
// Base Section
	int iSize;
	int iVersion;
	int iRet;
	DWORD dwNotifyType;
	DWORD dwTimeout;
	union {
		LPHANDLE lphNotifyEvent;
		HWND hNotifyWnd;
	} uNotifyHandle;
	HWND hProgressWnd;
	WORD wErrorEject;
	BYTE bBuzzerHz[MF_BUZZER_TYPE_MAX];
	BYTE bBuzzerCount[MF_BUZZER_TYPE_MAX];
	BYTE bUseNVMemory;
	char cPortName[256];
	WORD wSuccessEject;
} MF_BASE01, *LPMF_BASE01;

// Base Structure (OLD:Version 0x0100)
typedef struct {
// Base Section
	int iSize;
	int iVersion;
	int iRet;
	DWORD dwNotifyType;
	DWORD dwTimeout;
	union {
		LPHANDLE lphNotifyEvent;
		HWND hNotifyWnd;
	} uNotifyHandle;
	HWND hProgressWnd;
	WORD wErrorEject;
	BYTE bBuzzerHz[MF_BUZZER_TYPE_MAX];
	BYTE bBuzzerCount[MF_BUZZER_TYPE_MAX];
	BYTE bUseNVMemory;
	char cPortName[256];
} MF_BASE, *LPMF_BASE;

/**********************/
/* MICR parameters    */
/**********************/
// Structure version(for iVersion)
#define MF_MICR_VERSION					0x0100

// MICR type(for bMicOcrSelect)
#define MF_MICR_USE_MICR				1
#define MF_MICR_USE_OCR					2

// Font(for bFont)
#define MF_MICR_FONT_E13B				0
#define MF_MICR_FONT_CMC7				1

#define MF_MICR_CHAR_MAX				81
#define MF_MICR_CHAR_LEN				80

// Reliability structure
typedef struct{
	CHAR cRecogChar;		// Recognized character
	LONG lPercentage;		// Reliability(%)
} MF_OCR_RELIABILITY;

// Reliability information structure
typedef struct{
	LONG				lPosition;		// Position(0 is left edge)
	MF_OCR_RELIABILITY	stFirstSelect;	// First recognition candidate
	MF_OCR_RELIABILITY	stSecondSelect;	// Second recognition candidate
} MF_OCR_RELIABLE_INFO;

// MICR/OCR structure
typedef struct{
// MIC_OCR Section
	int iSize;
	int iVersion;
	int iRet;
	BYTE bFont;
	BYTE bMicOcrSelect;
	BOOL blParsing;

	BYTE	bStatus;
	BYTE	bDetail;
	CHAR szMicrStr[MF_MICR_CHAR_MAX];
	MF_OCR_RELIABLE_INFO stOcrReliableInfo[MF_MICR_CHAR_MAX];
	CHAR	szAccountNumber[MF_MICR_CHAR_MAX];
	CHAR	szAmount[MF_MICR_CHAR_MAX];
	CHAR	szBankNumber[MF_MICR_CHAR_MAX];
	CHAR	szSerialNumber[MF_MICR_CHAR_MAX];
	CHAR	szEPC[MF_MICR_CHAR_MAX];
	CHAR	szTransitNumber[MF_MICR_CHAR_MAX];
	long	lCheckType;
	long	lCountryCode;
} MF_MICR, *LPMF_MICR;

/**********************/
/* Scan parameters    */
/**********************/
// Structure version
#define MF_SCAN_VERSION					0x0100

// Scan face type(for BiSCNSelectScanFace)
#define MF_SCAN_FACE_FRONT				0
#define MF_SCAN_FACE_BACK				1
#define MF_SCAN_FACE_BOTH				2

// Scan resolution(for sResolution)
#define MF_SCAN_DPI_DEFAULT				0
#define MF_SCAN_DPI_100					100
#define	MF_SCAN_DPI_120					120
#define MF_SCAN_DPI_200					200

// Scan structure
typedef struct{
// Scan Section
	int iSize;
	int iVersion;
	int iRet;
	WORD wImageID;
	short sResolution;
	BYTE bAddInfoDataSize;
	LPBYTE pAddInfoData;
	BYTE bStatus;
	BYTE bDetail;
	DWORD dwXSize;
	DWORD dwYSize;
	DWORD dwScanSize;
	LPBYTE lpbScanData;
} MF_SCAN, *LPMF_SCAN;

/**********************/
/* Print parameters   */
/**********************/
// Structure version(for iVersion)
#define MF_PRINT_VERSION01              0x0101
#define MF_PRINT_VERSION                0x0100

// Line number(for lpString,dwAttribute,wFont and wfontSize)
#define MF_PRINT_LINE_1					0
#define MF_PRINT_LINE_2					1
#define MF_PRINT_LINE_3					2
#define MF_PRINT_LINE_MAX				MF_PRINT_LINE_3

// Charactor attribute(for dwAttribute)
#define MF_PRINT_NO_ATTRIBUTE			0x00000000
#define MF_PRINT_BOLD					0x00000001
#define MF_PRINT_UNDERLINE_1			0x00000010
#define MF_PRINT_UNDERLINE_2			0x00000020
#define MF_PRINT_BLACK					0x00000100
#define MF_PRINT_1ST_COLOR				0x00000100
#define MF_PRINT_COLOR					0x00000200
#define MF_PRINT_2ND_COLOR				0x00000200
#define MF_PRINT_MIXED					0x00000300
#define MF_PRINT_REVERSEVIDEO			0x00001000

// Font type(for wFont)
#define MF_PRINT_FONT_A					1
#define MF_PRINT_FONT_B					2
#define MF_PRINT_SYSTEMFONT				3

// Font size(for wFontSize)
#define MF_PRINT_FONT_W1_H1				0x0011
#define MF_PRINT_FONT_W1_H2				0x0012
#define MF_PRINT_FONT_W2_H1				0x0021
#define MF_PRINT_FONT_W2_H2				0x0022

// Printing speed(for bSpeed)
#define	MF_PRINT_SPEED_NORMAL			0
#define	MF_PRINT_SPEED_HIGH				1
#define	MF_PRINT_SPEED_ECONOMY			2

// PrintDirection(for bDirection)
#define MF_PRINT_DERECTION_DOUBLE		0
#define MF_PRINT_DIRECTION_DOUBLE		0
#define MF_PRINT_DERECTION_SINGLE		1
#define MF_PRINT_DIRECTION_SINGLE		1

// Endorse type
#define MF_PRINT_TYPE_ENDORSE_ONLY				0
#define MF_PRINT_TYPE_ENDORSE_NORMAL			1
#define MF_PRINT_TYPE_ELECTRIC_ENDORSE_ONLY		2
#define MF_PRINT_TYPE_ENDORSE_EXTEND			3
#define MF_PRINT_TYPE_ELECTRIC_ENDORSE_EXTEND	4

// print structure(version 0x0101)
typedef struct{
// Print Section
	int iSize;
	int iVersion;
	int iRet;
	BOOL blDummy;
	LPSTR lpString[3];
	DWORD dwAttribute[3];
	WORD wFont[3];
	WORD wFontSize[3];
	BYTE bSpeed;
	BOOL bDirection;
	DWORD dwEndorseType;
} MF_PRINT01, *LPMF_PRINT01;

// print structure(OLD: version 0x0100)
typedef struct{
// Print Section
	int iSize;
	int iVersion;
	int iRet;
	BOOL blElectricEndorse;
	LPSTR lpString[3];
	DWORD dwAttribute[3];
	WORD wFont[3];
	WORD wFontSize[3];
	BYTE bSpeed;
	BOOL bDirection;
} MF_PRINT, *LPMF_PRINT;


/********************************/
/* BiMICRResultBeep             */
/********************************/
// define
#define MICR_BEEP_SUCCESS				0
#define MICR_BEEP_ERROR					1
#define MICR_BEEP_PAPER					2
#define MICR_BEEP_LOW					0
#define MICR_BEEP_HIGH					1
#define MICR_BEEP_COUNT_MAX				3


/********************************/
/* Process parameters			*/
/********************************/
// Structure version(for iVersion)
#define MF_PROCESS_VERSION				0x0100

// Activation mode(For bActivationMode)
#define MF_ACTIVATE_MODE_CONFIRMATION	0
#define MF_ACTIVATE_MODE_HIGH_SPEED		1

// Paper type(For bPaperType)
#define MF_PAPER_TYPE_OTHER				0
#define MF_PAPER_TYPE_CHECK				1

// Stamp status(For bSuccessStamp, bPaperMisInsertionStamp, bNoiseStamp
//				, bDoubleFeedStamp, bBaddataStamp, bNodataStamp)
#define MF_STAMP_DISABLE				0
#define MF_STAMP_ENABLE					1

// Error detect(For bPaperMisInsertionErrorSelect, bNoiseErrorSelect
//				, bDoubleFeedErrorSelect, bBaddataErrorSelect, bNodataErrorSelect)
#define MF_ERROR_SELECT_NODETECT		0
#define MF_ERROR_SELECT_DETECT			1

// Eject select(For bPaperMisInsertionErrorEject, bNoiseErrorEject
//				, bDoubleFeedErrorEject, bBaddataErrorEject, bNodataErrorEject)
#define MF_EJECT_MAIN_POCKET			0x22
#define MF_EJECT_SUB_POCKET				0x24
#define MF_EJECT_NOEJECT				0x28

// Cancel status(For bPaperMisInsertionCancel, bNoiseCancel, bDoubleFeedCancel
//				, bBaddataCancel, bNodataCancel)
#define MF_CANCEL_DISABLE				0
#define MF_CANCEL_ENABLE				1

// Nearfull parmit(For bNearFullSelect)
#define MF_NEARFULL_NOT_PERMIT			0x00
#define MF_NEARFULL_MAIN_PERMIT			0x01
#define MF_NEARFULL_SUB_PERMIT			0x02
#define MF_NEARFULL_PERMIT				0x03

// Result partial(For bResultPartialData)
#define MF_RESULT_NONE					0
#define MF_RESULT_PARTIAL				1

// Process structure
typedef struct{
// Process Section
	int iSize;
	int iVersion;
	BYTE bActivationMode;
	BYTE bPaperType;
	DWORD dwStartWaitTime;
	BYTE bSuccessStamp;
	BYTE bPaperMisInsertionErrorSelect;
	BYTE bPaperMisInsertionErrorEject;
	BYTE bPaperMisInsertionStamp;
	BYTE bPaperMisInsertionCancel;
	BYTE bNoiseErrorSelect;
	BYTE bNoiseErrorEject;
	BYTE bNoiseStamp;
	BYTE bNoiseCancel;
	BYTE bDoubleFeedErrorSelect;
	BYTE bDoubleFeedErrorEject;
	BYTE bDoubleFeedStamp;
	BYTE bDoubleFeedCancel;
	BYTE bBaddataErrorSelect;
	BYTE bBaddataCount;
	BYTE bBaddataErrorEject;
	BYTE bBaddataStamp;
	BYTE bBaddataCancel;
	BYTE bNodataErrorSelect;
	BYTE bNodataErrorEject;
	BYTE bNodataStamp;
	BYTE bNodataCancel;
	BYTE bNearFullSelect;
	BYTE bResultPartialData;
} MF_PROCESS, *LPMF_PROCESS;

/********************************/
/* OCR_AB parameters			*/
/********************************/
// Structure version(for iVersion)
#define MF_OCR_AB_VERSION					0x0100

// Ocr Type
#define MF_OCR_FONT_OCRA_NUM				1
#define MF_OCR_FONT_OCRA_ALPHA				2
#define MF_OCR_FONT_OCRA_ALPHANUM			3
#define MF_OCR_FONT_OCRA_ALPHANUM_WOOH		7
#define MF_OCR_FONT_OCRA_ALPHANUM_WOZERO	11
#define MF_OCR_FONT_OCRB_NUM				17
#define MF_OCR_FONT_OCRB_ALPHA				18
#define MF_OCR_FONT_OCRB_ALPHANUM			19
#define MF_OCR_FONT_OCRB_ALPHANUM_WOOH		23
#define MF_OCR_FONT_OCRB_ALPHANUM_WOZERO	27

// Direction
#define	MF_OCR_LEFTRIGHT					1
#define	MF_OCR_TOPBOTTOM					2
#define	MF_OCR_RIGHTLEFT					3
#define	MF_OCR_BOTTOMTOP					4

// Area(for bEndX, bEndY)
#define OCR_AREA_RIGHT						65535
#define	OCR_AREA_BOTTOM						65535

// SpeceHandling(for bSpeceHandling)
#define	OCR_SPACE_ENABLE					1
#define	OCR_SPACE_DISABLE					0

#define MF_OCR_AB_CHAR_MAX					128

typedef struct{
	int iSize;
	int iVersion;
	int iRet;
	BYTE bOcrType;
	BYTE bDirection;
	WORD wStartX;
	WORD wStartY;
	WORD wEndX;
	WORD wEndY;
	BYTE bSpaceHandling;
	CHAR szOcrStr[MF_OCR_AB_CHAR_MAX];
	MF_OCR_RELIABLE_INFO stOcrReliableInfo[MF_OCR_AB_CHAR_MAX];
} MF_OCR_AB, *LPMF_OCR_AB;

/********************************/
/* VERSION_INFO parameters		*/
/********************************/
#define VERSION_CHAR_MAX					64

typedef struct{
	CHAR lpszDescription[VERSION_CHAR_MAX];
	CHAR lpszVersion[VERSION_CHAR_MAX];
} VERSION_INFO, *LPVERSION_INFO;

#pragma pack(pop)

#endif // !defined(_MULTI_FUNCTION_H)
