/////////////////////////////////////////////////////////////////////
//
// EPSStmApiInterface.h 
//
// Interface definition of API
//
// Modification history
// ------------------------------------------------------------------
// 2006-01-13	New
// 2007-01-16	Modified
// 2007-05-21	Modified
/////////////////////////////////////////////////////////////////////

#if !defined(TMS1000_EPSSTMAPIINTERFACE_H)
#define TMS1000_EPSSTMAPIINTERFACE_H

#include <windows.h>
#include "MultiFunction.h"

///////////////////////////////////////////////////////////////////////////////
// ERROR CODE :
#define		SUCCESS						0			// Success
#define		ERR_TYPE					-10			// nType error
#define		ERR_OPENED					-20			// Already opened
#define		ERR_NO_PRINTER				-30			// There is not printer driver
#define		ERR_NO_TARGET				-40			// Printer out of object
#define		ERR_NO_MEMORY				-50			// No memory
#define		ERR_HANDLE					-60			// Invalid handle
#define		ERR_TIMEOUT					-70			// Ttime out
#define		ERR_ACCESS					-80			// cannot read/write
#define		ERR_PARAM					-90			// param error
#define		ERR_NOT_SUPPORT				-100		// not support
#define		ERR_OFFLINE					-110		// now offline
#define		ERR_NOT_EPSON				-120		// not epson printer
#define		ERR_WITHOUT_CB				-130		// without callback function
#define		ERR_BUFFER_OVER_FLOW		-140		// Read buffer over flow
#define		ERR_REGISTRY				-150		// Regstry error
#define		ERR_ENABLE					-160		// BiOpenMonPrinter() is already called
#define		ERR_DISK_FULL				-170		// Capacity of a disk is insufficient
#define		ERR_NO_IMAGE				-180		// No image data
#define		ERR_ENTRY_OVER				-190		// Registration number-of-cases over
#define		ERR_CROPAREAID				-200		// No specific CropAreaID
#define		ERR_EXIST					-210		// Already the same thing
#define		ERR_NOT_FOUND				-220		// Not found
#define		ERR_IMAGE_FILEOPEN			-230		// Open failure
#define		ERR_IMAGE_UNKNOWNFORMAT		-240		// Format injustice
#define		ERR_IMAGE_FAILED			-250		// Image creation failure
#define		ERR_WORKAREA_NO_MEMORY		-260		// No memory for WORKAREA
#define		ERR_WORKAREA_UNKNOWNFORMAT	-270		// Image creation failure because of format injustice
#define		ERR_WORKAREA_FAILED			-280		// WORKAREA creation failure
#define		ERR_IMAGE_FILEREAD			-290		// Image file read error
#define		ERR_PAPERINSERT_TIMEOUT		-300		// Image paper insert error of timeout
#define		ERR_EXEC_FUNCTION			-310		// Other API is running (3.01)
#define		ERR_EXEC_MICR				-320		// Now reading MICR (3.01)
#define		ERR_EXEC_SCAN				-330		// Now scaning image(3.01)
#define		ERR_SS_NOT_EXIST			-340		// Status service not started (3.01)
#define		ERR_SPL_NOT_EXIST			-350		// Spooler service not started (3.01)
#define		ERR_SPL_PAUSED				-370		// Spooler service paused (3.01)
#define		ERR_RESET					-400		// Now printer is reseting (3.01)
#define		ERR_THREAD					-420		// Failure to the start of Thread (3.03)
#define		ERR_ABORT					-430		// Because BiSCNMICRCancelFunction was called, processing was stopped (3.03)
#define		ERR_MICR					-440		// Error occurred during MICR processing (3.03)
#define		ERR_SCAN					-450		// Error occurred during the image scan (3.03)
#define		ERR_LINE_OVERFLOW			-460		// A line overflow has occurred during transaction printing (3.03)
#define		ERR_NOT_EXEC				-470		// Processing is not executed (3.03)
#define		ERR_SIZE					-1000		// Size excess error
#define		ERR_PAPER_PILED				-1010		// Paper pilling error
#define		ERR_PAPER_JAM				-1020		// Paper jam has occurred
#define		ERR_COVER_OPEN				-1030		// Cover open error
#define		ERR_MICR_NODATA				-1040		// MICR data is not existing
#define		ERR_MICR_BADDATA			-1050		// MICR data is not able to recognize
#define		ERR_MICR_PARSE				-1060		// MICR data can not be parsed
#define		ERR_MICR_NOISE				-1070		// Noise error has ocurred during MICR reading
#define		ERR_SCN_COMPRESS			-1080		// Scan image data compressing error
#define		ERR_PAPER_EXIST				-1090		// Because there is a paper on the path, API can not be execute
#define		ERR_PAPER_INSERT			-1100		// Paper insertion error
#define		ERR_LESS_CHECKS				-1110		// User requested to scan more checks than inserted in the main pocket
#define		ERR_EXEC_SCAN_CHECK_CONTINUOUS	-2000	// Now scanning check continuous mode
#define		ERR_EXEC_SCAN_CHECK_ONEBYONE	-2010	// Now scanning check one by one mode
#define		ERR_EXEC_SCAN_IDCARD			-2020	// Now scanning ID card
#define		ERR_EXEC_PRINT_ROLLPAPER		-2030	// Now printing text, image or barcode on roll paper.
#define		ERR_EXEC_PRINT_VALIDATION		-2040	// Now printing text, image or barcode on validation.
#define		ERR_FORM_LENGTH					-2050	// Paper Length error
#define		ERR_MECHANICAL					-2060	// Mechanical error

///////////////////////////////////////////////////////////////////////////////
// ASB Bit :
#define		ASB_NO_RESPONSE				0x00000001	//No response
#define		ASB_PRINT_SUCCESS			0x00000002	//Finish to print
#define		ASB_UNRECOVER_ERR			0x00002000	//Unrecoverable error
#define		ASB_AUTORECOVER_ERR			0x00004000	//Auto-Recoverable error
#define		ASB_OFF_LINE				0x00000008	//Off-line
#define		ASB_WAIT_ON_LINE			0x00000100	//Waiting for on-line recovery
#define		ASB_WAIT_PEPRT_EJECT		0x00000100	//Waiting for slip remove  (3.03)
#define		ASB_PANEL_SWITCH			0x00000200	//Panel switch
#define		ASB_PRINTER_FEED			0x00000040	//Paper is being fed by using the PAPER FEED button
#define		ASB_HEAD_TEMPERATURE_ERR	0x00004000	//Mechanical error
#define		ASB_MECHANICAL_ERR			0x00000400	//Mechanical error
#define		ASB_LABEL_ERR				0x00000400	//LABEL check error
#define		ASB_AUTOCUTTER_ERR			0x00000800	//Auto cutter error

#define		ASB_DRAWER_KICK				0x00000004	//Drawer kick-out connector pin3 is HIGH
#define		ASB_PRESENTER_COVER			0x00000004	//Presenter cover is open

#define		ASB_JOURNAL_END				0x00040000	//Journal paper roll end
#define		ASB_EJECT_SENSOR_NO_PAPER	0x00040000	//Eject sensor no paper   (3.03)
#define		ASB_RECEIPT_END				0x00080000	//Receipt paper roll end

#define		ASB_COVER_OPEN				0x00000020	//Cover is open
#define		ASB_PLATEN_OPEN				0x00000020	//Platen is open

#define		ASB_JOURNAL_NEAR_END		0x00010000	//Journal paper roll near-end
#define		ASB_JOURNAL_NEAR_END_FIRST	0x00010000	//Journal paper roll near-end-first
#define		ASB_NOT_CARD_INSERT			0x00010000	//Not card insert (3.03)

#define		ASB_RECEIPT_NEAR_END		0x00020000	//Receipt paper roll near-end
#define		ASB_RECEIPT_NEAR_END_FIRST	0x00020000	//Receipt paper roll near-end-first

#define		ASB_SLIP_TOF				0x00200000	//SLIP TOF
#define		ASB_SLIP_TOF_2				0x00400000	//SLIP TOF 2
#define		ASB_PSUPPLIER_END			0x00200000	//Paper supply end

#define		ASB_SLIP_BOF				0x00400000	//SLIP BOF
#define		ASB_SLIP_BOF_2				0x00200000	//SLIP BOF 2
#define		ASB_RECEIPT_NEAR_END_SECOND	0x00400000	//Receipt paper roll near-end-second
#define		ASB_ASF_PAPER				0x00400000	//No paper ASF(3.03)

#define		ASB_SLIP_SELECTED			0x01000000	//Slip is not selected
#define		ASB_PRESENTER_TE			0x01000000	//Presenter T/E receipt end

#define		ASB_PRINT_SLIP				0x02000000	//Cannot print on slip
#define		ASB_PRESENTER_TT			0x02000000	//Presenter T/T receipt end

#define		ASB_VALIDATION_SELECTED		0x04000000	//Validation is not selected
#define		ASB_RETRACTOR_R1JAM			0x04000000	//Retractor receipt end R1JAM 

#define		ASB_PRINT_VALIDATION		0x08000000	//Cannot print on validation
#define		ASB_RETRACTOR_BOX			0x08000000	//Retractor box

#define		ASB_VALIDATION_TOF			0x20000000	//Validation TOF
#define		ASB_RETRACTOR_R2JAM			0x20000000	//Retractor receipt end R2JAM
#define		ASB_WAIT_INSERT				0x20000000	//Wait insert (3.03)

#define		ASB_VALIDATION_BOF			0x40000000	//Validation BOF
#define		ASB_RETRACTOR_SENSOR3		0x40000000	//Retractor sensor NO.3
#define		ASB_VALIDATION_NO_PAPER		0x40000000	//Validation no paper (3.03)

#define		ASB_BATTERY_OFFLINE			0x00000004	//Off-line for BATTERY QUANTITY		

#define		ASB_PAPER_FEED				0x00000040	//Paper is now feeding by PF FW (3.01)
#define		ASB_PAPER_END				0x00040000	//Detected paper roll end (3.01)

#define		ASB_PAPER_INTERMEDIATE		0x00010000	//Not paper in Paper intermediate sensor
#define		ASB_MAIN_NEAR_FULL			0x00020000	//Main Pocket not Near Full
#define		ASB_SUB_NEAR_FULL			0x00080000	//Sub Pocket not Near Full
#define		ASB_SLIP_PAPER_SIZE			0x00200000	//Not paper in Slip Paper size sensor
#define		ASB_STAMP_EXIST				0x02000000	//Stamp not Exist
#define		ASB_FRANKING_SENSOR			0x40000000	//Not paper in Franking sensor

#define		INK_ASB_NEAR_END			0x0001		//Ink near-end
#define		INK_ASB_END					0x0002		//Ink end
#define		INK_ASB_NO_CARTRIDGE		0x0004		//Cartridge is not present
#define		INK_ASB_NO_CARTRIDGE2		0x0008		//Cartridge2 is not present
#define		INK_ASB_CLEANING			0x0020		//Being cleaned
#define		INK_ASB_NEAR_END2			0x0100		//Ink near-end2
#define		INK_ASB_END2				0x0200		//Ink end2

///////////////////////////////////////////////////////////////////////////////
// BiOpenMonPrinter() :

// Type
#define		TYPE_PORT					1			// use port name
#define		TYPE_PRINTER				2			// use printer driver name

///////////////////////////////////////////////////////////////////////////////
// BiSCNSetImageQuality() :

// Scale Option
#define		EPS_BI_SCN_1BIT				1			// Monochrome
#define		EPS_BI_SCN_8BIT				8			// Glay scale

// Color Option
#define		EPS_BI_SCN_MONOCHROME		48			// Monochrome
#define		EPS_BI_SCN_COLOR			49			// Color

// Extensive Option
#define		EPS_BI_SCN_AUTO				48			// Auto
#define		EPS_BI_SCN_MANUAL			49			// Manual
#define		EPS_BI_SCN_SHARP			50			// Sharpness
#define		EPS_BI_SCN_SHARP_CUSTOM		51			// Custom sharpness
#define		EPS_BI_SCN_SHARP_CUSTOM2	52			// Combination of sharpness and custom sharpness

///////////////////////////////////////////////////////////////////////////////
// BiSCNSetImageFormat() :

// Format Option
#define		EPS_BI_SCN_TIFF				1			// TIFF format(CCITT Group 4)
#define		EPS_BI_SCN_RASTER			2			// Raster Image
#define		EPS_BI_SCN_BITMAP			3			// Bitmap
#define		EPS_BI_SCN_TIFF256			4			// TIFF format(Glay scale)
#define		EPS_BI_SCN_JPEGHIGH			5			// Jpeg format(High complessed)
#define		EPS_BI_SCN_JPEGNORMAL		6			// Jpeg format(Normal)
#define		EPS_BI_SCN_JPEGLOW			7			// Jpeg format(Low complessed)
#define		EPS_BI_SCN_JTIFF			8			// TIFF format(Jpeg complessed)

///////////////////////////////////////////////////////////////////////////////
// BiSCNSelectScanUnit() :

// Scan unit ID
#define		EPS_BI_SCN_UNIT_CHECKPAPER		48	// Checkpaper scanner unit
#define		EPS_BI_SCN_UNIT_CARD			49	// Card scanner unit

///////////////////////////////////////////////////////////////////////////////
// BiESCNEnable() :

// Saved Method
#define		CROP_STORE_MEMORY			0			// Saved memory
#define		CROP_STORE_FILE				1			// Saved file

///////////////////////////////////////////////////////////////////////////////
// BiESCNSetAutoSize() :

// Enable/Disable Autosize
#define		CROP_AUTOSIZE_DISABLE		0			// Enabled auto size
#define		CROP_AUTOSIZE_ENABLE		1			// Disabled auto size

///////////////////////////////////////////////////////////////////////////////
// BiESCNSetRotate() :

// Enable/Disable Rotate
#define		CROP_ROTATE_DISABLE			0			// Enabled rotate
#define		CROP_ROTATE_ENABLE			1			// Disabled rotate

///////////////////////////////////////////////////////////////////////////////
// BiESCNDefineCropArea() :

// CropArea Setting
#define		CROP_AREA_RESET_ALL			0			// Delete all crop area
#define		CROP_AREA_ENTIRE_IMAGE		1			// CropArea entire image
#define		CROP_AREA_RIGHT				65535		// End X Point
#define		CROP_AREA_BOTTOM			65535		// End Y Point

///////////////////////////////////////////////////////////////////////////////
// BiESCNClearImage() :

// Delete Method
#define		CROP_CLEAR_ALL_IMAGE		0			// Delete all image
#define		CROP_CLEAR_BY_FILEINDEX		1			// Clear by fileIndex
#define		CROP_CLEAR_BY_FILEID		2			// Clear by fileId
#define		CROP_CLEAR_BY_IMAGETAGDATA	4			// by imageTagData

///////////////////////////////////////////////////////////////////////////////
// BiSetPrintStation() :

// Printing Station
#define		MF_ST_ROLLPAPER				0x0001		// Roll paper printing
#define		MF_ST_VALIDATION			0x0002		// Validation printing
#define		MF_ST_E_ENDORSEMENT			0x0004		// Electric endorse printing

///////////////////////////////////////////////////////////////////////////////
// BiPrintText

#pragma pack(push, 1)

typedef struct {
	DWORD	dwAttribute;
	WORD	wFont;
	LPSTR	szFontName;
	WORD	wFontSize;
} MF_DECORATE, *LPMF_DECORATE;

#pragma pack(pop)

///////////////////////////////////////////////////////////////////////////////
// BiMICRSelectDataHandling

// bErrorSelect
#define		ES_STOP_ALL				0x00
#define		ES_CONTINUE_ALL			0x01
#define		ES_CONTINUE_DOUBLEFEED	0x02
#define		ES_CONTINUE_NODATA		0x04
#define		ES_CONTINUE_BADDATA		0x08
#define		ES_CONTINUE_NOISE		0x10

///////////////////////////////////////////////////////////////////////////////
// BiBufferedPrint
#define		MF_PRT_BUFFERING		1
#define		MF_PRT_EXEC				2
#define		MF_PRT_CLEAR			3

///////////////////////////////////////////////////////////////////////////////
// BiRingBuzzer
#define		MF_BUZZER_TONE_HIGH		0
#define		MF_BUZZER_TONE_MIDDLE	2
#define		MF_BUZZER_TONE_LOW		1

///////////////////////////////////////////////////////////////////////////////
// BiSCNSetBehaviorToScnResult
#define 	MF_PROCESS_CONTINUE_OVERLAP		1
#define 	MF_PROCESS_CONTINUE_NOOVERLAP	2
#define 	MF_PROCESS_CONTINUE_CANCEL		3

///////////////////////////////////////////////////////////////////////////////
// BiGetOcrABText
#define		OCR_SOURCE_TRANSACTION_NUMBER	0
#define		OCR_SOURCE_IMAGE_FILE			1

///////////////////////////////////////////////////////////////////////////////
// BiGetVersion
#define		DRIVER_TYPE_J9000				1
#define		DRIVER_TYPE_S1000				2

#define		VERSION_TYPE_DRIVER				0x00000001
#define		VERSION_TYPE_USB				0x00000002
#define		VERSION_TYPE_MICR				0x00000003
#define		VERSION_TYPE_MICR_E13B			0x00000003
#define		VERSION_TYPE_MICR_CMC7			0x00010003
#define		VERSION_TYPE_OCR				0x00000004
#define		VERSION_TYPE_IMAGE				0x00000005

///////////////////////////////////////////////////////////////////////////////
// BiESCNGetDeSkew, BiESCNSetDeSkew
#define 	DESKEW_ALL						0
#define		DESKEW_DISABLE					65535

typedef int (WINAPI* DLL_BiOpenMonPrinter)( int, LPSTR);
typedef int (WINAPI* DLL_BiSetMonInterval)( int, WORD, WORD);
typedef int (WINAPI* DLL_BiGetStatus)( int, LPDWORD);
typedef int (WINAPI* DLL_BiSetStatusBackFunction)( int, int (CALLBACK EXPORT *pStatusCB)(DWORD dwStatus));
typedef int (WINAPI* DLL_BiSetStatusBackWnd)( int, long, LPDWORD);
typedef int (WINAPI* DLL_BiSetStatusBackFunctionEx)( int, int (CALLBACK EXPORT *pStatusCB)(DWORD dwStatus, LPSTR lpcPortName));
typedef int (WINAPI* DLL_BiCancelStatusBack)( int );
typedef int (WINAPI* DLL_BiResetPrinter)( int);
typedef int (WINAPI* DLL_BiGetCounter)( int, WORD, LPDWORD);
typedef int (WINAPI* DLL_BiResetCounter)( int, WORD);
typedef int (WINAPI* DLL_BiCancelError)( int);
typedef int (WINAPI* DLL_BiGetType)( int, LPBYTE, LPBYTE, LPBYTE, LPBYTE);
typedef int (WINAPI* DLL_BiGetOfflineCode)( int, LPBYTE);
typedef int (WINAPI* DLL_BiGetOfflineCodeByIndex)( int, int, LPBYTE);
typedef int (WINAPI* DLL_BiMICRSelectDataHandling)( int, BYTE, BYTE, BYTE);
typedef int (WINAPI* DLL_BiMICRGetStatus)( int, LPBYTE);
typedef int (WINAPI* DLL_BiMICRCleaning)( int);
typedef int (WINAPI* DLL_BiSCNSetImageQuality)( int, BYTE, char, BYTE, BYTE);
typedef int (WINAPI* DLL_BiSCNSetImageFormat)( int, BYTE);
typedef int (WINAPI* DLL_BiSCNSetScanArea)( int, BYTE, BYTE, BYTE, BYTE);
typedef int (WINAPI* DLL_BiSCNGetImageQuality)( int, LPBYTE, char *, LPBYTE, LPBYTE);
typedef int (WINAPI* DLL_BiSCNGetImageFormat)( int, LPBYTE);
typedef int (WINAPI* DLL_BiSCNGetScanArea)( int, LPBYTE, LPBYTE, LPBYTE, LPBYTE);
typedef int (WINAPI* DLL_BiSCNSetCroppingArea)( int, BYTE, BYTE, BYTE, BYTE, BYTE);
typedef int (WINAPI* DLL_BiSCNGetCroppingArea)( int, LPWORD, LPBYTE);
typedef int (WINAPI* DLL_BiSCNDeleteCroppingArea)( int, BYTE);
typedef int (WINAPI* DLL_BiSCNSelectScanUnit)( int, BYTE);
typedef int (WINAPI* DLL_BiSCNMICRFunction)(int iHandle, LPVOID lpvStruct, WORD wFunction);
typedef int (WINAPI* DLL_BiSCNMICRCancelFunction)(int iHandle, WORD wEjectType);
typedef int (WINAPI* DLL_BiSCNSelectScanFace)(int iHandle, BYTE bFace);
typedef int (WINAPI* DLL_BiGetPrnCapability)( int, BYTE, LPBYTE, LPBYTE);
typedef int (WINAPI* DLL_BiCloseMonPrinter)( int );
typedef int (WINAPI* DLL_BiGetRealStatus)( int, LPDWORD);
typedef int (WINAPI* DLL_BiSCNMICRFunctionContinuously)(int iHandle, LPVOID lpvStruct, WORD wFunction);
typedef int (WINAPI* DLL_BiSCNMICRFunctionPostPrint)(int iHandle, LPVOID lpvStruct, WORD wFunction);
typedef int (WINAPI* DLL_BiSCNMICRSetStatusBackFunction)(int, int (CALLBACK EXPORT *pScnMicrCB)(DWORD dwTransactionNumber, WORD wMainStatus, WORD wSubStatus, LPSTR lpPortName));
typedef int (WINAPI* DLL_BiSCNMICRSetStatusBackWnd)(int, long, LPDWORD, LPWORD, LPWORD);
typedef int (WINAPI* DLL_BiSCNMICRCancelStatusBack)(int);
typedef int (WINAPI* DLL_BiGetMicrText)(int, DWORD, LPMF_MICR);
typedef int (WINAPI* DLL_BiGetOcrABText)(int, DWORD, BYTE, LPCSTR, LPMF_OCR_AB);
typedef int (WINAPI* DLL_BiGetScanImage)(int, DWORD, LPMF_SCAN);
typedef int (WINAPI* DLL_BiGetTransactionNumber)(int, LPDWORD);
typedef int (WINAPI* DLL_BiSetTransactionNumber)(int, DWORD);
typedef int (WINAPI* DLL_BiGetPrintStation)(int, LPWORD);
typedef int (WINAPI* DLL_BiSetPrintStation)(int, WORD);
typedef int (WINAPI* DLL_BiPrintText)(int, LPSTR, MF_DECORATE);
typedef int (WINAPI* DLL_BiPrintImage)(int, LPSTR);
typedef int (WINAPI* DLL_BiPrintMemoryImage)(int, LPBYTE, DWORD);
typedef int (WINAPI* DLL_BiGetPrintSize)(int, LPWORD, LPWORD);
typedef int (WINAPI* DLL_BiSetPrintSize)(int, WORD, WORD);
typedef int (WINAPI* DLL_BiGetPrintPosition)(int, LPWORD, LPWORD);
typedef int (WINAPI* DLL_BiSetPrintPosition)(int, WORD, WORD);
typedef int (WINAPI* DLL_BiUpdateEndorseText)(int, LPSTR[3], DWORD[3], WORD[3], WORD[3]);
typedef int (WINAPI* DLL_BiBufferedPrint)(int, DWORD);
typedef int (WINAPI* DLL_BiSetTransactionNumberWithIncremental)(int, DWORD, DWORD);
typedef int (WINAPI* DLL_BiSetBehaviorToScnResult)(int, BYTE, BYTE, BYTE);
typedef int (WINAPI* DLL_BiRingBuzzer)(int, BYTE, BYTE, WORD, WORD);
typedef int (WINAPI* DLL_BiGetVersion)(int, int, LPVERSION_INFO);
typedef int (WINAPI* DLL_BiSetNumberOfDocuments)(int, BYTE);

typedef int (WINAPI* DLL_BiESCNEnable)(BYTE);
typedef int (WINAPI* DLL_BiESCNGetAutoSize)(int, LPBYTE);
typedef int (WINAPI* DLL_BiESCNSetAutoSize)(int, BYTE);
typedef int (WINAPI* DLL_BiESCNGetCutSize)(int, LPWORD);
typedef int (WINAPI* DLL_BiESCNSetCutSize)(int, WORD);
typedef int (WINAPI* DLL_BiESCNGetRotate)(int, LPBYTE);
typedef int (WINAPI* DLL_BiESCNSetRotate)(int, BYTE);
typedef int (WINAPI* DLL_BiESCNGetDocumentSize)(int, LPWORD, LPWORD);
typedef int (WINAPI* DLL_BiESCNSetDocumentSize)(int, WORD, WORD);
typedef int (WINAPI* DLL_BiESCNGetDeSkew)(int, LPWORD);
typedef int (WINAPI* DLL_BiESCNSetDeSkew)(int, WORD);
typedef int (WINAPI* DLL_BiESCNDefineCropArea)(int, BYTE, WORD, WORD, WORD, WORD);
typedef int (WINAPI* DLL_BiESCNGetMaxCropAreas)(int, LPBYTE);
typedef int (WINAPI* DLL_BiESCNStoreImage)(int, DWORD, LPSTR, LPSTR, BYTE);
typedef int (WINAPI* DLL_BiESCNRetrieveImage)(int, DWORD, LPSTR, LPSTR, LPDWORD, LPBYTE);
typedef int (WINAPI* DLL_BiESCNClearImage)(int, BYTE, DWORD, LPSTR, LPSTR);
typedef int (WINAPI* DLL_BiESCNGetRemainingImages)(int, LPBYTE);

#endif // !defined(_EPSSTMAPIINTERFACE_H)
