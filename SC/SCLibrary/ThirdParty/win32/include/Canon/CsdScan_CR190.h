#ifndef _CSD_SCAN_H_INCLUDED
#define _CSD_SCAN_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#ifdef CSD_EXPORTS
#define CSD_API __declspec(dllexport)
#else
#define CSD_API __declspec(dllimport)
#endif

#include <windows.h>
#include <pshpack4.h>

/* //////////////////////////////////////////////////////////
//
// CSD API Version
//
////////////////////////////////////////////////////////// */
#define CSD_APIVERSION		"1.012"


/* //////////////////////////////////////////////////////////
//
// CSD Status
//
////////////////////////////////////////////////////////// */
/* {{ CSD_ERROR_CODE */
#define CSD_OK                    0
#define CSD_NOPAGE                1
#define CSD_NODEVICE              2
#define CSD_BADPARMNO             3
#define CSD_BADFILE               4
#define CSD_BADPARM               5
#define CSD_NOPAPER               6
#define CSD_JAM                   7
#define CSD_COVEROPEN             8
#define CSD_POWERON               9
#define CSD_BADFILE0             10
#define CSD_BADFILE1             11
#define CSD_COUNTONLY            12
#define CSD_COUNTMISS            13
#define CSD_ABORTED              14
#define CSD_RESFAIL              15
#define CSD_NOTREADY             16
#define CSD_HARDERROR            17
#define CSD_NOTSELECTED          18
#define CSD_NEWFILE              19
#define CSD_DOUBLEFEED           20
#define CSD_SKEWFEED             21
#define CSD_FILMEND				 22
#define CSD_NOCAMERA			 23
#define CSD_BADLOGFILE			 24
#define CSD_FILMERROR			 25
#define CSD_NOMEM				 26
#define CSD_UNKNOWN				 27
#define CSD_ENDOFPAGE			 28
#define CSD_CANCEL				 29
#define CSD_NOCARTRIDGE			 30
#define CSD_COUNTMISSTOOMANY	 31
#define CSD_COUNTMISSTOOFEW		 32
#define CSD_STAPLEDETECTED		 33
#define CSD_DELIVERYFULL		 34
#define CSD_TIMEOUT				 35

#define CSD_SOFTWARE			1000
#define CSD_NOTFINDMODULE		1001
#define CSD_DRIVERBUSY			1002
/* CSD_ERROR_CODE }} */



/* //////////////////////////////////////////////////////////
//
// Parameter type
//
////////////////////////////////////////////////////////// */
#define CSDTAG_ASCII        2
#define CSDTAG_LONG         4
#define CSDTAG_RATIONAL     5



/* //////////////////////////////////////////////////////////
//
// Parameter choices flags
//
////////////////////////////////////////////////////////// */
#define CSDCHOICEFLAG_RANGE     0x00020000L
#define CSDCHOICEFLAG_LIST      0x00040000L
#define CSDCHOICEFLAG_ANY		0x00080000L
#define CSDCHOICEFLAG_SYMRANGE	0x00100000L



/* //////////////////////////////////////////////////////////
//
// Parameter choices index
//
////////////////////////////////////////////////////////// */
#define CSDCHOICE_LOW   0xffffffff
#define CSDCHOICE_HIGH  0xfffffffe
#define CSDCHOICE_STEP  0xfffffffd



/* //////////////////////////////////////////////////////////
//
// CsdParSet/Get parameters
//
////////////////////////////////////////////////////////// */
/* {{ CSD_PARAMETER */
#define CSDP_WIDTH				   1
#define CSDP_PAGEWIDTH			   CSDP_WIDTH
#define CSDP_LENGTH				   2
#define CSDP_PAGELENGTH			   CSDP_LENGTH
#define CSDP_FEEDER				   3
#define CSDP_ED					   4
#define CSDP_BRIGHTNESS			   5
#define CSDP_XRESOLUTION		   6
#define CSDP_YRESOLUTION           7
#define CSDP_COMPRESSION           8
#define CSDP_FUNCTIONSHEET         9
#define CSDP_CONTROLLSHEET        10
#define CSDP_JOBSEPARATOR         11
#define CSDP_AUTOSIZE             12
#define CSDP_FUNCTIONRESULT       13
#define CSDP_FEEDINGOPTION        14
#define CSDP_DOCUMENTNAME         15
#define CSDP_DOCUMENTNAMELENGTH   16
#define CSDP_READAHEAD            17
#define CSDP_WRITEAMOUNT          18
#define CSDP_FILETYPE             19
#define CSDP_BITPERPIXEL          20
#define CSDP_BITSPERSAMPLE		  CSDP_BITPERPIXEL
#define CSDP_CONTRAST             21
#define CSDP_MODE                 22
#define CSDP_HIGHSPEED            23
#define CSDP_SCANWIDTH            24
#define CSDP_SCANHEIGHT           25
#define CSDP_VERIFYSCAN           26
#define CSDP_BACKSCAN             27
#define CSDP_BACKBRIGHTNESS       28
#define CSDP_DBLFEEDTHICK         29
#define CSDP_DBLFEEDLENGTH        30
#define CSDP_NOBUZZER             31
#define CSDP_MARGIN               32
#define CSDP_MARGINVAL            33
#define CSDP_FIXEDTHICK           34
#define CSDP_FIXEDTHICKVAL        35
#define CSDP_AUTONEWFILE          36
#define CSDP_BKBRIGHTNESSVAL      37
#define CSDP_NEWFILE              38
#define CSDP_FRAMEAREA            39
#define CSDP_XOFFSET              40
#define CSDP_YOFFSET              41
#define CSDP_PICTURE              49
#define CSDP_DROPCOLOR            50
#define CSDP_DROPINHSTART         51
#define CSDP_DROPINHEND           52
#define CSDP_EDGEEMPHASIS         53
#define CSDP_BDOTERASE            54
#define CSDP_WDOTERASE            55
#define CSDP_NOTCHERASE           56
#define CSDP_CTHINLINE            57
#define CSDP_AEINTENSITY          58
#define CSDP_GRC                  59
#define CSDP_IMPRINTER            60
#define CSDP_ADDON                61
#define CSDP_IMPSTRING            62
#define CSDP_IMPPTIMING           63
#define CSDP_IMPXPOS              64
#define CSDP_IMPYPOS              65
#define CSDP_IMPSMALLFONT         66
#define CSDP_IMPADDONFMT          67
#define CSDP_IMPCHARROT           68
#define CSDP_IMPPORDERREV         69
#define CSDP_IMPCUPT1             70
#define CSDP_IMPCUPA1             71
#define CSDP_IMPCRT1              72
#define CSDP_IMPCRV1              73
#define CSDP_IMPCUPT2             74
#define CSDP_IMPCUPA2             75
#define CSDP_IMPCRT2              77
#define CSDP_IMPCRV2              78
#define CSDP_IMPXPOS_SORDER       79
#define CSDP_BARCODE              80
#define CSDP_BARNUM               81
#define CSDP_BARSIDE              82
#define CSDP_BARBUZZER            83
#define CSDP_BARAREA              84
#define CSDP_BARAREASX            85
#define CSDP_BARAREAEX            86
#define CSDP_BARAREASY            87
#define CSDP_BARAREAEY            88
#define CSDP_NW7MODE              89
#define CSDP_CODE39MODE           90
#define CSDP_CODE128MODE          91
#define CSDP_2OF5MODE             92
#define CSDP_UPCAMODE             93
#define CSDP_UPCEMODE             94
#define CSDP_EANMODE              95
#define CSDP_BARCODEDATA          96
#define CSDP_BARRESULTNUM         97
#define CSDP_ENDORSER             98
#define CSDP_NEGAPOSI             99
#define CSDP_GAMMAGRAY		100
#define CSDP_GAMMARED		101
#define CSDP_GAMMAGREEN		102
#define CSDP_GAMMABLUE		103
#define CSDP_DATETIME		104
#define CSDP_MAXWIDTH	105
#define CSDP_MAXLENGTH	106
#define CSDP_STDRESOLUTION	107
#define CSDP_NEGATIVE	108
#define CSDP_JPEGQUALITY	109
#define CSDP_FKEY1		110
#define CSDP_FKEY2		111
#define CSDP_FKEY3		112
#define CSDP_FKEY4		113
#define CSDP_FKEY5		114
#define CSDP_FKEY6		115
#define CSDP_FKEY7		116
#define CSDP_FKEY8		117
#define CSDP_FKEY9		118
#define CSDP_FKEY10		119
#define CSDP_AEMODE		120
#define CSDP_BATCHTYPE		121
#define CSDP_BATCHNUMBER	122
#define CSDP_FILENUMBER		123
#define CSDP_PAGENUMBER		124
#define CSDP_ROTATE90		125
#define CSDP_DETECTEDWIDTH	126
#define CSDP_DETECTEDLENGTH	127
#define CSDP_SEPARATIONMODE	128
#define CSDP_FSC3	129
#define CSDP_BACKGROUNDERASE	130
#define CSDP_MICR 				131
#define CSDP_MICRDATALEN 		132	
#define CSDP_MICRDATA 			133
#define CSDP_CAMERAID 			134
#define CSDP_CAMERAREDUCTRATIO 	135		
#define CSDP_CAMERATHICKNESS 	136		
#define CSDP_CAMERASD			137	
#define CSDP_CAMERANEAREND 		138	
#define CSDP_CAMERAODOMETER 	139		
#define CSDP_CAMERAINFORMATION 	140		
#define CSDP_PATCHCODE 			141
#define CSDP_PATCHCODE_TYPE 	142		
#define CSDP_PATCHSIDE 			143
#define CSDP_PATCHAREA 			144
#define CSDP_PATCHORIENT 		145	
#define CSDP_PATCHAREASX 		146	
#define CSDP_PATCHAREAEX 		147	
#define CSDP_PATCHAREASY 		148	
#define CSDP_PATCHAREAEY		149
#define CSDP_LOGFILENAME		150
#define CSDP_LOGFILENAMELEN		151
#define CSDP_LOG				152
#define CSDP_OPTION_PATCHCODE	153
#define CSDP_OPTION_MICR		154
#define CSDP_OPTION_BARCODE		155
#define CSDP_OPTION_ENDORSER	156
#define CSDP_OPTION_RGBLED		157
#define CSDP_BACKUPMODE			158
#define CSDP_BLIPCONTROL		159
#define CSDP_OPTION_FILMEXPOSE	160
#define CSDP_OPTION_ROLLNUMBER	161
#define CSDP_FILMEXPOSE_BATCH	162
#define CSDP_FILMEXPOSE_FILE	163
#define CSDP_FILMEXPOSE_PAGE	164
#define CSDP_ROLLNUMBER			165
#define CSDP_BATCHNUMBEREX		166
#define CSDP_FILENUMBEREX		167
#define CSDP_PAGENUMBEREX		168
#define CSDP_SAMPLESPERPIXEL	169
#define CSDP_MICR_FONT			170
#define CSDP_DITHER				171
#define CSDP_MAXDOCUMENT		172
#define CSDP_IMPBMP				173
#define CSDP_DBLFEEDUSS			174
#define CSDP_IMPBMP_FILE		175
#define CSDP_CONVEY_DOCUMENT	176
#define CSDP_JOGDOCUMENT		177
#define CSDP_PAPER_ORIENTATION	178
#define CSDP_SCANNER_STATUS		179
#define CSDP_IMAGEWIDTH			180
#define CSDP_IMAGELENGTH		181
#define CSDP_PIXEL_FLAVOR		182
#define CSDP_WINDOW				183
#define CSDP_WINDOWCOUNT_FRONT	184
#define CSDP_WINDOWCOUNT_BACK	185
#define CSDP_LOAD_CONFIG_FILE	186
#define CSDP_SAVE_CONFIG_FILE	187
#define CSDP_OCR				188
#define CSDP_OCR_FONT			189
#define CSDP_OCR_XPOS			190
#define CSDP_OCR_YPOS			191
#define CSDP_OCR_WIDTH			192
#define CSDP_OCR_LENGTH			193
#define CSDP_GET_INQUIRY		194
#define CSDP_MICR_E13B_POSITION	195
#define CSDP_AUTOSTART_WAIT_TIME	196
#define CSDP_MULTIPAGE_FILE		197
#define CSDP_FEEDER_LOADED		198
#define CSDP_COUNT_DOCUMENTS	199
#define CSDP_LOGRENAMEFILENAMESTRING	200
#define CSDP_FRONT_MODE			201
#define CSDP_BACK_MODE			202
#define CSDP_FRONT_BITPERPIXEL	203
#define CSDP_BACK_BITPERPIXEL	204
#define CSDP_FRONT_BRIGHTNESS	205
#define CSDP_BACK_BRIGHTNESS	206
#define CSDP_FRONT_CONTRAST		207
#define CSDP_BACK_CONTRAST		208
#define CSDP_SORT_METHOD		209
// #define CSDP_SORT_METHOD_POS	210
// #define CSDP_SORT_METHOD_ENDPOS	211
// #define CSDP_SORT_METHOD_STR1	212
// #define CSDP_SORT_METHOD_STR2	213
#define CSDP_SORT_RESULT			214
#define CSDP_TIFF_GET_PAGES			215
#define CSDP_OCRDATA				216
#define CSDP_OCRDATALEN				217
#define CSDP_BRIGHTNESS_MANUAL		218
#define CSDP_BRIGHTNESS_AUTO		219
#define CSDP_CONTRAST_MANUAL		220
#define CSDP_CONTRAST_AUTO			221
#define CSDP_IMPYPOSFROM			222
#define CSDP_GET_MAINTENANCE_INFORMATION	223
#define CSDP_SCAN_JOGTIME			224
#define CSDP_IMPCHARFONT			225
#define CSDP_SAVEMICRLINE			226
#define CSDP_TRACE_LOG				227
#define CSDP_TRACE_LOG_FILE			228
#define CSDP_START_VIEW				229
#define CSDP_CLOSE_VIEW				230
#define CSDP_DISPLAY_VIEW_FRONT		231
#define CSDP_DISPLAY_VIEW_BACK		232
#define CSDP_REFRESH_VIEW			233
#define CSDP_COLOREMPHASIS			234
#define CSDP_MICR_SPACE_FORMAT		235
#define CSDP_MICR_REMOVE_SPACE		236
//#define CSDP_SORT_METHOD_STRLEN	237
#define CSDP_GAMMAGRAY_FRONT		238
#define CSDP_GAMMARED_FRONT			239
#define CSDP_GAMMAGREEN_FRONT		240
#define CSDP_GAMMABLUE_FRONT		241
#define CSDP_GAMMAGRAY_BACK			242
#define CSDP_GAMMARED_BACK			243
#define CSDP_GAMMAGREEN_BACK		244
#define CSDP_GAMMABLUE_BACK			245
#define CSDP_POCKETNUM				246
#define CSDP_OCR_SPACE_FORMAT		247
#define CSDP_OCR_REMOVE_SPACE		248
#define CSDP_MICR_CHAR_INFO			249
#define CSDP_OCR_CHAR_INFO			250
#define CSDP_GET_PAGE_NUMBER		251
#define CSDP_STANDARD_OUTPUT_POCKET	252
#define CSDP_PDF_PARAM              253
#define CSDP_MICR_CHECK_DIGIT		254
#define CSDP_REJECT_MICR_DECODE_ERROR	255
#define CSDP_IMPRINTED_STRING		256
#define CSDP_DESKEW					257
#define CSDP_CROP					258
#define CSD_TEXTENHANCE_BRIGHTNESS	259
#define CSDP_TEXTENHANCE_BRIGHTNESS	CSD_TEXTENHANCE_BRIGHTNESS
#define CSD_ASC_BRIGHTNESS			260
#define CSDP_ASC_BRIGHTNESS			CSD_ASC_BRIGHTNESS
#define CSDP_OCR_POS_ORIGIN			261
#define CSDP_IMPRESETCOUNTER		262
#define CSDP_SORT_TOO_LATE			263
#define CSDP_FRONT_SAMPLEPERPIXEL	264
#define CSDP_BACK_SAMPLEPERPIXEL	265
#define CSDP_MICR_SYMBOLCHAR		266
#define CSDP_STAPLEDETECT			267
#define CSDP_DECIDEPOCKET			268
#define CSDP_CALLBACK_FUNC			269
#define CSDP_SORTBY					270
#define CSDP_BARROTATION			271
#define CSDP_IMPRESETSTRING			272
#define CSDP_POCKETFULLSTOP			273
#define CSDP_GET_POCKET_STATUS		274
#define CSDP_PC_SORT_AVAILABLE		275
#define CSDP_CAN_DETECT_DELIVERY_EMPTY		276
#define CSDP_CAN_DETECT_DELIVERY_FULL		277
#define CSDP_IMPRINTER_AVAILABLE			278
#define CSDP_MICR_SENSITIVITY_AVAILABLE		279
#define CSDP_MICR_SENSITIVITY_LEVEL	280
#define CSDP_MICR_PLURALITY_LEVEL	281
#define CSDP_DBLFEEDINFRAREDLED		282
#define CSDP_IMPCUPAS1				283
#define CSDP_IMPCRVS1				284
#define CSDP_IMPCUPAS2				285
#define CSDP_IMPCRVS2				286
#define CSDP_OCRCHARPOSITION		287
#define CSDP_FRONT_TEXTENHANCE_BRIGHTNESS	288
#define CSDP_BACK_TEXTENHANCE_BRIGHTNESS	289
#define CSDP_SERIAL_NUMBER			290
#define CSDP_TEXTMODE_METHOD		291
#define CSDP_FRONT_TEXTMODE_METHOD	292
#define CSDP_BACK_TEXTMODE_METHOD	293
#define CSDP_LAST_DOCUMENT_THICKNESS	294
#define CSDP_DFD_THICKNESS_REFERENCE	295
#define CSDP_DFD_THICKNESS_REFERENCE_MIN	296
#define CSDP_CUSTOM_FILTER					297
#define CSDP_LAST_DOCUMENT_THICKNESS_MAX	298
#define CSDP_LAST_DOCUMENT_THICKNESS_MIN	299
#define CSDP_DFD_REFERENCE_AVAILABLE	318
#define CSDP_GET_NEXT_PAGE				319
#define CSDP_CURRENT_IMPCOUNTER1		320
#define CSDP_CURRENT_IMPCOUNTER2		321
#define CSDP_CURRENT_IMPCOUNTERS1		322
#define CSDP_CURRENT_IMPCOUNTERS2		323
#define CSDP_KEYCALLBACK_FUNC			324
#define CSDP_IMPDYNAMIC					325
#define CSDP_READ_LOCK_COUNT			326
#define CSDP_CURRENT_ADDONCOUNTER1		327
#define CSDP_CURRENT_ADDONCOUNTERS1		328
#define CSDP_ADDONCHARFONT				341
#define CSDP_ADDONCHARROT				342
#define CSDP_ADDONCRV1					343
#define CSDP_ADDONCRVS1					344
#define CSDP_ADDONCUPA1					345
#define CSDP_ADDONCUPAS1				346
#define CSDP_ADDONRESETCOUNTER			347
#define CSDP_ADDONED_STRING				348
#define CSDP_ADDONSTRING				349
#define CSDP_ADDONYPOS					350
#define CSDP_ADDONXPOS					352
#define CSDP_DIAGNOSTIC_CODE			353
#define CSDP_POCKETWATERFALL			354
#define CSDP_IQA_BRIGHTNESS				355
#define CSDP_IQA_BRIGHTNESS_RESULT		356
#define CSDP_IQA_BRIGHTNESS_VALUE		357
#define CSDP_IQA_FILESIZE				358
#define CSDP_IQA_FILESIZE_RESULT		359
#define CSDP_IQA_FILESIZE_VALUE			360
#define CSDP_IQA_SETTINGFILE			361
#define CSDP_IQA_FILETYPE				363
#define CSDP_IQA_COMPRESSION			364
#define CSDP_IQA_JPEGQUALITY			365
#define CSDP_DBLFEEDSTATUS				366
#define CSDP_MOCR						367
#define CSDP_FRONTIMAGE_AVAILABLE		368
#define CSDP_FRONTCOLORIMAGE_AVAILABLE	369
#define CSDP_FRONTDROPOUT_AVAILABLE		370
#define CSDP_BACKIMAGE_AVAILABLE		371
#define CSDP_BACKCOLORIMAGE_AVAILABLE	372
#define CSDP_BACKDROPOUT_AVAILABLE		373
#define CSDP_IMPBMP_AVAILABLE			374
#define CSDP_IMPMULTILINE_AVAILABLE		375
#define CSDP_IMPMAXLINE					376
#define CSDP_MICRAUTO_AVAILABLE			377
#define CSDP_MICRCMC7_AVAILABLE			378
#define CSDP_MICRE13B_AVAILABLE			379
#define CSDP_OCRAB_AVAILABLE			380
#define CSDP_OCRBARCODE1D_AVAILABLE		381
#define CSDP_OCRBARCODE2D_AVAILABLE		382
#define CSDP_MOCR_AVAILABLE				383
#define CSDP_FEEDERLIMIT				384
#define CSDP_MAXCPM						385
#define CSDP_APIVERSION					386
#define CSDP_JAMCOUNT					387
#define CSDP_WAITFEEDING				388
#define CSDP_LED_EZ						389
#define CSDP_TRACE_WRITEA				390
#define CSDP_TRACE_WRITEW				391
#define CSDP_IMPINKREMAIN				392
#define CSDP_DOCUMENTNUM				393
#define CSDP_JAMCOUNT_FEEDJAM			394
#define CSDP_JAMCOUNT_TRACKJAM			395
#define CSDP_LEDTIME					396
#define CSDP_TOTAL_PRINTED_DOCUMENT		397
#define CSDP_TOTAL_DFD_COUNT			398
#define CSDP_SORTPOCKET1_LIMIT			399
#define CSDP_SORTPOCKET2_LIMIT			400
#define CSDP_SORTPOCKET3_LIMIT			401
#define CSDP_SORTPOCKET4_LIMIT			402
#define CSDP_SORTPOCKET5_LIMIT			403
/* 404 - 408 reserved */
#define CSDP_IMPABSORB_RATE				409
#define CSDP_RESET_IMPABSORB_RATE		410


/* CSD_PARAMETER }} */


/* //////////////////////////////////////////////////////////
//
// Parameters
//
////////////////////////////////////////////////////////// */
#define CSD_NORMALAE             0
#define CSD_PICTUREAE            1
#define CSD_LOWCONTRASTAE        2

#define CSD_REMOTE             0
#define CSD_KEYSTART		   1
#define CSD_AUTOSTART          2
#define CSD_MANUAL             4

#define CSD_BATCHCURRENT       0
#define CSD_BATCHPAGE		   1
#define CSD_BATCHFILE          2
#define CSD_BATCHBATCH         3

#define CSD_SEPBATCH          0
#define CSD_SEPFILE           1
#define CSD_SEPBATCHFILE      2

/*IMPRINTER*/
#define CSD_IMPEACH_PAGE         0x00
#define CSD_IMPNEXT_SEP          0x01
#define CSD_IMPCROT0             0x00
#define CSD_IMPCROT90            0x01
#define CSD_IMPCROT180           0x02
#define CSD_IMPCROT270           0x03
#define CSD_IMPADDBLACK          0x00
#define CSD_IMPADDWHITE          0x01
#define CSD_IMPADDOR             0x02
#define CSD_IMPTRG_NEWFILE       0x00
#define CSD_IMPTRG_SEP           0x01
#define CSD_IMPTRG_NEWSEP        0x02
#define CSD_IMPTRG_EACHP         0x03
#define CSD_IMPTRG_NEVER         0x04
#define CSD_IMPTRG_FILE          0x05
#define CSD_IMPTRG_BATCH         0x06
#define CSD_IMPTRG_FILEBATCH     0x07
#define CSD_IMPFONT_NORMAL		 0x0
#define CSD_IMPFONT_SMALL		 0x1
#define CSD_IMPFONT_LORGE		 0x2
#define CSD_IMPCORDER_LTOR		 0x00
#define CSD_IMPCORDER_RTOL		 0x01
/*BARCODE*/
#define CSD_BAR_DECODE           0x0080
#define CSD_BAR_CDNONE           0x0040
#define CSD_BAR_NOCDOUT          0x0020
#define CSD_BAR_CDMD16           0x0000
#define CSD_BAR_CD7DR            0x0008
#define CSD_BAR_NOHD0OUT         0x0004
#define CSD_BAR_NOSTSPOUT        0x0002
#define CSD_BAR_ADDON            0x0001
#define CSD_BAR_ADDONAUTO        0x0100
#define CSD_BAR_FULLASCII        0x0200

#define CSD_BAR_FRONT            0x00
#define CSD_BAR_BACK             0x01
#define CSD_BAR_DUPLEX           0x02

#define CSD_BAR_ROT0             0x00
#define CSD_BAR_ROT90            0x01
#define CSD_BAR_ROT180           0x02
#define CSD_BAR_ROT270           0x03

#define CSD_BAR_ADDON_OFF        0x0000
#define CSD_BAR_ADDON_ON         CSD_BAR_ADDON
#define CSD_BAR_ADDON_AUTO       (CSD_BAR_ADDON | CSD_BAR_ADDONAUTO)

#define CSD_FEEDER_SIMPLEX            0
#define CSD_FEEDER_DUPLEX             1
#define CSD_FEEDER_BLANKSKIP          2
#define CSD_DETECT_OFF                0
#define CSD_DETECT_SCAN_CONTINUE      1
#define CSD_DETECT_STOP               2
#define CSD_DETECT_SCAN_STOP          3
#define CSD_DETECT_DISCARD            4
#define CSD_DETECT_DISCARD_CONTINUE   5
#define CSD_DETECT_DISCARD_STOP       7
#define CSD_DETECT_ON                 DETECT_DISCARD_CONTINUE
#define CSD_JOBSEPARATOR              1
#define CSD_DUPLEX_CONTROLL           2
#define CSD_SIMPLEX_CONTROLL          3
#define CSD_PHOTO_CONTROLL            4
#define CSD_TEXT_CONTROLL             5
#define CSD_BINARY                    1
#define CSD_BINARY_ED                 2
#define CSD_4BITGRAYSCALE			  3
#define CSD_GRAYSCALE                 4
#define CSD_COLOR                     8
#define CSD_BINARY_FINE               9
#define CSD_BINARY_PHOTO             10
#define CSD_BINARY_TEXTENHANCED		 11
#define CSD_GRAYSCALE_SMOOTHING      12
#define CSD_COLOR_SMOOTHING          13
#define CSD_BINARY_AUTOTHRESHOLD	 14
#define CSD_BINARY_DYNAMICTHRESHOLD	 15
#define CSD_BINARY_FINETEXTFILTERING 16

#define CSD_DITHER_NONE					0
#define CSD_ERROR_DIFFUSION				1

#define CSD_TEXTMETHOD_NONE				0
#define CSD_TEXTMETHOD_CC				1
#define CSD_TEXTMETHOD_ATE				2
#define CSD_TEXTMETHOD_NIT				3
#define CSD_TEXTMETHOD_TKBKG			4
#define CSD_TEXTMETHOD_SBLEDG			5

#define CSD_FILE_NONE					0
#define CSD_TIFF_FILE                 1
#define CSD_JPEG_FILE                 2
#define CSD_PDF_FILE                  3
#define CSD_JBIG_FILE                 4
#define CSD_BMP_FILE                  8

#define CSD_COMP_NONE                 0
#define CSD_COMP_MH                   1
#define CSD_COMP_MR                   2
#define CSD_COMP_MMR                  3
#define CSD_COMP_JPEG                 4
#define CSD_COMP_JBIG                 5

#define CSD_DROP_NONE                 0
#define CSD_DROP_RED                  1
#define CSD_DROP_GREEN                2
#define CSD_DROP_BLUE                 3

#define CSD_NEGA                      0
#define CSD_POSI                      1
#define CSD_NPAUTO                    2

#define CSD_FRAMECENTER               0
#define CSD_FRAMELEFT                 1
#define CSD_FRAMESEPARATE             2
#define CSD_FRAMEOVERLAY              3

#define CSD_SAVEALLSETTING           32
#define CSD_IMPRINTERONOFF           16
#define CSD_DBLFEEDONOFF              8
#define CSD_FIXEDSLITONOFF            4
#define CSD_CALIBONOFF                2
#define CSD_MANUALADJONOFF            1
#define CSD_FILMDENS7				0x8000
#define CSD_FILMDENS6				0x4000
#define CSD_FILMDENS5				0x2000
#define CSD_FILMDENS4				0x1000
#define CSD_FILMDENS3				0x0800
#define CSD_FILMDENS2				0x0400
#define CSD_FILMDENS1				0x0200
#define CSD_ODOMETERONOFF			0x010000
#define CSD_DISPPAGEADDRESS			0x020000
#define CSD_DISPFILEADDRESS			0x040000
#define CSD_DISPBATCHADDRESS		0x080000

#define CSD_ADDONONOFF				64
#define CSD_CENTERINGONOFF			128
#define CSD_DESKEWONOFF				256
#define CSD_ROTATECCLOCKWISE		512
#define CSD_ROTATECLOCKWISE			1024
#define CSD_STANDBYONOFF			2048
#define CSD_DARKENSCREEN			4096
#define CSD_LIGHTENSCREEN           8192
#define CSD_NEWFILEONOFF			16384
#define CSD_OVERLAYONOFF			32768
#define CSD_FINEMODEONOFF           65536
#define CSD_ADJUSTSHARPNESS         131072
#define CSD_ADJUSTMARGIN            262144
#define CSD_MARGINONOFF             524288
#define CSD_600DPIONOFF             1048576
#define CSD_SCANNERMODE             2097152
#define CSD_PRINTERMODE             4194304
#define CSD_BACKGROUNDERASE         8388608

#define CSD_DETECT_NONE			0
#define CSD_DETECT_OCRA			1
#define CSD_DETECT_OCRB			2
#define CSD_DETECT_CMC7			7
#define CSD_DETECT_E13B			13
#define CSD_DETECT_OCR			20
#define CSD_DETECT_CHKWRT		21

#define CSD_SORTPOCKET1				1
#define CSD_SORTPOCKET2				2
#define CSD_SORTPOCKET3				3
#define CSD_SORTPOCKET4				4
#define CSD_SORTPOCKET5				5

#define CSD_HORIZONTAL				0x1
#define CSD_VERTICAL				0x2
#define CSD_ORIENTATION_ROT0		0x3
#define CSD_ORIENTATION_ROT90		0x4
#define CSD_ORIENTATION_ROT180		0x5
#define CSD_ORIENTATION_ROT270		0x6

/* CSDP_PIXEL_FLAVOR */
#define CSD_CHOCOLATE				0x01
#define CSD_VANILLA					0x02

/* CSDP_SORT_METHOD */
#define CSD_SORT_METHOD_NONE		0x00		/* not sort */
#define CSD_SORT_METHOD_EQUAL		0x01		/* == string0 */
#define CSD_SORT_METHOD_LARGER		0x02		/* >= string0 */
#define CSD_SORT_METHOD_SMALLER		0x03		/* <= string0 */
#define CSD_SORT_METHOD_RANGE		0x04		/* string0 <= XXX  <= string1 */

/* CSDP_MICR_E13B_POSITION */
#define CSD_E13B_POS_ASCII			1			/* Ascii */
#define CSD_E13B_POS_XEROX			2			/* Xerox position */
#define CSD_E13B_POS_PSP			3			/* Peple Soft position */
#define CSD_E13B_POS_NCR			4

/* CSDP_MICR_SYMBOLCHAR */
#define CSD_MICR_ASCII				1			/* Ascii */
#define CSD_MICR_XEROX				2			/* Xerox position */
#define CSD_MICR_PSP				3			/* Peple Soft position */
#define CSD_MICR_NCR				4

/* CSDP_IMPYPOSFROM */
#define CSD_LEADINGEDGE				0
#define CSD_TRAILEDGE				1

/* CSDP_COLOREMPHASIS */
#define CSD_COLOREMPHASIS_NONE		0
#define CSD_COLOREMPHASIS_RED		1
#define CSD_COLOREMPHASIS_GREEN		2
#define CSD_COLOREMPHASIS_BLUE		3
#define CSD_BOSSEMPHASIS_GRAY		4

/* CSDP_SKEW */
#define CSD_DESKEW_NONE				0
#define CSD_DESKEW_BY_ROLLER		1
#define CSD_DESKEW_BY_IMAGE			2

/* CSDP_MICR_SPACE_FORMAT */
#define CSDP_MICR_ALL_SPACES		0
#define CSDP_MICR_NO_SPACE			1

/* CSDP_OCR_SPACE_FONRMAT */
#define CSDP_OCR_ALL_SPACES			0
#define CSDP_OCR_NO_SPACE			1

/* CSDP_OCR_POS_ORIGIN */
#define CSD_ORIGIN_TOP					0x0000
#define CSD_ORIGIN_BOTTOM				0x0001
#define CSD_ORIGIN_VERMASK				0x00ff
#define CSD_ORIGIN_LEFT					0x0000
#define CSD_ORIGIN_RIGHT				0x0100
#define CSD_ORIGIN_LEADINGEDGE			0x0200
#define CSD_ORIGIN_TRAILEDGE			0x0300
#define CSD_ORIGIN_HORMASK				0xff00
#define CSD_ORIGIN_TOP_LEFT				CSD_ORIGIN_TOP | CSD_ORIGIN_LEFT
#define CSD_ORIGIN_BOOTM_RIGHT			CSD_ORIGIN_BOTTOM | CSD_ORIGIN_RIGHT
#define CSD_ORIGIN_BOTTOM_LEADINGEDGE	CSD_ORIGIN_BOTTOM | CSD_ORIGIN_LEADINGEDGE

/* CSD_SORTBY */
#define CSD_SORTBY_SCANNER				1
#define CSD_SORTBY_PC					2

/* CSD_POCKET */
#define CSD_POCKET1_EMPTY				0x0001	
#define CSD_POCKET2_EMPTY				0x0002
#define CSD_POCKET3_EMPTY				0x0004
#define CSD_POCKET1_FULL				0x0010
#define CSD_POCKET2_FULL				0x0020
#define CSD_POCKET3_FULL				0x0040

typedef VOID (CALLBACK* LPFNSCANCALLBACK)(DWORD dwReason, LPARAM lParam, INT32 nStatus);
typedef VOID (CALLBACK* LPFNSCANCALLBACKEX)(LPVOID pVoid, DWORD dwReason, LPARAM lParam, INT32 nStatus);
typedef LPFNSCANCALLBACK	LPFNKEYCALLBACK;

/* CSDP_CUSTOM_FILTER */
#define CSD_CUSTOMFILTER_NONE				0
#define CSD_CUSTOMFILTER_SMOOTH				0x01

/* CSDP_IMPDYNAMIC */
#define CSD_IMPDYNAMIC_DISABLE				0
#define CSD_IMPDYNAMIC_WORKAROUND			1
#define CSD_IMPDYNAMIC_ENABLE				2

/* CSDP_IMPFONT */
#define CSD_IMPFONT_LARGE_HIGHQUALITY		0x10
#define CSD_IMPFONT_LARGE_GORGEOUS			CSD_IMPFONT_LARGE_HIGHQUALITY	// use CSD_IMPFONT_LARGE_HIGHQUALITY instead
#define CSD_IMPFONT_LARGE_NORMAL			0x11
#define CSD_IMPFONT_LARGE_LITE				0x12	// this parameter removed
#define CSD_IMPFONT_LARGE_ECONOMY			0x13
#define CSD_IMPFONT_LARGE_ECO				CSD_IMPFONT_LARGE_ECONOMY	// use CSD_IMPFONT_LARGE_ECONOMY instead
#define CSD_IMPFONT_SMALL_HIGHQUALITY		0x20
#define CSD_IMPFONT_SMALL_GORGEOUS			CSD_IMPFONT_SMALL_HIGHQUALITY	// use CSD_IMPFONT_SMALL_HIGHQUALITY instead
#define CSD_IMPFONT_SMALL_NORMAL			0x21
#define CSD_IMPFONT_SMALL_LITE				0x22	// this parameter removed
#define CSD_IMPFONT_SMALL_ECONOMY			0x23
#define CSD_IMPFONT_SMALL_ECO				CSD_IMPFONT_SMALL_ECONOMY	// use CSD_IMPFONT_SMALL_ECONOMY instead

/* CSDP_IQA */
#define CSD_IQA_BRIGHTNESS_PASSED			0
#define CSD_IQA_BRIGHTNESS_TOOLIGHT			1
#define CSD_IQA_BRIGHTNESS_TOODARK			2
#define CSD_IQA_BRIGHTNESS_NORESULT			-1
#define CSD_IQA_FILESIZE_PASSED				0
#define CSD_IQA_FILESIZE_TOOLARGE			1
#define CSD_IQA_FILESIZE_TOOSMALL			2
#define CSD_IQA_FILESIZE_NORESULT			-1

/* CSDP_LED */
#define CSD_LED_OFF							0
#define CSD_LED_ON							1
#define CSD_LED_BLINK						2

/* //////////////////////////////////////////////////////////
//
// API Functions
//
////////////////////////////////////////////////////////// */
#ifndef _CEIIMGINFO_H
#define _CEIIMGINFO_H

typedef struct tagCEIIMAGEINFO {
	size_t	cbSize;				/* size of CEIIMAGEINFO */
	BYTE	*lpImage;			/* ptr of Image buffer */
	long	lXpos;				/* start dot of image */
	long	lYpos;				/* start line of image */
	long	lWidth;				/* width of image (dot) */
	long	lHeight;			/* heigth of image (line) */
	long	lSync;				/* line bytes */
	size_t	tImageSize;			/* buffer size */
	long	lBps;				/* bits per sample */
	long	lSpp;				/* samples per pixel */
	DWORD	dwRGBOrder;			/*  */
	long	lXResolution;		/* resolution x */
	long	lYResolution;		/* resolution y */
} CEIIMAGEINFO, *LPCEIIMAGEINFO;

#define CEIIMAGEINFO_V1_SIZE	CCSIZEOF_STRUCT(CEIIMAGEINFO, lYResolution)

typedef struct tagCEIIMAGEINFO2 {
	size_t	cbSize;				/* size of CEIIMAGEINFO2 */
	BYTE	*lpImage;			/* ptr of Image buffer */
	long	lXpos;				/* start dot of image */
	long	lYpos;				/* start line of image */
	long	lWidth;				/* width of image (dot) */
	long	lHeight;			/* heigth of image (line) */
	long	lSync;				/* line bytes */
	size_t	tImageSize;			/* buffer size */
	long	lBps;				/* bits per sample */
	long	lSpp;				/* samples per pixel */
	DWORD	dwRGBOrder;			/*  */
	long	lXResolution;		/* resolution x */
	long	lYResolution;		/* resolution y */
	/* V2 */
	long	nFileType;			/* File Format */
	long	nCompType;			/* Compression Type */
	long	nJpegQuality;		/* Quality of JPEG compression */
	DWORD	dwReserved;			/* Reserved ( must be 0 ) */
} CEIIMAGEINFO2, *LPCEIIMAGEINFO2;

#define CEIIMAGEINFO_V2_SIZE	CCSIZEOF_STRUCT(CEIIMAGEINFO2, nJpegQuality)

#ifndef RGB_ORDER
#define RGB_ORDER
	#define PIXEL_ORDER			0
	#define LINE_ORDER			1
	#define PSEUDOCOLOR			2
#endif	/* RGB_ORDER */

#endif	/* _CEIIMGINFO_H */

typedef struct tagCEIIMAGEFILEINFO {
	size_t	cbSize;				/* size of CEIIMAGEFILEINFO */
	long	nFileType;			/* File Format */
	long	nCompType;			/* Compression Type */
	long	nPage;				/* Page */
	long	nJpegQuality;		/* Quality of JPEG compression */
} CEIIMAGEFILEINFO, *LPCEIIMAGEFILEINFO;

#define CEIIMAGEFILEINFO_V1_SIZE	CCSIZEOF_STRUCT(CEIIMAGEFILEINFO, nJpegQuality)

typedef struct tagVIEWOFIMAGE {
	DWORD cbSize;
	HWND hwndFrontSideImage;		/* displays the front side image */
	HWND hwndBackSideImage;			/* displays the back side image */
} VIEWOFIMAGE, *LPVIEWOFIMAGE;

#define VIEWOFIMAGE_V1_SIZE	CCSIZEOF_STRUCT(VIEWOFIMAGE, hwndBackSideImage)

typedef struct tagMAINTENANCEINFO {
	DWORD cbSize;			/* size of MAINTENANCEINFO */
	DWORD dwScanCounter;		/* total number of scan */
} MAINTENANCEINFO, *LPMAINTENANCEINFO;

#define MAINTENANCEINFO_V1_SIZE	CCSIZEOF_STRUCT(MAINTENANCEINFO, dwScanCounter)

typedef struct tagGETPAGENUM {
	DWORD	cbSize;
	LPSTR	szFileName;
	INT32	nFileType;
	UINT	nPage;
} GETPAGENUM, *LPGETPAGENUM;

#define GETPAGENUM_V1_SIZE CCSIZEOF_STRUCT(GETPAGENUM, nFileType)

typedef struct tagCHARCANDIDATE {
	CHAR	cChar;				/* candidate */
	BYTE	byConfLevel;		/*Confidence level of the cChar */
} CHARCANDIDATE, *LPCHARCANDIDATE;

#define CHARCANDIDATE_V1_SIZE CCSIZEOF_STRUCT(CHARCANDIDATE, byConfLevel)

typedef struct tagRECOGCHARINFO {
	DWORD cbSize;		/* Specifies the length, in bytes, of the structure. */
	INT32	nIndex;		/* Set the index of the character that you want */
	INT32	nMaxNumCandidates;	/* Maximum number of chandidates */
	INT32	nNumCandidates;	/* Number of chandidates */
	CHARCANDIDATE	Candidate[ 1 ];
} RECOGCHARINFO, *LPRECOGCHARINFO;

#define RECOGCHARINFO_V1_SIZE CCSIZEOF_STRUCT(RECOGCHARINFO, Candidate)

typedef struct tagRECOGCHARINFO2 {
	DWORD cbSize;		/* Specifies the length, in bytes, of the structure. */
	INT32	nIndex;		/* Set the index of the character that you want */
	INT32	nMaxNumCandidates;	/* Maximum number of chandidates */
	INT32	nNumCandidates;	/* Number of chandidates */
	CHARCANDIDATE	Candidate[ 2 ];
} RECOGCHARINFO2, *LPRECOGCHARINFO2;

#define RECOGCHARINFO2_V1_SIZE CCSIZEOF_STRUCT(RECOGCHARINFO2, Candidate)

typedef struct tagOCRCHARPOSITION {
	DWORD	cbSize;		/* Specifies the length, in bytes, of the structure. */
	INT32	nIndex;		/* Set the index of the character that you want */
	RECT	charRect;	/* Position of the specified character */
} OCRCHARPOSITION, *LPOCRCHARPOSITION;

#define OCRCHARPOSITION_V1_SIZE CCSIZEOF_STRUCT(OCRCHARPOSITION, charRect)

typedef struct tagRECOGBARINFO {
	BYTE byType;
	BYTE byLength;
	BYTE byData[ 36 ];
} RECOGBARINFO, *LPRECOGBARINFO;

typedef struct tagREJECTPOCKET {
	DWORD cbSize;			/* sizeof REJECTPOCKET structure */
	/* Decode Error */
	BOOL	bRejectDecodeError;
	INT32	nRejectPocket;
} REJECTPOCKET, *LPREJECTPOCKET;

#define REJECTPOCKET_V1_SIZE CCSIZEOF_STRUCT(REJECTPOCKET, nRejectPocket)

typedef struct tagMICRCHECKDIGIT {
	DWORD cbSize;			/* sizeof MICRCHECKDIGIT structure */
	BOOL	bCheckDigit;
	INT32	nPocket;
	INT32	nDigit;
	BOOL	Reserved0;		/* must be 0 */
	LONG	Reserved1;		/* must be 0 */
	CHAR	cStartChar;
	BOOL	Reserved2;		/* must be 0 */
	LONG	Reserved3;		/* must be 0 */
	CHAR	cEndChar;
} MICRCHECKDIGIT, *LPMICRCHECKDIGIT;

#define MICRCHECKDIGIT_V1_SIZE CCSIZEOF_STRUCT(MICRCHECKDIGIT, cEndChar)

typedef struct tagSORTMETHOD {
	DWORD cbSize;			/* sizeof SORTMETHOD structure */
	INT32	nPocket;	
	INT32	nSortMethod;
	LONG	lUseStartPosChar;
	LONG	lPos;
	CHAR	cStartPos;
	LONG	lUseEndPosChar;
	LONG	lLen;
	CHAR	cEndPos;
	LPSTR	lpszComparison;
	BOOL	bPolarity;
} SORTMETHOD, *LPSORTMETHOD;

#define SORTMETHOD_V1_SIZE CCSIZEOF_STRUCT(SORTMETHOD, bPolarity)

typedef struct tagPDFPARAM {
	DWORD	cbSize;
	INT32	nType;
	INT32	nLanguage;
} PDFPARAM, *LPPDFPARAM;

#define PDFPARAM_V1_SIZE CCSIZEOF_STRUCT(PDFPARAM, nLanguage)

#define	PDFPAR_TYPE0	0
#define	PDFPAR_TYPE1	1
#define	PDFPAR_TYPE2	2
#define	PDFPAR_TYPE3	3
#define	PDFPAR_TYPE2EX	20

#define PDFPAR_L_JAPANESE	0x00000001
#define PDFPAR_L_ENGLISH	0x00000002
#define PDFPAR_L_CHINESE	0x00000004	/* should not use */
#define PDFPAR_L_FRENCH		0x00000008
#define PDFPAR_L_ITALIAN	0x00000010
#define PDFPAR_L_GERMAN		0x00000020
#define PDFPAR_L_SPANISH	0x00000040
#define PDFPAR_L_DUTCH		0x00000080
#define PDFPAR_L_PORTUGUESE	0x00000100
#define PDFPAR_DISABLEOCR	0x80000000


#define UPLDFLAG_SHOW_PROGRESS	0x00000001
#define UPLDFLAG_USE_CALLBACK	0x00000002
/* Upload firmware info */
typedef struct tagUPLOADFIRMWAREINFO {
	DWORD	dwSize;					/* size of this structure */
	LPSTR	lpstrFilename;			/* filename */
	DWORD	dwFlags;				/* flags */
	HWND	hwndOwner;				/* handle to owner window */
	LPFNSCANCALLBACKEX	lpfnFunc;	/* callback function */
	LPVOID	pParam;
} UPLOADFIRMWAREINFO, *LPUPLOADFIRMWAREINFO;


/* //////////////////////////////////////////////////////////
//
// Callback functions
//
////////////////////////////////////////////////////////// */

#define CBR_SCAN_COMPLETE	1
#define CBR_DOC_READ		2
#define CBR_SCAN_START		3
#define CBR_MICR			4
#define CBR_POCKET_EVENT	5
#define CBR_UPLOADPROGRESS	6
#define CBR_KEY_EVENT		7
#define CBR_DOC_DELETED		8
#define CBR_DBLFEED_EVENT	9
#define CBR_IMP_WARNING		10


/* CBR_KEY_EVENT */
#define CBR_KEY_EVENT_NONE					0
#define CBR_KEY_EVENT_START					1
#define CBR_KEY_EVENT_STOP					2
#define CBR_KEY_EVENT_STARTSTOP				3
#define CBR_KEY_EVENT_EZ					4


#define CBR_KEY_EVENT_STATUS_NONE			0		
#define CBR_KEY_EVENT_STATUS_CLICK			1
#define CBR_KEY_EVENT_STATUS_LONGPUSH		2

/* CBR_IMP_WARNING */
#define CBR_IMP_WARNING_NONE				0x00
#define CBR_IMP_WARNING_INKNEAREMPTY		0x01
#define CBR_IMP_WARNING_INKEMPTY			0x02
#define CBR_IMP_WARNING_INKLIMIT			0x04
#define CBR_IMP_WARNING_IMPABSORBNEARFULL	0x100
#define CBR_IMP_WARNING_IMPABSORBFULL		0x200

/* //////////////////////////////////////////////////////////
//
// API Functions
//
////////////////////////////////////////////////////////// */

CSD_API
INT32 WINAPI CsdGetDriverName( LPSTR lpszDriverName, LPSTR lpszVersion );

CSD_API
INT32 WINAPI CsdGetScannerName( LPSTR lpszScannerName );

CSD_API
LPCEIIMAGEINFO WINAPI CsdCreateImageEx(LONG lWidth, LONG lHeight, LONG lBps, LONG lSpp);

CSD_API
INT32 WINAPI CsdCreateImage( LPCEIIMAGEINFO lpInfo );

CSD_API
INT32 WINAPI CsdReleaseImage( LPCEIIMAGEINFO lpInfo );

CSD_API
INT32 WINAPI CsdSaveImage(LPCEIIMAGEINFO lpImage, LPCSTR szFileName, int nPage, LPARAM lFileType);

CSD_API
INT32 WINAPI CsdSaveImageEx( LPCEIIMAGEINFO lpImage, LPCEIIMAGEFILEINFO lpiInfo, LPCSTR szFileName );

CSD_API
INT32 WINAPI CsdLoadImage(LPCEIIMAGEINFO lpImage, LPCSTR szFileName, int nPage, LPARAM lFileType);

CSD_API
INT32 WINAPI CsdLoadImageEx( LPCEIIMAGEINFO lpImage, LPCEIIMAGEFILEINFO lpInfo, LPCSTR szFileName );

CSD_API
INT32 WINAPI CsdCompImage( LPCEIIMAGEINFO lpImage, LPCEIIMAGEFILEINFO lpInfo );

CSD_API
INT32 WINAPI CsdDecompImage( LPCEIIMAGEINFO lpImage );

CSD_API
INT32 WINAPI CsdGetPageNumber( LPCTSTR szFileName, LPLONG lpPage, LPARAM lFileType );

CSD_API
INT32 WINAPI CsdGetLastError();

CSD_API
void WINAPI CsdSetLastError( INT32 iErr );

CSD_API
INT32 WINAPI CsdProbe();

CSD_API
INT32 WINAPI CsdProbeEx(LPCSTR szDriverName);

CSD_API
INT32 WINAPI CsdTerminate();

CSD_API
INT32 WINAPI CsdCountOnly();

CSD_API
INT32 WINAPI CsdCountOnlyStart( LPFNSCANCALLBACK lpfnCallBack );

CSD_API
INT32 WINAPI CsdParGet(UINT uiParam, LPVOID lpParam);

CSD_API
INT32 WINAPI CsdParSet(UINT uiParam, LPARAM lParam);

CSD_API
INT32 WINAPI CsdParGetType(UINT uiParNo, UINT32 *lpType);

CSD_API
INT32 WINAPI CsdParGetChoiceFlags(UINT uiParNo, UINT32 *lpFlags);

CSD_API
INT32 WINAPI CsdParGetChoiceCount(UINT uiParNo, UINT32 *lpCount);

CSD_API
INT32 WINAPI CsdParGetChoice(UINT uiParNo, INT32 iIndex, LPVOID lpVoid);

CSD_API
INT32 WINAPI CsdParGetChoiceLen(UINT uiParNo, INT32 iIndex, UINT32 *lpLength);

CSD_API
INT32 WINAPI CsdParSaveFile(char *lpszFileName);

CSD_API
INT32 WINAPI CsdParRestoreFile(char *lpszFileName);

CSD_API
INT32 WINAPI CsdFormatString(INT32 nCode, LPSTR lpString, LPLONG lMaxLen);

CSD_API
INT32 WINAPI CsdScanOnePage( LPCSTR lpFileName );

CSD_API
INT32 WINAPI CsdScan( LPCSTR lpFileName );

CSD_API
INT32 WINAPI CsdStartScan( LPCSTR lpFileName, LPVOID lpReserved1, LPVOID lpReserved2 );

CSD_API
INT32 WINAPI CsdReadPage( LPCEIIMAGEINFO lpImage );

CSD_API
INT32 WINAPI CsdReadPageStart( LPCEIIMAGEINFO lpImage, LPFNSCANCALLBACK lpCallBack);

CSD_API
INT32 WINAPI CsdReadBuffer(LPBYTE lpBuffer, LPDWORD lpdwReadSize);

CSD_API
INT32 WINAPI CsdFlashScannedImage();

CSD_API
INT32 WINAPI CsdGetStatus();

CSD_API
INT32 WINAPI CsdDrvSetDialog(HINSTANCE hInstance, HWND hWnd);

CSD_API
INT32 WINAPI CsdDrvDialog(HINSTANCE hInstance, HWND hWnd, INT32 nDlgID);

CSD_API
INT32 WINAPI CsdStopScan();

CSD_API
INT32 WINAPI CsdAbortScan();

CSD_API
INT32 WINAPI CsdUploadFirmware(LPUPLOADFIRMWAREINFO pInfo);


#include <poppack.h>

#ifdef __cplusplus
}
#endif

#endif	/* _CSD_SCAN_H_INCLUDED */
