#ifndef QueuedDocumentProcessor_h
#define QueuedDocumentProcessor_h

// Base class.
#include <QtCore/QThread>
// Required stuff.
#include <RELibrary/RETypes>
#include <RELibrary/Engine>
#include "Controllers/AScannerController.h"
#include <QtCore/QSemaphore>
#include <QtCore/QMutexLocker>

namespace SC {

class QueuedDocumentProcessor : public QThread {
Q_OBJECT

public:
    // Constructors.
    /**/				QueuedDocumentProcessor	(RE::Engine *re=0, QObject *p=0) : QThread(p), _re(re), _mutex(INT_MAX) {}

    // Getter / Setter on recognition engine.
    RE::Engine*			recognitionEngine		() const			{ return _re; }
    void				setRecognitionEngine	(RE::Engine* re)	{ _re = re; }

    // Queues a document and schedule it for processing.
    void				queueDocument			(void *d) {
        // Document needs to be retained once.
        RE::T::Document	*doc	= (RE::T::Document*)d;
        doc->semaphore.acquire	();
        // Enqueue document.
        _mutex.acquire          ();
        // Make sure loop is processing documents.
        emit documentQueued(doc);
    }
    // Status getter.
    bool				hasPendingDocuments		() const {
        return _mutex.available()!=INT_MAX;
    }

protected:
    // Main run loop, actually just polls signal to slot.
    void				run						() {
        connect		(this, SIGNAL(documentQueued(void*)), this, SLOT(processDocument(void*)), Qt::QueuedConnection);
        exec		();
    }
protected slots:
    void						processDocument     	(void *docPtr) {
        RE::T::Document	*doc	=           (RE::T::Document*)docPtr;
        // Wait until the document has a front picture. The max total time to wait would be 80*25 ms.
        for (quint32 i=0; i<80; ++i) {
            { // Mutex guard.
                QMutexLocker    ml          (&doc->mutex);
                if (doc->images.contains("Front"))
                    break;
            }
            msleep                          (25);
        }
        // Lock document.
        doc->mutex.lockInline();
        // Recognize document type.
        _re->guessDocumentType              (doc, QThread::idealThreadCount());
        // Type is known, recognize document.
        if (doc->typeData)
            _re->recognizeDocumentData      (doc, QThread::idealThreadCount());
        // Unlock doc and release it.
        _mutex.release      ();
        doc->mutex.unlockInline();
        // Signal that everything is done.
        emit documentProcessed(doc);
    }

private:
    RE::Engine					*_re;
    QSemaphore                  _mutex;

signals:
    void						documentQueued				(void *doc);
    void						documentProcessed			(void *doc);
};

}

#endif
