#include "SCTypes.h"
#include "../../Versioning/SCLibraryVersion.h"
// Required stuff.
#include <LMLibrary/LMTypes>
#include <RELibrary/RETypes>
#include <QtCore/QDir>
#include <QtCore/QSettings>
#include <QtCore/QTextCodec>

SC::DeviceConfig::DeviceConfig ()
: enableLogging(false), deviceId(SC::E::DeviceIdentifierDummy), dataPath(0), locale(0) {
}
SC::DeviceConfig::DeviceConfig (SC::DeviceConfig const &dc) : dataPath(0), locale(0) {
    operator= (dc);
}
SC::DeviceConfig::~DeviceConfig () {
    SC::deleteAndCopy				(dataPath, 0);
    SC::deleteAndCopy				(locale, 0);
}
SC::DeviceConfig& SC::DeviceConfig::operator= (SC::DeviceConfig const &dc) {
    // Reset previously assigned strings.
    SC::deleteAndCopy				(dataPath, 0);
    SC::deleteAndCopy				(locale, 0);
    // Copy everything as a whole block.
    memcpy							(this, &dc, sizeof(SC::DeviceConfig));
    // Since pointers has been copied, reset string pointers and alloc them.
    SC::deleteAndCopy				(dataPath, dc.dataPath, false);
    SC::deleteAndCopy				(locale, dc.locale, false);
    return							*this;
}
void SC::DeviceConfig::readOverrides (SC::DeviceConfig &c, QSettings const &s) {
    c.enableLogging					= s.value("DeviceConfig/enableLogging",				(quint8)	c.enableLogging)	.value<quint8>();
    c.deviceId						= (SC::E::DeviceIdentifier)
                                      s.value("DeviceConfig/deviceId",                  (int)		c.deviceId)			.value<int>();
    QString		dp					= s.value("DeviceConfig/dataPath",					(QString)	c.dataPath)			.value<QString>();
    SC::deleteAndCopy				(c.dataPath, dp);
    QString		loc					= s.value("DeviceConfig/locale",					(QString)	c.locale)			.value<QString>();
    SC::deleteAndCopy				(c.locale, loc);
}

SC::Config::Config ()
: enableMicr(false), sides(SC::E::DocumentSideNone), reversedDocument(false), preferedResolution(0), enableColor(false),
enableImagesSaving(false), dropFeedEnabled(false), machineUid(0), enableDocUidReset(false),
enableRejectionTests(false), enableDoubleFeedingDetection(false),
enableRecognition(false), enableProprietaryRecognition(false), recognitionDataPath(0),
enableResultFilesSplit(false), maximumDocumentCount(0),
printData(0), printImageFilename(0),
printInBold(false), printOffset(125), printImageOffset(125),
onProcessingStarted(0), onProcessingFinished(0), onNewDocument(0), onDocumentRecognitionDone(0) {
}
SC::Config::Config (Config const &c) : recognitionDataPath(0), printData(0), printImageFilename(0) {
    operator=						(c);
}
SC::Config::~Config	() {
    SC::deleteAndCopy				(recognitionDataPath, 0);
    SC::deleteAndCopy				(printData, 0);
    SC::deleteAndCopy				(printImageFilename, 0);
}
SC::Config& SC::Config::operator= (const SC::Config &c) {
    // Reset previously assigned strings.
    SC::deleteAndCopy				(recognitionDataPath, 0);
    SC::deleteAndCopy				(printData, 0);
    SC::deleteAndCopy				(printImageFilename, 0);
    // Copy everything as a whole block.
    memcpy							(this, &c, sizeof(Config));
    // Since pointers has been copied, reset string pointers and alloc them.
    SC::deleteAndCopy				(recognitionDataPath, c.recognitionDataPath, false);
    SC::deleteAndCopy				(printData, c.printData, false);
    SC::deleteAndCopy				(printImageFilename, c.printImageFilename, false);
    return							*this;
}
void SC::Config::readOverrides (SC::Config &c, QSettings const &s) {
    c.enableMicr					= s.value("Config/enableMicr",						(quint8)	c.enableMicr)					.value<quint8>();
    c.dropFeedEnabled				= s.value("Config/dropFeedEnabled",					(quint8)	c.dropFeedEnabled)				.value<quint8>();
    c.sides							= (SC::E::DocumentSide)
                                      s.value("Config/sides",							(int)		c.sides)						.value<int>();
    c.reversedDocument				= s.value("Config/reversedDocument",				(quint8)	c.reversedDocument)				.value<quint8>();
    c.preferedResolution			= s.value("Config/preferedResolution",				(quint32)	c.preferedResolution)			.value<quint32>();
    c.enableColor					= s.value("Config/enableColor",						(quint8)	c.enableColor)					.value<quint8>();
    c.enableImagesSaving			= s.value("Config/enableImagesSaving",				(quint8)	c.enableImagesSaving)			.value<quint8>();
    c.machineUid					= s.value("Config/machineUid",						(quint32)	c.machineUid)					.value<quint32>();
    c.enableDocUidReset				= s.value("Config/enableDocUidReset",				(quint8)	c.enableDocUidReset)			.value<quint8>();
    c.enableRejectionTests			= s.value("Config/enableRejectionTests",			(quint8)	c.enableRejectionTests)			.value<quint8>();
    c.enableDoubleFeedingDetection	= s.value("Config/enableDoubleFeedingDetection",	(quint8)	c.enableDoubleFeedingDetection)	.value<quint8>();
    c.enableRecognition				= s.value("Config/enableRecognition",				(quint8)	c.enableRecognition)			.value<quint8>();
    c.enableProprietaryRecognition	= s.value("Config/enableProprietaryRecognition",	(quint8)	c.enableProprietaryRecognition)	.value<quint8>();
    // Recognition data path.
    QString		rdp					= s.value("Config/recognitionDataPath",				(QString)	c.recognitionDataPath)			.value<QString>();
    SC::deleteAndCopy				(c.recognitionDataPath, rdp);
    c.enableResultFilesSplit		= s.value("Config/enableResultFilesSplit",			(quint8)	c.enableResultFilesSplit)		.value<quint8>();
    c.maximumDocumentCount			= s.value("Config/maximumDocumentCount",			(quint32)	c.maximumDocumentCount)			.value<quint32>();
    // Print data.
    QString		pd					= s.value("Config/printData",						(QString)	c.printData)					.value<QString>();
    SC::deleteAndCopy				(c.printData, pd);
    // Print image path.
    QString		pi					= s.value("Config/printImageFilename",				(QString)	c.printImageFilename)			.value<QString>();
    SC::deleteAndCopy				(c.printImageFilename, pi);
    c.printInBold					= s.value("Config/printInBold",						(quint8)	c.printInBold)					.value<quint8>();
    c.printOffset					= s.value("Config/printOffset",						(quint32)	c.printOffset)					.value<quint32>();
    c.printImageOffset				= s.value("Config/printImageOffset",				(quint32)	c.printImageOffset)				.value<quint32>();
}

void SC::StaticInitializer () {
    RE::StaticInitializer                   ();

    static bool	initDone			= false;
    if (initDone)					return;
    initDone						= true;

    // Set translation encoding.
    QTextCodec::setCodecForTr       (QTextCodec::codecForName("UTF-8"));

    // Register all streaming operators.
    #define EnumFullRegistration(name)	qRegisterMetaType<name>(#name); qRegisterMetaTypeStreamOperators<int>(#name)
    EnumFullRegistration			(SC::E::ErrorCode);
    EnumFullRegistration			(SC::E::DeviceIdentifier);
    EnumFullRegistration			(SC::E::DeviceState);
    EnumFullRegistration			(SC::E::DocumentSide);
    #undef EnumFullRegistration

    #define TypeFullRegistration(name)	qRegisterMetaTypeStreamOperators<name>(#name); qRegisterMetaType<name>(#name)
    TypeFullRegistration			(SC::DeviceConfig);
    TypeFullRegistration			(SC::Config);
    #undef TypeFullRegistration
}

char const* SC::Version () {
    return const_cast<char*>(STRINGIZE(SCLibraryVersion));
}
