<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>QObject</name>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="126"/>
        <source>Licensing Issue</source>
        <translation>Problème de License</translation>
    </message>
</context>
<context>
    <name>SC::AScannerController</name>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="575"/>
        <source>Unicity check failed.</source>
        <translation>Le titre a déjà été lu.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="487"/>
        <source>Recognition is disabled.</source>
        <translatorcomment>	</translatorcomment>
        <translation>La reconnaissance est désactivée.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="539"/>
        <source>MICR didn&apos;t match with the guessed type.</source>
        <translation>Les donnée MICR sont erronées pour ce type.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="519"/>
        <source>Accepted by Callback #1.</source>
        <translation>Acceptation par le Callback #1.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="558"/>
        <source>Accepted by Callback #2.</source>
        <translation>Acceptation par le Callback #2.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="583"/>
        <source>Stolen document.</source>
        <translation>Titre volé.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="513"/>
        <source>Rejected by Callback #1.</source>
        <translation>Rejeté par Callback #1.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="478"/>
        <source>The Reject All flag is set.</source>
        <translation>Le marqueur de rejet inconditionnel est actif.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="500"/>
        <source>Rejection tests are disabled.</source>
        <translation>Les Tests de Rejet sont désactivés.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="530"/>
        <source>Unable to guess the document&apos;s type.</source>
        <translation>Impossible de déterminer le Type de Document.</translation>
    </message>
    <message>
        <location filename="../Controllers/AScannerController.cpp" line="553"/>
        <source>Rejected by Callback #2.</source>
        <translation>Rejeté par Callback #2.</translation>
    </message>
</context>
</TS>
