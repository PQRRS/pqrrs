#include "SCLibrary.h"
#include "Controllers/AScannerController.h"
#include <LMLibrary/LMTypes>
#include <LMLibrary/Core>
#include <RELibrary/RETypes>
#include <QtCore/QCoreApplication>
#include <QtCore/QDir>

#define deviceConfigFromHandle  ((SC::DeviceConfig*)devC)
#define configFromHandle        ((SC::Config*)config)
#define controllerFromHandle    ((SC::AScannerController*)scHndl)

static bool         initDone    = false;
#ifdef Q_OS_WIN
#include "../ThirdParty/win32/include/QtMfcApp/QMfcApp"
static HINSTANCE     hInst      = 0;
// Entry point.
BOOL WINAPI DllMain (HINSTANCE hInstance, DWORD, LPVOID) {
    hInst                       = hInstance;
    return true;
}
#endif

// Version getter.
SCLibOpt void const* SCCConv SCVersion () {
    return SC::Version();
}

// Static initializer.
SCLibOpt void SCCConv SCStaticInitializer () {
    if (initDone)        return;
    initDone            = true;
    if (!qApp) {
        bool    ok      = false;
        #ifdef Q_OS_WIN
            ok          = QMfcApp::pluginInstance(hInst);
        #else
            ok          = qApp!=0;
        #endif
        if (!ok) {
            int argc    = 0;
            new QCoreApplication(argc, NULL);
        }
    }
    // Remove default Qt plugins path.
    //foreach (QString const &p, QCoreApplication::libraryPaths())
    //    if (p.startsWith("C:/Qt"))
    //        QCoreApplication::removeLibraryPath(p);
    // Finally call SCLibrary static initializer.
    SC::StaticInitializer();
}

// Instanciation / Init / Config / Delete.
SCLibOpt void* SCCConv SCInstanciate (char const *licenseFIlename, void const *devC) {
    LM::Core            *lm  = new LM::Core();
    lm->loadFromFile         (licenseFIlename);
    return devC ? SC::AScannerController::instanciate (lm, *deviceConfigFromHandle) : 0;
}
SCLibOpt qint32 SCCConv SCInitialize (void *scHndl) {
    return scHndl && controllerFromHandle->initialize();
}
SCLibOpt qint32 SCCConv SCConfigure (void *scHndl, void const *config) {
    return scHndl && controllerFromHandle->configure(*configFromHandle);
}
SCLibOpt void SCCConv SCDelete (void *scHndl) {
    delete controllerFromHandle;
}

SCLibOpt qint32 SCCConv SCErrorCode (void *scHndl, quint8 reset) {
    return scHndl ? controllerFromHandle->errorCode(reset) : 0;
}

SCLibOpt qint32 SCCConv SCStartProcessing (void *scHndl, quint8 block) {
    if (!scHndl)        return false;
    return block ? controllerFromHandle->startBlockingProcessing() : controllerFromHandle->startProcessing();
}
SCLibOpt void SCCConv SCStopProcessing (void *scHndl) {
    if (!scHndl)        return;
     controllerFromHandle->stopProcessing();
}

// Query documents strings...
SCLibOpt quint32 SCCConv SCDocumentKeyAndStringSize (void *scHndl, quint32 docId, quint32 offset, quint32 &keySize, quint32 &valSize) {
    return scHndl ? controllerFromHandle->attachedREDocumentKeyAndStringSize(docId, offset, keySize, valSize) : 0;
}
SCLibOpt quint32 SCCConv SCDocumentKeyAndString (void *scHndl, quint32 docId, quint32 offset, quint8 *key, quint8 *val) {
    return scHndl ? controllerFromHandle->attachedREDocumentKeyAndString(docId, offset, (char*)key, (char*)val) : 0;
}
