#include "DummyController.h"
#include <LMLibrary/Core>
#include <RELibrary/Engine>
#include <QtCore/QDir>
#include <QtCore/QDebug>

using namespace SC;

DummyController::DummyController (LM::Core *lm, const DeviceConfig &config, QObject *parent)
: AScannerController(lm, config, parent) {
}

DummyController::~DummyController () {
}

QList<unsigned int>& DummyController::availableResolutions () const {
    static QList<unsigned int>	resolutions;
    if (resolutions.isEmpty())
        resolutions << 100 << 200 << 300;
    return resolutions;
}
unsigned int DummyController::deviceMaximumDocumentCount () const {
    return 200;
}
int DummyController::devicePrintMaxLength () const {
    return -1;
}
bool DummyController::deviceCanPrintsMultiline	() const {
    return false;
}

bool DummyController::doInitialize () {
    return true;
}
bool DummyController::doDeviceSerial (char *buff) {
    strcpy(buff, LM::Core::hostUuid().toUtf8().constData());
    return true;
}
bool DummyController::doConfigure (const Config&) {
    return true;
}
bool DummyController::doStartProcessing () {
    if (!isRunning())
        start                               ();
    return true;
}
void DummyController::doStopProcessing () {
}

void DummyController::run () {
    DeviceConfig const		&devConfig		= deviceConfiguration();
    Config const			&config			= configuration();

    QHash<QString,QString>
        fakeMicr;
    QString					dp				= devConfig.dataPath && devConfig.dataPath[0] ? devConfig.dataPath : ".";
    QFile					fMicr			(QString("%1/Dummy/MICR.txt").arg(dp));
    QTextStream				sMicr			(&fMicr);
    fMicr.open								(QIODevice::ReadOnly);
    while (!sMicr.atEnd()) {
        QStringList			parts			= sMicr.readLine().split("->");
        if (parts.count()!=2)				continue;
        fakeMicr[parts[0]]					= parts[1];
    }
    fMicr.close								();

    QString					path			= QString("%1/Dummy").arg(dp);
    QStringList				fns				= QDir(path).entryList(QStringList() << "*.bmp" << "*.gif" << "*.jpg" << "*.png" << "*.tif" << "*.tiff", QDir::Files | QDir::NoDotAndDotDot);
    for (quint32 i=0; i<config.maximumDocumentCount && !fns.isEmpty(); i++) {
        QString const       &sfn            = fns.takeFirst();
        // Current picture is a rear one?
        if (sfn.contains("_R."))            continue;
        QString const		&fn				= QString("%1/Dummy/%2").arg(dp).arg(sfn);
        QFileInfo           fi              = QFileInfo(fn);
        QString             bn              = fi.baseName();
        createREDocument					(i);
        if (config.enableMicr && fakeMicr.contains(bn))
               attachStringToREDocument     (i, "General", "MICR", fakeMicr[bn]);
        attachImageToREDocument				(i, "Front", QImage(fn));
        // If there is a rear image, use it.
        if (config.sides & SC::E::DocumentSideRear) {
            QString             fnRear      = fn;
            fnRear.replace                  ("_F.", "_R.");
            if (QFile::exists(fnRear))
                attachImageToREDocument     (i, "Rear", QImage(fnRear));
        }
        shouldRejectREDocument				(i);
        releaseREDocument					(i);
    }
    finishedProcessing();
}
