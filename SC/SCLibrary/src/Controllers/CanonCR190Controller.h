#ifndef SC_CanonCR190Controller_h
#define SC_CanonCR190Controller_h

#include <QtCore/QtGlobal>
#ifdef Q_OS_WIN
// Base class.
#include "AScannerController.h"
// Required stuff.
#include <QtCore/QList>
#ifdef Q_OS_WIN
#   include <windows.h>
#endif

// Forward declarations.
namespace RE {
    namespace T {
        struct Document;
    }
}

namespace SC {

class SCLibOpt CanonCR190Controller : public AScannerController {
Q_OBJECT

public:
    // Constructor / Destructor.
                                    CanonCR190Controller        (LM::Core *lm, const DeviceConfig &devConfig, QObject *parent=0);
    virtual                         ~CanonCR190Controller       ();

    QList<quint32>&                 availableResolutions        () const;
    quint32                         deviceMaximumDocumentCount  () const;
    qint32                          devicePrintMaxLength        () const;
    bool                            deviceCanPrintsMultiline    () const;
    bool                            doInitialize                ();
    bool                            doDeviceSerial              (char *buff);
    bool                            doConfigure                 (const Config &config);
    bool                            doStartProcessing           ();
    void                            doStopProcessing            ();

protected:
    void                            run                            ();
    qint32                          processDocumentSide            (RE::T::Document *doc, SC::E::DocumentSide sides);

private:
    // Driver callback stuff.
    static void WINAPI                StaticDriverCB                (DWORD reason, LPARAM param, INT32 status);
    void                              driverCB                      (unsigned long reason, LPARAM param, quint32 status);

    // Singleton instance.
    static CanonCR190Controller        *_instance;
    // Library handler.
    static HMODULE                    _hLib;
    // Current document.
    RE::T::Document                    *_document;
};

};

#else
#include "DummyController.h"
namespace SC {
class SCLibOpt CanonCR190Controller : public DummyController {
Q_OBJECT
public:
    // Constructor / Destructor.
    /**/                            CanonCR190Controller        (LM::Core *lm, const DeviceConfig &devConfig, QObject *parent=0) : DummyController(lm, devConfig, parent) {}
};
}
#endif

#endif
