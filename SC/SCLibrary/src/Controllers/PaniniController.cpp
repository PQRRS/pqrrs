#include "PaniniController.h"

#ifdef Q_OS_WIN
#include <RELibrary/RETypes>
#include <QtCore/QDebug>
#include <time.h>
#include "../../ThirdParty/win32/include/Panini/VApiInterface.h"

#ifdef x86
#   define      WIN_HACK_GWLP_USERDATA  GWL_USERDATA
#else
#   define      WIN_HACK_GWLP_USERDATA  GWLP_USERDATA
#endif

// Library handlers.
HMODULE     SC::PaniniController::_hLib     = 0;

namespace _internalPaniniNamespace {
    typedef VAPI_RET_TYPE   (VISION_API* FNVAPISETDEVICEENGINE) (DWORD Selector);
    typedef DWORD           (VISION_API* FNSTARTUP)             (HWND Handle, UINT Message);
    typedef BOOL            (VISION_API* FNSHUTDOWN)            (DWORD DevId);
    typedef BOOL            (VISION_API* GETDEVICEFEATURE)      (DWORD DevId, DWORD, LPVOID, DWORD);
    typedef BOOL            (VISION_API* FNGETDEVICESTATE)      (DWORD DevId, DWORD*);
    typedef BOOL            (VISION_API* FNONLINE)              (DWORD DevId);
    typedef BOOL            (VISION_API* FNSETDEVICEPARAMETERS) (DWORD DevId, DeviceParameters);
    typedef BOOL            (VISION_API* FNCHANGEPARAMETERS)    (DWORD DevId);
    typedef BOOL            (VISION_API* FNSETSORTERPARAMETER)  (DWORD DevId, WORD, WORD);
    typedef BOOL            (VISION_API* FNGETDEVICEFEATURE)    (DWORD DevId, DWORD, LPVOID, DWORD);
    typedef BOOL            (VISION_API* FNSTARTFEEDING)        (DWORD DevId);
    typedef BOOL            (VISION_API* FNSTOPFEEDING)         (DWORD DevId);
    typedef BOOL            (VISION_API* FNSETIMAGEADJUSTEMENT) (DWORD DevId, int, int, BOOL);
    typedef BOOL            (VISION_API* FNSENDPRINTERDATA)     (DWORD DevId, DWORD, LOGFONTA, char*, DWORD, char*, DWORD, DWORD);
    typedef BOOL            (VISION_API* FNGETDOCUMENTLENGTH)   (DWORD DevId, DWORD, DWORD*);
    typedef BOOL            (VISION_API* FNFREEIMAGESBUFFER)    (ImagesStruct*);
    typedef BOOL            (VISION_API* FNGETMICRCODELINE)     (DWORD DevId, char*, DWORD);
    typedef BOOL            (VISION_API* FNGETOCRCODELINE)      (DWORD DevId, BYTE*, char*, DWORD, int, int);
    typedef BOOL            (VISION_API* FNENABLEFRANKING)      (DWORD DevId, DWORD, DWORD);
    typedef BOOL            (VISION_API* FNSETPOCKET)           (DWORD DevId, DWORD, BYTE);
    typedef BOOL            (VISION_API* FNFREETRACK)           (DWORD DevId, BYTE);
    typedef BOOL            (VISION_API* FNGETDEVICEERROR)      (DWORD DevId, DWORD*);
    typedef BOOL            (VISION_API* FNGETSORTERERROR)      (DWORD DevId, DWORD*);
    typedef VAPI_RET_TYPE   (VISION_API* FNVAPIGETERROR)        (void);
    FNVAPISETDEVICEENGINE   fnVApiSetDeviceEngine           = 0;
    FNSTARTUP               fnStartUp                       = 0;
    FNSHUTDOWN				fnShutDown                      = 0;
    FNGETDEVICESTATE        fnGetDeviceState                = 0;
    FNONLINE                fnOnLine                        = 0;
    FNSETDEVICEPARAMETERS   fnSetDeviceParameters           = 0;
    FNSETSORTERPARAMETER    fnSetSorterParameter            = 0;
    FNGETDEVICEFEATURE      fnGetDeviceFeature              = 0;
    FNCHANGEPARAMETERS      fnChangeParameters              = 0;
    FNSTARTFEEDING          fnStartFeeding                  = 0;
    FNSTOPFEEDING           fnStopFeeding                   = 0;
    FNSETIMAGEADJUSTEMENT   fnSetImageAdjustment            = 0;
    FNSENDPRINTERDATA       fnSendPrinterData               = 0;
    FNGETDOCUMENTLENGTH     fnGetDocumentLength             = 0;
    FNFREEIMAGESBUFFER      fnFreeImagesBuffer              = 0;
    FNGETMICRCODELINE       fnGetMicrCodeline               = 0;
    FNGETOCRCODELINE        fnGetOCRCodeline                = 0;
    FNENABLEFRANKING        fnEnableFranking                = 0;
    FNSETPOCKET             fnSetPocket                     = 0;
    FNFREETRACK             fnFreeTrack                     = 0;
    FNGETDEVICEERROR        fnGetDeviceError                = 0;
    FNGETSORTERERROR        fnGetSorterError                = 0;
    FNVAPIGETERROR          fnVApiGetError                  = 0;
}
using namespace _internalPaniniNamespace;

SC::PaniniController::PaniniController (LM::Core *lm, const SC::DeviceConfig &config, QObject *parent)
: AScannerController(lm, config, parent), _hWnd(0), _hDevice(0), _shouldStop(false) {
    // Prepare current instance handle.
    HINSTANCE	hMInst		= GetModuleHandle(0);
    // Prepare window type to be used for messages callbacks.
    WNDCLASSEX				wc;
    memset					(&wc, 0, sizeof(WNDCLASSEX));
    wc.cbSize				= sizeof(WNDCLASSEX);
    wc.lpfnWndProc			= staticListenerCallback;
    wc.hInstance			= hMInst;
    wc.lpszClassName		= L"PaniniListener";
    RegisterClassEx         (&wc);

    // Prepare font structure.
    memset					(&_font, 0, sizeof(LOGFONTA));
    _font.lfHeight			= 24;
    _font.lfWeight			= FW_NORMAL;
    _font.lfOutPrecision	= OUT_DEFAULT_PRECIS;
    _font.lfClipPrecision	= CLIP_DEFAULT_PRECIS;
    _font.lfQuality			= DEFAULT_QUALITY;
    _font.lfPitchAndFamily	= 49;
    strcpy					(_font.lfFaceName , "Arial");
}

SC::PaniniController::~PaniniController () {
    // Destroy the message pump window.
    if (_hWnd) {
        SetWindowLong       (_hWnd, GWLP_USERDATA, 0);
        CloseWindow         (_hWnd);
        DestroyWindow       (_hWnd);
        _hWnd               = 0;
    }
    // A device handle is pending.
    if (_hDevice) {
        stopProcessing      ();
        fnChangeParameters  (_hDevice);
        fnShutDown			(_hDevice);
    }
    // Wait for the processing thread to exit.
    while (isRunning()) {
        exit				();
        msleep				(50);
    }
    Dbg                     << "Destructor called.";
    // Unload the panini library.
    //if (_hLib) {
    //    FreeLibrary         (_hLib);
    //    _hLib               = 0;
    //}
}

SC::E::DeviceState SC::PaniniController::deviceState () const {
    // Get device state.
    DWORD	state	= 0;
    // No device handle.
    if (!_hDevice)
        return SC::E::DeviceStateUnknown;
    // Cannot get device state.
    else if (!fnGetDeviceState(_hDevice, &state))
        return SC::E::DeviceStateUnknown;
    // Build state code.
    switch (state) {
        // Busy state.
        case DeviceStartingUp:
        case DeviceShuttingDown:
        case DeviceChangingParameters:
        case DeviceFeeding:
        case DeviceExceptionInProgress:
        case DeviceLocked:
            return SC::E::DeviceStateBusy;
        // Up state.
        case DeviceOnLine:
        case DeviceStandBy:
            return SC::E::DeviceStateUp;
        // Down state.
        case DeviceShutDown:
        case DeviceOffLine:
            return SC::E::DeviceStateDown;
        // Configurable state.
        case DeviceChangeParameters:
            return SC::E::DeviceStateConfigurable;
        // Unknown state.
        default:
            return SC::E::DeviceStateUnknown;
    }
}
QList<quint32>& SC::PaniniController::availableResolutions () const {
    static QList<quint32>	resolutions;
    if (resolutions.isEmpty())
        resolutions << 100 << 200 << 300;
    return resolutions;
}
quint32 SC::PaniniController::deviceMaximumDocumentCount () const {
    switch (deviceConfiguration().deviceId) {
        case SC::E::DeviceIdentifierPaniniVisionS:
        case SC::E::DeviceIdentifierPaniniVisionX:
            return 200;
        case SC::E::DeviceIdentifierPaniniIDeal:
        case SC::E::DeviceIdentifierPaniniWIDeal:
        default:
            return 1;
    }
}
qint32 SC::PaniniController::devicePrintMaxLength () const {
    return -1;
}
bool SC::PaniniController::deviceCanPrintsMultiline	() const {
    switch (deviceConfiguration().deviceId) {
        case SC::E::DeviceIdentifierPaniniVisionS:
        case SC::E::DeviceIdentifierPaniniVisionX:
            return true;
        case SC::E::DeviceIdentifierPaniniIDeal:
        case SC::E::DeviceIdentifierPaniniWIDeal:
        default:
            return 1;
    }
}

bool SC::PaniniController::doInitialize () {
    // Load library.
    if (!_hLib) {
        _hLib		= LoadLibrary(L"VisionAPI.dll");
        if ((int)_hLib<32) {
            Crt << "Error loading VisionAPI.dll, setting error code to" << SC::E::ErrorCodeLibraryLoadingFailure << "(Lib Handle:" << _hLib << ", Failure code:" << GetLastError() << ").";
            _hLib                   = 0;
            setErrorCode(SC::E::ErrorCodeLibraryLoadingFailure);
            return false;
        }

        // Get pointers to library functions.
        fnVApiSetDeviceEngine   = (FNVAPISETDEVICEENGINE)	GetProcAddress(_hLib, "?VApiSetDeviceEngine@@YGKK@Z");
        fnStartUp               = (FNSTARTUP)               GetProcAddress(_hLib, "?StartUp@@YGKPAUHWND__@@I@Z");
        fnShutDown              = (FNSHUTDOWN)              GetProcAddress(_hLib, "?ShutDown@@YGHK@Z");
        fnGetDeviceState		= (FNGETDEVICESTATE)		GetProcAddress(_hLib, "?GetDeviceState@@YGHKPAK@Z");
        fnOnLine                = (FNONLINE)                GetProcAddress(_hLib, "?OnLine@@YGHK@Z");
        fnSetDeviceParameters	= (FNSETDEVICEPARAMETERS)	GetProcAddress(_hLib, "?SetDeviceParameters@@YGHKU_DeviceParameters@@@Z");
        fnSetSorterParameter    = (FNSETSORTERPARAMETER)    GetProcAddress(_hLib, "?SetSorterParameter@@YGHKGG@Z");
        fnGetDeviceFeature      = (FNGETDEVICEFEATURE)      GetProcAddress(_hLib, "?GetDeviceFeature@@YGHKKPAXK@Z");
        fnChangeParameters		= (FNCHANGEPARAMETERS)		GetProcAddress(_hLib, "?ChangeParameters@@YGHK@Z");
        fnStartFeeding			= (FNSTARTFEEDING)          GetProcAddress(_hLib, "?StartFeeding@@YGHK@Z");
        fnStopFeeding           = (FNSTOPFEEDING)           GetProcAddress(_hLib, "?StopFeeding@@YGHK@Z");
        fnSetImageAdjustment	= (FNSETIMAGEADJUSTEMENT)	GetProcAddress(_hLib, "?SetImageAdjustment@@YGHKHHH@Z");
        fnSendPrinterData       = (FNSENDPRINTERDATA)       GetProcAddress(_hLib, "?SendPrinterData@@YGHKKUtagLOGFONTA@@PADK1KK@Z");
        fnGetDocumentLength     = (FNGETDOCUMENTLENGTH)     GetProcAddress(_hLib, "?GetDocumentLength@@YGHKKPAK@Z");
        fnFreeImagesBuffer		= (FNFREEIMAGESBUFFER)      GetProcAddress(_hLib, "?FreeImagesBuffer@@YGHPAU_ImagesStruct@@@Z");
        fnGetMicrCodeline		= (FNGETMICRCODELINE)       GetProcAddress(_hLib, "?GetMicrCodeline@@YGHKPADK@Z");
        fnGetOCRCodeline		= (FNGETOCRCODELINE)        GetProcAddress(_hLib, "?GetOCRCodeline@@YGHKPAEPADKHH@Z");
        fnEnableFranking		= (FNENABLEFRANKING)        GetProcAddress(_hLib, "?EnableFranking@@YGHKKK@Z");
        fnSetPocket             = (FNSETPOCKET)             GetProcAddress(_hLib, "?SetPocket@@YGHKKE@Z");
        fnFreeTrack             = (FNFREETRACK)             GetProcAddress(_hLib, "?FreeTrack@@YGHKE@Z");
        fnGetDeviceError		= (FNGETDEVICEERROR)        GetProcAddress(_hLib, "?GetDeviceError@@YGHKPAK@Z");
        fnGetSorterError		= (FNGETSORTERERROR)        GetProcAddress(_hLib, "?GetSorterError@@YGHKPAK@Z");
        fnVApiGetError          = (FNVAPIGETERROR)          GetProcAddress(_hLib, "?VApiGetError@@YGKXZ");
        if (!fnVApiSetDeviceEngine || !fnStartUp || !fnShutDown ||
                !fnGetDeviceState || !fnOnLine || !fnSetDeviceParameters ||
                !fnSetSorterParameter || !fnGetDeviceFeature || !fnChangeParameters ||
                !fnStartFeeding || !fnStopFeeding || !fnSetImageAdjustment || !fnSendPrinterData ||
                !fnGetDocumentLength || !fnFreeImagesBuffer || !fnGetMicrCodeline ||
                !fnGetOCRCodeline || !fnEnableFranking || !fnSetPocket ||
                !fnFreeTrack || !fnGetDeviceError ||
                !fnGetSorterError || !fnVApiGetError) {
            Crt                 << "Error retrieving a callback from VisionAPI.dll, setting error code to" << SC::E::ErrorCodeLibraryFunctionFailure << " (Function pointers following)" <<
                                   fnVApiSetDeviceEngine << fnStartUp << fnShutDown <<
                                   fnGetDeviceState << fnOnLine << fnSetDeviceParameters <<
                                   fnSetSorterParameter << fnGetDeviceFeature << fnChangeParameters <<
                                   fnStartFeeding << fnStopFeeding << fnSetImageAdjustment << fnSendPrinterData <<
                                   fnGetDocumentLength << fnFreeImagesBuffer << fnGetMicrCodeline <<
                                   fnGetOCRCodeline << fnEnableFranking << fnSetPocket <<
                                   fnFreeTrack << fnGetDeviceError <<
                                   fnGetSorterError << fnVApiGetError;
            setErrorCode        (SC::E::ErrorCodeLibraryFunctionFailure);
            FreeLibrary         (_hLib);
            _hLib               = 0;
            return false;
        }
    }

    // Make sure thread is running.
    if (!isRunning()) {
        _waitMutex.lockInline();
        start				();
        _waitCondition.wait	(&_waitMutex);
        _waitMutex.unlockInline();
    }
    if (!_hWnd) {
        Crt << "Failed to create the proxy message pump, settings error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode(SC::E::ErrorCodeDeviceNotFound);
        return false;
    }

    // Initialize scanner.
    quint32     engineApi;
    switch (deviceConfiguration().deviceId) {
        case SC::E::DeviceIdentifierPaniniVisionS:
            engineApi       = VAPI_ENGINE_VS;
            break;
        case SC::E::DeviceIdentifierPaniniVisionX:
            engineApi       = VAPI_ENGINE_VX;
            break;
        case SC::E::DeviceIdentifierPaniniIDeal:
        case SC::E::DeviceIdentifierPaniniWIDeal:
            engineApi       = VAPI_ENGINE_ID;
            break;
        default:
            engineApi       = VAPI_ENGINE_BASE;
    }
    Dbg                     << "Engine identifier set to" << engineApi << ".";
    if (fnVApiSetDeviceEngine(engineApi)!=API_ERR_NONE) {
        Crt << "Failed to set up the device engine to " << engineApi << ", setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode(SC::E::ErrorCodeDeviceNotFound);
        return false;
    }

    // Try to get a handle on the device.
    _hDevice				= fnStartUp(_hWnd, WM_APP);
    // Make sure everything is okay.
    if (!_hDevice) {
        Crt << "Failed to wait for the device to show up, setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode(SC::E::ErrorCodeDeviceNotFound);
        return false;
    }

    // Wait for the device to be online.
    if (!waitForDeviceState(SC::E::DeviceStateConfigurable, 15)) {
        Crt << "Failed to wait for the device to show up, setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode(SC::E::ErrorCodeDeviceNotFound);
        return false;
    }

    // For VS / VX, make sure the ultrasound is ready.
    if (deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionS || deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionX) {
        // Check for ultra-sound sensor.
        DWORD	ret			= 0;
        fnGetDeviceFeature	(_hDevice, DEVICE_FEATURE_ULTRASONIC_DFD, &ret, sizeof(DWORD));
        bool	bUSPresent	= ret==DEVICE_FEATURE_YES;
        if (!bUSPresent) {
            Wrn << "Ultrasound sensor not detected, setting error code to" << SC::E::ErrorCodeIncompatibleDevice << ".";
            setErrorCode(SC::E::ErrorCodeIncompatibleDevice);
            //return false;
        }
        // Lock device speed to 50DPM.
        //fnSetMaxDPM(_hDevice, 75);
    }

    // Try to switch to online mode.
    if (!fnOnLine(_hDevice)) {
        DWORD	error;
        fnGetDeviceError(_hDevice, &error);
        Crt << "Failed to set the device to the online state with error code" << error << ", setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode(SC::E::ErrorCodeDeviceNotFound);
        return false;
    }
    // Finally take device up.
    return waitForDeviceState(SC::E::DeviceStateUp, 3);
}
bool SC::PaniniController::doDeviceSerial (char *buff) {
    return _hDevice && fnGetDeviceFeature(_hDevice, DEVICE_FEATURE_SN, buff, 20);
}
bool SC::PaniniController::doConfigure (const SC::Config&) {
    Config const	&config	= configuration();

    // Try to free the track in case anything got stuck inside.
    //if (deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionS ||
    //     deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionX)
    //    fnFreeTrack(_hDevice, 2);

    // Switch to config mode.
    if (!fnChangeParameters	(_hDevice)) {
        DWORD	error;
        fnGetDeviceError	(_hDevice, &error);
        Crt					<< "Unable to set device state to configurable with error code" << error << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode		(SC::E::ErrorCodeDeviceBadConfig);
        return				false;
    }
    // Device cannot be switched to the configurable state.
    else if (!waitForDeviceState(SC::E::DeviceStateConfigurable, 3)) {
        Crt					<< "Failed to wait for the device to be configurable, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode		(SC::E::ErrorCodeDeviceJammed);
        return				false;
    }

    bool        isSingleDocDevice           = deviceMaximumDocumentCount()==1;
    bool        oneDoc                      = isSingleDocDevice || config.dropFeedEnabled;
    quint32     feedMode                    = isSingleDocDevice ? DROP_FEED :  (config.dropFeedEnabled ? DROP_FEED : HOPPER_FEED);

    // Prepare parameters.
    DeviceParameters		params;
    memset									(&params, 0, sizeof(DeviceParameters));
    params.bMICREnable						= config.enableMicr;
    params.bMICRSaveSamples					= false;
    params.cRejectSymbol					= '?';
    params.nMICRFont						= MICR_FONT_CMC7;
    params.nMICRSpaces						= SPACES_NONE;
    params.bOneDoc							= oneDoc;
    params.nFeedingMode						= feedMode;
    // For VS / VX, enable printing.
    if (deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionS ||
            deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionX) {
        params.bPrintEnable                 = ((config.printData && config.printData[0]) || (config.printImageFilename && config.printImageFilename[0])) ?
                                                PRINTER_ENABLE_LEADING_EDGE | PRINTER_ENABLE_SMART_JET | PRINTER_AGP_QUALITY_NORMAL :
                                                PRINTER_DISABLE;
        // Set up print parameters.
        _font.lfWeight                      = config.printInBold ? FW_BOLD : FW_NORMAL;
    }

    #define IMAGE_FORMAT					config.enableColor ? FORMAT_JPEG_COLOR : FORMAT_BMP
    // Front side.
    if (config.sides & SC::E::DocumentSideFront) {
        IMAGE_PROPERTIES	*imgProps		= config.reversedDocument ? &params.ImagePropertiesRear1 : &params.ImagePropertiesFront1;
        imgProps->Format					= IMAGE_FORMAT;
        imgProps->Paging					= PAGING_ONLY_SINGLE;
        imgProps->Resolution				= closestAvailableResolution();
        imgProps->ColorDepth				= 256;
        imgProps->Threshold					= 75;
    }
    // Rear side.
    if (config.sides & SC::E::DocumentSideRear) {
        IMAGE_PROPERTIES	*imgProps		= config.reversedDocument ? &params.ImagePropertiesFront1 : &params.ImagePropertiesRear1;
        imgProps->Format					= IMAGE_FORMAT;
        imgProps->Paging					= PAGING_ONLY_SINGLE;
        imgProps->Resolution				= closestAvailableResolution();
        imgProps->ColorDepth				= 256;
        imgProps->Threshold					= 75;
    }

    // Front side.
    if (config.sides & SC::E::DocumentSideFront) {
        SNIPPET_PROPERTIES	*snipProps		= &params.SnippetProperties[0];
        snipProps->Enable					= config.enableRejectionTests && config.enableRecognition ? SNIPPET_ENABLE_FOR_DECISION : SNIPPET_ENABLE;
        snipProps->Front					= !config.reversedDocument;
        snipProps->Properties.Xposition		= 0;
        snipProps->Properties.Yposition		= 0;
        snipProps->Properties.Width			= 0;
        snipProps->Properties.Height		= 0;
        snipProps->Properties.Orientation	= SNIPPET_ORIENTATION_NORMAL;
        snipProps->Properties.Color			= config.enableColor ? SNIPPET_COLOR_COLOR : SNIPPET_COLOR_GREY_LEVEL;
    }
    // Rear side.
    if (config.sides & SC::E::DocumentSideRear) {
        SNIPPET_PROPERTIES	*snipProps		= &params.SnippetProperties[1];
        snipProps->Enable					= config.enableRejectionTests && config.enableRecognition ? SNIPPET_ENABLE_FOR_DECISION : SNIPPET_ENABLE;
        snipProps->Front					= config.reversedDocument;
        snipProps->Properties.Xposition		= 0;
        snipProps->Properties.Yposition		= 0;
        snipProps->Properties.Width			= 0;
        snipProps->Properties.Height		= 0;
        snipProps->Properties.Orientation	= SNIPPET_ORIENTATION_NORMAL;
        snipProps->Properties.Color			= config.enableColor ? SNIPPET_COLOR_COLOR : SNIPPET_COLOR_GREY_LEVEL;
    }

    // Proprietary OCR if requested.
    if (config.enableProprietaryRecognition) {
        SNIPPET_PROPERTIES	*snipProps		= &params.SnippetProperties[2];
        snipProps->Enable					= config.enableRejectionTests && config.enableRecognition ? SNIPPET_ENABLE_FOR_DECISION : SNIPPET_ENABLE;
        snipProps->Front					= !config.reversedDocument;
        snipProps->Properties.Xposition		= 14;
        snipProps->Properties.Yposition		= 0;
        snipProps->Properties.Width			= 105;
        snipProps->Properties.Height		= 12;
        snipProps->Properties.Millimeters	= true;
        snipProps->Properties.Orientation	= SNIPPET_ORIENTATION_NORMAL;
        snipProps->Properties.Color			= SNIPPET_COLOR_BLACK_WHITE;
    }

    // Tweak contrast a bit.
    fnSetImageAdjustment(_hDevice, 0, 0, TRUE);

    // Try to change parameters.
    if (!fnSetDeviceParameters(_hDevice, params)) {
        DWORD	error;
        fnGetDeviceError(_hDevice, &error);
        Crt				<< "Failed to configure the device with error code" << error << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode	(SC::E::ErrorCodeDeviceBadConfig);
        return			false;
    }

    // Enable double-feeding detection.
    if ((deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionX ||
            deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionS) &&
            !fnSetSorterParameter(_hDevice, 0, config.enableDoubleFeedingDetection)) {
        Crt				<< "Failed to enable double-feeding detection, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode	(SC::E::ErrorCodeDeviceBadConfig);
        return			false;
        // Double feeding sensor power (Default = 50). Recommended: 10 (Thomas BOWCUT).
        if (!fnSetSorterParameter(_hDevice, 2, 70)) {
            Crt				<< "Failed to configure the double-feeding sensor power, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode	(SC::E::ErrorCodeDeviceBadConfig);
            return			false;
        }
        // Set confidence level of double feeding sensor to 10 (Thomas BOWCUT).
        if (!fnSetSorterParameter(_hDevice, 3, 10)) {
            Crt				<< "Failed to configure the double-feeding sensor delay, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode	(SC::E::ErrorCodeDeviceBadConfig);
            return			false;
        }
    }

    // Try to switch to online mode.
    if (!fnOnLine(_hDevice)) {
        Crt				<< "Failed to set the device to the online state, setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode	(SC::E::ErrorCodeDeviceNotFound);
        return			false;
    }
    // All right!
    return				waitForDeviceState(SC::E::DeviceStateUp, 5);
}
// Start scanning.
bool SC::PaniniController::doStartProcessing () {
    _shouldStop						= false;

    // No oppenned device.
    if (!_hDevice) {
        Crt							<< "Can't start processing because no device was yet handled, setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode				(SC::E::ErrorCodeDeviceNotFound);
        return						false;
    }
    // Try to put the device online.
    else if (!fnOnLine(_hDevice)) {
        Crt							<< "Failed to set the device to the online state, setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode				(SC::E::ErrorCodeDeviceNotFound);
        return						false;
    }
    // Wait for the device to be up for maximum 5s.
    else if (!waitForDeviceState(SC::E::DeviceStateUp, 5)) {
        Crt							<< "Failed to set the device to the up state, setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        setErrorCode				(SC::E::ErrorCodeDeviceNotFound);
        return						false;
    }
    // Try to start feeding..
    else if (!fnStartFeeding(_hDevice)) {
        DWORD				error;
        fnGetDeviceError			(_hDevice, &error);
        Crt							<< "Failed to start feeding with code" << error << ", setting error code to" << SC::E::ErrorCodeStartScanFailure << ".";
        setErrorCode				(SC::E::ErrorCodeStartScanFailure);
        return						false;
    }
    // All right!
    return							true;
}
void SC::PaniniController::doStopProcessing () {
    Dbg						<< "Stopping processing...";
    if (_hDevice && fnStopFeeding(_hDevice))
        waitForDeviceState	(SC::E::DeviceStateUp, 5);
}
// Main processing thread method.
void SC::PaniniController::run () {
    _hWnd					= CreateWindowEx(WS_EX_CLIENTEDGE, L"PaniniListener", 0, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 0, 0, NULL, NULL, GetModuleHandle(0), NULL);
    SetWindowLong			(_hWnd, WIN_HACK_GWLP_USERDATA, (long)this);
    _waitCondition.wakeAll	();
    exec					();
}
void SC::PaniniController::purgeDocumentsQueue (bool propagate, quint32 docIdLimit) {
    QMutableMapIterator<quint32, RE::T::Document*>
                    it			(_docsQueue);
    while (it.hasNext()) {
        it.next					();
        if (it.key()<=docIdLimit) {
            RE::T::Document *doc    = it.value();
            if (propagate)
                shouldRejectREDocument(doc);
            releaseREDocument	(doc);
            it.remove();
        }
    }
}

// Wait for thrad to stop OR device state to be equal to given state OR any error to occur.
bool SC::PaniniController::waitForDeviceState (SC::E::DeviceState state, unsigned long timeout) {
    time_t              end         = time(0)+timeout;
    SC::E::DeviceState  curState    = deviceState();
    bool                erroreous   = errorOccured();
    while (true) {
        if (!isRunning() || erroreous)
            break;
        else if (timeout!=ULONG_MAX && time(0)>end)
            break;
        else if (curState==state)
            break;
        msleep      (250);
        curState                    = deviceState();
        erroreous                   = errorOccured();
    }
    return curState==state && !erroreous;
}

void SC::PaniniController::onDeviceEvent (WPARAM wParam, LPARAM lParam) {
    Config const	&config				= configuration();

    // Handle device message.
    DWORD			propDocId			= (DWORD)lParam;
    switch (wParam) {
        // Device is connected or disconnected.
        case WMPAR_SORTER_DISCONNECTED:
            fnShutDown					(_hDevice);
            _hDevice					= 0;
            purgeDocumentsQueue         (true);
            finishedProcessing			();
            break;

        // Device connected.
        case WMPAR_SORTER_CONNECTED:
            break;

        // Device is switched to standby mode, wake it up.
        case WMPAR_STANDBY:
            fnOnLine					(_hDevice);
            break;

        // New document on track.
        case WMPAR_SORTER_NEW_DOCUMENT: {
            // Enqueue a new document info.
            incrementDocumentCount		();
            incrementDocumentUid		();
            _docsQueue[propDocId]		= createREDocument(documentUid());
            // Document count reached maxumum requested document count.
            if (documentCount()>=config.maximumDocumentCount) {
                _shouldStop				= true;
                fnStopFeeding			(_hDevice);
            }
            break;
        };

        // Snippets images ready.
        case WMPAR_SNIPPETS_READY: {
            // Retrieve Images pointer.
            ImagesStruct	*pImages	= (ImagesStruct*)lParam;
            if (!pImages)
                break;
            propDocId					= pImages->DocNumber;
            RE::T::Document	*doc		= _docsQueue[propDocId];
            // Front images requested and front images not yet retrieved and buffer is valid.
            if (config.sides & SC::E::DocumentSideFront && !doc->images.contains("Frint") && pImages->Images[0].BufferLen && pImages->Images[0].pBuffer) {
                BYTE	*buff			= pImages->Images[0].pBuffer;
                qint32	len				= pImages->Images[0].BufferLen;
                QImage	origImage		= QImage::fromData(buff, len);
                attachImageToREDocument	(doc, "Front", origImage);
            }
            // Rear images requested and front images not yet retrieved and buffer is valid.
            if (config.sides & SC::E::DocumentSideRear && !doc->images.contains("Rear") && pImages->Images[1].BufferLen && pImages->Images[1].pBuffer) {
                BYTE	*buff			= pImages->Images[1].pBuffer;
                qint32	len				= pImages->Images[1].BufferLen;
                QImage	origImage		= QImage::fromData(buff, len);
                attachImageToREDocument	(doc, "Rear", origImage);
            }
            // Do proprietary OCR recognition if requested.
            if (config.enableProprietaryRecognition && pImages->Images[2].BufferLen && pImages->Images[2].pBuffer) {
                char	szOcr[512];
                /* PQR TODO
                if(fnGetOCRCodeline(_hDevice, pImages->Images[2].pBuffer, szOcr, 512, OCR1_OCRBALNUM, 55))
                    attachStringToREDocument(doc, "OCR2", szOcr);
                else
                    Dbg << "Proprietary OCR failed with error code" << fnVApiGetError() << ".";
                */
            }
            fnFreeImagesBuffer(pImages);
            break;
        };

        // MICR line available.
        case WMPAR_SORTER_MICR_AVAILABLE: {
            if (!config.enableMicr)
                break;
            char	szMICRCodeline[255];
            if (fnGetMicrCodeline(_hDevice, szMICRCodeline, 254) && strlen(szMICRCodeline))
                attachStringToREDocument(_docsQueue[propDocId], "General", "MICR", szMICRCodeline);
            break;
        };

        // Sort document to destination pocket.
        case WMPAR_SORTER_SET_ITEM_OUTPUT: {
            // Get document size.
            DWORD			width		= 0;
            RE::T::Document	*doc		= _docsQueue[propDocId];
            if (fnGetDocumentLength(_hDevice, propDocId, &width))
                setREDocumentSize		(doc, width, 0);
            // Determine wether the document has to be rejected or not.
            bool			reject		= shouldRejectREDocument(doc);
            // If the device is a VS / VX...
            if (deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionS ||
                    deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionX) {
                // There is image to print.
                char    *strData    = config.printData && config.printData[0] ? config.printData : 0;
                char    *imgData    = config.printImageFilename && config.printImageFilename[0] ? config.printImageFilename : 0;
                if ((strData && strData[0]) || (imgData && imgData[0]))
                    fnSendPrinterData   (_hDevice, 0, _font, strData, config.printOffset, imgData, imgData ? (0 << 16) | config.printImageOffset : 0, imgData ? PRT_SRC_FILE : 0);
            }
            // Pocket selection.
            BYTE			pocket		= reject ? 2 : 1;
            if (!fnSetPocket(_hDevice, propDocId, pocket)) {
                DWORD	error;
                fnGetDeviceError		(_hDevice, &error);
                Crt						<< "Failed to set the destination pocket with code" << error << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
                setErrorCode			(SC::E::ErrorCodeDeviceBadConfig);
                _shouldStop				= true;
            }
            break;
        }

        // Downstream options, for franking.
        case WMPAR_SET_DOWNSTREAM_OPTIONS: {
             RE::T::Document	*doc		= _docsQueue[propDocId];
            // Device is a IDeal / wIDeal...
            if (doc->validity==RE::E::DocumentValidityValid &&
                    (deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniIDeal || deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniWIDeal)) {
                // There is text to print.
                if ((config.printData && config.printData[0]) || (config.printImageFilename && config.printImageFilename[0])) {
                    Dbg					<< "Sending franking order to device.";
                    // Try to stamp the document.
                    if (!fnEnableFranking(_hDevice, true, 0)) {
                        DWORD                       error;
                        fnGetDeviceError            (_hDevice, &error);
                        Crt                         << "Failed to stamp the document with code" << error << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
                        setErrorCode                (SC::E::ErrorCodeDeviceBadConfig);
                        _shouldStop                 = true;
                    }
                }
            }
            break;
       }

        // Images are ready.
        case WMPAR_IMAGES_READY: {
            if (lParam)
                fnFreeImagesBuffer                  ((ImagesStruct*)lParam);
            break;
        }

        // Document completed.
        case WMPAR_DOC_COMPLETED: {
            // Remove any document with id<propDocId from the queue.
            purgeDocumentsQueue                     (false, propDocId);
            // If the queue is empty and the device is scheduled for a stop, signal the end of processing.
            if (_docsQueue.isEmpty() && _shouldStop)
                finishedProcessing                  ();
            break;
        }

        // Exception.
        case WMPAR_SORTER_EXCEPTION: {
            // Before doing anything, we need to make sure the device is back to online.
            waitForDeviceState                      (SC::E::DeviceStateUp, 5);

            DWORD	errCode							= 0;
            fnGetDeviceError						(_hDevice, &errCode);

            // Not really an error. No more document to process.
            if (errCode==DEVICE_ERR_FEEDER_EMPTY || errCode==DEVICE_ERR_FULL_POCKET) {
                Dbg									<< "Finished feeding.";
                _shouldStop							= true;
                if (_docsQueue.isEmpty())
                    finishedProcessing();
                return;
            }

            // Device error.
            else if (errCode==DEVICE_ERR_SORTER_ERROR_PENDING) {
                // Wait for the device to be ready.
                if (!waitForDeviceState(SC::E::DeviceStateUp, 3)) {
                    Crt								<< "Can't recover from a device error, setting error code to" << SC::E::ErrorCodeDeviceJammed << ".";
                    setErrorCode					(SC::E::ErrorCodeDeviceJammed);
                }
                else {
                    // Retrieve subcodes.
                    DWORD	sorterErrCode			= 0;
                    fnGetSorterError				(_hDevice, &sorterErrCode);
                    BYTE errClass					= (sorterErrCode & 0xff000000) >> 24;
                    BYTE errPeriph					= (sorterErrCode & 0x00ff0000) >> 16;
                    BYTE errSubcode					= (sorterErrCode & 0x0000ff00) >> 8;
                    Wrn                             << "A sorter error has occured with code" << errCode << ", class" << errClass << ", peripheral" << errPeriph << "and subcode" << errSubcode << ", trying to recover from it...";
                    if (deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionS || deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionX) {
                        waitForDeviceState(SC::E::DeviceStateUp, 5);
                        if (!fnFreeTrack(_hDevice, 2)) {
                            Crt                     << "Recovering from the previous error failed, setting error code to" << SC::E::ErrorCodeDeviceJammed << ".";
                            setErrorCode            (SC::E::ErrorCodeDeviceJammed);
                        }
                    }
                    else {
                        waitForDeviceState          (SC::E::DeviceStateUp, 5);
                        if (!fnStartFeeding(_hDevice)) {
                            Crt                     << "The device was not able to start feeding again. Setting error code to" << SC::E::ErrorCodeDeviceJammed << ".";
                            setErrorCode            (SC::E::ErrorCodeDeviceJammed);
                        }
                    }
                }
            }

            // Any other error: stop everything.
            else {
                Crt									<< "An unknown error has occured with code" << errCode << ", setting error code to" << SC::E::ErrorCodeUnknownError << ".";
                setErrorCode						(SC::E::ErrorCodeUnknownError);
            }

            // Request stop.
            _shouldStop                             = true;
            if (deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionS || deviceConfiguration().deviceId==SC::E::DeviceIdentifierPaniniVisionX) {
                waitForDeviceState(SC::E::DeviceStateUp);
                if (!fnFreeTrack(_hDevice, 2)) {
                    Crt                             << "Recovering from the previous error failed, setting error code to" << SC::E::ErrorCodeDeviceJammed << ".";
                    setErrorCode                    (SC::E::ErrorCodeDeviceJammed);
                }
            }
            purgeDocumentsQueue                     (true);
            finishedProcessing                      ();
            break;
        }
        default:
            Dbg                                     << "Unhandled device event:" << wParam << ".";
            break;
    }
}

LRESULT CALLBACK SC::PaniniController::staticListenerCallback (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    SC::PaniniController *_this = (SC::PaniniController*)GetWindowLong(hWnd, WIN_HACK_GWLP_USERDATA);
    switch(msg) {
        case WM_APP:
            if (_this)
                _this->onDeviceEvent(wParam, lParam);
            break;
        default: {
            return DefWindowProc(hWnd, msg, wParam, lParam);
        }
    }
    return 0;
}

#endif
