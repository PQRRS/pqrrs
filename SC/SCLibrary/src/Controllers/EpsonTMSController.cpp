#include "EpsonTMSController.h"

#ifdef Q_OS_WIN
#include "../../ThirdParty/win32/include/Epson/TMS2000/EpsStmApiInterface.h"
#include <QtCore/QDebug>

// Singleton instance.
SC::EpsonTMSController*			SC::EpsonTMSController::_instance	= 0;
// Library handler.
HMODULE								SC::EpsonTMSController::_hLib		= 0;

// Library functions.
DLL_BiOpenMonPrinter				BiOpenMonPrinter						= 0;
DLL_BiCloseMonPrinter				BiCloseMonPrinter						= 0;
DLL_BiESCNEnable					BiESCNEnable							= 0;
DLL_BiSetStatusBackFunction			BiSetStatusBackFunction					= 0;
DLL_BiCancelStatusBack				BiCancelStatusBack						= 0;
DLL_BiSCNMICRSetStatusBackFunction	BiSCNMICRSetStatusBackFunction			= 0;
DLL_BiSCNMICRCancelStatusBack		BiSCNMICRCancelStatusBack				= 0;
DLL_BiSCNMICRCancelFunction			BiSCNMICRCancelFunction					= 0;
DLL_BiESCNGetDocumentSize			BiESCNGetDocumentSize					= 0;
DLL_BiGetMicrText					BiGetMicrText							= 0;
DLL_BiSCNSelectScanFace				BiSCNSelectScanFace						= 0;
DLL_BiSCNSetImageQuality			BiSCNSetImageQuality					= 0;
DLL_BiSCNSetImageFormat				BiSCNSetImageFormat						= 0;
DLL_BiGetOcrABText					BiGetOcrABText							= 0;
DLL_BiGetScanImage					BiGetScanImage							= 0;
DLL_BiSCNMICRFunctionContinuously	BiSCNMICRFunctionContinuously			= 0;
DLL_BiSCNMICRFunctionPostPrint		BiSCNMICRFunctionPostPrint				= 0;
DLL_BiSetNumberOfDocuments			BiSetNumberOfDocuments					= 0;

SC::EpsonTMSController::EpsonTMSController (LM::Core *lm, const DeviceConfig &config, QObject *parent)
: AScannerController(lm, config, parent), _hDevice(0) {
    // First-time init.
    if (!_instance)
        _instance	= this;
}
SC::EpsonTMSController::~EpsonTMSController () {
    // End everything.
    if (_hDevice && BiSCNMICRCancelFunction) {
        BiSCNMICRCancelFunction     (_hDevice, MF_EJECT_RELEASE);
        BiSCNMICRCancelStatusBack   (_hDevice);
        BiCancelStatusBack          (_hDevice);
        BiCloseMonPrinter           (_hDevice);
        while (isRunning()) {
            exit                    ();
            terminate               ();
            msleep                  (50);
        }
        _hDevice                    = 0;
        _instance                   = 0;
    }
}

QList<quint32>& SC::EpsonTMSController::availableResolutions () const {
    static QList<quint32>	resolutions;
    if (resolutions.isEmpty())
        resolutions << 100 << 120 << 200;
    return resolutions;
}
quint32 SC::EpsonTMSController::deviceMaximumDocumentCount () const {
    return 100;
}
qint32 SC::EpsonTMSController::devicePrintMaxLength () const {
    return -1;
}
bool SC::EpsonTMSController::deviceCanPrintsMultiline	() const {
    return false;
}

bool SC::EpsonTMSController::doInitialize () {
    // Load library.
    if (!_hLib) {
        _hLib		= LoadLibraryA("EpsStmApiWrapper.dll");
        if ((int)_hLib<32) {
            Crt << "Error loading EpsStmApiWrapper.dll, setting error code to" << SC::E::ErrorCodeLibraryLoadingFailure << "(Lib Handle:" << _hLib << ", Failure code:" << GetLastError() << ").";
            _hLib                   = 0;
            setErrorCode(SC::E::ErrorCodeLibraryLoadingFailure);
            return false;
        }

        // Get pointers to library functions.
        BiOpenMonPrinter					= (DLL_BiOpenMonPrinter)				GetProcAddress(_hLib, "BiOpenMonPrinter");
        BiCloseMonPrinter					= (DLL_BiCloseMonPrinter)				GetProcAddress(_hLib, "BiCloseMonPrinter");
        BiESCNEnable						= (DLL_BiESCNEnable)					GetProcAddress(_hLib, "BiESCNEnable");
        BiSetStatusBackFunction				= (DLL_BiSetStatusBackFunction)			GetProcAddress(_hLib, "BiSetStatusBackFunction");
        BiCancelStatusBack					= (DLL_BiCancelStatusBack)				GetProcAddress(_hLib, "BiCancelStatusBack");
        BiSCNMICRSetStatusBackFunction		= (DLL_BiSCNMICRSetStatusBackFunction)	GetProcAddress(_hLib, "BiSCNMICRSetStatusBackFunction");
        BiSCNMICRCancelStatusBack			= (DLL_BiSCNMICRCancelStatusBack)		GetProcAddress(_hLib, "BiSCNMICRCancelStatusBack");
        BiSCNMICRCancelFunction				= (DLL_BiSCNMICRCancelFunction)			GetProcAddress(_hLib, "BiSCNMICRCancelFunction");
        BiESCNGetDocumentSize				= (DLL_BiESCNGetDocumentSize)			GetProcAddress(_hLib, "BiESCNGetDocumentSize");
        BiGetMicrText						= (DLL_BiGetMicrText)					GetProcAddress(_hLib, "BiGetMicrText");
        BiSCNSelectScanFace					= (DLL_BiSCNSelectScanFace)				GetProcAddress(_hLib, "BiSCNSelectScanFace");
        BiSCNSetImageQuality				= (DLL_BiSCNSetImageQuality)			GetProcAddress(_hLib, "BiSCNSetImageQuality");
        BiSCNSetImageFormat					= (DLL_BiSCNSetImageFormat)				GetProcAddress(_hLib, "BiSCNSetImageFormat");
        BiGetOcrABText						= (DLL_BiGetOcrABText)					GetProcAddress(_hLib, "BiGetOcrABText");
        BiGetScanImage						= (DLL_BiGetScanImage)					GetProcAddress(_hLib, "BiGetScanImage");
        BiSCNMICRFunctionContinuously		= (DLL_BiSCNMICRFunctionContinuously)	GetProcAddress(_hLib, "BiSCNMICRFunctionContinuously");
        BiSCNMICRFunctionPostPrint			= (DLL_BiSCNMICRFunctionPostPrint)		GetProcAddress(_hLib, "BiSCNMICRFunctionPostPrint");
        BiSetNumberOfDocuments				= (DLL_BiSetNumberOfDocuments)			GetProcAddress(_hLib, "BiSetNumberOfDocuments");
        // Check pointers.
        if (!BiOpenMonPrinter || !BiCloseMonPrinter || !BiESCNEnable ||
            !BiSetStatusBackFunction || !BiCancelStatusBack ||
            !BiSCNMICRSetStatusBackFunction || !BiSCNMICRCancelStatusBack ||
            !BiSCNMICRCancelFunction || !BiESCNGetDocumentSize || !BiGetMicrText || !BiSCNSelectScanFace ||
            !BiSCNSetImageQuality || !BiSCNSetImageFormat || !BiGetOcrABText || !BiGetScanImage ||
            !BiSCNMICRFunctionContinuously || !BiSCNMICRFunctionPostPrint ||
            !BiSetNumberOfDocuments) {
                Crt                 << "Error retrieving a callback from EpsStmApiWrapper.dll, setting error code to" << SC::E::ErrorCodeLibraryFunctionFailure << ".";
                setErrorCode        (SC::E::ErrorCodeLibraryFunctionFailure);
                FreeLibrary         (_hLib);
                _hLib               = 0;
                return false;
        }
    }

    // Prepare required temp vars.
    qint32	retCode	= 0;

    // Prepare image saving method.
    if ((retCode=BiESCNEnable(CROP_STORE_MEMORY))!=SUCCESS) {
        Crt << "Failed to set image storage method to memory, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }

    // Open a handle to the device.
    _hDevice	= BiOpenMonPrinter(TYPE_PRINTER, (CHAR*)"TM-S1000U");
    if (_hDevice<SUCCESS) {
        Crt << "Failed to retrieve a handle to the TM-S1000U device, setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        _hDevice	= 0;
        setErrorCode(SC::E::ErrorCodeDeviceNotFound);
        return false;
    }

    // Set up MICR / Status callbacks.
    if ((retCode=BiSCNMICRSetStatusBackFunction(_hDevice, staticScanStatusCallback)!=SUCCESS)) {
        Crt << "Failed to configure asynchroneous function with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }
    // All right.
    return true;
}
bool SC::EpsonTMSController::doDeviceSerial (char*) {
    return false;
}
bool SC::EpsonTMSController::doConfigure (const Config&) {
    // Prepare return code.
    quint32 retCode;

    // Set up base configuration.
    baseConf.iSize										= sizeof(MF_BASE01);
    baseConf.iVersion									= MF_BASE_VERSION01;
    if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &baseConf, MF_GET_BASE_DEFAULT))!=SUCCESS) {
        Crt << "Failed to set device with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }
    baseConf.bBuzzerHz		[MF_BUZZER_TYPE_SUCCESS]	= MF_BUZZER_HZ_440;
    baseConf.bBuzzerCount	[MF_BUZZER_TYPE_SUCCESS]	= MF_BUZZER_DISABLE;
    baseConf.bBuzzerHz		[MF_BUZZER_TYPE_ERROR]		= MF_BUZZER_HZ_880;
    baseConf.bBuzzerCount	[MF_BUZZER_TYPE_ERROR]		= MF_BUZZER_DISABLE;
    baseConf.bBuzzerHz		[MF_BUZZER_TYPE_WFEED]		= MF_BUZZER_HZ_4000;
    baseConf.bBuzzerCount	[MF_BUZZER_TYPE_WFEED]		= MF_BUZZER_DISABLE;
    if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &baseConf, MF_SET_BASE_PARAM))!=SUCCESS) {
        Crt << "Failed to set buzzer with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }

    // Set up front configuration.
    scanFrontConf.iSize		= sizeof(MF_SCAN);
    scanFrontConf.iVersion	= MF_SCAN_VERSION;
    if (configuration().sides & SC::E::DocumentSideFront) {
        if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &scanFrontConf, MF_GET_SCAN_DEFAULT))!=SUCCESS) {
            Crt << "Failed to get default front config with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
            return false;
        }
        scanFrontConf.sResolution							= closestAvailableResolution();
        if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &scanFrontConf, MF_SET_SCAN_FRONT_PARAM))!=SUCCESS) {
            Crt << "Failed to set front config with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
            return false;
        }
    }
    else
        BiSCNMICRFunctionContinuously(_hDevice, 0, MF_CLEAR_SCAN_FRONT_PARAM);

    // Set up rear configuration.
    scanRearConf.iSize		= sizeof(MF_SCAN);
    scanRearConf.iVersion	= MF_SCAN_VERSION;
    if (configuration().sides & SC::E::DocumentSideRear) {
        if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &scanRearConf, MF_GET_SCAN_DEFAULT))!=SUCCESS) {
            Crt << "Failed to get default rear config with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
            return false;
        }
        scanRearConf.sResolution							= closestAvailableResolution();
        if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &scanRearConf, MF_SET_SCAN_BACK_PARAM))!=SUCCESS) {
            Crt << "Failed to set rear config with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
            return false;
        }
    }
    else
        BiSCNMICRFunctionContinuously(_hDevice, NULL, MF_CLEAR_SCAN_BACK_PARAM);

    // MICR configuration.
    micrConf.iSize		= sizeof(MF_MICR);
    micrConf.iVersion	= MF_MICR_VERSION;
    if (configuration().enableMicr) {
        if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &micrConf, MF_GET_MICR_DEFAULT))!=SUCCESS) {
            Crt << "Failed to get default MICR config with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
            return false;
        }
        micrConf.bFont				= MF_MICR_FONT_CMC7;
        micrConf.bMicOcrSelect		= MF_MICR_USE_MICR;
        micrConf.blParsing			= FALSE;
        if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &micrConf, MF_SET_MICR_PARAM))!=SUCCESS) {
            Crt << "Failed to set MICR config with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
            return false;
        }
    }
    else
        BiSCNMICRFunctionContinuously(_hDevice, NULL, MF_CLEAR_MICR_PARAM);

    // OCR configuration #1, used for TR types.
    ocrConf1.iSize				= sizeof(MF_OCR_AB);
    ocrConf1.iVersion			= MF_OCR_AB_VERSION;
    ocrConf1.bOcrType			= MF_OCR_FONT_OCRB_ALPHANUM;
    ocrConf1.bDirection			= MF_OCR_LEFTRIGHT;
    // Originally: 9,61 - 114,OCR_AREA_BOTTOM.
    ocrConf1.wStartX			= 5;
    ocrConf1.wStartY			= 55;
    ocrConf1.wEndX				= OCR_AREA_RIGHT;
    ocrConf1.wEndY				= OCR_AREA_BOTTOM;
    ocrConf1.bSpaceHandling		= OCR_SPACE_DISABLE;

    // OCR configuration #2, used for Cadhoc type.
    ocrConf2.iSize				= sizeof(MF_OCR_AB);
    ocrConf2.iVersion			= MF_OCR_AB_VERSION;
    ocrConf2.bOcrType			= MF_OCR_FONT_OCRA_ALPHANUM;
    ocrConf2.bDirection			= MF_OCR_LEFTRIGHT;
    ocrConf2.wStartX			= 20;
    ocrConf2.wStartY			= 55;
    ocrConf2.wEndX				= 105;
    ocrConf2.wEndY				= 60;
    ocrConf2.bSpaceHandling		= OCR_SPACE_DISABLE;

    // Set up processment configuration.
    memset(&procConf, 0, sizeof(MF_PROCESS));
    procConf.iSize = sizeof(MF_PROCESS);
    procConf.iVersion									= MF_PROCESS_VERSION;
    if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &procConf, MF_GET_PROCESS_DEFAULT))!=SUCCESS) {
        Crt << "Failed to get default processing config with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }
    bool bPrintEnable	= configuration().printData && configuration().printData[0];
    if (bPrintEnable)
        Wrn << "Print is:" << bPrintEnable << "and print data is" << configuration().printData << "of length" << strlen(configuration().printData) << ".";
    procConf.bActivationMode							= MF_ACTIVATE_MODE_HIGH_SPEED;
    procConf.bPaperType									= MF_PAPER_TYPE_CHECK;
    procConf.dwStartWaitTime							= 300;
    procConf.bSuccessStamp								= bPrintEnable ? MF_STAMP_ENABLE : MF_STAMP_DISABLE;
    // Paper misinsertion.
    procConf.bPaperMisInsertionErrorSelect				= MF_ERROR_SELECT_NODETECT;
    procConf.bPaperMisInsertionErrorEject				= MF_EJECT_NOEJECT;
    procConf.bPaperMisInsertionStamp					= MF_STAMP_DISABLE;
    procConf.bPaperMisInsertionCancel					= MF_CANCEL_DISABLE;
    // Noise detection.
    procConf.bNoiseErrorSelect							= MF_ERROR_SELECT_NODETECT;
    procConf.bNoiseErrorEject							= MF_EJECT_NOEJECT;
    procConf.bNoiseStamp								= MF_STAMP_DISABLE;
    procConf.bNoiseCancel								= MF_CANCEL_DISABLE;
    // Bad data.
    procConf.bBaddataErrorSelect						= MF_ERROR_SELECT_NODETECT;
    procConf.bBaddataErrorEject							= MF_EJECT_NOEJECT;
    procConf.bBaddataStamp								= MF_STAMP_DISABLE;
    procConf.bBaddataCancel								= MF_CANCEL_DISABLE;
    // No data.
    procConf.bNodataErrorSelect							= MF_ERROR_SELECT_NODETECT;
    procConf.bNodataErrorEject							= MF_EJECT_NOEJECT;
    procConf.bNodataStamp								= MF_STAMP_DISABLE;
    procConf.bNodataCancel								= MF_CANCEL_DISABLE;
    procConf.bNearFullSelect							= MF_NEARFULL_PERMIT;

    // Double-feeding detection enabled.
    if (configuration().enableDoubleFeedingDetection) {
        procConf.bDoubleFeedErrorSelect					= MF_ERROR_SELECT_DETECT;
        procConf.bDoubleFeedErrorEject					= MF_EJECT_SUB_POCKET;
        procConf.bDoubleFeedStamp						= MF_STAMP_DISABLE;
        procConf.bDoubleFeedCancel						= MF_CANCEL_DISABLE;
    }
    // Double-feeding detection disabled.
    else {
        procConf.bDoubleFeedErrorSelect					= MF_ERROR_SELECT_NODETECT;
        procConf.bDoubleFeedErrorEject					= MF_EJECT_MAIN_POCKET;
        procConf.bDoubleFeedStamp						= MF_STAMP_DISABLE;
        procConf.bDoubleFeedCancel						= MF_CANCEL_DISABLE;
    }

    if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, &procConf, MF_SET_PROCESS_PARAM))!=SUCCESS) {
        Crt << "Failed to set processing config with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }
    // Success.
    return true;
}
// Start scanning.
bool SC::EpsonTMSController::doStartProcessing () {
    if (!isRunning())
        start();
    _docsQueue.clear            ();
    // Prepare some variables.
    bool    toRet               = true;
    qint32	retCode             = 0;
    // Try	to set up the pages count.
    if ((retCode=BiSetNumberOfDocuments(_hDevice, configuration().maximumDocumentCount))!=SUCCESS) {
        Crt                     << "Failed to configure maximum document count with code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode            (SC::E::ErrorCodeDeviceBadConfig);
        toRet                   = false;
    }
    // Try to feed a document.
    else if ((retCode=BiSCNMICRFunctionContinuously(_hDevice, 0, MF_EXEC))!=SUCCESS) {
        Crt                     << "Failed to configure continuous function with code" << retCode << ", setting error code to" << SC::E::ErrorCodeStartScanFailure << ".";
        setErrorCode            (SC::E::ErrorCodeStartScanFailure);
        toRet                   = false;
    }
    if (!toRet) {
        exit                    ();
        finishedProcessing      ();
    }
    return                      toRet;
}
void SC::EpsonTMSController::doStopProcessing () {
}

// Event handlers.
quint32 SC::EpsonTMSController::scanStatusCallback (DWORD propDocId, WORD mainStatus, WORD subStatus, LPSTR) {
    Dbg << propDocId << mainStatus << subStatus;
    // Bad document ID.
    if (propDocId<=0) {
        Crt										<< "Callback without a valid document ID" << propDocId << ", setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode							(SC::E::ErrorCodeDeviceBadConfig);
        finishedProcessing                      ();
    }
    // New document.
    else if (mainStatus==MF_CHECKPAPER_PROCESS_START) {
        incrementDocumentCount					();
        incrementDocumentUid					();
        ProcessedDocumentInfo	newDoc;
        _doubleFeeding							= false;
        newDoc.documentId						= documentUid();
        newDoc.sides							= SC::E::DocumentSideNone;
        _docsQueue[propDocId]					= newDoc;
        createREDocument						(newDoc.documentId);
    }
    // Data ready.
    else if (mainStatus==MF_DATARECEIVE_DONE) {
        // Double feeding detected, nothing to do.
        if (_doubleFeeding)
            return -1;

        // Get document size.
        WORD	width, height;
        if (SUCCESS==BiESCNGetDocumentSize(_hDevice, &width, &height))
            setREDocumentSize					(_docsQueue[propDocId].documentId, width/10, height/10);

        // MICR enabled.
        if (configuration().enableMicr) {
            // Retrieve MICR.
            micrConf.szMicrStr[0]				= 0;
            qint32	retCode						= BiGetMicrText(_hDevice, propDocId, &micrConf);
            if (retCode==SUCCESS && micrConf.szMicrStr && strlen(micrConf.szMicrStr))
                attachStringToREDocument		(_docsQueue[propDocId].documentId, "General", "MICR", micrConf.szMicrStr);
        }

        // Depending on requested sides, process scans.
        if (configuration().sides & SC::E::DocumentSideFront) {
            processScanFace(propDocId, SC::E::DocumentSideFront);
            // OCR.
            if (configuration().enableProprietaryRecognition && strlen(micrConf.szMicrStr)<14) {
                qint32	retCode;
                // Regular OCR (TR type).
                retCode	= BiGetOcrABText(_hDevice, propDocId, OCR_SOURCE_TRANSACTION_NUMBER, "", &ocrConf1);
                /* PQR TODO.
                if (retCode==SUCCESS && strlen(ocrConf1.szOcrStr)>=14)
                    attachStringToREDocument	(_docsQueue[propDocId].documentId, "OCR2", ocrConf1.szOcrStr);
                // 2nd OCR (Cadhoc type).
                else {
                    retCode						= BiGetOcrABText(_hDevice, propDocId, OCR_SOURCE_TRANSACTION_NUMBER, "", &ocrConf2);
                    if (retCode==SUCCESS)		attachStringToREDocument(_docsQueue[propDocId].documentId, "OCR2", ocrConf1.szOcrStr);
                }
                */
            }
        }
        if (configuration().sides & SC::E::DocumentSideRear)
            processScanFace(propDocId, SC::E::DocumentSideRear);
    }
    // End document.
    else if (mainStatus==MF_CHECKPAPER_PROCESS_DONE) {
        QMutableMapIterator<DWORD, ProcessedDocumentInfo>
                                            it	(_docsQueue);
        while (it.hasNext()) {
            it.next();
            if (it.key()<=propDocId) {
                if (it.key()==propDocId && !_doubleFeeding)
                    shouldRejectREDocument		(_docsQueue[propDocId].documentId);
                releaseREDocument				((quint32)it.value().documentId);
                it.remove						();
            }
        }
    }

    // All done.
    else if (mainStatus==MF_FUNCTION_DONE)
        finishedProcessing                      ();

    // Error occured.
    else if (mainStatus==MF_ERROR_OCCURED) {
        if (subStatus & ERR_PAPER_PILED) {
            Dbg									<< "Double-feeding detected.";
            _doubleFeeding						= true;
            return								-1;
        }
        else if (subStatus & ERR_PAPER_JAM) {
            Crt									<< "Device jammed, setting error code to" << SC::E::ErrorCodeDeviceJammed << ".";
            setErrorCode						(SC::E::ErrorCodeDeviceJammed);
        }
        else {
            Crt									<< "Unknown error with code" << mainStatus << "and subcode" << subStatus << ", setting error code to" << SC::E::ErrorCodeUnknownError << ".";
            setErrorCode						(SC::E::ErrorCodeUnknownError);
        }
        finishedProcessing                      ();
        return									-1;
    }

    // All done!
    return										0;
}

qint32 SC::EpsonTMSController::deviceStatusCallback (DWORD) {
    return 0;
}

bool SC::EpsonTMSController::processScanFace (DWORD propDocId, SC::E::DocumentSide side) {
    // Select scan face.
    BYTE	face	= (side & SC::E::DocumentSideFront)?MF_SCAN_FACE_FRONT:MF_SCAN_FACE_BACK;
    qint32	retCode;
    if ((retCode=BiSCNSelectScanFace(_hDevice, face))==SUCCESS) {
        // Set image quality.
        if ((retCode=BiSCNSetImageQuality(_hDevice, EPS_BI_SCN_8BIT, 0 , EPS_BI_SCN_MONOCHROME, EPS_BI_SCN_SHARP))==SUCCESS) {
            // Set image format.
            if ((retCode=BiSCNSetImageFormat(_hDevice, EPS_BI_SCN_BITMAP))==SUCCESS) {
                MF_SCAN		scanData;
                memset(&scanData, 0, sizeof(MF_SCAN));
                // Get image.
                if ((retCode=BiGetScanImage(_hDevice, propDocId, &scanData))==SUCCESS && scanData.lpbScanData) {
                    QImage	origImage	= QImage::fromData(scanData.lpbScanData, scanData.dwScanSize);
                    attachImageToREDocument(_docsQueue[propDocId].documentId, side==SC::E::DocumentSideFront?"Front":"Rear", origImage);
                    _docsQueue[propDocId].sides	= (SC::E::DocumentSide)((int)_docsQueue[propDocId].sides+(int)side);
                }
                // Failed.
                else {
                    Crt << "Failed to process a document face with code" << retCode << ".";
                    return false;
                }
                GlobalFree(scanData.lpbScanData);
            }
            // Failed to set image format.
            else {
                Crt << "Failed to set image format with code" << retCode << ".";
                return false;
            }
        }
        // Failed to set image quality.
        else {
            Crt << "Failed to set image quality with code" << retCode << ".";
            return false;
        }
    }
    // Failed to select scan face.
    else {
        Crt << "Failed to select scan face with code" << retCode << ".";
        return false;
    }

    // All done.
    return true;
}

// Static callbacks.
int CALLBACK SC::EpsonTMSController::staticScanStatusCallback (DWORD propDocId, WORD mainStatus, WORD subStatus, LPSTR portName) {
    if (!_instance)
        return 0;
    return _instance->scanStatusCallback(propDocId, mainStatus, subStatus, portName);
}

#endif
