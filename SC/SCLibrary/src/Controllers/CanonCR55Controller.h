#ifndef SC_CanonCR55Controller_h
#define SC_CanonCR55Controller_h

#include <QtCore/QtGlobal>
#ifdef Q_OS_WIN
// Base class.
#include "AScannerController.h"
// Required stuff.
#include <QtCore/QList>
#ifdef Q_OS_WIN
#   include <windows.h>
#endif

namespace RE {
    namespace T {
        struct Document;
    }
}

namespace SC {

class SCLibOpt CanonCR55Controller : public AScannerController {
Q_OBJECT

public:
    // Constructor / Destructor.
                                    CanonCR55Controller			(LM::Core *lm, const DeviceConfig &devConfig, QObject *parent=0);
    virtual							~CanonCR55Controller		();

    QList<quint32>&					availableResolutions		() const;
    quint32							deviceMaximumDocumentCount	() const;
    qint32							devicePrintMaxLength		() const;
    bool							deviceCanPrintsMultiline	() const;
    bool							doInitialize				();
    bool                            doDeviceSerial              (char *buff);
    bool							doConfigure					(const Config &config);
    bool							doStartProcessing			();
    void							doStopProcessing			();

protected:
    void							run							();
    int								processDocumentSide			(RE::T::Document *doc, SC::E::DocumentSide sides);

private:
    // Library handler.
    static HMODULE                    _hLib;
};

}

#else
#include "DummyController.h"
namespace SC {
class SCLibOpt CanonCR55Controller : public DummyController {
Q_OBJECT
public:
    // Constructor / Destructor.
    /**/                            CanonCR55Controller        (LM::Core *lm, const DeviceConfig &devConfig, QObject *parent=0) : DummyController(lm, devConfig, parent) {}
};
}
#endif

#endif
