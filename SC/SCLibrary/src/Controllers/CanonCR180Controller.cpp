#include "CanonCR180Controller.h"

#ifdef Q_OS_WIN
#include "../../ThirdParty/win32/include/Canon/CsdScan_CR55_CR180.h"
#include <QtCore/QDebug>

// Library handlers.
HMODULE				SC::CanonCR180Controller::_hLib			= 0;
namespace _internalCR180Namespace {
    typedef INT32 (WINAPI* FNCSDPROBE)			();
    typedef INT32 (WINAPI* FNCSDGETDRIVERNAME)	(LPSTR lpszDriverName, LPSTR lpszVersion);
    typedef INT32 (WINAPI* FNCSDTERMINATE)		();
    typedef INT32 (WINAPI* FNCSDPARGET)			(UINT uiParam, LPVOID lpParam);
    typedef INT32 (WINAPI* FNCSDPARSET)			(UINT uiParam, LPARAM lParam);
    typedef INT32 (WINAPI* FNCSDSTARTSCAN)		(LPCSTR lpFileName, LPVOID lpReserved1, LPVOID lpReserved2);
    typedef INT32 (WINAPI* FNCSDSTOPSCAN)		();
    typedef INT32 (WINAPI* FNCSDREADPAGE)		( LPCEIIMAGEINFO lpImage);
    typedef INT32 (WINAPI* FNCSDREADPAGESTART)	(LPCEIIMAGEINFO ceiimageinfo, LPFNSCANCALLBACK lpfnScanComplete);
    typedef INT32 (WINAPI* FNCSDDRVDIALOG)		(HINSTANCE hInstance, HWND hWnd, INT32 nDlgID);
    typedef INT32 (WINAPI* FNCSDLOADIMAGE)		(LPCEIIMAGEINFO lpImage, LPCSTR szFileName, quint32 nPage, LPARAM lFileType);
    typedef INT32 (WINAPI* FNCSDSAVEIMAGEEX)	(LPCEIIMAGEINFO lpImage, LPCEIIMAGEFILEINFO lpiInfo, LPCSTR szFileName);
    typedef INT32 (WINAPI* FNCSDRELEASEIMAGE)	(LPCEIIMAGEINFO lpImage);
    typedef INT32 (WINAPI* FNCSDFORMATSTRING)	(INT32 nCode, LPSTR lpString, LPLONG lMaxLen);
    typedef INT32 (WINAPI* FNCSDABORTSCAN)		();
    FNCSDPROBE				fnCsdProbe				= 0;
    FNCSDGETDRIVERNAME		fnCsdGetDriverName		= 0;
    FNCSDTERMINATE			fnCsdTerminate			= 0;
    FNCSDPARGET				fnCsdParGet				= 0;
    FNCSDPARSET				fnCsdParSet				= 0;
    FNCSDSTARTSCAN			fnCsdStartScan			= 0;
    FNCSDSTOPSCAN			fnCsdStopScan			= 0;
    FNCSDREADPAGE			fnCsdReadPage			= 0;
    FNCSDREADPAGESTART		fnCsdReadPageStart		= 0;
    FNCSDDRVDIALOG			fnCsdDrvDialog			= 0;
    FNCSDLOADIMAGE			fnCsdLoadImage			= 0;
    FNCSDSAVEIMAGEEX		fnCsdSaveImageEx		= 0;
    FNCSDRELEASEIMAGE		fnCsdReleaseImage		= 0;
    FNCSDFORMATSTRING		fnCsdFormatString		= 0;
    FNCSDABORTSCAN			fnCsdAbortScan			= 0;
}

using namespace SC;
using namespace _internalCR180Namespace;

CanonCR180Controller::CanonCR180Controller (LM::Core *lm, const DeviceConfig &config, QObject *parent)
: AScannerController(lm, config, parent) {
}

CanonCR180Controller::~CanonCR180Controller () {
    // Cancel everything.
    fnCsdStopScan   ();
    fnCsdAbortScan  ();
    fnCsdTerminate  ();
}

QList<unsigned int>& CanonCR180Controller::availableResolutions () const {
    static QList<unsigned int>	resolutions;
    if (resolutions.isEmpty())
        resolutions << 100 << 200 << 300;
    return resolutions;
}
unsigned int CanonCR180Controller::deviceMaximumDocumentCount () const {
    return 200;
}
int CanonCR180Controller::devicePrintMaxLength () const {
    return 32;
}
bool CanonCR180Controller::deviceCanPrintsMultiline	() const {
    return false;
}

bool CanonCR180Controller::doInitialize () {
    // Load library.
    if (!_hLib) {
        _hLib		= LoadLibraryA("CanonCr.dll");
        if ((int)_hLib<32) {
            Crt << "Error loading CanonCr.dll, setting error code to" << SC::E::ErrorCodeLibraryLoadingFailure << "(Lib Handle:" << _hLib << ", Failure code:" << GetLastError() << ").";
            _hLib                   = 0;
            setErrorCode(SC::E::ErrorCodeLibraryLoadingFailure);
            return false;
        }

        // Get pointers to library functions.
        fnCsdProbe              = (FNCSDPROBE)			GetProcAddress(_hLib, "CsdProbe");
        fnCsdTerminate			= (FNCSDTERMINATE)		GetProcAddress(_hLib, "CsdTerminate");
        fnCsdParSet             = (FNCSDPARSET)			GetProcAddress(_hLib, "CsdParSet");
        fnCsdParGet             = (FNCSDPARGET)			GetProcAddress(_hLib, "CsdParGet");
        fnCsdStartScan			= (FNCSDSTARTSCAN)		GetProcAddress(_hLib, "CsdStartScan");
        fnCsdStopScan			= (FNCSDSTOPSCAN)		GetProcAddress(_hLib, "CsdStopScan");
        fnCsdReadPage			= (FNCSDREADPAGE)		GetProcAddress(_hLib, "CsdReadPage");
        fnCsdLoadImage			= (FNCSDLOADIMAGE)		GetProcAddress(_hLib, "CsdLoadImage");
        fnCsdSaveImageEx		= (FNCSDSAVEIMAGEEX)	GetProcAddress(_hLib, "CsdSaveImageEx");
        fnCsdReleaseImage		= (FNCSDRELEASEIMAGE)	GetProcAddress(_hLib, "CsdReleaseImage");
        fnCsdFormatString		= (FNCSDFORMATSTRING)	GetProcAddress(_hLib, "CsdFormatString");
        fnCsdDrvDialog			= (FNCSDDRVDIALOG)		GetProcAddress(_hLib, "CsdDrvDialog");
        fnCsdAbortScan			= (FNCSDABORTSCAN)		GetProcAddress(_hLib, "CsdAbortScan");
        fnCsdGetDriverName		= (FNCSDGETDRIVERNAME)	GetProcAddress(_hLib, "CsdGetDriverName");
        fnCsdReadPageStart		= (FNCSDREADPAGESTART)	GetProcAddress(_hLib, "CsdReadPageStart");
        if (!fnCsdProbe || !fnCsdTerminate || !fnCsdParSet || !fnCsdParGet ||
                !fnCsdStartScan || !fnCsdStopScan || !fnCsdReadPage || !fnCsdLoadImage ||
                !fnCsdSaveImageEx || !fnCsdReleaseImage || !fnCsdFormatString || !fnCsdDrvDialog ||
                !fnCsdAbortScan || !fnCsdGetDriverName || !fnCsdReadPageStart) {
            Crt                 << "Error retrieving a callback from CanonCr.dll, setting error code to" << SC::E::ErrorCodeLibraryFunctionFailure << ".";
            setErrorCode        (SC::E::ErrorCodeLibraryFunctionFailure);
            FreeLibrary         (_hLib);
            _hLib               = 0;
            return false;
        }
    }

    // Cancel everything.
    fnCsdStopScan();
    fnCsdAbortScan();
    fnCsdTerminate();

    INT32	retCode	= fnCsdProbe();
    // Initialize device.
    if (retCode!=CSD_OK) {
        Crt << "Failed to probe for a Canon CR180 with error code" << retCode << ", setting error code to" << SC::E::ErrorCodeDeviceNotFound << ".";
        fnCsdTerminate();
        setErrorCode(SC::E::ErrorCodeDeviceNotFound);
        return false;
    }

    // Restore previous params if available.
    fnCsdParSet(CSDP_LOAD_CONFIG_FILE, (LPARAM)"CrScan.ini");

    // Success :)
    return true;
}
bool CanonCR180Controller::doDeviceSerial (char*) {
    return false;
}
bool CanonCR180Controller::doConfigure (const Config&) {
    unsigned int	res	= closestAvailableResolution();
    Config const	&config	= configuration();

    // Try to set parameters.
    if (fnCsdParSet(CSDP_BRIGHTNESS, 148)!=CSD_OK ||
        fnCsdParSet(CSDP_EDGEEMPHASIS, 3)!=CSD_OK ||
        fnCsdParSet(CSDP_XRESOLUTION, res)!=CSD_OK ||
        fnCsdParSet(CSDP_YRESOLUTION, res)!=CSD_OK ||
        fnCsdParSet(CSDP_MODE, config.enableColor?CSD_BINARY:CSD_GRAYSCALE)!=CSD_OK) {
        fnCsdTerminate();
        Crt << "Failed to pass first configuration step, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }

    // Prepare sides to be scanned.
    LPARAM	sides	= CSD_FEEDER_SIMPLEX;
    if ((!config.reversedDocument && config.sides & SC::E::DocumentSideRear) || (config.reversedDocument && config.sides & SC::E::DocumentSideFront))
        sides		= CSD_FEEDER_DUPLEX;

    if (fnCsdParSet(CSDP_FEEDER, sides)!=CSD_OK ||
        fnCsdParSet(CSDP_AUTOSIZE, true)!=CSD_OK) {
        fnCsdTerminate();
        Crt << "Failed to pass second configuration step, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }

    // Set up MICR.
    if (fnCsdParSet(CSDP_MICR, config.enableMicr)!=CSD_OK ||
        fnCsdParSet(CSDP_MICR_FONT, CSD_DETECT_CMC7)!=CSD_OK) {
        fnCsdTerminate();
        Crt << "Failed to pass third configuration step, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }

    // Set up OCR.
    if (config.enableProprietaryRecognition &&
        (fnCsdParSet(CSDP_OCR, true)!=CSD_OK ||
        fnCsdParSet(CSDP_OCR_FONT, CSD_DETECT_OCRB)!=CSD_OK ||
        fnCsdParSet(CSDP_OCR_XPOS, 15)!=CSD_OK ||
        fnCsdParSet(CSDP_OCR_YPOS, 59)!=CSD_OK ||
        fnCsdParSet(CSDP_OCR_WIDTH, 98)!=CSD_OK ||
        fnCsdParSet(CSDP_OCR_LENGTH, 12)!=CSD_OK)) {
        fnCsdTerminate();
        Crt << "Failed to pass fourth configuration step, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }

    // Enable thickness (Double-feeding) detection (FORCE NOT!!!).
    //if (fnCsdParSet(CSDP_DBLFEEDTHICK, config.enableDoubleFeedingDetection)!=CSD_OK) {
    if (fnCsdParSet(CSDP_DBLFEEDTHICK, false)!=CSD_OK) {
        fnCsdTerminate();
        Crt << "Failed to configure double-feeding detection, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }

    // Enable / Disable printing.
    bool bPrintEnable	= config.printData && config.printData[0];
    if (bPrintEnable)
        Wrn << "Print is:" << bPrintEnable << "and print data is" << config.printData << "of length" << strlen(config.printData) << ".";
    if (fnCsdParSet(CSDP_IMPRINTER, bPrintEnable)!=CSD_OK) {
        fnCsdTerminate();
        Crt << "Failed to configure printing, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }
    // Max length should be 32 chars.
    if (bPrintEnable) {
        if (fnCsdParSet(CSDP_IMPSTRING, (LPARAM)config.printData)!=CSD_OK ||
            fnCsdParSet(CSDP_IMPCHARFONT, CSD_IMPFONT_SMALL)!=CSD_OK ||
            fnCsdParSet(CSDP_IMPYPOS, 30)!=CSD_OK) {
            fnCsdTerminate();
            Crt << "Failed to pass last configuration step, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
            setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
            return false;
        }
    }
    return true;
}
// Start scanning.
bool CanonCR180Controller::doStartProcessing () {
    Config const	&config	= configuration();

    // Set up max documents.
    if (fnCsdParSet(CSDP_MAXDOCUMENT, config.maximumDocumentCount)!=CSD_OK) {
        Crt << "Failed to configure maximum document count, setting error code to" << SC::E::ErrorCodeDeviceBadConfig << ".";
        setErrorCode(SC::E::ErrorCodeDeviceBadConfig);
        return false;
    }

    // Start scan.
    if (fnCsdStartScan(0, 0, 0)!=CSD_OK) {
        Crt << "Failed to start scan, setting error code to" << SC::E::ErrorCodeStartScanFailure << ".";
        setErrorCode(SC::E::ErrorCodeStartScanFailure);
        return false;
    }
    return true;
}
void CanonCR180Controller::doStopProcessing () {
}
void SC::CanonCR180Controller::run () {
    Config const	&config	= configuration();
    // Prepare variables.
    unsigned long			micrLength, ocrLength;
    INT32					retCode;
    QImage					origImage;
    // While there are documents to process...
    while (true) {
        Dbg << "Looping for new document...";
        // Increment documenst ID.
        _doubleFeeding						= false;
        // Assign unique ID.
        incrementDocumentCount				();
        incrementDocumentUid				();
        quint32			uid					= documentUid();
        RE::T::Document	*doc				= createREDocument(uid);

        // Requested front, but failed.
        if ((!config.reversedDocument && config.sides & SC::E::DocumentSideFront) || (config.reversedDocument && config.sides & SC::E::DocumentSideRear)) {
            retCode							= processDocumentSide(doc, config.reversedDocument ? SC::E::DocumentSideRear : SC::E::DocumentSideFront);
            if (retCode!=CSD_OK) {
                releaseREDocument			(doc);
                if (retCode==CSD_DOUBLEFEED) {
                    Wrn						<< "Double-feeding occured while scanning real front side.";
                    fnCsdParSet				(CSDP_CONVEY_DOCUMENT, CSD_SORTPOCKET1);
                    if (documentCount()>=config.maximumDocumentCount)
                        break;
                    else {
                        fnCsdStartScan		(0, 0, 0);
                        continue;
                    }
                }
                else if (retCode==CSD_JAM) {
                    Wrn						<< "Double-feeding occured while scanning real front side, setting error code to" << SC::E::ErrorCodeDeviceJammed << ".";
                    setErrorCode			(SC::E::ErrorCodeDeviceJammed);
                }
                else if (retCode==CSD_UNKNOWN) {
                    Wrn						<< "Unknown error while scanning real front side, trying to restart scanning...";
                    fnCsdStartScan			(0, 0, 0);
                    continue;
                }
                else if (retCode!=CSD_NOPAGE && retCode!=CSD_NOPAPER) {
                    Wrn						<< "No paper error while scanning real front side.";
                    setErrorCode			(SC::E::ErrorCodeNoPaper);
                }
                else
                    Crt						<< "Unknown error while scanning real front side with code" << retCode << ".";
                break;
            }
        }

        // Try to get MICR length.
        if (config.enableMicr) {
            micrLength						= 0;
            retCode							= fnCsdParGet(CSDP_MICRDATALEN, &micrLength);
            if (retCode!=CSD_OK) {
                Crt							<< "Error while trying to read MICR with code" << retCode << ", setting error code to" << SC::E::ErrorCodeMicrReadingFailure << ".";
                setErrorCode				(SC::E::ErrorCodeMicrReadingFailure);
                releaseREDocument			(doc);
                break;
            }
            // MICR available.
            if (micrLength>0) {
                char	*szMicr				= new char[micrLength+1];
                // Can't get MICR.
                retCode						= fnCsdParGet(CSDP_MICRDATA, szMicr);
                if (retCode!=CSD_OK) {
                    delete [] szMicr;
                    Wrn						<< "Error while getting MICR with code" << retCode << ", setting error code to" << SC::E::ErrorCodeMicrReadingFailure << ".";
                    setErrorCode			(SC::E::ErrorCodeMicrReadingFailure);
                    releaseREDocument		(doc);
                    break;
                }
                // Write MICR file.
                attachStringToREDocument	(doc, "General", "MICR", szMicr);
                delete[]					szMicr;
            }

            // Read OCR.
            if (config.enableProprietaryRecognition && micrLength<14) {
                ocrLength	= 0;
                retCode	= fnCsdParGet(CSDP_OCRDATALEN, &ocrLength);
                if (retCode!=CSD_OK) {
                    Wrn						<< "Error while trying to read OCR with code" << retCode << ", setting error code to" << SC::E::ErrorCodeOcrReadingFailure << ".";
                    setErrorCode			(SC::E::ErrorCodeOcrReadingFailure);
                    releaseREDocument		(doc);
                    break;
                }
                // OCR available.
                if (ocrLength>0) {
                    char	*szOcr	= new char[ocrLength+1];
                    // Can't get OCR.
                    retCode	= fnCsdParGet(CSDP_OCRDATA, szOcr);
                    if (retCode!=CSD_OK) {
                        delete [] szOcr;
                        Wrn << "Error while getting OCR with code" << retCode << ", setting error code to" << SC::E::ErrorCodeOcrReadingFailure << ".";
                        setErrorCode(SC::E::ErrorCodeOcrReadingFailure);
                        releaseREDocument(doc);
                        break;
                    }
                    // Write OCR to file.
                    // PQR TODO attachStringToREDocument(doc, "OCR2", szOcr);
                }
            }
        }

        // Requested rear, but failed.
        if ((!config.reversedDocument && config.sides & SC::E::DocumentSideRear) || (config.reversedDocument && config.sides & SC::E::DocumentSideFront)) {
            retCode	= processDocumentSide(doc, config.reversedDocument ? SC::E::DocumentSideFront : SC::E::DocumentSideRear);
            if (retCode!=CSD_OK) {
                releaseREDocument(doc);
                if (retCode==CSD_DOUBLEFEED) {
                    Wrn << "Double-feeding occured while scanning real rear side.";
                    fnCsdParSet(CSDP_CONVEY_DOCUMENT, CSD_SORTPOCKET1);
                    if (documentCount()>=config.maximumDocumentCount)
                        break;
                    else {
                        fnCsdStartScan(0, 0, 0);
                        continue;
                    }
                }
                else if (retCode==CSD_JAM) {
                    Wrn << "Double-feeding occured while scanning real rear side, setting error code to" << SC::E::ErrorCodeDeviceJammed << ".";
                    setErrorCode(SC::E::ErrorCodeDeviceJammed);
                }
                else if (retCode==CSD_UNKNOWN) {
                    Wrn << "Unknown error while scanning real rear side, trying to restart scanning...";
                    fnCsdStartScan(0, 0, 0);
                    continue;
                }
                else if (retCode!=CSD_NOPAGE && retCode!=CSD_NOPAPER) {
                    Wrn << "No paper error while scanning real rear side, setting error code to" << SC::E::ErrorCodeNoPaper << ".";
                    setErrorCode(SC::E::ErrorCodeNoPaper);
                }
                else
                    Crt << "Unknown error while scanning real rear side with code" << retCode << ".";
                break;
            }
        }
        // Document complete. Send it to callback.
        shouldRejectREDocument		(doc);
        releaseREDocument			(doc);
        // Maximum document count reached?
        if (documentCount()>config.maximumDocumentCount)
            break;
    }
    fnCsdStopScan();
    finishedProcessing();
}

int CanonCR180Controller::processDocumentSide (RE::T::Document *doc, SC::E::DocumentSide side) {
    // Prepare variables.
    CEIIMAGEINFO2	imgData;
    // Prepare image to be retrieved.
    memset(&imgData, 0, sizeof(CEIIMAGEINFO2));
    imgData.cbSize			= sizeof(CEIIMAGEINFO2);
    imgData.nFileType		= CSD_BMP_FILE;
    imgData.nCompType		= CSD_COMP_NONE;
    // Try to read a page.
    INT32			retCode	= fnCsdReadPage((CEIIMAGEINFO*)&imgData);
    if (retCode!=CSD_OK)
        return retCode;
    // Prepare image...
    QImage	origImage		= QImage::fromData(imgData.lpImage, imgData.tImageSize, "BMP");
    attachImageToREDocument(doc, side==SC::E::DocumentSideFront?"Front":"Rear", origImage);
    // Release original image.
    fnCsdReleaseImage((CEIIMAGEINFO*)&imgData);
    // Everything went ok.
    return CSD_OK;
}

#endif
