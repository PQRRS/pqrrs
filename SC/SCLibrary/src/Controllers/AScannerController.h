#ifndef SC_AScannerController_h
#define SC_AScannerController_h

#include "../SCTypes.h"
#include <QtCore/QThread>
#include <QtGui/QImage>
#include <QtCore/QList>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>
class QFile;

// Forward declarations.
namespace LM {
    class Core;
}
namespace RE {
    namespace T {
        struct Document;
    }
    class Engine;
}
namespace SC {
    class QueuedDocumentProcessor;
}

namespace SC {

// Scanner controller base class.
class SCLibOpt AScannerController : public QThread {
Q_OBJECT

protected:
    // Constructor is hidden.
    /**/							AScannerController              (LM::Core *lm, DeviceConfig const &devConfig, QObject *parent=0);
public:
    // Destructor and factory instanciator.
    /**/							~AScannerController             ();
    static AScannerController*      instanciate                     (LM::Core *lm, DeviceConfig const &devConfig);
    // Main methods.
    bool							initialize                      ();
    bool                            deviceSerial                    (char *buff);
    bool							configure                       (Config const &config);
    bool							startProcessing                 ();
    bool                            startBlockingProcessing         ();
    void							stopProcessing                  ();
    void							finishedProcessing              ();
    void							yieldFinishedWhenIdle           ();
    // Recognition engine getter / setter.
    RE::Engine*						recognitionEngine               ();
    void							setRecognitionEngine            (RE::Engine *re);
    // Recognition document getters.
    bool                            attachedREDocumentKeyAndStringSize (quint32 id, quint32 offset, quint32 &keySize, quint32 &valSize);
    bool                            attachedREDocumentKeyAndString  (quint32 id, quint32 offset, char *key, char *val) const;

protected:
    quint32							closestAvailableResolution      () const;
    RE::T::Document*				createREDocument                (quint32 id);
    RE::T::Document*				retrieveREDocument              (quint32 id);
    void							setREDocumentSize               (quint32 id, quint32 width, quint32 height);
    void							setREDocumentSize               (RE::T::Document *doc, quint32 width, quint32 height);
    void							attachStringToREDocument        (quint32 id, QString const &categoryId, QString const &stringId, const QVariant &string);
    void							attachStringToREDocument        (RE::T::Document *doc, QString const &categoryId, QString const &stringId, const QVariant &string);
    void							attachImageToREDocument         (quint32 id, QString const &imageId, const QImage &image);
    void							attachImageToREDocument         (RE::T::Document *doc, QString const &imageId, const QImage &image);
    bool							shouldRejectREDocument          (quint32 id);
    bool							shouldRejectREDocument          (RE::T::Document *doc);
    void							releaseREDocument               (quint32 id);
    void							releaseREDocument               (RE::T::Document *doc);
    // Unicity / Stolen check...
    void							readStolenDatabase              ();
    void							readUnicityDatabase             ();
    // TCP device status server...
    void							sendDeviceStatus                ();

    // Message handler for QDebug.
    static void						staticMessageHandler            (QtMsgType t, const char *m);

protected slots:
    void							qdp_documentProcessed           (void *doc);

// All those have to be reimplemented.
public:
    virtual bool					doInitialize                    ()									= 0;
    virtual bool                    doDeviceSerial                  (char *buff)                        = 0;
    virtual bool					doConfigure                     (Config const &config)				= 0;
    virtual bool					doStartProcessing               ()									= 0;
    virtual void					doStopProcessing                ()									= 0;
    virtual QList<quint32>&			availableResolutions            ()							const	= 0;
    virtual quint32					deviceMaximumDocumentCount      ()							const	= 0;
    virtual qint32					devicePrintMaxLength            ()							const	= 0;
    virtual bool					deviceCanPrintsMultiline        ()							const	= 0;

private:
    // Licensing.
    LM::Core                        *_lm;
    bool                            _licensed;
    QString                         _hostUuid;
    quint8                          *_obfCheesecake;
    // Recognition engine.
    RE::Engine						*_re;
    bool							_selfCreatedRE;
    // Logging objects.
    static QFile					*_logFile;
    // Configuration structures.
    SC::DeviceConfig				_deviceConfig;
    Config							_config;
    // Internal flag for error checking.
    SC::E::ErrorCode				_lastError;
    // Counters.
    bool                            _finishYeilded;
    quint32							_docUid;
    quint32							_docCount;
    // Document processor.
    QueuedDocumentProcessor			*_docsProcessor;
    // Documents list for recognition.
    QHash<quint32, RE::T::Document*>_recognitionDocs;
    mutable QMutex                  _processingMutex;
    mutable QWaitCondition          _processingWaitCondition;
    mutable QMutex					_docsQueueMutex;
    // Stolen / Unicity control list.
    QStringList						_stolenList;
    QStringList						_documentHistory;
    // Global rejection flag.
    bool							_rejectAll;
    // Accepted CS families.
    qint32							_acceptedCsFamilies;

// Public inline accessors.
public:
    inline bool						errorOccured				() const				{
        return _lastError!=SC::E::ErrorCodeNoError;
    }
    inline SC::E::ErrorCode			errorCode					(bool reset)			{
        if (!reset)
            return _lastError;
        SC::E::ErrorCode toRet = _lastError;
        _lastError = SC::E::ErrorCodeNoError;
        return toRet;
    }
    inline void						setErrorCode				(SC::E::ErrorCode code)	{
        _lastError = code;
    }

// Protected inline accessors.
protected:
    inline const DeviceConfig&		deviceConfiguration			() const				{ return _deviceConfig; }
    inline const Config&			configuration				() const				{ return _config; }
    inline quint32					documentUid					() const				{ return _docUid; }
    inline void						incrementDocumentUid		()						{ _docUid++; }
    inline quint32					documentCount				() const				{ return _docCount; }
    inline void						incrementDocumentCount		()						{ _docCount++; }
};

};

#endif
