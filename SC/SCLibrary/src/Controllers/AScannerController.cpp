#include "AScannerController.h"
#include "../SCTypes.h"
#include "../QueuedDocumentProcessor.h"
// Required stuff.
#include <LMLibrary/LMTypes>
#include <LMLibrary/Core>
#include <RELibrary/Engine>
// Controllers... for instanciation. Ugly shit to be solved.
#include "DummyController.h"
#include "CanonCR55Controller.h"
#include "CanonCR180Controller.h"
#include "CanonCR190Controller.h"
#include "EpsonTMSController.h"
#include "PaniniController.h"
// Qt Stuff.
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>
#include <QtCore/QTextCodec>
#include <QtCore/QTextCodec>
#include <QtCore/QTranslator>
#include <QtCore/QTextStream>
#include <QtCore/QMutexLocker>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QCoreApplication>
#include <QtGui/QImage>
#include <QtGui/QMessageBox>

QFile* SC::AScannerController::_logFile (0);

SC::AScannerController::AScannerController (LM::Core *lm, const DeviceConfig &devConfig, QObject *parent)
: QThread(parent),
_lm(lm), _obfCheesecake(0),
_re(0), _selfCreatedRE(false),
_lastError(SC::E::ErrorCodeNoError), _finishYeilded(false), _docUid(0), _docCount(0), _docsProcessor(0) {
    SC::StaticInitializer();

    // Copy passed config to local one.
    _deviceConfig								= devConfig;
    // Make log file.
    if (_deviceConfig.enableLogging) {
        if (!_logFile)
            _logFile							= new QFile(QString("%1/%2.log").arg(_deviceConfig.dataPath).arg(QDateTime::currentDateTime().toString("yyyyMMdd-hhmmss")));
        // For debugging only...
        #define TypeSimpleRegistration(name)	qRegisterMetaType<name>(#name);
        TypeSimpleRegistration					(QtMsgType);
        #undef TypeSimpleRegistration
        if (_deviceConfig.enableLogging && _deviceConfig.dataPath && _deviceConfig.dataPath[0]) {
            qInstallMsgHandler					(SC::AScannerController::staticMessageHandler);
            Dbg									<< "Logging messages handler registered.";
        }
    }
    Dbg											<< "Constructing with version" << SC::Version() << "...";

    // Set up licensing.
    _obfCheesecake                              = new quint8[16];
    _hostUuid                                   = LM::Core::hostUuid();
    _licensed                                   = _lm->isVerified() && LM::Core::LMObfuscatedCheesecakeForData(_lm, SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);

    // Make document processor thread.
    _docsProcessor								= new QueuedDocumentProcessor(0);
    connect										(_docsProcessor, SIGNAL(documentProcessed(void*)), this, SLOT(qdp_documentProcessed(void*)), Qt::QueuedConnection);
}
SC::AScannerController::~AScannerController () {
    Dbg                                 << "Destructing...";
    // Make sure documents processor has finished any pending jobs.
    if (_docsProcessor) {
        while (_docsProcessor->hasPendingDocuments())
            msleep						(100);
        while (_docsProcessor->isRunning()) {
            _docsProcessor->exit		();
            _docsProcessor->terminate   ();
            msleep						(50);
        }
        disconnect						(_docsProcessor);
        delete							_docsProcessor;
        _docsProcessor                  = 0;
    }
    // Mark current controller thread as stopped.
    while (isRunning()) {
        exit							();
        terminate                       ();
        msleep							(50);
    }
    if (_selfCreatedRE)
        delete							_re;
    delete[]                            _obfCheesecake;
}
SC::AScannerController* SC::AScannerController::instanciate (LM::Core *lm, SC::DeviceConfig const &devConfig) {
    // Initialize everything.
    SC::StaticInitializer                   ();
    // Load translator based on locale.
    QString                     rLoc        = devConfig.locale;
    QLocale                     loc         = !rLoc.isEmpty() && rLoc!="Auto" ? QLocale(rLoc) : QLocale::system();
    static QTranslator			trans;
    { // Memguard.
        // Install translator.
        if (trans.load(loc, ":/Translations/SCLibrary.qm")) {
            qApp->removeTranslator          (&trans);
            qApp->installTranslator         (&trans);
        }
    }
    // Finally instanciate the controller.
    AScannerController  *ret    = 0;
    if (devConfig.deviceId==SC::E::DeviceIdentifierCanonCR55)
        ret     = new CanonCR55Controller   (lm, devConfig);
    else if (devConfig.deviceId==SC::E::DeviceIdentifierCanonCR180)
        ret     = new CanonCR180Controller  (lm, devConfig);
    else if (devConfig.deviceId==SC::E::DeviceIdentifierCanonCR190)
        ret     = new CanonCR190Controller  (lm, devConfig);
    else if (devConfig.deviceId==SC::E::DeviceIdentifierEpsonTMS1000 ||
             devConfig.deviceId==SC::E::DeviceIdentifierEpsonTMS2000)
        ret     = new EpsonTMSController    (lm, devConfig);
    else if (devConfig.deviceId==SC::E::DeviceIdentifierPaniniVisionS ||
             devConfig.deviceId==SC::E::DeviceIdentifierPaniniVisionX ||
             devConfig.deviceId==SC::E::DeviceIdentifierPaniniIDeal ||
             devConfig.deviceId==SC::E::DeviceIdentifierPaniniWIDeal)
        ret     = new PaniniController      (lm, devConfig);
    else if (devConfig.deviceId==SC::E::DeviceIdentifierDummy)
        ret     = new DummyController       (lm, devConfig);
    else
        return 0;
    // Licensing problem...
    if (!ret->_licensed) {
        QMessageBox::critical(0, QObject::tr("Licensing Issue"), ret->_lm->lastMessage(), QMessageBox::Ok);
        delete ret;
        return 0;
    }
    // Finally return controller.
    return ret;
}


void SC::AScannerController::staticMessageHandler (QtMsgType t, const char *m) {
    if (_logFile) {
        if (!_logFile->isOpen())
            _logFile->open			(QIODevice::WriteOnly);
        // Prepare color and icon.
        if (t==QtDebugMsg)
            _logFile->write				(QString("Dbg: %2\r\n").arg(m).toUtf8().constData());
        else if (t==QtWarningMsg)
            _logFile->write				(QString("Wrn: %2\r\n").arg(m).toUtf8().constData());
        else if (t==QtCriticalMsg)
            _logFile->write				(QString("Crt: %2\r\n").arg(m).toUtf8().constData());
        else if (t==QtFatalMsg) {
            _logFile->write				(QString("Ftl: %2\r\n").arg(m).toUtf8().constData());
            _logFile->flush				();
            abort                       ();
        }
    }
    _logFile->flush				();
}

bool SC::AScannerController::initialize () {
    // Set up licensing.
    _obfCheesecake                              = new quint8[16];
    _hostUuid                                   = LM::Core::hostUuid();
    if (_lm->isVerified())
        LM::Core::LMObfuscatedCheesecakeForData (_lm, SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    DeclareCheesecake                           (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    _lastError                                  = (SC::E::ErrorCode)(SC::E::ErrorCodeNoError + CKZ07);
    return doInitialize();
}

bool SC::AScannerController::deviceSerial (char *buff) {
    DeclareCheesecake                       (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    _lastError                              = (SC::E::ErrorCode)(SC::E::ErrorCodeNoError + CKZ07);
    return doDeviceSerial(buff);
}

bool SC::AScannerController::configure (const Config &config) {
    _lastError                          = SC::E::ErrorCodeNoError;
    // Copy configuration.
    _config								= config;
    // Copy print data.
    if (config.printData && config.printData[0]) {
        Dbg								<< "Setting up franking...";
        // Replace any carrier return by a space.
        if (!deviceCanPrintsMultiline()) {
            Dbg << "Configuring multi-line...";
            for (quint32 isz=0; isz<strlen(_config.printData); isz++)
                if (_config.printData[isz]=='\r' || _config.printData[isz]=='\n') {
                    _config.printData[isz]	= 0;
                    break;
                }
        }
        Dbg								<< "Print data is now" << strlen(config.printData) << "characters wide.";
    }

    // Cap the max doc count.
    _config.maximumDocumentCount		= qMin(_config.maximumDocumentCount, deviceMaximumDocumentCount());
    Dbg									<< "Maximum document count is now" << _config.maximumDocumentCount << ".";

    // Load client binary.
    if (_config.enableRecognition && _config.recognitionDataPath && recognitionEngine())
        _re->loadClientBinaryData		(_config.recognitionDataPath ? _config.recognitionDataPath : "");

    if (!doConfigure(_config)) {
        finishedProcessing              ();
        return                          false;
    }
    return true;
}

bool SC::AScannerController::startProcessing () {
    DeclareCheesecake           (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    _lastError                  = (SC::E::ErrorCode)(SC::E::ErrorCodeNoError + CKZ07);
    _finishYeilded              = false;
    // Callback...
    if (_config.onProcessingStarted)
        (*_config.onProcessingStarted)(_config.userObject);

    // Create directories.
    if (_deviceConfig.dataPath && _deviceConfig.dataPath[0]) {
        Dbg << "Populating filesystem tree...";
        QDir(QString("%1").arg(_deviceConfig.dataPath)).mkpath("Archived");
        QDir(QString("%1").arg(_deviceConfig.dataPath)).mkpath("Accepted");
        QDir(QString("%1").arg(_deviceConfig.dataPath)).mkpath("Controls");
        QDir(QString("%1").arg(_deviceConfig.dataPath)).mkpath("Groupped");
        QDir(QString("%1").arg(_deviceConfig.dataPath)).mkpath("Rejected");
    }
    else
        Dbg << "No data path provided, no file will be created.";

    // Load various dbs.
    _acceptedCsFamilies         = -1;
    if (_deviceConfig.dataPath && _deviceConfig.dataPath[0]) {
        QFile			db		(QString("%1/Controls/AcceptedCSFamilies.dat").arg(_deviceConfig.dataPath));
        if (db.exists() && db.open(QIODevice::ReadOnly)) {
            QTextStream		in		(&db);
            _acceptedCsFamilies		= in.readLine().toInt();
            db.close();
        }
    }
    // Reset document count.
    _docCount                   = 0;
    // If requested, reset document UID.
    if (_config.enableDocUidReset) {
        Dbg                     << "Reseting document UID...";
        _docUid                 = 0;
    }

    // Async processing?
    if (!_config.enableRejectionTests && !_docsProcessor->isRunning())
        _docsProcessor->start   ();

    // Implementation failed.
    if (!doStartProcessing())
        return false;
    return true;
}
bool SC::AScannerController::startBlockingProcessing () {
    _processingMutex.lockInline();
    if (!startProcessing())
        return false;
    _processingWaitCondition.wait(&_processingMutex);
    _processingMutex.unlockInline();
    return true;
}
void SC::AScannerController::stopProcessing () {
    _lastError                  = SC::E::ErrorCodeNoError;
    return doStopProcessing();
}
void SC::AScannerController::finishedProcessing () {
    if (_finishYeilded)
        return;
    _finishYeilded              = true;
    QtConcurrent::run           <void>(this, &SC::AScannerController::yieldFinishedWhenIdle);
}
void SC::AScannerController::yieldFinishedWhenIdle () {
    DeclareCheesecake           (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    quint32		isz				= CKZ07;
    while (true) {
        bool	allDone			= true;
        // Check for pending documents.
        if (!_docsQueueMutex.tryLock() || !_recognitionDocs.isEmpty()) {
            if (isz%50==0)		Dbg << "The document queue isn't empty.";
            allDone				= false;
        }
        _docsQueueMutex.unlock	();
        // Check for recognition thread.
        if (_docsProcessor->hasPendingDocuments()) {
            if (isz%100==0)		Dbg << "Document processor is still busy...";
            allDone				= false;
        }
        if (allDone)
            break;
        isz						++;
        msleep					(100);
    }
    Dbg							<< "Yielding finish callback!";
    if (_config.onProcessingFinished)
        (*_config.onProcessingFinished)(_config.userObject);
    _processingWaitCondition.wakeAll();
}
RE::Engine* SC::AScannerController::recognitionEngine () {
    // Instanciate recognition engine if none created.
    if (!_re) {
        Dbg                     << "A Recognition Engine is being instanciated...";
        setRecognitionEngine    (new RE::Engine(_lm, this));
        _selfCreatedRE          = true;
    }
    return _re;
}
void SC::AScannerController::setRecognitionEngine (RE::Engine *re) {
    _docsProcessor->setRecognitionEngine	(0);
    // A self-created engine exists.
    if (_re && _selfCreatedRE)
        delete								_re;
    DeclareCheesecake                       (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    _re										= re+CKZ07;
    // Set up the documents processor with this new engine.
    _docsProcessor->setRecognitionEngine	(_re+CKZ07);
}

bool SC::AScannerController::attachedREDocumentKeyAndStringSize (quint32 id, quint32 offset, quint32 &keySize, quint32 &valSize) {
    Q_UNUSED(id);
    Q_UNUSED(offset);
    Q_UNUSED(keySize);
    Q_UNUSED(valSize);
    /* PQR TODO
    RE::T::Document			*doc        = 0;
    { // Mutex guard.
        QMutexLocker		ml			(&_docsQueueMutex);
        // No such document in queue.
        if (!_recognitionDocs.contains(id)) {
            Wrn << "Requested Document doesn't exist!";
            return false;
        }
        DeclareCheesecake               (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
        doc								= _recognitionDocs[id]+CKZ07;
    }
    QStringList const		&keys		= doc->results.keys();
    if (offset>=(quint32)keys.count()) {
        Wrn << "Requested key offset out of bounds!";
        return false;
    }
    QString const           &key        = keys.at(offset);
    QString const           &val        = doc->strings[key];
    keySize                             = key.length()+1;
    valSize                             = val.length()+1;
    */
    return                              true;
}
bool SC::AScannerController::attachedREDocumentKeyAndString (quint32 id, quint32 offset, char *outKey, char *outVal) const {
    Q_UNUSED(id);
    Q_UNUSED(offset);
    Q_UNUSED(outKey);
    Q_UNUSED(outVal);
    /* PQR TODO.
    RE::T::Document			*doc        = 0;
    { // Mutex guard.
        QMutexLocker		ml			(&_docsQueueMutex);
        // No such document in queue.
        if (!_recognitionDocs.contains(id)) {
            Wrn << "Requested Document doesn't exist!";
            return false;
        }
        DeclareCheesecake               (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
        doc								= _recognitionDocs[id]+CKZ07;
    }
    QStringList const		&keys		= doc->strings.keys();
    if (offset>=(quint32)keys.count()) {
        Wrn << "Requested key offset out of bounds!";
        return false;
    }
    QString const           &key        = keys.at(offset);
    QString const           &val        = doc->strings[key];
    strcpy                              (outKey, key.toUtf8().constData());
    strcpy                              (outVal, val.toUtf8().constData());
    */
    return                              true;
}

quint32 SC::AScannerController::closestAvailableResolution () const {
    RE::T::UIntList::const_iterator i;
    for (i=availableResolutions().constBegin(); i!=availableResolutions().constEnd(); ++i)
        if ((*i)>=configuration().preferedResolution)
            return (*i);
    return 0;
}

// Recognition documents handling.
RE::T::Document* SC::AScannerController::createREDocument (quint32 id) {
    RE::T::Document		*doc;
    { // Mutex guard.
        QMutexLocker	ml		(&_docsQueueMutex);
        // No such document in queue.
        if (_recognitionDocs.contains(id)) {
            Wrn					<< "A document already exists for UID" << id << "!";
            return				0;
        }
        Dbg						<< "Doc " << id << "-> Creating...";
        DeclareCheesecake       (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
        doc						= new RE::T::Document(id)+CKZ07;
        doc->semaphore.acquire  ();
        _recognitionDocs[id]	= doc;
        return					doc;
    }
}
RE::T::Document* SC::AScannerController::retrieveREDocument (quint32 id) {
    { // Mutex guard.
        QMutexLocker	ml		(&_docsQueueMutex);
        // No such document in queue.
        if (_recognitionDocs.contains(id)) {
            Wrn						<< "Requested Document doesn't exist!";
            return					0;
        }
        return						_recognitionDocs[id];
    }
}

void SC::AScannerController::setREDocumentSize (quint32 id, quint32 width, quint32 height) {
    { // Mutex guard.
        QMutexLocker		ml		(&_docsQueueMutex);
        // No such document in queue.
        if (!_recognitionDocs.contains(id)) {
            Wrn						<< "Requested Document doesn't exist!";
            return;
        }
        DeclareCheesecake           (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
        setREDocumentSize			(_recognitionDocs[id]+CKZ07, width, height);
    }
}
void SC::AScannerController::setREDocumentSize (RE::T::Document *doc, quint32 width, quint32 height) {
    Dbg								<< "Document" << doc->uid << "Width:" << width << "Height" << height << ".";
    QMutexLocker			ml		(&doc->mutex);
    // Only add data if non-null.
    if (width)
        doc->results["Metrics"]["Width"]    = QString::number(width);
    if (height)
        doc->results["Metrics"]["Height"]   = QString::number(height);
}

void SC::AScannerController::attachStringToREDocument (quint32 id, QString const &categoryId, QString const &stringId, const QVariant &string) {
    { // Mutex guard.
        QMutexLocker	ml			(&_docsQueueMutex);
        // No such document in queue.
        if (!_recognitionDocs.contains(id)) {
            Wrn						<< "Requested Document doesn't exist!";
            return;
        }
        attachStringToREDocument	(_recognitionDocs[id], categoryId, stringId, string);
    }
}
void SC::AScannerController::attachStringToREDocument (RE::T::Document *doc, QString const &categoryId, QString const &stringId, const QVariant &string) {
    QMutexLocker		ml		(&doc->mutex);
    Dbg							<< "Document" << doc->uid << "Attaching string" << stringId << ".";
    doc->results[categoryId][stringId]  = string;
}

void SC::AScannerController::attachImageToREDocument (quint32 id, QString const &imageId, const QImage &image) {
    { // Mutex guard.
        QMutexLocker	ml		(&_docsQueueMutex);
        // No such document in queue.
        if (!_recognitionDocs.contains(id)) {
            Wrn << "Requested Document doesn't exist!";
            return;
        }
        attachImageToREDocument	(_recognitionDocs[id], imageId, image);
    }
}
void SC::AScannerController::attachImageToREDocument (RE::T::Document *doc, QString const &imageId, const QImage &image) {
    QMutexLocker		ml		(&doc->mutex);
    Dbg							<< "Document" << doc->uid << "Attaching image ID:" << imageId << ".";
    doc->images[imageId]		= image.convertToFormat(image.isGrayscale() ? QImage::Format_Indexed8 : QImage::Format_ARGB32);
}

bool SC::AScannerController::shouldRejectREDocument (quint32 id) {
    RE::T::Document		*doc;
    { // Mutex guard.
        QMutexLocker	ml		(&_docsQueueMutex);
        // No such document in queue.
        if (!_recognitionDocs.contains(id)) {
            Wrn					<< "Requested Document doesn't exist!";
            return				true;
        }
        doc						= _recognitionDocs[id];
    }
    return						shouldRejectREDocument(doc);
}
bool SC::AScannerController::shouldRejectREDocument (RE::T::Document *doc) {
    Dbg											<< "Document" << doc->uid << ".";

    // Inconditional rejection flag.
    _rejectAll									= _deviceConfig.dataPath && _deviceConfig.dataPath[0] && QFileInfo(QString("%1/Controls/RejectAll").arg(_deviceConfig.dataPath)).exists();
    // Reject all flag is set...
    if (_rejectAll) {
        doc->rejectionReasons					<< tr("The Reject All flag is set.");
        if (_config.onNewDocument)
            (*_config.onNewDocument)(_config.userObject, doc->uid, -1, "", doc);
        /* PQR TODO
        if (_config.onDocumentRecognitionDone)
            (*_config.onDocumentRecognitionDone)(_config.userObject, doc->uid, doc->strings.count(), doc->strings["UUID"].toUtf8().constData(), doc);
        */
        return true;
    }
    // Recognition disabled.
    else if (!_config.enableRecognition) {
        doc->acceptationReasons					<< tr("Recognition is disabled.");
        if (_config.onNewDocument)
            (*_config.onNewDocument)(_config.userObject, doc->uid, -1, "", doc);
        /* PQR TODO
        if (_config.onDocumentRecognitionDone)
            (*_config.onDocumentRecognitionDone)(_config.userObject, doc->uid, doc->strings.count(), doc->strings["UUID"].toUtf8().constData(), doc);
        */
        return									false;
    }

    // Rejection tests are disabled, don't block on document recognition.
    if (!_config.enableRejectionTests) {
        if (!_docsProcessor->isRunning())
            _docsProcessor->start               ();
        _docsProcessor->queueDocument			(doc);
        doc->acceptationReasons					<< tr("Rejection tests are disabled.");
        return									false;
    }

    // Recognize document type.
    DeclareCheesecake                           (SC::SoftwareIdentifier, _hostUuid, _obfCheesecake);
    _re->guessDocumentType						(doc+CKZ07, QThread::idealThreadCount());

    // If there is a user callback, ask the third party application wether to reject (0), accept (1), or go on (2).
    if (_config.onNewDocument) {
        quint32		sort						= 2; // PQR TODO (*_config.onNewDocument)(_config.userObject, doc->uid, doc->typeData ? doc->typeData->code : -1, doc->strings.contains("Type.Name") ? doc->strings["Type.Name"].toUtf8().constData() : "", doc);
        // Rejection.
        if (sort==0) {
            doc->rejectionReasons				<< tr("Rejected by Callback #1.");
            doc->validity                       = RE::E::DocumentValidityInvalid;
            return								true;
        }
        // Acceptation.
        else if (sort==1) {
            doc->acceptationReasons				<< tr("Accepted by Callback #1.");
            doc->validity                       = RE::E::DocumentValidityValid;
            return								false;
        }
        // Invalid callback code.
        else if (sort>2)
            Wrn									<< "Third party application returned an invalid value for Callback #1!";
    }

    // Unknown type.
    if (!doc->typeData) {
        doc->rejectionReasons					<< tr("Unable to guess the document's type.");
        /* PQR TODO
        if (_config.onDocumentRecognitionDone) {
            QString const   &ts                 = doc->results["General"]["UUID"].toString();
            (*_config.onDocumentRecognitionDone)(_config.userObject, doc->uid, doc->strings.count(), ts.toUtf8().constData(), doc);
        }
        */
        return									true;
    }
    // Marked as "Reject when MICR doesn't match".
    else if (doc->typeData && doc->typeData->badMicrRejection && !doc->micrMatches) {
        doc->rejectionReasons					<< tr("MICR didn't match with the guessed type.");
        /* PQR TODO
        if (_config.onDocumentRecognitionDone)
            (*_config.onDocumentRecognitionDone)(_config.userObject, doc->uid, doc->strings.count(), doc->strings["UUID"].toUtf8().constData(), doc);
        */
        return									true;
    }

    // Recognize document and check for it's validity.
    _re->recognizeDocumentData                  (doc, QThread::idealThreadCount());

    // Third party application callback present.
    if (_config.onDocumentRecognitionDone) {
        quint32		sort						= 2; // PQR TODO (*_config.onDocumentRecognitionDone)(_config.userObject, doc->uid, doc->strings.count(), doc->strings["UUID"].toUtf8().constData(), doc);
        // Rejection.
        if (sort==0) {
            doc->rejectionReasons				<< tr("Rejected by Callback #2.");
            doc->validity                       = RE::E::DocumentValidityInvalid;
            return								true;
        }
        else if (sort==1) {
            doc->acceptationReasons				<< tr("Accepted by Callback #2.");
            doc->validity                       = RE::E::DocumentValidityValid;
            return								false;
        }
        // Invalid callback code.
        else if (sort>2)
            Wrn									<< "Third party application returned an invalid value for Callback #2!";
    }

    // Check document unicity.
    if (doc->results.contains("Serial")) {
        QString					serial			= doc->results["Serial"]["Raw"].toString();
        // Update unicity database.
        readUnicityDatabase						();
        // Doc already processed?
        if (_documentHistory.contains(serial)) {
            doc->validity						= RE::E::DocumentValidityInvalid;
            doc->rejectionReasons				<< tr("Unicity check failed.");
            return true;
        }
        // Update stolen database.
        readStolenDatabase						();
        // Doc is stolen?
        if (_stolenList.contains(serial)) {
            doc->validity						= RE::E::DocumentValidityInvalid;
            doc->rejectionReasons				<< tr("Stolen document.");
            return true;
        }
    }
    return doc->validity!=RE::E::DocumentValidityValid;
}

void SC::AScannerController::qdp_documentProcessed (void *d) {
    RE::T::Document	*doc				= (RE::T::Document*)d;
    // If there is a callback, notify of new document.
    /* PQR TODO
    if (_config.onNewDocument)
        (*_config.onNewDocument)(_config.userObject, doc->uid, doc->typeData ? doc->typeData->code : -1, doc->strings.contains("Type.Name") ? doc->strings["Type.Name"].toUtf8().constData() : "", doc);
    // If there is a callback, notify of recognition.
    if (_config.onDocumentRecognitionDone)
        (*_config.onDocumentRecognitionDone)(_config.userObject, doc->uid, doc->strings.count(), doc->strings["UUID"].toUtf8().constData(), doc);
    */
    // Finally release doc.
    releaseREDocument					(doc);
}

void SC::AScannerController::releaseREDocument (quint32 id) {
    RE::T::Document		*doc;
    { // Mutex guard.
        QMutexLocker	ml		(&_docsQueueMutex);
        // No such document in queue.
        if (!_recognitionDocs.contains(id)) {
            Wrn << "Requested Document doesn't exist!";
            return;
        }
        doc						= _recognitionDocs[id];
    }
    releaseREDocument			(doc);
}
void SC::AScannerController::releaseREDocument (RE::T::Document *doc) {
    // Release document.
    doc->semaphore.release          ();
    // Doc still being used somewhere else?
    if (doc->semaphore.available()<INT_MAX) {
        Dbg                         << "Document" << doc->uid << "still has a RC of" << INT_MAX-doc->semaphore.available() << ".";
        return;
    }
    // Lock queue, and remove document from it.
    { // Mutex guard.
        QMutexLocker    ml          (&_docsQueueMutex);
        _recognitionDocs.remove	(doc->uid);
    }

    Dbg                             << "Document" << doc->uid << "Attached Strings:" << doc->results.count() << "Images:" << doc->images.count() << ".";
    // Concatenate acceptation / rejection reasons.
    bool            bAccepted       = !_config.enableRejectionTests || doc->validity==RE::E::DocumentValidityValid;
    if (bAccepted)
        doc->results["General"]["AcceptationReason"]   = doc->acceptationReasons.join(" ");
    else
        doc->results["General"]["RejectionReason"]     = doc->rejectionReasons.join(" ");

    // Is there a data path provided?
    if (_deviceConfig.dataPath && _deviceConfig.dataPath[0]) {
        QString		szTS			= doc->results["General"]["UUID"].toString();
        QString		szOutDir		= QString("%1/%2").arg(_deviceConfig.dataPath).arg(bAccepted ? "Accepted" : "Rejected");

        // Document was rejected.
        if (!bAccepted) {
            QFile		fData		(QString("%1/%2_RR.txt").arg(szOutDir).arg(szTS));
            QTextStream	sData		(&fData);
            fData.open				(QIODevice::WriteOnly | QIODevice::Truncate);
            sData					<< doc->results["General"]["RejectionReason"].toString();
            fData.close				();
        }

        // Recognition is enabled...
        if (_config.enableRecognition) {
            { // Prepare text file.
                QSettings	fData	(QString("%1/%2.ini").arg(szOutDir).arg(szTS), QSettings::IniFormat);
                fData.setIniCodec	(QTextCodec::codecForName("UTF-8"));
                QStringList keys;
                // Export strings to individual file.
                /* PQR TODO
                for (RE::T::StringToStringMap::const_iterator i=doc->strings.constBegin(); i!=doc->strings.constEnd(); ++i)
                    fData.setValue	(i.key(), i.value());
                */
            }

            // If the document was accepted and if the machine UID is set, export results to day file.
            if (bAccepted && _config.machineUid!=0) {
                QSettings	fData	(QString("%1/Groupped/%2_%3.ini").arg(_deviceConfig.dataPath).arg(_config.machineUid,3,10,QChar('0')).arg(doc->timestamp.toString("yyyyMMdd")), QSettings::IniFormat);
                fData.setIniCodec	(QTextCodec::codecForName("UTF-8"));
                QStringList keys;
                fData.beginGroup	(szTS);
                // Export strings to individual file.
                /* PQR TODO
                for (RE::T::StringToStringMap::const_iterator i=doc->strings.constBegin(); i!=doc->strings.constEnd(); ++i)
                    fData.setValue	(i.key(), i.value());
                */
            }
        }

        // Special flag, save MICR / OCR in different files scope.
        if (_config.enableResultFilesSplit) {
            /* PQR TODO
            for (RE::T::StringToStringMap::const_iterator i=doc->strings.constBegin(); i!=doc->strings.constEnd(); ++i) {
                QString const	&k	= i.key();
                QString const	&v	= i.value();
                if (v.isEmpty())	continue;
                QFile		fData	(QString("%1/tmp").arg(szOutDir));
                fData.open			(QIODevice::WriteOnly);
                fData.write			(v.toUtf8().constData());
                fData.close			();
                fData.rename		(QString("%1/%2_%3.txt").arg(szOutDir).arg(szTS).arg(k));
            }
            */
        }

        // Export pictures scope, only if asked to or if document was rejected.
        if (_config.enableImagesSaving) {
            for (RE::T::StringToImageMap::const_iterator i = doc->images.constBegin(); i!=doc->images.constEnd(); ++i) {
                QString const	&k	= i.key();
                // Prepare filename.
                if (k=="Front")		i.value().save	(QString("%1/%2_F.tif").arg(szOutDir).arg(szTS), "TIFF", 100);
                else if (k=="Rear")	i.value().save	(QString("%1/%2_R.tif").arg(szOutDir).arg(szTS), "TIFF", 100);
                else				i.value().save	(QString("%1/%2_%3.tif").arg(szOutDir).arg(szTS).arg(k), "TIFF", 100);
            }
        }
    }
    Dbg						<< "Document" << doc->uid << "has been released.";
    delete					doc;
}


void SC::AScannerController::readStolenDatabase () {
    if (!_deviceConfig.dataPath || !_deviceConfig.dataPath[0])
        return;
    _stolenList.clear			();
    QString			dbFn		= QString("%1/Controls/Stolen.txt").arg(_deviceConfig.dataPath);
    // Check if there is a DB.
    if (!QFileInfo(dbFn).exists())
        return;
    // Read db.
    QFile			db			(dbFn);
    if (!db.open(QIODevice::ReadOnly)) {
        Wrn						<< "Unable to open the stolen documents database!";
        return;
    }
    QTextStream		in			(&db);
    while (true) {
        QString		ln			= in.readLine();
        // EOF?
        if (ln.isNull())		break;
        // This line already added?
        else if (!_stolenList.contains(ln))
            _stolenList			<< ln;
    }
    qDebug()					<< "Stolen documents database is now set to" << _stolenList.count() << "elements...";
    db.close					();
}
void SC::AScannerController::readUnicityDatabase () {
    if (!_deviceConfig.dataPath || !_deviceConfig.dataPath[0])
        return;
    _documentHistory.clear		();
    QString			dbFn		= QString("%1/Controls/Unicity.txt").arg(_deviceConfig.dataPath);
    // Check if there is a DB.
    if (!QFileInfo(dbFn).exists())
        return;
    // Read db.
    QFile			db			(dbFn);
    if (!db.open(QIODevice::ReadOnly)) {
        Wrn						<< "Unable to open the unicity database!";
        return;
    }
    QTextStream		in			(&db);
    while (true) {
        QString		ln			= in.readLine();
        // EOF?
        if (ln.isNull())		break;
        // This line already added?
        else if (!_documentHistory.contains(ln))
            _documentHistory	<< ln;
    }
    qDebug()					<< "Document history database is now set to" << _documentHistory.count() << "elements...";
    db.close					();
}
