#ifndef SC_PaniniController_h
#define SC_PaniniController_h

#include <QtCore/QtGlobal>
#ifdef Q_OS_WIN
// Base class.
#include "AScannerController.h"
// Required stuff.
#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>
#ifdef Q_OS_WIN
#   include <windows.h>
#endif

namespace SC {

class SCLibOpt PaniniController : public AScannerController {
Q_OBJECT

public:
    // Constructor / Destructor.
                                    PaniniController    		(LM::Core *lm, const DeviceConfig&, QObject *parent=0);
    virtual							~PaniniController           ();

    QList<quint32>&					availableResolutions		() const;
    quint32							deviceMaximumDocumentCount	() const;
    qint32							devicePrintMaxLength		() const;
    bool							deviceCanPrintsMultiline	() const;
    bool							doInitialize				();
    bool                            doDeviceSerial              (char *buff);
    bool							doConfigure					(const Config &config);
    bool							doStartProcessing			();
    void							doStopProcessing			();

    SC::E::DeviceState				deviceState					() const;
    bool							waitForDeviceState			(SC::E::DeviceState, unsigned long timeout=ULONG_MAX);

protected:
    void							run							();
    void                            purgeDocumentsQueue         (bool propagate, quint32 docIdLimit=UINT_MAX);

private:
    static LRESULT CALLBACK			staticListenerCallback		(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void							onDeviceEvent				(WPARAM wParam, LPARAM lParam);

    // Library handler.
    static HMODULE                 _hLib;
    // Various handles.
    HWND							_hWnd;
    DWORD							_hDevice;
    LOGFONTA						_font;
    // Various mutexes.
    QMutex							_waitMutex;
    QWaitCondition					_waitCondition;
    // Flag to know wether the device has to continue or stop feeding documents.
    bool							_shouldStop;
    bool							_errorHasOccured;
    // Mapping between panini doc id and internal id.
    QMap<quint32, RE::T::Document*>	_docsQueue;
    // Print image temp file.
    QByteArray						_printImageData;
};

}

#else
#include "DummyController.h"
namespace SC {
class SCLibOpt PaniniController : public DummyController {
Q_OBJECT
public: PaniniController        (LM::Core *lm, const DeviceConfig &devConfig, QObject *parent=0) : DummyController(lm, devConfig, parent) {}
};
}
#endif

#endif
