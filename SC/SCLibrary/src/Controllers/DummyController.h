#ifndef SC_DummyController_h
#define SC_DummyController_h

// Base class.
#include "AScannerController.h"
// Required stuff.
#include <QtCore/QList>

namespace SC {

class SCLibOpt DummyController : public AScannerController {
Q_OBJECT

public:
    // Constructor / Destructor.
                                    DummyController				(LM::Core *lm, const DeviceConfig &devConfig, QObject *parent=0);
    virtual							~DummyController			();

    QList<quint32>&					availableResolutions		() const;
    quint32							deviceMaximumDocumentCount	() const;
    qint32							devicePrintMaxLength		() const;
    bool							deviceCanPrintsMultiline	() const;
    bool							doInitialize				();
    bool                            doDeviceSerial              (char *buff);
    bool							doConfigure					(const Config &config);
    bool							doStartProcessing			();
    void							doStopProcessing			();

protected:
    virtual void					run							();
};

}

#endif
