#ifndef SC_EpsonTMSController_h
#define SC_EpsonTMSController_h

#include <QtCore/QtGlobal>
#ifdef Q_OS_WIN
// Base class.
#include "AScannerController.h"
// Required stuff.
#include <QtCore/QMap>
#include <QtCore/QList>
#ifdef Q_OS_WIN
#   include <windows.h>
#endif
#include "../../ThirdParty/win32/include/Epson/TMS2000/MultiFunction.h"

namespace SC {

class SCLibOpt EpsonTMSController : public AScannerController {
Q_OBJECT

public:
                                    EpsonTMSController          (LM::Core *lm, const DeviceConfig &devConfig, QObject *parent=0);
    virtual							~EpsonTMSController         ();

    QList<quint32>&					availableResolutions		() const;
    quint32							deviceMaximumDocumentCount	() const;
    qint32							devicePrintMaxLength		() const;
    bool							deviceCanPrintsMultiline	() const;
    bool							doInitialize				();
    bool                            doDeviceSerial              (char *buff);
    bool							doConfigure					(const Config &config);
    bool							doStartProcessing			();
    void							doStopProcessing			();

private:
    // Status / MICR callback.
    static int CALLBACK				staticScanStatusCallback	(DWORD, WORD, WORD, LPSTR);
    // Callback methods.
    quint32 						scanStatusCallback			(DWORD, WORD, WORD, LPSTR);
    qint32							deviceStatusCallback		(DWORD);
    bool							processScanFace				(DWORD, SC::E::DocumentSide);

    // Singleton instance.
    static EpsonTMSController	*_instance;
    // Library handler.
    static HMODULE					_hLib;
    // Device handle.
    qint32							_hDevice;
    // Documents queue.
    QMap<DWORD, ProcessedDocumentInfo>
                                    _docsQueue;
    bool							_doubleFeeding;

    // Device-specific structures.
    MF_BASE01						baseConf;
    MF_SCAN							scanFrontConf;
    MF_SCAN							scanRearConf;
    MF_MICR							micrConf;
    MF_OCR_AB						ocrConf1, ocrConf2;
    MF_PROCESS						procConf;
};

};

#else
#include "DummyController.h"
namespace SC {
class SCLibOpt EpsonTMSController : public DummyController {
Q_OBJECT
public:
    // Constructor / Destructor.
    /**/                            EpsonTMSController        (LM::Core *lm, const DeviceConfig &devConfig, QObject *parent=0) : DummyController(lm, devConfig, parent) {}
};
}
#endif

#endif
