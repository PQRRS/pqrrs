/*! @file SCTypes.h */

#ifndef SC_SCTypes_h
#define SC_SCTypes_h

#include <QtCore/QtGlobal>
#include <QtCore/QDebug>
#ifdef Q_OS_WIN
    #include <windows.h>
#endif

//! @brief Library Import / Export Definition.
#ifdef SCLib
#	define SCLibOpt Q_DECL_EXPORT
#else
#	define SCLibOpt Q_DECL_IMPORT
#endif

//! @brief Logging Helpers.
#ifndef Dbg
#   define Dbg      qDebug()    << Q_FUNC_INFO << "#"
#   define Wrn      qWarning()  << Q_FUNC_INFO << "#"
#   define Crt      qCritical() << Q_FUNC_INFO << "#"
#endif

//! @brief Define SCLibrary Calling Convention based on the platform's calling convention.
#ifndef SCCConv
    #ifdef Q_OS_WIN
        #define SCCConv WINAPI
    #endif
    #ifndef SCCConv
        #define SCCConv
    #endif
#endif
//! @brief Define RELibrary User Callback Calling Convention based on the platform's calling convention.
#ifndef SCCBCConv
    #ifdef Q_OS_WIN
        #define SCCBCConv WINAPI
    #endif
    #ifndef SCCBCConv
        #define SCCBCConv
    #endif
#endif

// Required stuff.
#include <QtCore/QVariant>
#include <QtCore/QSettings>
#include <QtCore/QDebug>

#pragma pack(1)

//! @brief Enumeration Meta-type Streaming Registration Helper Macro.
#define	RegisterEnumStream(T)\
        friend QDataStream&	operator<<	(QDataStream& s, T const& t)	{ int i=t; return s << i; };\
        friend QDataStream&	operator>>	(QDataStream& s, T &t)			{ int i; s >> i; t=(T)i; return s; }

/*! @brief Recognition Engine Namespace Definition. */
namespace SC {
    static const char * const   SoftwareIdentifier = "fr.PierreQR.RecognitionSuite.SCLibrary.Base";

    // ------------------------------------			Functions			------------------------------------ //
    //! @brief Native char* Delete-and-Copy Safeguard Helper.
    inline void					deleteAndCopy						(char *&dest, char const *source=0, bool deleteDest=true) {
        if (dest && deleteDest && dest!=source) {
            delete[]			dest;
            dest				= 0;
        }
        if (source && strlen(source)) {
            dest				= new char[strlen(source)+1];
            strcpy				(dest, source);
        }
    }
    //! @brief Native char* Delete-and-Copy Helper.
    inline void					deleteAndCopy						(char *&dest, QString const &source, bool deleteDest=true) {
        deleteAndCopy			(dest, source.toUtf8().constData(), deleteDest);
    }

    // ------------------------------------				Enums			------------------------------------ //
    //! @brief Enumerations Encapsulation as a Fake QObject for Reflection Meta Access.
    class SCLibOpt E {
    Q_GADGET
    Q_ENUMS(First ErrorCode DeviceIdentifier PaniniDeviceModel DeviceState DocumentSide Last)
    Q_FLAGS(DocumentSides)
    public:
        //! @brief Error Types Definition.
        enum ErrorCode {
            ErrorCodeNoError                = 0,
            ErrorCodeDeviceBadConfig,
            ErrorCodeDeviceNotFound,
            ErrorCodeDeviceJammed,
            ErrorCodeFeedingFailure,
            ErrorCodeIncompatibleDevice,
            ErrorCodeLibraryLoadingFailure,
            ErrorCodeLibraryFunctionFailure,
            ErrorCodeStartScanFailure,
            ErrorCodeNoPaper,
            ErrorCodeMicrReadingFailure,
            ErrorCodeOcrReadingFailure,
            ErrorCodeNoCartridge,
            ErrorCodeUnknownError
        };
        //! @brief Hardware Identifiers Definition.
        enum DeviceIdentifier {
            DeviceIdentifierCanonCR55		= 0,
            DeviceIdentifierCanonCR180		= 1,
            DeviceIdentifierCanonCR190		= 2,
            DeviceIdentifierEpsonTMS1000	= 3,
            DeviceIdentifierPaniniVisionX	= 4,
            DeviceIdentifierPaniniIDeal		= 5,
            DeviceIdentifierEpsonTMS2000	= 6,
            DeviceIdentifierPaniniVisionS   = 7,
            DeviceIdentifierPaniniWIDeal	= 8,
            DeviceIdentifierDummy			= 255
        };
        //! @brief Possible Device States Definition.
        enum DeviceState {
            DeviceStateUnknown				= 0,
            DeviceStateBusy,
            DeviceStateConfigurable,
            DeviceStateUp,
            DeviceStateDown
        };
        //! @brief Possible Document Sides to Scan Definition.
        enum DocumentSide {
            DocumentSideNone				= 0,
            DocumentSideFront				= 1,
            DocumentSideRear				= 2,
            DocumentSideBoth				= DocumentSideFront|DocumentSideRear
        };

        Q_DECLARE_FLAGS(DocumentSides, DocumentSide)
        RegisterEnumStream(SC::E::ErrorCode)
        RegisterEnumStream(SC::E::DeviceIdentifier)
        RegisterEnumStream(SC::E::DeviceState)
        RegisterEnumStream(SC::E::DocumentSide)
    };

    // ------------------------------------			Structures			------------------------------------ //

    //! @brief Processed Document Informative Structure Definition.
    struct SCLibOpt ProcessedDocumentInfo {
        // Members.
        quint32							documentId;
        E::DocumentSide					sides;
    };

    /*! @brief Device Configuration Structure Definition.
     * Holds configuration options to be passed to the scanner controller so it can configure a hardware device
     * accordingly. */
    struct SCLibOpt DeviceConfig {
    public:
        // Constructors / Destructor.
        DeviceConfig                    ();
        DeviceConfig                    (DeviceConfig const &dc);
        ~DeviceConfig                   ();
        // Assignment operator.
        DeviceConfig&					operator=						(DeviceConfig const &dc);
        // Overrides reader from settings file.
        static void						readOverrides					(DeviceConfig &c, QSettings const &s);
        // QDataStream friend serialization stuff.
        friend QDataStream&				operator<<						(QDataStream& s, DeviceConfig const &dc)	{
            return				s << dc.enableLogging << dc.deviceId << QString(dc.dataPath?dc.dataPath:"") << QString(dc.locale?dc.locale:"");
        }
        friend QDataStream&				operator>>						(QDataStream& s, DeviceConfig &dc)			{
            QString		dp, loc;
            s					>> dc.enableLogging >> dc.deviceId >> dp >> loc;
            deleteAndCopy		(dc.dataPath, dp);
            deleteAndCopy		(dc.locale, loc);
            return				s;
        }
        // Members.
        quint8							enableLogging;					// Logging.
        quint8                          deviceId;						// Device to use.
        char							*dataPath;						// Where to save data.
        char							*locale;						// Locale.
    };

    /*! @brief Configuration Structure Definition.
     * Holds global configuration (Callbacks, Printing, Sides, RECB Prefs, etc). */
    struct SCLibOpt Config {
    public:
        // Types.
        typedef quint32                 (SCCBCConv *ProcessingStartedCallback)        (void *userObject);
        typedef quint32                 (SCCBCConv *ProcessingFinishedCallback)       (void *userObject);
        typedef quint32                 (SCCBCConv *NewDocumentCallback)              (void *userObject, quint32 docId, quint32 code, char const *name, void *doc);
        typedef quint32                 (SCCBCConv *DocumentRecognitionDoneCallback)  (void *userObject, quint32 docId, quint32 stringCount, char const *timestamp, void *doc);
        //#if defined(SCLib) || defined(SCLibCppStyle)
        // Constructors / Destructor.
        /**/							Config							();
        /**/							Config							(Config const &c);
        /**/							~Config							();
        // Assignment operator.
        Config&							operator=						(Config const &c);
        // Overrides reader from settings file.
        static void						readOverrides					(Config &c, QSettings const &s);
        // QDataStream friend serialization stuff.
        //#endif
        friend QDataStream&				operator<<						(QDataStream& s, Config const &c) {
            return s << c.enableMicr << c.sides << c.reversedDocument << c.preferedResolution
                << c.enableColor << c.enableImagesSaving << c.dropFeedEnabled << c.machineUid << c.enableDocUidReset
                << c.enableRejectionTests << c.enableDoubleFeedingDetection
                << c.enableRecognition << c.enableProprietaryRecognition
                << QString(c.recognitionDataPath?c.recognitionDataPath:"")
                << c.enableResultFilesSplit << c.maximumDocumentCount
                << QString(c.printData?c.printData:"")
                << QString(c.printImageFilename?c.printImageFilename:"")
                << c.printInBold << c.printOffset << c.printImageOffset;
        }
        friend QDataStream&				operator>>						(QDataStream& s, Config &c) {
            QString		rdp, pd, pif;
            s >> c.enableMicr >> c.sides >> c.reversedDocument >> c.preferedResolution
                >> c.enableColor >> c.enableImagesSaving >> c.dropFeedEnabled >> c.machineUid >> c.enableDocUidReset
                >> c.enableRejectionTests >> c.enableDoubleFeedingDetection
                >> c.enableRecognition >> c.enableProprietaryRecognition
                >> rdp
                >> c.enableResultFilesSplit >> c.maximumDocumentCount
                >> pd >> pif
                >> c.printInBold >> c.printOffset >> c.printImageOffset;
            deleteAndCopy				(c.recognitionDataPath, rdp);
            deleteAndCopy				(c.printData, pd);
            deleteAndCopy				(c.printImageFilename, pif);
            return s;
        }
        // MICR options members.
        quint8							enableMicr;						// Activate MICR sensor.
        // Image options members.
        quint8                          sides;							// Sides to scan.
        quint8							reversedDocument;				// Get colored pictures.
        quint32							preferedResolution;				// Resolution.
        quint8							enableColor;					// Get colored pictures.
        quint8							enableImagesSaving;				// Save images to file.
        // Misc options members.
        quint8							dropFeedEnabled;				// Feed mode set to drop feed.
        quint32							machineUid;						// Machine identification.
        quint8							enableDocUidReset;				// Reset document increment.
        // Tests / Recognition.
        quint8							enableRejectionTests;			// Wether or not to do rejection tests.
        quint8							enableDoubleFeedingDetection;	// Activate double-feeding sensor.
        quint8							enableRecognition;				// RE activation.
        quint8							enableProprietaryRecognition;	// Proprietary OCR and barcode stuff.
        char							*recognitionDataPath;			// RE data file path.
        // Misc.
        quint8							enableResultFilesSplit;			// Create one file per result.
        quint32							maximumDocumentCount;			// Maximum documents to scan before returning.
        // Print.
        char							*printData;						// Data to print or zero if nothing.
        char							*printImageFilename;			// Image filename to be printed next to the text, or zero for none.
        quint8							printInBold;					// Bold font printing.
        quint32							printOffset;					// Text offset for printing.
        quint32							printImageOffset;				// Image offset for printing.
        // Callback.
        void                            *userObject;                    // User object sent to each callback function.
        ProcessingStartedCallback		onProcessingStarted;			// Start processing callback.
        ProcessingFinishedCallback		onProcessingFinished;			// Finished processing callback.
        NewDocumentCallback				onNewDocument;					// New document.
        DocumentRecognitionDoneCallback	onDocumentRecognitionDone;		// Recognition done.
    };

    // Device informations structure.
    struct SCLibOpt DeviceInformations {
    public:
        qint32							processedDocumentCounter;
        qint32							frankedDocumentsCounter;
    };

    // -------------------------------------		Other stuff			------------------------------------- //
    // Complete types registration for streaming and register metatypes.
    //#if defined(SCLib) || defined(SCLibCppStyle)
    SCLibOpt void						StaticInitializer				();
    SCLibOpt char const*                Version							();
    //#endif
}

#pragma pack()

// Enums.
Q_DECLARE_TYPEINFO(SC::E::ErrorCode, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(SC::E::ErrorCode)
Q_DECLARE_TYPEINFO(SC::E::DeviceIdentifier, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(SC::E::DeviceIdentifier)
Q_DECLARE_TYPEINFO(SC::E::DeviceState, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(SC::E::DeviceState)
Q_DECLARE_TYPEINFO(SC::E::DocumentSide, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(SC::E::DocumentSide)
// Flags.
Q_DECLARE_OPERATORS_FOR_FLAGS(SC::E::DocumentSides)
// Other types.
Q_DECLARE_TYPEINFO(SC::ProcessedDocumentInfo, Q_MOVABLE_TYPE);
Q_DECLARE_METATYPE(SC::ProcessedDocumentInfo)
Q_DECLARE_TYPEINFO(SC::DeviceConfig, Q_MOVABLE_TYPE);
Q_DECLARE_METATYPE(SC::DeviceConfig)
Q_DECLARE_TYPEINFO(SC::Config, Q_MOVABLE_TYPE);
Q_DECLARE_METATYPE(SC::Config)

#endif
