# Base settings.
include         (../../Common.pri)
DESTDIR         =   "$$MAINBINDIR"
TARGET          =   SCLibrary
TEMPLATE        =   lib
CONFIG          +=  shared
QT              +=  gui

include         (../../LMLibrary.pri)
include         (../../RELibrary.pri)
macx: {
    QMAKE_POST_LINK +=  echo "Running install_name_tool on $${TARGET}..." && \
                            install_name_tool \
                            -change \
                                "libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "@loader_path/libRELibrary.$${RELibraryMajVersion}.dylib" \
                                "$${DESTDIR}/lib$${TARGET}.dylib"
}

target.path     =   $${INSTALLDIR}/lib
INSTALLS        +=  target

VERSION             =   $${SCLibraryVersion}
message             ($${TARGET} is at version $${VERSION}.)
checkTouchVersion   ($$TARGET, $$VERSION)

DEFINES         +=  SCLib


win32 {
    LIBS        += -luser32 -lkernel32
    HEADERS     +=  ThirdParty/win32/include/Canon/CsdScan_CR55_CR180.h \
                    ThirdParty/win32/include/Canon/CsdScan_CR190.h \
                    ThirdParty/win32/include/Epson/TMS2000/EpsStmApiInterface.h \
                    ThirdParty/win32/include/Epson/TMS2000/MultiFunction.h \
                    ThirdParty/win32/include/Panini/VApiInterface.h \
                    ThirdParty/win32/include/QtMfcApp/qmfcapp.h
    SOURCES     +=  ThirdParty/win32/include/QtMfcApp/qmfcapp.cpp
}

TRANSLATIONS    +=  src/Translations/SCLibrary_fr.ts

RESOURCES       +=  src/SCLibrary.qrc

HEADERS         +=  src/Controllers/AScannerController.h \
                    src/Controllers/CanonCR180Controller.h \
                    src/Controllers/CanonCR190Controller.h \
                    src/Controllers/CanonCR55Controller.h \
                    src/Controllers/DummyController.h \
                    src/Controllers/EpsonTMSController.h \
                    src/Controllers/PaniniController.h \
                    src/QueuedDocumentProcessor.h \
                    src/SCLibrary.h \
                    src/SCTypes.h

SOURCES         +=  src/Controllers/AScannerController.cpp \
                    src/Controllers/CanonCR180Controller.cpp \
                    src/Controllers/CanonCR190Controller.cpp \
                    src/Controllers/CanonCR55Controller.cpp \
                    src/Controllers/DummyController.cpp \
                    src/Controllers/EpsonTMSController.cpp \
                    src/Controllers/PaniniController.cpp \
                    src/SCLibrary.cpp \
                    src/SCTypes.cpp
